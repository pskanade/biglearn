
## build BigLearn.Base.dll
xbuild BigLearn.Base/BigLearn.csproj

## build BigLearn.Common.dll
xbuild DeepLearningLib/Common/BigLearn.Common.csproj

## build BigLearn.DeepNet.exe file.
xbuild DeepLearningLib/DeepNet/BigLearn.DeepNet.csproj

xbuild DeepLearningLib/DeepNet/BigLearn.Test.csproj
