@echo off
Setlocal EnableDelayedExpansion
set nugetTool=Tools\Nuget\Nuget.exe
echo Pull down BigLearnBuilds package ...
set InstallCmd=%nugetTool% install BigLearnBuilds -Source https://msblox.pkgs.visualstudio.com/_packaging/BigLearnBuilds/nuget/v3/index.json 
for /f "tokens=1-5 delims= " %%a in ('call %InstallCmd%') do (
		set var=%%a
		set package=%%b
		if "%%a %%b"=="Successfully installed" (
			set package=%%c.%%d
			set var=%%a %%b
		) else if "%%a %%c %%d %%e"=="Package is already installed." (
			set var=%%a %%c %%d %%e
			set package=%%b
		) 
	)

set package=%package:~1,-1%
if "%var%"=="Successfully installed" (
	echo New package %package% installed.
) else if "%var%"=="Package is already installed." (
	echo Package %package% is already installed.
) else (
	echo Failed to install package.
	exit /b 0
)

echo Try to update targets folder with package files ...
xcopy /D /Y /S /I Packages\%package%\Targets Targets
