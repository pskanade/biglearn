#!/bin/bash
#docker run --rm -it -v `pwd`/../:/biglearn --tmpfs=/tmp -h biglearndev --name=biglearn_box -w /biglearn  biglearn
export NV_GPU=7 && nvidia-docker run --rm -it -v `pwd`/../:/biglearn --tmpfs=/tmp:rw,size=1024M,mode=1777 -v /data:/data --tmpfs=/tmp -h biglearndev --name=biglearn_box -w /biglearn  biglearn
