FROM ubuntu:16.04
ENV  LANG="en_US.UTF-8" LC_ALL="en_US.UTF-8" LANGUAGE="en_US.UTF-8" LC_TYPE="en_US.UTF-8" TERM=xterm
RUN locale-gen en_US en_US.UTF-8
RUN apt-get update
RUN apt-get install -y git build-essential wget vim rpm2cpio cpio mono-complete module-init-tools libgfortran3 libibumad3 libibverbs1 libpmi0
COPY vimrc /root/.vimrc

#Setup CUDA dev env
LABEL com.nvidia.volumes.needed="nvidia_driver"
LABEL com.nvidia.cuda.version="8.0"
ARG CUDA_URL="https://developer.nvidia.com/compute/cuda/8.0/prod/local_installers/cuda_8.0.44_linux-run"
ARG CUDA_VER=8.0.44
ARG CUDA_PATH=/usr/local/cuda
RUN wget -q -O /root/cuda_${CUDA_VER}.run ${CUDA_URL} &&\
	mkdir /root/cuda_${CUDA_VER} && /bin/sh /root/cuda_${CUDA_VER}.run --extract=/root/cuda_${CUDA_VER}

RUN wget -q -O /root/cuda_${CUDA_VER}/NVIDIA-Linux-x86_64-375.26.run ftp://download.nvidia.com/XFree86/Linux-x86_64/375.26/NVIDIA-Linux-x86_64-375.26.run

ENV CUDNN_VERSION=5 LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CUDA_PATH/lib64 CUDA_VERSION=8.0 CUDA_PKG_VERSION=8-0=8.0.44-1
RUN sh /root/cuda_${CUDA_VER}/NVIDIA-Linux-x86_64-375.26.run -sN --no-kernel-module \
	&& sh /root/cuda_${CUDA_VER}/cuda-linux*.run -noprompt \
	&& sh /root/cuda_${CUDA_VER}/cuda-samples*.run -noprompt -cudaprefix=/usr/local/cuda 

ENV LIBRARY_PATH /usr/local/cuda/lib64/stubs:${LIBRARY_PATH}

ARG CUDNN_URL="http://developer.download.nvidia.com/compute/redist/cudnn/v5.1/cudnn-8.0-linux-x64-v5.1.tgz"
RUN wget -q -O /root/cudnn-8.0-linux-x64-v5.1.tgz ${CUDNN_URL} && \
	tar -xzf /root/cudnn-8.0-linux-x64-v5.1.tgz -C /usr/local 

ARG MPI_PATH=/usr/local/mvapich
ARG MPI_LIB_PATH=$MPI_PATH/lib64
ARG MVAPICH_URL="http://mvapich.cse.ohio-state.edu/download/mvapich/gdr/2.2/mofed-3.2/mvapich2-gdr-cuda8.0-gnu-2.2-2.slurm.el7.centos.x86_64.rpm"
ARG MVAPICH_RPM=mvapich2-gdr-cuda8.0-gnu-2.2-2.slurm.el7.centos.x86_64.rmp 
RUN wget -q $MVAPICH_URL -O /root/$MVAPICH_RPM && \
	cd /root && rpm2cpio $MVAPICH_RPM |cpio -id && \
	ln -s /root/opt/mvapich2/gdr/2.2/cuda8.0/gnu /usr/local/mvapich 

ARG NCCL_URL=https://github.com/NVIDIA/nccl/archive/v1.2.3-1+cuda8.0.tar.gz
ARG NCCL_TAR=nccl.1.2.3.tar.gz
ARG NCCL_PATH=/usr/local/nccl
RUN wget -q $NCCL_URL -O /root/$NCCL_TAR &&\
	cd /root && tar -xzf $NCCL_TAR && \
	cd nccl-1.2.3-1-cuda8.0 && mkdir /usr/local/nccl &&\
	make -e PREFIX=/usr/local/nccl install

RUN echo "/usr/local/cuda/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
	ldconfig
ENV CUDA_DRIVER_PATH=$CUDA_PATH/lib64/stubs/  MPI_PATH=$MPI_PATH MPI_LIB_PATH=$MPI_LIB_PATH LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MPI_LIB_PATH:$NCCL_PATH/lib 
