#pragma once
#include <chana_ps.h>
#include <vcclr.h>

namespace CommLib {
#pragma managed
	using namespace System;
	using namespace System::Runtime::InteropServices;

	ref class ParameterServerBase;

	public class ParameterServerWrapper :public ChaNaPSBase
	{
	public:
		ParameterServerWrapper()
		{
		}

		void control(int cmd_id, void *data, const size_t len) override;

		void server_process_pull(
			uint64_t *keys,
			size_t key_count,
			void **vals,
			size_t *val_sizes,
			val_deallocator_t *dealloc,
			void **args,
			bool *fixed_val_size
		) override;

		void server_process_push(size_t key_count, uint64_t *keys, void **vals, size_t *valsizes) override;

		void worker_apply_pull(void *args) override;

		static void AttachPS(ParameterServerBase^ imp);
	protected:
		static gcroot<ParameterServerBase^> m_Impl;
	};

	public  ref class ParameterServerBase
	{
	protected:
		ParameterServerBase()
		{
			ParameterServerWrapper::AttachPS(this);
		}

	public:
		delegate void PsPushCallback(size_t count, uint64_t *keys, void **vals, size_t *val_sizes, void *args);
		delegate void DeAlocator(void *args);

		virtual void OnControl(int cmd_id, void *data, const size_t len)
		{
		}

		virtual void OnPullProc(
			uint64_t *keys,
			size_t key_count,
			void **vals,
			size_t *val_sizes,
			DeAlocator^ dealloc,
			void **args,
			bool *fixed_val_size
		) = 0;

		virtual void OnPushProc(size_t key_count, uint64_t *keys, void **vals, size_t *valsizes) = 0;

		virtual void OnPullAck(void *args) = 0;

		[DllImport("chanalib.dll", EntryPoint = "ChaNaPSControl")]
		static uint64_t ChaNaPSControl(int roles, void *data, size_t len, DeAlocator^ cb, void *args);

		[DllImport("chanalib.dll", EntryPoint = "ChaNaPSPull")]
		static uint64_t ChaNaPSPull(size_t count, uint64_t *keys, void **vals, size_t *val_sizes, void *args);

		[DllImport("chanalib.dll", EntryPoint = "ChaNaPSPush")]
		static uint64_t ChaNaPSPush(size_t count, uint64_t *keys, void **vals, size_t *val_sizes, PsPushCallback^ cb, void *args);
	};
}