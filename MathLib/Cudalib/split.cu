/*
   Parallel split kernels
   xjia@ust.hk 2015/03/18
 */
#include <iostream>
#include <cstdio>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <cstdlib>
#include <ctime>
#include <helper_cuda.h>
#include <thrust/scan.h>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <sys/time.h>
#include "stdafx.h"
using namespace std;

int *d_pidArray,*d_loc,*d_Hist,*d_psSum;
int numPart = 2; //even or odd
dim3 grid(256);
dim3 block(512);
int numThread = grid.x * block.x;
int Hist_len = numThread * numPart;
  __device__
int getPartID(float element)
{
  return (element == 0.0)?1:0;
}

  __global__
void mapPart(float *d_R,int *d_pidArray,int r_len)
{
  int threadId = blockIdx.x * blockDim.x + threadIdx.x;
  int threadNumber = blockDim.x * gridDim.x;
  while(threadId < r_len)
  {
	d_pidArray[threadId] = getPartID(d_R[threadId]);
	threadId += threadNumber;
  }
}

  __global__
void count_Hist(int *d_Hist,int *d_pidArray,int r_len, int numPart)
{
  __shared__ int s_Hist[2048];
  int threadId = blockIdx.x * blockDim.x + threadIdx.x;
  int threadNumber = blockDim.x * gridDim.x;
  int offset = threadIdx.x * numPart;

  for(int i = 0; i < numPart; ++i)
	s_Hist[i + offset] = 0;

  for(int i = threadId; i < r_len; i += threadNumber)
	s_Hist[offset + d_pidArray[i]]++;

  for(int i = 0; i < numPart; ++i)
	d_Hist[i * threadNumber + threadId] = s_Hist[offset + i];
}

  __global__
void write_Hist(int d_pidArray[],int d_psSum[],int d_loc[],int numPart,int r_len)
{
  __shared__ int s_psSum[2048];
  int threadId = threadIdx.x + blockIdx.x * blockDim.x;
  int threadNumber = gridDim.x * blockDim.x;
  int offset = threadIdx.x * numPart;

  for(int i = 0; i < numPart; ++i)
	s_psSum[i + offset] = d_psSum[threadId + i * threadNumber];

  for(int i = threadId; i < r_len; i += threadNumber)
  {
	int pid = d_pidArray[i];
	d_loc[i] = s_psSum[pid + offset];
	s_psSum[pid+ offset]++;
  }
}

  __global__
void scatter(float d_Rin[],float d_Rout[],int *d_id,int d_loc[],int r_len,int *cnt)
{
  int threadId = threadIdx.x + blockIdx.x * blockDim.x;
  int threadNumber = blockDim.x * gridDim.x;
  int loc;
  while(threadId < r_len)
  {
	loc = d_loc[threadId];
	if(loc < (*cnt))
	{
	  d_Rout[loc] = d_Rin[threadId];
	  d_id[loc] = threadId;
	}
	threadId += threadNumber;
  }
}

__global__
void updateGradient(float *gpu_floats_weight,int *id,float *delta,int *cnt,int numprocs,int offset,int rank)
{
  int threadId = threadIdx.x + blockIdx.x * blockDim.x;
  int threadNumber = blockDim.x * gridDim.x;
  int pos;
  for(int i = 0; i < numprocs; ++i)
  {
	if(i == rank)
	  continue;
	for(int j = threadId; j < cnt[i]; j += threadNumber)
	{
	  pos = i * offset + j;
	  gpu_floats_weight[id[pos]] += delta[pos];
	}
  }
}

double diffTime2(timeval start,timeval end)
{
  return (end.tv_sec - start.tv_sec) * 1000 + (end.tv_usec - start.tv_usec) * 0.001;
}

void split_init(int r_len)
{
  checkCudaErrors(cudaMalloc(&d_pidArray,sizeof(int) * r_len));
  checkCudaErrors(cudaMalloc(&d_loc,sizeof(int) * r_len));
  checkCudaErrors(cudaMalloc(&d_Hist,sizeof(int) * Hist_len));
  checkCudaErrors(cudaMalloc(&d_psSum,sizeof(int) * Hist_len));
}
void split(float *d_R,float *d_result,int *d_id,int r_len,int *cnt)
{
  cudaDeviceSynchronize();
  mapPart<<<grid,block>>>(d_R,d_pidArray,r_len);
  count_Hist<<<grid,block>>>(d_Hist,d_pidArray,r_len,numPart);

  thrust::device_ptr<int> dev_Hist(d_Hist);
  thrust::device_ptr<int> dev_psSum(d_psSum);
  thrust::exclusive_scan(dev_Hist,dev_Hist + Hist_len,dev_psSum);

  checkCudaErrors(cudaMemcpy(cnt,d_psSum + numThread,sizeof(int),cudaMemcpyDeviceToDevice));

  write_Hist<<<grid,block>>>(d_pidArray,d_psSum,d_loc,numPart,r_len);
  scatter<<<grid,block>>>(d_R,d_result,d_id,d_loc,r_len,cnt);
  cudaDeviceSynchronize();
}

void update(float *gpu_floats_weight,int *id,float *delta,int *cnt,int numprocs,int offset,int rank)
{
  dim3 block(1024);
  dim3 grid((offset - 1) / block.x + 1);
  updateGradient<<<grid,block>>>(gpu_floats_weight,id,delta,cnt,numprocs,offset,rank);
  cudaDeviceSynchronize();
}
