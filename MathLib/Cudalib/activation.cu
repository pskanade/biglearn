#include "stdafx.h"
#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>
#include "device_functions.h"
#include <fstream>

#include "cublas_v2.h"
#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

using namespace std;

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
using namespace _com_util;
#else
#include <cfloat>
#endif

//////////////////////////////////////////////////////////////////////////////////////// Logistic/Sigmoid Function Family.
/// One Dimension Logistic/Sigmoid Function.
__global__ void cuda_Logistic(float * a, float * b,  int size, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < size)
	{
		float v = tanhf(a[idx] * gamma / 2.0f);
		b[idx] = (v + 1.0f) / 2.0f; // (1.0 + expf(- a[idx] * gamma)); 
	}
}
/// Two Dimension Logistic/Sigmoid Function.
__global__ void cuda_Logistic(float ** a, float ** b, int row, int column, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < column && idy < row)
	{
		float v = tanhf(a[idy][idx] * gamma / 2.0f);
		b[idy][idx] = (v + 1.0f) / 2.0f;
	}
}
// b = logistc(a, gamma)
void Cuda_Logistic(float * a, float * b, int size, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (size + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_Logistic <<<nBlockPerGrid, nThreadPerBlock >> >(a, b, size, gamma);
}
// b = logistc(a, gamma)
void Cuda_Logistic(float ** a, float ** b, int row,  int column, float gamma)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid( (column - 1) / DEFAULT_THREAD_PER_DIM + 1, (row - 1) / DEFAULT_THREAD_PER_DIM + 1 );
	cuda_Logistic <<<nBlockPerGrid, nThreadPerBlock >>>(a, b, row, column, gamma);
}


__global__ void cuda_Log(float * src, float * dst, float alpha, float beta, int size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < size)
	{
		dst[idx] = alpha * dst[idx] + beta * logf(src[idx]);
	}
}
void Cuda_Log(float * a, float * b, float alpha, float beta, int size)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (size + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_Log <<<nBlockPerGrid, nThreadPerBlock >> >(a, b, alpha, beta, size);
}


// b = b * a * (1-a) * gamma;
__global__ void cuda_DerivLogistic(float * a, float * b, int size, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < size)
	{
		b[idx] = b[idx] * a[idx] * (1- a[idx]) * gamma; 
	}
}
__global__ void cuda_DerivLogistic(float ** a, float ** b, int row, int column, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < column && idy < row)
	{
		b[idy][idx] = b[idy][idx] * a[idy][idx] * (1 - a[idy][idx]) * gamma;
	}
}
__global__ void cuda_deriv_sigmoid(float * delta, float * layer_output, int batchsize, int m)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < m && idy < batchsize)
	{
		delta[idy * m + idx] = delta[idy * m + idx] * (layer_output[idy * m + idx]) * (1 - layer_output[idy * m + idx]);
	}
}
__global__ void cuda_DerivSigmoid(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float alpha)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < m && idy < batchsize)
	{
		int id = idy * m + idx;
		inputDeriv[id] = inputDeriv[id] * alpha + outputDeriv[id] * (1 - output[id]) * output[id];
	}
}
// b = b * a * (1-a)
void Cuda_DerivLogistic(float * a, float * b, int size, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (size + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_DerivLogistic <<<nBlockPerGrid, nThreadPerBlock >> >(a, b, size, gamma);
}
// b = b * a * (1-a)
void Cuda_DerivLogistic(float ** a, float ** b, int row,  int column, float gamma)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid( (column - 1) / DEFAULT_THREAD_PER_DIM + 1, (row - 1) / DEFAULT_THREAD_PER_DIM + 1 );
	cuda_DerivLogistic <<<nBlockPerGrid, nThreadPerBlock >>>(a, b, row, column, gamma);
}
void cuda_Deriv_Sigmoid(float * delta, float * layer_output, int batchsize, int m)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((m + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_deriv_sigmoid << <block_tail, thread_tail >> >(delta, layer_output, batchsize, m);
}
void Cuda_DerivSigmoid(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((m - 1) / DEFAULT_THREAD_PER_DIM + 1, (batchsize - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_DerivSigmoid << <block_tail, thread_tail >> >(inputDeriv, outputDeriv, output, batchsize, m, wei_a);
}


__global__ void cuda_Tanh(float * a, float * b, int size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < size)
	{
		b[idx] = tanhf(a[idx]);
	}
}
__global__ void cuda_Tanh(float ** a, float ** b, int row, int column)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;

	if (idx < column && idy < row)
	{
		b[idy][idx] = tanhf(a[idy][idx]);
	}
}
// b = tanh(a)
void Cuda_Tanh(float * a, float * b, int size)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (size + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_Tanh <<<nBlockPerGrid, nThreadPerBlock >> >(a, b, size);
}
// b = tanh(a)
void Cuda_Tanh(float ** a, float ** b, int row, int column)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((column - 1) / DEFAULT_THREAD_PER_DIM + 1, (row - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_Tanh << <nBlockPerGrid, nThreadPerBlock >> >(a, b, row, column);
}

__global__ void cuda_DerivTanh(float * a, float * b, int size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < size)
	{
		b[idx] = b[idx] * ( 1 + a[idx] ) * (1-a[idx]);
	}
}
__global__ void cuda_DerivTanh(float ** a, float ** b, int row, int column)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;

	if (idx < column && idy < row)
	{
		b[idy][idx] = b[idy][idx] * (1 + a[idy][idx]) * (1 - a[idy][idx]);
	}
}
__global__ void cuda_deriv_tanh(float * delta, float * layer_output, int batchsize, int m)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < m && idy < batchsize)
	{
		delta[idy * m + idx] = delta[idy * m + idx] * (1 - layer_output[idy * m + idx]) * (1 + layer_output[idy * m + idx]);
	}
}
__global__ void cuda_DerivTanh(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < m && idy < batchsize)
	{
		int id = idy * m + idx;
		inputDeriv[id] = inputDeriv[id] * wei_a + outputDeriv[id] * (1 - output[id]) * (1 + output[id]);
	}
}
// b = b * (1+a) * (1-a)
void Cuda_DerivTanh(float * a, float * b, int size)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (size + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_DerivTanh <<<nBlockPerGrid, nThreadPerBlock >> >(a, b, size);
}
// b = b * (1+a) * (1-a)
void Cuda_DerivTanh(float ** a, float ** b, int row, int column)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid( (column - 1) / DEFAULT_THREAD_PER_DIM + 1, (row - 1) / DEFAULT_THREAD_PER_DIM + 1 );
	cuda_DerivTanh << <nBlockPerGrid, nThreadPerBlock >> >(a, b, row, column);
}
void cuda_Deriv_Tanh(float * delta, float * layer_output, int batchsize, int m)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((m + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_deriv_tanh << <block_tail, thread_tail >> >(delta, layer_output, batchsize, m);
}
void Cuda_DerivTanh(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((m + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_DerivTanh << <block_tail, thread_tail >> >(inputDeriv, outputDeriv, output, batchsize, m, wei_a);
}


__global__ void cuda_ReLU(float * a, float * b, int size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < size)
	{
		b[idx] = a[idx] < 0 ? 0 : a[idx];
	}
}
__global__ void cuda_ReLU(float ** a, float ** b, int row, int column)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;

	if (idx < column && idy < row)
	{
		b[idy][idx] = a[idy][idx] < 0 ? 0 : a[idy][idx];
	}
}
// b = relu(a)
void Cuda_ReLU(float * a, float * b, int size)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (size + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_ReLU << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(a, b, size);
}
// b = relu(a)
void Cuda_ReLU(float ** a, float ** b, int row, int column)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((column - 1) / DEFAULT_THREAD_PER_BLOCK + 1, (row - 1) / DEFAULT_THREAD_PER_BLOCK + 1);
	cuda_ReLU << <nBlockPerGrid, nThreadPerBlock >> >(a, b, row, column);
}

__global__ void cuda_deriv_rectified(float * delta, float * layer_output, int batchsize, int m)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < m && idy < batchsize)
	{
		if (layer_output[idy * m + idx] <= 0)
		{
			delta[idy * m + idx] = 0; // delta[idy * m +idx] ;
		}
	}
}
__global__ void cuda_DerivRectified(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < m && idy < batchsize)
	{
		int id = idy * m + idx;
		if (output[id] <= 0) inputDeriv[id] = inputDeriv[id] * wei_a; // delta[idy * m +idx] ;
		else inputDeriv[id] = inputDeriv[id] * wei_a + outputDeriv[id];
	}
}
__global__ void cuda_DerivReLU(float * a, float * b, int size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < size)
	{
		b[idx] = a[idx] < 0 ? 0 : b[idx];
	}
}
__global__ void cuda_DerivReLU(float ** a, float ** b, int row, int column)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;

	if (idx < column && idy < row)
	{
		b[idy][idx] = a[idy][idx] < 0 ? 0 : b[idy][idx];
	}
}
// b = a == 0 ? 0 : b;
void cuda_Deriv_Rectified(float * delta, float * layer_output, int batchsize, int m)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((m + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_deriv_rectified << <block_tail, thread_tail >> >(delta, layer_output, batchsize, m);
}
void Cuda_DerivRectified(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((m + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_DerivRectified << <block_tail, thread_tail >> >(inputDeriv, outputDeriv, output, batchsize, m, wei_a);
}
void Cuda_DerivReLU(float * a, float * b, int size)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (size + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_DerivReLU << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(a, b, size);
}
void Cuda_DerivReLU(float ** a, float ** b, int row, int column)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((column - 1) / DEFAULT_THREAD_PER_DIM + 1, (row - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_DerivReLU << <nBlockPerGrid, nThreadPerBlock >> >(a, b, row, column);
}

__global__ void cuda_crossentropyloss(float * outputScore, float * outputLabel, float * outputDeriv, int outputSize)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < outputSize)
	{
		float score = (tanhf(outputScore[idx] / 2) + 1) / 2.0f;
		if (outputLabel[idx] == 10001) outputDeriv[idx] = 0;
		else if (outputLabel[idx] > 0) outputDeriv[idx] = (1 - score);
		else outputDeriv[idx] = (-score);
	}
}
void cuda_CrossEntropyLoss(float * outputScore, float * outputLabel, float * outputDeriv, int outputSize)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (outputSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_crossentropyloss << <nBlockPerGrid, nThreadPerBlock >> >(outputScore, outputLabel, outputDeriv, outputSize);
}

__global__ void cuda_MultiClassSoftmax(float * outputScore, float * outputDeriv, int dim, float gamma, int batchSize)
{
	// int idx = blockDim.x * blockIdx.x + threadIdx.x;
	__shared__ float buffer[9];
	int k = blockIdx.x;
	float *input_k = outputScore + k * dim;
	float *output_k = outputDeriv + k * dim;

	int i_start = threadIdx.x;
	int i_end = dim;
	int i_step = blockDim.x;	//8 thread in the block

	// max?
	buffer[threadIdx.x] = -FLT_MAX;
	for (int i = i_start; i<i_end; i += i_step)
	{
		float z = input_k[i];
		if (buffer[threadIdx.x] < z) buffer[threadIdx.x] = z;
	}
	__syncthreads();

	// reduce
	if (threadIdx.x == 0)
	{
		float max_k = -FLT_MAX;
		for (int i = 0; i<blockDim.x; i++)
		{
			if (max_k < buffer[i]) max_k = buffer[i];
		}
		buffer[8] = max_k;
	}

	__syncthreads();

	// sum?
	float max_k = buffer[8];
	buffer[threadIdx.x] = 0;
	for (int i = i_start; i<i_end; i += i_step)
	{
		float z = __expf(gamma * (input_k[i] - max_k));
		buffer[threadIdx.x] += z;
		output_k[i] = z;
	}

	__syncthreads();

	// reduce
	if (threadIdx.x == 0)
	{
		float sum_k = 0;
		for (int i = 0; i<blockDim.x; i++) sum_k += buffer[i];
		buffer[8] = sum_k;
	}

	__syncthreads();

	// softmax
	float sum_k = buffer[8];
	for (int i = i_start; i<i_end; i += i_step)
		output_k[i] = output_k[i] / sum_k;
}
void Cuda_MultiClassSoftmax(float * outputScore, float * outputDeriv, int dim, float gamma, int batchSize)
{
	int nThreadPerBlock = 8;
	int nBlockPerGrid = batchSize;
	cuda_MultiClassSoftmax << <nBlockPerGrid, nThreadPerBlock >> >(outputScore, outputDeriv, dim, gamma, batchSize);
}

__global__ void cuda_SparseMultiClassSoftmax(int * smpIdx, float * outputScore, float * outputProb, float gamma, int batchSize)
{
	// int idx = blockDim.x * blockIdx.x + threadIdx.x;
	__shared__ float buffer[9];
	int k = blockIdx.x;
	int ks = k == 0 ? 0 : smpIdx[k - 1];
	int ke = smpIdx[k];

	float *input_k = outputScore + ks;
	float *output_k = outputProb + ks;

	int i_start = threadIdx.x;
	int i_end = ke - ks;
	int i_step = blockDim.x;	//8 thread in the block

	// max?
	buffer[threadIdx.x] = -FLT_MAX;
	for (int i = i_start; i<i_end; i += i_step)
	{
		float z = input_k[i];
		if (buffer[threadIdx.x] < z) buffer[threadIdx.x] = z;
	}
	__syncthreads();

	// reduce
	if (threadIdx.x == 0)
	{
		float max_k = -FLT_MAX;
		for (int i = 0; i<blockDim.x; i++)
		{
			if (max_k < buffer[i]) max_k = buffer[i];
		}
		buffer[8] = max_k;
	}

	__syncthreads();

	// sum?
	float max_k = buffer[8];
	buffer[threadIdx.x] = 0;
	for (int i = i_start; i<i_end; i += i_step)
	{
		float z = __expf(gamma * (input_k[i] - max_k));
		buffer[threadIdx.x] += z;
		output_k[i] = z;
	}

	__syncthreads();

	// reduce
	if (threadIdx.x == 0)
	{
		float sum_k = 0;
		for (int i = 0; i<blockDim.x; i++) sum_k += buffer[i];
		buffer[8] = sum_k;
	}

	__syncthreads();

	// softmax
	float sum_k = buffer[8];
	for (int i = i_start; i<i_end; i += i_step)
		output_k[i] = output_k[i] / sum_k;
}
void Cuda_SparseMultiClassSoftmax(int * smpIdx, float * outputScore, float * outputProb, float gamma, int batchSize)
{
	int nThreadPerBlock = 8;
	int nBlockPerGrid = batchSize;
	cuda_SparseMultiClassSoftmax << <nBlockPerGrid, nThreadPerBlock >> >(smpIdx, outputScore, outputProb, gamma, batchSize);
}

__global__ void cuda_DerivLogSparseMultiClassSoftmax(int * smpIdx, float * outputProb, float * probDeriv, float * outputDeriv, float gamma, float alpha, float beta, int batchSize)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int batchS = idx == 0 ? 0 : smpIdx[idx - 1];
		int batchE = smpIdx[idx];
		float dp = 0;
		for (int i = batchS; i < batchE; i++)
		{
			dp += probDeriv[i];
		}
		for (int i = batchS; i < batchE; i++)
		{
			outputDeriv[i] = alpha * outputDeriv[i] + beta * gamma * (probDeriv[i] - dp *  outputProb[i]);
		}
	}
}
void Cuda_DerivLogSparseMultiClassSoftmax(int * smpIdx, float * outputProb, float * probDeriv, float * outputDeriv, float gamma, float alpha, float beta, int batchSize)
{
	cuda_DerivLogSparseMultiClassSoftmax << <(batchSize - 1) / 16 + 1, 16 >> >(smpIdx, outputProb, probDeriv, outputDeriv, gamma, alpha, beta, batchSize);
}

__global__ void cuda_DerivLogProbEntropy(int * smpIdx, float * outputProb, float * logProbDeriv, float alpha, float beta, float epislon, int batchSize)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int batchS = idx == 0 ? 0 : smpIdx[idx - 1];
		int batchE = smpIdx[idx];
		float dp = 0;
		for (int i = batchS; i < batchE; i++)
		{
			logProbDeriv[i] = alpha * logProbDeriv[i] - beta * outputProb[i] * (1 + logf(outputProb[i] + epislon)); 
		}
	}
}
// add maxentropy regularization into policy gradient.
void Cuda_DerivLogProbEntropy(int * smpIdx, float * outputProb, float * logProbDeriv, float alpha, float beta, float epislon, int batchSize)
{
	cuda_DerivLogProbEntropy << <(batchSize - 1) / 16 + 1, 16 >> >(smpIdx, outputProb, logProbDeriv, alpha, beta, epislon, batchSize);
}

__global__ void cuda_DerivSparseMultiClassSoftmax(int * smpIdx, float * outputProb, float * probDeriv, float * outputDeriv, float gamma, float alpha, float beta, int batchSize)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int batchS = idx == 0 ? 0 : smpIdx[idx - 1];
		int batchE = smpIdx[idx];
		float dp = 0;
		for (int i = batchS; i < batchE; i++)
		{
			dp += outputProb[i] * probDeriv[i];
		}
		for (int i = batchS; i < batchE; i++)
		{
			outputDeriv[i] = alpha * outputDeriv[i] + beta * gamma * outputProb[i] * (probDeriv[i] - dp);
		}
	}
}
void Cuda_DerivSparseMultiClassSoftmax(int * smpIdx, float * outputProb, float * probDeriv, float * outputDeriv, float gamma, float alpha, float beta, int batchSize)
{
	cuda_DerivSparseMultiClassSoftmax << <(batchSize - 1) / 16 + 1, 16 >> >(smpIdx, outputProb, probDeriv, outputDeriv, gamma, alpha, beta, batchSize);
}

__global__ void cuda_LogProb2Deriv(float * outputProb, int dim, int batchSize, float * smpProb, 
		float * label, int * labelMask, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int lid = labelMask == NULL ? idx : labelMask[idx];
		int l = label == NULL ? 0 : (int)label[lid];
		float p = outputProb[idx * dim + l];
		smpProb[lid] = (p < eps ? log2f(eps) : log2f(p));
		outputProb[idx * dim + l] = outputProb[idx * dim + l] - 1;
	}
}
float Cublas_LogLossDeriv(cublasHandle_t handle, float * outputProb, int dim, int batchSize, float * smpProb, 
		float * label, float * labelwei, int * labelMask, float gamma, float eps)
{
	float result = 0;

	if (labelwei == NULL)
	{
		cuda_LogProb2Deriv << <(batchSize - 1) / 64 + 1, 64 >> >(outputProb, dim, batchSize, smpProb, label, labelMask, eps);
		cublasSasum(handle, batchSize, smpProb, 1, &result);
		gamma = gamma * -1;
		cublasSscal(handle, batchSize * dim, &gamma, outputProb, 1);
	}
	return result;
}

__global__ void cuda_DerivSoftmax(float ** outputScore, int * label, float ** outputDeriv, int dim, float gamma, int batchSize, float * targetProb)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		float * pScore = outputScore[idx];


		float log_sum = gamma * pScore[0];
		for (int i = 1; i < dim; i++)
		{
			float tmpa = gamma * pScore[i];
			if (log_sum >= tmpa) log_sum = log_sum + logf(1 + expf((tmpa - log_sum)));
			else log_sum = tmpa + logf(1 + expf((log_sum - tmpa)));
		}
		__syncthreads();

		float * pDeriv = outputDeriv[idx];
		for (int i = 0; i < dim; i++)
		{
			float tmpa = gamma * pScore[i];
			pDeriv[i] = -(float)(expf(tmpa - log_sum) * gamma);
		}
		targetProb[idx] = targetProb[idx] * (-pDeriv[label[idx]] / gamma);
		pDeriv[label[idx]] += gamma;
		/*for (int i = 0; i < dim; i++)
		  {
		  if (outputDeriv[idx][i] > 0)
		  printf("here1 is something wrong labelIdx %d, dim %d, outputDeriv %f \n", label[idx], dim, outputDeriv[idx][i]);
		  }*/

		//	//if (targetProb[idx] < 0)
		//	//	printf("here2 is something wrong labelIdx %d outputDeriv %f \n", label[idx], outputDeriv[idx][label[idx]]);

	}
}
void Cuda_DerivSoftmax(float ** outputScore, int * label, float ** outputDeriv, int dim, float gamma, int batchSize, float * targetProb)
{
	cuda_DerivSoftmax << <(batchSize - 1) / 32 + 1, 32 >> >(outputScore, label, outputDeriv, dim, gamma, batchSize, targetProb);
}

__global__ void cuda_CalculateSeqLinkMatrixs_Test(float * SeqMatrix, int dim, int bSum, float **linkMatrix)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < bSum)
	{
		linkMatrix[idx] = SeqMatrix + idx * dim;
	}
}

void Cuda_DerivSoftmax_Test(float * outputScore, int * outputLabel, float * outputDeriv, int dim, float gamma, int batchSize, float * targetProb)
{
	float ** outputLinkMatrix;
	float ** derivLinkMatrix;

	cudaMalloc((void **)&outputLinkMatrix, (batchSize *  dim) * sizeof(float *));
	cudaMalloc((void **)&derivLinkMatrix, (batchSize + dim) * sizeof(float *));

	cuda_CalculateSeqLinkMatrixs_Test << <(batchSize - 1) / 32 + 1, 32 >> >(outputScore, dim, batchSize, outputLinkMatrix);
	cuda_CalculateSeqLinkMatrixs_Test << <(batchSize - 1) / 32 + 1, 32 >> >(outputDeriv, dim, batchSize, derivLinkMatrix);

	cuda_DerivSoftmax << <(batchSize - 1) / 32 + 1, 32 >> >(outputLinkMatrix, outputLabel, derivLinkMatrix, dim, gamma, batchSize, targetProb);
}

__global__ void cuda_DerivSoftmax(float * outputScore, int * outputLabel, float * outputDeriv, int dim, float gamma, int batchSize, float * targetProb)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int mIdx = idx * dim;
		float log_sum = gamma * outputScore[mIdx];
		for (int i = 1; i < dim; i++)
		{
			float tmpa = gamma * outputScore[mIdx + i];
			if (log_sum >= tmpa) log_sum = log_sum + __logf(1 + __expf((tmpa - log_sum)));
			else log_sum = tmpa + __logf(1 + __expf((log_sum - tmpa)));
		}

		for (int i = 0; i < dim; i++)
		{
			float tmpa = gamma * outputScore[mIdx + i];
			outputDeriv[mIdx + i] = -__expf(tmpa - log_sum) * gamma;
		}
		targetProb[idx] = targetProb[idx] * (- outputDeriv[mIdx + outputLabel[idx]] / gamma);
		outputDeriv[mIdx + outputLabel[idx]] += gamma;
	}
}
void Cuda_DerivSoftmax(float * outputScore, int * outputLabel, float * outputDeriv, int dim, float gamma, int batchSize, float * targetProb)
{
	cuda_DerivSoftmax << <(batchSize - 1) / 32 + 1, 32 >> >(outputScore, outputLabel, outputDeriv, dim, gamma, batchSize, targetProb);
}

__global__ void cuda_Softmax(float * a, float * b, int labelDim, int batchsize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float log_sum = 0;
		int mIdx = idx * labelDim;
		float tmpa = gamma * a[mIdx];
		log_sum = tmpa;

		for (int i = 1; i < labelDim; i++)
		{
			tmpa = gamma * a[mIdx + i];
			if (log_sum >= tmpa) log_sum = log_sum + logf(1 + expf((tmpa - log_sum)));
			else log_sum = tmpa + logf(1 + expf((log_sum - tmpa)));
		}

		for (int i = 0; i < labelDim; i++)
		{
			tmpa = gamma * a[mIdx + i];
			b[mIdx + i] = expf(tmpa - log_sum);
		}
	}
}
void Cuda_SoftMax(float * a, float * b, int labelDim, int batchsize, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_Softmax<< <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(a, b, labelDim, batchsize, gamma);
}
__global__ void cuda_LogSoftmax(float * a, float * b, int labelDim, int batchsize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		int mIdx = idx * labelDim;
		float log_sum = gamma * a[mIdx];

		for (int i = 1; i < labelDim; i++)
		{
			float tmpa = gamma * a[mIdx + i];
			if (log_sum >= tmpa) log_sum = log_sum + logf(1 + expf((tmpa - log_sum)));
			else log_sum = tmpa + logf(1 + expf((log_sum - tmpa)));
		}

		for (int i = 0; i < labelDim; i++)
		{
			float tmpa = gamma * a[mIdx + i];
			b[mIdx + i] = tmpa - log_sum;
		}
	}
}
void Cuda_LogSoftMax(float * a, float * b, int labelDim, int batchsize, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_LogSoftmax << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(a, b, labelDim, batchsize, gamma);
}


__global__ void cuda_LogSparseSoftmax(int * classIdx, int * wordIdx, int classNum, float * wordAct,
		int batchSize, int wordSize, float * wordLogProb, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < classNum && idy < batchSize)
	{
		int cStart = idx == 0 ? 0 : classIdx[idx - 1];
		int cEnd = classIdx[idx];

		float * pW = wordAct + idy * wordSize;

		float log_sum = gamma * pW[wordIdx[cStart]];
		for (int w = cStart + 1; w < cEnd; w++)
		{
			float tmpa = gamma * pW[wordIdx[w]];
			if (log_sum >= tmpa) log_sum = log_sum + logf(1 + expf((tmpa - log_sum)));
			else log_sum = tmpa + logf(1 + expf((log_sum - tmpa)));
		}

		float *pWProb = wordLogProb + idy * wordSize;
		for (int w = cStart; w < cEnd; w++)
		{
			float tmpa = gamma * pW[wordIdx[w]];
			pWProb[wordIdx[w]] = tmpa - log_sum;
		}
	}
}

void Cuda_LogSparseSoftmax(int * classIdx, int * wordIdx, int classNum, float * wordAct, 
		int batchSize, int wordSize, float * wordLogProb, float gamma)
{
	//int nThreadPerBlock = 32;
	//int nBlockPerGrid = (classNum + 32 - 1) / 32;

	dim3 thread_tail(32, 8);
	dim3 block_tail((classNum + 32 - 1) / 32, (batchSize + 8 - 1) / 8);
	cuda_LogSparseSoftmax << <block_tail, thread_tail >> >(
			classIdx, wordIdx, classNum, wordAct, batchSize, wordSize, wordLogProb, gamma);
}

__global__ void cuda_DerivSparseSoftmax(int * word_candidate_count, float * wordAct, float * wordDeriv, int * wordLabel, float gamma, int batchSize, float * targetProb)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int wStart = idx == 0 ? 0 : word_candidate_count[idx - 1];
		int wEnd = word_candidate_count[idx];
		float log_sum = gamma * wordAct[wStart];
		for (int i = wStart + 1; i < wEnd; i++)
		{
			float tmpa = gamma * wordAct[i];
			if (log_sum >= tmpa) log_sum = log_sum + logf(1 + expf((tmpa - log_sum)));
			else log_sum = tmpa + logf(1 + expf((log_sum - tmpa)));
		}
		int label = wordLabel[idx] + wStart;
		__syncthreads();

		for (int i = wStart; i < wEnd; i++)
		{
			wordDeriv[i] = -gamma * expf(gamma * wordAct[i] - log_sum);
		}
		float p2 = -(wordDeriv[label] / gamma);
		//__syncthreads();
		targetProb[idx] = targetProb[idx] * p2; 
		wordDeriv[label] += gamma;

	}
}
void Cuda_DerivSparseSoftmax(int * word_candidate_count, float * wordAct, float * wordDeriv, int * wordLabel, float gamma, int batchSize, float * targetProb)
{
	cuda_DerivSparseSoftmax << <(batchSize - 1) / 16 + 1, 16 >> >(word_candidate_count, wordAct, wordDeriv, wordLabel, gamma, batchSize, targetProb);
}

