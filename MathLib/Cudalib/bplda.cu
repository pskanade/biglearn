#include "stdafx.h"
#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>
#include "device_functions.h"
#include <fstream>
#include "cublas_v2.h"

#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

using namespace std;

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
using namespace _com_util;
#else
#include <cfloat>
#endif

/********************************************/
//Epsilon used for numerical stability
/********************************************/
__constant__ float epsilon = 1e-8;

/**********************************************/
//Calculate columnwise sum
/**********************************************/
__global__ void cuda_vertical_sum(float *a, float *verticalSum, uint32_t col, uint32_t row)
{
	uint32_t colIndex = blockDim.x * blockIdx.x + threadIdx.x;

	if (colIndex < col)
	{
		float *startVerticalSum = verticalSum + colIndex;

		for (int iRow = 0; iRow < row; iRow++)
		{
			int iA = iRow * col + colIndex;
			(*startVerticalSum) += a[iA];
		}
	}
}

/**********************************************/
//Normalization columnwise
/**********************************************/
__global__ void cuda_normalize(float *a, float *verticalSum, uint32_t col, uint32_t row)
{
	uint32_t colIndex = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t rowIndex = blockDim.y * blockIdx.y + threadIdx.y;

	if (rowIndex < row && colIndex < col)
	{
		int iA = rowIndex * col + colIndex;
		float *startVerticalSum = verticalSum + colIndex;
		a[iA] /= (*startVerticalSum);
	}
}

/*********************************************/
// Normalize by column
/*********************************************/
void cuda_Matrix_Normalize_By_Col(float *a, uint32_t col, uint32_t row)
{
	float *d_verticalSum;
	int colSize = col * sizeof(float);
	cudaMalloc((void**)&d_verticalSum, colSize);
	cudaMemset(d_verticalSum, 0, colSize);

	dim3 thread_tail_col_row(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail_col_row((col + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (row + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	uint32_t nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	uint32_t nBlockPerGrid = (col + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;


	cuda_vertical_sum << <nBlockPerGrid, nThreadPerBlock >> >(a, d_verticalSum, col, row);

	cuda_normalize << <block_tail_col_row, thread_tail_col_row >> >(a, d_verticalSum, col, row);
	cudaFree(d_verticalSum);
}


/***********************************************/
// Rowwise sum. given matrix in ROW MAJOR ORDER
/**********************************************/
__global__ void cuda_rowwise_sum_part_by_part(
		float *matrix,
		float *result,
		int col
		)
{
	uint32_t idx = threadIdx.x;

	__shared__ float sum_value[64];

	sum_value[idx] = 0;
	int rowIndex = blockIdx.x;
	float *float_array = matrix + rowIndex * col;

	sum_value[idx] = 0;
	for (int i = idx; i<col; i += 64)
	{
		sum_value[idx] += float_array[i];
	}
	__syncthreads();

	if (idx == 0)
	{
		float sum = 0;
		float sumIdx = 0;
		for (int i = 0; i < 64; i++)
		{
			sum += sum_value[i];
		}
		result[rowIndex] = sum;
	}
}


/***********************************************/
// Rowwise sum. given matrix in ROW MAJOR ORDER
/**********************************************/
__global__ void cuda_rowwise_sum_part_by_part_additive(
		float *matrix,
		float *result,
		int col,
		float additiveSign
		)
{
	uint32_t idx = threadIdx.x;

	__shared__ float sum_value[64];

	sum_value[idx] = 0;
	int rowIndex = blockIdx.x;
	float *float_array = matrix + rowIndex * col;

	sum_value[idx] = 0;
	for (int i = idx; i<col; i += 64)
	{
		sum_value[idx] += float_array[i];
	}
	__syncthreads();

	if (idx == 0)
	{
		float sum = 0;
		float sumIdx = 0;
		for (int i = 0; i < 64; i++)
		{
			sum += sum_value[i];
		}
		result[rowIndex] += (additiveSign)* sum;
	}
}

/********************************************************/
// Multiply Phi with Theta(Layer - 1)
/********************************************************/
__global__ void cuda_bp_lda_calc_phitheta_loss_pre_1D(
		float *phi,
		float *theta,
		float *b,
		uint32_t IdxLayer,
		uint32_t *matchIdx,
		uint32_t *featureIdx,
		float *featureValue,
		uint32_t batchsize,
		uint32_t elementSize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_Phitheta, // batchSize * elementsize
		float *d_tempSparseMat, //batchSize * elementsize
		float *d_tempSparseMatTemp1, //batchSize * elementsize
		float *d_tempDenseMatTemp, //batchSize * hiddenDim // !!!! inside use d_tempDenseMat !!! do not use d_tempDenseMatTemp
		float *d_loss_pre,
		float *d_loss_post
		)
{
	uint32_t iFeatureIdx = blockDim.x * blockIdx.x + threadIdx.x;

	if (iFeatureIdx < elementSize)
	{
		// Find which batch this feature belongs to
		uint32_t batchId = 0;

		for (; batchId<batchsize; ++batchId)
		{
			// start and end index of this batch;
			uint32_t col_end = matchIdx[batchId];
			uint32_t col_begin = batchId > 0 ? matchIdx[batchId - 1] : 0;

			if (iFeatureIdx >= col_begin && iFeatureIdx < col_end)
			{
				break;
			}
		}

		int prod_batchId_hiddenDim = batchId * hiddenDim;

		float *loss_pre = d_loss_pre + batchId;
		float *loss_post = d_loss_post + batchId;

		// Go to the previous layer by adding ((IdxLayer - 1) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *thetaLayerMinusOne = theta + ((IdxLayer - 1)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim);

		// Calculate Phi Theta 
		// Calculate Phi Theta(d, IdxLayer-1)
		uint32_t inputIdx = featureIdx[iFeatureIdx];

		float tempD_tempSparseMat;

		float sum = 0.0;
		// Go to the row in phi by adding (inputIdx * hiddenDim);
		//phi_iter = phi + (inputIdx * hiddenDim);
		float *phi_iter = phi + (inputIdx * hiddenDim);
		// Determining the end of row for phi
		float *phi_end = phi_iter + hiddenDim;
		// Go to the current layer;
		float *startThetaLayerMinusOne = thetaLayerMinusOne;
		for (; phi_iter < phi_end; ++phi_iter, ++startThetaLayerMinusOne)
		{
			sum += (*phi_iter) * (*startThetaLayerMinusOne);
		}

		d_Phitheta[iFeatureIdx] = sum;

		if (IdxLayer > 1)
		{
			*loss_pre = *loss_post;
		}
		else
		{
			// Calculate the - X^T log (PhiTheta(d, IdxLayer - 1 ))
			tempD_tempSparseMat = logf(d_Phitheta[iFeatureIdx]);
			tempD_tempSparseMat *= featureValue[iFeatureIdx];

			d_tempSparseMatTemp1[iFeatureIdx] = tempD_tempSparseMat;
		}

		// Pre computing X/(Phi Theta) for function cuda_bp_lda_calc_inner_prod_phi_t_x_over_phi_theta_part1
		d_tempSparseMat[iFeatureIdx] = featureValue[iFeatureIdx] / (d_Phitheta[iFeatureIdx] + epsilon);
	}
}

/********************************************************/
// Calculate loss_pre 1st step. Store loss_pre batchwise. because if we do featurewise we have to use atomic. we want to avoid atomic to save precision
/********************************************************/
__global__ void cuda_bp_lda_calc_loss_pre_1D_part1(
		uint32_t * matchIdx,
		uint32_t batchsize,
		float *d_tempSparseMatTemp1, //elementSize
		float *d_loss_pre
		)
{
	uint32_t batchId = blockDim.x * blockIdx.x + threadIdx.x;
	if (batchId < batchsize)
	{
		// start and end index of current batch;
		uint32_t col_end = matchIdx[batchId];
		uint32_t col_begin = batchId > 0 ? matchIdx[batchId - 1] : 0;

		float *loss_pre = d_loss_pre + batchId;

		float sum = 0.0;

		for (uint32_t iFeatureIdx = col_begin; iFeatureIdx < col_end; ++iFeatureIdx)
		{
			sum += d_tempSparseMatTemp1[iFeatureIdx];
		}

		*loss_pre = (-1.0) * sum;
	}
}

/********************************************************/
// Calculate loss_pre 2nd step
/********************************************************/
__global__ void cuda_bp_lda_calc_loss_pre_1D_part2(
		float *theta,
		float *b,
		uint32_t IdxLayer,
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_tempDenseMatTemp //batchSize * hiddenDim 
		)
{
	uint32_t iHid = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t batchId = blockDim.y * blockIdx.y + threadIdx.y;

	if (batchId < batchsize && iHid < hiddenDim)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;

		//// use d_tempDenseMat !!! do not use d_tempDenseMatTemp
		// Go to the hidden index by adding iHid
		float *d_tempDenseMat = d_tempDenseMatTemp + prod_batchId_hiddenDim + iHid;

		// Go to the previous layer by adding ((IdxLayer - 1) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim) and go to the hidden Dim Index by adding iHid;
		float *thetaLayerMinusOne = theta + ((IdxLayer - 1)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim)+iHid;

		*d_tempDenseMat = b[iHid] * logf(*thetaLayerMinusOne);
	}
}


/*********************************************************************************************************************************************************/
// Calculate loss_pre 3rd step. Store loss_pre batchwise. because if we do hiddenDim wise we have to use atomic. we want to avoid atomic to save precision
/*********************************************************************************************************************************************************/
__global__ void cuda_bp_lda_calc_loss_pre_1D_part3(
		uint32_t batchsize,
		uint32_t hiddenDim,
		float *d_tempDenseMatTemp, //batchSize * hiddenDim // !!!! inside use d_tempDenseMat !!! do not use d_tempDenseMatTemp
		float *d_loss_pre
		)
{
	uint32_t batchId = blockDim.x * blockIdx.x + threadIdx.x;
	if (batchId < batchsize)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;
		//// use d_tempDenseMat !!! do not use d_tempDenseMatTemp
		// Go to the hidden index by adding iHid
		float *d_tempDenseMat = d_tempDenseMatTemp + prod_batchId_hiddenDim;

		float *loss_pre = d_loss_pre + batchId;
		float sum = 0.0f;
		for (uint32_t iHid = 0; iHid < hiddenDim; ++iHid)
		{
			sum += d_tempDenseMat[iHid];
		}
		*loss_pre -= sum;
	}
}

/********************************************************/
// Calculate the inner product of Phi^T .  X/(Phi Theta)
/********************************************************/
/******************************************************/
// [Phi^T (X/(Phi Theta(d, layer-1))) + b/(Theta(d, layer-1))]
/******************************************************/
__global__ void cuda_bp_lda_calc_inner_prod_phi_t_x_over_phi_theta_neggrad(
		float *theta,
		float *phi,
		float *b,
		uint32_t IdxLayer,
		uint32_t *matchIdx,
		uint32_t *featureIdx,
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_tempSparseMat,
		float *d_tempDenseMatTemp, //batchSize * hiddenDim 
		float *d_NegGradTemp //batchSize * hiddenDim
		)
{
	uint32_t iHid = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t batchId = blockDim.y * blockIdx.y + threadIdx.y;

	if (batchId < batchsize && iHid < hiddenDim)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;

		uint32_t col_end = matchIdx[batchId];
		uint32_t col_begin = batchId > 0 ? matchIdx[batchId - 1] : 0;

		float *d_tempDenseMat = d_tempDenseMatTemp + prod_batchId_hiddenDim + iHid;
		float *d_NegGrad = d_NegGradTemp + prod_batchId_hiddenDim + iHid;
		float *phi_iter;

		for (uint32_t iFeatureIdx = col_begin; iFeatureIdx < col_end; ++iFeatureIdx)
		{
			uint32_t inputIdx = featureIdx[iFeatureIdx];
			phi_iter = phi + (inputIdx * hiddenDim) + iHid;
			*d_tempDenseMat += (*phi_iter) * d_tempSparseMat[iFeatureIdx];
		}

		// Go to the previous layer by adding ((IdxLayer - 1) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim) and go to the hidden Dim Index by adding iHid;
		float *thetaLayerMinusOne = theta + ((IdxLayer - 1)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim)+iHid;
		float tempVar = b[iHid] / (*thetaLayerMinusOne + epsilon);
		*d_NegGrad = tempVar + (*d_tempDenseMat);

	}
}



/*********************************************************************************************************************************************************/
// Initialize T and loss_post before going to inner loop
// Use this function to init the flag_WhileBreak as well
/*********************************************************************************************************************************************************/
__global__ void cuda_bp_lda_init_t_loss_post(
		float *T,
		float *d_loss_post,
		float *d_loss_pre,
		float *d_TLayerValue, // batchsize
		uint32_t *d_flagWhileBreak, // batchsize
		uint32_t batchsize,
		uint32_t IdxLayer,
		float eta
		)
{
	uint32_t batchId = blockDim.x * blockIdx.x + threadIdx.x;
	if (batchId < batchsize)
	{
		// get the T (d, layer-1) which is a single value and multiply with eta
		float *TLayerMinusOne = T + (IdxLayer - 1) * batchsize + batchId;

		// This value will be used inside the inner for loop for each batch
		float *TLayerValue = d_TLayerValue + batchId;

		// This will be used for while loop break conditions (for line search
		// Initializing all values to one
		uint32_t *flagWhileBreak = d_flagWhileBreak + batchId;
		*flagWhileBreak = 0;

		// Compute T(d,layer) to (1/eta) * T (d, layer-1)
		*TLayerValue = (1 / eta) * (*TLayerMinusOne);

		float *loss_pre = d_loss_pre + batchId;
		float *loss_post = d_loss_post + batchId;

		*loss_post = *loss_pre;
	}
}


/******************************************************/
// Calculate MaxColValue before doing exp
/******************************************************/
__global__ void cuda_bp_lda_calc_maxcolvalue_before_exp(
		float *theta,
		uint32_t IdxLayer,
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_MaxColValue,
		float *d_NegGradTemp, //batchSize * hiddenDim
		float *d_TLayerValue
		)
{
	uint32_t batchId = blockDim.x * blockIdx.x + threadIdx.x;

	if (batchId < batchsize)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;

		// Go to the current layer and hid by adding ((IdxLayer) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayer = theta + ((IdxLayer)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim);

		// TLayerValue for current sample
		float *TLayerValue = d_TLayerValue + batchId;

		// NegGrad for current batch
		float *d_NegGrad = d_NegGradTemp + prod_batchId_hiddenDim;

		// MaxColValue for current sample
		float *maxColValue = d_MaxColValue + batchId;


		float tempVar;

		for (int iHid = 0; iHid < hiddenDim; ++iHid)
		{
			tempVar = d_NegGrad[iHid] * (*TLayerValue);

			(*startThetaLayer) = tempVar;

			if (iHid == 0)
			{
				*maxColValue = tempVar;
			}
			else
			{
				*maxColValue = ((tempVar) > (*maxColValue)) ? (tempVar) : (*maxColValue);
			}
			++startThetaLayer;
		}
	}
}

/******************************************************/
// Calculate SumColValue for normalization
/******************************************************/
__global__ void cuda_bp_lda_calc_sumcolvalue_for_normalization(
		float *theta,
		uint32_t IdxLayer,
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_MaxColValue,
		float *d_SumColValue
		)
{
	uint32_t batchId = blockDim.x * blockIdx.x + threadIdx.x;

	if (batchId < batchsize)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;

		// Go to the current layer and hid by adding ((IdxLayer) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayer = theta + ((IdxLayer)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim);

		// Go to the previous layer and hid by adding ((IdxLayer - 1) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayerMinusOne = theta + ((IdxLayer - 1)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim);

		// MaxColValue for current sample
		float *maxColValue = d_MaxColValue + batchId;

		// SumColValue for current sample
		float *sumColValue = d_SumColValue + batchId;

		float tempVar;

		for (int iHid = 0; iHid < hiddenDim; ++iHid)
		{
			tempVar = (*startThetaLayer) - (*maxColValue);
			tempVar = expf(tempVar);
			tempVar = tempVar * (*startThetaLayerMinusOne);

			*startThetaLayer = tempVar;

			*sumColValue += tempVar;

			++startThetaLayer;
			++startThetaLayerMinusOne;
		}
	}
}


/***********************************************************
//Normalize tempProductHiddenDimSize[Hid].
//Which is C(Theta) in equation and save it in Theta(d, layer)
 ************************************************************/
__global__ void cuda_bp_lda_calc_normalization(
		float *theta,
		uint32_t IdxLayer,
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_SumColValue
		)
{
	uint32_t iHid = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t batchId = blockDim.y * blockIdx.y + threadIdx.y;
	if (batchId < batchsize && iHid < hiddenDim)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;

		// SumColValue for current sample
		float *sumColValue = d_SumColValue + batchId;

		// Go to the current layer by adding ((IdxLayer) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayer = theta + ((IdxLayer)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim)+iHid;

		*startThetaLayer /= (*sumColValue + epsilon);
	}
}

/********************************************************/
// Multiply Phi with Theta(Layer)
/********************************************************/
__global__ void cuda_bp_lda_calc_phitheta_layer(
		float *phi,
		float *theta,
		uint32_t IdxLayer,
		uint32_t *matchIdx,
		uint32_t *featureIdx,
		uint32_t batchsize,
		uint32_t elementSize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		uint32_t *d_flagWhileBreak,
		float *d_Phitheta // batchSize * elementsize
		)
{
	uint32_t iFeatureIdx = blockDim.x * blockIdx.x + threadIdx.x;

	if (iFeatureIdx < elementSize)
	{
		// Find which batch this feature belongs to
		uint32_t batchId = 0;

		for (; batchId < batchsize; ++batchId)
		{
			// start and end index of this batch;
			uint32_t col_end = matchIdx[batchId];
			uint32_t col_begin = batchId > 0 ? matchIdx[batchId - 1] : 0;

			if (iFeatureIdx >= col_begin && iFeatureIdx < col_end)
			{
				break;
			}
		}

		uint32_t *flagWhileBreak = d_flagWhileBreak + batchId;

		if (*flagWhileBreak == 1)
		{
			return;
		}

		int prod_batchId_hiddenDim = batchId * hiddenDim;

		// Go to the current layer by adding ((IdxLayer - 1) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayer = theta + ((IdxLayer)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim);

		// Calculate Phi Theta 
		// Calculate Phi Theta(d, IdxLayer-1)
		uint32_t inputIdx = featureIdx[iFeatureIdx];

		float tempD_tempSparseMat;

		float sum = 0.0;
		// Go to the row in phi by adding (inputIdx * hiddenDim);
		//phi_iter = phi + (inputIdx * hiddenDim);
		float *phi_iter = phi + (inputIdx * hiddenDim);
		// Determining the end of row for phi
		float *phi_end = phi_iter + hiddenDim;

		for (; phi_iter < phi_end; ++phi_iter, ++startThetaLayer)
		{
			sum += (*phi_iter) * (*startThetaLayer);
		}

		d_Phitheta[iFeatureIdx] = sum;
	}
}


/********************************************************/
// Calculate loss_post 
/********************************************************/
__global__ void cuda_bp_lda_calc_loss_post(
		uint32_t *matchIdx,
		float *featureValue,
		uint32_t batchsize,
		float *d_loss_post,
		float *d_Phitheta
		)
{
	uint32_t batchId = blockDim.x * blockIdx.x + threadIdx.x;
	if (batchId < batchsize)
	{
		// start and end index of current batch;
		uint32_t col_end = matchIdx[batchId];
		uint32_t col_begin = batchId > 0 ? matchIdx[batchId - 1] : 0;

		float *loss_post = d_loss_post + batchId;
		float sum = 0.0;

		for (uint32_t iFeatureIdx = col_begin; iFeatureIdx < col_end; ++iFeatureIdx)
		{
			sum += featureValue[iFeatureIdx] * logf(d_Phitheta[iFeatureIdx]);
		}

		*loss_post = (-1.0) * sum;
	}
}

/**********************************************/
//Theta(d, layer) - Theta(d, layer - 1)
/**********************************************/
__global__ void cuda_bp_lda_calc_theta_theta_layerminusone(
		float *theta,
		uint32_t IdxLayer,
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_tempDenseMatTemp
		)
{
	uint32_t iHid = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t batchId = blockDim.y * blockIdx.y + threadIdx.y;
	if (batchId < batchsize && iHid < hiddenDim)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;

		// Go to the current layer and hid by adding ((IdxLayer) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayer = theta + ((IdxLayer)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim)+iHid;

		// Go to the previous layer and hid by adding ((IdxLayer - 1) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayerMinusOne = theta + ((IdxLayer - 1)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim)+iHid;

		float *d_tempDenseMat = d_tempDenseMatTemp + prod_batchId_hiddenDim + iHid;

		*d_tempDenseMat = (*startThetaLayer) - (*startThetaLayerMinusOne);
	}
}


/******************************************************/
// pre Calculate //b^T log (Theta(d, IdxLayer))	  b[iHid] * logf(*startThetaLayer);
// pre calculate //Deriv(FTheta(d, Layer-1) ^ T (Theta(d, layer) - Theta(d, layer-1))   sumGradProj += (*d_NegGrad) * (tempVar);
/******************************************************/
__global__ void cuda_bp_lda_calc_b_logtheta_deriv_ftheta(
		float *theta,
		float *b,
		uint32_t IdxLayer,
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_tempDenseMatTemp,
		float *d_NegGradTemp,
		float *d_sum,
		float *d_sumGradProj
		)
{
	uint32_t iHid = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t batchId = blockDim.y * blockIdx.y + threadIdx.y;
	if (batchId < batchsize && iHid < hiddenDim)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;

		// Go to the current layer and hid by adding ((IdxLayer) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayer = theta + ((IdxLayer)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim)+iHid;

		float *d_tempDenseMat = d_tempDenseMatTemp + prod_batchId_hiddenDim + iHid;

		float *d_NegGrad = d_NegGradTemp + prod_batchId_hiddenDim + iHid;

		float *sum = d_sum + prod_batchId_hiddenDim + iHid;

		float *sumGradProj = d_sumGradProj + prod_batchId_hiddenDim + iHid;

		*sum = b[iHid] * logf(*startThetaLayer);

		*sumGradProj = (*d_NegGrad) * (*d_tempDenseMat);
	}
}


/******************************************************/
// Calculate SumColValue for normalization
/******************************************************/
__global__ void cuda_bp_lda_calc_gradproj_update_loss_post(
		float *theta,
		float *b,
		float *T,
		float eta,
		uint32_t IdxLayer,
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_TLayerValue,
		float *d_loss_post,
		float *d_loss_pre,
		float *d_tempDenseMatTemp,
		float *d_NegGradTemp,
		float *d_sum,
		float *d_sumGradProj,
		uint32_t *d_flagWhileBreak,
		uint32_t *d_whileBreakCount
		)
{
	uint32_t batchId = blockDim.x * blockIdx.x + threadIdx.x;

	if (batchId < batchsize)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;

		// Go to the current layer and hid by adding ((IdxLayer) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayer = theta + ((IdxLayer)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim);

		float *loss_post = d_loss_post + batchId;

		float *loss_pre = d_loss_pre + batchId;

		uint32_t *flagWhileBreak = d_flagWhileBreak + batchId;

		float *d_tempDenseMat = d_tempDenseMatTemp + prod_batchId_hiddenDim;

		float *d_NegGrad = d_NegGradTemp + prod_batchId_hiddenDim;

		float *sumPerHid = d_sum + prod_batchId_hiddenDim;

		float *sumGradProjPerHid = d_sumGradProj + prod_batchId_hiddenDim;

		float sum = 0.0;
		// temporary sum for calculating gradProj
		float sumGradProj = 0.0;
		// This variable holds part of the right side of the break condition for the while loop to calculate l1 norm
		float tempSubtractionSquare = 0.0;
		for (int iHid = 0; iHid < hiddenDim; ++iHid)
		{
			//b^T log (Theta(d, IdxLayer))								
			sum += sumPerHid[iHid];

			float tempVar = *d_tempDenseMat;
			// Caclulate ||Theta(d, layer) - Theta(d, layer-1)||^2, Doing the first step: Calculate the sum of absolute value of elements
			tempSubtractionSquare += abs(tempVar);

			//Deriv(FTheta(d, Layer-1) ^ T (Theta(d, layer) - Theta(d, layer-1)) 
			sumGradProj += sumGradProjPerHid[iHid];

			++startThetaLayer;
			++d_tempDenseMat;
			++d_NegGrad;
		}

		*loss_post -= sum;

		// Calculating loss difference
		float loss_gap = *loss_post - *loss_pre;

		//Gradproj calculation
		float gradproj = (-1.0) * sumGradProj;

		// Caclulate ||Theta(d, layer) - Theta(d, layer-1)||^2, Doing the second step: Calculate the square 
		tempSubtractionSquare = powf(tempSubtractionSquare, 2.0);


		// This value will be used inside the inner for loop for each batch
		float *TLayerValue = d_TLayerValue + batchId;

		// Adding terms together:  Deriv(FTheta(d, Layer-1) ^ T (Theta(d, layer) - Theta(d, layer-1))     +     (1 / 2T(L)) * ||Theta(d, layer) - Theta(d, layer-1)||^2
		float loss_gap_thresh = gradproj + tempSubtractionSquare / ((*TLayerValue) * 2.0);


		if (loss_gap > loss_gap_thresh + 1e-12)
		{
			// Calculate new value of T(d,layer): which is (eta) * T (d, layer)
			(*TLayerValue) = eta * (*TLayerValue);
		}
		else
		{
			// Store T(d,layer)
			float *TLayer = T + IdxLayer * batchsize + batchId;
			*TLayer = *TLayerValue;



			// If the break condition occurs for a sample, keep track and break the while in C# code
			if (*flagWhileBreak == 0)
			{
				//printf("\n while break for batch %d IdxLayer %d *Tlayer %.10f\n", batchId, IdxLayer, *TLayer);

				*flagWhileBreak = 1;
				atomicAdd(d_whileBreakCount, 1);
			}
		}
	}
}


/******************************************************/
// Calculate output, reading theta from the last layer
/******************************************************/
__global__ void cuda_bp_lda_calc_output(
		float *theta,
		float *U,
		float *iterT,
		float *matchLabelValue,
		float gamma,
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t outputDim,
		uint32_t lastLayer,
		uint32_t prod_hiddenDim_batchsize,
		uint32_t calculateSoftMax,
		float *output,
		float *d_Loss_Per_Epoch
		)
{
	uint32_t batchId = blockDim.x * blockIdx.x + threadIdx.x;

	if (batchId < batchsize)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;

		// For regression declare output variable 
		float YHat = 0.0;

		// Set the iterT
		iterT[batchId] = lastLayer;

		// Go to the last layer and hid by adding ((layerDim) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayer = theta + ((lastLayer)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim);

		for (int iHid = 0; iHid < hiddenDim; ++iHid)
		{
			// Inner product of U and Theta (d, endLayer)
			YHat += U[iHid] * (*startThetaLayer);
			++startThetaLayer;
		}

		// Save YHat in output for the sample
		if (calculateSoftMax == 1)
		{
			float t = gamma * YHat;
			output[batchId] = (tanhf(t / 2) + 1) / 2; // 1 / (1 + expf((-1.0) * gamma * YHat));
		}
		else
		{
			output[batchId] = YHat;
		}

		/***************************************************************/
		// Current we support only regression where outputDim = 1
		// But following code is prepared to support future case where outputDim > 1 (for multiclass classification). 
		// Later change the above code to support outputDim > 1
		/***************************************************************/
		for (int iOutputDim = 0; iOutputDim < outputDim; ++iOutputDim)
		{
			// Think output array contains batches as rows and outputDim as columns
			// Find the output index to compute yd - yHat
			int indexOutput = batchId * outputDim + iOutputDim;
			// Do yd - yHat
			float tempError = output[indexOutput] - matchLabelValue[indexOutput];
			// Save the loss ( MSE) by Atomic add in a common variable which will be used in result
			atomicAdd(d_Loss_Per_Epoch, tempError * tempError);
		}

	}
}

void cuda_Bp_Lda_Forward(
		float *phi,
		float *theta,
		float *b,
		float *U,
		float *T,
		float *d_TLayerValue,
		float *d_MaxColValue,
		float *d_SumColValue,
		float *iterT,
		float *output,
		float *matchLabelValue,
		float *d_Loss_Per_Epoch,
		uint32_t *matchIdx,
		uint32_t *featureIdx,
		float *featureValue,
		uint32_t elementSize,
		float eta,
		uint32_t useAdaptivenHidLayer,
		uint32_t batchsize,
		uint32_t inputDim,
		uint32_t hiddenDim,
		uint32_t outputDim,
		uint32_t layerDim,
		float *d_Phitheta, // batchSize * elementsize
		float *d_tempSparseMat, //batchSize * elementsize
		float *d_tempSparseMatTemp1, //batchSize * elementsize
		float *d_tempDenseMatTemp, //batchSize * hiddenDim // !!!! inside use d_tempDenseMat !!! do not use d_tempDenseMatTemp
		float *d_NegGradTemp,//batchSize * hiddenDim
		float *d_LogThetaTemp,//batchSize * hiddenDim
		float *d_loss_pre,
		float *d_loss_post,
		float *d_sum,
		float *d_sumGradProj,
		uint32_t *d_flagWhileBreak, // batchsize
		uint32_t *d_whileBreakCount, // 1
		uint32_t *whileBreakCount,
		float gamma,
		uint32_t calculateSoftMax,
		uint32_t MaxLineSearchIter
		)
{
	/**********************Precompute constants*************/
	/******************************************************/
	int prod_hiddenDim_batchsize = hiddenDim * batchsize;
	/**********************Precompute constants*************/
	/******************************************************/

	/**********RESET BLOCK**************/
	/***********************************/
	cudaMemset(d_loss_pre, 0, batchsize * sizeof(float));
	cudaMemset(d_loss_post, 0, batchsize * sizeof(float));
	/**********RESET BLOCK**************/
	/***********************************/

	uint32_t nThreadPerBlock_batchsize_1D = DEFAULT_THREAD_PER_BLOCK;
	uint32_t nBlockPerGrid_batchsize_1D = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail_hidden_batch_2D(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail_hidden_batch_2D((hiddenDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	uint32_t IdxLayer = 1; // until layerDim
	for (; IdxLayer <= layerDim; ++IdxLayer)
	{
		uint32_t nThreadPerBlock_elementSize_1D = DEFAULT_THREAD_PER_BLOCK;
		uint32_t nBlockPerGrid_elementSize_1D = (elementSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

		cuda_bp_lda_calc_phitheta_loss_pre_1D << < nBlockPerGrid_elementSize_1D, nThreadPerBlock_elementSize_1D >> >(
				phi,
				theta,
				b,
				IdxLayer, // Which layer of theta to multiply with
				matchIdx,
				featureIdx,
				featureValue,
				batchsize,
				elementSize,
				hiddenDim,
				prod_hiddenDim_batchsize,
				d_Phitheta, // batchSize * elementsize
				d_tempSparseMat, //batchSize * elementsize
				d_tempSparseMatTemp1,
				d_tempDenseMatTemp, //batchSize * hiddenDim
				d_loss_pre,
				d_loss_post
				);

		if (IdxLayer <= 1)
		{

			cuda_bp_lda_calc_loss_pre_1D_part1 << <nBlockPerGrid_batchsize_1D, nThreadPerBlock_batchsize_1D >> >(
					matchIdx,
					batchsize,
					d_tempSparseMatTemp1, // elementSize
					d_loss_pre
					);

			cuda_bp_lda_calc_loss_pre_1D_part2 << <block_tail_hidden_batch_2D, thread_tail_hidden_batch_2D >> >(
					theta,
					b,
					IdxLayer,
					batchsize,
					hiddenDim,
					prod_hiddenDim_batchsize,
					d_tempDenseMatTemp //batchSize * hiddenDim 
					);

			// Calculate tempDenseMat sum to be used for xi calculation
			cuda_rowwise_sum_part_by_part_additive << < batchsize, 64 >> >(
					d_tempDenseMatTemp,
					d_loss_pre,
					hiddenDim,
					-1.0f
					);
		}

		// Reset the tempDenseMat
		cudaMemset(d_tempDenseMatTemp, 0, prod_hiddenDim_batchsize * sizeof(float));

		cuda_bp_lda_calc_inner_prod_phi_t_x_over_phi_theta_neggrad << <block_tail_hidden_batch_2D, thread_tail_hidden_batch_2D >> >(
				theta,
				phi,
				b,
				IdxLayer,
				matchIdx,
				featureIdx,
				batchsize,
				hiddenDim,
				prod_hiddenDim_batchsize,
				d_tempSparseMat,
				d_tempDenseMatTemp, //batchSize * hiddenDim 
				d_NegGradTemp //batchSize * hiddenDim
				);

		// Use this function to init the flag_WhileBreak as well
		cuda_bp_lda_init_t_loss_post << <nBlockPerGrid_batchsize_1D, nThreadPerBlock_batchsize_1D >> >(
				T,
				d_loss_post,
				d_loss_pre,
				d_TLayerValue, // batchsize
				d_flagWhileBreak,
				batchsize,
				IdxLayer,
				eta
				);

		// reset while loop break count
		cudaMemset(d_whileBreakCount, 0, 1 * sizeof(uint32_t));

		// Will let the while run until all the batches meet break condition
		while (true)
		{
			cudaMemcpy(whileBreakCount, d_whileBreakCount, 1 * sizeof(uint32_t), cudaMemcpyDeviceToHost);

			if (*whileBreakCount >= batchsize)
			{
				break;
			}

			// Reset the SumColValue, Max value reset is not required as it will be assigned to new value
			//cudaMemset(d_MaxColValue, 0, batchsize * sizeof(float));
			cudaMemset(d_SumColValue, 0, batchsize * sizeof(float));

			cuda_bp_lda_calc_maxcolvalue_before_exp << <nBlockPerGrid_batchsize_1D, nThreadPerBlock_batchsize_1D >> >(
					theta,
					IdxLayer,
					batchsize,
					hiddenDim,
					prod_hiddenDim_batchsize,
					d_MaxColValue,
					d_NegGradTemp, //batchSize * hiddenDim
					d_TLayerValue
					);

			cuda_bp_lda_calc_sumcolvalue_for_normalization << <nBlockPerGrid_batchsize_1D, nThreadPerBlock_batchsize_1D >> >(
					theta,
					IdxLayer,
					batchsize,
					hiddenDim,
					prod_hiddenDim_batchsize,
					d_MaxColValue,
					d_SumColValue
					);

			cuda_bp_lda_calc_normalization << <block_tail_hidden_batch_2D, thread_tail_hidden_batch_2D >> >(
					theta,
					IdxLayer,
					batchsize,
					hiddenDim,
					prod_hiddenDim_batchsize,
					d_SumColValue
					);

			cuda_bp_lda_calc_phitheta_layer << < nBlockPerGrid_elementSize_1D, nThreadPerBlock_elementSize_1D >> >(
					phi,
					theta,
					IdxLayer,
					matchIdx,
					featureIdx,
					batchsize,
					elementSize,
					hiddenDim,
					prod_hiddenDim_batchsize,
					d_flagWhileBreak,
					d_Phitheta // batchSize * elementsize
					);

			cuda_bp_lda_calc_loss_post << <nBlockPerGrid_batchsize_1D, nThreadPerBlock_batchsize_1D >> >(
					matchIdx,
					featureValue,
					batchsize,
					d_loss_post,
					d_Phitheta
					);

			cuda_bp_lda_calc_theta_theta_layerminusone << <block_tail_hidden_batch_2D, thread_tail_hidden_batch_2D >> >(
					theta,
					IdxLayer,
					batchsize,
					hiddenDim,
					prod_hiddenDim_batchsize,
					d_tempDenseMatTemp
					);

			cuda_bp_lda_calc_b_logtheta_deriv_ftheta << <block_tail_hidden_batch_2D, thread_tail_hidden_batch_2D >> >(
					theta,
					b,
					IdxLayer,
					batchsize,
					hiddenDim,
					prod_hiddenDim_batchsize,
					d_tempDenseMatTemp,
					d_NegGradTemp,
					d_sum,
					d_sumGradProj
					);

			cuda_bp_lda_calc_gradproj_update_loss_post << <nBlockPerGrid_batchsize_1D, nThreadPerBlock_batchsize_1D >> >(
					theta,
					b,
					T,
					eta,
					IdxLayer,
					batchsize,
					hiddenDim,
					prod_hiddenDim_batchsize,
					d_TLayerValue,
					d_loss_post,
					d_loss_pre,
					d_tempDenseMatTemp,
					d_NegGradTemp,
					d_sum,
					d_sumGradProj,
					d_flagWhileBreak,
					d_whileBreakCount
					);
		}
	}

	cuda_bp_lda_calc_output << <nBlockPerGrid_batchsize_1D, nThreadPerBlock_batchsize_1D >> >(
			theta,
			U,
			iterT,
			matchLabelValue,
			gamma,
			batchsize,
			hiddenDim,
			outputDim,
			IdxLayer - 1, // Super important!! this is the last layer , as the forloop has exited 
			prod_hiddenDim_batchsize,
			calculateSoftMax,
			output,
			d_Loss_Per_Epoch
			);

}




__global__ void cuda_bp_lda_init_grad_q_u(
		float *grad_Q_U,
		uint32_t hiddenDim,
		uint32_t outputDim
		)
{
	uint32_t hiddenIndex = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t outputIndex = blockDim.y * blockIdx.y + threadIdx.y;
	if (hiddenIndex < hiddenDim && outputIndex < outputDim)
	{
		float *startGrad_Q_U = grad_Q_U + outputIndex * hiddenDim + hiddenIndex;
		*startGrad_Q_U = 0.0;
	}
}

__global__ void cuda_bp_lda_init_grad_q_phi(
		float *grad_Q_Phi,
		float *phi,
		float value,
		uint32_t hiddenDim,
		uint32_t inputDim
		)
{
	uint32_t hiddenIndex = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t inputIndex = blockDim.y * blockIdx.y + threadIdx.y;
	if (hiddenIndex < hiddenDim && inputIndex < inputDim)
	{
		float *startGrad_Q_Phi = grad_Q_Phi + inputIndex * hiddenDim + hiddenIndex;
		float *startPhi = phi + inputIndex * hiddenDim + hiddenIndex;

		*startGrad_Q_Phi = value / (*startPhi + epsilon);
	}
}



/************************************************************************/
//Update grad_Q_U; grad_Q_U = -(1/Gamma).2.(yd - yHat) Theta(d, layer-1)T or -(Gamma).(yd - yHat) Theta(d, layer-1)T 
//Initializing Epsilon(Lastlayer, d)
/************************************************************************/
__global__ void cuda_bp_lda_calc_grad_q_u_init_xi_part1(
		float *output,
		float *matchLabelValue,
		float *theta,
		float *U,
		float *iterT,
		float gamma,
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t outputDim,
		uint32_t layerDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_xi,
		float *d_tempDenseMat, //batchsize * hiddenDim
		float *d_sumGrad_Q_U, // !!!COLUMN MAJOR FORMAT SO WE CAN USE IT IN CUBLAS!!!  outputdim * hiddenDim * batchsize
		float *d_sumTempDenseMat, // !!!COLUMN MAJOR FORMAT SO WE CAN USE IT IN CUBLAS!!!  * hiddenDim * batchsize
		uint32_t calculateSoftMax
		)
{
	uint32_t iHid = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t batchId = blockDim.y * blockIdx.y + threadIdx.y;

	if (batchId < batchsize && iHid < hiddenDim)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;
		int prod_iHid_batchSize = iHid * batchsize;

		// Find last layer for the current Sample
		uint32_t LastLayer = iterT[batchId];
		// Go to the last layer and hid by adding ((layerDim) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		// Change the 1 to layerDim once the algorithm is finalized!
		float *startThetaLayer = theta + ((LastLayer)* prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim)+iHid;

		// !!!SPECIAL!!!! 
		// Take batchSize along x axis
		// HiddenDim along y axis
		// OutputDim along layers
		float *sumGrad_Q_U = d_sumGrad_Q_U + prod_iHid_batchSize + batchId;

		float *sumTempDenseMat = d_sumTempDenseMat + prod_batchId_hiddenDim + iHid;

		float *xi = d_xi + prod_batchId_hiddenDim + iHid;

		float *tempDenseMat = d_tempDenseMat + prod_batchId_hiddenDim + iHid;


		float tempVar;
		float tempVar1;

		for (int iOutputDim = 0; iOutputDim < outputDim; ++iOutputDim)
		{
			// Think output array contains batches as rows and outputDim as columns
			// Find the output index to compute yd - yHat
			int indexOutput = batchId * outputDim + iOutputDim;
			// Do yd - yHat
			tempVar = output[indexOutput] - matchLabelValue[indexOutput];

			if (calculateSoftMax == 1)
			{
				// Gamma . (yd - yHat)
				tempVar *= gamma;
			}
			else
			{
				// (1/Gamma) .2. (yd - yHat)
				tempVar *= (2.0f / gamma);
			}

			/**************************************************************/
			//Calculating Grad_Q_U
			/**************************************************************/
			// Calculate (1/Gamma) .2. (yd - yHat) Theta(d, LastLayer)^T 
			// and multiply (1/ batch) to start calculating grad_Q_U
			float tempProd = (tempVar * (*startThetaLayer)) / (batchsize); // IDEA: can do this division later in summation to check speed difference

			// Find the address of grad_Q_U where we start storing current tempProd
			sumGrad_Q_U = sumGrad_Q_U + (iOutputDim * prod_hiddenDim_batchsize);

			// Storing the values which will be added in a separate function
			// This will give us (1/batch)SUM (d)[grad] for all batches 
			*sumGrad_Q_U = tempProd;



			/***************************************************************/
			//Initializing Epsilon(Lastlayer, d)
			/***************************************************************/
			//This part will be used for 
			// Depending on regression or classification: Multiply U^T with (1/Gamma).2.(yd - yHat) Theta(d, layer-1)T  or (Gamma).(yd - yHat) Theta(d, layer-1)T 
			//Implementing the following C# code 
			//MatrixOperation.MatrixTransposeMultiplyVector(
			//	xi.DenseMatrixValue[IdxSample],
			//	paramModel.U,
			//	grad_Q_po.DenseMatrixValue[IdxSample]
			//	);
			// Here we use xi to  represent U^T . (1/Gamma).2.(yd - yHat) Theta(d, layer-1)T or U^T . (Gamma).(yd - yHat) Theta(d, layer-1)T
			*xi = U[iOutputDim * outputDim + iHid] * tempVar;

			//Theta(d, LastLayer)^T . xi (from above)
			//Implementing below C# code
			//MatrixOperation.ElementwiseVectorMultiplyVector(
			//	TmpDenseMat.DenseMatrixValue[IdxSample],
			//	DNNRun.theta_pool[DNNRun.nHidLayerEffective[IdxSample] - 1].DenseMatrixValue[IdxSample],
			//	xi.DenseMatrixValue[IdxSample]
			//	);
			//TmpDenseRowVec.VectorValue[IdxSample] = TmpDenseMat.DenseMatrixValue[IdxSample].Sum();
			tempVar1 = (*startThetaLayer) * (*xi);
			*tempDenseMat = tempVar1;
			*sumTempDenseMat = tempVar1;
		}
	}
}


/**************************************************/
//Init xi
/**************************************************/
__global__ void cuda_bp_lda_calc_grad_q_u_init_xi_part2(
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_xi,
		float *d_tempResBatch
		)
{
	uint32_t iHid = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t batchId = blockDim.y * blockIdx.y + threadIdx.y;

	if (batchId < batchsize && iHid < hiddenDim)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;

		float *xi = d_xi + prod_batchId_hiddenDim + iHid;
		// holds the result from previous step
		float *tempResBatch = d_tempResBatch + batchId;

		*xi -= *tempResBatch;
	}
}

/******************************************************/
// Calculate temp_theta_xi_b_T_OVER_theta_lm1_2
/******************************************************/
__global__ void cuda_bp_lda_calc_temp_theta_xi_b_T_OVER_theta_lm1_2(
		uint32_t IdxLayer,
		float *theta,
		float *T,
		float *b,
		float *d_xi, // batchsize * hiddenDim
		float *d_temp_theta_xi, // batchsize * hiddenDim
		float *d_temp_theta_xi_b_T_OVER_theta_lm1_2,
		float *U,
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize
		)
{
	uint32_t iHid = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t batchId = blockDim.y * blockIdx.y + threadIdx.y;

	if (batchId < batchsize && iHid < hiddenDim)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;

		// Go to the previous layer and hid by adding ((IdxLayer - 1) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayerMinusOne = theta + ((IdxLayer - 1) * prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim)+iHid;

		// Go to the current layer and hid by adding ((IdxLayer) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayer = startThetaLayerMinusOne + prod_hiddenDim_batchsize;

		// Go to the current layer T (d, layer)
		float *TLayer = T + IdxLayer * batchsize + batchId;

		float *xi = d_xi + prod_batchId_hiddenDim + iHid;

		float *temp_theta_xi = d_temp_theta_xi + prod_batchId_hiddenDim + iHid;

		float *temp_theta_xi_b_T_OVER_theta_lm1_2 = d_temp_theta_xi_b_T_OVER_theta_lm1_2 + prod_batchId_hiddenDim + iHid;

		float *startB = b + iHid;

		// Calculate Theta(d, layer) * Epsilon (d, layer)
		float tempVar = (*startThetaLayer) * (*xi);
		*temp_theta_xi = tempVar;

		// Calculate (Theta(d, layer) * Epsilon (d, layer) ) / (Theta (d, layer - 1) ^ 2)
		// Dividing twice with Theta (d, layer - 1) 				
		float tempVar1 = tempVar / (*startThetaLayerMinusOne + epsilon);
		tempVar1 /= (*startThetaLayerMinusOne + epsilon);

		//Calculate b * [  (Theta(d, layer) * Epsilon (d, layer) ) / (Theta (d, layer - 1) ^ 2)   ]
		tempVar1 *= *startB;

		//Calculate T(d, layer)   *  b * [  (Theta(d, layer) * Epsilon (d, layer) ) / (Theta (d, layer - 1) ^ 2)   ]
		tempVar1 *= (*TLayer);

		// Reset the elements to zero if theta_{l-1} is zero at these positions (mainly for alpha<1 case)
		//if (IdxLayer > 0)
		//{
		//	MatrixOperation.ResetVectorSparsePattern(
		//		tmp_theta_xi_b_T_OVER_theta_lm1_2.DenseMatrixValue[IdxSample],
		//		DNNRun.theta_pool[IdxLayer - 1].DenseMatrixValue[IdxSample]
		//		);
		//}
		if (abs(*startThetaLayerMinusOne) < 1e-30)
		{
			tempVar1 = 0.0;
		}

		*temp_theta_xi_b_T_OVER_theta_lm1_2 = tempVar1;
	}
}

__global__ void cuda_bp_lda_calc_x_over_phitheta_grad_q_phi_part0(
		float *phi,
		float *theta,
		float *T,
		uint32_t IdxLayer,
		uint32_t *matchIdx,
		uint32_t *featureIdx,
		float *featureValue,
		uint32_t batchsize,
		uint32_t elementSize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_temp_Xt_OVER_Phitheta, // elementSize
		float *d_tempSparseMatValue, // elementSize
		float *d_tempDenseMat, //batchSize * hiddenDim 
		float *d_tempSparseMat, // elementSize
		float *d_temp_theta_xi // batchsize * hiddenDim
		)
{
	uint32_t iFeatureIdx = blockDim.x * blockIdx.x + threadIdx.x;

	if (iFeatureIdx < elementSize)
	{

		// Find which batch this feature belongs to
		uint32_t batchId = 0;

		for (; batchId<batchsize; ++batchId)
		{
			// start and end index of this batch;
			uint32_t col_end = matchIdx[batchId];
			uint32_t col_begin = batchId > 0 ? matchIdx[batchId - 1] : 0;

			if (iFeatureIdx >= col_begin && iFeatureIdx < col_end)
			{
				break;
			}
		}

		float tempProd;
		float temp_Phi_theta_xi = 0.0;

		int prod_batchId_hiddenDim = batchId * hiddenDim;

		float *temp_Xt_OVER_Phitheta = d_temp_Xt_OVER_Phitheta + iFeatureIdx;
		float *tempSparseMatValue = d_tempSparseMatValue + iFeatureIdx;

		float *tempDenseMat = d_tempDenseMat + prod_batchId_hiddenDim;

		// Go to the previous layer by adding ((IdxLayer - 1) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *thetaLayerMinusOne = theta + ((IdxLayer - 1) * prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim);

		// Int temp_theta_xi
		float *temp_theta_xi = d_temp_theta_xi + prod_batchId_hiddenDim;

		// Calculate Phi Theta 
		// Calculate Phi Theta(d, IdxLayer-1)
		uint32_t inputIdx = featureIdx[iFeatureIdx];
		float *startPhi_iter = phi + (inputIdx * hiddenDim);

		/*****************************************************************/
		// Calculate XtOver Theta^2  and 
		// Calculate Phi. Theta(d, layer) * Epsilon (d, layer)
		/*****************************************************************/
		float sum = 0.0;
		// Go to the row in phi by adding (inputIdx * hiddenDim);
		//phi_iter = phi + (inputIdx * hiddenDim);
		float *phi_iter = startPhi_iter;
		// Determining the end of row for phi
		float *phi_end = phi_iter + hiddenDim;
		// Go to the current layer;
		float *startThetaLayerMinusOne = thetaLayerMinusOne;
		float *startTemp_theta_xi = temp_theta_xi;
		for (; phi_iter < phi_end; ++phi_iter, ++startThetaLayerMinusOne, ++startTemp_theta_xi)
		{
			sum += (*phi_iter) * (*startThetaLayerMinusOne);

			//Calculate Phi.Theta(d, layer) * Epsilon(d, layer)
			temp_Phi_theta_xi += (*phi_iter) * (*startTemp_theta_xi);
		}

		// Xt / ( Phi * Theta(d, layer - 1) )
		tempProd = featureValue[iFeatureIdx] / (sum + epsilon);
		*temp_Xt_OVER_Phitheta = tempProd;
		// Xt / ( Phi * Theta(d, layer - 1) ) ^ 2
		*tempSparseMatValue = tempProd / (sum + epsilon);


		/*****************************************************************/
		//Calculate Phi. Theta(d, layer) * Epsilon (d, layer)
		/*****************************************************************/
		// Already calculated inthe previous loop

		// Multiplication inside the previous loop might be costly. So doing separately
		// tempSparseMat = ( temp_Phi_theta_xi . temp_Xt_OVER_Phitheta2 )
		//MatrixOperation.ElementwiseVectorMultiplyVector(
		//	TmpSparseMat.SparseColumnVectors[IdxSample],
		//	tmp_Phi_theta_xi.SparseColumnVectors[IdxSample]
		//	); // TmpSparseMat is ( tmp_Phi_theta_xi.*tmp_Xt_OVER_Phitheta2 )
		*tempSparseMatValue *= temp_Phi_theta_xi;

		// Go to the current layer T (d, layer)
		float *TLayer = T + (IdxLayer)* batchsize + batchId;

		///****************************************************************/
		////Preparing temporary variable to be used to Calculate Epsilon (d, layer - 1)
		///****************************************************************/
		//Do this in separate function cuda_bp_lda_calc_temp_dense_mat as we dont want to use atomic add here.
		// Initialize phi start point
		phi_iter = phi + (inputIdx * hiddenDim);
		float *startTempDenseMat = tempDenseMat;
		for (int iHid = 0; iHid < hiddenDim; ++iHid, ++startTempDenseMat)
		{
			atomicAdd(startTempDenseMat, (*(phi_iter + iHid)) * (*tempSparseMatValue) *(*TLayer));
		}

		/***************************************************************/
		//TLayer * temp_Xt_OVER_Phitheta
		/***************************************************************/
		*temp_Xt_OVER_Phitheta *= (*TLayer);


		/***************************************************************/
		//tempSparseMat = TLayer * ( temp_Phi_theta_xi . temp_Xt_OVER_Phitheta2 )
		/***************************************************************/
		*tempSparseMatValue *= (*TLayer);
		*tempSparseMatValue *= -1.0;
	}
}



/********************************************************/
// Calculate the division X/(Phi Theta(d, layer-1)) and tempsum for grad_Q_Phi
/********************************************************/
__global__ void cuda_bp_lda_calc_x_over_phitheta_grad_q_phi_part1(
		float *theta,
		float *T,
		uint32_t IdxLayer,
		uint32_t *matchIdx,
		uint32_t *featureIdx,
		float *featureValue,
		uint32_t batchsize,
		uint32_t elementSize,
		uint32_t hiddenDim,
		uint32_t inputDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_temp_Xt_OVER_Phitheta, // elementSize
		float *d_tempSparseMatValue, // elementSize
		float *d_tempDenseMat, //batchSize * hiddenDim 
		float *d_tempSparseMat, // elementSize
		float *d_temp_theta_xi, // batchsize * hiddenDim
		float *grad_Q_Phi
		)
{
	uint32_t iFeatureIdx = blockDim.x * blockIdx.x + threadIdx.x;

	if (iFeatureIdx < elementSize)
	{
		// Find which batch this feature belongs to
		uint32_t batchId = 0;

		for (; batchId<batchsize; ++batchId)
		{
			// start and end index of this batch;
			uint32_t col_end = matchIdx[batchId];
			uint32_t col_begin = batchId > 0 ? matchIdx[batchId - 1] : 0;

			if (iFeatureIdx >= col_begin && iFeatureIdx < col_end)
			{
				break;
			}
		}

		float tempProd;
		float tempProd1;

		int prod_batchId_hiddenDim = batchId * hiddenDim;

		float *temp_Xt_OVER_Phitheta = d_temp_Xt_OVER_Phitheta + iFeatureIdx;
		float *tempSparseMatValue = d_tempSparseMatValue + iFeatureIdx;

		// Go to the previous layer by adding ((IdxLayer - 1) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *thetaLayerMinusOne = theta + ((IdxLayer - 1) * prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim);

		// Int temp_theta_xi
		float *temp_theta_xi = d_temp_theta_xi + prod_batchId_hiddenDim;

		uint32_t inputIdx = featureIdx[iFeatureIdx];

		float *startThetaLayerMinusOne = thetaLayerMinusOne;
		float *startTemp_theta_xi = temp_theta_xi;


		float *tempStartGrad_Q_Phi = grad_Q_Phi + inputIdx * hiddenDim;
		float *startGrad_Q_Phi;

		for (uint32_t iHid = 0; iHid < hiddenDim; ++iHid, ++startTemp_theta_xi, ++startThetaLayerMinusOne)
		{
			// Accumulatic sum for batchwise addition: TLayer * temp_Xt_OVER_Phitheta * temp_theta_xi^T * (1 / batchSize)
			tempProd = (*temp_Xt_OVER_Phitheta) * (*startTemp_theta_xi);

			//// Accumulatic sum for batchwise addition: TLayer * temp_Xt_OVER_Phitheta * theta(d, layer - 1)^T * (1 / batchSize)
			tempProd1 = (*tempSparseMatValue) * (*startThetaLayerMinusOne);

			float prod = (tempProd + tempProd1) / batchsize;

			startGrad_Q_Phi = tempStartGrad_Q_Phi + iHid;
			atomicAdd(startGrad_Q_Phi, prod);
		}
	}
}


__global__ void cuda_bp_lda_sum_grad_q_phi(
		float *d_tempSparseMatTemp2,
		float *grad_Q_Phi,
		uint32_t *matchIdx,
		uint32_t *featureIdx,
		uint32_t batchSize,
		uint32_t elementSize,
		uint32_t hiddenDim,
		uint32_t inputDim,
		uint32_t prod_hiddenDim_batchsize
		)
{
	uint32_t iHid = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t iInput = blockDim.y * blockIdx.y + threadIdx.y;
	if (iHid < hiddenDim && iInput < inputDim)
	{
		float *startGrad_Q_Phi = grad_Q_Phi + iInput * hiddenDim + iHid;
		float *startD_tempSparseMatTemp2;

		float sum = 0.0;
		for (int batchId = 0; batchId < batchSize; batchId++)
		{
			// Calculate the X Over Phi Theta (d, layer - 1)
			uint32_t col_end = matchIdx[batchId];
			uint32_t col_begin = batchId > 0 ? matchIdx[batchId - 1] : 0;

			for (uint32_t iFeatureIdx = col_begin; iFeatureIdx < col_end; ++iFeatureIdx)
			{
				uint32_t inputIdx = featureIdx[iFeatureIdx];

				if (inputIdx == iInput)
				{
					startD_tempSparseMatTemp2 = d_tempSparseMatTemp2 + iHid * elementSize + iFeatureIdx;

					sum += *startD_tempSparseMatTemp2;
				}
			}
		}

		*startGrad_Q_Phi += sum;
	}
}


/*************************************************/
//Calculate Epsilon (d, layer - 1)
/*************************************************/
__global__ void cuda_bp_lda_calc_xi_d_layer_minus_one_part1(
		float *theta,
		uint32_t IdxLayer,
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_xi,
		float *d_tempDenseMat, //batchsize * hiddenDim
		float *d_sumTempDenseMat, // hiddenDim * batchsize
		float *d_temp_theta_xi_b_T_OVER_theta_lm1_2,
		float *d_temp_theta_xi
		)
{
	uint32_t iHid = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t batchId = blockDim.y * blockIdx.y + threadIdx.y;

	if (batchId < batchsize && iHid < hiddenDim)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;
		int prod_iHid_batchSize = iHid * batchsize;

		// Go to the previous layer and hid by adding ((IdxLayer - 1) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayerMinusOne = theta + ((IdxLayer - 1) * prod_hiddenDim_batchsize) + (prod_batchId_hiddenDim)+iHid;

		// Go to the current layer and hid by adding ((IdxLayer) * prod_hiddenDim_batchsize) and go to the row by adding (prod_batchId_hiddenDim);
		float *startThetaLayer = startThetaLayerMinusOne + prod_hiddenDim_batchsize;

		float tempVar;
		float tempVar1;

		float *sumTempDenseMat = d_sumTempDenseMat + prod_batchId_hiddenDim + iHid;

		float *xi = d_xi + prod_batchId_hiddenDim + iHid;

		float *tempDenseMat = d_tempDenseMat + prod_batchId_hiddenDim + iHid;

		float *temp_theta_xi_b_T_OVER_theta_lm1_2 = d_temp_theta_xi_b_T_OVER_theta_lm1_2 + prod_batchId_hiddenDim + iHid;

		float *temp_theta_xi = d_temp_theta_xi + prod_batchId_hiddenDim + iHid;

		// Theta(d, layer) / theta(d, layer - 1)
		tempVar = (*startThetaLayer) / (*startThetaLayerMinusOne + epsilon);
		if (abs(*startThetaLayerMinusOne) < 1e-30)
		{
			tempVar = 0.0;
		}

		// [Theta(d, layer) / Theta(d, layer - 1) ]    .    xi(Epsilon)
		*xi *= tempVar;

		// [Theta(d, layer) / Theta(d, layer - 1) ]    .    xi(Epsilon)     -    Phi ^ T.TLayer * temp_Phi_theta_xi . temp_Xt_OVER_Phitheta2
		//tempDenseMat[iHid]
		tempVar1 = *xi - *tempDenseMat;

		//  [Theta(d, layer) / Theta(d, layer - 1) ] . xi(Epsilon)   -  Phi ^ T.TLayer * temp_Phi_theta_xi . temp_Xt_OVER_Phitheta2 - T(d, layer)   *  b * [  (Theta(d, layer) * Epsilon (d, layer) ) / (Theta (d, layer - 1) ^ 2)   ]
		tempVar1 -= *temp_theta_xi_b_T_OVER_theta_lm1_2;
		*tempDenseMat = tempVar1;

		//MatrixOperation.ElementwiseVectorMultiplyVector(
		//	tmp_theta_xi.DenseMatrixValue[IdxSample],
		//	DNNRun.theta_pool[IdxLayer - 1].DenseMatrixValue[IdxSample],
		//	TmpDenseMat.DenseMatrixValue[IdxSample]
		//	); // tmp_theta_xi is tmp1
		*temp_theta_xi = (*startThetaLayerMinusOne) * tempVar1;
		*sumTempDenseMat += *temp_theta_xi;
	}
}

/**************************************************/
//Update xi
/**************************************************/
__global__ void cuda_bp_lda_calc_xi_d_layer_minus_one_part2(
		uint32_t batchsize,
		uint32_t hiddenDim,
		uint32_t prod_hiddenDim_batchsize,
		float *d_xi,
		float *d_tempResBatch,
		float *d_tempDenseMat
		)
{
	uint32_t iHid = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t batchId = blockDim.y * blockIdx.y + threadIdx.y;

	if (batchId < batchsize && iHid < hiddenDim)
	{
		int prod_batchId_hiddenDim = batchId * hiddenDim;

		float *xi = d_xi + prod_batchId_hiddenDim + iHid;
		float *tempResBatch = d_tempResBatch + batchId;
		float *tempDenseMat = d_tempDenseMat + prod_batchId_hiddenDim + iHid;

		*xi = *tempDenseMat - *tempResBatch;
	}
}

/***********************************************/
// Backward compatible 
/***********************************************/
void cuda_Bp_Lda_Backward(
		float *output,
		float *matchLabelValue,
		float *theta,
		float *phi,
		float *b,
		float *U,
		float *T,
		float *iterT,
		float *grad_Q_U,
		float *grad_Q_Phi,
		uint32_t *matchIdx,
		uint32_t *featureIdx,
		float *featureValue,
		float gamma,
		float negativeBetaMinusOneOverTotalSamples,
		uint32_t batchsize,
		uint32_t inputDim,
		uint32_t hiddenDim,
		uint32_t outputDim,
		uint32_t layerDim,
		uint32_t elementSize,
		float *d_xi,
		float *d_tempDenseMat,
		float *d_temp_theta_xi,
		float *d_temp_theta_xi_b_T_OVER_theta_lm1_2,
		float *d_thetaRatio,
		float *d_sumGrad_Q_U, //outputdim * hiddenDim * batchsize
		float *d_temp_Xt_OVER_Phitheta, // elementSize
		float *d_tempSparseMatValue, // elementSize
		float *d_sumTempDenseMat, // batchsize * hiddendim
		float *d_tempSparseMat, // elementSize
		float *d_tempResBatch, // batchsize
		uint32_t calculateSoftMax
		)
{
	/***************************************************/
	// Init Grad_Q_Phi with (-1.0f) * ((beta - 1) / ((float)nTrain)) / paramModel.Phi
	/***************************************************/
	//MatrixOperation.ScalarDivideMatrix(Grad.grad_Q_Phi, (-1.0f) * ((beta - 1) / ((float)nTrain)), paramModel.Phi, true);
	dim3 thread_tail_hidden_input_2D(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail_hidden_input_2D((hiddenDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (inputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_bp_lda_init_grad_q_phi << <block_tail_hidden_input_2D, thread_tail_hidden_input_2D >> >(
			grad_Q_Phi,
			phi,
			negativeBetaMinusOneOverTotalSamples,
			hiddenDim,
			inputDim
			);

	/****************************************************/
	//Init grad_Q_U with zero 
	// !!!!!!!!!!!!!!!!!!!!!!!!!  CAUTION: possible place to lose precision: We can uncomment and do it in C code too !!!!!!!!!!!!!!!!
	/**************************************************************/
	dim3 thread_tail_u(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail_u((hiddenDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (outputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_bp_lda_init_grad_q_u << <block_tail_u, thread_tail_u >> >(
			grad_Q_U,
			hiddenDim,
			outputDim
			);

	/**********************Precompute constants*************/
	/******************************************************/
	int prod_hiddenDim_batchsize = hiddenDim * batchsize;
	/**********************Precompute constants*************/
	/******************************************************/

	uint32_t nThreadPerBlock_elementSize_1D = DEFAULT_THREAD_PER_BLOCK;
	uint32_t nBlockPerGrid_elementSize_1D = (elementSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail_hidden_batch_2D(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail_hidden_batch_2D((hiddenDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	uint32_t nThreadPerBlock_batchsize_1D = DEFAULT_THREAD_PER_BLOCK;
	uint32_t nBlockPerGrid_batchsize_1D = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;


	cuda_bp_lda_calc_grad_q_u_init_xi_part1 << <block_tail_hidden_batch_2D, thread_tail_hidden_batch_2D >> >(
			output,
			matchLabelValue,
			theta,
			U,
			iterT,
			gamma,
			batchsize,
			hiddenDim,
			outputDim,
			layerDim,
			prod_hiddenDim_batchsize,
			d_xi,
			d_tempDenseMat, //hiddenDim * batchsize 
			d_sumGrad_Q_U, // outputdim * hiddenDim * batchsize
			d_sumTempDenseMat, // batchsize * hiddenDim
			calculateSoftMax
			);

	// Calculate 
	for (int iOutputDim = 0; iOutputDim < outputDim; ++iOutputDim)
	{
		float *startD_sumGrad_Q_U = d_sumGrad_Q_U + iOutputDim * prod_hiddenDim_batchsize;
		float *startGrad_Q_U = grad_Q_U + iOutputDim * hiddenDim;

		cuda_rowwise_sum_part_by_part << < hiddenDim, 64 >> >(
				startD_sumGrad_Q_U,
				startGrad_Q_U,
				batchsize
				);
	}

	// Calculate tempDenseMat sum to be used for xi calculation
	cuda_rowwise_sum_part_by_part << < batchsize, 64 >> >(
			d_sumTempDenseMat,
			d_tempResBatch,
			hiddenDim
			);

	// Calculate xi using sum from previous step
	cuda_bp_lda_calc_grad_q_u_init_xi_part2 << <block_tail_hidden_batch_2D, thread_tail_hidden_batch_2D >> >(
			batchsize,
			hiddenDim,
			prod_hiddenDim_batchsize,
			d_xi,
			d_tempResBatch
			);

	/*****************************************************/
	//Implementing main loop
	/*****************************************************/
	// !! in original code layerDim need to be used. as its fixed number of layers
	for (int IdxLayer = layerDim; IdxLayer >= 1; IdxLayer--)
	{
		cuda_bp_lda_calc_temp_theta_xi_b_T_OVER_theta_lm1_2 << <block_tail_hidden_batch_2D, thread_tail_hidden_batch_2D >> >(
				IdxLayer,
				theta,
				T,
				b,
				d_xi, // batchsize * hiddenDim
				d_temp_theta_xi, // batchsize * hiddenDim
				d_temp_theta_xi_b_T_OVER_theta_lm1_2,
				U,
				batchsize,
				hiddenDim,
				prod_hiddenDim_batchsize
				);

		// Reset the d_tempDenseMat
		cudaMemset(d_tempDenseMat, 0, prod_hiddenDim_batchsize * sizeof(float));

		cuda_bp_lda_calc_x_over_phitheta_grad_q_phi_part0 << < nBlockPerGrid_elementSize_1D, nThreadPerBlock_elementSize_1D >> >(
				phi,
				theta,
				T,
				IdxLayer,
				matchIdx,
				featureIdx,
				featureValue,
				batchsize,
				elementSize,
				hiddenDim,
				prod_hiddenDim_batchsize,
				d_temp_Xt_OVER_Phitheta, // elementSize
				d_tempSparseMatValue, // elementSize
				d_tempDenseMat, //batchSize * hiddenDim 
				d_tempSparseMat, // elementSize
				d_temp_theta_xi
				);

		cuda_bp_lda_calc_x_over_phitheta_grad_q_phi_part1 << < nBlockPerGrid_elementSize_1D, nThreadPerBlock_elementSize_1D >> >(
				theta,
				T,
				IdxLayer,
				matchIdx,
				featureIdx,
				featureValue,
				batchsize,
				elementSize,
				hiddenDim,
				inputDim,
				prod_hiddenDim_batchsize,
				d_temp_Xt_OVER_Phitheta, // elementSize
				d_tempSparseMatValue, // elementSize
				d_tempDenseMat, //batchSize * hiddenDim 
				d_tempSparseMat, // elementSize
				d_temp_theta_xi, // batchsize * hiddenDim
				grad_Q_Phi
				);

		/*************************************************/
		//Calculate Epsilon (d, layer - 1)
		/*************************************************/
		if (IdxLayer > 1)
		{
			// Reset the d_sumTempDenseMat
			cudaMemset(d_sumTempDenseMat, 0, prod_hiddenDim_batchsize * sizeof(float));

			cuda_bp_lda_calc_xi_d_layer_minus_one_part1 << <block_tail_hidden_batch_2D, thread_tail_hidden_batch_2D >> >(
					theta,
					IdxLayer,
					batchsize,
					hiddenDim,
					prod_hiddenDim_batchsize,
					d_xi,
					d_tempDenseMat, //batchsize * hiddenDim
					d_sumTempDenseMat, // !!!COLUMN MAJOR FORMAT SO WE CAN USE IT IN CUBLAS!!!  * hiddenDim * batchsize
					d_temp_theta_xi_b_T_OVER_theta_lm1_2,
					d_temp_theta_xi
					);

			// Calculate tempDenseMat sum to be used for xi calculation
			cuda_rowwise_sum_part_by_part << < batchsize, 64 >> >(
					d_sumTempDenseMat,
					d_tempResBatch,
					hiddenDim
					);

			cuda_bp_lda_calc_xi_d_layer_minus_one_part2 << <block_tail_hidden_batch_2D, thread_tail_hidden_batch_2D >> >(
					batchsize,
					hiddenDim,
					prod_hiddenDim_batchsize,
					d_xi,
					d_tempResBatch,
					d_tempDenseMat
					);
		}
	}
}


/*****************************************************/
//U = U - mu_U . grad_Q_U
/*****************************************************/
__global__ void cuda_bp_lda_update_u(
		float *grad_Q_U,
		float *U,
		float mu_U,
		uint32_t hiddenDim,
		uint32_t outputDim
		)
{
	uint32_t hiddenIndex = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t outputIndex = blockDim.y * blockIdx.y + threadIdx.y;

	if (hiddenIndex < hiddenDim && outputIndex < outputDim)
	{
		float *startGrad_Q_U = grad_Q_U + outputIndex * hiddenDim + hiddenIndex;
		float *startU = U + outputIndex * hiddenDim + hiddenIndex;
		*startU = (*startU) - mu_U * (*startGrad_Q_U);
	}
}


/***************************************************/
// adaGradSum(k) += (1/inputDim)||grad_Q_Phi||^2
// Store adagrad to be summed later
/***************************************************/
__global__ void cuda_bp_lda_update_adagradsum_part1(
		float *d_sumAdaGradSum,
		float *grad_Q_Phi,
		uint32_t inputDim,
		uint32_t hiddenDim
		)
{
	uint32_t hiddenIndex = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t inputIndex = blockDim.y * blockIdx.y + threadIdx.y;
	if (inputIndex < inputDim && hiddenIndex < hiddenDim)
	{
		float *startGrad_Q_Phi = grad_Q_Phi + inputIndex * hiddenDim + hiddenIndex;
		float *startD_sumAdaGradSum = d_sumAdaGradSum + hiddenIndex * inputDim + inputIndex;
		// Using atomic as we are using two dimensional parallelism
		*startD_sumAdaGradSum = (1.0 / (float)inputDim) * (*startGrad_Q_Phi) * (*startGrad_Q_Phi);
	}
}


/********************************************************************************************/
// mu_PhiSearch(k) = mu_Phi / [ sqrt (adagradSum(k)/cntModelUpdate) + mu_Phi ]
/******************************************************************************************/
__global__ void cuda_bp_lda_update_mu_phi_search(
		float *adaGradSum,
		float *muPhiSearch,
		float mu_Phi,
		uint32_t hiddenDim,
		uint32_t cntModelUpdate
		)
{
	uint32_t hiddenIndex = blockDim.x * blockIdx.x + threadIdx.x;

	if (hiddenIndex < hiddenDim)
	{
		float *startAdaGradSum = adaGradSum + hiddenIndex;
		float *startMuPhiSearch = muPhiSearch + hiddenIndex;

		*startMuPhiSearch = (mu_Phi) / (sqrtf((*startAdaGradSum) / (float)cntModelUpdate) + mu_Phi);
	}
}

/******************************************************/
//Calculates (-1.0) muPhiSearch . grad_Q_Phi)
//Calculates column max
/******************************************************/
__global__ void cuda_bp_lda_update_phi_step_10(
		float *muPhiSearch,
		float *grad_Q_Phi,
		float *update,
		float *tempColSum,
		float *tempColMax,
		uint32_t inputDim,
		uint32_t hiddenDim
		)
{
	uint32_t hiddenIndex = blockDim.x * blockIdx.x + threadIdx.x;

	if (hiddenIndex < hiddenDim)
	{
		int iTempColMax = hiddenIndex;
		float tempVal;

		for (uint32_t iInputDim = 0; iInputDim < inputDim; iInputDim++)
		{
			int iGrad_Q_Phi = iInputDim * hiddenDim + hiddenIndex;

			//(-1.0) muPhiSearch . grad_Q_Phi)
			float tempProd = (-1.0) * muPhiSearch[hiddenIndex] * grad_Q_Phi[iGrad_Q_Phi];

			//save it for return
			update[iGrad_Q_Phi] = tempProd;

			tempVal = tempColMax[iTempColMax];
			// Calculating the max for each column
			if (iInputDim == 0)
			{
				tempColMax[iTempColMax] = tempProd;
			}
			else
			{
				tempColMax[iTempColMax] = tempProd > tempVal ? tempProd : tempVal;
			}
		}
	}
}


/******************************************************/
//phi(k) = phi(k) * exp (- muPhiSearch . grad_Q_Phi)
/******************************************************/
__global__ void cuda_bp_lda_update_phi_step_15(
		float *phi,
		float *update,
		float *d_sumTempColSum,
		float *tempColMax,
		uint32_t inputDim,
		uint32_t hiddenDim
		)
{
	uint32_t hiddenIndex = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t inputIndex = blockDim.y * blockIdx.y + threadIdx.y;

	if (inputIndex < inputDim && hiddenIndex < hiddenDim)
	{
		float tempVal;

		int iPhi = inputIndex * hiddenDim + hiddenIndex;
		int iTempColMax = hiddenIndex;
		float *sumTempColSum = d_sumTempColSum + hiddenIndex * inputDim + inputIndex;

		// x - xMax before taking exp		
		tempVal = update[iPhi] - tempColMax[iTempColMax];

		// exp
		tempVal = expf(tempVal);
		update[iPhi] = tempVal;

		// Phi . Exp (-muPhiSearch . grad_Q_Phi)
		phi[iPhi] *= tempVal;

		*sumTempColSum = phi[iPhi];
	}
}

/******************************************************/
//Normalizing
//phi(k) = 1/C(k) . phi(k) * exp (- muPhiSearch . grad_Q_Phi)
/******************************************************/
__global__ void cuda_bp_lda_update_phi_step_20(
		float *phi,
		float *tempColSum,
		uint32_t inputDim,
		uint32_t hiddenDim
		)
{
	uint32_t hiddenIndex = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t inputIndex = blockDim.y * blockIdx.y + threadIdx.y;
	if (inputIndex < inputDim && hiddenIndex < hiddenDim)
	{
		float *startPhi = phi + inputIndex * hiddenDim + hiddenIndex;
		float *startTempColSum = tempColSum + hiddenIndex;
		// normalizing
		*startPhi = (*startPhi) / (*startTempColSum);
	}
}



/*********************************************************/
//SMD Update
/*********************************************************/
void cuda_Bp_Lda_Update(
		float *grad_Q_U,
		float *grad_Q_Phi,
		float *U,
		float *phi,
		float *adaGradSum,
		float *d_sumAdaGradSum,
		float *d_sumTempColSum,
		float *muPhiSearch,
		float mu_U,
		float mu_Phi,
		uint32_t cntModelUpdate,
		uint32_t inputDim,
		uint32_t hiddenDim,
		uint32_t outputDim,
		float *d_update,
		float *d_tempColSum,
		float *d_tempColMax
		)
{
	// (ii) Update U                    
	//MatrixOperation.ScalarMultiplyMatrix(Grad.grad_Q_U, (-1.0f) * mu_U);
	//MatrixOperation.MatrixAddMatrix(paramModel.U, Grad.grad_Q_U);
	dim3 thread_tail_u(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail_u((hiddenDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (outputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_bp_lda_update_u << <block_tail_u, thread_tail_u >> >(
			grad_Q_U,
			U,
			mu_U,
			hiddenDim,
			outputDim
			);

	// adaGradSum(k) += (1/inputDim)||grad_Q_Phi||^2
	dim3 thread_tail_hidden_input(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail_hidden_input((hiddenDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (inputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_bp_lda_update_adagradsum_part1 << <block_tail_hidden_input, thread_tail_hidden_input >> >(
			d_sumAdaGradSum,
			grad_Q_Phi,
			inputDim,
			hiddenDim
			);

	// Calculate the separately to avoid atomic and speed up
	cuda_rowwise_sum_part_by_part_additive << < hiddenDim, 64 >> >(
			d_sumAdaGradSum,
			adaGradSum,
			inputDim,
			1.0f
			);

	// mu_PhiSearch(k) = mu_Phi / [ sqrt (adagradSum(k)/cntModelUpdate) + mu_Phi ]
	uint32_t nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	uint32_t nBlockPerGrid = (hiddenDim + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_bp_lda_update_mu_phi_search << <nBlockPerGrid, nThreadPerBlock >> >(
			adaGradSum,
			muPhiSearch,
			mu_Phi,
			hiddenDim,
			cntModelUpdate
			);

	// Reset the d_tempColSum, d_tempColMax does not need to reset as it gets assigned in the kernel instead of adding on top of it
	//cudaMemset(d_tempColMax, 0, hiddenDim * sizeof(float));
	cudaMemset(d_tempColSum, 0, hiddenDim * sizeof(float));

	/*******************************************************************************/
	// Breaking down update in three steps to achieve maximum parallelization
	/*******************************************************************************/
	cuda_bp_lda_update_phi_step_10 << <nBlockPerGrid, nThreadPerBlock >> >(
			muPhiSearch,
			grad_Q_Phi,
			d_update,
			d_tempColSum,
			d_tempColMax,
			inputDim,
			hiddenDim);

	cuda_bp_lda_update_phi_step_15 << <block_tail_hidden_input, thread_tail_hidden_input >> >(
			phi,
			d_update,
			d_sumTempColSum,
			d_tempColMax,
			inputDim,
			hiddenDim);

	// Calculate the separately to avoid atomic and speed up
	cuda_rowwise_sum_part_by_part << < hiddenDim, 64 >> >(
			d_sumTempColSum,
			d_tempColSum,
			inputDim
			);

	cuda_bp_lda_update_phi_step_20 << <block_tail_hidden_input, thread_tail_hidden_input >> >(
			phi,
			d_tempColSum,
			inputDim,
			hiddenDim
			);
}
