#include "stdafx.h"
#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <cfloat>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>
#include "device_functions.h"
#include <fstream>
#include "cublas_v2.h"

#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

using namespace std;

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
using namespace _com_util;
#else
#include <cfloat>
#endif

/**************************New Version of Cuda Matrix Operators************************/
__global__ void cuda_VectorProjection(float * a, float * M, float * b, int inputSize, int outputSize, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if(idx < outputSize)
	{
		float sum = 0;
		float * d_iter = a;
		float * m_iter = M + idx;
		float * d_end_pt = d_iter + inputSize;
		while (d_iter < d_end_pt)
		{
			sum += (*d_iter++) * (*m_iter);
			m_iter += outputSize;
		}
		b[idx] = b[idx] * weight + sum;
	}
}
/// b = b * weight + a * M;
/// M : row : inputSize; column : outputSize;
void Cuda_VectorProjection(float * a, float * M, float * b, int inputSize, int outputSize, float weight)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (outputSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_VectorProjection<<<nBlockPerGrid, nThreadPerBlock>>>(a, M, b, inputSize, outputSize, weight);
}


__global__ void cuda_VectorProjectionTranspose(float * a, float * M, float * b, int inputSize, int outputSize, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if(idx < outputSize)
	{
		float sum = 0;
		float * d_iter = a;
		float * m_iter = M + idx * inputSize;
		float * d_end_pt = d_iter + inputSize;
		while (d_iter < d_end_pt)
		{
			sum += (*d_iter++) * (*m_iter++);
		}
		b[idx] = b[idx] * weight + sum;
	}
}
/// b = b * weight + M * a;
/// M : row : outputSize; column : inputSize;
void Cuda_VectorProjectionTranspose(float * a, float * M, float * b, int inputSize, int outputSize, float weight)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (outputSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_VectorProjectionTranspose<<<nBlockPerGrid, nThreadPerBlock>>>(a, M, b, inputSize, outputSize, weight);
}



__global__ void cuda_Matrix_Add(float ** pLeft, float ** pRight, float ** pDst, int row, int column, float weiLeft, float weiRight, float weiDst)
{
	int x = blockDim.x*blockIdx.x + threadIdx.x;
	int y = blockDim.y*blockIdx.y + threadIdx.y;
	if (x < column && y < row)
		pDst[y][x] = weiDst * pDst[y][x] + weiLeft * pLeft[y][x] + weiRight * pRight[y][x];
}
__global__ void cuda_Matrix_Add(float * pLeft, float * pRight, float * pDst, int row, int column, float weiLeft, float weiRight, float weiDst)
{
	int x = blockDim.x*blockIdx.x + threadIdx.x;
	int y = blockDim.y*blockIdx.y + threadIdx.y;

	int pos = y * column + x;
	if (x < column && y < row)
		pDst[pos] = weiDst * pDst[pos] + weiLeft * pLeft[pos] + weiRight * pRight[pos];
}
__global__ void cuda_Matrix_Add(float * a, int * aMask, float * b, int * bMask, float * c, int * cMask, int dim, int batchSize, 
		float awei, float bwei, float cwei)
{
	int x = blockDim.x*blockIdx.x + threadIdx.x;
	int y = blockDim.y*blockIdx.y + threadIdx.y;

	if (x < dim && y < batchSize)
	{
		int aPos = (aMask == NULL ? y : aMask[y]) * dim + x;
		int bPos = (bMask == NULL ? y : bMask[y]) * dim + x;
		int cPos = (cMask == NULL ? y : cMask[y]) * dim + x;
		c[cPos] = cwei * c[cPos] + awei * a[aPos] + bwei * b[bPos];
	}
}
__global__ void cuda_Matrix_Add(float * a, int skipa, float * b, int skipB, float * c, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
{
	int x = blockDim.x*blockIdx.x + threadIdx.x;
	int y = blockDim.y*blockIdx.y + threadIdx.y;

	if (x < dim && y < batchSize)
	{
		int aPos = y * skipa + x;
		int bPos = y * skipB + x;
		int cPos = y * skipC + x;
		c[cPos] = cwei * c[cPos] + awei * a[aPos] + bwei * b[bPos];
	}
}
__global__ void cuda_Matrix_Add(float * a, int * aMask, int skipA, float * b, int * bMask, int skipB, float * c, int * cMask, int skipC, int dim, int batchSize,
	float awei, float bwei, float cwei)
{
	int x = blockDim.x*blockIdx.x + threadIdx.x;
	int y = blockDim.y*blockIdx.y + threadIdx.y;

	if (x < dim && y < batchSize)
	{
		int aPos = (aMask == NULL ? y : aMask[y]) * skipA + x;
		int bPos = (bMask == NULL ? y : bMask[y]) * skipB + x;
		int cPos = (cMask == NULL ? y : cMask[y]) * skipC + x;
		c[cPos] = cwei * c[cPos] + awei * a[aPos] + bwei * b[bPos];
	}
}
/// pDst = pDst * weiDst + pLeft * weiLeft + pRight * weiRight;
void Cuda_Matrix_Add(float ** pLeft, float ** pRight, float ** pDst, int row, int column, float weiLeft, float weiRight, float weiDst)
{
	dim3 thread_tail(8, 32);
	dim3 block_tail((column + 8 - 1) / 8, (row + 32 - 1) / 32);
	cuda_Matrix_Add << <block_tail, thread_tail >> >(
			pLeft, pRight, pDst, row, column, weiLeft, weiRight, weiDst);
}
void Cuda_Matrix_Add(float * pLeft, float * pRight, float * pDst, int row, int column, float weiLeft, float weiRight, float weiDst)
{
	dim3 thread_tail(8, 32);
	dim3 block_tail((column + 8 - 1) / 8, (row + 32 - 1) / 32);
	cuda_Matrix_Add << <block_tail, thread_tail >> >( pLeft, pRight, pDst, row, column, weiLeft, weiRight, weiDst);
}
void Cuda_Matrix_Add(float * a, int * aMask, float * b, int * bMask, float * c, int * cMask, int dim, int batchSize, float awei, float bwei, float cwei)
{
	dim3 thread_tail(8, 32);
	dim3 block_tail((dim + 8 - 1) / 8, (batchSize + 32 - 1) / 32);
	cuda_Matrix_Add << <block_tail, thread_tail >> >(a, aMask, b, bMask, c, cMask, dim, batchSize, awei, bwei, cwei);
}
void Cuda_Matrix_Add(float * a, int skipa, float * b, int skipB, float * c, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
{
	dim3 thread_tail(8, 32);
	dim3 block_tail((dim + 8 - 1) / 8, (batchSize + 32 - 1) / 32);
	cuda_Matrix_Add << <block_tail, thread_tail >> >(a, skipa, b, skipB, c, skipC, dim, batchSize, awei, bwei, cwei);
}
void Cuda_Matrix_Add(float * a, int * aMask, int skipA, float * b, int * bMask, int skipB, float * c, int * cMask, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
{
	dim3 thread_tail(8, 32);
	dim3 block_tail((dim + 8 - 1) / 8, (batchSize + 32 - 1) / 32);
	cuda_Matrix_Add << <block_tail, thread_tail >> >(a, aMask, skipA, b, bMask, skipB, c, cMask, skipC, dim, batchSize, awei, bwei, cwei);
}

__global__ void cuda_SoftAttention_Matching(float * a, int * aMask, float * b, int * bMask, int dim, int a_func, int op, float * vec,
	float * c, int * cMask, int batchSize, float alpha, float beta)
{
	int x = blockDim.x * blockIdx.x + threadIdx.x;
	if (x < batchSize)
	{
		int aPos = (aMask == NULL ? x : aMask[x]) * dim;
		int bPos = (bMask == NULL ? x : bMask[x]) * dim;
		int cPos = (cMask == NULL ? x : cMask[x]);

		float sum = 0;

		if (op == 0) // addition.
		{
			if (a_func == 0) //linear.
			{
				for (int i = 0; i < dim; i++)
				{
					sum += (a[aPos + i] + b[bPos + i]) * vec[i];
				}
			}
			else if (a_func == 1) //tanh.
			{
				for (int i = 0; i < dim; i++)
				{
					sum += tanhf(a[aPos + i] + b[bPos + i]) * vec[i];
				}
			}
			else if (a_func == 2)//rectified.
			{
				for (int i = 0; i < dim; i++)
				{
					float v = a[aPos + i] + b[bPos + i];
					if (v < 0) v = 0;
					sum += v * vec[i];
				}
			}
			else if (a_func == 3)//sigmoid.
			{
				for (int i = 0; i < dim; i++)
				{
					float v = a[aPos + i] + b[bPos + i];
					v = tanhf(v / 2.0f);
					v = (v + 1.0f) / 2.0f;
					sum += v * vec[i];
				}
			}
		}
		else if (op == 1)
		{
			if (a_func == 0) //linear.
			{
				for (int i = 0; i < dim; i++)
				{
					sum += (a[aPos + i] * b[bPos + i]) * vec[i];
				}
			}
			else if (a_func == 1) //tanh.
			{
				for (int i = 0; i < dim; i++)
				{
					sum += tanhf(a[aPos + i] * b[bPos + i]) * vec[i];
				}
			}
			else if (a_func == 2)//rectified.
			{
				for (int i = 0; i < dim; i++)
				{
					float v = a[aPos + i] * b[bPos + i];
					if (v < 0) v = 0;
					sum += v * vec[i];
				}
			}
			else if (a_func == 3)//sigmoid.
			{
				for (int i = 0; i < dim; i++)
				{
					float v = a[aPos + i] * b[bPos + i];
					v = tanhf(v / 2.0f);
					v = (v + 1.0f) / 2.0f;
					sum += v * vec[i];
				}
			}
		}
		c[cPos] = alpha * c[cPos] + beta * sum;
	}
}

void Cuda_SoftAttention_Matching(float * a, int * aMask, float * b, int * bMask, int dim, int a_func, int op, float * vec,
	float * c, int * cMask, int batchSize, float alpha, float beta)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_SoftAttention_Matching << <nBlockPerGrid, nThreadPerBlock >> > (a, aMask, b, bMask, dim, a_func, op, vec, c, cMask, batchSize, alpha, beta);
}

__global__ void cuda_SoftAttention(float * a,  float * b, int dim, int a_func, int op, float * vec,
	float * c, int aBatchSize, int bBatchSize, float alpha, float beta)
{
	int x = blockDim.x * blockIdx.x + threadIdx.x;
	int y = blockDim.y * blockIdx.y + threadIdx.y;
	if (x < aBatchSize && y < bBatchSize)
	{
		int aPos = x * dim; 
		int bPos = y * dim;
		int cPos = x * bBatchSize + y;

		float sum = 0;

		if (op == 0) // addition.
		{
			if (a_func == 0) //linear.
			{
				for (int i = 0; i < dim; i++)
				{
					sum += (a[aPos + i] + b[bPos + i]) * vec[i];
				}
			}
			else if (a_func == 1) //tanh.
			{
				for (int i = 0; i < dim; i++)
				{
					sum += tanhf(a[aPos + i] + b[bPos + i]) * vec[i];
				}
			}
			else if (a_func == 2)//rectified.
			{
				for (int i = 0; i < dim; i++)
				{
					float v = a[aPos + i] + b[bPos + i];
					if (v < 0) v = 0;
					sum += v * vec[i];
				}
			}
			else if (a_func == 3)//sigmoid.
			{
				for (int i = 0; i < dim; i++)
				{
					float v = a[aPos + i] + b[bPos + i];
					v = tanhf(v / 2.0f);
					v = (v + 1.0f) / 2.0f;
					sum += v * vec[i];
				}
			}
		}
		else if (op == 1)
		{
			if (a_func == 0) //linear.
			{
				for (int i = 0; i < dim; i++)
				{
					sum += (a[aPos + i] * b[bPos + i]) * vec[i];
				}
			}
			else if (a_func == 1) //tanh.
			{
				for (int i = 0; i < dim; i++)
				{
					sum += tanhf(a[aPos + i] * b[bPos + i]) * vec[i];
				}
			}
			else if (a_func == 2)//rectified.
			{
				for (int i = 0; i < dim; i++)
				{
					float v = a[aPos + i] * b[bPos + i];
					if (v < 0) v = 0;
					sum += v * vec[i];
				}
			}
			else if (a_func == 3)//sigmoid.
			{
				for (int i = 0; i < dim; i++)
				{
					float v = a[aPos + i] * b[bPos + i];
					v = tanhf(v / 2.0f);
					v = (v + 1.0f) / 2.0f;
					sum += v * vec[i];
				}
			}
		}
		c[cPos] = alpha * c[cPos] + beta * sum;
	}
}

void Cuda_SoftAttention(float * a, float * b, int dim, int a_func, int op, float * vec, float * c, int aBatchSize, int bBatchSize, float alpha, float beta)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((aBatchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (bBatchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_SoftAttention << <block_tail, thread_tail >> > (a, b, dim, a_func, op, vec, c, aBatchSize, bBatchSize, alpha, beta);
}


__global__ void cuda_DerivSoftAttention_Vec(float * a, int * aMask, float * b, int * bMask, int dim, int a_func, int op, float * vecDeriv, float * cDeriv, int * cMask, int batchSize, float vwei)
{
	int d = blockDim.x * blockIdx.x + threadIdx.x;
	if (d < dim)
	{
		float sum = 0;

		if (op == 0) // addition.
		{
			if (a_func == 0) //linear.
			{
				for (int x = 0; x < batchSize; x++)
				{
					int aPos = (aMask == NULL ? x : aMask[x]) * dim;
					int bPos = (bMask == NULL ? x : bMask[x]) * dim;
					int cPos = (cMask == NULL ? x : cMask[x]);
					float v = a[aPos + d] + b[bPos + d];
					sum += cDeriv[cPos] * v;
				}
			}
			else if (a_func == 1) //tanh.
			{
				for (int x = 0; x < batchSize; x++)
				{
					int aPos = (aMask == NULL ? x : aMask[x]) * dim;
					int bPos = (bMask == NULL ? x : bMask[x]) * dim;
					int cPos = (cMask == NULL ? x : cMask[x]);
					float v = tanhf(a[aPos + d] + b[bPos + d]);
					sum += cDeriv[cPos] * v;
				}

			}
			else if (a_func == 2)//rectified.
			{
				for (int x = 0; x < batchSize; x++)
				{
					int aPos = (aMask == NULL ? x : aMask[x]) * dim;
					int bPos = (bMask == NULL ? x : bMask[x]) * dim;
					int cPos = (cMask == NULL ? x : cMask[x]);
					float v = a[aPos + d] + b[bPos + d];
					if (v < 0) v = 0;
					sum += cDeriv[cPos] * v;
				}
			}
			else if (a_func == 3)//sigmoid.
			{
				for (int x = 0; x < batchSize; x++)
				{
					int aPos = (aMask == NULL ? x : aMask[x]) * dim;
					int bPos = (bMask == NULL ? x : bMask[x]) * dim;
					int cPos = (cMask == NULL ? x : cMask[x]);
					float v = a[aPos + d] + b[bPos + d];
					v = tanhf(v / 2.0f);
					v = (v + 1.0f) / 2.0f;
					sum += cDeriv[cPos] * v;
				}
			}
		}
		else if (op == 1)
		{
			if (a_func == 0) //linear.
			{
				for (int x = 0; x < batchSize; x++)
				{
					int aPos = (aMask == NULL ? x : aMask[x]) * dim;
					int bPos = (bMask == NULL ? x : bMask[x]) * dim;
					int cPos = (cMask == NULL ? x : cMask[x]);
					float v = a[aPos + d] * b[bPos + d];
					sum += cDeriv[cPos] * v;
				}
			}
			else if (a_func == 1) //tanh.
			{
				for (int x = 0; x < batchSize; x++)
				{
					int aPos = (aMask == NULL ? x : aMask[x]) * dim;
					int bPos = (bMask == NULL ? x : bMask[x]) * dim;
					int cPos = (cMask == NULL ? x : cMask[x]);
					float v = tanhf(a[aPos + d] * b[bPos + d]);
					sum += cDeriv[cPos] * v;
				}

			}
			else if (a_func == 2)//rectified.
			{
				for (int x = 0; x < batchSize; x++)
				{
					int aPos = (aMask == NULL ? x : aMask[x]) * dim;
					int bPos = (bMask == NULL ? x : bMask[x]) * dim;
					int cPos = (cMask == NULL ? x : cMask[x]);
					float v = a[aPos + d] * b[bPos + d];
					if (v < 0) v = 0;
					sum += cDeriv[cPos] * v;
				}
			}
			else if (a_func == 3)//sigmoid.
			{
				for (int x = 0; x < batchSize; x++)
				{
					int aPos = (aMask == NULL ? x : aMask[x]) * dim;
					int bPos = (bMask == NULL ? x : bMask[x]) * dim;
					int cPos = (cMask == NULL ? x : cMask[x]);
					float v = a[aPos + d] * b[bPos + d];
					v = tanhf(v / 2.0f);
					v = (v + 1.0f) / 2.0f;
					sum += cDeriv[cPos] * v;
				}
			}
		}
		vecDeriv[d] = vwei * vecDeriv[d] + sum;
	}
}

__global__ void cuda_DerivSoftAttention_Src(int * smpIdx, int * elementIdx, int size, float * src, float * srcDeriv, int * srcMask, 
				float * tgt, int * tgtMask, int dim, int a_func, int op, float * vec, float * cDeriv, int * cMask, float srcwei)
{
	int s = blockDim.x * blockIdx.x + threadIdx.x;
	int d = blockDim.y * blockIdx.y + threadIdx.y;

	if (s < size && d < dim)
	{
		int smpBgn = s == 0 ? 0 : smpIdx[s - 1];
		int smpEnd = smpIdx[s];
		float sum = 0;
		if (op == 0 && a_func == 0)
		{
			for (int i = smpBgn; i < smpEnd; i++)
			{
				int x = elementIdx[i];
				int cPos = (cMask == NULL ? x : cMask[x]);
				sum += cDeriv[cPos] * vec[d];
			}
		}
		else if (op == 0 && a_func == 1)
		{
			for (int i = smpBgn; i < smpEnd; i++)
			{
				int x = elementIdx[i];
				int cPos = (cMask == NULL ? x : cMask[x]);
				int aPos = (srcMask == NULL ? x : srcMask[x]) * dim;
				int bPos = (tgtMask == NULL ? x : tgtMask[x]) * dim;
				float v = tanhf(src[aPos + d] + tgt[bPos + d]);
				sum += cDeriv[cPos] * vec[d] * (1 - v) * (1 + v);
			}
		}
		else if (op == 0 && a_func == 2)
		{
			for (int i = smpBgn; i < smpEnd; i++)
			{
				int x = elementIdx[i];
				int cPos = (cMask == NULL ? x : cMask[x]);
				int aPos = (srcMask == NULL ? x : srcMask[x]) * dim;
				int bPos = (tgtMask == NULL ? x : tgtMask[x]) * dim;
				float v = (src[aPos + d] + tgt[bPos + d]);
				if (v < 0) v = 0;
				else v = 1;
				sum += cDeriv[cPos] * vec[d] * v;
			}
		}
		else if (op == 0 && a_func == 3)
		{
			for (int i = smpBgn; i < smpEnd; i++)
			{
				int x = elementIdx[i];
				int cPos = (cMask == NULL ? x : cMask[x]);
				int aPos = (srcMask == NULL ? x : srcMask[x]) * dim;
				int bPos = (tgtMask == NULL ? x : tgtMask[x]) * dim;
				float v = (src[aPos + d] + tgt[bPos + d]);
				v = tanhf(v / 2.0f);
				v = (v + 1.0f) / 2.0f;
				sum += cDeriv[cPos] * vec[d] * (1 - v) * v;
			}
		}
		else if (op == 1 && a_func == 0)
		{
			for (int i = smpBgn; i < smpEnd; i++)
			{
				int x = elementIdx[i];
				int cPos = (cMask == NULL ? x : cMask[x]);
				int aPos = (srcMask == NULL ? x : srcMask[x]) * dim;
				int bPos = (tgtMask == NULL ? x : tgtMask[x]) * dim;
				sum += cDeriv[cPos] * vec[d] * tgt[bPos + d];
			}
		}
		else if (op == 1 && a_func == 1)
		{
			for (int i = smpBgn; i < smpEnd; i++)
			{
				int x = elementIdx[i];
				int cPos = (cMask == NULL ? x : cMask[x]);
				int aPos = (srcMask == NULL ? x : srcMask[x]) * dim;
				int bPos = (tgtMask == NULL ? x : tgtMask[x]) * dim;
				float v = tanhf(src[aPos + d] * tgt[bPos + d]);
				sum += cDeriv[cPos] * vec[d] * (1 - v) * (1 + v) * tgt[bPos + d];
			}
		}
		else if (op == 1 && a_func == 2)
		{
			for (int i = smpBgn; i < smpEnd; i++)
			{
				int x = elementIdx[i];
				int cPos = (cMask == NULL ? x : cMask[x]);
				int aPos = (srcMask == NULL ? x : srcMask[x]) * dim;
				int bPos = (tgtMask == NULL ? x : tgtMask[x]) * dim;
				float v = (src[aPos + d] * tgt[bPos + d]);
				if (v < 0) v = 0;
				else v = 1;
				sum += cDeriv[cPos] * vec[d] * v * tgt[bPos + d];
			}
		}
		else if (op == 1 && a_func == 3)
		{
			for (int i = smpBgn; i < smpEnd; i++)
			{
				int x = elementIdx[i];
				int cPos = (cMask == NULL ? x : cMask[x]);
				int aPos = (srcMask == NULL ? x : srcMask[x]) * dim;
				int bPos = (tgtMask == NULL ? x : tgtMask[x]) * dim;
				float v = (src[aPos + d] * tgt[bPos + d]);
				v = tanhf(v / 2.0f);
				v = (v + 1.0f) / 2.0f;
				sum += cDeriv[cPos] * vec[d] * (1 - v) * v * tgt[bPos + d];
			}
		}


		int m = elementIdx[smpBgn];
		int srcPos = (srcMask == NULL ? m : srcMask[m]) * dim;
		srcDeriv[srcPos + d] = srcwei * srcDeriv[srcPos + d] + sum;
	}
}

void Cuda_DerivSoftAttention_Matching(float * a, float * aDeriv, int * aMask, float * b, float * bDeriv, int * bMask, int dim, int a_func, int op, float * vec, float * vecDeriv,
	float * c, float * cDeriv, int * cMask, int * aSmpIdx, int * aElementIdx, int aSize, int * bSmpIdx, int * bElementIdx, int bSize, int batchSize, float awei, float bwei, float vwei)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nDimBlockPerGrid = (dim + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	//(float * a, int * aMask, float * b, int * bMask, int dim, int a_func, int op, float * vecDeriv, float * cDeriv, int * cMask, int batchSize, float vwei)
	cuda_DerivSoftAttention_Vec << <nDimBlockPerGrid, nThreadPerBlock >> > (a, aMask, b, bMask, dim, a_func, op, vecDeriv, cDeriv, cMask, batchSize, vwei);

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 src_block_tail((aSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	dim3 tgt_block_tail((bSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	if (aDeriv != NULL)
	{
		cuda_DerivSoftAttention_Src << <src_block_tail, thread_tail >> > (aSmpIdx, aElementIdx, aSize, a, aDeriv, aMask,
			b, bMask, dim, a_func, op, vec, cDeriv, cMask, awei);
	}

	if (bDeriv != NULL)
	{
		cuda_DerivSoftAttention_Src << <tgt_block_tail, thread_tail >> > (bSmpIdx, bElementIdx, bSize, b, bDeriv, bMask,
			a, aMask, dim, a_func, op, vec, cDeriv, cMask, bwei);
	}
}

__global__ void cuda_DerivSoftAttention_VecV2(float * a, float * b, int dim, int a_func, int op, float * vecDeriv, float * cDeriv, int aBatchSize, int bBatchSize, float vwei)
{
	int d = blockDim.x * blockIdx.x + threadIdx.x;
	if (d < dim)
	{
		float sum = 0;

		if (op == 0) // addition.
		{
			if (a_func == 0) //linear.
			{
				for (int x = 0; x < aBatchSize; x++)
				{
					int aPos = x * dim + d;
					for (int y = 0; y < bBatchSize; y++)
					{
						int bPos = y * dim + d;
						int cPos = x * bBatchSize + y;
						float v = a[aPos] + b[bPos];
						sum += cDeriv[cPos] * v;
					}
				}
			}
			else if (a_func == 1) //tanh.
			{
				for (int x = 0; x < aBatchSize; x++)
				{
					int aPos = x * dim + d;
					for (int y = 0; y < bBatchSize; y++)
					{
						int bPos = y * dim + d;
						int cPos = x * bBatchSize + y;
						float v = tanhf(a[aPos] + b[bPos]);
						sum += cDeriv[cPos] * v;
					}
				}
			}
			else if (a_func == 2)//rectified.
			{
				for (int x = 0; x < aBatchSize; x++)
				{
					int aPos = x * dim + d;
					for (int y = 0; y < bBatchSize; y++)
					{
						int bPos = y * dim + d;
						int cPos = x * bBatchSize + y;
						float v = a[aPos] + b[bPos];
						if (v < 0) v = 0;
						sum += cDeriv[cPos] * v;
					}
				}
			}
			else if (a_func == 3)//sigmoid.
			{
				for (int x = 0; x < aBatchSize; x++)
				{
					int aPos = x * dim + d;
					for (int y = 0; y < bBatchSize; y++)
					{
						int bPos = y * dim + d;
						int cPos = x * bBatchSize + y;
						float v = a[aPos] + b[bPos];
						v = tanhf(v / 2.0f);
						v = (v + 1.0f) / 2.0f;
						sum += cDeriv[cPos] * v;
					}
				}
			}
		}
		else if (op == 1)
		{
			if (a_func == 0) //linear.
			{
				for (int x = 0; x < aBatchSize; x++)
				{
					int aPos = x * dim + d;
					for (int y = 0; y < bBatchSize; y++)
					{
						int bPos = y * dim + d;
						int cPos = x * bBatchSize + y;
						float v = a[aPos] * b[bPos];
						sum += cDeriv[cPos] * v;
					}
				}
			}
			else if (a_func == 1) //tanh.
			{
				for (int x = 0; x < aBatchSize; x++)
				{
					int aPos = x * dim + d;
					for (int y = 0; y < bBatchSize; y++)
					{
						int bPos = y * dim + d;
						int cPos = x * bBatchSize + y;
						float v = tanhf(a[aPos] * b[bPos]);
						sum += cDeriv[cPos] * v;
					}
				}
			}
			else if (a_func == 2)//rectified.
			{
				for (int x = 0; x < aBatchSize; x++)
				{
					int aPos = x * dim + d;
					for (int y = 0; y < bBatchSize; y++)
					{
						int bPos = y * dim + d;
						int cPos = x * bBatchSize + y;
						float v = a[aPos] * b[bPos];
						if (v < 0) v = 0;
						sum += cDeriv[cPos] * v;
					}
				}
			}
			else if (a_func == 3)//sigmoid.
			{
				for (int x = 0; x < aBatchSize; x++)
				{
					int aPos = x * dim + d;
					for (int y = 0; y < bBatchSize; y++)
					{
						int bPos = y * dim + d;
						int cPos = x * bBatchSize + y;
						float v = a[aPos] * b[bPos];
						v = tanhf(v / 2.0f);
						v = (v + 1.0f) / 2.0f;
						sum += cDeriv[cPos] * v;
					}
				}
			}
		}
		vecDeriv[d] = vwei * vecDeriv[d] + sum;
	}
}

__global__ void cuda_DerivSoftAttention_SrcV2(float * src, float * srcDeriv, 
	float * tgt, int dim, int a_func, int op, float * vec, float * cDeriv, int srcBatchSize, int tgtBatchSize, float srcwei)
{
	int s = blockDim.x * blockIdx.x + threadIdx.x;
	int d = blockDim.y * blockIdx.y + threadIdx.y;

	if (s < srcBatchSize && d < dim)
	{
		float sum = 0;
		if (op == 0 && a_func == 0)
		{
			for (int i = 0; i < tgtBatchSize; i++)
			{
				int cPos = s * tgtBatchSize + i;
				sum += cDeriv[cPos] * vec[d];
			}
		}
		else if (op == 0 && a_func == 1)
		{
			for (int i = 0; i < tgtBatchSize; i++)
			{
				int cPos = s * tgtBatchSize + i;
				int aPos = s * dim;
				int bPos = i * dim;
				float v = tanhf(src[aPos + d] + tgt[bPos + d]);
				sum += cDeriv[cPos] * vec[d] * (1 - v) * (1 + v);
			}
		}
		else if (op == 0 && a_func == 2)
		{
			for (int i = 0; i < tgtBatchSize; i++)
			{
				int cPos = s * tgtBatchSize + i;
				int aPos = s * dim;
				int bPos = i * dim;
				float v = (src[aPos + d] + tgt[bPos + d]);
				if (v < 0) v = 0;
				else v = 1;
				sum += cDeriv[cPos] * vec[d] * v;
			}
		}
		else if (op == 0 && a_func == 3)
		{
			for (int i = 0; i < tgtBatchSize; i++)
			{
				int cPos = s * tgtBatchSize + i;
				int aPos = s * dim;
				int bPos = i * dim;
				float v = (src[aPos + d] + tgt[bPos + d]);
				v = tanhf(v / 2.0f);
				v = (v + 1.0f) / 2.0f;
				sum += cDeriv[cPos] * vec[d] * (1 - v) * v;
			}
		}
		else if (op == 1 && a_func == 0)
		{
			for (int i = 0; i < tgtBatchSize; i++)
			{
				int cPos = s * tgtBatchSize + i;
				int aPos = s * dim;
				int bPos = i * dim;
				sum += cDeriv[cPos] * vec[d] * tgt[bPos + d];
			}
		}
		else if (op == 1 && a_func == 1)
		{
			for (int i = 0; i < tgtBatchSize; i++)
			{
				int cPos = s * tgtBatchSize + i;
				int aPos = s * dim;
				int bPos = i * dim;
				float v = tanhf(src[aPos + d] * tgt[bPos + d]);
				sum += cDeriv[cPos] * vec[d] * (1 - v) * (1 + v) * tgt[bPos + d];
			}
		}
		else if (op == 1 && a_func == 2)
		{
			for (int i = 0; i < tgtBatchSize; i++)
			{
				int cPos = s * tgtBatchSize + i;
				int aPos = s * dim;
				int bPos = i * dim;
				float v = (src[aPos + d] * tgt[bPos + d]);
				if (v < 0) v = 0;
				else v = 1;
				sum += cDeriv[cPos] * vec[d] * v * tgt[bPos + d];
			}
		}
		else if (op == 1 && a_func == 3)
		{
			for (int i = 0; i < tgtBatchSize; i++)
			{
				int cPos = s * tgtBatchSize + i;
				int aPos = s * dim;
				int bPos = i * dim;
				float v = (src[aPos + d] * tgt[bPos + d]);
				v = tanhf(v / 2.0f);
				v = (v + 1.0f) / 2.0f;
				sum += cDeriv[cPos] * vec[d] * (1 - v) * v * tgt[bPos + d];
			}
		}

		int srcPos = s * dim;
		srcDeriv[srcPos + d] = srcwei * srcDeriv[srcPos + d] + sum;
	}
}

__global__ void cuda_DerivSoftAttention_TgtV2(float * src, 
	float * tgt, float * tgtDeriv, int dim, int a_func, int op, float * vec, float * cDeriv, int srcBatchSize, int tgtBatchSize, float srcwei)
{
	int t = blockDim.x * blockIdx.x + threadIdx.x;
	int d = blockDim.y * blockIdx.y + threadIdx.y;

	if (t < tgtBatchSize && d < dim)
	{
		float sum = 0;
		if (op == 0 && a_func == 0)
		{
			for (int s = 0; s < srcBatchSize; s++)
			{
				int cPos = s * tgtBatchSize + t;
				sum += cDeriv[cPos] * vec[d];
			}
		}
		else if (op == 0 && a_func == 1)
		{
			for (int s = 0; s < srcBatchSize; s++)
			{
				int cPos = s * tgtBatchSize + t;
				int aPos = s * dim;
				int bPos = t * dim;
				float v = tanhf(src[aPos + d] + tgt[bPos + d]);
				sum += cDeriv[cPos] * vec[d] * (1 - v) * (1 + v);
			}
		}
		else if (op == 0 && a_func == 2)
		{
			for (int s = 0; s < srcBatchSize; s++)
			{
				int cPos = s * tgtBatchSize + t;
				int aPos = s * dim;
				int bPos = t * dim;
				float v = (src[aPos + d] + tgt[bPos + d]);
				if (v < 0) v = 0;
				else v = 1;
				sum += cDeriv[cPos] * vec[d] * v;
			}
		}
		else if (op == 0 && a_func == 3)
		{
			for (int s = 0; s < srcBatchSize; s++)
			{
				int cPos = s * tgtBatchSize + t;
				int aPos = s * dim;
				int bPos = t * dim;
				float v = (src[aPos + d] + tgt[bPos + d]);
				v = tanhf(v / 2.0f);
				v = (v + 1.0f) / 2.0f;
				sum += cDeriv[cPos] * vec[d] * (1 - v) * v;
			}
		}
		else if (op == 1 && a_func == 0)
		{
			for (int s = 0; s < srcBatchSize; s++)
			{
				int cPos = s * tgtBatchSize + t;
				int aPos = s * dim;
				int bPos = t * dim;
				sum += cDeriv[cPos] * vec[d] * tgt[bPos + d];
			}
		}
		else if (op == 1 && a_func == 1)
		{
			for (int s = 0; s < srcBatchSize; s++)
			{
				int cPos = s * tgtBatchSize + t;
				int aPos = s * dim;
				int bPos = t * dim;
				float v = tanhf(src[aPos + d] * tgt[bPos + d]);
				sum += cDeriv[cPos] * vec[d] * (1 - v) * (1 + v) * tgt[bPos + d];
			}
		}
		else if (op == 1 && a_func == 2)
		{
			for (int s = 0; s < srcBatchSize; s++)
			{
				int cPos = s * tgtBatchSize + t;
				int aPos = s * dim;
				int bPos = t * dim;
				float v = (src[aPos + d] * tgt[bPos + d]);
				if (v < 0) v = 0;
				else v = 1;
				sum += cDeriv[cPos] * vec[d] * v * tgt[bPos + d];
			}
		}
		else if (op == 1 && a_func == 3)
		{
			for (int s = 0; s < srcBatchSize; s++)
			{
				int cPos = s * tgtBatchSize + t;
				int aPos = s * dim;
				int bPos = t * dim;
				float v = (src[aPos + d] * tgt[bPos + d]);
				v = tanhf(v / 2.0f);
				v = (v + 1.0f) / 2.0f;
				sum += cDeriv[cPos] * vec[d] * (1 - v) * v * tgt[bPos + d];
			}
		}

		int tgtPos = t * dim;
		tgtDeriv[tgtPos + d] = srcwei * tgtDeriv[tgtPos + d] + sum;
	}
}

void Cuda_DerivSoftAttention(float * a, float * aDeriv, float * b, float * bDeriv, int dim, int a_func, int op, float * vec, float * vecDeriv,
	float * c, float * cDeriv, int aBatchSize, int bBatchSize, float awei, float bwei, float vwei)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nDimBlockPerGrid = (dim + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	//float * a, float * b, int dim, int a_func, int op, float * vecDeriv, float * cDeriv, int aBatchSize, int bBatchSize, float vwei)
	cuda_DerivSoftAttention_VecV2 << <nDimBlockPerGrid, nThreadPerBlock >> > (a, b, dim, a_func, op, vecDeriv, cDeriv, aBatchSize, bBatchSize, vwei);

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 src_block_tail((aBatchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	dim3 tgt_block_tail((bBatchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	//float * src, float * srcDeriv, float * tgt, int dim, int a_func, int op, float * vec, float * cDeriv, int srcBatchSize, int tgtBatchSize, float srcwei
	cuda_DerivSoftAttention_SrcV2 << <src_block_tail, thread_tail >> > (a, aDeriv, b, dim, a_func, op, vec, cDeriv, aBatchSize, bBatchSize, awei);

	//float * src, float * tgt, float * tgtDeriv, int dim, int a_func, int op, float * vec, float * cDeriv, int srcBatchSize, int tgtBatchSize, float srcwei)
	cuda_DerivSoftAttention_TgtV2 << <tgt_block_tail, thread_tail >> > (a, b, bDeriv, dim, a_func, op, vec, cDeriv, aBatchSize, bBatchSize, bwei);
}


/// b_row = b_row + a
__global__ void cuda_Matrix_AddVector(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < dimension && idy < batchsize)
	{
		gpu_floats_a[idy * dimension + idx] = gpu_floats_a[idy * dimension + idx] + gpu_floats_b[idx];
	}
}
void Cuda_Matrix_AddVector(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_Matrix_AddVector << <block_tail, thread_tail >> >(gpu_floats_a, gpu_floats_b, batchsize, dimension);
}

/// b_row = b_row + a
__global__ void cuda_Matrix_AddVector(float ** gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < dimension && idy < batchsize)
	{
		gpu_floats_a[idy][idx] = gpu_floats_a[idy][idx] + gpu_floats_b[idx];
	}
}
void Cuda_Matrix_AddVector(float ** gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_Matrix_AddVector << <block_tail, thread_tail >> >(gpu_floats_a, gpu_floats_b, batchsize, dimension);
}


/***********************************************/
// Rowwise sum. given matrix in ROW MAJOR ORDER
// result = result + wei * sum_i(matrix_i*)
/**********************************************/
__global__ void cuda_ColumnWiseSum(float **matrix, float * result, int row, int col, float wei)
{
	uint32_t idx = threadIdx.x;
	__shared__ float sum_value[64];
	int colIndex = blockIdx.x;
	sum_value[idx] = 0;
	for (int i = idx; i<row; i += 64)
	{
		sum_value[idx] += matrix[i][colIndex];
	}
	__syncthreads();
	if (idx == 0)
	{
		float sum = 0;
		for (int i = 0; i < 64; i++)
		{
			sum += sum_value[i];
		}
		result[colIndex] += wei * sum;
	}
}
__global__ void cuda_ColumnWiseSum(float *matrix, float * result, int row, int col, float wei)
{
	uint32_t idx = threadIdx.x;
	__shared__ float sum_value[64];
	int colIndex = blockIdx.x;

	sum_value[idx] = 0;
	for (int i = idx; i<row; i += 64)
	{
		sum_value[idx] += matrix[i * col + colIndex];
	}
	__syncthreads();
	if (idx == 0)
	{
		float sum = 0;
		for (int i = 0; i < 64; i++)
			sum += sum_value[i];
		result[colIndex] += wei * sum;
	}
}
__global__ void cuda_ColumnWiseSum(float * matrix, int * matrixMask, float * weight,
		int * smpIdx, int batchSize,
		float * output, int * outputMask,
		int row, int col,
		float alpha, float beta)
{
	uint32_t batchIndex = blockIdx.y;
	int colIndex = blockIdx.x;

	uint32_t idx = 4 * threadIdx.y + threadIdx.x;

	int batchS = batchIndex == 0 ? 0 : smpIdx[batchIndex - 1];
	int batchE = smpIdx[batchIndex];
	int batchLen = batchE - batchS;

	__shared__ float sum_value[16];

	sum_value[idx] = 0;
	for (int i = idx; i<batchLen; i += 16)
	{
		int mp = matrixMask == NULL ? i + batchS : matrixMask[i + batchS];
		sum_value[idx] += matrix[mp * col + colIndex] * (weight == NULL ? 1 : weight[i + batchS]);
	}
	__syncthreads();
	if (idx == 0)
	{
		float sum = 0;
		for (int i = 0; i < 16; i++)
			sum += sum_value[i];
		int op = outputMask == NULL ? batchIndex : outputMask[batchIndex];
		output[op * col + colIndex] = alpha * output[op * col + colIndex]  + beta * sum;
	}
}

__global__ void cuda_ColumnWiseSumV2(float * matrix, int * matrixMask, float * weight,
		int * smpIdx, int batchSize,
		float * output, int * outputMask,
		int row, int col,
		float alpha, float beta)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < col && idy < batchSize)
	{
		int batchS = idy == 0 ? 0 : smpIdx[idy - 1];
		int batchE = smpIdx[idy];
		int batchLen = batchE - batchS;
		float sum = 0;
		for (int i = batchS; i<batchE; i ++)
		{
			int mp = matrixMask == NULL ? i : matrixMask[i];
			sum += matrix[mp * col + idx] * (weight == NULL ? 1 : weight[i]);
		}
		int op = outputMask == NULL ? idy : outputMask[idy];
		output[op * col + idx] = alpha * output[op * col + idx] + beta * sum;
	}
}

__global__ void cuda_ColumnWiseSumV3(float * matrix, int * matrixMask, float * weight,
	int skipMatrix,
	int * smpIdx, int batchSize,
	float * output, int * outputMask,
	int skipOutput,
	int row, int col,
	float alpha, float beta)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < col && idy < batchSize)
	{
		int batchS = idy == 0 ? 0 : smpIdx[idy - 1];
		int batchE = smpIdx[idy];
		int batchLen = batchE - batchS;
		float sum = 0;
		for (int i = batchS; i<batchE; i++)
		{
			int mp = matrixMask == NULL ? i : matrixMask[i];
			sum += matrix[mp * skipMatrix + idx] * (weight == NULL ? 1 : weight[i]);
		}
		int op = outputMask == NULL ? idy : outputMask[idy];
		output[op * skipOutput + idx] = alpha * output[op * skipOutput + idx] + beta * sum;
	}
}

__global__ void cuda_matrix_aggragate_weight(float * a, float * b, int batchsize, int m, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < m)
	{
		float sum = 0;
		for (int i = 0; i < batchsize; i++)
		{
			sum += a[i * m + idx]; //* alpha[alpha_index * BATCH_SIZE + i];
		}
		b[idx] = b[idx] + weight * sum;
	}
}

void Cuda_ColumnWiseSum(float **matrix, float * result, int row, int col, float wei)
{
	cuda_ColumnWiseSum << < col, 64 >> >(matrix, result, row, col, wei);
}
//result = result + wei * sum_i(matrix_i*)
void Cuda_ColumnWiseSum(float *matrix, float * result, int row, int col, float wei)
{
	cuda_ColumnWiseSum << < col, 64 >> >(matrix, result, row, col, wei);
}
//b = b + weight * sum_i(a_i*)
void cuda_Matrix_Aggragate_Weight(float * a, float * b, int batchsize, int m, float weight)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (m + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_matrix_aggragate_weight << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(a, b, batchsize, m, weight); //,kept, alpha, ntrial, BATCH_SIZE, alpha_index);
}

void Cuda_ColumnWiseSum(float * matrix, int * matrixMask, float * weight,
		int * smpIdx, int batchSize,
		float * output, int * outputMask,
		int row, int col,
		float alpha, float beta)
{

	dim3 thread_tail(4, 4);
	dim3 block_tail(col, batchSize);

	cuda_ColumnWiseSum << <block_tail, thread_tail >> >(matrix, matrixMask, weight, smpIdx, batchSize,
			output, outputMask, row, col, alpha, beta);

}

void Cuda_ColumnWiseSumV2(float * matrix, int * matrixMask, float * weight,
		int * smpIdx, int batchSize,
		float * output, int * outputMask,
		int row, int col,
		float alpha, float beta)
{

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((col + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_ColumnWiseSumV2 << <block_tail, thread_tail >> >(matrix, matrixMask, weight, smpIdx, batchSize,
			output, outputMask, row, col, alpha, beta);
}

void Cuda_ColumnWiseSumV3(float * matrix, int * matrixMask, float * weight,
	int skipMatrix,
	int * smpIdx, int batchSize,
	float * output, int * outputMask,
	int skipOutput,
	int row, int col,
	float alpha, float beta)
{

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((col + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_ColumnWiseSumV3 << <block_tail, thread_tail >> >(matrix, matrixMask, weight, skipMatrix, smpIdx, batchSize,
		output, outputMask, skipOutput, row, col, alpha, beta);
}

//result_i = left_(leftIdx_i) * right_(rightIdx_i)
__global__ void cuda_InnerProduct(float ** left, float * right, int leftRow, int rightRow, int dim, int * leftIdx, int * rightIdx, int wSum, float * result)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < wSum)
	{
		int lRow = leftIdx[idx];
		int rRow = rightIdx[idx];

		float * pLeft = left[lRow];
		float * pRight = right + rRow * dim;
		float sum = 0;
		for (int i = 0; i < dim; i++)
		{
			sum += (*pLeft) * (*pRight);
			pLeft++;
			pRight++;
		}
		result[idx] = sum;
	}
}
void Cuda_InnerProduct(float ** left, float * right, int leftRow, int rightRow, int dim, int * leftIdx, int * rightIdx, int wSum, float * result)
{
	cuda_InnerProduct << < (wSum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(left, right, leftRow, rightRow, dim, leftIdx, rightIdx, wSum, result);
}
__global__ void cuda_InnerProduct(float * left, float * right, int leftRow, int rightRow, int dim, int * leftIdx, int * rightIdx, int wSum, float * result)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < wSum)
	{
		int lRow = leftIdx[idx];
		int rRow = rightIdx[idx];

		float * pLeft = left + lRow * dim;
		float * pRight = right + rRow * dim;
		float sum = 0;
		for (int i = 0; i < dim; i++)
		{
			sum += (*pLeft) * (*pRight);
			pLeft++;
			pRight++;
		}
		result[idx] = sum;
	}
}
void Cuda_InnerProduct(float * left, float * right, int leftRow, int rightRow, int dim, int * leftIdx, int * rightIdx, int wSum, float * result)
{
	cuda_InnerProduct << < (wSum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(left, right, leftRow, rightRow, dim, leftIdx, rightIdx, wSum, result);
}



__global__ void cuda_add_vector(float * gpu_floats_a, float * gpu_float_b, int m, float awei, float bwei)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id < m)
	{
		gpu_floats_a[id] = gpu_floats_a[id] * awei + gpu_float_b[id] * bwei;
	}
}
/// a = a * awei + b * bwei;
void cuda_Add_Vector(float * gpu_floats_a, float * gpu_float_b, int m, float awei, float bwei)
{
	int RowNum = (m - 1) / COL_FOLD_NUM + 1;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_add_vector << <block_tail, thread_tail >> >(gpu_floats_a, gpu_float_b, m, awei, bwei);
}


__global__ void cuda_init_vector(float * gpu_floats_a, float b, int m, float awei)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id < m)
	{
		gpu_floats_a[id] = gpu_floats_a[id] * awei + b;
	}
}
/// a = a * awei + b * bwei;
void cuda_Init_Vector(float * gpu_floats_a, float b, int m, float awei)
{
	int RowNum = (m - 1) / COL_FOLD_NUM + 1;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_init_vector << <block_tail, thread_tail >> >(gpu_floats_a, b, m, awei);
}



__global__ void cuda_VectorAddition(float * src, int * srcIdx, int size, float * bias, float * dst)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < size)
	{
		dst[idx] = src[idx] + bias[srcIdx[idx]];
	}
}
/// dst_i = src_i + bias(srcIdx_i)
void Cuda_VectorAddition(float * src, int * srcIdx, int size, float * bias, float * dst)
{
	cuda_VectorAddition << < (size - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(src, srcIdx, size, bias, dst);
}


__global__ void cuda_VectorAdditionBatch(float * src, int sSize, int * srcIdx, float * bias, int bSize, int batchSize, float * dst)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < sSize * batchSize)
	{
		int sIdx = idx % sSize;
		int bIdx = idx / sSize;
		dst[idx] = src[idx] + bias[bIdx * bSize + srcIdx[sIdx]];
	}
}
/// dst_i = src_i + bias(i / batchSize * bSize + srcIdx_(i % batchSize))
void Cuda_VectorAdditionBatch(float * src, int sSize, int * srcIdx, float * bias, int bSize, int batchSize, float * dst)
{
	cuda_VectorAdditionBatch << < (sSize * batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(src, sSize, srcIdx, bias, bSize, batchSize, dst);
}


__global__ void cuda_VectorIndexMatrixMultiplication(float * srcVec, int * srcIdx, int * tgtIdx, int batchSize, float * srcMatrix, float ** dst, int dim, float wei)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < dim && idy < batchSize)
	{
		int srcId = srcIdx[idy];
		int tgtId = tgtIdx[idy];

		atomicAdd(dst[tgtId] + idx, srcVec[idy] * srcMatrix[srcId * dim + idx] * wei);
	}
}
/// dst_(tgtIdx_i) = dst_(tgtIdx_i) + wei * srcVec_i * srcMatrix_(srcIdx_i);
void Cuda_VectorIndexMatrixMultiplication(float * srcVec, int * srcIdx, int * tgtIdx, int batchSize, float * srcMatrix, float ** dst, int dim, float wei)
{
	dim3 thread_tail(64, 8);
	dim3 block_tail((dim + 64 - 1) / 64, (batchSize + 8 - 1) / 8);
	cuda_VectorIndexMatrixMultiplication << <block_tail, thread_tail >> >(srcVec, srcIdx, tgtIdx, batchSize, srcMatrix, dst, dim, wei);
}

__global__ void cuda_VectorIndexMatrixMultiplication(float * srcVec, int * srcIdx, int * tgtIdx, int batchSize, float ** srcMatrix, float * dst, int dim, float wei)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < dim && idy < batchSize)
	{
		int srcId = srcIdx[idy];
		int tgtId = tgtIdx[idy];
		atomicAdd(dst + tgtId * dim + idx, srcVec[idy] * srcMatrix[srcId][idx] * wei);
	}
}
/// dst_(tgtIdx_i) = dst_(tgtIdx_i) + wei * srcVec_i * srcMatrix_(srcIdx_i);
void Cuda_VectorIndexMatrixMultiplication(float * srcVec, int * srcIdx, int * tgtIdx, int batchSize, float ** srcMatrix, float * dst, int dim, float wei)
{
	dim3 thread_tail(64, 8);
	dim3 block_tail((dim + 64 - 1) / 64, (batchSize + 8 - 1) / 8);
	cuda_VectorIndexMatrixMultiplication << <block_tail, thread_tail >> >(srcVec, srcIdx, tgtIdx, batchSize, srcMatrix, dst, dim, wei);
}

__global__ void cuda_VectorIndexMatrixMultiplication(float * srcVec, int * srcIdx, int * tgtIdx, int batchSize, float * srcMatrix, float * dst, int dim, float wei)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < dim && idy < batchSize)
	{
		int srcId = srcIdx[idy];
		int tgtId = tgtIdx[idy];
		atomicAdd(dst + tgtId * dim + idx, srcVec[idy] * srcMatrix[srcId * dim + idx] * wei);
	}
}
/// dst_(tgtIdx_i) = dst_(tgtIdx_i) + wei * srcVec_i * srcMatrix_(srcIdx_i);
void Cuda_VectorIndexMatrixMultiplication(float * srcVec, int * srcIdx, int * tgtIdx, int batchSize, float * srcMatrix, float * dst, int dim, float wei)
{
	dim3 thread_tail(64, 8);
	dim3 block_tail((dim + 64 - 1) / 64, (batchSize + 8 - 1) / 8);
	cuda_VectorIndexMatrixMultiplication << <block_tail, thread_tail >> >(srcVec, srcIdx, tgtIdx, batchSize, srcMatrix, dst, dim, wei);
}


__global__ void cuda_VectorSparseAgg(float * srcVec, int * srcIdx, int batchSize, float * bias, float wei)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
		atomicAdd(bias + srcIdx[idx], srcVec[idx] * wei);
}
/// bias_(srcIdx_i) += wei * srcVec_i;
void Cuda_VectorSparseAgg(float * srcVec, int * srcIdx, int batchSize, float * bias, float wei)
{
	cuda_VectorSparseAgg << < (batchSize - 1) / 512 + 1, 512 >> >(srcVec, srcIdx, batchSize, bias, wei);
}


__global__ void cuda_matrix_multipy_weight(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, int inverse, float low_wei)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < n && idy < batchsize)
	{
		float sum = 0;
		if (inverse == 1)
		{
			float * d_iter = delta + (idy * m);
			float * w_iter = weight + (idx * m);
			float * d_end_pt = d_iter + m;
			while (d_iter < d_end_pt)
			{
				sum += (*d_iter++) * (*w_iter++);
			}
		}
		else
		{
			float * d_iter = delta + (idy * m);
			float * w_iter = weight + idx;
			float * d_end_pt = d_iter + m;
			while (d_iter < d_end_pt)
			{
				sum += (*d_iter++) * (*w_iter);
				w_iter += n;
			}
		}

		delta_low[idy * n + idx] = delta_low[idy * n + idx] * low_wei + sum;
	}
}
/// inverse == 0 : delta_low = delta_low * low_wei + delta * weight; 
///				   delta_low : row : batchsize; col : n;
///				   delta : row : batchsize; col : m;
///				   weight : row : n; col : m;
/// inverse == 1 : delta_low = delta_low * low_wei + delta * weight';
///				   delta_low : row : batchsize; col : n;
///				   delta : row : batchsize; col : m;
///				   weight : row : m; col : n;
void cuda_Matrix_Multipy_Weight(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, int inverse, float low_wei)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((n + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_matrix_multipy_weight << <block_tail, thread_tail >> >(delta, weight, delta_low, batchsize, m, n, inverse, low_wei);
}


__global__ void cuda_matrix_product_weight(float * a, float * b, float * c, int batchsize, int m, int n, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < n && idy < m)
	{
		int row = idy; // / n;
		int col = idx;// % n;
		float sum = 0;
		float *a_iter = a + row;
		float *b_iter = b + col;
		float *a_end_pt = a_iter + (m*batchsize);
		while (a_iter < a_end_pt)
		{
			sum += (*a_iter) * (*b_iter);
			a_iter += m;
			b_iter += n;
		}
		c[idy * n + idx] += weight * sum;
	}
}
/// c = c + weight * a' * b
/// c : row : m; col : n;
/// a : row : batchsize; col : m;
/// b : row : batchsize; col : n;
void cuda_Matrix_Product_Weight(float * a, float * b, float * c, int batchsize, int m, int n, float weight)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((n + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (m + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_matrix_product_weight << <block_tail, thread_tail >> >(a, b, c, batchsize, m, n, weight);
}


__global__ void cuda_sparse_matrix_transpose_multiply_INTEX_weight(int * Smp_Index, int batchsize, int * Fea_Index,
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < output_dimension && idy < batchsize)
	{
		int fea_end = Smp_Index[idy];
		int fea_begin = idy == 0 ? 0 : Smp_Index[idy - 1];
		for (int i = fea_begin; i < fea_end; ++i)
		{
			int fea_idx = Fea_Index[i];
			atomicAdd(mul_weight + fea_idx * output_dimension + idx, Fea_Value[i] * output[idy * output_dimension + idx] * weight);
			//mul_weight[fea_idx * output_dimension + idx] += Fea_Value[i] * output[idy * output_dimension + idx] * weight;
		}
	}
}
/// (Smp_Index, batchSize, Fea_Index, Fea_Value, elementSize) -> Sparse Matrix A
/// mul_weight = mul_weight + weight * A' * output;
void cuda_Sparse_Matrix_Transpose_Multiply_INTEX_Weight(int * Smp_Index, int batchsize, int * Fea_Index,
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, float weight)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_sparse_matrix_transpose_multiply_INTEX_weight << <block_tail, thread_tail >> >(Smp_Index, batchsize,
			Fea_Index, Fea_Value, elementsize, mul_weight, output, Feature_dimension, output_dimension, weight);
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// optimized version -- C=AB
//__global__ void cuda_matrix_multipy(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt)
//{
//	int idx = blockDim.x * blockIdx.x + threadIdx.x;
//	int idy = blockDim.y * blockIdx.y + threadIdx.y;
//	if (idx < rightColumnCnt && idy < leftRowCnt)
//	{
//		float sum = 0;
//
//		float * d_iter = pLeft + (idy * leftColumnCnt);
//		float * w_iter = pRight + idx;
//		float * d_end_pt = d_iter + leftColumnCnt;
//		while (d_iter < d_end_pt)
//		{
//			sum += (*d_iter++) * (*w_iter);
//			w_iter += rightColumnCnt;
//		}
//
//		pDst[idy * rightColumnCnt + idx] = sum;
//	}
//}


///Compared with native Cublas code. it is still slower than cublas_sgemm. 1024, 300, 300 : six times slower; 32 ,300 ,300 : same.
__global__ void cuda_matrix_multipy(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt)
{
	//@@ Insert code to implement matrix multiplication here
	__shared__ float ds_M[32][32];
	__shared__ float ds_N[32][32];
	int bx = blockIdx.x, by = blockIdx.y,
		tx = threadIdx.x, ty = threadIdx.y,
		Row = by * 32 + ty,
		Col = bx * 32 + tx;
	float Pvalue = 0;

	for (int m = 0; m < leftColumnCnt; m+=32){ // (leftColumnCnt - 1) / 32 + 1; ++m) {
		ds_M[ty][tx] = 0;
		if (Row < leftRowCnt && m + tx < leftColumnCnt)
			ds_M[ty][tx] = pLeft[Row*leftColumnCnt + m + tx];

		ds_N[ty][tx] = 0;
		if (Col < rightColumnCnt && m + ty < leftColumnCnt)
			ds_N[ty][tx] = pRight[(m + ty)*rightColumnCnt + Col];

		__syncthreads();
		for (int k = 0; k < 32; ++k)
			Pvalue += ds_M[ty][k] * ds_N[k][tx];
		__syncthreads();
	}
	if (Row < leftRowCnt && Col < rightColumnCnt)
		pDst[Row*rightColumnCnt + Col] = Pvalue;
	}

	__global__ void cuda_matrix_multipy_tr(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt)
	{

		int bx = blockIdx.x, by = blockIdx.y,
			tx = threadIdx.x, ty = threadIdx.y;

		int x = blockDim.x*blockIdx.x + threadIdx.x;
		int y = blockDim.y*blockIdx.y + threadIdx.y;

		float tmp = 0;
		__shared__ float tileA[32][32], tileB[32][32];

		for (int i = 0; i < rightColumnCnt; i += 32)
		{
			tileA[ty][tx] = 0;
			if (y<leftRowCnt && i + tx < rightColumnCnt)
			{
				tileA[ty][tx] = pLeft[y*rightColumnCnt + i + tx];
			}

			tileB[ty][tx] = 0;
			if (x < rightRowCnt && i + ty < rightColumnCnt)
			{
				tileB[ty][tx] = pRight[x*rightColumnCnt + i + ty];
			}

			__syncthreads();

			for (int j = 0; j < 32; j++)
			{
				tmp += tileA[ty][j] * tileB[j][tx];
			}

			__syncthreads();
		}

		if (x < rightRowCnt && y < leftRowCnt)
			pDst[y*rightRowCnt + x] = tmp;
	}

	__global__ void cuda_MatrixMultiplicationLeftTranspose(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weiDst, float wei)
	{
		int bx = blockIdx.x, by = blockIdx.y,
			tx = threadIdx.x, ty = threadIdx.y;

		int x = blockDim.x*blockIdx.x + threadIdx.x;
		int y = blockDim.y*blockIdx.y + threadIdx.y;
		float tmp = 0;
		__shared__ float tileA[32][32], tileB[32][32];

		for (int i = 0; i < leftRowCnt; i += 32)
		{
			tileA[ty][tx] = 0;
			if (i + tx < leftRowCnt && y < leftColumnCnt)
			{
				tileA[ty][tx] = pLeft[(i + tx) * leftColumnCnt + y]; // + threadIdx.x];
			}

			tileB[ty][tx] = 0;
			if (i + ty < leftRowCnt && x < rightColumnCnt)
			{
				tileB[ty][tx] = pRight[(i + ty) * rightColumnCnt + x]; // + threadIdx.y];
			}

			__syncthreads();

			for (int j = 0; j < 32; j++)
			{
				tmp += tileA[ty][j] * tileB[j][tx];
			}
			__syncthreads();
		}

		if (x < rightColumnCnt && y < leftColumnCnt)
		{
			pDst[y * rightColumnCnt + x] = pDst[y * rightColumnCnt + x] * weiDst + tmp * wei;
		}
	}


	__global__ void cuda_MatrixMultiplication(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weight)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < rightColumnCnt && idy < leftRowCnt)
		{
			float sum = 0;
			float * d_iter = pLeft + (idy * leftColumnCnt);
			float * w_iter = pRight + idx;
			float * d_end_pt = d_iter + leftColumnCnt;
			while (d_iter < d_end_pt)
			{
				sum += (*d_iter++) * (*w_iter);
				w_iter += rightColumnCnt;
			}
			pDst[idy * rightColumnCnt + idx] = pDst[idy * rightColumnCnt + idx] * weight + sum;
		}
	}
	// pDst = pDst * weight + pLeft * pRight;
	void Cuda_MatrixMultiplication(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weight)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((rightColumnCnt + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (leftRowCnt + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_MatrixMultiplication << <block_tail, thread_tail >> >(pLeft, pRight, pDst, leftRowCnt, leftColumnCnt, rightColumnCnt, weight);
	}

	__global__ void cuda_MatrixMultiplication_LeftRowMask(float * pLeft, int * pLeftRowMask, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weight)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < rightColumnCnt && idy < leftRowCnt)
		{
			float sum = 0;
			if (pLeftRowMask[idy] > 0)
			{
				float * d_iter = pLeft + (idy * leftColumnCnt);
				float * w_iter = pRight + idx;
				float * d_end_pt = d_iter + leftColumnCnt;
				while (d_iter < d_end_pt)
				{
					sum += (*d_iter++) * (*w_iter);
					w_iter += rightColumnCnt;
				}
			}
			pDst[idy * rightColumnCnt + idx] = pDst[idy * rightColumnCnt + idx] * weight + sum;
		}
	}
	/// pDst = pDst * weight + pLeftRowMask (diagonal matrix) * pLeft * pRight;
	void Cuda_MatrixMultiplication_LeftRowMask(float * pLeft, int * pLeftRowMask, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weight)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((rightColumnCnt + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (leftRowCnt + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_MatrixMultiplication_LeftRowMask << <block_tail, thread_tail >> >(pLeft, pLeftRowMask, pRight, pDst, leftRowCnt, leftColumnCnt, rightColumnCnt, weight);
	}

	__global__ void cuda_MatrixMultiplication(float ** pLeft, float * pRight, float ** pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weight)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < rightColumnCnt && idy < leftRowCnt)
		{
			float sum = 0;
			float * d_iter = pLeft[idy]; // + (idy * leftColumnCnt);
			float * w_iter = pRight + idx;
			float * d_end_pt = d_iter + leftColumnCnt;
			while (d_iter < d_end_pt)
			{
				sum += (*d_iter++) * (*w_iter);
				w_iter += rightColumnCnt;
			}
			pDst[idy][idx] = pDst[idy][idx] * weight + sum;
		}
	}
	/// pDst = pDst * weight + pLeft * pRight.
	void Cuda_MatrixMultiplication(float ** pLeft, float * pRight, float ** pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weight)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((rightColumnCnt + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (leftRowCnt + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_MatrixMultiplication << <block_tail, thread_tail >> >(pLeft, pRight, pDst, leftRowCnt, leftColumnCnt, rightColumnCnt, weight);
	}

	// pDst = pDst * weight + pLeft * pRight'
	__global__ void cuda_MatrixMultiplicationTranspose(float ** pLeft, float * pRight, float ** pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt, float weight)
	{
		int x = blockDim.x*blockIdx.x + threadIdx.x;
		int y = blockDim.y*blockIdx.y + threadIdx.y;
		float tmp = 0;
		__shared__ float tileA[32][32], tileB[32][32];

		float * pleft = pLeft[y]; // y*rightColumnCnt;
		float * pright = pRight + x * rightColumnCnt;

		for (int i = 0; i < rightColumnCnt; i += 32)
		{
			tileA[threadIdx.y][threadIdx.x] = 0;
			if (i + threadIdx.x < rightColumnCnt && y < leftRowCnt)
			{
				tileA[threadIdx.y][threadIdx.x] = pleft[i + threadIdx.x];
			}

			tileB[threadIdx.x][threadIdx.y] = 0;
			if (i + threadIdx.y < rightColumnCnt && x < rightRowCnt)
			{
				tileB[threadIdx.x][threadIdx.y] = pright[i + threadIdx.y];
			}

			__threadfence_block();
			__syncthreads();

			for (int j = 0; j < 32; j++)
			{
				tmp += tileA[threadIdx.y][j] * tileB[threadIdx.x][j];
			}
			__syncthreads();
		}

		if (x < rightRowCnt && y < leftRowCnt)
		{
			pDst[y][x] = pDst[y][x] * weight + tmp;
		}
	}
	// pDst = pDst * weight + pLeft * pRight'
	void Cuda_MatrixMultiplicationTranspose(float ** pLeft, float * pRight, float ** pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt, float weight)
	{
		dim3 thread_tail(32, 32);
		dim3 block_tail((rightRowCnt + 32 - 1) / 32, (leftRowCnt + 32 - 1) / 32);
		cuda_MatrixMultiplicationTranspose << <block_tail, thread_tail, 32 * 32 * 4 * 2 >> >(pLeft, pRight, pDst, leftRowCnt, rightColumnCnt, rightRowCnt, weight);
	}
	// pDst = pLeft * pRight'
	void Cuda_MatrixMultiplicationTranspose(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((rightRowCnt + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (leftRowCnt + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_matrix_multipy_tr << <block_tail, thread_tail, DEFAULT_THREAD_PER_DIM*DEFAULT_THREAD_PER_DIM * 4 * 2 >> >(pLeft, pRight, pDst, leftRowCnt, rightColumnCnt, rightRowCnt);
	}

	__global__ void cuda_MatrixMultiplicationTranspose(float ** pLeft, float * pRight, float * pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt, float weight)
	{
		int x = blockDim.x*blockIdx.x + threadIdx.x;
		int y = blockDim.y*blockIdx.y + threadIdx.y;
		float tmp = 0;
		__shared__ float tileA[32][32], tileB[32][32];

		float * pleft = pLeft[y]; // y*rightColumnCnt;
		float * pright = pRight + x * rightColumnCnt;

		for (int i = 0; i < rightColumnCnt; i += 32)
		{
			tileA[threadIdx.y][threadIdx.x] = 0;
			if (i + threadIdx.x < rightColumnCnt && y < leftRowCnt)
			{
				tileA[threadIdx.y][threadIdx.x] = pleft[i + threadIdx.x];
			}

			tileB[threadIdx.x][threadIdx.y] = 0;
			if (i + threadIdx.y < rightColumnCnt && x < rightRowCnt)
			{
				tileB[threadIdx.x][threadIdx.y] = pright[i + threadIdx.y];
			}

			__threadfence_block();
			__syncthreads();

			for (int j = 0; j < 32; j++)
			{
				tmp += tileA[threadIdx.y][j] * tileB[threadIdx.x][j];
			}
			__syncthreads();
		}

		if (x < rightRowCnt && y < leftRowCnt)
		{
			pDst[y * rightRowCnt + x] = pDst[y * rightRowCnt + x] * weight + tmp;
		}
	}
	// pDst = pDst * weight + pLeft * pRight'
	void Cuda_MatrixMultiplicationTranspose(float ** pLeft, float * pRight, float * pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt, float weight)
	{
		dim3 thread_tail(32, 32);
		dim3 block_tail((rightRowCnt + 32 - 1) / 32, (leftRowCnt + 32 - 1) / 32);
		cuda_MatrixMultiplicationTranspose << <block_tail, thread_tail, 32 * 32 * 4 * 2 >> >(pLeft, pRight, pDst, leftRowCnt, rightColumnCnt, rightRowCnt, weight);
	}


	// pDst = pDst * weiDst + pLeft' * pRight * wei
	__global__ void cuda_MatrixMultiplicationLeftTranspose(float ** pLeft, float ** pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weiDst, float wei)
	{
		int x = blockDim.x*blockIdx.x + threadIdx.x;
		int y = blockDim.y*blockIdx.y + threadIdx.y;
		float tmp = 0;
		__shared__ float tileA[32][32], tileB[32][32];

		for (int i = 0; i < leftRowCnt; i += 32)
		{
			tileA[threadIdx.x][threadIdx.y] = 0;
			if (i + threadIdx.x < leftRowCnt && y < leftColumnCnt)
			{
				tileA[threadIdx.x][threadIdx.y] = pLeft[i + threadIdx.x][y]; // + threadIdx.x];
			}

			tileB[threadIdx.y][threadIdx.x] = 0;
			if (i + threadIdx.y < leftRowCnt && x < rightColumnCnt)
			{
				tileB[threadIdx.y][threadIdx.x] = pRight[i + threadIdx.y][x]; // + threadIdx.y];
			}

			__threadfence_block();
			__syncthreads();

			for (int j = 0; j < 32; j++)
			{
				tmp += tileA[j][threadIdx.y] * tileB[j][threadIdx.x];
			}
			__syncthreads();
		}

		if (x < rightColumnCnt && y < leftColumnCnt)
		{
			pDst[y * rightColumnCnt + x] = pDst[y * rightColumnCnt + x] * weiDst + tmp * wei;
		}
	}
	// pDst = pDst * weiDst + pLeft' * pRight * wei
	void Cuda_MatrixMultiplicationLeftTranspose(float ** pLeft, float ** pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weiDst, float wei)
	{
		dim3 thread_tail(32, 32);
		dim3 block_tail((rightColumnCnt + 32 - 1) / 32, (leftColumnCnt + 32 - 1) / 32);
		cuda_MatrixMultiplicationLeftTranspose << <block_tail, thread_tail, 32 * 32 * 4 * 2 >> >(
				pLeft, pRight, pDst, leftRowCnt, leftColumnCnt, rightColumnCnt, weiDst, wei);
	}

	// pDst = pDst * weiDst + pLeft' * pRight * wei
	void Cuda_MatrixMultiplicationLeftTranspose(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weiDst, float wei)
	{
		dim3 thread_tail(32, 32);
		dim3 block_tail((rightColumnCnt + 32 - 1) / 32, (leftColumnCnt + 32 - 1) / 32);
		cuda_MatrixMultiplicationLeftTranspose << <block_tail, thread_tail, 32 * 32 * 4 * 2 >> >(
				pLeft, pRight, pDst, leftRowCnt, leftColumnCnt, rightColumnCnt, weiDst, wei);
	}



	void cuda_Matrix_Multipy(float * pLeft, float * pRight, float * pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int rightTransposed)
	{
		//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
		//int nBlockPerGrid = (batchsize * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

		dim3 thread_tail(32, 32);
		dim3 block_tail((dstColumnCnt + 32 - 1) / 32, (dstRowCnt + 32 - 1) / 32);

		if (rightTransposed)
		{
			cuda_matrix_multipy_tr << <block_tail, thread_tail, 32*32 * 4 * 2 >> >(pLeft, pRight, pDst, dstRowCnt, intermediateColumnCnt, dstColumnCnt);
		}
		else
		{
			cuda_matrix_multipy << <block_tail, thread_tail, 32*32 * 4 * 2 >> >(pLeft, pRight, pDst, dstRowCnt, intermediateColumnCnt, dstColumnCnt);
		}
	}


	__global__ void cuda_matrix_transpose_multiply_INTEX_weight(int * Smp_Index, int batchsize, int * Fea_Index,
			float * Fea_Value, int elementsize,
			float * mul_weight, float * output, int Feature_dimension, int output_dimension, float weight)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < output_dimension && idy < batchsize)
		{
			int fea_end = Smp_Index[idy];
			int fea_begin = idy == 0 ? 0 : Smp_Index[idy - 1];
			for (int i = fea_begin; i < fea_end; ++i)
			{
				int fea_idx = Fea_Index[i];
				atomicAdd(mul_weight + fea_idx * output_dimension + idx, Fea_Value[i] * output[idy * output_dimension + idx] * weight);
				//mul_weight[fea_idx * output_dimension + idx] += Fea_Value[i] * output[idy * output_dimension + idx] * weight;
			}
		}
	}

	void cuda_Matrix_Transpose_Multiply_INTEX_Weight(int * Smp_Index, int batchsize, int * Fea_Index,
			float * Fea_Value, int elementsize,
			float * mul_weight, float * output, int Feature_dimension, int output_dimension, float weight)
	{
		/*dim3 thread_tail(DEFAULT_THREAD_PER_DIM);
		  dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		 */
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_matrix_transpose_multiply_INTEX_weight << <block_tail, thread_tail >> >(Smp_Index, batchsize,
				Fea_Index, Fea_Value, elementsize, mul_weight, output, Feature_dimension, output_dimension, weight);
	}


	__global__ void cuda_tensor_matrix_multiply_weight(float * input, float * weight, float * output, int inputRow, int inputCol, int outputCol, int inverse, float wei)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx <  inputRow && idy < outputCol)
		{
			//int row = idy; // / n;
			//int col = idx; // % n;
			float sum = 0;
			if (inverse == 1)
			{
				/*float * d_iter = delta + (idy * m);
				  float * w_iter = weight + (idx * m);
				  float * d_end_pt = d_iter + m;
				  while (d_iter < d_end_pt)
				  {
				  sum += (*d_iter++) * (*w_iter++);
				  }*/
			}
			else
			{
				int inputIdx = idx * inputCol;
				int tensorIdx = idy * inputCol * inputCol;
				float * startI2 = weight + tensorIdx;
				///---> Will it be very very slow?? 
				for (int i = 0; i<inputCol; i++)
				{
					float cache1 = input[inputIdx + i];
					float * i1 = input + inputIdx;
					for (int j = 0; j<inputCol; j++)
					{
						sum += cache1 * (*i1++) * (*startI2++);
					}
				}
			}
			output[idx * outputCol + idy] = input[idx * outputCol + idy] * wei + sum;
		}
	}

	void cuda_Tensor_Matrix_Multiply_Weight(float * input, float * weight, float * output, int inputRow, int inputCol, int outputCol, int inverse, float wei)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((inputRow + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (outputCol + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

		cuda_tensor_matrix_multiply_weight << <block_tail, thread_tail >> >(input, weight, output, inputRow, inputCol, outputCol, inverse, wei);
	}

	__global__ void cuda_matrix_tensor_product_weight(float * input, float * deriv, float * gradient, int batchsize, int inputDim, int outputDim, float weight)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < inputDim * inputDim && idy < outputDim)
		{
			int col = idy;// % n;
			int row = idx; // / n;
			int inputI = idx / inputDim;
			int inputJ = idx % inputDim;

			float sum = 0;

			float *a1_iter = input + inputI;
			float *a2_iter = input + inputJ;
			float *b_iter = deriv + idy;
			for (int i = 0; i<batchsize; i++)
			{
				sum += (*a1_iter) * (*a2_iter) * (*b_iter);
				a1_iter += inputDim;
				a2_iter += inputDim;
				b_iter += outputDim;
			}
			gradient[idy * inputDim * inputDim + idx] += weight * sum;
		}
	}

	//Input.DenseFeatureData.CudaPtr, Output.Deriv.Data.CudaPtr, Model.WeightOptimizer.Gradient.CudaPtr,
	//                                    Input.BatchSize, Model.Neural_In, Model.Neural_Out, Model.WeightOptimizer.GradientStep);
	void cuda_Matrix_Tensor_Product_Weight(float * input, float * deriv, float * gradient, int batchsize, int inputDim, int outputDim, float weight)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((inputDim * inputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (outputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

		cuda_matrix_tensor_product_weight << <block_tail, thread_tail >> >(input, deriv, gradient, batchsize, inputDim, outputDim, weight);
		//, kept, alpha, ntrial, BATCH_SIZE, alpha_index);
	}







	__global__ void cuda_matrix_add_vector_selected(float * gpu_floats_a, 
			float * gpu_floats_b, int batchsize, int dimension,
			float * targets, int * leftIndex, int * classIndex, int * columnIndex)
	{
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		if (idx < dimension && idy < batchsize)
		{
			int t = (int)targets[idy];
			int c = leftIndex[t];

			int wordBegin = c == 0 ? 0 : classIndex[c - 1];
			int wordEnd = classIndex[c];

			int wordIdx = idx + wordBegin;
			if(wordIdx < wordEnd)
			{
				int widx = columnIndex[wordIdx];
				gpu_floats_a[idy * dimension + idx] += gpu_floats_b[widx];
			}
		}
	}

	__global__ void cuda_matrix_multipy_tr_selected(float * left, float * right, float * dst, int dstRow, int intermedDim, int dstCol, 
			float * targets, int * leftIndex, int * classIndex, int * columnIndex)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < dstCol && idy < dstRow)
		{
			int t = (int)targets[idy];
			int c = leftIndex[t];

			int wordBegin = c == 0 ? 0 : classIndex[c - 1];
			int wordEnd = classIndex[c];

			int wordIdx = idx + wordBegin;
			if(wordIdx < wordEnd)
			{
				int wIdx = columnIndex[wordIdx];

				float sum = 0;
				float * d_iter = left + (idy * intermedDim);
				float * w_iter = right + wIdx * intermedDim;
				float * d_end_pt = d_iter + intermedDim;
				while (d_iter < d_end_pt)
				{
					sum += (*d_iter++) * (*w_iter++);
				}
				dst[idy * dstCol + idx] = sum;
			}
			else
			{
				dst[idy * dstCol + idx] = 0;
			}
		}
	}

	__global__ void cuda_DerivSoftmax_selected(float * a, float * b, int labelDim, int batchsize, float gamma,
			float * targets, int * leftIndex, int * classIndex, int * columnIndex, int *wordColumnIdx, float * targetProb)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		if (idx < batchsize)
		{
			float log_sum = 0;
			int mIdx = idx * labelDim;

			int t = (int)targets[idx];
			int c = leftIndex[t];
			int wordBegin = c == 0 ? 0 : classIndex[c - 1];
			int wordEnd = classIndex[c];
			int wordLen = wordEnd - wordBegin;

			float tmpa = gamma * a[mIdx];
			log_sum = tmpa;

			for (int i = 1; i < wordLen; i++)
			{
				tmpa = gamma * a[mIdx + i];
				if (log_sum >= tmpa) log_sum = log_sum + logf(1 + expf((tmpa - log_sum)));
				else log_sum = tmpa + logf(1 + expf((log_sum - tmpa)));
			}

			for (int i = 0; i < wordLen; i++)
			{
				tmpa = gamma * a[mIdx + i];
				b[mIdx + i] =  - expf(tmpa - log_sum) * gamma;
			}

			targetProb[idx] = targetProb[idx] * (- b[mIdx + wordColumnIdx[t]] / gamma);
			b[mIdx + wordColumnIdx[t]] += gamma;
		}
	}

	__global__ void cuda_DerivSoftmax_class(float * outputScore, float * targets, int * outputLabel, float * outputDeriv, int dim, float gamma, int batchSize, float * targetProb)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		if (idx < batchSize)
		{
			int mIdx = idx * dim;
			float log_sum = gamma * outputScore[mIdx];
			for (int i = 1; i < dim; i++)
			{
				float tmpa = gamma * outputScore[mIdx + i];
				if (log_sum >= tmpa) log_sum = log_sum + __logf(1 + __expf((tmpa - log_sum)));
				else log_sum = tmpa + __logf(1 + __expf((log_sum - tmpa)));
			}

			for (int i = 0; i < dim; i++)
			{
				float tmpa = gamma * outputScore[mIdx + i];
				outputDeriv[mIdx + i] = - __expf(tmpa - log_sum) * gamma;
			}
			targetProb[idx] = -outputDeriv[mIdx + outputLabel[(int)targets[idx]]] / gamma;

			outputDeriv[mIdx + outputLabel[(int)targets[idx]]] += gamma;

		}
	}


	__global__ void cuda_matrix_multipy_weight_selected(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, float low_wei,
			float * targets, int * leftIndex, int * classIdx, int * columnIndex)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < n && idy < batchsize)
		{
			float sum = 0;

			int c = leftIndex[(int)targets[idy]];
			int wordEnd = classIdx[c];
			int wordBegin = c == 0 ? 0 : classIdx[c-1];

			float * d_iter = delta + (idy * m);
			float * w_iter = weight + idx;

			for(int i=wordBegin;i<wordEnd;i++)
			{
				w_iter = weight + columnIndex[i] * n + idx;
				sum += (*d_iter++) * (*w_iter);
			}

			delta_low[idy * n + idx] = delta_low[idy * n + idx] * low_wei + sum;
		}
	}

	__global__ void cuda_bias_aggragate_weight(float * a, float * b, int batchsize, int m, float weight)
		//int kept, float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		if (idx < m)
		{
			float sum = 0;
			for (int i = 0; i < batchsize; i++)
			{
				sum += a[i * m + idx]; //* alpha[alpha_index * BATCH_SIZE + i];
			}
			b[idx] = b[idx] + weight * sum;
		}
	}


	__global__ void cuda_bias_aggragate_weight_selected(float * a, float * b, int batchsize, int m, float weight,
			float * targets, int * leftIndex, int * classIdx, int * columnIndex)
		//int kept, float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;

		if (idx < m && idy < batchsize)
		{
			int c = leftIndex[(int)targets[idy]];
			int wordEnd = classIdx[c];
			int wordBegin = c == 0 ? 0 : classIdx[c-1];
			int wordIdx = idx + wordBegin;

			if(wordIdx < wordEnd)
			{
				atomicAdd(b + columnIndex[wordIdx],  weight * a[idy * m + idx]); 
			}
		}
	}

	__global__ void cuda_InnerProductSpace_update(float * a, float * b, float * c, int batchsize, int m, int n, float weight)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < n && idy < m)
		{
			int row = idy; // / n;
			int col = idx;// % n;
			float sum = 0;
			float *a_iter = a + row;
			float *b_iter = b + col;
			float *a_end_pt = a_iter + (m*batchsize);
			while (a_iter < a_end_pt)
			{
				sum += (*a_iter) * (*b_iter);
				a_iter += m;
				b_iter += n;
			}
			c[idy * n + idx] += weight * sum;
		}
	}

	__global__ void cuda_InnerProductSpace_update_selected(float * a, float * b, float * c, int batchsize, int m, int n, float weight,
			float * targets, int * leftIndex, int * classIdx, int * columnIndex)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < n && idy < m)
		{
			for(int i=0;i<batchsize;i++)
			{
				int p = leftIndex[(int)targets[i]];
				int wordEnd = classIdx[p];
				int wordBegin = p == 0 ? 0 : classIdx[p-1];
				int wordIdx = idy + wordBegin;
				if(wordIdx < wordEnd)
				{
					int wIdx = columnIndex[wordIdx];
					c[wIdx * n + idx] += weight * a[i * m + idy] * b[i * n + idx];
				}
			}

		}
	}


	/*********This function will need to call other __global__ functions ********/
	void Cuda_HierarcialSoftmaxEmbedding(float * targets, int targetNum, 
			float * embedding, float * deriv, int dim,
			float * vSpace, float * vBias, int vocabSize,
			float * cSpace, float * cBias, int cSize,
			float step,
			int * v2c, // word 2 class,
			int * classIdx, // class 2 word number
			int * wordIdx, // class 2 word index
			int * wordClassIdx, //word 2 class segment index.
			float gamma,
			float * classOutput,
			float * wordOutput,
			int wordSegSize, // word Max SegmentSize,
			float * targetProb
			)	// step size for update vSpace.
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 class_block_tail((cSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (targetNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		dim3 word_block_tail((wordSegSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (targetNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		dim3 embed_block_tail((dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (targetNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

		dim3 cSpace_block_tail((dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (cSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		dim3 vSpace_block_tail((dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (wordSegSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);


		/**********Phase 1 softmax on classes.*****************/
		Cuda_MatrixMultiplicationTranspose(embedding, cSpace, classOutput, targetNum, dim, cSize);

		Cuda_Matrix_AddVector(classOutput, cBias, targetNum, cSize);

		cuda_DerivSoftmax_class<<< (targetNum - 1) / 8 + 1, 8>>>(
				classOutput, targets, v2c, classOutput, cSize, gamma, targetNum, targetProb);
		/**********Phase 1*****************/

		/**********Phase 2 softmax on words*****************/
		cuda_matrix_multipy_tr_selected <<<word_block_tail, thread_tail>> >(
				embedding, vSpace, wordOutput, targetNum, dim, wordSegSize,
				targets, v2c, classIdx, wordIdx);

		cuda_matrix_add_vector_selected<<<word_block_tail, thread_tail>> >(
				wordOutput, vBias, targetNum, wordSegSize, targets, v2c, classIdx, wordIdx);

		cuda_DerivSoftmax_selected<<< (targetNum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK>>>(
				wordOutput, wordOutput, wordSegSize, targetNum, gamma,
				targets, v2c, classIdx, wordIdx, wordClassIdx, targetProb);
		/***********Phase 2****************/

		///backpropagate from class to embedding.
		cuda_matrix_multipy_weight<<<embed_block_tail,  thread_tail>>>(classOutput, cSpace, deriv, targetNum,  cSize, dim, 0, 0);

		//backpropagate from words to embedding.
		cuda_matrix_multipy_weight_selected<<<embed_block_tail,  thread_tail>>>(wordOutput, vSpace, deriv, targetNum, wordSegSize, dim, 1,
				targets, v2c, classIdx, wordIdx);

		/**********Update Class Bias and Class Space*************/
		cuda_bias_aggragate_weight<<<(cSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK>>>(
				classOutput, cBias, targetNum, cSize, step);

		cuda_InnerProductSpace_update<<< cSpace_block_tail,thread_tail>>>(classOutput, embedding, cSpace, targetNum, cSize, dim, step);

		/**********Update Word Bias and Word Space*************/
		cuda_bias_aggragate_weight_selected<<<word_block_tail, thread_tail>>>(
				wordOutput, vBias, targetNum, wordSegSize, step,
				targets, v2c, classIdx, wordIdx);

		cuda_InnerProductSpace_update_selected<<<vSpace_block_tail, thread_tail>>>(
				wordOutput, embedding, vSpace, targetNum, wordSegSize, dim, step,
				targets, v2c, classIdx, wordIdx);
	}


	void Cuda_HierarcicalProbability(float * targets, int targetNum, 
			float * embedding, int dim,
			float * vSpace, float * vBias, int vocabSize,
			float * cSpace, float * cBias, int cSize,

			int * v2c, // word 2 class,
			int * classIdx, // class 2 word number
			int * wordIdx, // class 2 word index
			int * wordClassIdx, //word 2 class segment index.
			float gamma,
			float * classOutput,
			float * wordOutput,
			int wordSegSize, // word Max SegmentSize,
			float * targetProb
			)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 class_block_tail((cSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (targetNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

		dim3 word_block_tail((wordSegSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (targetNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		dim3 embed_block_tail((dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (targetNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

		dim3 cSpace_block_tail((dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (cSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		dim3 vSpace_block_tail((dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (wordSegSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);


		/**********Phase 1 softmax on classes.*****************/
		cuda_matrix_multipy_tr <<<class_block_tail, thread_tail, DEFAULT_THREAD_PER_DIM*DEFAULT_THREAD_PER_DIM * 4 * 2 >> >(
				embedding, cSpace, classOutput, targetNum, dim, cSize);

		Cuda_Matrix_AddVector(classOutput, cBias, targetNum, cSize);

		cuda_DerivSoftmax_class<<< (targetNum - 1) / 8 + 1, 8>>>(
				classOutput, targets, v2c, classOutput, cSize, gamma, targetNum, targetProb);
		/**********Phase 1*****************/

		/**********Phase 2 softmax on words*****************/
		cuda_matrix_multipy_tr_selected <<<word_block_tail, thread_tail>> >(
				embedding, vSpace, wordOutput, targetNum, dim, wordSegSize,
				targets, v2c, classIdx, wordIdx);

		cuda_matrix_add_vector_selected<<<word_block_tail, thread_tail>> >(
				wordOutput, vBias, targetNum, wordSegSize, targets, v2c, classIdx, wordIdx);

		cuda_DerivSoftmax_selected<<< (targetNum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK>>>(
				wordOutput, wordOutput, wordSegSize, targetNum, gamma,
				targets, v2c, classIdx, wordIdx, wordClassIdx, targetProb);
		/***********Phase 2****************/
	}


	/// c_column_i = b_columnm_i + a
	__global__ void cuda_MatrixTran_AddVector(float * gpu_floats_a, float * gpu_floats_b, float * gpu_floats_c, int batchsize, int dimension, float weight)
	{
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		int idx = blockDim.x * blockIdx.x + threadIdx.x;

		if (idx < dimension && idy < batchsize)
		{
			gpu_floats_c[idy * dimension + idx] = gpu_floats_a[idy * dimension + idx] + gpu_floats_b[idy] * weight;
		}
	}
	void Cuda_MatrixTran_AddVector(float * gpu_floats_a, float * gpu_floats_b, float * gpu_floats_c, int batchsize, int dimension, float weight)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_MatrixTran_AddVector << <block_tail, thread_tail >> >(gpu_floats_a, gpu_floats_b, gpu_floats_c, batchsize, dimension, weight);
	}



	/// Obtain the last state of seqMatrix.
	__global__ void cuda_CalculateLastMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int dim, float * matrix)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < batchSize && idy < dim)
		{
			int instanceBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
			int instanceEnd = InstanceIndex[idx];
			if (instanceEnd > instanceBegin) matrix[idx * dim + idy] = SeqMatrixs[(instanceEnd - 1) * dim + idy];
			else matrix[idx * dim + idy] = 0;
		}
	}
	__global__ void cuda_CalculateLastMatrixs(int * InstanceIndex, int * seqTransIdx, int batchSize, float * SeqMatrixs, int dim, float * matrix, float * matrixMask)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < batchSize && idy < dim)
		{
			int instanceBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
			int instanceEnd = InstanceIndex[idx];

			int oIdx = matrixMask == NULL ? idx : (int)matrixMask[idx];
			if (instanceEnd > instanceBegin) matrix[oIdx * dim + idy] = SeqMatrixs[seqTransIdx[instanceEnd - 1] * dim + idy];
			else matrix[oIdx * dim + idy] = 0;
		}
	}
	void Cuda_CalculateLastMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int dim, float * matrix)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_CalculateLastMatrixs << <block_tail, thread_tail >> >(InstanceIndex, batchSize, SeqMatrixs, dim, matrix);
	}
	void Cuda_CalculateLastMatrixs(int * InstanceIndex, int * seqTransIdx, int batchSize, float * SeqMatrixs, int dim, float * matrix, float * matrixMask)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_CalculateLastMatrixs << <block_tail, thread_tail >> >(InstanceIndex, seqTransIdx, batchSize, SeqMatrixs, dim, matrix, matrixMask);
	}

	/// Set the last state of seqMatrix.
	__global__ void cuda_DerivCalculateLastMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int dim, float * matrix, float alpha)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < batchSize && idy < dim)
		{
			int instanceBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
			int instanceEnd = InstanceIndex[idx];
			if (instanceEnd > instanceBegin)
			{
				SeqMatrixs[(instanceEnd - 1) * dim + idy] = SeqMatrixs[(instanceEnd - 1) * dim + idy] * alpha + matrix[idx * dim + idy];
			}
		}
	}
	__global__ void cuda_DerivCalculateLastMatrixs(int * InstanceIndex, int * seqTransIdx, int batchSize, float * SeqMatrixs, int dim, float * matrix, float * matrixMask, float alpha)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < batchSize && idy < dim)
		{
			int iIdx = matrixMask == NULL ? idx : (int)matrixMask[idx];
			int instanceBegin = iIdx == 0 ? 0 : InstanceIndex[iIdx - 1];
			int instanceEnd = InstanceIndex[iIdx];
			if (instanceEnd > instanceBegin)
			{
				SeqMatrixs[seqTransIdx[instanceEnd - 1] * dim + idy] = SeqMatrixs[seqTransIdx[instanceEnd - 1] * dim + idy] * alpha + matrix[idx * dim + idy];
			}
		}
	}
	void Cuda_DerivCalculateLastMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int dim, float * matrix, float alpha)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_DerivCalculateLastMatrixs << <block_tail, thread_tail >> >(InstanceIndex, batchSize, SeqMatrixs, dim, matrix, alpha);
	}
	void Cuda_DerivCalculateLastMatrixs(int * InstanceIndex, int * seqTransIdx, int batchSize, float * SeqMatrixs, int dim, float * matrix, float * matrixMask, float alpha)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_DerivCalculateLastMatrixs << <block_tail, thread_tail >> >(InstanceIndex, seqTransIdx, batchSize, SeqMatrixs, dim, matrix, matrixMask, alpha);
	}

	/// Obtain the seq order of seqMatrix.
	__global__ void cuda_GetSeqOrderMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int * mapForward, int dim, int isReverse, int order, float * matrix, float alpha, float beta)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < batchSize && idy < dim)
		{
			int instanceBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
			int instanceEnd = InstanceIndex[idx] - 1;
			int pos = -1;
			if(isReverse == 0)
			{
				pos = instanceBegin + order;
			}
			else
			{
				pos = instanceEnd - order;
			}

			if (pos >= instanceBegin && pos <= instanceEnd)
			{
				int newPos = mapForward == NULL ? pos : mapForward[pos];
				matrix[idx * dim + idy] = alpha * matrix[idx * dim + idy] + beta * SeqMatrixs[newPos * dim + idy];
			}
			else matrix[idx * dim + idy] = 0;
		}
	}
	void Cuda_GetSeqOrderMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int * mapForward, int dim, int isReverse, int order, float * matrix, float alpha, float beta)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_GetSeqOrderMatrixs << <block_tail, thread_tail >> >(InstanceIndex, batchSize, SeqMatrixs, mapForward, dim, isReverse, order, matrix, alpha, beta);
	}
		/// Obtain the seq order of seqMatrix.
	__global__ void cuda_SetSeqOrderMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int * mapForward, int dim, int isReverse, int order, float * matrix, float alpha, float beta)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < batchSize && idy < dim)
		{
			int instanceBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
			int instanceEnd = InstanceIndex[idx] - 1;
			int pos = -1;
			if(isReverse == 0)
			{
				pos = instanceBegin + order;
			}
			else
			{
				pos = instanceEnd - order;
			}

			if (pos >= instanceBegin && pos <= instanceEnd) 
			{
				int newPos = mapForward == NULL ? pos : mapForward[pos];
				SeqMatrixs[newPos * dim + idy] = alpha * SeqMatrixs[newPos * dim + idy] + beta * matrix[idx * dim + idy];
			}
		}
	}
	void Cuda_SetSeqOrderMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int * mapForward, int dim, int isReverse, int order, float * matrix, float alpha, float beta)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_SetSeqOrderMatrixs << <block_tail, thread_tail >> >(InstanceIndex, batchSize, SeqMatrixs, mapForward, dim, isReverse, order, matrix, alpha, beta);
	}


	__global__ void cuda_GetWindowMatrixs(int * InstanceMargin, int sentsize, float * SeqMatrixs, int * mapForward, int dim,
		int winsize, float * matrix, float alpha, float beta)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < sentsize && idy < dim)
		{
			int half_win = winsize / 2;

			int smpidx = InstanceMargin[idx];
			for (int i = -half_win; i < winsize - half_win; i++)
			{
				int curidx = idx + i;
				matrix[idx * (winsize * dim) + (i + half_win) * dim + idy] = alpha * matrix[idx * (winsize * dim) + (i + half_win) * dim + idy];

				if (curidx >= 0 && curidx < sentsize && InstanceMargin[curidx] == smpidx)
				{
					int newPos = mapForward == NULL ? curidx : mapForward[curidx];
					matrix[idx * (winsize * dim) + (i + half_win) * dim + idy] += beta * SeqMatrixs[newPos * dim + idy];
				}
			}

		}
	}
	void Cuda_GetWindowMatrixs(int * InstanceMargin, int sentsize, float * SeqMatrixs, int * mapForward, int dim,
		int winsize, float * matrix, float alpha, float beta)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((sentsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_GetWindowMatrixs << <block_tail, thread_tail >> >(InstanceMargin, sentsize, SeqMatrixs, mapForward, dim, winsize, matrix, alpha, beta);
	}

	__global__ void cuda_SetWindowMatrixs(int * InstanceMargin, int sentsize, float * SeqMatrixs, int * mapForward, int dim,
		int winsize, float * matrix, float alpha, float beta)
	{
		int idx = blockDim.x * blockIdx.x + threadIdx.x;
		int idy = blockDim.y * blockIdx.y + threadIdx.y;
		if (idx < sentsize && idy < dim)
		{
			int half_win = winsize / 2;

			float sum = 0;
			int smpidx = InstanceMargin[idx];
			for (int i = -half_win; i < winsize - half_win; i++)
			{
				int curidx = idx + i;
				//matrix[idx * (winsize * dim) + (i + half_win) * dim + idy] = alpha * matrix[idx * (winsize * dim) + (i + half_win) * dim + idy];

				if (curidx >= 0 && curidx < sentsize && InstanceMargin[curidx] == smpidx)
				{
					//int newPos = mapForward == NULL ? curidx : mapForward[curidx];
					sum += matrix[curidx * (winsize * dim) + (winsize - 1 - (i + half_win)) * dim + idy];
					//matrix[idx * (winsize * dim) + (i + half_win) * dim + idy] += beta * SeqMatrixs[newPos * dim + idy];
				}
			}

			int newPos = mapForward == NULL ? idx : mapForward[idx];
			SeqMatrixs[newPos * dim + idy] = alpha * SeqMatrixs[newPos * dim + idy] + beta * sum;
		}
	}
	void Cuda_SetWindowMatrixs(int * InstanceMargin, int sentsize, float * SeqMatrixs, int * mapForward, int dim,
		int winsize, float * matrix, float alpha, float beta)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((sentsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_SetWindowMatrixs << <block_tail, thread_tail >> >(InstanceMargin, sentsize, SeqMatrixs, mapForward, dim, winsize, matrix, alpha, beta);
	}


	__global__ void cuda_ElementwiseProduct(float ** pLeft, float ** pRight, float ** pDst, int row, int column, float weiDst)
	{
		int x = blockDim.x*blockIdx.x + threadIdx.x;
		int y = blockDim.y*blockIdx.y + threadIdx.y;
		if (x < column && y < row)
			pDst[y][x] = weiDst * pDst[y][x] + pLeft[y][x] * pRight[y][x];
	}
	__global__ void cuda_ElementwiseProduct(float * pLeft, float * pRight, float * pDst, int row, int column, float weiDst)
	{
		int x = blockDim.x*blockIdx.x + threadIdx.x;
		int y = blockDim.y*blockIdx.y + threadIdx.y;

		if (x < column && y < row)
		{
			int idx = y * column + x;
			pDst[idx] = weiDst * pDst[idx] + pLeft[idx] * pRight[idx];
		}
	}
	__global__ void cuda_ElementwiseProduct(float * pLeft, float * pRight, float * pDst,
			int * leftmask, int * rightmask, int * dstmask, int row, int column, float alpha, float beta)
	{
		int x = blockDim.x*blockIdx.x + threadIdx.x;
		int y = blockDim.y*blockIdx.y + threadIdx.y;
		if (x < column && y < row)
		{
			int rightIdx = (rightmask == NULL ? y : rightmask[y]) * column + x;
			int leftIdx = (leftmask == NULL ? y : leftmask[y]) * column + x;
			int dstIdx = (dstmask == NULL ? y : dstmask[y]) * column + x;
			pDst[dstIdx] = alpha * pDst[dstIdx] + beta * pLeft[leftIdx] * pRight[rightIdx];
		}
	}
	__global__ void cuda_AccurateElementwiseProduct(float * pLeft, float * pRight, float * pDst,
		int * leftmask, int * rightmask, int * dstmask, int row, int column, float beta)
	{
		int x = blockDim.x*blockIdx.x + threadIdx.x;
		int y = blockDim.y*blockIdx.y + threadIdx.y;
		if (x < column && y < row)
		{
			int rightIdx = (rightmask == NULL ? y : rightmask[y]) * column + x;
			int leftIdx = (leftmask == NULL ? y : leftmask[y]) * column + x;
			int dstIdx = (dstmask == NULL ? y : dstmask[y]) * column + x;
			atomicAdd(pDst + dstIdx, beta * pLeft[leftIdx] * pRight[rightIdx]);
		}
	}

	// pDst = pDst * weiDst + pLeft * pRight;
	void Cuda_ElementwiseProduct(float ** pLeft, float ** pRight, float ** pDst, int row, int column, float weiDst)
	{
		dim3 thread_tail(32, 32);
		dim3 block_tail((column + 32 - 1) / 32, (row + 32 - 1) / 32);
		cuda_ElementwiseProduct << <block_tail, thread_tail >> >(pLeft, pRight, pDst, row, column, weiDst);
	}
	// pDst = pDst * weiDst + pLeft * pRight;
	int Cuda_ElementwiseProduct(float * pLeft, float * pRight, float * pDst, int row, int column, float weiDst)
	{
		dim3 thread_tail(32, 32);
		dim3 block_tail((column + 32 - 1) / 32, (row + 32 - 1) / 32);
		//printf("hello word here");
		cuda_ElementwiseProduct << <block_tail, thread_tail >> >(pLeft, pRight, pDst, row, column, weiDst);
		return 1;
	}
	// pDst = pDst * alpha + pLeft * pRight * beta;
	void Cuda_ElementwiseProduct(float * pLeft, float * pRight, float * pDst,
			int * leftmask, int * rightmask, int * dstmask, int row, int column, float alpha, float beta)
	{
		dim3 thread_tail(32, 32);
		dim3 block_tail((column + 32 - 1) / 32, (row + 32 - 1) / 32);
		cuda_ElementwiseProduct << <block_tail, thread_tail >> >(pLeft, pRight, pDst, leftmask, rightmask, dstmask, row, column, alpha, beta);
	}
	void Cuda_AccurateElementwiseProduct(float * pLeft, float * pRight, float * pDst,
		int * leftmask, int * rightmask, int * dstmask, int row, int column, float beta)
	{
		dim3 thread_tail(32, 32);
		dim3 block_tail((column + 32 - 1) / 32, (row + 32 - 1) / 32);
		cuda_AccurateElementwiseProduct << <block_tail, thread_tail >> >(pLeft, pRight, pDst, leftmask, rightmask, dstmask, row, column, beta);
	}

	///Compared with native Cublas code. it is still slower than cublas_sgemm. 1024, 300, 300 : six times slower; 32 ,300 ,300 : same.
	__global__ void cuda_matrix_forward(float * pLeft, float * pRight, float * pDst, int batchsize, int inDim, int outDim,
			int * aMask, int * bMask, int * cMask, float alpha, float beta)
	{
		//@@ Insert code to implement matrix multiplication here
		__shared__ float ds_M[32][32];
		__shared__ float ds_N[32][32];
		int bx = blockIdx.x, by = blockIdx.y,
			tx = threadIdx.x, ty = threadIdx.y,
			Row = by * 32 + ty,
			Col = bx * 32 + tx;
		float Pvalue = 0;

		pLeft = pLeft + ( (aMask == NULL || Row >= batchsize) ? Row : aMask[Row]) * inDim;
		pDst = pDst + ((cMask == NULL || Row >= batchsize) ? Row : cMask[Row]) * outDim;

		for (int m = 0; m < inDim; m += 32){ // (leftColumnCnt - 1) / 32 + 1; ++m) {
			ds_M[ty][tx] = 0;
			if (Row < batchsize && m + tx < inDim)
				ds_M[ty][tx] = pLeft[m + tx];

			ds_N[ty][tx] = 0;
			if (Col < outDim && m + ty < inDim)
				ds_N[ty][tx] = pRight[(bMask == NULL ? m + ty : bMask[m + ty]) * outDim + Col];
			__syncthreads();
			for (int k = 0; k < 32; ++k)
				Pvalue += ds_M[ty][k] * ds_N[k][tx];
			__syncthreads();
		}
		if (Row < batchsize && Col < outDim)
			pDst[Col] = pDst[Col] * alpha + Pvalue * beta;
		}
		__global__ void cuda_matrix_backward_data(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt,
				int * aMask, int * bMask, int * cMask, float alpha, float beta)
		{
			int bx = blockIdx.x, by = blockIdx.y,
			tx = threadIdx.x, ty = threadIdx.y;

			int x = blockDim.x*blockIdx.x + threadIdx.x;
			int y = blockDim.y*blockIdx.y + threadIdx.y;

			float tmp = 0;
			__shared__ float tileA[32][32], tileB[32][32];

			pLeft = pLeft + ((aMask == NULL || y >= leftRowCnt) ? y : aMask[y]) * rightColumnCnt;

			pRight = pRight + ((bMask == NULL || x >= rightRowCnt) ? x : bMask[x]) * rightColumnCnt;

			pDst = pDst + ((cMask == NULL || y >= leftRowCnt) ? y : cMask[y]) * rightRowCnt;

			for (int i = 0; i < rightColumnCnt; i += 32)
			{
				tileA[ty][tx] = 0;
				if (y<leftRowCnt && i + tx < rightColumnCnt)
					tileA[ty][tx] = pLeft[i + tx];

				tileB[ty][tx] = 0;
				if (x < rightRowCnt && i + ty < rightColumnCnt)
					tileB[ty][tx] = pRight[i + ty];

				__syncthreads();
				for (int j = 0; j < 32; j++)
				{
					tmp += tileA[ty][j] * tileB[j][tx];
				}
				__syncthreads();
			}

			if (x < rightRowCnt && y < leftRowCnt)
				pDst[x] = alpha * pDst[x] + beta * tmp;
		}
		__global__ void cuda_matrix_backward_weight(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, 
				int * aMask, int * bMask, int * cMask, float alpha, float beta)
		{
			int bx = blockIdx.x, by = blockIdx.y,
			tx = threadIdx.x, ty = threadIdx.y;

			int x = blockDim.x*blockIdx.x + threadIdx.x;
			int y = blockDim.y*blockIdx.y + threadIdx.y;
			float tmp = 0;
			__shared__ float tileA[32][32], tileB[32][32];

			pDst = pDst + ((cMask == NULL || y >= leftColumnCnt) ? y : cMask[y]) * rightColumnCnt;

			for (int i = 0; i < leftRowCnt; i += 32)
			{
				tileA[ty][tx] = 0;
				if (i + tx < leftRowCnt && y < leftColumnCnt)
				{
					tileA[ty][tx] = pLeft[(aMask == NULL ? i + tx : aMask[i + tx]) * leftColumnCnt + y];
				}

				tileB[ty][tx] = 0;
				if (i + ty < leftRowCnt && x < rightColumnCnt)
				{
					tileB[ty][tx] = pRight[(bMask == NULL ? i + ty : bMask[i + ty]) * rightColumnCnt + x]; 
				}

				__syncthreads();
				for (int j = 0; j < 32; j++)
					tmp += tileA[ty][j] * tileB[j][tx];
				__syncthreads();
			}

			if (x < rightColumnCnt && y < leftColumnCnt)
				pDst[x] = pDst[x] * alpha + tmp * beta;
		}
		void Cuda_SgemmMask(float * A, float * B, float * C, int batchsize, int m, int n, 
				int * aMask, int * bMask, int * cMask, float alpha, float beta, bool transA, bool transB)
		{
			dim3 thread_tail(32, 32);

			if (!transA && !transB)
			{
				dim3 block_forward((n + 32 - 1) / 32, (batchsize + 32 - 1) / 32);
				cuda_matrix_forward << <block_forward, thread_tail, 32 * 32 * 4 * 2 >> >(A, B, C, batchsize, m, n,
						aMask, bMask, cMask, alpha, beta);
				/// c = alpha A * B + beta C
				//Matrix_Multipy
				//cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, batchsize, m, &alpha, B, n, A, m, &beta, C, n);
			}
			else if (!transA && transB)
			{
				dim3 block_backward((n + 32 - 1) / 32, (batchsize + 32 - 1) / 32);
				cuda_matrix_backward_data << <block_backward, thread_tail, 32 * 32 * 4 * 2 >> >(A, B, C, batchsize, m, n,
						aMask, bMask, cMask, alpha, beta);
				/// c = alpha op(A) * B + beta C
				//cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, n, batchsize, m, &alpha, B, m, A, m, &beta, C, n);
			}
			else if (transA && !transB)
			{
				dim3 block_weight((n + 32 - 1) / 32, (m + 32 - 1) / 32);
				cuda_matrix_backward_weight << <block_weight, thread_tail, 32 * 32 * 4 * 2 >> >(
						A, B, C, batchsize, m, n, aMask, bMask, cMask, alpha, beta);
				/// c = alpha A * op(B) + beta C
				//cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, m, batchsize, &alpha, B, n, A, m, &beta, C, n);
			}
		}

		/// bMask is not supported yet.
		__global__ void cuda_sparse_matrix_forward(int * smpIdx, int * feaIndex, float * feaValue, float * weight, float * output, int batchSize, int m, int n,
				int * aMask, int * bMask, int * cMask, float alpha, float beta)
		{
			int idx = blockDim.x * blockIdx.x + threadIdx.x;
			int idy = blockDim.y * blockIdx.y + threadIdx.y;
			if (idx < batchSize && idy < n)
			{
				int aIdx = aMask == NULL ? idx : aMask[idx];
				int cIdx = cMask == NULL ? idx : cMask[idx];
				int col_end = smpIdx[aIdx];
				int col_begin = aIdx == 0 ? 0 : smpIdx[aIdx - 1];

				float sum = 0;
				for (int i = col_begin; i < col_end; ++i)
				{
					sum += (feaValue == NULL ? 1 : feaValue[i])* weight[feaIndex[i] * n + idy];
				}
				output[cIdx * n + idy] = alpha * output[cIdx * n + idy] + beta * sum;
			}
		}
		/// cMask is not supported yet.
		__global__ void cuda_sparse_matrix_backward_weight(int * smpIdx, int * feaIndex, float * feaValue, float * output, float * mul_weight, int batchSize, int m, int n,
				int * aMask, int * bMask, int * cMask, float alpha, float beta)
		{
			int idx = blockDim.x * blockIdx.x + threadIdx.x;
			if (idx < n)
			{
				for (int sample = 0; sample < batchSize; ++sample)
				{
					int Asmp = aMask == NULL ? sample : aMask[sample];
					int Bsmp = bMask == NULL ? sample : bMask[sample];
					int col_end = smpIdx[Asmp];
					int col_begin = Asmp == 0 ? 0 : smpIdx[Asmp - 1];
					float sum = 0;
					for (int i = col_begin; i < col_end; ++i)
					{
						int fea_idx = feaIndex[i];
						mul_weight[fea_idx * n + idx] = mul_weight[fea_idx * n + idx] * alpha + (feaValue == NULL ? 1 : feaValue[i]) * output[Bsmp * n + idx] * beta;
					}
				}
			}
		}
		__global__ void cuda_sparse_matrix_backward_weight_v2(int * smpIdx, int * feaIndex, float * feaValue, float * output, float * mul_weight, int batchSize, int m, int n,
				int * aMask, int * bMask, int * cMask, float alpha, float beta)
		{
			int idx = blockDim.x * blockIdx.x + threadIdx.x;
			int idy = blockDim.y * blockIdx.y + threadIdx.y;
			if (idx < n && idy < batchSize)
			{
				int Asmp = aMask == NULL ? idy : aMask[idy];
				int Bsmp = bMask == NULL ? idy : bMask[idy];

				int fea_end = smpIdx[Asmp];
				int fea_begin = Asmp == 0 ? 0 : smpIdx[Asmp - 1];
				for (int i = fea_begin; i < fea_end; ++i)
				{
					int fea_idx = feaIndex[i];
					atomicAdd(mul_weight + fea_idx * n + idx, (feaValue == NULL ? 1 : feaValue[i]) * output[Bsmp * n + idx] * beta);
				}
			}
		}
		void Cuda_SparseSgemmMask(int * AIndex, int * AFeaIndex, float * AFeaValue, float * B, float * C, int batchsize, int m, int n,
				int * aMask, int * bMask, int * cMask, float alpha, float beta, bool transA, bool transB)
		{
			if (!transA && !transB)
			{
				dim3 thread_forward(32, 32);
				dim3 block_forward((batchsize + 32 - 1) / 32, (n + 32 - 1) / 32);
				cuda_sparse_matrix_forward << <block_forward, thread_forward >> >(AIndex, AFeaIndex, AFeaValue, B, C, batchsize, m, n,
						aMask, bMask, cMask, alpha, beta);
			}
			else if (transA && !transB)
			{
				//dim3 thread_backward(256);
				//dim3 block_weight((n + 256 - 1) / 256); // (m + 32 - 1) / 32);
				//cuda_sparse_matrix_backward_weight << <block_weight, thread_backward >> >(
				//	AIndex, AFeaIndex, AFeaValue, B, C, batchsize, m, n, aMask, bMask, cMask, alpha, beta);

				dim3 thread_backward(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
				dim3 block_weight((n + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
				cuda_sparse_matrix_backward_weight_v2 << <block_weight, thread_backward >> >(
						AIndex, AFeaIndex, AFeaValue, B, C, batchsize, m, n, aMask, bMask, cMask, alpha, beta);

				/// c = alpha A * op(B) + beta C
				//cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, m, batchsize, &alpha, B, n, A, m, &beta, C, n);
			}
		}

		__global__ void cuda_NDArrayTranspose(float * input, int * indim, int * transDim, float * output, int dimNum, int length, float alpha, float beta)
		{
			int idx = blockDim.x * blockIdx.x + threadIdx.x;
			if (idx < length)
			{
				int c = idx;
				int newc = 0;
				for (int d = dimNum - 1; d >= 0; d--)
				{
					int pk = c % indim[d];
					newc = newc + pk * transDim[d];
					c = c / indim[d];
				}
				output[newc] = alpha * output[newc] + beta * input[idx];
			}
		}
		void Cuda_NDArrayTranspose(float * input, int * indim, int * transDim, float * output, int dimNum, int length, float alpha, float beta)
		{
			cuda_NDArrayTranspose << <(length - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(input, indim, transDim, output, dimNum, length, alpha, beta);
		}
