#pragma once

#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>

#include "cublas_v2.h"
#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
#endif

/// b = b * weight + a * M;
/// M : row : inputSize; column : outputSize;
void Cuda_VectorProjection(float * a, float * M, float * b, int inputSize, int outputSize, float weight);

/// b = b * weight + M * a;
/// M : row : outputSize; column : inputSize;
void Cuda_VectorProjectionTranspose(float * a, float * M, float * b, int inputSize, int outputSize, float weight);

/// pDst = pDst * weight + pLeft * pRight.
void Cuda_MatrixMultiplication(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weight);

/// pDst = pDst * weight + pLeftRowMask (diagonal matrix) * pLeft * pRight;
void Cuda_MatrixMultiplication_LeftRowMask(float * pLeft, int * pLeftRowMask, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weight);

/// pDst = pDst * weight + pLeft * pRight.
void Cuda_MatrixMultiplication(float ** pLeft, float * pRight, float ** pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weight);

// pDst = pDst * weight + pLeft * pRight';
void Cuda_MatrixMultiplicationTranspose(float ** pLeft, float * pRight, float ** pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt, float weight);
void Cuda_MatrixMultiplicationTranspose(float ** pLeft, float * pRight, float * pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt, float weight);
// pDst = pLeft * pRight'
void Cuda_MatrixMultiplicationTranspose(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt);

// pDst = pDst * weiDst + pLeft' * pRight * wei
void Cuda_MatrixMultiplicationLeftTranspose(float ** pLeft, float ** pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weiDst, float wei);
void Cuda_MatrixMultiplicationLeftTranspose(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weiDst, float wei);

// pDst = pDst * weiDst + pLeft * pRight;
void Cuda_ElementwiseProduct(float ** pLeft, float ** pRight, float ** pDst, int row, int column, float weiDst);
int Cuda_ElementwiseProduct(float * pLeft, float * pRight, float * pDst, int row, int column, float weiDst);
void Cuda_ElementwiseProduct(float * pLeft, float * pRight, float * pDst, int * leftmask, int * rightmask, int * dstmask, int row, int column, float alpha, float beta);
// pDst = pDst * weiDst + pLeft * weiLeft + pRight * weiRight;
void Cuda_Matrix_Add(float ** pLeft, float ** pRight, float ** pDst, int row, int column, float weiLeft, float weiRight, float weiDst);
void Cuda_Matrix_Add(float * pLeft, float * pRight, float * pDst, int row, int column, float weiLeft, float weiRight, float weiDst);
void Cuda_Matrix_Add(float * a, int * aMask, float * b, int * bMask, float * c, int * cMask, int dim, int batchSize, float awei, float bwei, float cwei);


/// b_row = b_row + a
void Cuda_Matrix_AddVector(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension);
void Cuda_Matrix_AddVector(float ** gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension);

/// result = result + wei * sum_i(matrix_i*)
void Cuda_ColumnWiseSum(float **matrix, float * result, int row, int col, float wei);
void Cuda_ColumnWiseSum(float *matrix, float * result, int row, int col, float wei);
void Cuda_ColumnWiseSum(float * matrix, int * matrixMask, float * weight,
	int * smpIdx, int batchSize,
	float * output, int * outputMask,
	int row, int col,
	float alpha, float beta);
void Cuda_ColumnWiseSumV2(float * matrix, int * matrixMask, float * weight,
	int * smpIdx, int batchSize,
	float * output, int * outputMask,
	int row, int col,
	float alpha, float beta);
void Cuda_ColumnWiseSumV3(float * matrix, int * matrixMask, float * weight,
	int skipMatrix,
	int * smpIdx, int batchSize,
	float * output, int * outputMask,
	int skipOutput,
	int row, int col,
	float alpha, float beta);

/// result = result + wei * sum_i(matrix_i*)
/// It just uses a different name. the same functionality as Cuda_ColumnWiseSum;
void cuda_Matrix_Aggragate_Weight(float * a, float * b, int batchsize, int m, float weight);

//result_i = left_(leftIdx_i) * right_(rightIdx_i)
void Cuda_InnerProduct(float ** left, float * right, int leftRow, int rightRow, int dim, int * leftIdx, int * rightIdx, int wSum, float * result);
void Cuda_InnerProduct(float * left, float * right, int leftRow, int rightRow, int dim, int * leftIdx, int * rightIdx, int wSum, float * result);

/// dst_i = src_i + bias(srcIdx_i)
void Cuda_VectorAddition(float * src, int * srcIdx, int size, float * bias, float * dst);

/// dst_i = src_i + bias(i / batchSize * bSize + srcIdx_(i % batchSize))
void Cuda_VectorAdditionBatch(float * src, int sSize, int * srcIdx, float * bias, int bSize, int batchSize, float * dst);

/// dst_(tgtIdx_i) = dst_(tgtIdx_i) + wei * srcVec_i * srcMatrix_(srcIdx_i);
void Cuda_VectorIndexMatrixMultiplication(float * srcVec, int * srcIdx, int * tgtIdx, int batchSize, float * srcMatrix, float ** dst, int dim, float wei);
void Cuda_VectorIndexMatrixMultiplication(float * srcVec, int * srcIdx, int * tgtIdx, int batchSize, float ** srcMatrix, float * dst, int dim, float wei);
void Cuda_VectorIndexMatrixMultiplication(float * srcVec, int * srcIdx, int * tgtIdx, int batchSize, float * srcMatrix, float * dst, int dim, float wei);

/// bias_(srcIdx_i) += wei * srcVec_i;
void Cuda_VectorSparseAgg(float * srcVec, int * srcIdx, int batchSize, float * bias, float wei);

/// matrix = last state of SeqMatrix.
void Cuda_CalculateLastMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int dim, float * matrix);
void Cuda_CalculateLastMatrixs(int * InstanceIndex, int * seqTransIdx, int batchSize, float * SeqMatrixs, int dim, float * matrix, float * matrixMask);
void Cuda_DerivCalculateLastMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int dim, float * matrix, float alpha);
void Cuda_DerivCalculateLastMatrixs(int * InstanceIndex, int * seqTransIdx, int batchSize, float * SeqMatrixs, int dim, float * matrix, float * matrixMask, float alpha);

/// 
void cuda_Matrix_Product_Weight(float * a, float * b, float * c, int batchsize, int m, int n, float weight);

void cuda_Sparse_Matrix_Transpose_Multiply_INTEX_Weight(int * Smp_Index, int batchsize, int * Fea_Index,
	float * Fea_Value, int elementsize,
	float * mul_weight, float * output, int Feature_dimension, int output_dimension, float weight);


void cuda_Matrix_Multipy_Weight(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, int inverse, float low_wei);

void cuda_Matrix_Multipy(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, int inverse);

void cuda_Tensor_Matrix_Multiply_Weight(float * input, float * weight, float * output, int inputRow, int inputCol, int outputCol, int inverse, float wei);

void cuda_Matrix_Tensor_Product_Weight(float * input, float * deriv, float * gradient, int batchsize, int inputDim, int outputDim, float weight);

void Cuda_HierarcialSoftmaxEmbedding(float * targets, int targetNum, 
			float * embedding, float * deriv, int dim,
			float * vSpace, float * vBias, int vocabSize,
			float * cSpace, float * cBias, int cSize,
			float step,
			int * v2c, // word 2 class,
			int * classIdx, // class 2 word number
			int * wordIdx, // class 2 word index
			int * wordClassIdx, //word 2 class segment index.
			float gamma,
			float * classOutput,
			float * wordOutput,
			int wordSegSize, // word Max SegmentSize,
			float * targetProb
			);

void Cuda_HierarcicalProbability(float * targets, int targetNum, 
								 float * embedding, int dim,
			float * vSpace, float * vBias, int vocabSize,
			float * cSpace, float * cBias, int cSize,
			
			int * v2c, // word 2 class,
			int * classIdx, // class 2 word number
			int * wordIdx, // class 2 word index
			int * wordClassIdx, //word 2 class segment index.
			float gamma,
			float * classOutput,
			float * wordOutput,
			int wordSegSize, // word Max SegmentSize,
			float * targetProb
			);

void Cuda_MatrixTran_AddVector(float * gpu_floats_a, float * gpu_floats_b, float * gpu_floats_c, int batchsize, int dimension, float weight);

void Cuda_SeqIndex_Matrix_Multiply_Mapping(int * smpIdx, int * feaIndex, int * outIdx, int batchSize,
	float * weight, float * output, int hiddenDim, float wei);



void Cuda_SgemmMask(float * A, float * B, float * C, int batchsize, int m, int n,
	int * aMask, int * bMask, int * cMask, float alpha, float beta, bool transA, bool transB);

void Cuda_SparseSgemmMask(int * AIndex, int * AFeaIndex, float * AFeaValue, float * B, float * C, int batchsize, int m, int n,
	int * aMask, int * bMask, int * cMask, float alpha, float beta, bool transA, bool transB);

void Cuda_GetSeqOrderMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int * mapForward, int dim, int isReverse, int order, float * matrix, float alpha, float beta);
void Cuda_SetSeqOrderMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int * mapForward, int dim, int isReverse, int order, float * matrix, float alpha, float beta);


void Cuda_GetWindowMatrixs(int * InstanceMargin, int sentsize, float * SeqMatrixs, int * mapForward, int dim, int winsize, float * matrix, float alpha, float beta);

void Cuda_SetWindowMatrixs(int * InstanceMargin, int sentsize, float * SeqMatrixs, int * mapForward, int dim, int winsize, float * matrix, float alpha, float beta);

void Cuda_Matrix_Add(float * a, int skipa, float * b, int skipB, float * c, int skipC, int dim, int batchSize, float awei, float bwei, float cwei);

void Cuda_Matrix_Add(float * a, int * aMask, int skipA, float * b, int * bMask, int skipB, float * c, int * cMask, int skipC, int dim, int batchSize, float awei, float bwei, float cwei);

void Cuda_AccurateElementwiseProduct(float * pLeft, float * pRight, float * pDst, int * leftmask, int * rightmask, int * dstmask, int row, int column, float beta);


void Cuda_SoftAttention_Matching(float * a, int * aMask, float * b, int * bMask, int dim, int a_func, int op, float * vec, 
		float * c, int * cMask, int batchSize, float alpha, float beta);

void Cuda_DerivSoftAttention_Matching(float * a, float * aDeriv, int * aMask, float * b, float * bDeriv, int * bMask, 
		int dim, int a_func, int op, float * vec, float * vecDeriv, float * c, float * cDeriv, int * cMask, 
		int * aSmpIdx, int * aElementIdx, int aSize, int * bSmpIdx, int * bElementIdx, int bSize, int batchSize, float awei, float bwei, float vwei);

void Cuda_SoftAttention(float * a, float * b, int dim, int a_func, int op, float * vec, float * c, int aBatchSize, int bBatchSize, float alpha, float beta);

void Cuda_DerivSoftAttention(float * a, float * aDeriv, float * b, float * bDeriv, int dim, int a_func, int op, float * vec, float * vecDeriv,
	float * c, float * cDeriv, int aBatchSize, int bBatchSize, float awei, float bwei, float vwei);

void Cuda_NDArrayTranspose(float * input, int * indim, int * transDim, float * output, int dimNum, int length, float alpha, float beta);