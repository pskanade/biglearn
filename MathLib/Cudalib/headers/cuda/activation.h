#pragma once

#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>

#include "cublas_v2.h"
#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
#endif

void Cuda_Logistic(float * a, float * b, int size, float gamma);
void Cuda_Logistic(float ** a, float ** b, int row,  int column, float gamma);
void Cuda_DerivLogistic(float * a, float * b, int size, float gamma);
void Cuda_DerivLogistic(float ** a, float ** b, int row,  int column, float gamma);

void Cuda_Log(float * a, float * b, float alpha, float beta, int size);

void Cuda_Tanh(float * a, float * b, int size);
void Cuda_Tanh(float ** a, float ** b, int row, int column);
void Cuda_DerivTanh(float * a, float * b, int size);
void Cuda_DerivTanh(float ** a, float ** b, int row, int column);

void Cuda_ReLU(float * a, float * b, int size);
void Cuda_ReLU(float ** a, float ** b, int row, int column);
void Cuda_DerivReLU(float * a, float * b, int size);
void Cuda_DerivReLU(float ** a, float ** b, int row, int column);

void Cuda_DerivSoftmax(float ** outputScore, int * label, float ** outputDeriv, int dim, float gamma, int batchSize, float * targetProb);
void Cuda_DerivSoftmax(float * outputScore, int * outputLabel, float * outputDeriv, int dim, float gamma, int batchSize, float * targetProb);
void Cuda_DerivSoftmax_Test(float * outputScore, int * outputLabel, float * outputDeriv, int dim, float gamma, int batchSize, float * targetProb);

void Cuda_SoftMax(float * a, float * b, int labelDim, int batchsize, float gamma);
void Cuda_LogSoftMax(float * a, float * b, int labelDim, int batchsize, float gamma);

void cuda_CrossEntropyLoss(float * outputScore, float * outputLabel, float * outputDeriv, int outputSize);
void Cuda_MultiClassSoftmax(float * outputScore, float * outputDeriv, int dim, float gamma, int batchSize);
void Cuda_SparseMultiClassSoftmax(int * smpIdx, float * outputScore, float * outputProb, float gamma, int batchSize);
//void Cuda_SparseMultiClassSoftmax(int * smpIdx, int * elementIdx, float * outputScore, float * outputProb, float gamma, int batchSize);
void Cuda_DerivSparseMultiClassSoftmax(int * smpIdx, float * outputProb, float * probDeriv, float * outputDeriv, float gamma, float alpha, float beta, int batchSize);

//void Cuda_LogSparseSoftmax(int * classIdx, int * wordIdx, int classNum, float * wordAct, float * wordLogProb, float gamma);
void Cuda_LogSparseSoftmax(int * classIdx, int * wordIdx, int classNum, float * wordAct,
	int batchSize, int wordSize, float * wordLogProb, float gamma);
void Cuda_DerivSparseSoftmax(int * word_candidate_count, float * wordAct, float * wordDeriv, int * wordLabel, float gamma, int batchSize, float * targetProb);

void cuda_Deriv_Rectified(float * delta, float * layer_output, int batchsize, int m);
void cuda_Deriv_Tanh(float * delta, float * layer_output, int batchsize, int m);
void cuda_Deriv_Sigmoid(float * delta, float * layer_output, int batchsize, int m);

void Cuda_DerivLinear(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a);
void Cuda_DerivSigmoid(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a);
void Cuda_DerivRectified(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a);
void Cuda_DerivTanh(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a);

float Cublas_LogLossDeriv(cublasHandle_t handle, float * outputProb, int dim, int batchSize, float * smpProb,
	float * label, float * labelwei, int * labelMask, float gamma, float eps);

void Cuda_DerivLogSparseMultiClassSoftmax(int * smpIdx, float * outputProb, float * probDeriv, float * outputDeriv, float gamma, float alpha, float beta, int batchSize);
