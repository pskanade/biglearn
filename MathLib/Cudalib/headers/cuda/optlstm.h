#pragma once

#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>

#include "cublas_v2.h"
#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
#endif

void Cuda_LSTMForwardPropagateBatchV2(int * InstanceIndex, int batchSize,  //->x;
	float * o, //-> output,
	float * gate_i, //->gate i,
	float * c_hat, //->candidate memory,
	float * gate_f, //->forget gate,
	float * c, //->memory,
	float * gate_o, //->gate o,
	float * tanhc, //->tanh memory
	int cell,
	float * Ui,
	float * Uc,
	float * Uf,
	float * Uo, float * Vo,
	int * RecursiveIdx, int * RecursiveMatrix, int MaxLag);

void Cuda_LSTMBackwardPropagateBatchV2(int * InstanceIndex, int batchSize, int seqSize, int cell,
	float * o,
	float * gate_i,
	float * c_hat,
	float * gate_f,
	float * c,
	float * gate_o,
	float * tanhc,

	float * deriv_o,
	float * deriv_gate_i,
	float * deriv_cHat,
	float * deriv_gate_f,
	float * deriv_c,
	float * deriv_gate_o,
	float * deriv_tanhc,

	float * Ui, float * Uc, float * Uf, float * Uo, float * Vo,
	int * RecursiveIdx, int * RecursiveMatrix, int MaxLag);
