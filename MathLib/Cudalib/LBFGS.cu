#include "stdafx.h"
#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>
#include "device_functions.h"
#include <fstream>
#include "cublas_v2.h"

#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

using namespace std;

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
using namespace _com_util;
#else
#include <cfloat>
#endif

__global__ void cuda_L1Reg_CheckSign(float *src, float *dst, int len, int keepZero)
{
	int idx = blockDim.x*blockIdx.x + threadIdx.x;
	if (idx<len)
	{
		if ((src[idx]<0 && dst[idx]>0) || (src[idx]>0 && dst[idx] < 0))
		{
			dst[idx] = 0;
		}

		if (keepZero && src[idx] == 0)
		{
			dst[idx] = 0;
		}
	}
}

__global__ void cuda_L1Reg_CheckSignReverse(float *src, float *dst, int len, int keepZero)
{
	int idx = blockDim.x*blockIdx.x + threadIdx.x;
	if (idx < len)
	{
		if ((src[idx]<0 && dst[idx]<0) || (src[idx]>0 && dst[idx]>0))
		{
			dst[idx] = 0;
		}

		if (keepZero && src[idx] == 0)
		{
			dst[idx] = 0;
		}
	}
}

void Cuda_L1Reg_CheckSign(float *src, float *dst, int len, int keepZero, int reverse)
{
	if (reverse)
	{
		cuda_L1Reg_CheckSignReverse << <(len + 32 - 1) / 32, 32 >> >(src, dst, len, keepZero);
	}
	else
	{
		cuda_L1Reg_CheckSign << <(len + 32 - 1) / 32, 32 >> >(src, dst, len, keepZero);
	}
}

__global__ void cuda_L1Reg_CalcPseudoGradient(float* weight, float *grad, float *pseudoGrad, float cl1, float cl2, int len)
{
	int totalThread = blockDim.x*gridDim.x;
	int numPerThread = ((len + totalThread - 1) / totalThread);
	int threadId = blockDim.x*blockIdx.x + threadIdx.x;
	int startId = numPerThread*threadId;
	int endId = numPerThread*threadId + numPerThread;
	if (endId > len)
	{
		endId = len;
	}

	for (int i = startId; i<endId; i++)
	{
		pseudoGrad[i] = grad[i];
		if (cl1>0)
		{
			if (weight[i] < 0)
			{
				pseudoGrad[i] -= cl1;
			}
			else if (weight[i] > 0)
			{
				pseudoGrad[i] += cl1;
			}
			else if (grad[i] > cl1)
			{
				pseudoGrad[i] -= cl1;
			}
			else if (grad[i] < -cl1)
			{
				pseudoGrad[i] += cl1;
			}
			else
			{
				pseudoGrad[i] = 0;
			}
		}

		if (cl2>0)
		{
			pseudoGrad[i] += cl2*weight[i];
		}
	}
}

void Cuda_L1Reg_CalcPseudoGradient(float* weight, float *grad, float *pseudoGradient, float cl1, float cl2, int len)
{
	cuda_L1Reg_CalcPseudoGradient << <32, 32 >> >(weight, grad, pseudoGradient, cl1, cl2, len);
}

__global__ void cuda_L1Norm(float* weight, float* norm, int len)
{
	int numPerThread = ((len + blockDim.x - 1) / blockDim.x);
	int start = numPerThread * threadIdx.x;
	int end = start + numPerThread;

	__shared__ float normD[32];

	normD[threadIdx.x] = 0;
	if (end > len)
	{
		end = len;
	}
	for (int i = start; i < end; i++)
	{
		normD[threadIdx.x] += weight[i] >= 0 ? weight[i] : -weight[i];
	}

	__syncthreads();
	if (threadIdx.x == 0)
	{
		norm[0] = 0;
		for (int i = 0; i < 32; i++)
		{
			norm[0] += normD[i];
		}
	}
}

void Cuda_L1Norm(float* weight, float* norm, int len)
{
	cuda_L1Norm << <1, 32, 128 >> >(weight, norm, len);
}

__global__ void cuda_CountZero(float* weight, int* zcount, int len)
{
	int numPerThread = ((len + blockDim.x - 1) / blockDim.x);
	int start = numPerThread * threadIdx.x;
	int end = start + numPerThread;
	__shared__ 	int countD[32];
	countD[threadIdx.x] = 0;
	if (end > len)
	{
		end = len;
	}
	for (int i = start; i < end; i++)
	{
		countD[threadIdx.x] += (weight[i] == 0 ? 1 : 0);
	}

	__syncthreads();
	if (threadIdx.x == 0)
	{
		zcount[0] = 0;
		for (int i = 0; i < 32; i++)
		{
			zcount[0] += countD[i];
		}
	}
}

void Cuda_CountZero(float* weight, int* zcount, int len)
{
	cuda_CountZero << <1, 32, 128 >> >(weight, zcount, len);
}

