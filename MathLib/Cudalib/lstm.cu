#include "stdafx.h"
#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>
#include "device_functions.h"
#include <fstream>
#include "cublas_v2.h"

#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

using namespace std;

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
using namespace _com_util;
#else
#include <cfloat>
#endif

__global__ void cuda_LSTMForwardPropagate(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, //->x;
		float * H, //-> hiddenState,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->output,
		int cell,
		float * Wi, float * Ui, float * bi,
		float * Wc, float * Uc, float * bc,
		float * Wf, float * Uf, float * bf,
		float * Wo, float * Uo, float * Vo, float * bo)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];

		/// t = 0;
		int seqCell = seqBegin * cell;
		for(int h=0;h<cell;h++) gate_i[seqCell + h] = bi[h];
		for(int h=0;h<cell;h++) c_hat[seqCell + h] = bc[h];
		for(int h=0;h<cell;h++) gate_f[seqCell + h] = bf[h];
		for(int h=0;h<cell;h++) gate_o[seqCell + h] = bo[h];

		int fend = seqIdx[seqBegin];
		int fbegin = seqBegin == 0 ? 0 : seqIdx[seqBegin - 1];

		for(int f = fbegin; f < fend; f++)
		{
			float fv = feaValue[f];
			int idx = feaIdx[f] * cell;
			for(int h=0;h<cell;h++) gate_i[seqCell + h] += Wi[idx + h] * fv;
			for(int h=0;h<cell;h++) c_hat[seqCell + h] += Wc[idx + h] * fv;
			for(int h=0;h<cell;h++) gate_f[seqCell + h] += Wf[idx + h] * fv;
			for(int h=0;h<cell;h++) gate_o[seqCell + h] += Wo[idx + h] * fv;
		}

		for(int h=0;h<cell;h++) gate_i[seqCell + h] = (tanhf(gate_i[seqCell + h]/2)+1)/2;
		for(int h=0;h<cell;h++) c_hat[seqCell + h] = tanhf(c_hat[seqCell + h] );
		for(int h=0;h<cell;h++) gate_f[seqCell + h] = (tanhf(gate_f[seqCell + h]/2)+1)/2;
		for(int h=0;h<cell;h++) c[seqCell + h] = gate_i[seqCell + h] * c_hat[seqCell + h];

		for(int h1=0;h1<cell;h1++)
		{
			float fsum = 0;
			float * pC = c + seqCell;
			float * pVo = Vo + h1;
			for(int h2=0;h2<cell; h2++)
			{
				fsum += (*pC) * (*pVo); //Vo[h1*cell + h2]; 
				pC += 1;
				pVo += cell;
			}
			gate_o[seqCell + h1] += fsum;
		}
		for(int h=0;h<cell;h++) gate_o[seqCell + h] = (tanhf(gate_o[seqCell + h] /2)+1)/2;
		for(int h=0;h<cell;h++) H[seqCell + h] = gate_o[seqCell + h]* tanhf(c[seqCell + h]);
		//(tanhf(output[seqCell + h] /2)+1)/2;

		/// t = 1..T
		for(int t=seqBegin + 1; t<seqEnd;t++)
		{
			fbegin = seqIdx[t - 1];
			fend = seqIdx[t];

			seqCell = t * cell;
			int preSeqCell = seqCell - cell;
			for(int h=0;h<cell;h++) gate_i[seqCell + h] = bi[h];
			for(int h=0;h<cell;h++) c_hat[seqCell + h] = bc[h];
			for(int h=0;h<cell;h++) gate_f[seqCell + h] = bf[h];
			for(int h=0;h<cell;h++) gate_o[seqCell + h] = bo[h];


			for(int f = fbegin; f < fend; f++)
			{
				float fv = feaValue[f];
				int idx = feaIdx[f] * cell;
				for(int h=0;h<cell;h++) gate_i[seqCell + h] += Wi[idx + h] * fv;
				for(int h=0;h<cell;h++) c_hat[seqCell + h] += Wc[idx + h] * fv;
				for(int h=0;h<cell;h++) gate_f[seqCell + h] += Wf[idx + h] * fv;
				for(int h=0;h<cell;h++) gate_o[seqCell + h] += Wo[idx + h] * fv;
			}

			for(int h1=0;h1<cell;h1++)
			{
				float Isum = 0;
				float Chatsum = 0;
				float Fsum = 0;
				float Osum = 0;

				//int hCell = h1 * cell;

				float * pH = H + preSeqCell;

				float * pUi = Ui + h1;
				float * pUc = Uc + h1;
				float * pUf = Uf + h1;
				float * pUo = Uo + h1;
				for(int h2=0;h2<cell; h2++)
				{
					Isum += (*pH) * (*pUi); //Vo[h1*cell + h2]; 
					Chatsum += (*pH) * (*pUc);
					Fsum += (*pH) * (*pUf);
					Osum += (*pH) * (*pUo);
					pH += 1;
					pUi += cell;
					pUc += cell;
					pUf += cell;
					pUo += cell;
				}
				gate_i[seqCell + h1] += Isum;
				c_hat[seqCell + h1] += Chatsum;
				gate_f[seqCell + h1] += Fsum;
				gate_o[seqCell + h1] += Osum;
			}


			for(int h=0;h<cell;h++) gate_i[seqCell + h] = (tanhf(gate_i[seqCell + h]/2)+1)/2;
			for(int h=0;h<cell;h++) c_hat[seqCell + h] = tanhf(c_hat[seqCell + h] );
			for(int h=0;h<cell;h++) gate_f[seqCell + h] = (tanhf(gate_f[seqCell + h]/2)+1)/2;
			for(int h=0;h<cell;h++) c[seqCell + h] = gate_i[seqCell + h] * c_hat[seqCell + h] + gate_f[seqCell + h] * c[preSeqCell + h];

			for(int h1=0;h1<cell;h1++)
			{
				float Osum = 0;
				float * pC = c + seqCell;
				float * pVo = Vo + h1;
				for(int h2=0;h2<cell; h2++)
				{
					Osum += (*pC) * (*pVo); //Vo[h1*cell + h2]; 
					pC += 1;
					pVo += cell;
				}
				gate_o[seqCell + h1] += Osum;
			}
			for(int h=0;h<cell;h++) gate_o[seqCell + h] = (tanhf(gate_o[seqCell + h] /2)+1)/2;
			for(int h=0;h<cell;h++) H[seqCell + h] = gate_o[seqCell + h]* tanhf(c[seqCell + h]);
		}
	}
}

void Cuda_LSTMForwardPropagate(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, //->x;
		float * h, //-> hiddenState,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->output,
		int cell,
		float * Wi, float * Ui, float * bi,
		float * Wc, float * Uc, float * bc,
		float * Wf, float * Uf, float * bf,
		float * Wo, float * Uo, float * Vo, float * bo)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_LSTMForwardPropagate<<<nBlockPerGrid, nThreadPerBlock>>>(smpIdx, batchSize, seqIdx, seqSize, feaIdx, feaValue, 
			h, gate_i, c_hat, gate_f, c, gate_o, cell, Wi, Ui, bi, Wc, Uc, bc, Wf, Uf, bf, Wo, Uo, Vo, bo);
}


__global__ void cuda_LSTMForwardPropagateStep2(int * smpIdx, int batchSize,  //int * seqIdx, int seqSize, int * feaIdx, float * feaValue, //->x;
		float * H, //-> hiddenState,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->output,
		int cell,
		float * Wi, float * Ui, float * bi,
		float * Wc, float * Uc, float * bc,
		float * Wf, float * Uf, float * bf,
		float * Wo, float * Uo, float * Vo, float * bo)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];

		if(seqEnd > seqBegin)
		{
			/// t = 0;
			int seqCell = seqBegin * cell;

			for(int h=0;h<cell;h++) gate_i[seqCell + h] = (tanhf(gate_i[seqCell + h]/2)+1)/2;
			for(int h=0;h<cell;h++) c_hat[seqCell + h] = tanhf(c_hat[seqCell + h] );
			for(int h=0;h<cell;h++) gate_f[seqCell + h] = (tanhf(gate_f[seqCell + h]/2)+1)/2;
			for(int h=0;h<cell;h++) c[seqCell + h] = gate_i[seqCell + h] * c_hat[seqCell + h];

			for(int h1=0;h1<cell;h1++)
			{
				float fsum = 0;
				float * pC = c + seqCell;
				float * pVo = Vo + h1;
				for(int h2=0;h2<cell; h2++)
				{
					fsum += (*pC) * (*pVo); //Vo[h1*cell + h2]; 
					pC ++;
					pVo += cell;
				}
				gate_o[seqCell + h1] += fsum;
			}
			for(int h=0;h<cell;h++) gate_o[seqCell + h] = (tanhf(gate_o[seqCell + h] /2)+1)/2;
			for(int h=0;h<cell;h++) H[seqCell + h] = gate_o[seqCell + h]* tanhf(c[seqCell + h]);
			//(tanhf(output[seqCell + h] /2)+1)/2;

			/// t = 1..T
			for(int t=seqBegin + 1; t<seqEnd;t++)
			{

				seqCell = t * cell;
				int preSeqCell = seqCell - cell;

				for(int h1=0;h1<cell;h1++)
				{
					float Isum = 0;
					float Chatsum = 0;
					float Fsum = 0;
					float Osum = 0;

					float * pH = H + preSeqCell;

					float * pUi = Ui + h1;
					float * pUc = Uc + h1;
					float * pUf = Uf + h1;
					float * pUo = Uo + h1;
					for(int h2=0;h2<cell; h2++)
					{
						Isum += (*pH) * (*pUi); //Vo[h1*cell + h2]; 
						Chatsum += (*pH) * (*pUc);
						Fsum += (*pH) * (*pUf);
						Osum += (*pH) * (*pUo);
						pH += 1;
						pUi += cell;
						pUc += cell;
						pUf += cell;
						pUo += cell;
					}
					gate_i[seqCell + h1] += Isum;
					c_hat[seqCell + h1] += Chatsum;
					gate_f[seqCell + h1] += Fsum;
					gate_o[seqCell + h1] += Osum;
				}


				for(int h=0;h<cell;h++) gate_i[seqCell + h] = (tanhf(gate_i[seqCell + h]/2)+1)/2;
				for(int h=0;h<cell;h++) c_hat[seqCell + h] = tanhf(c_hat[seqCell + h] );
				for(int h=0;h<cell;h++) gate_f[seqCell + h] = (tanhf(gate_f[seqCell + h]/2)+1)/2;
				for(int h=0;h<cell;h++) c[seqCell + h] = gate_i[seqCell + h] * c_hat[seqCell + h] + gate_f[seqCell + h] * c[preSeqCell + h];

				for(int h1=0;h1<cell;h1++)
				{
					float Osum = 0;
					float * pC = c + seqCell;
					float * pVo = Vo + h1; // * cell;
					for(int h2=0;h2<cell; h2++)
					{
						Osum += (*pC) * (*pVo); //Vo[h1*cell + h2]; 
						pC += 1;
						pVo += cell;
					}
					gate_o[seqCell + h1] += Osum;
				}
				for(int h=0;h<cell;h++) gate_o[seqCell + h] = (tanhf(gate_o[seqCell + h] /2)+1)/2;
				for(int h=0;h<cell;h++) H[seqCell + h] = gate_o[seqCell + h]* tanhf(c[seqCell + h]);
			}
		}
	}
}

void Cuda_LSTMForwardPropagateStep2(int * smpIdx, int batchSize, //int * seqIdx, int seqSize, int * feaIdx, float * feaValue, //->x;
		float * h, //-> hiddenState,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * output, //->output,
		int cell,
		float * Wi, float * Ui, float * bi,
		float * Wc, float * Uc, float * bc,
		float * Wf, float * Uf, float * bf,
		float * Wo, float * Uo, float * Vo, float * bo)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_LSTMForwardPropagateStep2<<<nBlockPerGrid, nThreadPerBlock>>>(smpIdx, batchSize,  //seqIdx, seqSize, feaIdx, feaValue, 
			h, gate_i, c_hat, gate_f, c, output, cell, Wi, Ui, bi, Wc, Uc, bc, Wf, Uf, bf, Wo, Uo, Vo, bo);
}




__global__ void cuda_LSTMForwardPropagateDense(int * smpIdx, int batchSize, float * seqInput, int seqSize, int inputDim,
		float * H,
		float * gate_i,
		float * c_hat,
		float * gate_f,
		float * c,
		float * output,
		int cell,
		float * Wi, float * Ui, float * bi,
		float * Wc, float * Uc, float * bc,
		float * Wf, float * Uf, float * bf,
		float * Wo, float * Uo, float * Vo, float * bo)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];

		/// t = 0;
		int seqCell = seqBegin * cell;
		for(int h=0;h<cell;h++) gate_i[seqCell + h] = bi[h];
		for(int h=0;h<cell;h++) c_hat[seqCell + h] = bc[h];
		for(int h=0;h<cell;h++) gate_f[seqCell + h] = bf[h];
		for(int h=0;h<cell;h++) output[seqCell + h] = bo[h];

		//int fend = seqBegin * inputDim;
		//int fbegin = seqBegin == 0 ? 0 : seqIdx[seqBegin - 1];

		for(int f = 0; f < inputDim; f++)
		{
			float fv = seqInput[seqCell + f];
			int idx = f * cell;
			for(int h=0;h<cell;h++) gate_i[seqCell + h] += Wi[idx + h] * fv;
			for(int h=0;h<cell;h++) c_hat[seqCell + h] += Wc[idx + h] * fv;
			for(int h=0;h<cell;h++) gate_f[seqCell + h] += Wf[idx + h] * fv;
			for(int h=0;h<cell;h++) output[seqCell + h] += Wo[idx + h] * fv;
		}

		for(int h=0;h<cell;h++) gate_i[seqCell + h] = (tanhf(gate_i[seqCell + h]/2)+1)/2;
		for(int h=0;h<cell;h++) c_hat[seqCell + h] = tanhf(c_hat[seqCell + h] );
		for(int h=0;h<cell;h++) gate_f[seqCell + h] = (tanhf(gate_f[seqCell + h]/2)+1)/2;
		for(int h=0;h<cell;h++) c[seqCell + h] = gate_i[seqCell + h] * c_hat[seqCell + h];

		for(int h1=0;h1<cell;h1++)
		{
			float fsum = 0;
			float * pC = c + seqCell;
			float * pVo = Vo + h1;
			for(int h2=0;h2<cell; h2++)
			{
				fsum += (*pC) * (*pVo); //Vo[h1*cell + h2]; 
				pC += 1;
				pVo += cell;
			}
			output[seqCell + h1] += fsum;
		}

		for(int h=0;h<cell;h++) output[seqCell + h] = (tanhf(output[seqCell + h] /2)+1)/2;
		for(int h=0;h<cell;h++) H[seqCell + h] = output[seqCell + h]* tanhf(c[seqCell + h]);
		//(tanhf(output[seqCell + h] /2)+1)/2;

		/// t = 1..T
		for(int t=seqBegin + 1; t<seqEnd;t++)
		{
			//fbegin = seqIdx[t - 1];
			//fend = seqIdx[t];

			seqCell = t * cell;
			int preSeqCell = seqCell - cell;
			for(int h=0;h<cell;h++) gate_i[seqCell + h] = bi[h];
			for(int h=0;h<cell;h++) c_hat[seqCell + h] = bc[h];
			for(int h=0;h<cell;h++) gate_f[seqCell + h] = bf[h];
			for(int h=0;h<cell;h++) output[seqCell + h] = bo[h];

			for(int f = 0; f < inputDim; f++)
			{
				float fv = seqInput[seqCell + f];
				int idx = f * cell;
				for(int h=0;h<cell;h++) gate_i[seqCell + h] += Wi[idx + h] * fv;
				for(int h=0;h<cell;h++) c_hat[seqCell + h] += Wc[idx + h] * fv;
				for(int h=0;h<cell;h++) gate_f[seqCell + h] += Wf[idx + h] * fv;
				for(int h=0;h<cell;h++) output[seqCell + h] += Wo[idx + h] * fv;
			}

			for(int h1=0;h1<cell;h1++)
			{
				float Isum = 0;
				float Chatsum = 0;
				float Fsum = 0;
				float Osum = 0;

				//int hCell = h1 * cell;
				float * pH = H + preSeqCell;
				float * pUi = Ui + h1;
				float * pUc = Uc + h1;
				float * pUf = Uf + h1;
				float * pUo = Uo + h1;
				for(int h2=0;h2<cell; h2++)
				{
					Isum += (*pH) * (*pUi); //Vo[h1*cell + h2]; 
					Chatsum += (*pH) * (*pUc);
					Fsum += (*pH) * (*pUf);
					Osum += (*pH) * (*pUo);
					pH += 1;
					pUi += cell;
					pUc += cell;
					pUf += cell;
					pUo += cell;
				}
				gate_i[seqCell + h1] += Isum;
				c_hat[seqCell + h1] += Chatsum;
				gate_f[seqCell + h1] += Fsum;
				output[seqCell + h1] += Osum;
			}

			for(int h=0;h<cell;h++) gate_i[seqCell + h] = (tanhf(gate_i[seqCell + h]/2)+1)/2;
			for(int h=0;h<cell;h++) c_hat[seqCell + h] = tanhf(c_hat[seqCell + h] );
			for(int h=0;h<cell;h++) gate_f[seqCell + h] = (tanhf(gate_f[seqCell + h]/2)+1)/2;
			for(int h=0;h<cell;h++) c[seqCell + h] = gate_i[seqCell + h] * c_hat[seqCell + h] + gate_f[seqCell + h] * c[preSeqCell + h];

			for(int h1=0;h1<cell;h1++)
			{
				float Osum = 0;
				float * pC = c + seqCell;
				float * pVo = Vo + h1;
				for(int h2=0;h2<cell; h2++)
				{
					Osum += (*pC) * (*pVo); //Vo[h1*cell + h2]; 
					pC += 1;
					pVo += cell;
				}
				output[seqCell + h1] += Osum;
			}

			for(int h=0;h<cell;h++) output[seqCell + h] = (tanhf(output[seqCell + h] /2)+1)/2;
			for(int h=0;h<cell;h++) H[seqCell + h] = output[seqCell + h]* tanhf(c[seqCell + h]);
		}
	}
}

void Cuda_LSTMForwardPropagateDense(int * smpIdx, int batchSize, float * seqInput, int seqSize, int inputDim,
		float * h,
		float * gate_i,
		float * c_hat,
		float * gate_f,
		float * c,
		float * output,
		int cell,
		float * Wi, float * Ui, float * bi,
		float * Wc, float * Uc, float * bc,
		float * Wf, float * Uf, float * bf,
		float * Wo, float * Uo, float * Vo, float * bo)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_LSTMForwardPropagateDense<<<nBlockPerGrid, nThreadPerBlock>>>(smpIdx, batchSize, seqInput, seqSize, inputDim, 
			h, gate_i, c_hat, gate_f, c, output, cell, Wi, Ui, bi, Wc, Uc, bc, Wf, Uf, bf, Wo, Uo, Vo, bo);
}

__global__ void cuda_LSTMBackwardPropagate(int * smpIdx, int batchSize, int seqSize, int cell,
		float * H,
		float * gate_i,
		float * c_hat,
		float * gate_f,
		float * c,
		float * gate_o,

		float * deriv_h,
		float * deriv_gate_i,
		float * deriv_cHat,
		float * deriv_gate_f,
		float * deriv_c,
		float * deriv_gate_o,
		float * Wi, float * Ui, float * bi,
		float * Wc, float * Uc, float * bc,
		float * Wf, float * Uf, float * bf,
		float * Wo, float * Uo, float * Vo, float * bo)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];

		int seqCell = 0;
		for(int i= seqEnd - 1; i > seqBegin; i--)
		{
			seqCell = i * cell;
			int seqPreCell = seqCell - cell;

			for(int h=0;h<cell;h++)
			{
				float tanhC =  tanhf(c[seqCell + h]);
				deriv_c[seqCell + h] += deriv_h[seqCell + h] * gate_o[seqCell + h] * (1- tanhC) * (1 + tanhC);
				deriv_gate_o[seqCell + h] = deriv_h[seqCell + h] * tanhC * gate_o[seqCell + h] * ( 1 - gate_o[seqCell + h]);
			}

			for(int h1 = 0; h1 < cell; h1 ++)
			{
				float sum1 = 0;
				float sum2 = 0;
				float * dgo = deriv_gate_o + seqCell;
				float * dVo = Vo + h1 * cell;
				float *dUo = Uo + h1 * cell;
				for(int h2 = 0; h2 < cell; h2++)
				{
					sum1 += (*dgo) * (*dVo); // deriv_gate_o[seqCell + h2] * Vo[h1 * cell + h2];
					sum2 += (*dgo) * (*dUo);
					dgo++;
					dVo++;
					dUo++;
				}
				deriv_c[seqCell + h1] += sum1;
				deriv_h[seqPreCell + h1] += sum2;
			}

			for(int h = 0; h < cell; h++)
			{
				int n = seqCell + h;
				float v_gi = gate_i[n];
				float v_chat = c_hat[n];
				float v_gate_f = gate_f[n];
				float v_C_t_1 = c[seqPreCell + h];
				float dc = deriv_c[n];

				deriv_cHat[n] = dc * v_gi * ( 1 + v_chat) * ( 1-v_chat);
				deriv_gate_i[n] = dc * v_chat * v_gi * (1-v_gi);
				deriv_gate_f[n] = dc * v_C_t_1 * v_gate_f * ( 1-v_gate_f);
				deriv_c[seqPreCell + h] += dc * v_gate_f;
			}


			for(int h1 = 0; h1 < cell; h1 ++)
			{
				float sum1 = 0;
				float sum2 = 0;
				float sum3 = 0;
				float * dgi = deriv_gate_i + seqCell;
				float * dUi = Ui + h1 * cell;

				float * dchat = deriv_cHat + seqCell;
				float * dUc = Uc + h1 * cell;

				float * dgf = deriv_gate_f + seqCell;
				float * dUf = Uf + h1 * cell;

				for(int h2 = 0; h2 < cell; h2++)
				{
					sum1 += (*dgi) * (*dUi); // deriv_gate_o[seqCell + h2] * Vo[h1 * cell + h2];
					sum2 += (*dchat) * (*dUc);
					sum3 +=  (*dgf) * (*dUf);
					dgi++;
					dUi++;
					dchat++;
					dUc++;
					dgf++;
					dUf++;
				}
				deriv_h[seqPreCell + h1] += sum1 + sum2 + sum3;
			}
		}

		seqCell = seqBegin * cell;
		for(int h=0;h<cell;h++)
		{
			float tanhC = tanhf(c[seqCell + h]);
			deriv_c[seqCell + h] += deriv_h[seqCell + h] * gate_o[seqCell + h] * (1- tanhC) * (1 + tanhC);
			deriv_gate_o[seqCell + h] = deriv_h[seqCell + h] * tanhC * gate_o[seqCell + h] * ( 1 - gate_o[seqCell + h]);
		}

		for(int h1 = 0; h1 < cell; h1 ++)
		{
			float sum1 = 0;
			float * dgo = deriv_gate_o + seqCell;
			float * dVo = Vo + h1 * cell;
			for(int h2 = 0; h2 < cell; h2++)
			{
				sum1 += (*dgo) * (*dVo); // deriv_gate_o[seqCell + h2] * Vo[h1 * cell + h2];
				dgo++;
				dVo++;
			}
			deriv_c[seqCell + h1] += sum1;
		}

		for(int h = 0; h < cell; h++)
		{
			int n = seqCell + h;
			float v_gi = gate_i[n];
			float v_chat = c_hat[n];
			float dc = deriv_c[n];
			deriv_cHat[n] = dc * v_gi * ( 1 + v_chat) * ( 1-v_chat);
			deriv_gate_i[n] = dc * v_chat * v_gi * (1-v_gi);
		}
	}
}

void Cuda_LSTMBackwardPropagate(int * smpIdx, int batchSize, int seqSize, int cell,
		float * h,
		float * gate_i,
		float * c_hat,
		float * gate_f,
		float * c,
		float * gate_o,

		float * deriv_h,
		float * deriv_gate_i,
		float * deriv_cHat,
		float * deriv_gate_f,
		float * deriv_c,
		float * deriv_gate_o,
		float * Wi, float * Ui, float * bi,
		float * Wc, float * Uc, float * bc,
		float * Wf, float * Uf, float * bf,
		float * Wo, float * Uo, float * Vo, float * bo)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_LSTMBackwardPropagate<<<nBlockPerGrid, nThreadPerBlock>>>(smpIdx, batchSize, seqSize, cell, 
			h, gate_i, c_hat, gate_f, c, gate_o, deriv_h, deriv_gate_i, deriv_cHat, deriv_gate_f, deriv_c, deriv_gate_o,
			Wi, Ui, bi, Wc, Uc, bc, Wf, Uf, bf, Wo, Uo, Vo, bo);
}


__global__ void cuda_LogBayesianRatingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < srcBatchSize)
	{
		int eleEnd = src2MatchIdx[idx];
		int eleBegin = idx > 0 ? src2MatchIdx[idx - 1] : 0;

		for(int i = eleBegin; i < eleEnd; i++)
		{
			outputDeriv[src2MatchElement[i]] = 0;
		}

		int listNum = eleEnd - eleBegin;
		if (listNum <= 0) { srcBatchLoss[idx] = 0; return; }

		float maxValue = -FLT_MAX;
		for (int i = eleBegin; i < eleEnd; i++) 
		{ 
			float tmp = outputScore[src2MatchElement[i]];
			if (tmp > maxValue) { maxValue = tmp;}
		}

		float pure_sum = 0;
		float rate_sum = 0;
		for(int i = eleBegin; i < eleEnd; i++)
		{
			int matchI = src2MatchElement[i];
			float tmpScore = expf(gamma * (outputScore[matchI] - maxValue) );
			float rateTmpScore = outputLabel[matchI] * tmpScore;
			pure_sum += tmpScore;
			rate_sum += rateTmpScore;
		}
		float avgRating =  rate_sum / (pure_sum + eplison);
		srcBatchLoss[idx] = logf(avgRating + eplison);

		for(int i = eleBegin; i < eleEnd; i++)
		{
			int matchI = src2MatchElement[i];
			float tmpScore = expf(gamma * (outputScore[matchI] - maxValue ));
			outputDeriv[matchI] =  gamma * ( outputLabel[matchI] * tmpScore / (rate_sum + eplison) - tmpScore /  (pure_sum + eplison) );
		}
	}
}

void Cuda_LogBayesianRatingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (srcBatchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_LogBayesianRatingLoss << <nBlockPerGrid, nThreadPerBlock >> >(src2MatchIdx, src2MatchElement, srcBatchSize, srcBatchLoss,
			outputScore, outputLabel, outputDeriv, outputSize, eplison, gamma);
}


__global__ void cuda_BayesianRatingProb(
	int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
	float * outputScore, float * outputLabel, float * outputProb, int outputSize, float eplison, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < srcBatchSize)
	{
		int eleEnd = src2MatchIdx[idx];
		int eleBegin = idx > 0 ? src2MatchIdx[idx - 1] : 0;

		for (int i = eleBegin; i < eleEnd; i++)
		{
			int idx = src2MatchElement == NULL ? i : src2MatchElement[i];
			outputProb[idx] = 0;
		}

		int listNum = eleEnd - eleBegin;
		if (listNum <= 0) { srcBatchLoss[idx] = 0; return; }

		float maxValue = -FLT_MAX;
		for (int i = eleBegin; i < eleEnd; i++)
		{
			int idx = src2MatchElement == NULL ? i : src2MatchElement[i];
			float tmp = outputScore[idx];
			if (tmp > maxValue) { maxValue = tmp; }
		}

		float pure_sum = 0;
		float rate_sum = 0;
		for (int i = eleBegin; i < eleEnd; i++)
		{
			int matchI = src2MatchElement == NULL ? i : src2MatchElement[i];
			float tmpScore = expf(gamma * (outputScore[matchI] - maxValue));
			float rateTmpScore = outputLabel[matchI] * tmpScore;
			pure_sum += tmpScore;
			rate_sum += rateTmpScore;
		}
		float avgRating = rate_sum / (pure_sum + eplison);
		srcBatchLoss[idx] = logf(avgRating + eplison);

		for (int i = eleBegin; i < eleEnd; i++)
		{
			int matchI = src2MatchElement == NULL ? i : src2MatchElement[i];
			float tmpScore = expf(gamma * (outputScore[matchI] - maxValue));
			outputProb[matchI] = tmpScore / (pure_sum + eplison);
			//outputDeriv[matchI] = gamma * (outputLabel[matchI] * tmpScore / (rate_sum + eplison) - tmpScore / (pure_sum + eplison));
		}
	}
}

void Cuda_BayesianRatingProb(
	int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
	float * outputScore, float * outputLabel, float * outputProb, int outputSize, float eplison, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (srcBatchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_BayesianRatingProb << <nBlockPerGrid, nThreadPerBlock >> >(src2MatchIdx, src2MatchElement, srcBatchSize, srcBatchLoss,
		outputScore, outputLabel, outputProb, outputSize, eplison, gamma);
}
