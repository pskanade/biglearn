// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <iostream> 
#include <vector> 
//#include <cuda_runtime.h> 
//#include <cublas.h>
//#include <cuda.h>
//#include <cuda_runtime_api.h>
//#include <cuda_surface_types.h>
//#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>

//#include "cublas_v2.h"
//#pragma comment(lib, "cudart") 
//#pragma comment(lib,"cublas.lib")

//typedef amp_tausworthe_hybrid_collection<2> amp_rng;
// TODO: reference additional headers your program requires here
#define DROPOUT_CONST (3e38f)

#ifdef WIN32
#define DLLEXP extern "C" __declspec(dllexport)
#endif
// TODO: reference additional headers your program requires here
