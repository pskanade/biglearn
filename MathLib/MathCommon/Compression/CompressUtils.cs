﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;
namespace ScopeML
{
    public struct BufferBlock
    {
        public byte[] buffer;
        public int offset;
        public int length;
        public int size;
    }

    public static class CompressUtils
    {
        public static byte[] Compress(byte[] uncompressed)
        {
            return Compress(uncompressed, 0, uncompressed.Length);
        }

        public static byte[] Compress(byte[] uncompressed, int offset, int length)
        {
            MemoryStream ms = new MemoryStream();
            Compress(uncompressed, offset, length, ms);
            return ms.ToArray();
        }

        public static void Compress(byte[] uncompressed, int offset, int length, Stream baseStream)
        {
            using (BinaryWriter br = new BinaryWriter(baseStream, Encoding.Default, true))
            {
                br.Write(length);
                using (GZipStream compressedStream = new GZipStream(baseStream, CompressionMode.Compress, true))
                {
                    compressedStream.Write(uncompressed, offset, length);
                    compressedStream.Flush();
                }
            }
        }

        public static byte[] Decompress(byte[] compressed)
        {
            using (MemoryStream ms = new MemoryStream(compressed, false))
            {
                return Decompress(ms);
            }
        }

        public static byte[] Decompress(Stream ms)
        {
            using (BinaryReader br = new BinaryReader(ms, Encoding.Default, true))
            {
                int length = br.ReadInt32();
                byte[] buffer = new byte[length];

                using (GZipStream decompressedStream = new GZipStream(ms, CompressionMode.Decompress, true))
                {
                    decompressedStream.Read(buffer, 0, length);
                }

                return buffer;
            }
        }
    }

}
