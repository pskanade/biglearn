﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;
using System.Diagnostics;
using System.Collections;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime;
using System.Threading;

namespace ScopeML
{
    public unsafe partial class FastVector : IDisposable
    {
        #region vector computation
        public void Add(FastVector other)
        {
            SseUtils.Add(other.pinnedBasePtr, this.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public void Add(float[] _values)
        {
            fixed (float* ptr = &_values[0])
            {
                SseUtils.Add(ptr, this.pinnedBasePtr, this.pinnedBasePtr, this.length);
            }
        }

        public void Add(int[] _indices, float[] _values)
        {
            fixed (float* ptr = &_values[0])
            fixed (int* idxPtr = &_indices[0])
            {
                SseUtils.Add(ptr, idxPtr, this.pinnedBasePtr, _indices.Length);
            }
        }

        public static void Add(float[] left, int offsetLeft, float[] right, int offsetRight, float[] dst, int offsetDst, int count)
        {
            fixed (float* lptr = &left[offsetLeft])
            fixed (float* rptr = &right[offsetRight])
            fixed (float* dptr = &dst[offsetDst])
            {
                SseUtils.Add(lptr, rptr, dptr, count);
            }
        }

        public void AddScale(float scale, int[] _indices, float[] _values)
        {
            fixed (float* ptr = &_values[0])
            fixed (int* idxPtr = &_indices[0])
            {
                SseUtils.AddScale(scale, ptr, idxPtr, this.pinnedBasePtr, _indices.Length);
            }
        }

        public float DotProduct(int offset, float[] _values)
        {
            fixed (float* ptr = &_values[0])
            {
                return SseUtils.DotProductDense(ptr, this.pinnedBasePtr + offset, _values.Length);
            }
        }

        public float DotProduct(int[] _indices, float[] _values)
        {
            fixed (float* ptr = &_values[0])
            fixed (int* idxPtr = &_indices[0])
            {
                return SseUtils.DotProductSparse(this.pinnedBasePtr, ptr, idxPtr, _indices.Length);
            }
        }

        public float Sum(int[] index)
        {
            fixed (int* idxPtr = &index[0])
            {
                return SseUtils.Sum(this.pinnedBasePtr, idxPtr, index.Length);
            }
        }

        public void Sub(FastVector other)
        {
            SseUtils.Sub(this.pinnedBasePtr, other.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public void Sub(float[] _values)
        {
            fixed (float* ptr = &_values[0])
            {
                SseUtils.Sub(this.pinnedBasePtr, ptr, this.pinnedBasePtr, this.length);
            }
        }

        public void Sub(FastVector other, out FastVector result)
        {
            result = FastVector.Create(this.length);
            SseUtils.Sub(this.pinnedBasePtr, other.pinnedBasePtr, result.pinnedBasePtr, this.length);
        }

        public float DotProduct(FastVector other)
        {
            return SseUtils.DotProductDense(other.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public float DotProduct(float[] _values)
        {
            fixed (float* ptr = &_values[0])
            {
                return SseUtils.DotProductDense(ptr, this.pinnedBasePtr, this.length);
            }
        }

        public void ApplyAdaGrad(FastVector grad)
        {
            SseUtils.ApplyAdaGrad(this.pinnedBasePtr, grad.pinnedBasePtr, this.length);
        }

        public void ApplyAdaGrad(float[] grad)
        {
            fixed (float* ptr = &grad[0])
            {
                SseUtils.ApplyAdaGrad(this.pinnedBasePtr, ptr, this.length);
            }
        }

        public void ApplyExp(float[] dst)
        {
            fixed (float* ptr = &dst[0])
            {
                SseUtils.ApplyExp(this.pinnedBasePtr, ptr, this.length);
            }
        }

        public void ApplyExp(FastVector dst)
        {
            SseUtils.ApplyExp(this.pinnedBasePtr, dst.pinnedBasePtr, this.length);
        }

        public void ApplyExp()
        {
            SseUtils.ApplyExp(this.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public float Sum()
        {
            return SseUtils.Sum(this.pinnedBasePtr, this.length);
        }

        public float Norm(int normtype = 2)
        {
            switch (normtype)
            {
                case 2:
                    {
                        return (float)System.Math.Sqrt(NormSquare());
                    }
                case 1:
                    {
                        return SseUtils.SumAbs(this.pinnedBasePtr, this.length);
                    }
                default:
                    {
                        throw new Exception("no supported");
                    }
            }
        }

        public static float Norm(float[] data, int normtype = 2, int length = 0)
        {
            if (length <= 0)
            {
                length = data.Length;
            }

            fixed(float *ptr = data)
            {

                switch (normtype)
                {
                    case 2:
                        {
                            return (float)System.Math.Sqrt(SseUtils.SumSq(ptr, length));
                        }
                    case 1:
                        {
                            return SseUtils.SumAbs(ptr, length);
                        }
                    default:
                        {
                            throw new Exception("no supported");
                        }
                }
            }
        }

        public float NormSquare(int normtype = 2)
        {
            if (normtype == 2)
            {
                return SseUtils.SumSq(this.pinnedBasePtr, this.length);
            }
            else
            {
                throw new NotSupportedException("should only for L2 norm");
            }
        }

        public void Scale(float scale)
        {
            SseUtils.Scale(scale, this.pinnedBasePtr, this.length);
        }

        // this = this+other*scale
        public void AddScale(float scale, FastVector other)
        {
            SseUtils.AddScale(scale, other.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        // this = this+values*scale
        public void AddScale(float scale, float[] _values)
        {
            fixed (float* ptr = &_values[0])
            {
                SseUtils.AddScale(scale, ptr, this.pinnedBasePtr, this.length);
            }
        }

        public void ApplySqrtInPlace()
        {
            SseUtils.ApplySqrt(this.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public FastVector ApplySqrt()
        {
            FastVector result = FastVector.Create(this.length);
            SseUtils.ApplySqrt(this.pinnedBasePtr, result.pinnedBasePtr, this.length);

            return result;
        }

        public void ApplySquare()
        {
            SseUtils.ApplySquare(this.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        /// <summary>
        /// o(x) = tanh(f(x))
        /// tanh(f(x))=(1-e^(-2f(x)))/(1+e^(-2f(x)))
        /// o'(x) = o'(tanh(f))*tanh'(f(x))*f'(x)
        /// this = o'(x)
        /// output = tanh(f(x))
        /// tanhDeriv = o'*tanh'=this*(1-output)(1+output)
        /// result:
        /// this.value=o(tanh(x))'=this.value*tanh(x)'=this.value*(1-tanhGradient)*(1+tanhGradient);
        /// </summary>
        /// <param name="output"></param>
        public void ApplyTanhDerivativeInPlace(FastVector output)
        {
            SseUtils.ApplyTanhDerivative(output.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public static void ApplyTanhDerivativeInPlace(float* deriv, float* output, int length)
        {
            SseUtils.ApplyTanhDerivative(output, deriv, length);
        }

        public static void Tanh(FastVector src, FastVector dst, int batchsize)
        {
            SseUtils.ApplyTanh(src.pinnedBasePtr, dst.pinnedBasePtr, batchsize);
        }

        public static void Tanh(float* src, float* dst, int length)
        {
            SseUtils.ApplyTanh(src, dst, length);
        }

        public static void TanhDeriv(float* src, float* dst, int length)
        {
            SseUtils.ApplyTanh(src, dst, length);
        }

        public void ApplyTanhInPlace()
        {
            SseUtils.ApplyTanh(this.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public void ApplySigmoidInPlace()
        {
            SseUtils.ApplySigmoid(this.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public void ApplySigmoid(float[] dst)
        {
            fixed (float* ptr = &dst[0])
            {
                SseUtils.ApplySigmoid(this.pinnedBasePtr, ptr, this.length);
            }
        }

        public void ApplySigmoid(FastVector dst)
        {
            SseUtils.ApplySigmoid(this.pinnedBasePtr, dst.pinnedBasePtr, this.length);
        }

        public void ApplySigmoidDerivative(FastVector output)
        {
            SseUtils.ApplySigmoid(output.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public void ApplyRectifiedDerivativeInPlace(FastVector output)
        {
            SseUtils.ApplyRectifiedLinearDerivative(output.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public void ApplyRectifiedInPlace()
        {
            SseUtils.ApplyRectifiedLinear(this.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public void MultiplyInPlace(FastVector other)
        {
            if (this.Length != other.Length)
            {
                throw new ArgumentException("length unmatched");
            }

            SseUtils.ApplyMultiply(this.pinnedBasePtr, other.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public virtual FastVector Multiply(FastVector other)
        {
            if (this.Length != other.Length)
            {
                throw new ArgumentException("length unmatched");
            }

            FastVector res = FastVector.Create(this.Length);

            SseUtils.ApplyMultiply(this.pinnedBasePtr, other.pinnedBasePtr, res.pinnedBasePtr, this.length);

            return res;
        }

        public virtual void DivInPlace(FastVector other)
        {
            if (this.Length != other.Length)
            {
                throw new Exception("length unmatched");
            }

            SseUtils.ApplyDiv(this.pinnedBasePtr, other.pinnedBasePtr, this.pinnedBasePtr, this.length);
        }

        public virtual FastVector Div(FastVector other)
        {
            if (this.Length != other.Length)
            {
                throw new ArgumentException("length unmatched");
            }

            FastVector res = FastVector.Create(this.Length);

            SseUtils.ApplyDiv(this.pinnedBasePtr, other.pinnedBasePtr, res.pinnedBasePtr, this.length);
            return res;
        }

        public float L2Norm
        {
            get
            {
                return Norm();
            }
        }

        public float CosineSim(FastVector other)
        {
            float dp = this.DotProduct(other);

            float thisnorm = this.L2Norm;
            float othernorm = other.L2Norm;

            if (thisnorm == 0.0f || othernorm == 0.0f)
                return 0.0f;
            else
                return dp / (this.L2Norm * other.L2Norm);
        }

        public void Clip(int m, float maxThreshold, float minThreshold)
        {
            SseUtils.Clip(this.pinnedBasePtr, Math.Min(m, this.Length), minThreshold, maxThreshold);
        }

        public static void AdagradUpdate(FastVector ada, FastVector grad)
        {
            if (FastVector.IsAvx)
            {
                AvxUtils.ApplyAdaGrad(ada.pinnedBasePtr, grad.pinnedBasePtr, ada.Length);
            }
            else
            {
                SseUtils.ApplyAdaGrad(ada.pinnedBasePtr, grad.pinnedBasePtr, ada.Length);
            }
        }

        public static void AdagradUpdate(float[] ada, float[] grad)
        {
            int size = ada.Length;
            if (ada == null || ada.Length == 0)
            {
                return;
            }

            int parallelSize = 1024 * 32;
            int parrallelNum = (size + parallelSize - 1) / parallelSize;
            if (parrallelNum < 2)
            {
                fixed (float* pA = &ada[0])
                fixed (float* pG = &grad[0])
                {
                    if (FastVector.IsAvx)
                    {
                        AvxUtils.ApplyAdaGrad(pA, pG, size);
                    }
                    else
                    {
                        SseUtils.ApplyAdaGrad(pA, pG, size);
                    }
                }
            }
            else
            {
                Parallel.For(0, parrallelNum, i =>
                {
                    int offset = i * parallelSize;
                    int cnt = Math.Min(parallelSize, size - offset);
                    fixed (float* pA = &ada[offset])
                    fixed (float* pG = &grad[offset])
                    {
                        if (FastVector.IsAvx)
                        {
                            AvxUtils.ApplyAdaGrad(pA, pG, cnt);
                        }
                        else
                        {
                            SseUtils.ApplyAdaGrad(pA, pG, cnt);
                        }
                    }
                });
            }
        }

        public static void ScaleVector(float[] values, int length, float weight)
        {
            int parallelSize = 1024 * 32;
            int parrallelNum = (length + parallelSize - 1) / parallelSize;
            if (parrallelNum < 2)
            {
                fixed (float* pValues = &values[0])
                {
                    SseUtils.Scale(weight, pValues, length);
                }
            }
            else
            {
                Parallel.For(0, parrallelNum, i =>
                {
                    int offset = i * parallelSize;
                    int cnt = Math.Min(parallelSize, length - offset);
                    fixed (float* pValues = &values[offset])
                    {
                        SseUtils.Scale(weight, pValues, cnt);
                    }
                });
            }
        }

        public static void ApplySDCAL1Update(float primalUpdate, float[] values, float l1Threshold, FastVector denseWeights, FastVector weights)
        {
            fixed (float* pValues = &values[0])
            {
                SseUtils.SDCAL1Update(primalUpdate, pValues, l1Threshold, denseWeights.pinnedBasePtr, weights.pinnedBasePtr, values.Length);
            }
        }

        public static void ApplySDCAL1Update(float primalUpdate, float[] values, int[] indices, float l1Threshold, FastVector denseWeights, FastVector weights)
        {
            fixed (float* pValues = &values[0])
            fixed (int* pIndices = &indices[0])
            {
                SseUtils.SDCAL1Update(primalUpdate, pValues, pIndices, l1Threshold, denseWeights.pinnedBasePtr, weights.pinnedBasePtr, indices.Length);
            }
        }

        #endregion

        #region Matrix computation
        public static void MatrixMulRowMajor(FastVector left, FastVector right, FastVector dst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, bool transLeft, bool transRight, float w1, float w2)
        {
            if (!transLeft)
            {
                if (!transRight)
                {
                    SseUtils.ApplyMatMul(left.pinnedBasePtr, right.pinnedBasePtr, dst.pinnedBasePtr, dstRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
                }
                else
                {
                    SseUtils.ApplyMatMulTR(left.pinnedBasePtr, right.pinnedBasePtr, dst.pinnedBasePtr, dstRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
                }
            }
            else
            {
                if (!transRight)
                {
                    if (intermediateColumnCnt == 1 && w1 == 1)
                    {
                        SseUtils.ApplyOutterProduct(left.pinnedBasePtr, right.pinnedBasePtr, dst.pinnedBasePtr, dstRowCnt, dstColumnCnt, w2);
                    }
                    else
                    {
                        SseUtils.ApplyMatMulTL(left.pinnedBasePtr, right.pinnedBasePtr, dst.pinnedBasePtr, dstRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
                    }
                }
                else
                {
                    SseUtils.ApplyMatMulTLTR(left.pinnedBasePtr, right.pinnedBasePtr, dst.pinnedBasePtr, dstRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
                }
            }
        }

        public static void MatrixMulRowMajor(float* pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, bool transLeft, bool transRight, float w1, float w2)
        {
            if (!transLeft)
            {
                int blockSize = 32;
                int blockNum = (dstRowCnt + blockSize - 1) / blockSize;
                if (!transRight)
                {
                    Parallel.For(0, blockNum, (i) =>
                    {
                        int offset = i * blockSize;
                        if (offset >= dstRowCnt)
                        {
                            return;
                        }

                        int subRowCnt = Math.Min(blockSize, dstRowCnt - offset);

                        SseUtils.ApplyMatMul(pLeft + offset * intermediateColumnCnt, pRight, pDst + offset * dstColumnCnt, subRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
                    });
                }
                else
                {
                    Parallel.For(0, blockNum, (i) =>
                    {
                        int offset = i * blockSize;
                        if (offset >= dstRowCnt)
                        {
                            return;
                        }

                        int subRowCnt = Math.Min(blockSize, dstRowCnt - offset);
                        SseUtils.ApplyMatMulTR(pLeft + offset * intermediateColumnCnt, pRight, pDst + offset * dstColumnCnt, subRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
                    });
                }
            }
            else
            {
                int vBlockSize = 32;
                int blockNum = (dstColumnCnt + vBlockSize - 1) / vBlockSize;

                if (!transRight)
                {
                    if (intermediateColumnCnt == 1 && w1 == 1)
                    {
                        SseUtils.ApplyOutterProduct(pLeft, pRight, pDst, dstRowCnt, dstColumnCnt, w2);
                    }
                    else
                    {
                        Parallel.For(0, blockNum, i =>
                        {
                            int vOffset = i * vBlockSize;
                            if (vOffset >= dstColumnCnt)
                            {
                                return;
                            }

                            int subColCnt = Math.Min(vBlockSize, dstColumnCnt - vOffset);

                            int hBlockSize = 128;
                            int hBlockNum = (intermediateColumnCnt + hBlockSize - 1) / hBlockSize;
                            object lockObj = new object();
                            float[][] tmpDst = new float[hBlockNum][];
                            Parallel.For(0, hBlockNum, new ParallelOptions(), (h) =>
                              {
                                  tmpDst[h] = new float[dstRowCnt * subColCnt];
                                  int hOffset = h * hBlockSize;
                                  if (hOffset >= intermediateColumnCnt)
                                  {
                                      return;
                                  }

                                  int hSubRowCnt = Math.Min(hBlockSize, intermediateColumnCnt - hOffset);

                                  fixed (float* pTmp = tmpDst[h])
                                  {
                                      SseUtils.ApplyMatMulTL(pLeft + hOffset * dstRowCnt, pRight + vOffset + hOffset * dstColumnCnt, pTmp, dstRowCnt, hSubRowCnt, subColCnt, 0, w2, dstRowCnt, dstColumnCnt, subColCnt);
                                  }
                              });

                            for (int h = 0; h < hBlockNum; h++)
                            {
                                fixed (float* pTmp = tmpDst[h])
                                {
                                    for (int k = 0; k < dstRowCnt; k++)
                                    {
                                        int dstOff = k * dstColumnCnt + vOffset;
                                        int tmpOff = k * subColCnt;
                                        for (int l = 0; l < subColCnt; l++)
                                        {
                                            pDst[dstOff + l] += pTmp[tmpOff + l];
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
                else
                {
                    SseUtils.ApplyMatMulTLTR(pLeft, pRight, pDst, dstRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
                }
            }
        }

        public static void MatrixAddActivation(FastVector matA, FastVector vecB, int leftRowCnt, int leftColumnCnt, int activation)
        {
            SseUtils.ApplyMatrixAddActivation(matA.pinnedBasePtr, vecB.pinnedBasePtr, leftRowCnt, leftColumnCnt, activation);
        }

        public static void MatrixAggragateRowwise(FastVector matA, FastVector vecB, int leftRowCnt, int leftColumnCnt, float w1, float w2)
        {
            SseUtils.ApplyMatrixAggragateRowwise(matA.pinnedBasePtr, vecB.pinnedBasePtr, leftRowCnt, leftColumnCnt, w1, w2);
        }
        #endregion

        public static void DerivCrossEntropy(FastVector outputScore, FastVector outputLabel, FastVector outputDeriv, int outputSize, float missingLabel = 10001)
        {
            SseUtils.DerivCrossEntropy(outputScore.pinnedBasePtr, outputLabel.pinnedBasePtr, outputDeriv.pinnedBasePtr, outputSize, missingLabel);
        }

        #region DNN
        public static void RecursiveBackwardPropagateBatch(int[] InstanceIndex, int batchSize,
                            float[] SeqOutputMatrixs, int dim,
                            float[] WMatrix, int lag, int activationFunction, float[] SeqDerivMatrixs)
        {
            FastVector SeqOutputMatrixVec = FastVector.Create(SeqOutputMatrixs);
            FastVector SeqDerivMatrixVec = FastVector.Create(SeqDerivMatrixs);
            FastVector WMatrixVec = FastVector.Create(WMatrix);
            float* pSeqOutputMatrixs = SeqOutputMatrixVec.GetPinnedBasePtr();
            float* pSeqDerivMatrixs = SeqDerivMatrixVec.GetPinnedBasePtr();
            float* pWMatrixVec = WMatrixVec.GetPinnedBasePtr();
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = FastVector.MaxDegreeOfParallelism }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqLen = InstanceIndex[idx] - seqBegin;
                for (int h = seqLen - 1; h >= 0; h--)
                {
                    int seqIdx = seqBegin + h;
                    int cId = seqIdx * dim;
                    float* pSeqDerivVec = pSeqDerivMatrixs + cId;
                    float* pSeqOutVec = pSeqOutputMatrixs + cId;

                    if (activationFunction == 1)
                    {
                        SseUtils.ApplyTanhDerivative(pSeqOutVec, pSeqDerivVec, dim);
                    }
                    else if (activationFunction == 2)
                    {
                        SseUtils.ApplyRectifiedLinearDerivative(pSeqOutVec, pSeqDerivVec, dim);
                    }
                    else if (activationFunction == 3)
                    {
                        SseUtils.ApplySigmoidDerivative(pSeqOutVec, pSeqDerivVec, dim);
                    }

                    int acLag = Math.Min(h, lag);
                    for (int l = 0; l < acLag; l++)
                    {
                        int cursor = h - l - 1;
                        int hStart = l * dim * dim;
                        int hId = (seqBegin + cursor) * dim;
                        float* pSeqHisDerivVec = pSeqDerivMatrixs + hId;//, dim, false);
                        float* pWeightMatrixVec = pWMatrixVec + l * dim * dim; //, dim * dim, false);
                        SseUtils.ApplyMatMulTR(pSeqDerivVec, pWeightMatrixVec, pSeqHisDerivVec, 1, dim, dim, 1, 1);
                    }
                }
            });
        }

        public static void RecursiveForwardPropagateBatch(int[] InstanceIndex, int batchSize,
                            float[] SeqInputMatrixs, int dim,
                            float[] WMatrix, int lag, int activationFunction, float[] SeqOutputMatrix)
        {
            FastVector SeqOutputMatrixVec = FastVector.Create(SeqOutputMatrix);
            FastVector SeqInputMatrixVec = FastVector.Create(SeqInputMatrixs);
            FastVector WMatrixVec = FastVector.Create(WMatrix);

            float* pSeqOutputMatrix = SeqOutputMatrixVec.GetPinnedBasePtr();
            float* pSeqInputMatrixs = SeqInputMatrixVec.GetPinnedBasePtr();
            float* pWMatrix = WMatrixVec.GetPinnedBasePtr();

            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = FastVector.MaxDegreeOfParallelism }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqEnd = InstanceIndex[idx];
                for (int h = seqBegin; h < seqEnd; h++)
                {
                    int outStart = h * dim;
                    int acLag = Math.Min(h - seqBegin, lag);
                    float* pSeqOutputVec = pSeqOutputMatrix + h * dim;//, dim, false);
                    for (int l = 0; l < acLag; l++)
                    {
                        float* pSeqInputVec = pSeqInputMatrixs + (h - l - 1) * dim;//, dim, false);
                        float* pWeightMatrixVec = pWMatrix + l * dim * dim;//, dim * dim, false);
                        SseUtils.ApplyMatMul(pSeqInputVec, pWeightMatrixVec, pSeqOutputVec, 1, dim, dim, 1, 1);
                    }

                    // Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3 };
                    if (activationFunction == 1)
                    {
                        SseUtils.ApplyTanh(pSeqOutputVec, pSeqOutputVec, dim);
                    }
                    else if (activationFunction == 2)
                    {
                        SseUtils.ApplyRectifiedLinear(pSeqOutputVec, pSeqOutputVec, dim);
                    }
                    else if (activationFunction == 3)
                    {
                        SseUtils.ApplySigmoid(pSeqOutputVec, pSeqOutputVec, dim);
                    }
                }
            });
        }

        public static void MatrixMultiplicationSparseLeftTranspose(int[] pRowIndex, int RowNum, int[] pColumnIndex, float[] pValue, int elementsize,
float[] pRight, float[] pDst, int leftColumnCnt, int rightColumnCnt, float wei)
        {
            if (rightColumnCnt >= 512 || RowNum < 2048)
            {
                MatrixMultiplicationSparseLeftTransposeVertical(pRowIndex, RowNum, pColumnIndex, pValue, elementsize, pRight, pDst, leftColumnCnt, rightColumnCnt, wei);
            }
            else
            {
                MatrixMultiplicationSparseLeftTransposeHorizal(pRowIndex, RowNum, pColumnIndex, pValue, elementsize, pRight, pDst, leftColumnCnt, rightColumnCnt, wei);
            }
        }

        public static void MatrixMultiplicationSparseLeftTransposeVertical(int[] pRowIndex, int RowNum, int[] pColumnIndex, float[] pValue, int elementsize,
float[] pRight, float[] pDst, int leftColumnCnt, int rightColumnCnt, float wei)
        {
            int strideSize = 32;
            int cnt = (rightColumnCnt + strideSize - 1) / strideSize;

            Parallel.For(0, cnt, new ParallelOptions { MaxDegreeOfParallelism = FastVector.MaxDegreeOfParallelism }, c =>
            {
                int offset = (int)c * strideSize;
                int columns = Math.Min(strideSize, rightColumnCnt - offset);
                fixed (int* pIndex = &pRowIndex[0])
                fixed (int* pColumn = &pColumnIndex[0])
                fixed (float* pLeftValues = &pValue[0])
                fixed (float* pRightMat = &pRight[offset])
                fixed (float* pDstMat = &pDst[offset])
                {
                    SseUtils.ApplyMatMulAddSparseLeftTL(pIndex, pColumn, pLeftValues, pRightMat, pDstMat, leftColumnCnt, RowNum, columns, 1, wei, rightColumnCnt, rightColumnCnt);
                }
            });
        }

        public static void MatrixMultiplicationSparseLeftTransposeHorizal(int[] pRowIndex, int RowNum, int[] pColumnIndex, float[] pValue, int elementsize,
float[] pRight, float[] pDst, int leftColumnCnt, int rightColumnCnt, float wei)
        {
            int batchCnt = FastVector.MaxDegreeOfParallelism;
            int batchSize = (RowNum + batchCnt - 1) / batchCnt;

            Parallel.For(0, batchCnt, new ParallelOptions { MaxDegreeOfParallelism = FastVector.MaxDegreeOfParallelism }, () =>
            {
                return FastVector.Create(pDst.Length);
            }, (c, s, gradLocal) =>
            {
                int offset = (int)c * batchSize;
                if (offset >= RowNum)
                {
                    return gradLocal;
                }

                int rows = Math.Min(batchSize, RowNum - offset);
                if (c > 0)
                {
                    rows = -rows;
                }
                float* pDstMat = gradLocal.GetPinnedBasePtr();
                fixed (int* pIndex = &pRowIndex[offset])
                fixed (int* pColumn = &pColumnIndex[0])
                fixed (float* pLeftValues = &pValue[0])
                fixed (float* pRightMat = &pRight[offset * rightColumnCnt])
                {
                    SseUtils.ApplyMatMulAddSparseLeftTL(pIndex, pColumn, pLeftValues, pRightMat, pDstMat, leftColumnCnt, rows, rightColumnCnt, 1.0f, wei, rightColumnCnt, rightColumnCnt);
                }

                return gradLocal;
            }, (gradLocal) =>
             {
                 fixed (float* pGradient = pDst)
                 {
                     SseUtils.ApplyAddScale(pGradient, gradLocal.GetPinnedBasePtr(), 1, pDst.Length);
                 }
             });
        }

        public static void RecursiveUpdateBatch(int[] InstanceIndex, int batchSize, int seqSize, float[] SeqOutputMatrixs, int dim, float[] SeqDerivMatrixs, int lag, float[] gradient, float weight)
        {
            if (gradient.Length == 0 || lag == 0)
            {
                return;
            }

            FastVector SeqOutputMatrixVec = FastVector.Create(SeqOutputMatrixs);
            FastVector SeqDerivMatrixVec = FastVector.Create(SeqDerivMatrixs);
            FastVector GradientVec = FastVector.Create(gradient);

            float* pSeqOutputMatrix = SeqOutputMatrixVec.GetPinnedBasePtr();
            float* pSeqDerivMatrix = SeqDerivMatrixVec.GetPinnedBasePtr();
            float* pGradient = GradientVec.GetPinnedBasePtr();

            int blockSize = 16;
            int blockNum = (batchSize + blockSize - 1) / blockSize;
            Parallel.For(0, blockNum, new ParallelOptions { MaxDegreeOfParallelism = FastVector.MaxDegreeOfParallelism }, () =>
                {
                    return FastVector.Create(gradient.Length);
                },
                (block, s, gradLocal) =>
                {
                    int offset = block * blockSize;
                    int up = Math.Min(offset + blockSize, batchSize);
                    float* pGradLocal = gradLocal.GetPinnedBasePtr();
                    int seqBegin = offset == 0 ? 0 : InstanceIndex[offset - 1];
                    for (int idx = offset; idx < up; idx++)
                    {
                        int seqEnd = InstanceIndex[idx];
                        for (int i = 0; i < lag; i++)
                        {
                            float* pGradVec = pGradLocal + i * dim * dim;//, dim * dim, false);   
                            float* pSeqDerivVec = pSeqDerivMatrix + (seqBegin + i + 1) * dim;//, dim, false);
                            float* pSeqOutVec = pSeqOutputMatrix + seqBegin * dim;//, dim, false);

                            for (int h = seqBegin + i + 1; h < seqEnd; h++)
                            {
                                //SseUtils.ApplyMatMulTL(pSeqOutVec, pSeqDerivVec, pGradVec, dim, 1, dim, 1.0f, weight);
                                SseUtils.ApplyOutterProduct(pSeqOutVec, pSeqDerivVec, pGradVec, dim, dim, weight);
                                pSeqDerivVec += dim;
                                pSeqOutVec += dim;
                            }
                        }

                        seqBegin = seqEnd;
                    }

                    return gradLocal;
                },
                (gradLocal) =>
                {
                    //lock (GradientVec)
                    {
                        SseUtils.Add(pGradient, gradLocal.GetPinnedBasePtr(), pGradient, gradient.Length);
                    }
                });
        }
        #endregion

        #region CDSSM
        /// <summary>
        /// A*B
        /// </summary>
        /// <param name="Smp_Index"></param>
        /// <param name="batchsize"></param>
        /// <param name="Seg_Index"></param>
        /// <param name="Seg_Margin"></param>
        /// <param name="seg_size"></param>
        /// <param name="Fea_Index"></param>
        /// <param name="Fea_Value"></param>
        /// <param name="elementsize"></param>
        /// <param name="mul_weight"></param>
        /// <param name="output"></param>
        /// <param name="inputDimension"></param>
        /// <param name="outputDimension"></param>
        /// <param name="winSize"></param>
        public static void Convolution_Sparse_Matrix_Multiply_INTEX(int[] Smp_Index, int batchsize, int[] Seg_Index, int[] Seg_Margin, int seg_size, int[] Fea_Index, float[] Fea_Value, int elementsize, float[] mul_weight, float[] output, int inputDimension, int outputDimension, int winSize)
        {
            int THREAD_NUM = 32;
            int total = outputDimension * seg_size;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, seg_size, new ParallelOptions() { MaxDegreeOfParallelism = THREAD_NUM }, sed_idx =>
            {
                int ws = winSize / 2;
                int mSmp_idx = Seg_Margin[sed_idx];
                int outRowIdx = sed_idx * outputDimension;
                for (int idx = 0; idx < outputDimension; idx++)
                {
                    output[outRowIdx + idx] = 0;
                }

                for (int w = -ws; w <= ws; w++)
                {
                    if (sed_idx + w >= 0 && sed_idx + w < seg_size)
                    {
                        if (Seg_Margin[sed_idx + w] == mSmp_idx)
                        {
                            int row = sed_idx + w; // idx / n;
                            int col_end = Seg_Index[row];
                            int col_begin = 0;
                            if (row > 0)
                            {
                                col_begin = Seg_Index[row - 1];
                            }

                            fixed (int* pIndex = &Seg_Index[row])
                            fixed (int* pColumn = &Fea_Index[0])
                            fixed (float* pLeftValues = &Fea_Value[0])
                            fixed (float* pRightMat = &mul_weight[(w + ws) * inputDimension * outputDimension])
                            fixed (float* pDstMat = &output[outRowIdx])
                            {
                                SseUtils.ApplyMatMulAddSparseLeft(col_begin, pIndex, pColumn, pLeftValues, pRightMat, pDstMat, 1, inputDimension, outputDimension, 1, 1, outputDimension, outputDimension);
                            }
                        }
                    }
                }
            });
        }

        public static void Convolution_Sparse_Matrix_Multiply_INTEX(int* Smp_Index, int batchsize, int* Seg_Index, int* Seg_Margin, int seg_size,
            int* Fea_Index, float* Fea_Value, int elementsize, float* mul_weight, float* output, int inputDimension, int outputDimension, int winSize)
        {
            int THREAD_NUM = 32;
            int total = outputDimension * seg_size;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, seg_size, new ParallelOptions() { MaxDegreeOfParallelism = THREAD_NUM }, sed_idx =>
            {
                int ws = winSize / 2;
                int mSmp_idx = Seg_Margin[sed_idx];
                int outRowIdx = sed_idx * outputDimension;
                for (int idx = 0; idx < outputDimension; idx++)
                {
                    output[outRowIdx + idx] = 0;
                }

                for (int w = -ws; w <= ws; w++)
                {
                    if (sed_idx + w >= 0 && sed_idx + w < seg_size)
                    {
                        if (Seg_Margin[sed_idx + w] == mSmp_idx)
                        {
                            int row = sed_idx + w; // idx / n;
                            int col_end = Seg_Index[row];
                            int col_begin = 0;
                            if (row > 0)
                            {
                                col_begin = Seg_Index[row - 1];
                            }

                            int* pIndex = &Seg_Index[row];
                            int* pColumn = &Fea_Index[0];
                            float* pLeftValues = &Fea_Value[0];
                            float* pRightMat = &mul_weight[(w + ws) * inputDimension * outputDimension];
                            float* pDstMat = &output[outRowIdx];
                            SseUtils.ApplyMatMulAddSparseLeft(col_begin, pIndex, pColumn, pLeftValues, pRightMat, pDstMat, 1, inputDimension, outputDimension, 1, 1, outputDimension, outputDimension);
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Grad = Grad + learnRate*(Fea'*Deriv)
        /// </summary>
        /// <param name="deriv"></param>
        /// <param name="maxpooling_index"></param>
        /// <param name="Seg_Index"></param>
        /// <param name="SegMargin_Index"></param>
        /// <param name="seg_size"></param>
        /// <param name="win_size"></param>
        /// <param name="batchsize"></param>
        /// <param name="output_dimension"></param>
        /// <param name="Fea_Index"></param>
        /// <param name="Fea_Value"></param>
        /// <param name="grad"></param>
        /// <param name="Feature_Dimension"></param>
        /// <param name="learnRate"></param>
        public static void Convolution_Sparse_Matrix_Product_INTEX_Weight(
float[] deriv, int[] maxpooling_index, int[] Seg_Index, int[] SegMargin_Index, int seg_size, int win_size,
        int batchsize, int output_dimension, int[] Fea_Index, float[] Fea_Value, float[] grad, int Feature_Dimension, float learnRate)
        {
            int threadNum = 32;
            int outputSizePerBlock = 32;
            int outputBlockNum = (output_dimension + outputSizePerBlock - 1) / outputSizePerBlock;

            Parallel.For(0, batchsize, new ParallelOptions { MaxDegreeOfParallelism = threadNum }, batchIndex =>
            {
                int sampleOffset = batchIndex * output_dimension;
                for (int out_idx = 0; out_idx < output_dimension;)
                {
                    int target_seg = maxpooling_index[sampleOffset + out_idx];
                    int out_inner_idx_start = out_idx;
                    while (out_idx < output_dimension && (maxpooling_index[sampleOffset + out_idx] == target_seg))
                    {
                        out_idx++;
                    }

                    int target_smp = SegMargin_Index[target_seg];

                    for (int win_idx = 0; win_idx < win_size; win_idx++)
                    {
                        int ws = win_size / 2;
                        int w = win_idx - ws;
                        int row = target_seg + w; // idx / n;
                        if (row >= 0 && row < seg_size && SegMargin_Index[row] == target_smp)
                        {
                            int gradOffset = Feature_Dimension * win_idx * output_dimension;

                            int col_end = Seg_Index[row];
                            int col_begin = 0;
                            if (row > 0)
                            {
                                col_begin = Seg_Index[row - 1];
                            }

                            for (int output_idx = out_inner_idx_start; output_idx < out_idx; output_idx++)
                            {
                                float d = learnRate * deriv[sampleOffset + output_idx];
                                for (int i = col_begin; i < col_end; i++)
                                {
                                    int fea_idx = Fea_Index[i];
                                    float v = Fea_Value[i];
                                    int goff = gradOffset + fea_idx * output_dimension;
                                    grad[goff + output_idx] += v * d;
                                }
                            }
                        }
                    }
                }
            });
        }

        public static void Convolution_Sparse_Matrix_Product_INTEX_Weight(
float* deriv, int* maxpooling_index, int* Seg_Index, int* SegMargin_Index, int seg_size, int win_size,
        int batchsize, int output_dimension, int* Fea_Index, float* Fea_Value, float* grad, int Feature_Dimension, float learnRate)
        {
            int threadNum = 32;
            int outputSizePerBlock = 32;
            int outputBlockNum = (output_dimension + outputSizePerBlock - 1) / outputSizePerBlock;

            Parallel.For(0, outputBlockNum * win_size, new ParallelOptions { MaxDegreeOfParallelism = threadNum }, idx =>
            {
                int block_idx = idx / win_size;
                int win_idx = idx % win_size;
                int gradOffset = win_idx * Feature_Dimension * output_dimension;
                int outputIndexStart = block_idx * outputSizePerBlock;
                int outputIndexEnd = Math.Min((block_idx + 1) * outputSizePerBlock, output_dimension);
                //float sum = 0;
                for (int batchIndex = 0; batchIndex < batchsize; batchIndex++)
                {
                    int sampleOffset = batchIndex * output_dimension;
                    for (int out_idx = outputIndexStart; out_idx < outputIndexEnd;)
                    {
                        int target_seg = maxpooling_index[sampleOffset + out_idx];
                        int out_inner_idx_start = out_idx;
                        while (out_idx < outputIndexEnd && (maxpooling_index[sampleOffset + out_idx] == target_seg))
                        {
                            out_idx++;
                        }

                        int target_smp = SegMargin_Index[target_seg];
                        int ws = win_size / 2;
                        int w = win_idx - ws;
                        int row = target_seg + w; // idx / n;
                        if (row >= 0 && row < seg_size)
                        {
                            if (SegMargin_Index[row] == target_smp)
                            {
                                int col_end = Seg_Index[row];
                                int col_begin = 0;
                                if (row > 0) col_begin = Seg_Index[row - 1];
                                for (int output_idx = out_inner_idx_start; output_idx < out_idx; output_idx++)
                                {
                                    float d = learnRate * deriv[sampleOffset + output_idx];
                                    for (int i = col_begin; i < col_end; i++)
                                    {
                                        int fea_idx = Fea_Index[i];
                                        float v = Fea_Value[i];
                                        int goff = gradOffset + fea_idx * output_dimension + output_idx;
                                        grad[goff] += v * d;
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }

        public static void Add_Vector(float[] a, int offsetA, float[] b, int offsetB, int size, float aWeight, float bWeight)
        {
            fixed (float* lptr = &a[offsetA])
            fixed (float* rptr = &b[offsetB])
            {
                SseUtils.ApplyAddScale(lptr, rptr, aWeight, bWeight, lptr, size);
            }
        }

        public static void Add_Vector(float[] a, float[] b, int size, float aWeight, float bWeight)
        {
            if (a == null || b == null || a.Length == 0 || b.Length == 0 || size == 0)
            {
                return;
            }

            int parallelSize = 1024 * 64;
            int parrallelNum = (size + parallelSize - 1) / parallelSize;
            if (parrallelNum < 2)
            {
                fixed (float* pA = &a[0])
                fixed (float* pB = &b[0])
                {
                    SseUtils.ApplyAddScale(pA, pB, aWeight, bWeight, pA, size);
                }
            }
            else
            {
                Parallel.For(0, parrallelNum, i =>
                    {
                        int offset = i * parallelSize;
                        int cnt = Math.Min(parallelSize, size - offset);
                        fixed (float* pA = &a[offset])
                        fixed (float* pB = &b[offset])
                        {
                            SseUtils.ApplyAddScale(pA, pB, aWeight, bWeight, pA, cnt);
                        }
                    });
            }
        }

        public static void Add_Vector(float* pA, float* pB, int size)
        {
            if (pA == null || pB == null || size == 0)
            {
                return;
            }

            int parallelSize = 1024 * 64;
            int parrallelNum = (size + parallelSize - 1) / parallelSize;
            if (parrallelNum < 2)
            {
                SseUtils.ApplyAddScale(pA, pB, 1.0f, size);
            }
            else
            {
                Parallel.For(0, parrallelNum, i =>
                {
                    int offset = i * parallelSize;
                    int cnt = Math.Min(parallelSize, size - offset);
                    SseUtils.ApplyAddScale(pA + offset, pB + offset, 1.0f, cnt);
                });
            }
        }

        public static void Add_VectorAndClear(float* pA, float* pB, int size)
        {
            if (pA == null || pB == null || size == 0)
            {
                return;
            }

            int parallelSize = 1024 * 64;
            int parrallelNum = (size + parallelSize - 1) / parallelSize;
            if (parrallelNum < 2)
            {
                SseUtils.ApplyAddAndClearRight(pA, pB, 1.0f, size);
            }
            else
            {
                Parallel.For(0, parrallelNum, i =>
                {
                    int offset = i * parallelSize;
                    int cnt = Math.Min(parallelSize, size - offset);
                    SseUtils.ApplyAddAndClearRight(pA + offset, pB + offset, 1.0f, cnt);
                });
            }
        }

        public static void AddAndClear(float[] A, float[] B, int size, float bwei)
        {
            if (A == null || B == null || size == 0)
            {
                return;
            }

            int parallelSize = 1024 * 64;
            int parrallelNum = (size + parallelSize - 1) / parallelSize;
            if (parrallelNum < 2)
            {
                fixed (float* pA = A)
                fixed (float* pB = B)
                {
                    SseUtils.ApplyAddAndClearRight(pA, pB, bwei, size);
                }
            }
            else
            {
                Parallel.For(0, parrallelNum, i =>
                {
                    int offset = i * parallelSize;
                    int cnt = Math.Min(parallelSize, size - offset);
                    fixed (float* pA = A)
                    fixed (float* pB = B)
                    {
                        SseUtils.ApplyAddAndClearRight(pA + offset, pB + offset, bwei, cnt);
                    }
                });
            }
        }

        public static void ZeroItems(float[] a, int size)
        {
            if (a == null || a.Length == 0 || size == 0)
            {
                return;
            }

            int parallelSize = 1024 * 128;
            int parrallelNum = (size + parallelSize - 1) / parallelSize;
            if (parrallelNum < 2)
            {
                fixed (float* pA = &a[0])
                {
                    SseUtils.ApplyZeroItems(pA, size);
                }
            }
            else
            {
                Parallel.For(0, parrallelNum, i =>
                {
                    int offset = i * parallelSize;
                    int cnt = Math.Min(parallelSize, size - offset);
                    fixed (float* pA = &a[offset])
                    {
                        SseUtils.ApplyZeroItems(pA, cnt);
                    }
                });
            }
        }
        #endregion

        #region LSTM

        /// <summary>
        /// Note that @ denotes the element-wise product
        /// </summary>
        /// <param name="InstanceIndex"></param>
        /// <param name="batchSize"></param>
        /// <param name="o"></param>
        /// <param name="gate_i"></param>
        /// <param name="c_hat"></param>
        /// <param name="gate_f"></param>
        /// <param name="c"></param>
        /// <param name="gate_o"></param>
        /// <param name="tanhc"></param>
        /// <param name="cell"></param>
        /// <param name="Ui"></param>
        /// <param name="Uc"></param>
        /// <param name="Uf"></param>
        /// <param name="Uo"></param>
        /// <param name="Vo"></param>
        public static void LSTMForwardPropagateBatch(int[] InstanceIndex, int batchSize,  //->x;
            float[] o, //-> output,
            float[] gate_i, //->gate i,
            float[] c_hat, //->candidate memory,
            float[] gate_f, //->forget gate,
            float[] c, //->memory,
            float[] gate_o, //->gate o,
            float[] tanhc, //->tanh memory
            int cell,
            float[] Ui,
            float[] Uc,
            float[] Uf,
            float[] Uo, float[] Vo)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqEnd = InstanceIndex[idx];
                fixed (float* pO = o)
                fixed (float* pGate_i = gate_i)
                fixed (float* pC_hat = c_hat)
                fixed (float* pGate_f = gate_f)
                fixed (float* pC = c)
                fixed (float* pGate_o = gate_o)
                fixed (float* pTanhC = tanhc)
                fixed (float* pUi = Ui)
                fixed (float* pUc = Uc)
                fixed (float* pUf = Uf)
                fixed (float* pUo = Uo)
                fixed (float* pVo = Vo)
                {
                    ///time step
                    for (int t = seqBegin; t < seqEnd; t++)
                    {
                        if (t > seqBegin)
                        {
                            //gate_i += U_{i} * o_{t-1}
                            SseUtils.ApplyMatMul(pO + (t - 1) * cell, pUi, pGate_i + t * cell, 1, cell, cell, 1, 1);
                            //gate_f += U_{f} * o_{t-1}
                            SseUtils.ApplyMatMul(pO + (t - 1) * cell, pUf, pGate_f + t * cell, 1, cell, cell, 1, 1);
                            //c_hat += U_{c} * o_{t-1}
                            SseUtils.ApplyMatMul(pO + (t - 1) * cell, pUc, pC_hat + t * cell, 1, cell, cell, 1, 1);
                            //gate_o += U_{o} * o_{t-1}
                            SseUtils.ApplyMatMul(pO + (t - 1) * cell, pUo, pGate_o + t * cell, 1, cell, cell, 1, 1);
                        }

                        //gate_i = sigmoid(gate_i)
                        SseUtils.ApplySigmoid(pGate_i + t * cell, pGate_i + t * cell, cell);
                        //gate_f = sigmoid(gate_f)
                        SseUtils.ApplySigmoid(pGate_f + t * cell, pGate_f + t * cell, cell);
                        //c_hat = tanh(c_hat)
                        SseUtils.ApplyTanh(pC_hat + t * cell, pC_hat + t * cell, cell);
                        //c = gate_i @ c_hat
                        SseUtils.ApplyMultiply(pGate_i + t * cell, pC_hat + t * cell, pC + t * cell, cell);
                        //c += gate_f @ c(t-1)
                        if (t > seqBegin)
                        {
                            SseUtils.ApplyMultiplyAdd(pGate_f + t * cell, pC + (t - 1) * cell, pC + t * cell, cell, 1);
                        }

                        //gate_o += Vo * c(t)
                        SseUtils.ApplyMatMul(pC + t * cell, pVo, pGate_o + t * cell, 1, cell, cell, 1, 1);

                        //gate_o = simgoid(gate_o)
                        SseUtils.ApplySigmoid(pGate_o + t * cell, pGate_o + t * cell, cell);

                        //tanhc = tanh(c)
                        SseUtils.ApplyTanh(pC + t * cell, pTanhC + t * cell, cell);

                        //o = gate_o @ tanhc
                        SseUtils.ApplyMultiply(pGate_o + t * cell, pTanhC + t * cell, pO + t * cell, cell);
                    }
                }
            });
        }

        public static void LSTMBackwardPropagateBatch(int[] InstanceIndex, int batchSize, int seqSize, int cell,
            float[] o, float[] gate_i, float[] c_hat, float[] gate_f, float[] c, float[] gate_o, float[] tanhc,
            float[] deriv_o, float[] deriv_gate_i, float[] deriv_cHat, float[] deriv_gate_f, float[] deriv_c, float[] deriv_gate_o, float[] deriv_tanhc,
            float[] Ui, float[] Uc, float[] Uf, float[] Uo, float[] Vo)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqEnd = InstanceIndex[idx];
                fixed (float* pO = o)
                fixed (float* pGate_i = gate_i)
                fixed (float* pC_hat = c_hat)
                fixed (float* pGate_f = gate_f)
                fixed (float* pC = c)
                fixed (float* pGate_o = gate_o)
                fixed (float* pTanhC = tanhc)
                fixed (float* pUi = Ui)
                fixed (float* pUc = Uc)
                fixed (float* pUf = Uf)
                fixed (float* pUo = Uo)
                fixed (float* pVo = Vo)
                fixed (float* pDeriv_o = deriv_o)
                fixed (float* pDeriv_cHat = deriv_cHat)
                fixed (float* pDeriv_c = deriv_c)
                fixed (float* pDeriv_Gate_i = deriv_gate_i)
                fixed (float* pDeriv_Gate_f = deriv_gate_f)
                fixed (float* pDeriv_Gate_o = deriv_gate_o)
                fixed (float* pDeriv_tanchc = deriv_tanhc)
                {
                    ///time step
                    for (int t = seqEnd - 1; t >= seqBegin; t--)
                    {
                        // deriv_gate_o = deriv_o @ tanhc
                        SseUtils.ApplyMultiply(pDeriv_o + t * cell, pTanhC + t * cell, pDeriv_Gate_o + t * cell, cell);

                        // deriv_gate_o = deriv_gate_o *  gate_o * (1 - gate_o)
                        SseUtils.ApplySigmoidDerivative(pGate_o + t * cell, pDeriv_Gate_o + t * cell, cell);

                        //deriv_tanhc = deriv_o @ gate_o
                        SseUtils.ApplyMultiply(pDeriv_o + t * cell, pGate_o + t * cell, pDeriv_tanchc + t * cell, cell);

                        // deriv_tanhc = deriv_tanhc * (1 + tanhc) * ( 1- tanhc);
                        SseUtils.ApplyTanhDerivative(pTanhC + t * cell, pDeriv_tanchc + t * cell, cell);

                        if (t > seqBegin)
                        {
                            // deriv_o(t-1) += deriv_gate_o * Uo
                            SseUtils.ApplyMatMulTR(pDeriv_Gate_o + t * cell, pUo, pDeriv_o + (t - 1) * cell, 1, cell, cell, 1, 1);
                        }

                        // deriv_c += deriv_gate_o * Vo
                        SseUtils.ApplyMatMulTR(pDeriv_Gate_o + t * cell, pVo, pDeriv_c + t * cell, 1, cell, cell, 1, 1);

                        //deriv_c += deriv_tanhc
                        SseUtils.Add(pDeriv_tanchc + t * cell, pDeriv_c + t * cell, pDeriv_c + t * cell, cell);

                        // deriv_c_hat = deriv_c @ gate_i
                        SseUtils.ApplyMultiply(pDeriv_c + t * cell, pGate_i + t * cell, pDeriv_cHat + t * cell, cell);

                        // deriv_c_hat = deriv_c_hat * (1 + c_hat)*( 1- c_hat)
                        SseUtils.ApplyTanhDerivative(pC_hat + t * cell, pDeriv_cHat + t * cell, cell);

                        //deriv_gate_i = deriv_c @ c_hat
                        SseUtils.ApplyMultiply(pDeriv_c + t * cell, pC_hat + t * cell, pDeriv_Gate_i + t * cell, cell);

                        //deriv_gate_i = deriv_gate_i * gate_i * (1 - gate_i)
                        SseUtils.ApplySigmoidDerivative(pGate_i + t * cell, pDeriv_Gate_i + t * cell, cell);

                        if (t > seqBegin)
                        {
                            //deriv_gate_f = deriv_c * c(t-1)
                            SseUtils.ApplyMultiply(pDeriv_c + t * cell, pC + (t - 1) * cell, pDeriv_Gate_f + t * cell, cell);

                            //deriv_gate_f = deriv_gate_f * (1 -gate_f) * gate_f
                            SseUtils.ApplySigmoidDerivative(pGate_f + t * cell, pDeriv_Gate_f + t * cell, cell);

                            //deriv_o(t -1) += deriv_gate_f * Uf
                            SseUtils.ApplyMatMulTR(pDeriv_Gate_f + t * cell, pUf, pDeriv_o + (t - 1) * cell, 1, cell, cell, 1, 1);

                            //deriv_o(t-1) += deriv_gate_i * Ui
                            SseUtils.ApplyMatMulTR(pDeriv_Gate_i + t * cell, pUi, pDeriv_o + (t - 1) * cell, 1, cell, cell, 1, 1);

                            //deriv_o(t-1) += deriv_cHat(t) * Uc
                            SseUtils.ApplyMatMulTR(pDeriv_cHat + t * cell, pUc, pDeriv_o + (t - 1) * cell, 1, cell, cell, 1, 1);

                            //deriv_c(t-1) += deriv_c @ gate_f
                            SseUtils.ApplyMultiplyAdd(pDeriv_c + t * cell, pGate_f + t * cell, pDeriv_c + (t - 1) * cell, cell, 1);
                        }
                    }
                }
            });
        }

        public static void Clip(float[] vec, int m, float maxThreshold, float minThreshold)
        {
            fixed (float* pVec = vec)
            {
                SseUtils.Clip(pVec, m, minThreshold, maxThreshold);
            }
        }

        public static void ClipAdaGradUpdate(float[] ada, float[] grad, float[] parameters, int m, float updateRate, float gradClip, float weightClip)
        {
            fixed (float* pAda = ada)
            fixed (float* pGrad = grad)
            fixed (float* pParam = parameters)
            {
                SseUtils.ApplyClipAdaGradUpdate(pAda, pGrad, pParam, m, updateRate, gradClip, weightClip);
            }
        }

        public static void ClipAdamUpdate(float[] M, float[] V, float[] grad, float[] param, int dim, float beta1, float beta2, int iter, float updateRate, float gradClip, float weightClip)
        {
            if (dim <= 2048)
            {
                fixed (float* pM = M)
                fixed (float* pV = V)
                fixed (float* pgradient = grad)
                fixed (float* pWeight = param)
                {
                    if (IsAvx)
                    {
                        AvxUtils.ApplyAdamClipUpdate(pM, pV, pgradient, pWeight, dim, beta1, beta2, iter, updateRate, gradClip, weightClip);
                    }
                    else
                    {
                        SseUtils.ApplyAdamClipUpdate(pM, pV, pgradient, pWeight, dim, beta1, beta2, iter, updateRate, gradClip, weightClip);
                    }
                }
            }
            else
            {
                int blockSize = 512;
                int blockNum = (dim + blockSize - 1) / blockSize;
                Parallel.For(0, blockNum, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
                 {
                     int blockIdx = i * blockSize;
                     if (dim <= blockIdx)
                     {
                         return;
                     }

                     int len = Math.Min(dim - blockIdx, blockSize);
                     fixed (float* pM = &M[blockIdx])
                     fixed (float* pV = &V[blockIdx])
                     fixed (float* pgradient = &grad[blockIdx])
                     fixed (float* pWeight = &param[blockIdx])
                     {
                         if (IsAvx)
                         {
                             AvxUtils.ApplyAdamClipUpdate(pM, pV, pgradient, pWeight, len, beta1, beta2, iter, updateRate, gradClip, weightClip);
                         }
                         else
                         {
                             SseUtils.ApplyAdamClipUpdate(pM, pV, pgradient, pWeight, len, beta1, beta2, iter, updateRate, gradClip, weightClip);
                         }
                     }
                 });
            }
        }

        public static void MomentumUpdate(float[] weight, float[] momemtum, int length, float momentumRate)
        {
            if (length <= 1024)
            {
                fixed (float* pMom = momemtum)
                fixed (float* pWeight = weight)
                {
                    SseUtils.ApplyMomemtumUpdate(pWeight, pMom, length, momentumRate);
                }
            }
            else
            {
                int blockSize = 512;
                int blockNum = (length + blockSize - 1) / blockSize;
                Parallel.For(0, blockNum, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
                {
                    int blockIdx = i * blockSize;
                    if (length <= blockIdx)
                    {
                        return;
                    }

                    int len = Math.Min(length - blockIdx, blockSize);
                    fixed (float* pMom = &momemtum[blockIdx])
                    fixed (float* pWeight = &weight[blockIdx])
                    {
                        SseUtils.ApplyMomemtumUpdate(pWeight, pMom, len, momentumRate);
                    }
                });
            }
        }

        public static void NAGUpdate(float[] weight, float[] momemtum, float[] trueWeight, int length, float momentumRate)
        {
            if (length <= 1024)
            {
                fixed (float* pMom = momemtum)
                fixed (float* pWeight = weight)
                fixed (float* pTrueWeight = trueWeight)
                {
                    SseUtils.ApplyNAGUpdate(pWeight, pMom, pTrueWeight, length, momentumRate);
                }
            }
            else
            {
                int blockSize = 512;
                int blockNum = (length + blockSize - 1) / blockSize;
                Parallel.For(0, blockNum, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
                {
                    int blockIdx = i * blockSize;
                    if (length <= blockIdx)
                    {
                        return;
                    }

                    int len = Math.Min(length - blockIdx, blockSize);
                    fixed (float* pMom = &momemtum[blockIdx])
                    fixed (float* pWeight = &weight[blockIdx])
                    fixed (float* pTrueWeight = &trueWeight[blockIdx])
                    {
                        SseUtils.ApplyNAGUpdate(pWeight, pMom, pTrueWeight, len, momentumRate);
                    }
                });
            }
        }

        public static void SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(int[] Smp_Index, int batchsize, int[] Seg_Index,
    int seg_size, int[] Fea_Index, float[] Fea_Value, int elementsize,
    float[] mul_weight, float[] output, int Feature_dimension, int output_dimension, float weight)
        {
            int blockSize = 16;
            int blockNum = (output_dimension + blockSize - 1) / blockSize;
            Parallel.For(0, blockNum, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, blockId =>
            {
                int seg_begin = 0;
                int outStart = blockSize * blockId;
                int outEnd = Math.Min(outStart + blockSize, output_dimension);

                for (int sample = 0; sample < batchsize; ++sample)
                {
                    int seg_end = Smp_Index[sample];
                    for (int word_idx = seg_begin; word_idx < seg_end; ++word_idx)
                    {
                        int col_end = Seg_Index[word_idx];
                        int col_begin = 0;
                        if (word_idx > 0) col_begin = Seg_Index[word_idx - 1];

                        for (int i = col_begin; i < col_end; ++i)
                        {
                            int fea_idx = Fea_Index[i];
                            for (int idx = outStart; idx < outEnd; idx++)
                            {
                                mul_weight[fea_idx * output_dimension + idx] += Fea_Value[i] * output[sample * output_dimension + idx] * weight;
                            }
                        }
                    }
                    seg_begin = seg_end;
                }
            });
        }

        public static void SEQ_Sparse_Matrix_Multiply_INT(int[] Smp_Index, int batchsize, int[] Seg_Index,
    int seg_size, int[] Fea_Index, float[] Fea_Value, int elementsize,
    float[] mul_weight, float[] output, int Feature_dimension, int output_dimension)
        {
            Parallel.For(0, batchsize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, batchId =>
              {
                  int seg_end = Smp_Index[batchId];
                  int seg_begin = 0;
                  if (batchId > 0)
                  {
                      seg_begin = Smp_Index[batchId - 1];
                  }

                  int batchOutput = batchId * output_dimension;
                  for (int idx = 0; idx < output_dimension; idx++)
                  {
                      output[batchOutput + idx] = 0;
                  }

                  for (int word_idx = seg_begin; word_idx < seg_end; ++word_idx)
                  {
                      int col_end = Seg_Index[word_idx];
                      int col_begin = 0;
                      if (word_idx > 0) col_begin = Seg_Index[word_idx - 1];

                      for (int i = col_begin; i < col_end; ++i)
                      {
                          int fea_idx = Fea_Index[i];
                          int fOffset = fea_idx * output_dimension;
                          if (Fea_Value[i] == 1)
                          {
                              for (int idx = 0; idx < output_dimension; idx++)
                              {
                                  output[batchOutput + idx] += mul_weight[fOffset + idx];
                              }
                          }
                          else
                          {
                              for (int idx = 0; idx < output_dimension; idx++)
                              {
                                  output[batchOutput + idx] += Fea_Value[i] * mul_weight[fOffset + idx];
                              }
                          }
                      }
                  }
              });
        }
        #endregion

        #region GRU
        public static void GRUForwardPropagateBatch(int[] InstanceIndex, int batchSize, int Dim,  //->x;
            float[] H, //->hidden,
            float[] GateR, //->reset gate,
            float[] GateZ, //->update gate,
            float[] HHat, //->chat
            float[] RestH,
            float[] Ur,
            float[] Uz,
            float[] Uh)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqEnd = InstanceIndex[idx];
                fixed (float* pH = H)
                fixed (float* pGateR = GateR)
                fixed (float* pGateZ = GateZ)
                fixed (float* pHHat = HHat)
                fixed (float* pRestH = RestH)
                fixed (float* pUr = Ur)
                fixed (float* pUz = Uz)
                fixed (float* pUh = Uh)
                {
                    ///time step
                    for (int t = seqBegin; t < seqEnd; t++)
                    {
                        if (t > seqBegin)
                        {
                            //GateZ += Uz * h(t-1)
                            SseUtils.ApplyMatMul(pH + (t - 1) * Dim, pUz, pGateZ + t * Dim, 1, Dim, Dim, 1, 1);
                            //GateR += Ur * h(t-1)
                            SseUtils.ApplyMatMul(pH + (t - 1) * Dim, pUr, pGateR + t * Dim, 1, Dim, Dim, 1, 1);
                        }

                        //Sigmoid(GateR)
                        SseUtils.ApplySigmoid(pGateR + t * Dim, pGateR + t * Dim, Dim);

                        //Sigmoid(GateZ)
                        SseUtils.ApplySigmoid(pGateZ + t * Dim, pGateZ + t * Dim, Dim);

                        //compute HHat
                        if (t > seqBegin)
                        {
                            //RestH = GateR @ H(t-1)
                            SseUtils.ApplyMultiply(pH + (t - 1) * Dim, pGateR + t * Dim, pRestH + t * Dim, Dim);
                            //HHat += Uh * RestH
                            SseUtils.ApplyMatMul(pRestH + t * Dim, pUh, pHHat + t * Dim, 1, Dim, Dim, 1, 1);
                        }

                        //Tanh(HHat)
                        SseUtils.ApplyTanh(pHHat + t * Dim, pHHat + t * Dim, Dim);

                        //compute H
                        //H(t) = (1 - GateZ(t)) @ HHat(t)
                        SseUtils.ApplyScalarAddVectorMulVector(1, pGateZ + t * Dim, -1, pHHat + t * Dim, pH + t * Dim, Dim);

                        if (t > seqBegin)
                        {
                            //H(t) += H(t-1) @ GateZ(t)
                            SseUtils.ApplyMultiplyAdd(pGateZ + t * Dim, pH + (t - 1) * Dim, pH + t * Dim, Dim, 1);
                        }
                    }
                }
            });
        }

        public static void GRUBackwardPropagateBatch(int[] InstanceIndex, int batchSize, int Dim,//->x
            float[] H, //->hidden
            float[] GateR, //->reset gate,
            float[] GateZ, //->update gate,
            float[] HHat, //->hat
            float[] RestH, //H @ R
            float[] DerivH,
            float[] DerivGateR,
            float[] DerivGateZ,
            float[] DerivHHat,
            float[] DerivRestH,
            float[] Ur,
            float[] Uz,
            float[] Uh)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqEnd = InstanceIndex[idx];
                fixed (float* pH = H)
                fixed (float* pGateR = GateR)
                fixed (float* pGateZ = GateZ)
                fixed (float* pHHat = HHat)
                fixed (float* pRestH = RestH)
                fixed (float* pUr = Ur)
                fixed (float* pUz = Uz)
                fixed (float* pUh = Uh)
                fixed (float* pDerivH = DerivH)
                fixed (float* pDerivGateR = DerivGateR)
                fixed (float* pDerivGateZ = DerivGateZ)
                fixed (float* pDerivHHat = DerivHHat)
                fixed (float* pDerivRestH = DerivRestH)
                {
                    ///time step
                    for (int t = seqEnd - 1; t > seqBegin; t--)
                    {
                        //DerivHHat = (1 - GateZ)@DerivH
                        SseUtils.ApplyScalarAddVectorMulVector(1, pGateZ + t * Dim, -1, pDerivH + t * Dim, pDerivHHat + t * Dim, Dim);

                        //DerivHHat = DerivHHat * ( 1- HHat) * HHat 
                        SseUtils.ApplyTanhDerivative(pHHat + t * Dim, pDerivHHat + t * Dim, Dim);

                        //DerivGateZ = -HHat @ DerivH
                        SseUtils.ApplyScalarAddVectorMulVector(0, pHHat + t * Dim, -1, pDerivH + t * Dim, pDerivGateZ + t * Dim, Dim);

                        if (t > seqBegin)
                        {
                            //DerivGateZ += DerivH @ H(t-1)
                            SseUtils.ApplyMultiplyAdd(pH + (t - 1) * Dim, pDerivH + t * Dim, pDerivGateZ + t * Dim, Dim, 1);
                        }

                        //DerivGateZ = DerivGateZ * (1 + GateZ) * (1 - GateZ)
                        SseUtils.ApplySigmoidDerivative(pGateZ + t * Dim, pDerivGateZ + t * Dim, Dim);

                        //DerivRestH = Uh * DerivHHat
                        SseUtils.ApplyMatMulTR(pDerivHHat + t * Dim, pUh, pDerivRestH + Dim * t, 1, Dim, Dim, 0, 1);

                        //DerivGateR = DerivRestH @ H(t -1)
                        if (t > seqBegin)
                        {
                            SseUtils.ApplyMultiply(pH + (t - 1) * Dim, pDerivRestH + t * Dim, pDerivGateR + t * Dim, Dim);
                        }

                        //DerivGateR = DerivGateR * GateR * (1 - GateR)
                        SseUtils.ApplySigmoidDerivative(pGateR + t * Dim, pDerivGateR + t * Dim, Dim);

                        //compute DerivH(t-1)
                        if (t > seqBegin)
                        {
                            //DerivH(t-1) += GateZ @ DerivH
                            SseUtils.ApplyMultiplyAdd(pGateZ + t * Dim, pDerivH + t * Dim, pDerivH + (t - 1) * Dim, Dim, 1);

                            //DerivH(t-1) += DerivRestH @ GateR
                            SseUtils.ApplyMultiplyAdd(pGateR + t * Dim, pDerivRestH + t * Dim, pDerivH + (t - 1) * Dim, Dim, 1);

                            //DerivH(t-1) += Uz * DerivGateZ
                            SseUtils.ApplyMatMulTR(pDerivGateZ + t * Dim, pUz, pDerivH + (t - 1) * Dim, 1, Dim, Dim, 1, 1);

                            //DerivH(t-1) += Ur * DerivGateR
                            SseUtils.ApplyMatMulTR(pDerivGateR + t * Dim, pUr, pDerivH + (t - 1) * Dim, 1, Dim, Dim, 1, 1);
                        }
                    }
                }
            });
        }

        #endregion
    }
}
