﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;

namespace TestSSE
{
    public class TestApplyTanh
    {
        public unsafe static void Run()
        {
            int size = 1024 * 1024 * 100;
            float[] a = new float[size];
            float[] b = new float[size];
            Random rnd = new Random(0);
            for (int i = 0; i < size; i++)
            {
                a[i] = (0.5f - (float)rnd.NextDouble()) * 2;
            }
            a[0] = -1f;
            AlignedArray aa = new AlignedArray(a);
            AlignedArray ab = new AlignedArray(b);

            double d = 0;
            Stopwatch sw = new Stopwatch();

            int loop = 5;
            do
            {
                sw.Restart();
                AvxUtils.ApplyTanh(aa, ab, size);
                sw.Stop();
                d = 0;
                for (int i = 0; i < size; i++)
                {
                    d += ab[i];
                }
                Console.WriteLine("AvxApplyTanhA:{0}, Cost:{1}ms", d, sw.ElapsedMilliseconds);

                sw.Restart();
                fixed (float* pa = &a[0])
                {
                    fixed (float* pb = &b[0])
                    {
                        SseUtils.ApplyTanh(pa, pb, size);
                    }
                }
                sw.Stop();

                d = 0;
                for (int i = 0; i < size; i++)
                {
                    d += b[i];
                }
                Console.WriteLine("SseApplyTanhU:{0}, Cost:{1}ms", d, sw.ElapsedMilliseconds);

                sw.Restart();
                fixed (float* pa = &a[0])
                {
                    fixed (float* pb = &b[0])
                    {
                        AvxUtils.ApplyTanh(pa, pb, size);
                    }
                }
                sw.Stop();

                d = 0;
                for (int i = 0; i < size; i++)
                {
                    d += b[i];
                }
                Console.WriteLine("AvxApplyTanhU:{0}, Cost:{1}ms", d, sw.ElapsedMilliseconds);

                sw.Restart();
                AvxUtils.ApplyTanh((float*)aa.PinnedBasePtr.ToPointer(), (float*)ab.PinnedBasePtr.ToPointer(), size);
                sw.Stop();
                d = 0;
                for (int i = 0; i < size; i++)
                {
                    d += ab[i];
                }
                Console.WriteLine("AvxApplyTanhU-[A]:{0}, Cost:{1}ms", d, sw.ElapsedMilliseconds);

                sw.Restart();
                SseUtils.ApplyTanh((float*)aa.PinnedBasePtr.ToPointer(), (float*)ab.PinnedBasePtr.ToPointer(), size);
                sw.Stop();
                d = 0;
                for (int i = 0; i < size; i++)
                {
                    d += ab[i];
                }
                Console.WriteLine("SseApplyTanhU-[A]:{0}, Cost:{1}ms", d, sw.ElapsedMilliseconds);

                sw.Restart();
                TanhNative(a, b);
                sw.Stop();
                d = 0;
                for (int i = 0; i < size; i++)
                {
                    d += b[i];
                }
                Console.WriteLine("TanhNative-[A]:{0}, Cost:{1}ms", d, sw.ElapsedMilliseconds);
                Console.WriteLine();

            } while (loop-- > 0);
        }

        static public void TanhNative(float[] a, float[] b)
        {
            for (int i = 0; i < a.Length; i++)
            {
                double e = Math.Exp(-2*a[i]);
                b[i] = (float)((1-e) / (1 + e));
            }
        }

    }
}
