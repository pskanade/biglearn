﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;
namespace TestSSE
{
    unsafe class TestMatMul
    {
        public static void MatMulNative(float[] left, float[] right, float[] dst, int dstRowCnt, int interColCnt, int dstColCnt, float w1, float w2)
        {
            float[] dstSrc = new float[dst.Length];
            for (int i = 0; i < dstRowCnt; i++)
            {
                for (int j = 0; j < dstColCnt; j++)
                {
                    for (int k = 0; k < interColCnt; k++)
                    {
                        float tmp = (left[i * interColCnt + k] * right[k * dstColCnt + j]);
                        dstSrc[i * dstColCnt + j] += tmp;
                    }

                    dst[i * dstColCnt + j] = w2 * dstSrc[i * dstColCnt + j] + w1 * dst[i * dstColCnt + j];
                }
            }
        }

        static void MatMulNative(float* pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int leftStride, int rightStride, int dstStride)
        {
            float* pRowL = pLeft;
            float* pRowD = pDst;
            float* pRowR = pRight;
            float* pDstEnd = pDst + dstRowCnt * dstStride;
            for (; pRowD < pDstEnd; pRowL += leftStride, pRowD += dstStride)
            {
                pRowR = pRight;
                float* pLEnd = pRowL + intermediateColumnCnt;
                float* pDEnd = pRowD + dstColumnCnt;
                for (float* pTmp = pRowL; pTmp < pLEnd; pTmp++)
                {
                    for (float* pTmpDst = pRowD, pTmpRight = pRowR; pTmpDst < pDEnd; pTmpRight++, pTmpDst++)
                    {
                        *pTmpDst += *pTmp * (*pTmpRight);
                    }

                    pRowR += rightStride;
                }
            }
        }

        public static unsafe void MatMulNativeTR(float[] left, float[] right, float[] dst, int leftRowCnt, int leftColunmCnt, int rightRowCnt, float w1, float w2)
        {
            float[] dstSrc = new float[dst.Length];
            for (int i = 0; i < leftRowCnt; i++)
            {
                fixed (float* pDst = &dst[i * rightRowCnt])
                {
                    for (int j = 0; j < rightRowCnt; j++)
                    {
                        float tmp = 0;
                        fixed (float* pSrc = &left[i * leftColunmCnt])
                        {
                            fixed (float* pRight = &right[j * leftColunmCnt])
                            {
                                for (int k = 0; k < leftColunmCnt; k++)
                                {
                                    tmp += pSrc[k] * pRight[k];
                                }
                            }
                        }

                        pDst[j] = w1 * pDst[j] + w2 * tmp;
                    }
                }
            }
        }

        static void MatMulAddNativeTL(float* pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int leftStride, int rightStride, int dstStride)
        {
            float* pRowL = pLeft;
            float* pRowD = pDst;
            float* pRowR = pRight;
            float* pDstEnd = pDst + dstRowCnt * dstStride;
            float* pMatLEnd = pLeft + intermediateColumnCnt * leftStride;
            for (; pRowL < pMatLEnd; pRowL += leftStride, pRowR += rightStride)
            {
                float* pLEnd = pRowL + dstRowCnt;
                pRowD = pDst;
                for (float* pTmp = pRowL; pTmp < pLEnd; pTmp++)
                {
                    float* pTmpDst = pRowD;
                    float* pTmpRight = pRowR;
                    float* pDEnd = pRowD + dstColumnCnt;
                    for (; pTmpDst < pDEnd; pTmpRight++, pTmpDst++)
                    {
                        *pTmpDst += *pTmp * (*pTmpRight);
                    }

                    pRowD += dstStride;
                }
            }
        }

        static void MatMulAddNativeTLTR(float* pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int leftStride, int rightStride, int dstStride)
        {
            float* pRowL = pLeft;
            float* pRowD = pDst;
            float* pRowR = pRight;
            float* pREnd = pRight + dstColumnCnt * rightStride;
            for (; pRowR < pREnd; pRowR += rightStride, pRowD++)
            {                
                float* pRowREnd = pRowR + intermediateColumnCnt;
                pRowL = pLeft;
                for (float* pTmpRight = pRowR; pTmpRight < pRowREnd; pTmpRight++, pRowL += leftStride)
                {                    
                    float* pRowLEnd = pRowL + dstRowCnt;
                    float v = *pTmpRight;
                    float* pTmpDst = pRowD;
                 
                    for (float* pTmp = pRowL; pTmp < pRowLEnd; pTmp++, pTmpDst += dstStride)
                    {
                        *pTmpDst += *pTmp * v;
                    }
                }
            }
        }

        public static void MatTrans(float[] src, float[] dst, int rowCnt, int colCnt)
        {
            for (int i = 0; i < rowCnt; i++)
            {
                for (int j = 0; j < colCnt; j++)
                {
                    dst[j * rowCnt + i] = src[i * colCnt + j];
                }
            }
        }

        public unsafe static void Run()
        {
            int dstRowCnt = 127;
            int intermediateColCnt = 511;
            int dstColumnCnt = 1023;
            float[] left = new float[dstRowCnt * intermediateColCnt];
            float[] right = new float[intermediateColCnt * dstColumnCnt];
            float[] dst = new float[dstRowCnt * dstColumnCnt];
            float[] dst2 = new float[dstRowCnt * dstColumnCnt];
            Random rnd = new Random(0);
            for (int i = 0; i < left.Length; i++)
            {
                left[i] = (float)(10 * (rnd.NextDouble() - 0.5f));
            }

            for (int i = 0; i < right.Length; i++)
            {
                right[i] = (float)(10 * (rnd.NextDouble() - 0.5f));
            }

            for (int i = 0; i < dst.Length; i++)
            {
                dst2[i] = dst[i] = 0;// (float)(10 * (rnd.NextDouble() - 0.5f));
            }

            float[] rightTrans = new float[intermediateColCnt * dstColumnCnt];
            float[] leftTrans = new float[intermediateColCnt * dstRowCnt];

            MatTrans(left, leftTrans, intermediateColCnt, dstRowCnt);
            MatTrans(right, rightTrans, dstColumnCnt, intermediateColCnt);
            float w1 = 0;
            float w2 = 1;
            Stopwatch tN = new Stopwatch();
            Stopwatch tSse = new Stopwatch();
            tN.Start();
            MatMulNative(leftTrans, right, dst, dstRowCnt, intermediateColCnt, dstColumnCnt, w1, w2);
            //fixed (float* pLeft = &left[0])
            //{
            //    fixed (float* pRight = &rightTrans[0])
            //    {
            //        fixed (float* pDst = &dst[0])
            //        {
            //            SseUtils.ApplyMatMul(pLeft, pRight, pDst, leftRowCnt, leftColCnt, rightColCnt, w1, w2);
            //        }
            //    }
            //}
            float l1_1 = SseUtils.SumAbs(dst);
            tN.Stop();
            Console.WriteLine(".");
            tSse.Start();
            fixed (float* pLeft = &left[0])
            {
                fixed (float* pRight = &right[0])
                {
                    fixed (float* pDst = &dst2[0])
                    {
                        SseUtils.ApplyMatMulTL(pLeft, pRight, pDst, dstRowCnt, intermediateColCnt, dstColumnCnt, w1, w2);
                        //MatMulAddNativeTLTR(pLeft, pRight, pDst, dstRowCnt, intermediateColCnt, dstColumnCnt, dstRowCnt, intermediateColCnt, dstColumnCnt);
                    }
                }
            }
            float l1_2 = SseUtils.SumAbs(dst2);
            tSse.Stop();

            //for (int i = 0; i < dst.Length; i++)
            //{
            //    if (dst[i] != dst2[i])
            //    {
            //        Console.WriteLine("Diff[{0}]:{1}:{2}", i, dst[i], dst2[i]);
            //    }
            //}

            Console.WriteLine("Native:{0}ms, Sse:{1}ms", tN.ElapsedMilliseconds, tSse.ElapsedMilliseconds);
            Console.WriteLine("Native:{0}, Sse:{1}", l1_1, l1_2);
            //for (int i = 0; i < 100; i++)
            //{
            //    Console.WriteLine("Dst:{0} :{1}", dst[i], dst2[i]);
            //}
        }
    }
}
