﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;

namespace TestSSE
{
    public class TestApplyRecip
    {
        public unsafe static void Run()
        {
            int size = 1024 * 1024 * 10;
            float[] a = new float[size];
            float[] b = new float[size];
            float[] one = new float[size];
            Random rnd = new Random(0);
            for (int i = 0; i < size; i++)
            {
                a[i] = (0.5f - (float)rnd.NextDouble()) * 2;
                one[i] = 1.0f;
            }

            int loop = 3;
            Stopwatch sw = new Stopwatch();
            fixed (float* pa = &a[0])
            {
                fixed (float* pb = &b[0])
                {
                    while (loop-- > 0)
                    {

                        sw.Restart();
                        SseUtils.ApplyRecip(pa, pb, size);
                        sw.Stop();
                        double d = 0;
                        for (int i = 0; i < size; i++)
                        {
                            d += pb[i];
                        }
                        Console.WriteLine("SSeApplyRecip: {0}, {1}, {2}ms", d, pb[0], sw.ElapsedMilliseconds);

                        sw.Restart();
                        SseUtils.ApplyRecipNR(pa, pb, size);
                        sw.Stop();
                        d = 0;
                        for (int i = 0; i < size; i++)
                        {
                            d += pb[i];
                        }
                        Console.WriteLine("SseApplyRecipNR: {0}, {1}, {2}ms", d, pb[0], sw.ElapsedMilliseconds);

                        sw.Restart();
                        SseUtils.ApplyRecipFast(pa, pb, size);
                        sw.Stop();
                        d = 0;
                        for (int i = 0; i < size; i++)
                        {
                            d += pb[i];
                        }
                        Console.WriteLine("SSeApplyRecipFast: {0}, {1}, {2}ms", d, pb[0], sw.ElapsedMilliseconds);

                        sw.Restart();
                        RecipNative(a, b);
                        sw.Stop();
                        d = 0;
                        for (int i = 0; i < size; i++)
                        {
                            d += b[i];
                        }
                        Console.WriteLine("RecipNative: {0}, {1}, {2}ms", d, b[0], sw.ElapsedMilliseconds);

                        Console.WriteLine();
                    }
                }
            }
        }

        public static void RecipNative(float[] a, float[] b)
        {
            for (int i = 0; i < a.Length; i++)
            {
                b[i] = 1 / a[i];
            }
        }
    }
}
