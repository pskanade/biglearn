﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;
namespace TestSSE
{
    class TestAdaGrad
    {
        static MathThreadPool pool = new MathThreadPool();
        public static void Run()
        {
            try
            {
                //pool.Setup(2);
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp);
            }

            int size = 1024 * 1024 * 45;
            float[] a = new float[size];
            float[] b = new float[size];
            Random rnd = new Random(0);
            for (int i = 0; i < size; i++)
            {
                a[i] = (float)rnd.NextDouble();
                b[i] = 0.5f - (float)rnd.NextDouble();
            }


            //SseAdaGradA(a, b);

            //AvxAdaGradA(a, b);
            Console.WriteLine("Start add");
            //SseAdaGradU(a, b);
            AvxAdaGradU(a, b);

            // NativeAdaGrad(a, b);

        }

        public static void SseAdaGradA(float[] a, float[] b)
        {
            AlignedArray aa = new AlignedArray(a, 0, a.Length, AvxUtils.CbAlign);

            AlignedArray ab = new AlignedArray(b, 0, b.Length, AvxUtils.CbAlign);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 1; i++)
            {
                SseUtils.ApplyAdaGrad(aa.Items, aa.AlignedBase, ab.Items, ab.AlignedBase, a.Length);
            }
            sw.Stop();

            Console.WriteLine("SseAdaA:{0}s, |a|={1}, |b|={2}", sw.ElapsedMilliseconds / 1000.0f, SseUtils.SumSq(aa.Items), SseUtils.SumSq(ab.Items));
        }

        public static void AvxAdaGradA(float[] a, float[] b)
        {
            AlignedArray aa = new AlignedArray(a, 0, a.Length, AvxUtils.CbAlign);

            AlignedArray ab = new AlignedArray(b, 0, b.Length, AvxUtils.CbAlign);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 1; i++)
            {
                AvxUtils.ApplyAdaGrad(aa.Items, aa.AlignedBase, ab.Items, ab.AlignedBase, a.Length);
            }
            sw.Stop();

            Console.WriteLine("AvxAdaA:{0}s, |a|={1}, |b|={2}", sw.ElapsedMilliseconds / 1000.0f, SseUtils.SumSq(aa.Items), SseUtils.SumSq(ab.Items));
        }

        public static void NativeAdaGrad(float[] a, float[] b)
        {
            Stopwatch sw = new Stopwatch();

            sw.Start();
            for (int k = 0; k < 1; k++)
            {
                for (int i = 0; i < a.Length; i++)
                {
                    a[i] += b[i] * b[i];
                    b[i] = b[i] / (float)Math.Sqrt(a[i]);
                }
            }
            sw.Stop();
            Console.WriteLine("NativeAda:{0}s, |a|={1}, |b|={2}", sw.ElapsedMilliseconds / 1000.0f, AvxUtils.SumSq(a), AvxUtils.SumSq(b));
        }

        public unsafe static void AvxAdaGradU(float[] a, float[] b)
        {
            float[] aa = a;// new float[a.Length];
            float[] bb = b;// new float[b.Length];
           // Array.Copy(a, 0, aa, 0, a.Length);
           // Array.Copy(b, 0, bb, 0, a.Length);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int k = 0; k < 1; k++)
            {
                fixed (float* pA = &aa[0])
                fixed (float* pB = &bb[0])
                {
                    AvxUtils.ApplyAdaGrad(pA, pB, a.Length);
                }
            }
            sw.Stop();
            Console.WriteLine("AvxAdaGradU:{0}ms, |a|={1}, |b|={2}", ((double)sw.ElapsedTicks * 1000) / Stopwatch.Frequency, AvxUtils.SumSq(aa), AvxUtils.SumSq(bb));
        }

        public unsafe static void SseAdaGradU(float[] a, float[] b)
        {
            float[] aa = new float[a.Length];
            float[] bb = new float[b.Length];
            Array.Copy(a, 0, aa, 0, a.Length);
            Array.Copy(b, 0, bb, 0, a.Length);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int chunkNum = 2;
            int chunkSize = a.Length / chunkNum;
            Console.WriteLine("Length={0}, ChkNum={1}, ChkSize={2}", a.Length, chunkNum, chunkSize);
            MathTask[] tasks = new MathTask[chunkNum];
            for (int i = 0; i < 1; i++)
            {
                //Parallel.For(0, chunkNum, k =>
                for (int k = 0; k < chunkNum; k++)
                {
                    tasks[k] = pool.QueueWorkItem((s) =>
                    {
                        int idx = (int)s;
                        int offset = idx * chunkSize;
                        int size = chunkSize;
                        if (idx == chunkNum - 1)
                        {
                            size = a.Length - offset;
                        }

                        if (size == 0)
                        {
                            return;
                        }

                        fixed (float* pA = &aa[offset])
                        fixed (float* pB = &bb[offset])
                        {
                            SseUtils.ApplyAdaGrad(pA, pB, size);
                        }
                    }, k);
                }//);

                MathTask.WaitAll(tasks);
                //);
            }
            sw.Stop();
            Console.WriteLine("SseAdaGradU:{0}ms, |a|={1}, |b|={2}", ((double)sw.ElapsedTicks * 1000) / Stopwatch.Frequency, AvxUtils.SumSq(aa), AvxUtils.SumSq(bb));
        }
    }
}
