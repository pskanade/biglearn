using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
	// public class AddRunner<T> : StructRunner<Structure, T> where T : VectorData
 //    {
 //        float Scale { get; set; }

 //        public T InputA { get ; set; }
 //        public T InputB { get; set; }
 //        float AWei { get; set; }
 //        float BWei { get; set; }
 //        public new T Output { get { return (T)base.Output; } set { base.Output = value; } }

 //        // c = a * awei + b * bwei;
 //        public AddRunner(VectorData a, VectorData b, float awei, float bwei, RunnerBehavior behavior) : base(Structure.Empty, behavior)
 //        {
 //        	AWei = awei;
 //        	BWei = bwei;
        	
 //        	InputA = (T) a;
 //        	InputB = (T) b;

 //            Output = (T)(new VectorData(Input.MaxLength, behavior.Device));
 //        }

 //        public AddRunner(MatrixData a, MatrixData b, float awei, float bwei, RunnerBehavior behavior) : base(Structure.Empty, behavior)
 //        {
 //        	AWei = awei;
 //        	BWei = bwei;
        	
 //            InputA = (T)(VectorData)a;
 //            InputB = (T)(VectorData)b;
            
 //            if(a.Column != b.Column)
 //            {
 //            	throw new Exception(string.Format("AddRunner : Matrix A and B column doesn't match {0}, {1}", a.Column, b.Column));
 //            }
 //            Output = (T)(VectorData)(new MatrixData(a.Column, a.MaxRow, behavior.Device));
 //        }
        
 //        public override void Forward()
 //        {
 //            Output.Length = InputA.Length;
 //            ComputeLib.Add_Vector(a.Output, 0, b.Output, 0, InputA.Length, float awei, float bwei); Scale_Vector(Input.Output, 0, Output.Output, 0, Input.Length, Scale, 0);
 //        }

 //        public override void CleanDeriv()
 //        {
 //            ComputeLib.Zero(Output.Output, Output.Length);
 //        }

 //        public override void Backward(bool cleanDeriv)
 //        {
 //            ComputeLib.Scale_Vector(Output.Deriv, 0, Input.Deriv, 0, Input.Length, Scale, 1);
 //        }
 //    }
}