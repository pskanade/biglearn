﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{

    public class SeqDenseConvRunner<IN> : StructRunner<LayerStructure, IN> where IN : SeqDenseBatchData
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
        DropOutProcessor<HiddenBatchData> DropOutRegularized = null;

        HiddenBatchData ConvInput = null;

        public SeqDenseConvRunner(LayerStructure model, SeqDenseBatchData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            Output = new SeqDenseBatchData(Input.SampleIdx, Input.SentMargin, Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.Neural_Out, Behavior.Device);

            ConvInput = new HiddenBatchData(Input.MAX_SENTSIZE, Input.Dim * Model.N_Winsize, Behavior.RunMode, Behavior.Device);

            if (Model.DropOut > 0) DropOutRegularized = new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(Output.MAX_SENTSIZE, Output.Dim, Output.SentOutput, Output.SentDeriv, Behavior.Device), Model.DropOut, Behavior);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;
            //Output.SampleIdx = Input.SampleIdx;
            //Output.SentMargin = Input.SentMargin;

            ComputeLib.GetWindowMatrixs(Input.SentMargin, Input.SentSize, Input.SentOutput, CudaPieceInt.Empty, Input.Dim, Model.N_Winsize, ConvInput.Output.Data, 0, 1);
            ComputeLib.Sgemm(ConvInput.Output.Data, 0, Model.weight, 0, Output.SentOutput, 0, Input.SentSize, Input.Dim * Model.N_Winsize, Output.Dim, 0, 1, false, false);
            if (Behavior.RunMode == DNNRunMode.Train && Model.DropOut > 0) DropOutRegularized.Forward();

            ActivationFuncLib.ActivationFunc(ComputeLib, Output.SentOutput, Output.SentSize, Output.Dim, Model.Af, Model.bias);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            // Backpropagate Deriv.
            ActivationFuncLib.DerivActivationFunc(ComputeLib, Output.SentDeriv, Output.SentOutput, Output.SentSize, Output.Dim, Model.Af);

            if (Model.DropOut > 0) DropOutRegularized.Backward(false);

            // Conv Matrix Multipy Weight.
            ComputeLib.Sgemm(Output.SentDeriv, 0, Model.weight, 0, ConvInput.Deriv.Data, 0, Output.SentSize, Output.Dim, Input.Dim * Model.N_Winsize, 0, 1, false, true);

            //ConvInput.Output.Data, 0, Model.Weight, 0, Output.SentOutput, 0, Input.SentSize, Input.Dim * Model.N_Winsize, Output.Dim, 0, 1, false, false);
            ComputeLib.SetWindowMatrixs(Input.SentMargin, Input.SentSize, Input.SentDeriv, CudaPieceInt.Empty, Input.Dim, Model.N_Winsize, ConvInput.Deriv.Data, 1, 1);

            //Cudalib.Convolution_Matrix_Transpose_Multiply(Output.SentDeriv.CudaPtr, Model.Weight.CudaPtr, Input.SentDeriv.CudaPtr,
            //    Input.BatchSize, Input.SentSize, Input.SentMargin.CudaPtr, Model.Neural_Out, Model.Neural_In, Model.N_Winsize);
        }

        public override void Update()
        {
            if (!IsDelegateOptimizer) { Model.WeightOptimizer.BeforeGradient(); }
            ComputeLib.Sgemm(ConvInput.Output.Data, 0, Output.SentDeriv, 0, Model.WeightGrad, 0, Input.SentSize, ConvInput.Dim, Output.Dim, 1, Model.WeightOptimizer.GradientStep, true, false);
            //Cudalib.convolution_dense_matrix_gradient(Output.SentDeriv.CudaPtr, Input.SentMargin.CudaPtr, Input.SentSize,
            //            Model.N_Winsize, Input.BatchSize, Model.Neural_Out, Input.SentOutput.CudaPtr, Model.WeightOptimizer.Gradient.CudaPtr, Model.Neural_In, 
            //            Model.WeightOptimizer.GradientStep);
            if (!IsDelegateOptimizer) { Model.WeightOptimizer.AfterGradient(); }

            if (Model.IsBias)
            {
                if (!IsDelegateOptimizer) { Model.BiasOptimizer.BeforeGradient(); }
                ComputeLib.ColumnWiseSum(Output.SentDeriv, Model.BiasGrad, Input.SentSize, Model.Neural_Out, Model.BiasOptimizer.GradientStep);
                if (!IsDelegateOptimizer) { Model.BiasOptimizer.AfterGradient(); }
            }
        }

        //private bool disposed = false;
        //~SeqDenseConvRunner()
        //{
        //    this.Dispose(false);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (this.disposed) { return; }
        //    this.disposed = true;

        //    Output.Dispose();
        //    ConvInput.Dispose();
        //    base.Dispose(disposing);
        //}
    }
}
