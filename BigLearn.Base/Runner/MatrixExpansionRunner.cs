﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MatrixExpansionRunner : StructRunner
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }

        BiMatchBatchData MatchData;

        int Side { get; set; }
        /// <summary>
        /// Select Matrix Given BiMatchData.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="matchData"></param>
        /// <param name="side">0 : map source to target; 1 : source side; 2 : target side; </param>
        /// <param name="behavior"></param>
        public MatrixExpansionRunner(HiddenBatchData data, BiMatchBatchData matchData, int side, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = data;
            MatchData = matchData;
            Side = side;
            Output = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, Input.Dim, Behavior.RunMode, Behavior.Device);
        }

        public override void Forward()
        {
            Output.BatchSize = MatchData.BatchSize;

            if(Output.BatchSize == 0) { return; }

            if (Side == 0)
            {
                ComputeLib.Matrix_AdditionMask(Input.Output.Data, 0, MatchData.SrcIdx, 0,
                                               Output.Output.Data, 0, MatchData.TgtIdx, 0,
                                               Output.Output.Data, 0, MatchData.TgtIdx, 0,
                                               Input.Dim, MatchData.BatchSize, 1, 0, 0);
            }
            else if(Side == 1)
            {
                ComputeLib.Matrix_AdditionMask(Input.Output.Data, 0, MatchData.SrcIdx, 0,
                                               Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Input.Dim, MatchData.BatchSize, 1, 0, 0);
            }
            else if(Side == 2)
            {
                ComputeLib.Matrix_AdditionMask(Input.Output.Data, 0, MatchData.TgtIdx, 0,
                                               Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Input.Dim, MatchData.BatchSize, 1, 0, 0);
            }
        }

        public override void Backward(bool cleanDeriv)
        {
            if (Output.BatchSize == 0) { return; }
            if (Side == 0)
            {
                ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, MatchData.TgtIdx, 0,
                                             CudaPieceFloat.Empty, MatchData.Src2MatchIdx, MatchData.SrcSize,
                                             Input.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                             Output.BatchSize, Output.Dim, 1, 1);
            }
            else if(Side == 1)
            {
                ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, MatchData.Src2MatchElement, 0,
                                             CudaPieceFloat.Empty, MatchData.Src2MatchIdx, MatchData.SrcSize,
                                             Input.Deriv.Data, 0, MatchData.Src2Idx, 0,
                                             Input.BatchSize, Input.Dim, 1, 1);
            }
            else if(Side == 2)
            {
                ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, MatchData.Tgt2MatchElement, 0,
                                             CudaPieceFloat.Empty, MatchData.Tgt2MatchIdx, MatchData.TgtSize,
                                             Input.Deriv.Data, 0, MatchData.Tgt2Idx, 0,
                                             Input.BatchSize, Input.Dim, 1, 1);
            }
        }

        public override void CleanDeriv()
        {
            if (Output.BatchSize == 0) { return; }
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }
    }
}
