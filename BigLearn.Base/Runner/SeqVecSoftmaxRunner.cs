﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SeqVecSoftmaxRunner : StructRunner<Structure, BatchData>
    {
        public new SeqVectorData Output { get { return (SeqVectorData)base.Output; } set { base.Output = value; } }

        public new SeqVectorData Input { get { return (SeqVectorData)base.Input; } set { base.Input = value; } }

        float Gamma;
        bool LogDeriv = false;

        /// <summary>
        /// output = softmax(intput);
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public SeqVecSoftmaxRunner(SeqVectorData input, float gamma, RunnerBehavior behavior, bool logDeriv = false) : base(Structure.Empty, behavior)
        {
            Input = input;
            Gamma = gamma;
            LogDeriv = logDeriv;
            Output = new SeqVectorData(Input.MaxLength, Input.MaxSegment, Input.SegmentIdx, Input.SegmentMargin, Behavior.Device);
        }

        public override void Forward()
        {
            Output.Length = Input.Length;
            Output.Segment = Input.Segment;
            if(Output.Length == 0) { return; }
            ComputeLib.SparseSoftmax(Input.SegmentIdx, Input.Output, Output.Output, Gamma, Output.Segment);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if(Output.Length == 0) { return; }
            if (LogDeriv)
            {
                ComputeLib.DerivLogSparseSoftmax(Output.SegmentIdx, Output.Output, Output.Deriv, Input.Deriv, Gamma, 1, 1, Output.Segment);
            }
            else
            {
                ComputeLib.DerivSparseMultiClassSoftmax(Output.SegmentIdx, Output.Output, Output.Deriv, Input.Deriv, Gamma, Output.Segment, 1, 1);
            }
        }

        public override void Update() { }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }

}
