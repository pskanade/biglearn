﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    //public class SeqEmbedRunner : StructRunner<EmbedStructure, SeqSparseBatchData>
    //{
    //    public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

    //    SeqHelpData HelpInput { get; set; }

    //    public SeqEmbedRunner(EmbedStructure model, SeqSparseBatchData data, SeqHelpData helpInput, RunnerBehavior behavior)
    //        : base(model, data, behavior)
    //    {
    //        HelpInput = helpInput;
    //        Output = new SeqDenseBatchData(Input.SampleIdx, Input.SentMargin, Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.Dim, Behavior.Device);
    //    }

    //    public override void CleanDeriv()
    //    {
    //        ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
    //    }

    //    public override void Forward()
    //    {
    //        Output.BatchSize = Input.BatchSize;
    //        Output.SentSize = Input.SentSize;

    //        /*Embeding Input -> Output*/
    //        ComputeLib.SparseSgemmMask(Input.SequenceIdx, Input.FeaIdx, Input.FeaValue, Model.Embedding, 0, Output.SentOutput, 0, Input.SentSize,
    //                Input.Stat.FEATURE_DIM, Model.Dim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.TransSeqIndex, 0, 0, 1, false, false);
    //    }

    //    public override void Update()
    //    {
    //        Model.EmbeddingOptimizer.BeforeGradient();
    //        ComputeLib.SparseSgemmMask(Input.SequenceIdx, Input.FeaIdx, Input.FeaValue,
    //                                            Output.SentDeriv, 0,
    //                                            Model.EmbeddingOptimizer.Gradient, 0,
    //                                            Input.SentSize, Input.Stat.FEATURE_DIM, Model.Dim,
    //                                            CudaPieceInt.Empty, 0, HelpInput.TransSeqIndex, 0, CudaPieceInt.Empty, 0,
    //                                            1, Model.EmbeddingOptimizer.GradientStep, true, false);
    //        Model.EmbeddingOptimizer.AfterGradient();
    //    }
    //}


    public class AdvancedSeqEmbedRunner : StructRunner<EmbedStructure, SeqSparseBatchData>
    {
        public new SeqDenseRecursiveData Output { get { return (SeqDenseRecursiveData)base.Output; } set { base.Output = value; } }

        bool IsReverseOrder { get; set; }
        public AdvancedSeqEmbedRunner(EmbedStructure model, SeqSparseBatchData data, bool isReverseOrder, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            IsReverseOrder = isReverseOrder;
            Output = new SeqDenseRecursiveData(new SequenceDataStat(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.Dim),
                    Input.SampleIdx, Input.SentMargin, behavior.Device);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Stat.FEATURE_DIM);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            if (IsReverseOrder) { Output.RecurrentInfo.InitReverseAndTransposeInfo(Input.SampleIdx, Input.BatchSize, Input.SentSize); }
            else { Output.RecurrentInfo.InitTransposeInfo(Input.SampleIdx, Input.BatchSize, Input.SentSize); }

            /*Embeding Input -> Output*/
            ComputeLib.SparseSgemmMask(Input.SequenceIdx, Input.FeaIdx, Input.FeaValue, Model.Embedding, 0, Output.SentOutput, 0, Input.SentSize,
                        Input.Stat.FEATURE_DIM, Model.Dim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, Output.MapForward, 0, 0, 1, false, false);

            //Output.SentOutput.SyncToCPU();
        }

        public override void Update()
        {
            if(!IsDelegateOptimizer) Model.EmbeddingOptimizer.BeforeGradient();

            ComputeLib.SparseSgemmMask(Input.SequenceIdx, Input.FeaIdx, Input.FeaValue,
                                       Output.SentDeriv, 0,
                                       Model.EmbeddingOptimizer.Gradient, 0,
                                       Input.SentSize, Input.Stat.FEATURE_DIM, Model.Dim,
                                       CudaPieceInt.Empty, 0, Output.MapForward, 0, CudaPieceInt.Empty, 0,
                                       1, Model.EmbeddingOptimizer.GradientStep, true, false);

            if (!IsDelegateOptimizer) Model.EmbeddingOptimizer.AfterGradient();
        }
    }


    public class AdvancedEmbedRunner : StructRunner<EmbedStructure, GeneralBatchInputData>
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public AdvancedEmbedRunner(EmbedStructure model, GeneralBatchInputData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            Output = new HiddenBatchData(Input.Stat.MAX_BATCHSIZE, Model.Dim, Behavior.RunMode, Behavior.Device);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            /*Embeding Input -> Output*/
            ComputeLib.SparseSgemmMask(Input.BatchIdx, Input.FeatureIdx, Input.FeatureValue, Model.Embedding, 0, Output.Output.Data, 0, Input.BatchSize,
                        Model.VocabSize, Model.Dim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 0, 1, false, false);
        }

        public override void Update()
        {
            if (!IsDelegateOptimizer) Model.EmbeddingOptimizer.BeforeGradient();

            ComputeLib.SparseSgemmMask(Input.BatchIdx, Input.FeatureIdx, Input.FeatureValue,
                                       Output.Deriv.Data, 0,
                                       Model.EmbeddingOptimizer.Gradient, 0,
                                       Input.BatchSize, Model.VocabSize, Model.Dim,
                                       CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                       1, Model.EmbeddingOptimizer.GradientStep, true, false);

            if (!IsDelegateOptimizer) Model.EmbeddingOptimizer.AfterGradient();
        }
    }
}
