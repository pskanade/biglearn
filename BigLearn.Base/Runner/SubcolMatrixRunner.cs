﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SubcolMatrixRunner : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        public new MatrixData Input { get { return (MatrixData)base.Input; } set { base.Input = value; } }

        IntArgument FromColumn { get; set; }
        int ColumnNum { get; set; }
        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public SubcolMatrixRunner(MatrixData input, IntArgument fromColumn, int columnNum, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            FromColumn = fromColumn;
            ColumnNum = columnNum;
            Output = new MatrixData(columnNum, Input.MaxRow, Behavior.Device);
        }

        public override void Forward()
        {
            Output.Row = Input.Row;
            if(Output.Row <= 0) { throw new Exception(string.Format("Output Row shoule be larger than zero {0}", Output.Row)); }
            if (FromColumn.Value < 0) { throw new Exception(string.Format("From column index be larger than zero {0}", FromColumn.Value)); }
            if (ColumnNum  <= 0) { throw new Exception(string.Format("column number be larger than zero {0}", ColumnNum)); }
            if (Input.Column <= 0) { throw new Exception(string.Format("input column number shoule be larger than zero {0}", Input.Column)); }
            ComputeLib.Matrix_AdditionEx(Input.Output, FromColumn.Value, Input.Column, 
                                         Input.Output, FromColumn.Value, Input.Column, 
                                         Output.Output, 0, ColumnNum, 
                                         ColumnNum, Output.Row, 1, 0, 0);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Column * Output.Row);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Matrix_AdditionEx(Output.Deriv, 0, Output.Column, 
                                         Output.Deriv, 0, Output.Column, 
                                         Input.Deriv, FromColumn.Value, Input.Column, 
                                         ColumnNum, Output.Row, 1, 0, 1);
        }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }
}
