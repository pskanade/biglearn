using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class LookupEmbedRunner : StructRunner
    {
        public CudaPieceInt CudaInts { get; set; }
            
        public int MaxBatchSize = 0;

        public List<int> InputList = null;

        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        EmbedStructure Embed { get; set; }

        public LookupEmbedRunner(CudaPieceInt input, int maxBatchSize, EmbedStructure embed, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            CudaInts = input;
            Embed = embed;
            Output = new HiddenBatchData(maxBatchSize, embed.Dim, DNNRunMode.Train, behavior.Device);
            MaxBatchSize = maxBatchSize;
        }

        public LookupEmbedRunner(List<int> input, int maxBatchSize, EmbedStructure embed, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputList = input;
            CudaInts = new CudaPieceInt(maxBatchSize, behavior.Device);
            Embed = embed;
            Output = new HiddenBatchData(maxBatchSize, embed.Dim, DNNRunMode.Train, behavior.Device);
            MaxBatchSize = maxBatchSize;
        }

        public override void Forward()
        {
            if(InputList != null)
            {
                CudaInts.EffectiveSize = InputList.Count; 
                for(int i=0; i < InputList.Count; i++)
                {
                    CudaInts.MemPtr[i] = InputList[i];
                }
                CudaInts.SyncFromCPU();
            }
            Output.BatchSize = CudaInts.EffectiveSize;
            ComputeLib.LookupForward(CudaPieceInt.Empty, CudaInts, CudaInts.EffectiveSize, Embed.Embedding, Output.Output.Data, Embed.VocabSize,  Embed.Dim);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
        }

        public override void Update()
        {
            ComputeLib.LookupBackward(CudaPieceInt.Empty, CudaInts, Output.BatchSize, Output.BatchSize, 
                        Output.Deriv.Data, Embed.VocabSize, Embed.Dim, Embed.EmbeddingOptimizer.Gradient, 1);
        }
    }
}
