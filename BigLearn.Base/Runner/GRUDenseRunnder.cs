﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class FastGRUDenseRunner<IN> : StructRunner<GRUCell, IN> where IN : SeqDenseRecursiveData
    {
        public new SeqDenseRecursiveData Output { get { return (SeqDenseRecursiveData)base.Output; } set { base.Output = value; } }
        public SeqDenseRecursiveData Lag1Seq; // Lag1 Sequence Output.
        //CudaPieceFloat Lag1O;

        SeqDenseRecursiveData GateR; //reset gate
        SeqDenseRecursiveData GateZ; //update gate
        SeqDenseRecursiveData GateZ_N; //update gate
        SeqDenseRecursiveData HHat; //new memory
        SeqDenseRecursiveData RestH; //new memory

        HiddenBatchData InitO = null;
        //HiddenBatchData RemapInitO = null;

        MLPAttentionV2Runner AttentionRunner = null;

        public FastGRUDenseRunner(GRUCell model, SeqDenseRecursiveData input, RunnerBehavior behavior) : this(model, input, null, null, behavior)
        { }

        public FastGRUDenseRunner(GRUCell model, SeqDenseRecursiveData input, HiddenBatchData initO,  MLPAttentionV2Runner attentionRunner, RunnerBehavior behavior) : base(model, input, behavior)
        {
            InitO = initO;
            //if (InitO != null) RemapInitO = new HiddenBatchData(InitO.MAX_BATCHSIZE, InitO.Dim, Behavior.RunMode, Behavior.Device);

            SequenceDataStat stat = new SequenceDataStat(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim);

            GateR = new SeqDenseRecursiveData(stat, Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Behavior.Device);
            GateZ = new SeqDenseRecursiveData(stat, Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Behavior.Device);
            GateZ_N = new SeqDenseRecursiveData(stat, Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Behavior.Device);
            HHat = new SeqDenseRecursiveData(stat, Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Behavior.Device);
            RestH = new SeqDenseRecursiveData(stat, Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Behavior.Device);

            Lag1Seq = new SeqDenseRecursiveData(stat, Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Behavior.Device);
            //Lag1O = new CudaPieceFloat(Input.Stat.MAX_SEQUENCESIZE * Model.HiddenStateDim, true, Behavior.Device == DeviceType.GPU);

            Output = new SeqDenseRecursiveData(stat, Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Behavior.Device);
            AttentionRunner = attentionRunner;
        }
        


        public override void Forward()
        {
            if (InitO != null)
            {
                Lag1Seq.BatchSize = InitO.BatchSize;
                Lag1Seq.SentSize = Input.SentSize;

                ComputeLib.Matrix_AdditionMask(InitO.Output.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               Lag1Seq.SentOutput, 0, CudaPieceInt.Empty, 0,
                                               Lag1Seq.SentOutput, 0, CudaPieceInt.Empty, 0,
                                               InitO.Dim, InitO.BatchSize, 1, 0, 0);
            }
            else
            {
                ComputeLib.Zero(Lag1Seq.SentOutput, Input.RecurrentInfo.BatchSize * Model.HiddenStateDim);
            }

            if (AttentionRunner == null)
            {
                GRUComputeForwardLib.GRUForwardv2(ComputeLib, Input, Lag1Seq, InitO != null, Model, GateR, GateZ, GateZ_N, HHat, RestH, Output);
            }
            else
            {
                AttentionRunner.Forward();
                //AttentionRunner.ZSeq.BatchSize = Input.BatchSize;
                AttentionRunner.ZSeq.SentSize = Input.SentSize;
                GRUComputeForwardLib.GRUForwardAttentionv2(ComputeLib, Input, Lag1Seq, InitO != null, Model, AttentionRunner, AttentionRunner.ZSeq, GateR, GateZ, GateZ_N, HHat, RestH, Output);
            }

            //put o(t-1) -> lag1O,
            ComputeLib.Matrix_AdditionMask(Output.SentOutput, 0, Output.RecurrentInfo.RecurrentBackward, Output.RecurrentInfo.BatchSize,
                                           Lag1Seq.SentOutput, Output.BatchSize * Model.HiddenStateDim, CudaPieceInt.Empty, 0,
                                           Lag1Seq.SentOutput, Output.BatchSize * Model.HiddenStateDim, CudaPieceInt.Empty, 0,
                                           Model.HiddenStateDim, Output.SentSize - Output.BatchSize, 1, 0, 0);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            ComputeLib.Zero(Lag1Seq.SentDeriv, Output.SentSize * Output.Dim);
            if (AttentionRunner != null) { AttentionRunner.CleanDeriv(); }
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            /// apply lag1Seq to output.
            //put deriv_lag1 O -> deriv_o (i-1)
            ComputeLib.Matrix_AdditionMask(Lag1Seq.SentDeriv, Output.BatchSize * Model.HiddenStateDim, CudaPieceInt.Empty, 0,
                                           Lag1Seq.SentDeriv, Output.BatchSize * Model.HiddenStateDim, CudaPieceInt.Empty, 0,
                                           Output.SentDeriv, 0, Output.RecurrentInfo.RecurrentBackward, Output.RecurrentInfo.BatchSize,
                                           Model.HiddenStateDim, Output.SentSize - Output.BatchSize, 1, 0, 1);

            if (AttentionRunner == null)
            {
                GRUComputeBackwardDataLib.GRUBackwardDatav2(ComputeLib, Input, Lag1Seq, InitO != null , Model, GateR, GateZ, GateZ_N, HHat, RestH, Output);
            }
            else
            {
                GRUComputeBackwardDataLib.GRUAttentionBackwardDatav2(ComputeLib, Input, Lag1Seq, InitO != null, Model, AttentionRunner, AttentionRunner.ZSeq, GateR, GateZ, GateZ_N, HHat, RestH, Output);
                AttentionRunner.Backward(cleanDeriv);
            }

            if (InitO != null)
            {
                ComputeLib.Matrix_AdditionMask(Lag1Seq.SentDeriv, 0, CudaPieceInt.Empty, 0,
                                               InitO.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitO.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitO.Dim, InitO.BatchSize, 1, 1, 0);
            }
        }

        public override void Update()
        {
            if (AttentionRunner == null)
            {
                GRUComputeBackwardWeightLib.GRUBackwardWeightv2(ComputeLib, Input, Lag1Seq, InitO != null, Model, GateR, GateZ, GateZ_N, HHat, RestH, Output);
            }
            else
            {
                AttentionRunner.IsDelegateOptimizer = IsDelegateOptimizer;
                GRUComputeBackwardWeightLib.GRUAttentionBackwardWeightv2(ComputeLib, Input, Lag1Seq, InitO != null, Model, AttentionRunner, AttentionRunner.ZSeq, GateR, GateZ, GateZ_N, HHat, RestH, Output);
                AttentionRunner.Update();
            }
        }
    }

    /// <summary>
    /// will be removed.
    /// </summary>
    /// <typeparam name="IN"></typeparam>
    public class GRUDenseRunner<IN> : StructRunner<GRUCell, IN> where IN : SeqDenseBatchData
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        SeqDenseBatchData GateR; //reset gate
        SeqDenseBatchData GateZ; //update gate
        SeqDenseBatchData HHat; //new memory
        SeqDenseBatchData RestH; //new memory

        //BatchData input,  input, 
        //public GRUDenseRunner(Structure model, RunnerBehavior behavior)
        //    : base(model, behavior)
        //{
        //    Output = new SeqDenseBatchData();
        //}

        public GRUDenseRunner(Structure model, BatchData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            Output = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);

            GateR = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
            GateZ = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
            HHat = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
            RestH = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
        }

        public override void InitMemory()
        {
            if (Output.Stat == null || Output.MAX_BATCHSIZE < Input.Stat.MAX_BATCHSIZE)
            {
                //Support CPU device
                //Output.Init(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
                //GateR = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
                //GateZ = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
                //HHat = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
                //RestH = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
            }
        }

        ~GRUDenseRunner()
        {
            this.Dispose(false);
        }

        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
                if (GateR != null) GateR.Dispose(); GateR = null;
                if (GateZ != null) GateZ.Dispose(); GateZ = null;
                if (HHat != null) HHat.Dispose(); HHat = null;
                if (RestH != null) RestH.Dispose(); RestH = null;
            }

            base.Dispose(disposing);
        }

        public override void Forward()
        {
            MathOperatorManager.MathDevice = Behavior.Device;
            Output.SampleIdx = Input.SampleIdx;
            Output.SentMargin = Input.SentMargin;

            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            InitMemory();

            /*Wr X -> GateR*/
            MathOperatorManager.GlobalInstance.Matrix_Multipy(Input.SentOutput, Model.Wr, GateR.SentOutput, Input.SentSize, Model.InputFeatureDim, Model.HiddenStateDim, 0);
            MathOperatorManager.GlobalInstance.Matrix_Add_Linear(GateR.SentOutput, Model.Br, Input.SentSize, Model.HiddenStateDim);

            /*Wz X -> GateZ*/
            MathOperatorManager.GlobalInstance.Matrix_Multipy(Input.SentOutput, Model.Wz, GateZ.SentOutput, Input.SentSize, Model.InputFeatureDim, Model.HiddenStateDim, 0);
            MathOperatorManager.GlobalInstance.Matrix_Add_Linear(GateZ.SentOutput, Model.Bz, Input.SentSize, Model.HiddenStateDim);

            /*Wh X -> HHat*/
            MathOperatorManager.GlobalInstance.Matrix_Multipy(Input.SentOutput, Model.Wh, HHat.SentOutput,Input.SentSize, Model.InputFeatureDim, Model.HiddenStateDim, 0);
            MathOperatorManager.GlobalInstance.Matrix_Add_Linear(HHat.SentOutput, Model.Bh, Input.SentSize, Model.HiddenStateDim);

            MathOperatorManager.GlobalInstance.GRUForwardPropagateBatch(Input.SampleIdx, Input.BatchSize, Model.HiddenStateDim,
                                                   Output.SentOutput, GateR.SentOutput, GateZ.SentOutput, HHat.SentOutput, RestH.SentOutput,
                                                   Model.Ur, Model.Uz, Model.Uh);
        }

        public override void CleanDeriv()
        {
            Output.SentDeriv.Zero();
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {

            MathOperatorManager.GlobalInstance.GRUBackwardPropagateBatch(Input.SampleIdx, Input.BatchSize, Model.HiddenStateDim,
                Output.SentOutput, GateR.SentOutput, GateZ.SentOutput, HHat.SentOutput, RestH.SentOutput,
                Output.SentDeriv, GateR.SentDeriv, GateZ.SentDeriv, HHat.SentDeriv, RestH.SentDeriv,
                Model.Ur, Model.Uz, Model.Uh);

            //pass information
            MathOperatorManager.GlobalInstance.MatrixMultiplicationTranspose(GateR.SentDeriv, Model.Wr, Input.SentDeriv, Input.SentSize,
                    Model.HiddenStateDim, Model.InputFeatureDim, 1);

            MathOperatorManager.GlobalInstance.MatrixMultiplicationTranspose(GateZ.SentDeriv, Model.Wz, Input.SentDeriv, Input.SentSize,
                    Model.HiddenStateDim, Model.InputFeatureDim, 1);

            MathOperatorManager.GlobalInstance.MatrixMultiplicationTranspose(HHat.SentDeriv, Model.Wh, Input.SentDeriv, Input.SentSize,
                    Model.HiddenStateDim, Model.InputFeatureDim, 1);
        }

        public override void Update()
        {
            /*GateR*/
            Model.WrMatrixOptimizer.BeforeGradient();
            MathOperatorManager.GlobalInstance.MatrixMultiplicationLeftTranspose(Input.SentOutput, GateR.SentDeriv, Model.WrMatrixOptimizer.Gradient,
                                        Input.SentSize, Model.InputFeatureDim, Model.HiddenStateDim, Model.WrMatrixOptimizer.GradientStep);
            Model.WrMatrixOptimizer.AfterGradient();
            Model.BrMatrixOptimizer.BeforeGradient();
            MathOperatorManager.GlobalInstance.ColumnWiseSum(GateR.SentDeriv, Model.BrMatrixOptimizer.Gradient, Output.SentSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
            Model.BrMatrixOptimizer.AfterGradient();

            /*GateZ*/
            Model.WzMatrixOptimizer.BeforeGradient();
            MathOperatorManager.GlobalInstance.MatrixMultiplicationLeftTranspose(Input.SentOutput, GateZ.SentDeriv, Model.WzMatrixOptimizer.Gradient,
                                        Input.SentSize, Model.InputFeatureDim, Model.HiddenStateDim, Model.WzMatrixOptimizer.GradientStep);
            Model.WzMatrixOptimizer.AfterGradient();
            Model.BzMatrixOptimizer.BeforeGradient();
            MathOperatorManager.GlobalInstance.ColumnWiseSum(GateZ.SentDeriv, Model.BzMatrixOptimizer.Gradient, Output.SentSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);
            Model.BzMatrixOptimizer.AfterGradient();

            /*H*/
            Model.WhMatrixOptimizer.BeforeGradient();
            MathOperatorManager.GlobalInstance.MatrixMultiplicationLeftTranspose(Input.SentOutput, HHat.SentDeriv, Model.WhMatrixOptimizer.Gradient,
                                        Input.SentSize, Model.InputFeatureDim, Model.HiddenStateDim, Model.WhMatrixOptimizer.GradientStep);
            Model.WhMatrixOptimizer.AfterGradient();
            Model.BhMatrixOptimizer.BeforeGradient();
            MathOperatorManager.GlobalInstance.ColumnWiseSum(HHat.SentDeriv, Model.BhMatrixOptimizer.Gradient, Output.SentSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
            Model.BhMatrixOptimizer.AfterGradient();

            /*Hiddent*/
            Model.UhMatrixOptimizer.BeforeGradient();
            MathOperatorManager.GlobalInstance.MatrixMultiplicationLeftTranspose(RestH.SentOutput, HHat.SentDeriv, Model.UhMatrixOptimizer.Gradient,
                                    Input.SentSize, Model.HiddenStateDim, Model.HiddenStateDim, Model.UhMatrixOptimizer.GradientStep);
            Model.UhMatrixOptimizer.AfterGradient();

            /*Recurrent GateZ */
            Model.UzMatrixOptimizer.BeforeGradient();
            MathOperatorManager.GlobalInstance.RecursiveUpdateBatch(Input.SampleIdx, Input.BatchSize, Input.SentSize, Output.SentOutput, Model.HiddenStateDim,
                                        GateZ.SentDeriv, 1, Model.UzMatrixOptimizer.Gradient, Model.UzMatrixOptimizer.GradientStep);
            Model.UzMatrixOptimizer.AfterGradient();

            /*Recurrent GateR*/
            Model.UrMatrixOptimizer.BeforeGradient();
            MathOperatorManager.GlobalInstance.RecursiveUpdateBatch(Input.SampleIdx, Input.BatchSize, Input.SentSize, Output.SentOutput, Model.HiddenStateDim,
                                        GateR.SentDeriv, 1, Model.UrMatrixOptimizer.Gradient, Model.UrMatrixOptimizer.GradientStep);
            Model.UrMatrixOptimizer.AfterGradient();
        }
    }

    /// <summary>
    /// GRU State Runner.
    /// </summary>
    public class GRUStateRunner : StructRunner<GRUCell, HiddenBatchData>
    {
        public HiddenBatchData Query { get; set; }

        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        HiddenBatchData GateR; //reset gate
        HiddenBatchData GateZ; //update gate
        HiddenBatchData HHat; //new memory
        HiddenBatchData RestH; //new memory

        CudaPieceFloat Ones;
        CudaPieceFloat Tmp;
        public GRUStateRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
        {
            Query = query;

            Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

            GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
            GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
            HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
            RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

            Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
            Ones.Init(1);

            Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);

            if(input.Dim != model.InputFeatureDim)
            {
                throw new Exception(string.Format("GRUStateRunner InputFeatureDim doesn't match {0} , {1}", input.Dim, model.InputFeatureDim)); 
            }

            if(query.Dim != model.HiddenStateDim)
            {
                throw new Exception(string.Format("GRUStateRunner HiddenStateDim doesn't match {0} , {1}", query.Dim, model.HiddenStateDim)); 
            }

        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            if (Output.BatchSize == 0) { return; }

            

            //SubQuery.BatchSize = 1;
            /*Wr X -> GateR*/
            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
            
            /*h_t-1 -> GateR*/
            ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Input.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
            ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Input.BatchSize, Model.HiddenStateDim);

            /*Wz X -> GateZ*/
            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
            /*h_t-1 -> GateZ*/
            ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
            ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Input.BatchSize, Model.HiddenStateDim);

            /*Wh X -> HHat*/
            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
            ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Input.BatchSize, Model.HiddenStateDim, 0);
            ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
            ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Input.BatchSize, Model.HiddenStateDim);

            //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
            // tmp = (1 - GateZ(t))
            ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
                                           GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
                                           Tmp, 0, CudaPieceInt.Empty, 0,
                                           Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

            //CudaPieceFloatDebug.Print("GRU Output step 1", Output.Output.Data, 300, false);
            //Logger.WriteLog("Output Batch Size {0}", Output.BatchSize);
            //Logger.WriteLog("Output Dim {0}", Output.Dim);

            //CudaPieceFloatDebug.Print("GRU HHat Output", HHat.Output.Data, 300, false);
            //CudaPieceFloatDebug.Print("GRU Gate Z Output", GateZ.Output.Data, 300, false);

            // NewQuery = GateZ(t) * HHat
            ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);


            //CudaPieceFloatDebug.Print("GRU Output step 2", Output.Output.Data, 300, false);
            //CudaPieceFloatDebug.Print("GRU GateR Output", GateR.Output.Data, 300, true);
            
            // NewQuery += tmp * h_t-1
            ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
        
            //CudaPieceFloatDebug.Print("GRU Input", Input.Output.Data, 300, false);
            //CudaPieceFloatDebug.Print("GRU Output Final", Output.Output.Data, 300, true);
        }

        public override void Backward(bool cleanDeriv)
        {
            if (Output.BatchSize == 0) { return; }

            if (Query.Deriv != null)
            {
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Input.BatchSize, Model.HiddenStateDim, 1);
            }

            ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
            // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
            ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
                                           Query.Output.Data, 0, CudaPieceInt.Empty, 0,
                                           Tmp, 0, CudaPieceInt.Empty, 0,
                                           Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
            ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

            ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
            ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

            ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
            if (Query.Deriv != null)
            {
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);
            }

            ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
            ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

            if (Query.Deriv != null)
            {
                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
            }
            ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
        }

        public override void CleanDeriv()
        {
            if (Output.BatchSize == 0) { return; }
            ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
        }

        public override void Update()
        {
            if (Output.BatchSize == 0) { return; }
            
            ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WhMatrixOptimizer.GradientStep, true, false);
            ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WrMatrixOptimizer.GradientStep, true, false);
            ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WzMatrixOptimizer.GradientStep, true, false);

            ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UhMatrixOptimizer.GradientStep, true, false);
            ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UrMatrixOptimizer.GradientStep, true, false);
            ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UzMatrixOptimizer.GradientStep, true, false);

            ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
            ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
            ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);
        }
    }


    

}
