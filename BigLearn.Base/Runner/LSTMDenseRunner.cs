﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class FastLSTMDenseRunner<IN> : StructRunner<LSTMCell, IN> where IN : SeqDenseRecursiveData
    {
        public new SeqDenseRecursiveData Output { get { return (SeqDenseRecursiveData)base.Output; } set { base.Output = value; } }
        public SeqDenseRecursiveData C;

        SeqDenseBatchData GateO;
        SeqDenseBatchData GateI;
        SeqDenseBatchData CHat;
        SeqDenseBatchData GateF;
        SeqDenseBatchData TanhC;

        CudaPieceFloat Lag1O;
        HiddenBatchData InitC = null;
        HiddenBatchData InitO = null;

        HiddenBatchData RemapInitC = null;
        HiddenBatchData RemapInitO = null;

        BasicMLPAttentionRunner AttentionRunner = null;

        public FastLSTMDenseRunner(LSTMCell model, SeqDenseRecursiveData input, RunnerBehavior behavior) : this(model, input, null, null, null, behavior)
        { }

        public FastLSTMDenseRunner(LSTMCell model, SeqDenseRecursiveData input, HiddenBatchData initO, HiddenBatchData initC, BasicMLPAttentionRunner attentionRunner, RunnerBehavior behavior) : base(model, input, behavior)
        {
            InitC = initC;
            InitO = initO;

            if (InitO != null) RemapInitO = new HiddenBatchData(InitO.MAX_BATCHSIZE, InitO.Dim, Behavior.RunMode, Behavior.Device);
            if (InitC != null) RemapInitC = new HiddenBatchData(InitC.MAX_BATCHSIZE, InitC.Dim, Behavior.RunMode, Behavior.Device);

            GateO = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
            GateI = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
            CHat = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
            GateF = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
            TanhC = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);

            Lag1O = new CudaPieceFloat(Input.Stat.MAX_SEQUENCESIZE * Model.CellDim, true, Behavior.Device == DeviceType.GPU);

            Output = new SeqDenseRecursiveData(new SequenceDataStat(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim), Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Behavior.Device);
            C = new SeqDenseRecursiveData(new SequenceDataStat(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim), Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Behavior.Device);
            AttentionRunner = attentionRunner;
        }

        public override void Forward()
        {
            if (InitO != null)
            {
                RemapInitO.BatchSize = InitO.BatchSize;
                ComputeLib.Matrix_AdditionMask(InitO.Output.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               RemapInitO.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               RemapInitO.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               InitO.Dim, InitO.BatchSize, 1, 0, 0);
            }

            if (InitC != null)
            {
                RemapInitC.BatchSize = InitC.BatchSize;
                ComputeLib.Matrix_AdditionMask(InitC.Output.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               RemapInitC.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               RemapInitC.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               InitC.Dim, InitC.BatchSize, 1, 0, 0);
            }

            if (AttentionRunner == null)
            {
                LSTMComputeForwardLib.LSTMForwardv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
            }
            else
            {
                if(AttentionRunner.IsDelegate) AttentionRunner.Forward();
                LSTMComputeForwardLib.LSTMForwardAttentionv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output);
            }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            ComputeLib.Zero(C.SentDeriv, C.SentSize * C.Dim);

            if (RemapInitO != null) { ComputeLib.Zero(RemapInitO.Deriv.Data, RemapInitO.BatchSize * RemapInitO.Dim); }
            if (RemapInitC != null) { ComputeLib.Zero(RemapInitC.Deriv.Data, RemapInitC.BatchSize * RemapInitC.Dim); }

            if (AttentionRunner != null && AttentionRunner.IsDelegate) { AttentionRunner.CleanDeriv(); }
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (AttentionRunner == null)
            {
                LSTMComputeBackwardDataLib.LSTMBackwardDatav2(ComputeLib, Input, RemapInitO, RemapInitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
            }
            else
            {
                LSTMComputeBackwardDataLib.LSTMAttentionBackwardDatav2(ComputeLib, Input, RemapInitO, RemapInitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output);
                if(AttentionRunner.IsDelegate) AttentionRunner.Backward(cleanDeriv);
            }

            if (RemapInitO != null)
            {
                ComputeLib.Matrix_AdditionMask(RemapInitO.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                               InitO.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitO.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitO.Dim, InitO.BatchSize, 1, 1, 0);
            }

            if (RemapInitC != null)
            {
                ComputeLib.Matrix_AdditionMask(RemapInitC.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                               InitC.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitC.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitC.Dim, InitC.BatchSize, 1, 1, 0);
            }

        }

        public override void Update()
        {
            if (AttentionRunner == null)
            {
                LSTMComputeBackwardWeightLib.LSTMBackwardWeightv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output, Lag1O);
            }
            else
            {
                AttentionRunner.IsDelegateOptimizer = IsDelegateOptimizer;
                LSTMComputeBackwardWeightLib.LSTMAttentionBackwardWeightv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output, Lag1O);
                if(AttentionRunner.IsDelegate) AttentionRunner.Update();
            }

        }
    }

    

    public class LSTMStateRunner : CompositeNetRunner // StructRunner<LSTMCell, HiddenBatchData>
    {
        public HiddenBatchData pO { get; set; }
        public HiddenBatchData pC { get; set; }

        public new HiddenBatchData Input { get; set; }
        new LSTMCell Model { get; set; }

        public HiddenBatchData O { get; set; }
        public HiddenBatchData C { get; set; }

        HiddenBatchData GateF; 
        HiddenBatchData GateI; 
        HiddenBatchData GateO;
        HiddenBatchData CHat;
        HiddenBatchData TanhC;
        
        public LSTMStateRunner(LSTMCell model, HiddenBatchData po, HiddenBatchData pc, HiddenBatchData input, RunnerBehavior behavior) : base(behavior)
        {
            pO = po;
            pC = pc;
            Input = input;
            Model = model;

            O = new HiddenBatchData(po.MAX_BATCHSIZE, po.Dim, Behavior.RunMode, Behavior.Device);
            C = new HiddenBatchData(pC.MAX_BATCHSIZE, pC.Dim, Behavior.RunMode, Behavior.Device);

            GateF = new HiddenBatchData(po.MAX_BATCHSIZE, po.Dim, Behavior.RunMode, Behavior.Device);
            GateI = new HiddenBatchData(po.MAX_BATCHSIZE, po.Dim, Behavior.RunMode, Behavior.Device);
            GateO = new HiddenBatchData(po.MAX_BATCHSIZE, po.Dim, Behavior.RunMode, Behavior.Device);
            CHat = new HiddenBatchData(po.MAX_BATCHSIZE, po.Dim, Behavior.RunMode, Behavior.Device);
            TanhC = new HiddenBatchData(po.MAX_BATCHSIZE, po.Dim, Behavior.RunMode, Behavior.Device);

            /*Wf X -> GateF*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(Input), new MatrixData(Model.CellDim, Model.InputFeatureDim, Model.Wf, Model.WfGrad, Behavior.Device),
                new MatrixData(GateF), 0, Behavior));
            /*Wi X -> GateI*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(Input), new MatrixData(Model.CellDim, Model.InputFeatureDim, Model.Wi, Model.WiGrad, Behavior.Device),
                new MatrixData(GateI), 0, Behavior));
            /*Wo X -> GateO*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(Input), new MatrixData(Model.CellDim, Model.InputFeatureDim, Model.Wo, Model.WoGrad, Behavior.Device),
                new MatrixData(GateO), 0, Behavior));
            /*Wc X -> C_hat*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(Input), new MatrixData(Model.CellDim, Model.InputFeatureDim, Model.Wc, Model.WcGrad, Behavior.Device),
                new MatrixData(CHat), 0, Behavior));

            /*Uf h_{t-1} -> GateF*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(pO), new MatrixData(Model.CellDim, Model.CellDim, Model.Uf, Model.UfGrad, Behavior.Device),
                new MatrixData(GateF), 1, Behavior));
            /*Ui h_{t-1} -> GateI*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(pO), new MatrixData(Model.CellDim, Model.CellDim, Model.Ui, Model.UiGrad, Behavior.Device),
                new MatrixData(GateI), 1, Behavior));
            /*Uo h_{t-1} -> GateO*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(pO), new MatrixData(Model.CellDim, Model.CellDim, Model.Uo, Model.UoGrad, Behavior.Device),
                new MatrixData(GateO), 1, Behavior));
            /*Uc h_{t-1} -> C_hat*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(pO), new MatrixData(Model.CellDim, Model.CellDim, Model.Uc, Model.UcGrad, Behavior.Device),
                new MatrixData(CHat), 1, Behavior));

            // GateF
            LinkRunners.Add(new BiasAdditionProcessor(new MatrixData(GateF), new VectorData(Model.CellDim, Model.Bf, Model.BfGrad, Behavior.Device), Behavior));
            LinkRunners.Add(new ActivationRunner(GateF, A_Func.Sigmoid, Behavior));
            // GateI
            LinkRunners.Add(new BiasAdditionProcessor(new MatrixData(GateI), new VectorData(Model.CellDim, Model.Bi, Model.BiGrad, Behavior.Device), Behavior));
            LinkRunners.Add(new ActivationRunner(GateI, A_Func.Sigmoid, Behavior));
            // C_hat
            LinkRunners.Add(new BiasAdditionProcessor(new MatrixData(CHat), new VectorData(Model.CellDim, Model.Bc, Model.BcGrad, Behavior.Device), Behavior));
            LinkRunners.Add(new ActivationRunner(CHat, A_Func.Tanh, Behavior));

            //c = gate_i @ c_hat
            LinkRunners.Add(new ProductRunner(new MatrixData(GateI), new MatrixData(CHat), new MatrixData(C), 0, 1, Behavior));
            //c = c + gate_f @ c_{t-1}
            LinkRunners.Add(new ProductRunner(new MatrixData(GateF), new MatrixData(pC), new MatrixData(C), 1, 1, Behavior));

            // GateO = GateO + Vo C
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(C), new MatrixData(Model.CellDim, Model.CellDim, Model.Vo, Model.VoGrad, Behavior.Device),
                new MatrixData(GateO), 1, Behavior));

            // GateO
            LinkRunners.Add(new BiasAdditionProcessor(new MatrixData(GateO), new VectorData(Model.CellDim, Model.Bo, Model.BoGrad, Behavior.Device), Behavior));
            LinkRunners.Add(new ActivationRunner(GateO, A_Func.Sigmoid, Behavior));

            // tanhc = tanh(c)
            LinkRunners.Add(new ActivationRunner(C, A_Func.Tanh, TanhC, Behavior));

            // h = gate_o @ tanhc 
            LinkRunners.Add(new ProductRunner(new MatrixData(GateO), new MatrixData(TanhC), new MatrixData(O), 0, 1, Behavior));

            //ComputeLib.ElementwiseProduct(GateO.Output.Data, 0, TanhC.Output.Data, 0, O.Output.Data, 0, Input.BatchSize, Model.CellDim, 0);
        }
        
        public override void CleanDeriv()
        {
            if (O.BatchSize == 0) { return; }
            ComputeLib.Zero(O.Deriv.Data, O.Dim * Input.BatchSize);
            ComputeLib.Zero(C.Deriv.Data, C.Dim * Input.BatchSize);
            ComputeLib.Zero(GateF.Deriv.Data, O.Dim * O.BatchSize);
            ComputeLib.Zero(GateI.Deriv.Data, O.Dim * O.BatchSize);
            ComputeLib.Zero(GateO.Deriv.Data, O.Dim * O.BatchSize);
            ComputeLib.Zero(CHat.Deriv.Data, O.Dim * O.BatchSize);
            ComputeLib.Zero(TanhC.Deriv.Data, O.Dim * O.BatchSize);
        }
    }
}
