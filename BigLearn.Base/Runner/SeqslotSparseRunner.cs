﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    //public class SeqslotSparseRunner<IN> : StructRunner<RNNCell, IN> where IN : SeqSparseSlotBatchData
    //{
    //    public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

    //    //BatchData input,  input,
    //    //public SeqslotSparseRunner(Structure model, RunnerBehavior behavior)
    //    //    : base(model, behavior)
    //    //{
    //    //    Output = new SeqDenseBatchData();
    //    //}

    //    public override void InitMemory()
    //    {
    //        if (Output.MAX_BATCHSIZE < Input.Stat.MAX_BATCHSIZE)
    //        {
    //            Output.Init(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
    //        }
    //    }

    //    ~SeqslotSparseRunner()
    //    {
    //        this.Dispose(false);
    //    }

    //    private bool disposed = false;
    //    protected override void Dispose(bool disposing)
    //    {
    //        if (this.disposed)
    //        {
    //            return;
    //        }

    //        this.disposed = true;

    //        if (disposing)
    //        {
    //            if (Output != null)
    //            {
    //                Output.Dispose();
    //            }
    //        }

    //        Output = null;

    //        base.Dispose(disposing);
    //    }

    //    public override void Forward()
    //    {
    //        MathOperatorManager.MathDevice = Behavior.Device;
    //        Output.SampleIdx = Input.InputSeqData.SampleIdx;
    //        Output.SentMargin = Input.InputSeqData.SentMargin;

    //        Output.BatchSize = Input.InputSeqData.BatchSize;
    //        Output.SentSize = Input.InputSeqData.SentSize;
    //        InitMemory();

    //        ///Here we will use the ground truth.
    //        Cudalib.RNNForwardPropagateSlot(Input.InputSeqData.SampleIdx.CudaPtr, Input.InputSeqData.BatchSize, Input.InputSeqData.SequenceIdx.CudaPtr, Input.InputSeqData.SentSize,
    //                Input.InputSeqData.FeaIdx.CudaPtr, Input.InputSeqData.FeaValue.CudaPtr, Model.IMatrix.CudaPtr, Model.WMatrix.CudaPtr,
    //                Model.HistoryLag, Input.Stat.FEATURE_DIM, Model.HiddenStateDim, Model.Bias.CudaPtr, (int)Model.Af, Output.SentOutput.CudaPtr,
    //                Input.SentGroundTruth.CudaPtr, Input.SampleEarlyStop.CudaPtr);

    //    }

    //    /// <summary>
    //    /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
    //    /// </summary>
    //    /// <param name="cleanDeriv"></param>
    //    public override void Backward(bool cleanDeriv)
    //    {
    //        // Backpropagate Deriv.
    //        Cudalib.RNNBackwardPropagateSlot(Input.InputSeqData.SampleIdx.CudaPtr, Input.InputSeqData.BatchSize,
    //                Output.SentDeriv.CudaPtr, Input.SentGroundTruth.CudaPtr,
    //             Input.InputSeqData.SentSize, Model.WMatrix.CudaPtr, Model.HiddenStateDim, (int)Model.Af, Input.SampleEarlyStop.CudaPtr);
    //    }

    //    public override void Update()
    //    {
    //        Model.IMatrixOptimizer.BeforeGradient();
    //        Cudalib.Sparse_Matrix_Transpose_Multiply_INTEX_Weight(Input.InputSeqData.SequenceIdx.CudaPtr, Input.InputSeqData.SentSize, Input.InputSeqData.FeaIdx.CudaPtr,
    //                                    Input.InputSeqData.FeaValue.CudaPtr, Input.InputSeqData.ElementSize, Model.IMatrixOptimizer.Gradient.CudaPtr,
    //                                    Output.SentDeriv.CudaPtr, Model.InputFeatureDim, Model.HiddenStateDim,
    //                                    Model.IMatrixOptimizer.GradientStep);
    //        Model.IMatrixOptimizer.AfterGradient();

    //        Model.WMatrixOptimizer.BeforeGradient();
    //        Cudalib.RecurrentUpdateSlot(Input.InputSeqData.SampleIdx.CudaPtr, Input.InputSeqData.BatchSize, Input.SampleEarlyStop.CudaPtr,
    //            Input.SentGroundTruth.CudaPtr, Output.SentDeriv.CudaPtr, Model.HiddenStateDim,
    //                            Input.InputSeqData.SentSize, Model.HistoryLag, Model.WMatrixOptimizer.Gradient.CudaPtr, Model.WMatrixOptimizer.GradientStep);
    //        Model.WMatrixOptimizer.AfterGradient();

    //        if (Model.IsBias)
    //        {
    //            Model.BiasOptimizer.BeforeGradient();
    //            Cudalib.Matrix_Aggragate_Weight(Output.SentDeriv.CudaPtr, Model.BiasOptimizer.Gradient.CudaPtr, Output.SentSize, Model.HiddenStateDim, Model.BiasOptimizer.GradientStep);
    //            Model.BiasOptimizer.AfterGradient();
    //        }
    //    }
    //}

}
