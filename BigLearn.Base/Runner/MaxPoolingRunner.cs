﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MaxPoolingRunner<IN> : StructRunner<Structure, IN> where IN : SeqDenseBatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData) base.Output; } set { base.Output = value; } }

        //SeqHelpData TransInput = null;
        CudaPieceInt RecurrsiveMapForward = CudaPieceInt.Empty;
        CudaPieceInt LayerMaxPoolingIndex = null;


        public MaxPoolingRunner(SeqDenseBatchData data, RunnerBehavior behavior) : this(data, CudaPieceInt.Empty, behavior) { }


        public MaxPoolingRunner(SeqDenseBatchData data, CudaPieceInt recurrsiveMapForward, RunnerBehavior behavior)
            : base(Structure.Empty, data, behavior)
        {
            RecurrsiveMapForward = recurrsiveMapForward;
            Output = new HiddenBatchData(Input.Stat.MAX_BATCHSIZE, Input.Dim, Behavior.RunMode, Behavior.Device);
            LayerMaxPoolingIndex = new CudaPieceInt(Input.Stat.MAX_BATCHSIZE * Input.Dim, true, Behavior.Device == DeviceType.GPU);
        }

        public override void Forward()
        {
            iteration++;
            Output.BatchSize = Input.BatchSize;
            if (Output.BatchSize == 0) return;
            ComputeLib.MaxPoolingMask(Input.SentOutput, Input.SampleIdx, RecurrsiveMapForward, Input.BatchSize, Output.Output.Data, LayerMaxPoolingIndex, Input.Dim);

        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.BatchSize == 0) return;
            ComputeLib.DerivMaxPooling(Output.Deriv.Data, LayerMaxPoolingIndex, Input.SentDeriv, Input.BatchSize, Input.Dim, 1, 1);
        }

        public override void CleanDeriv()
        {
            if (Output.BatchSize == 0) return;
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

    }
}
