﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MaxPoolSpanRunner<IN> : StructRunner<Structure, IN> where IN : SeqDenseBatchData
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
        DenseBatchData Start;
        DenseBatchData End;
        BiMatchBatchData Match;
        CudaPieceInt LayerMaxPoolingIndex = null;

        public MaxPoolSpanRunner(SeqDenseBatchData data, DenseBatchData start, DenseBatchData end, BiMatchBatchData matchData, RunnerBehavior behavior)
            : base(Structure.Empty, data, behavior)
        {
            Start = start;
            End = end;
            Match = matchData;
            Output = new SeqDenseBatchData(matchData.Src2MatchIdx, matchData.SrcIdx, data.MAX_BATCHSIZE, matchData.Stat.MAX_MATCH_BATCHSIZE, data.Dim, behavior.Device);
            LayerMaxPoolingIndex = new CudaPieceInt(matchData.Stat.MAX_MATCH_BATCHSIZE * data.Dim, true, Behavior.Device == DeviceType.GPU);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Match.MatchSize;

            //Match.Src2MatchIdx.SyncToCPU();
            
            
            //Input.SentOutput.SyncToCPU();
            //Input.SampleIdx.SyncToCPU();
            //Start.Data.SyncToCPU();
            //End.Data.SyncToCPU();
            //Match.SrcIdx.SyncToCPU();

            //Match.SrcIdx.SyncToCPU();
            ComputeLib.SpanMaxPool(Input.SentOutput, Input.SampleIdx, Start.Data, End.Data, Match.SrcIdx, Match.MatchSize,  Output.SentOutput, LayerMaxPoolingIndex, Input.Dim);
            //Output.SentOutput.SyncToCPU();
            //LayerMaxPoolingIndex.SyncToCPU();
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            //Output.SampleIdx.SyncToCPU();
            ComputeLib.DerivSpanMaxPool(Output.SentDeriv, Output.SampleIdx, Output.BatchSize, Input.SentDeriv, LayerMaxPoolingIndex, Input.Dim, 1);
            //Input.SentDeriv.SyncToCPU();
            //Output.SentDeriv.SyncToCPU();
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }
    }


    public class LastPoolSpanRunner<IN> : StructRunner<Structure, IN> where IN : SeqDenseBatchData
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
        DenseBatchData Start;
        DenseBatchData End;
        BiMatchBatchData Match;

        public LastPoolSpanRunner(SeqDenseBatchData data, DenseBatchData start, DenseBatchData end, BiMatchBatchData matchData, RunnerBehavior behavior)
            : base(Structure.Empty, data, behavior)
        {
            Start = start;
            End = end;
            Match = matchData;

            Output = new SeqDenseBatchData(matchData.Src2MatchIdx, matchData.SrcIdx, data.MAX_BATCHSIZE, matchData.Stat.MAX_MATCH_BATCHSIZE, data.Dim * 2, behavior.Device);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Match.MatchSize;
            ComputeLib.SpanLastPool(Input.SentOutput, Input.SampleIdx, Start.Data, End.Data, Match.SrcIdx, Match.MatchSize, Output.SentOutput, Input.Dim);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.DerivSpanLastPool(Output.SentDeriv, Output.SampleIdx, Start.Data, End.Data, Output.BatchSize, Input.SentDeriv, Input.SampleIdx, Input.Dim, 1);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }
    }

}
