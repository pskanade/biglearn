﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// location based memory retrieval.
    /// </summary>
    /// <typeparam name="IN"></typeparam>
    public class MemoryControlRunner<IN> : StructRunner<Structure, IN> where IN : BatchData
    {
        /// <summary>
        /// Output of the key for the memory. (location based memory focusing)
        /// </summary>
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public SeqDenseBatchData InputMemory;
        public HiddenBatchData InputKey;

        //CudaPieceInt LocationIndex;
        CudaPieceFloat LocationValue;
        CudaPieceFloat LocationDeriv;
        
        public int Window { get; private set; }
        public MemoryControlRunner(SeqDenseBatchData memory, HiddenBatchData key, // input for the memory control.
            int window, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            
            InputMemory = memory;
            InputKey = key;
            Window = window;

            Output = new HiddenBatchData(InputMemory.Stat.MAX_BATCHSIZE, InputMemory.Dim, Behavior.RunMode, Behavior.Device);
            //LocationIndex = new CudaPieceInt(InputMemory.Stat.MAX_BATCHSIZE * InputKey.Dim * Window, true, Behavior.Device == DeviceType.GPU);
            LocationValue = new CudaPieceFloat(InputMemory.Stat.MAX_SEQUENCESIZE * InputKey.Dim, true, Behavior.Device == DeviceType.GPU);
            LocationDeriv = new CudaPieceFloat(InputMemory.Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);
        }

        /// <summary>
        /// key 
        /// </summary>
        public override void Forward()
        {
            MathOperatorManager.MathDevice = Behavior.Device;
            InitMemory();
            Output.BatchSize = InputKey.BatchSize;

            InputKey.Output.Data.CopyOutFromCuda();
            InputMemory.SampleIdx.CopyOutFromCuda();
            
            ///from the location pointer to gaussian windows.
            //Cudalib.RectifiedGaussian(InputKey.Output.Data.CudaPtr, InputKey.Dim, InputKey.BatchSize,
            //                    InputMemory.SampleIdx.CudaPtr, Window, 0.5f, LocationIndex.CudaPtr, LocationValue.CudaPtr);
            Cudalib.Gaussian(InputKey.Output.Data.CudaPtr, InputKey.Dim, InputKey.BatchSize,
                                InputMemory.SampleIdx.CudaPtr, 10, LocationValue.CudaPtr);

            //LocationIndex.CopyOutFromCuda();
            LocationValue.CopyOutFromCuda();

            ///from the location value to memory.
            Cudalib.GaussianMemoryAddressing(InputMemory.SampleIdx.CudaPtr, InputMemory.BatchSize,
                                InputMemory.SentOutput.CudaPtr, LocationValue.CudaPtr, InputKey.Dim, InputMemory.Dim, Output.Output.Data.CudaPtr);

            Output.Output.Data.CopyOutFromCuda();
            //.MemoryAddressing(InputMemory.SampleIdx.CudaPtr, InputMemory.BatchSize,
            //                InputMemory.SentOutput.CudaPtr, LocationIndex.CudaPtr,
            //                LocationValue.CudaPtr, InputKey.Dim, Window, InputMemory.Dim, Output.Output.Data.CudaPtr);
            /// Given the location and its guassian window, retrieval the Memory.
        }

        public override void CleanDeriv()
        {
            Output.Deriv.Data.Zero();
        }

        public override void Backward(bool cleanDeriv)
        {
            /// 5 * 0.6 + 6 * 0.1 + 7 * 0.3
            /// 10 * 0.8 + 11 * 0.05 + 12 * 0.05
            /// From Output.Deriv.Data.CudaPtr --> Location Value. 
            /// On Memory Derivation; and On LocationValue Derivation;

            Output.Deriv.Data.CopyOutFromCuda();
            InputMemory.SentMargin.CopyOutFromCuda();

            /// LocationValueDerivation.
            //Cudalib.DerivMemoryAddressing(InputMemory.SampleIdx.CudaPtr, InputMemory.BatchSize,
            //                    InputMemory.SentOutput.CudaPtr, LocationIndex.CudaPtr,
            //                    LocationDeriv.CudaPtr, InputKey.Dim, Window, InputMemory.Dim, Output.Deriv.Data.CudaPtr);
            Cudalib.DerivGaussianMemoryAddressing(InputMemory.SentMargin.CudaPtr, InputMemory.SentSize, InputMemory.SentOutput.CudaPtr, LocationDeriv.CudaPtr, 
                InputMemory.Dim, Output.Deriv.Data.CudaPtr);

            //Cudalib.DerivMemory(InputMemory.SampleIdx.CudaPtr, InputMemory.BatchSize, 
            //                    InputMemory.SentDeriv.CudaPtr, LocationIndex.CudaPtr, LocationValue.CudaPtr,
            //                    InputKey.Dim, Window, InputMemory.Dim, Output.Deriv.Data.CudaPtr);
            Cudalib.DerivGaussianMemory(InputMemory.SentMargin.CudaPtr, InputMemory.SentSize, InputMemory.SentDeriv.CudaPtr, LocationValue.CudaPtr, InputKey.Dim,
                    InputMemory.Dim, Output.Deriv.Data.CudaPtr);

            LocationDeriv.CopyOutFromCuda();

            Cudalib.DerivGaussian(InputKey.Output.Data.CudaPtr, InputKey.Dim, InputMemory.BatchSize, InputMemory.SampleIdx.CudaPtr,
                                10, LocationValue.CudaPtr, InputKey.Deriv.Data.CudaPtr, LocationDeriv.CudaPtr);

            //Cudalib.DerivRectifiedGaussian(InputKey.Output.Data.CudaPtr, InputKey.Dim, InputMemory.BatchSize,
            //                    InputMemory.SampleIdx.CudaPtr, Window, 0.5f, LocationIndex.CudaPtr, LocationValue.CudaPtr,
            //                    InputKey.Deriv.Data.CudaPtr, LocationDeriv.CudaPtr);
            InputKey.Deriv.Data.CopyOutFromCuda();
            InputMemory.SentDeriv.CopyOutFromCuda();
        }

        public override void Update()
        { }

    }
}
