﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Hideen Sequence of convolution nn.
    /// </summary>
    /// <typeparam name="IN"></typeparam>
    public class ConvDenseRunner<IN> : StructRunner<LayerStructure, IN> where IN : SeqDenseBatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        /// <summary>
        /// Number of words multiply 
        /// </summary>
        //CudaPieceFloat LayerPoolingOutput = null;

        /// <summary>
        /// Used if convolutional and maxpooling
        /// </summary>
        //CudaPieceInt LayerMaxPoolingIndex = null;

        //HiddenBatchData ConvInput = null;

        SeqDenseConvRunner<SeqDenseBatchData> ConvRunner = null;
        MaxPoolingRunner<SeqDenseBatchData> MaxPoolRunner = null;

        /// <summary>
        /// remove the risky construction function.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="data"></param>
        /// <param name="behavior"></param>
        //public ConvDenseRunner(Structure model, RunnerBehavior behavior)
        //    : base(model, behavior)
        //{
        //    Output = new HiddenBatchData();
        //}

        public ConvDenseRunner(LayerStructure model, SeqDenseBatchData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            ConvRunner = new SeqDenseConvRunner<SeqDenseBatchData>((model), data, behavior);
            MaxPoolRunner = new MaxPoolingRunner<SeqDenseBatchData>(ConvRunner.Output, behavior);
            Output = MaxPoolRunner.Output;
            //new HiddenBatchData(Input.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
            //LayerPoolingOutput = new CudaPieceFloat(Input.MAX_SENTSIZE * Model.Neural_Out, true, Behavior.Device == DeviceType.GPU);
            //LayerMaxPoolingIndex = new CudaPieceInt(Input.MAX_BATCHSIZE * Model.Neural_Out, true, Behavior.Device == DeviceType.GPU);
            //ConvInput = new HiddenBatchData(Input.MAX_SENTSIZE, Input.Dim * Model.N_Winsize, Behavior.RunMode, Behavior.Device);
        }

        public override void CleanDeriv()
        {
            ConvRunner.CleanDeriv();
            MaxPoolRunner.CleanDeriv();
        }

        public override void Forward()
        {
            ConvRunner.Forward();
            MaxPoolRunner.Forward();
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            MaxPoolRunner.Backward(cleanDeriv);
            ConvRunner.Backward(cleanDeriv);

            // Backpropagate Deriv.
            //Model.DeactiveOutput(Output.Output.Data, Output.Deriv.Data, Output.BatchSize, Behavior.RunMode);

            //ComputeLib.DerivMaxPooling(Output.Deriv.Data, LayerMaxPoolingIndex, Input.SentDeriv, Input.BatchSize, Input.Dim, 1, 1);

            // Conv Matrix Multipy Weight.
            //Cudalib.Conv_Matrix_Multipy(Output.Deriv.Data.CudaPtr, Model.weight.CudaPtr, Input.SentDeriv.CudaPtr, Input.BatchSize, Input.SentSize,
            //        Input.SentMargin.CudaPtr, LayerMaxPoolingIndex.CudaPtr, Model.Neural_Out, Model.Neural_In, Model.N_Winsize);
        }

        public override void Update()
        {
            ConvRunner.IsDelegateOptimizer = IsDelegateOptimizer;
            MaxPoolRunner.IsDelegateOptimizer = IsDelegateOptimizer;

            ConvRunner.Update();
            MaxPoolRunner.Update();
            /*
            if (!IsDelegateOptimizer) Model.WeightOptimizer.BeforeGradient();
            switch (Model.Nt)
            {
                case N_Type.Convolution_layer:
                    Cudalib.Convolution_Dense_Matrix_Product_INTEX_Weight(Output.Deriv.Data.CudaPtr, LayerMaxPoolingIndex.CudaPtr, Input.SentMargin.CudaPtr, Input.SentSize,
                                                                        Model.N_Winsize, Input.BatchSize, Model.Neural_Out, Input.SentOutput.CudaPtr,
                                                                        Model.WeightOptimizer.Gradient.CudaPtr, Model.Neural_In, Model.WeightOptimizer.GradientStep);
                    break;
            }
            if (!IsDelegateOptimizer) Model.WeightOptimizer.AfterGradient();

            if (Model.IsBias)
            {
                if(!IsDelegateOptimizer) Model.BiasOptimizer.BeforeGradient();
                ComputeLib.ColumnWiseSum(Output.Deriv.Data, Model.BiasOptimizer.Gradient, Output.BatchSize, Model.Neural_Out, Model.BiasOptimizer.GradientStep);
                //Cudalib.Matrix_Aggragate_Weight(Output.Deriv.Data.CudaPtr, Model.BiasOptimizer.Gradient.CudaPtr, Output.BatchSize, Model.Neural_Out, Model.BiasOptimizer.GradientStep);
                if (!IsDelegateOptimizer) Model.BiasOptimizer.AfterGradient();
            }
            */
        }


        private bool disposed = false;
        ~ConvDenseRunner()
        {
            this.Dispose(false);
        }

        protected override void Dispose(bool disposing)
        {
            if (this.disposed) { return; }
            this.disposed = true;

            Output.Dispose();
            base.Dispose(disposing);
        }

    }
}
