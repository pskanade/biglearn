﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    //public class ReverseDenseSeqRunner : StructRunner
    //{
    //    public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

    //    public new SeqDenseBatchData Input { get { return (SeqDenseBatchData)base.Input; } set { base.Input = value; } }
    //    public SeqHelpData HelpInput { get; set; }


    //    CudaPieceInt ReverseIdx;
    //    public ReverseDenseSeqRunner(SeqDenseBatchData data, SeqHelpData helpData, RunnerBehavior behavior)
    //        : base(Structure.Empty, behavior)
    //    {
    //        Input = data;
    //        HelpInput = helpData;
    //        Output = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Input.Stat.FEATURE_DIM, Behavior.Device);
    //        ReverseIdx = new CudaPieceInt(Input.Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);
    //    }

    //    public override void Forward()
    //    {
    //        for(int i=0;i<Input.BatchSize;i++)
    //        {
    //            int smpEnd = HelpInput.SampleIdx[i];
    //            int smpBgn = i == 0 ? 0 : HelpInput.SampleIdx[i - 1];
    //            for(int e = smpBgn; e < smpEnd; e++)
    //            {
    //                int lag = e - smpBgn;
    //                int origPos = (lag == 0 ? 0 : HelpInput.LagSeqIndex[lag - 1]) + i;

    //                int newLag = smpEnd - 1 - e;
    //                int newPos = (newLag == 0 ? 0 : HelpInput.LagSeqIndex[newLag - 1]) + i;

    //                ReverseIdx.MemPtr[origPos] = newPos;
    //            }
    //        }
    //        ReverseIdx.SyncFromCPU(Input.SentSize);

    //        Output.BatchSize = Input.BatchSize;
    //        Output.SentSize = Input.SentSize;
    //        Output.SampleIdx = Input.SampleIdx;
    //        Output.SentMargin = Input.SentMargin;

    //        ComputeLib.Matrix_AdditionMask(Input.SentOutput, 0, CudaPieceInt.Empty, 0,
    //                                       Output.SentOutput, 0, ReverseIdx, 0,
    //                                       Output.SentOutput, 0, ReverseIdx, 0,
    //                                       Output.Dim, Output.SentSize, 1, 0, 0);
    //    }

    //    public override void CleanDeriv()
    //    {
    //        ComputeLib.Zero(Output.SentDeriv, Output.Dim * Output.SentSize);
    //    }

    //    public override void Backward(bool cleanDeriv)
    //    {
    //        ComputeLib.Matrix_AdditionMask(Output.SentDeriv, 0, ReverseIdx, 0, 
    //                                       Input.SentDeriv, 0, CudaPieceInt.Empty, 0,
    //                                       Input.SentDeriv, 0, CudaPieceInt.Empty, 0,
    //                                       Input.Dim, Input.SentSize, 1, 1, 0);
    //    }
    //}
}
