﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MeanPoolingRunner<IN> : StructRunner<Structure, IN> where IN : SeqDenseBatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        //SeqHelpData TransInput = null;
        CudaPieceInt RecurrsiveMapForward = CudaPieceInt.Empty;
        //CudaPieceInt LayerMaxPoolingIndex = null;
        CudaPieceFloat weight = null;
        CudaPieceFloat weight1 = null;
        CudaPieceFloat tmpWeight = null;
        public MeanPoolingRunner(SeqDenseBatchData data, RunnerBehavior behavior) : this(data, CudaPieceInt.Empty, behavior) { }


        public MeanPoolingRunner(SeqDenseBatchData data, CudaPieceInt recurrsiveMapForward, RunnerBehavior behavior)
            : base(Structure.Empty, data, behavior)
        {
            RecurrsiveMapForward = recurrsiveMapForward;
            Output = new HiddenBatchData(Input.Stat.MAX_BATCHSIZE, Input.Dim, Behavior.RunMode, Behavior.Device);
            weight = new CudaPieceFloat(Input.MAX_SENTSIZE, Behavior.Device);
            weight1 = new CudaPieceFloat(Input.MAX_BATCHSIZE, Behavior.Device);
            tmpWeight = new CudaPieceFloat(Output.MAX_BATCHSIZE * Output.Dim, Behavior.Device);
            ///LayerMaxPoolingIndex = new CudaPieceInt(Input.Stat.MAX_BATCHSIZE * Input.Dim, true, Behavior.Device == DeviceType.GPU);
        }

        //public MaxPoolingRunner(SeqDenseBatchData data, SeqHelpData trans, RunnerBehavior behavior)
        //    : base(Structure.Empty, data, behavior)
        //{
        //    TransInput = trans;
        //    Output = new HiddenBatchData(Input.Stat.MAX_BATCHSIZE, Input.Dim, Behavior.RunMode, Behavior.Device);
        //    LayerPoolingOutput = new CudaPieceFloat(Input.Stat.MAX_SEQUENCESIZE * Input.Dim, true, Behavior.Device == DeviceType.GPU);
        //    LayerMaxPoolingIndex = new CudaPieceInt(Input.Stat.MAX_BATCHSIZE * Input.Dim, true, Behavior.Device == DeviceType.GPU);
        //}

        public override void Forward()
        {
            iteration++;
            Output.BatchSize = Input.BatchSize;

            Input.SampleIdx.SyncToCPU();
            for (int i = 0; i < Input.BatchSize; i++)
            {
                int smpBgn = i == 0 ? 0 : Input.SampleIdx.MemPtr[i - 1];
                int smpEnd = Input.SampleIdx.MemPtr[i];
                for (int m = smpBgn; m < smpEnd; m++)
                {
                    weight.MemPtr[m] = 1.0f / (smpEnd - smpBgn);
                }
                weight1.MemPtr[i] = 1.0f / (smpEnd - smpBgn);
            }
            weight.SyncFromCPU();
            weight1.SyncFromCPU();

            ComputeLib.ColumnWiseSumMask(Input.SentOutput, 0, RecurrsiveMapForward, 0, weight, Input.SampleIdx, Input.BatchSize,
                Output.Output.Data, 0, CudaPieceInt.Empty, 0, Input.SentSize, Input.Dim, 0, 1);
            //ComputeLib.MaxPoolingMask(Input.SentOutput, Input.SampleIdx, RecurrsiveMapForward, Input.BatchSize, Output.Output.Data, LayerMaxPoolingIndex, Input.Dim);

            //if (RecurrsiveMapForward != null) 
            //else if(TransInput != null) ComputeLib.MaxPoolingMask(Input.SentOutput, Input.SampleIdx, TransInput.TransSeqIndex, Input.BatchSize, Output.Output.Data, LayerMaxPoolingIndex, Input.Dim);
            //else ComputeLib.Max_Pooling(Input.SentOutput, Input.SampleIdx, Input.BatchSize, Output.Output.Data, LayerMaxPoolingIndex, Input.Dim);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            //if(iteration == 30 && name.Equals("char_maxpool_query"))
            //{
            //    Output.Deriv.SyncToCPU();
            //    LayerMaxPoolingIndex.SyncToCPU();
            //    Input.SentDeriv.SyncToCPU();
            //}

            ComputeLib.Scale_MatrixMask(Output.Deriv.Data, 0, CudaPieceInt.Empty, 0, tmpWeight, 0, CudaPieceInt.Empty, 0, Output.Dim, Output.BatchSize, weight1, 0);

            ComputeLib.Matrix_AdditionMask(tmpWeight, 0, Input.SentMargin, 0,
                                           tmpWeight, 0, Input.SentMargin, 0,
                                           Input.SentDeriv, 0, RecurrsiveMapForward, 0,
                                           Input.Dim, Input.SentSize, 1, 0, 1);

            //ComputeLib.DerivMaxPooling(Output.Deriv.Data, LayerMaxPoolingIndex, Input.SentDeriv, Input.BatchSize, Input.Dim, 1, 1);
            //if (iteration == 30 && name.Equals("char_maxpool_query"))
            //{
            //    Input.SentDeriv.SyncToCPU();
            //}
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }
    }
}
