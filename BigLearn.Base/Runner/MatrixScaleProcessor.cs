﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MatrixScaleProcessor : StructRunner
    {
        float Scale { get; set; }
        MatrixData Data { get; set; }

        public MatrixScaleProcessor(MatrixData data, float scale, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Data = data;
            Scale = scale;
        }

        public override void Forward()
        {
            ComputeLib.Scale_Matrix(Data.Output, Data.Row, Data.Column, Scale);
        }

        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Scale_Matrix(Data.Deriv, Data.Row, Data.Column, Scale);
        }
    }

    
    public class MatrixNormRunner : StructRunner
    {
        public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }
        public new VectorData Output { get { return (VectorData)base.Output; } set { base.Output = value; } }
        int Norm { get; set; }
        public MatrixNormRunner(HiddenBatchData input, int norm, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Norm = norm;
            Output = new VectorData(Input.MAX_BATCHSIZE, behavior.Device);
        }

        public override void Forward()
        {
            Output.Length = Input.BatchSize;
            if(Norm == 1)
            {
                ComputeLib.MatrixL1Norm(Input.Output.Data, Input.Dim, Input.BatchSize, Output.Output);    
            }
            else if(Norm == 2)
            {
                ComputeLib.MatrixL2Norm(Input.Output.Data, Input.Dim, Input.BatchSize, Output.Output);    
            }
        }
        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        public override void Backward(bool cleanDeriv)
        {
            if (Norm == 1)
                ComputeLib.DerivMatrixL1Norm(Input.Output.Data, Input.Dim, Input.BatchSize, Output.Output, Output.Deriv, Input.Deriv.Data);
            else if (Norm == 2)
                ComputeLib.DerivMatrixL2Norm(Input.Output.Data, Input.Dim, Input.BatchSize, Output.Output, Output.Deriv, Input.Deriv.Data);

        }

    }
}
