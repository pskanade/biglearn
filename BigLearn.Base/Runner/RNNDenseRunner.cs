﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class RNNDenseRunner : StructRunner<RNNCell, SeqDenseBatchData>
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        public RNNDenseRunner(RNNCell model, SeqDenseBatchData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            Output = new SeqDenseBatchData(new SequenceDataStat(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim), data.SampleIdx, data.SentMargin, Behavior.Device);
        }


        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            ComputeLib.Sgemm(Input.SentOutput, 0, Model.IMatrix, 0, Output.SentOutput, 0, Input.SentSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);

            ComputeLib.Matrix_Add_Linear(Output.SentOutput, Model.Bias, Input.SentSize, Model.HiddenStateDim);

            ComputeLib.RecursiveForwardPropagateBatch(Input.SampleIdx, Input.BatchSize, Output.SentOutput, Model.HiddenStateDim,
                                                          Model.WMatrix, Model.HistoryLag, (int)Model.Af, Output.SentOutput);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.RecursiveBackwardPropagateBatch(Input.SampleIdx, Input.BatchSize, Output.SentOutput, Model.HiddenStateDim,
                     Model.WMatrix, Model.HistoryLag, (int)Model.Af, Output.SentDeriv);
            ComputeLib.MatrixMultiplicationTranspose(Output.SentDeriv, Model.IMatrix, Input.SentDeriv, Input.SentSize, Model.HiddenStateDim, Model.InputFeatureDim, 1);
        }

        public override void Update()
        {
            if (!IsDelegateOptimizer) Model.IMatrixOptimizer.BeforeGradient();
            ComputeLib.MatrixMultiplicationLeftTranspose(Input.SentOutput, Output.SentDeriv, Model.IMatrixOptimizer.Gradient,
                                    Input.SentSize, Model.InputFeatureDim, Model.HiddenStateDim, Model.IMatrixOptimizer.GradientStep);
            if (!IsDelegateOptimizer) Model.IMatrixOptimizer.AfterGradient();

            if (!IsDelegateOptimizer) Model.WMatrixOptimizer.BeforeGradient();
            ComputeLib.RecursiveUpdateBatch(Input.SampleIdx, Input.BatchSize, Input.SentSize, Output.SentOutput, Model.HiddenStateDim,
                                        Output.SentDeriv, Model.HistoryLag, Model.WMatrixOptimizer.Gradient, Model.WMatrixOptimizer.GradientStep);
            if (!IsDelegateOptimizer) Model.WMatrixOptimizer.AfterGradient();

            if (Model.IsBias)
            {
                if (!IsDelegateOptimizer) Model.BiasOptimizer.BeforeGradient();
                ComputeLib.ColumnWiseSum(Output.SentDeriv, Model.BiasOptimizer.Gradient, Output.SentSize, Model.HiddenStateDim, Model.BiasOptimizer.GradientStep);
                if (!IsDelegateOptimizer) Model.BiasOptimizer.AfterGradient();
            }
        }

        #region Dispose Function.
        ~RNNDenseRunner()
        {
            this.Dispose(false);
        }

        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }

            base.Dispose(disposing);
        }
        #endregion
    }
}
