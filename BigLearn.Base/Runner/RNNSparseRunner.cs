﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class RNNSparseRunner: StructRunner<RNNCell, SeqSparseBatchData>
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        public RNNSparseRunner(RNNCell model, SeqSparseBatchData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            Output = new SeqDenseBatchData(new SequenceDataStat(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim), data.SampleIdx, data.SentMargin, Behavior.Device);    
        }
        
        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            ///// WI * Xi = Yi;
            ///*Wi X -> Hidden*/
            ComputeLib.Sparse_Matrix_Multiply_INTEX_Weight(
                                                    Input.SequenceIdx,
                                                    Input.FeaIdx,
                                                    Input.FeaValue,
                                                    Input.SentSize,
                                                    Input.ElementSize,
                                                    Model.IMatrix,
                                                    Output.SentOutput,
                                                    Model.HiddenStateDim,
                                                    0);
            /*Hidden + Bias*/
            /// Yi = Yi + Bias;
            ComputeLib.Matrix_Add_Linear(Output.SentOutput, Model.Bias, Input.SentSize, Model.HiddenStateDim);

            //MathOperatorManager.GlobalInstance.
            /// Y0 = Af(Y0)
            /// Y1 = Af(Y1 + W0 * Y0)
            /// Y2 = Af(Y2 + W0 Y1 + W1 Y0)
            /// Y3 = Af(Y3 + W0 Y2 + W1 Y1)
            /// Y(i+1) = Af( \sum_(0..T) Wt * Y(i-t))
            /// T = HistoryLag;
            ComputeLib.RecursiveForwardPropagateBatch(Input.SampleIdx, Input.BatchSize, Output.SentOutput, Model.HiddenStateDim,
                                                            Model.WMatrix, Model.HistoryLag, (int)Model.Af, Output.SentOutput);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.RecursiveBackwardPropagateBatch(Input.SampleIdx, Input.BatchSize, Output.SentOutput, Model.HiddenStateDim,
                     Model.WMatrix, Model.HistoryLag, (int)Model.Af, Output.SentDeriv);
        }

        public override void Update()
        {
            if(!IsDelegateOptimizer) Model.IMatrixOptimizer.BeforeGradient();
            ComputeLib.MatrixMultiplicationSparseLeftTranspose(Input.SequenceIdx, Input.SentSize, Input.FeaIdx,
                                        Input.FeaValue, Input.ElementSize, Output.SentDeriv,  Model.IMatrixOptimizer.Gradient,
                                        Model.InputFeatureDim, Model.HiddenStateDim,
                                        Model.IMatrixOptimizer.GradientStep);
            if (!IsDelegateOptimizer) Model.IMatrixOptimizer.AfterGradient();

            if (!IsDelegateOptimizer) Model.WMatrixOptimizer.BeforeGradient();
            ComputeLib.RecursiveUpdateBatch(Input.SampleIdx, Input.BatchSize, Input.SentSize, Output.SentOutput, Model.HiddenStateDim,
                                        Output.SentDeriv, Model.HistoryLag, Model.WMatrixOptimizer.Gradient, Model.WMatrixOptimizer.GradientStep);
            if (!IsDelegateOptimizer) Model.WMatrixOptimizer.AfterGradient();

            if (Model.IsBias)
            {
                if (!IsDelegateOptimizer) Model.BiasOptimizer.BeforeGradient();
                ComputeLib.ColumnWiseSum(Output.SentDeriv, Model.BiasOptimizer.Gradient, Output.SentSize, Model.HiddenStateDim, Model.BiasOptimizer.GradientStep);
                if (!IsDelegateOptimizer) Model.BiasOptimizer.AfterGradient();
            }
        }


        #region Dispose Function is not necessary.
        ~RNNSparseRunner()
        {
            this.Dispose(false);
        }

        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }

            base.Dispose(disposing);
        }
        #endregion

    }
}
