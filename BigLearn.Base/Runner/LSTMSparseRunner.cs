﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class FastLSTMSparseRunner<IN> : StructRunner<LSTMCell, IN> where IN : SeqSparseBatchData
    {
        public new SeqDenseRecursiveData Output { get { return (SeqDenseRecursiveData)base.Output; } set { base.Output = value; } }
        public SeqDenseRecursiveData C;

        SeqDenseBatchData GateO;
        SeqDenseBatchData GateI;
        SeqDenseBatchData CHat;
        SeqDenseBatchData GateF;
        SeqDenseBatchData TanhC;

        CudaPieceFloat Lag1O;
        HiddenBatchData InitC = null;
        HiddenBatchData InitO = null;

        HiddenBatchData RemapInitC = null;
        HiddenBatchData RemapInitO = null;

        bool IsReverse { get; set; }
        MLPAttentionV2Runner AttentionRunner = null;

        public FastLSTMSparseRunner(LSTMCell model, SeqSparseBatchData input, bool isReverseOrder, RunnerBehavior behavior) : this(model, input, isReverseOrder, null, null, null, behavior)
        { }

        public FastLSTMSparseRunner(LSTMCell model, SeqSparseBatchData input, bool isReverseOrder, HiddenBatchData initO, HiddenBatchData initC, MLPAttentionV2Runner attentionRunner, RunnerBehavior behavior) : base(model, input, behavior)
        {
            InitC = initC;
            InitO = initO;

            if (InitO != null) RemapInitO = new HiddenBatchData(InitO.MAX_BATCHSIZE, InitO.Dim, Behavior.RunMode, Behavior.Device);
            if (InitC != null) RemapInitC = new HiddenBatchData(InitC.MAX_BATCHSIZE, InitC.Dim, Behavior.RunMode, Behavior.Device);

            IsReverse = isReverseOrder;
            GateO = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
            GateI = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
            CHat = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
            GateF = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
            TanhC = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);

            Lag1O = new CudaPieceFloat(Input.Stat.MAX_SEQUENCESIZE * Model.CellDim, true, Behavior.Device == DeviceType.GPU);

            Output = new SeqDenseRecursiveData(new SequenceDataStat() { FEATURE_DIM = Model.CellDim, MAX_BATCHSIZE = Input.Stat.MAX_BATCHSIZE, MAX_SEQUENCESIZE = Input.Stat.MAX_SEQUENCESIZE },
                            Input.SampleIdx, Input.SentMargin, Behavior.Device);

            C = new SeqDenseRecursiveData(Output.Stat, Input.SampleIdx, Input.SentMargin, Output.RecurrentInfo, Behavior.Device);
            AttentionRunner = attentionRunner;
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            C.BatchSize = Input.BatchSize;
            C.SentSize = Input.SentSize;

            if (!IsReverse) Output.RecurrentInfo.InitTransposeInfo(Input.SampleIdx, Input.BatchSize, Input.SentSize);
            else Output.RecurrentInfo.InitReverseAndTransposeInfo(Input.SampleIdx, Input.BatchSize, Input.SentSize);

            if (InitO != null)
            {
                RemapInitO.BatchSize = InitO.BatchSize;
                ComputeLib.Matrix_AdditionMask(InitO.Output.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               RemapInitO.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               RemapInitO.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               InitO.Dim, InitO.BatchSize, 1, 0, 0);
            }

            if(InitC != null)
            {
                RemapInitC.BatchSize = InitC.BatchSize;
                ComputeLib.Matrix_AdditionMask(InitC.Output.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               RemapInitC.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               RemapInitC.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               InitC.Dim, InitC.BatchSize, 1, 0, 0);
            }

            if (AttentionRunner == null)
            {
                LSTMComputeForwardLib.LSTMForwardv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
            }
            else
            {
                AttentionRunner.Forward();
                LSTMComputeForwardLib.LSTMForwardAttentionv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output);
            }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Stat.FEATURE_DIM);
            ComputeLib.Zero(C.SentDeriv, C.SentSize * C.Stat.FEATURE_DIM);
            
            if (RemapInitO != null) { ComputeLib.Zero(RemapInitO.Deriv.Data, RemapInitO.BatchSize * RemapInitO.Dim); }
            if (RemapInitC != null) { ComputeLib.Zero(RemapInitC.Deriv.Data, RemapInitC.BatchSize * RemapInitC.Dim); }

            if (AttentionRunner != null) { AttentionRunner.CleanDeriv(); }
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (AttentionRunner == null)
            {
                LSTMComputeBackwardDataLib.LSTMBackwardDatav2(ComputeLib, Input, RemapInitO, RemapInitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
            }
            else
            {
                LSTMComputeBackwardDataLib.LSTMAttentionBackwardDatav2(ComputeLib, Input, RemapInitO, RemapInitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output);
                AttentionRunner.Backward(cleanDeriv);
            }

            if (RemapInitO != null)
            {
                ComputeLib.Matrix_AdditionMask(RemapInitO.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                               InitO.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitO.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitO.Dim, InitO.BatchSize, 1, 1, 0);
            }

            if (RemapInitC != null)
            {
                ComputeLib.Matrix_AdditionMask(RemapInitC.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                               InitC.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitC.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitC.Dim, InitC.BatchSize, 1, 1, 0);
            }
        }

        public override void Update()
        {
            if (!IsDelegateOptimizer) { foreach (ModelOptimizer optimizer in Model.ModelOptimizers) optimizer.Optimizer.BeforeGradient(); }

            if (AttentionRunner == null)
            {
                LSTMComputeBackwardWeightLib.LSTMBackwardWeightv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output, Lag1O);
            }
            else
            {
                LSTMComputeBackwardWeightLib.LSTMAttentionBackwardWeightv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output, Lag1O);
                AttentionRunner.Update();
            }
            if (!IsDelegateOptimizer) { foreach (ModelOptimizer optimizer in Model.ModelOptimizers) optimizer.Optimizer.AfterGradient(); }
        }
    }

    //public class LSTMSparseRunner<IN> : StructRunner<LSTMCell, IN> where IN : SeqSparseBatchData
    //{
    //    public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
    //    public SeqDenseBatchData C;
    //    public CudaPieceFloat Lag1O;

    //    SeqDenseBatchData GateO;
    //    SeqDenseBatchData GateI;
    //    SeqDenseBatchData CHat;
    //    SeqDenseBatchData GateF;
    //    SeqDenseBatchData TanhC;

    //    SeqHelpData HelpInput = null;
    //    HiddenBatchData InitC = null;
    //    HiddenBatchData InitO = null;

    //    MLPAttentionRunner AttentionRunner = null;

    //    CudaPieceInt RecursiveMatrix;
    //    CudaPieceInt RecursiveIndex;
    //    int MaxLag;

    //    public LSTMSparseRunner(Structure model, SeqSparseBatchData data, RunnerBehavior behavior)
    //        : base(model, data, behavior)
    //    {
    //        Output = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);

    //        RecursiveIndex = new CudaPieceInt(Input.Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);
    //        RecursiveMatrix = new CudaPieceInt(Input.Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);

    //        GateO = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
    //        GateI = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
    //        CHat = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
    //        GateF = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
    //        C = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
    //        TanhC = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
    //    }


    //    //BatchData input,  input, 
    //    public LSTMSparseRunner(LSTMCell model, SeqSparseBatchData input, SeqHelpData helpInput, RunnerBehavior behavior)
    //        : this(model, input, helpInput, null, null, behavior)
    //    { }

    //    /// <summary>
    //    /// Seq Seq Decoder.
    //    /// </summary>
    //    /// <param name="model"></param>
    //    /// <param name="input"></param>
    //    /// <param name="preC"></param>
    //    /// <param name="preH"></param>
    //    /// <param name="behavior"></param>
    //    public LSTMSparseRunner(LSTMCell model, SeqSparseBatchData input, SeqHelpData helpInput, 
    //        HiddenBatchData initO, HiddenBatchData initC, RunnerBehavior behavior)
    //        : this(model, input, helpInput, null, null, null, behavior)
    //    { }

    //    public LSTMSparseRunner(LSTMCell model, SeqSparseBatchData input, SeqHelpData helpInput, 
    //        HiddenBatchData initO, HiddenBatchData initC,  MLPAttentionRunner attentionRunner, RunnerBehavior behavior) : base(model, input, behavior)
    //    {
    //        HelpInput = helpInput;
    //        InitC = initC;
    //        InitO = initO;

    //        Output = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
    //        GateO = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
    //        GateI = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
    //        CHat = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
    //        GateF = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
    //        C = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);
    //        TanhC = new SeqDenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.CellDim, Behavior.Device);

    //        Lag1O = new CudaPieceFloat(Input.Stat.MAX_SEQUENCESIZE * Model.CellDim, true, Behavior.Device == DeviceType.GPU);
    //        AttentionRunner = attentionRunner; 
    //    }

    //    void Preprocess()
    //    {
    //        Input.SampleIdx.SyncToCPU(Input.BatchSize);
    //        KeyValuePair<int, int>[] seqLen = new KeyValuePair<int, int>[Input.BatchSize];
    //        for (int i = 0; i < Input.BatchSize; i++)
    //        {
    //            seqLen[i] = new KeyValuePair<int, int>(i + 1, i == 0 ?
    //                    Input.SampleIdx.MemPtr[i] : Input.SampleIdx.MemPtr[i] - Input.SampleIdx.MemPtr[i - 1]);
    //        }
    //        MaxLag = seqLen.Select(i => i.Value).Max();

    //        int elementCount = 0;
    //        for (int lag = 0; lag < MaxLag; lag++)
    //        {
    //            int[] avSet = seqLen.Where(i => i.Value > lag).Select(i => i.Key).ToArray();
    //            RecursiveIndex.MemPtr[lag] = lag == 0 ? avSet.Count() : avSet.Count() + RecursiveIndex.MemPtr[lag - 1];
    //            Buffer.BlockCopy(avSet, 0, RecursiveMatrix.MemPtr, elementCount * sizeof(int), avSet.Count() * sizeof(int));
    //            elementCount += avSet.Count();
    //        }
    //        //RecursiveIndex.SyncFromCPU(MaxLag);
    //        RecursiveMatrix.SyncFromCPU(elementCount);
    //    }

    //    unsafe public override void Forward()
    //    {
    //        /***** Before Forward propagation, pull model parameter from parameter server. ********/
    //        Output.BatchSize = Input.BatchSize;
    //        Output.SentSize = Input.SentSize;

    //        Output.SampleIdx = Input.SampleIdx;
    //        Output.SentMargin = Input.SentMargin;

    //        C.BatchSize = Input.BatchSize;
    //        C.SentSize = Input.SentSize;

    //        C.SampleIdx = Input.SampleIdx;
    //        C.SentMargin = Input.SentMargin;

    //        if(HelpInput == null)
    //        {
    //            Preprocess();
    //            LSTMComputeLib.LSTMForward(ComputeLib, Input, RecursiveMatrix, RecursiveIndex, MaxLag, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
    //        }
    //        else if (AttentionRunner == null)
    //        {
    //            LSTMComputeForwardLib.LSTMForward(ComputeLib, Input, HelpInput, InitO, InitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
    //        }
    //        else
    //        {
    //            AttentionRunner.Forward();

    //            LSTMComputeForwardLib.LSTMForwardAttention(ComputeLib, 
    //                Input, HelpInput,  InitO, InitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output);
    //        }
    //    }

    //    public override void CleanDeriv()
    //    {
    //        ComputeLib.Zero(Output.SentDeriv, Output.SentDeriv.Size);
    //        ComputeLib.Zero(C.SentDeriv, C.SentDeriv.Size);

    //        if(AttentionRunner != null) AttentionRunner.CleanDeriv();
    //    }

    //    /// <summary>
    //    /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
    //    /// </summary>
    //    /// <param name="cleanDeriv"></param>
    //    unsafe public override void Backward(bool cleanDeriv)
    //    {
    //        if(HelpInput == null)
    //        {
    //            LSTMComputeLib.RecurrentBackwardData(ComputeLib, Input.SampleIdx, Input.BatchSize, Input.SentSize,
    //                    RecursiveMatrix, RecursiveIndex, MaxLag, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
    //        }
    //        else if(AttentionRunner == null)
    //        {
    //            LSTMComputeBackwardDataLib.RecurrentBackwardData(ComputeLib,
    //                Input.SampleIdx, Input.BatchSize, Input.SentSize, InitO, InitC, HelpInput, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
    //        }
    //        else
    //        {
    //            LSTMComputeBackwardDataLib.RecurrentAttentionBackwardData(ComputeLib,
    //                Input.SampleIdx, Input.BatchSize, Input.SentSize, InitO, InitC, HelpInput, Model,
    //                AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output);

    //            AttentionRunner.Backward(cleanDeriv);
    //        }
    //    }

    //    /// <summary>
    //    /// In this Update funciton,
    //    /// Instead of Updating Model Parameter directly, push the model gradient into parameter server.
    //    /// </summary>
    //    public override void Update()
    //    {
    //        if (!IsDelegateOptimizer) { Model.BeforeGradient(); }
    //        if(HelpInput == null)
    //        {
    //            LSTMComputeLib.LSTMBackwardWeight(ComputeLib, Input, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
    //        }
    //        else if (AttentionRunner == null)
    //        { 
    //            LSTMComputeBackwardWeightLib.LSTMBackwardWeight(ComputeLib, Input, HelpInput, InitO, InitC, Model,
    //                GateO, GateI, CHat, GateF, C, TanhC, Output, Lag1O);
    //        }
    //        else
    //        {
    //            AttentionRunner.IsDelegateOptimizer = IsDelegateOptimizer;
    //            LSTMComputeBackwardWeightLib.LSTMAttentionBackwardWeight(ComputeLib, Input, HelpInput, InitO, InitC, Model,
    //                AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output, Lag1O);
    //            AttentionRunner.Update();
    //        }
    //        if (!IsDelegateOptimizer) { Model.AfterGradient(); }
    //    }

    //    #region Dispose Function.
    //    ~LSTMSparseRunner()
    //    {
    //        this.Dispose(false);
    //    }

    //    private bool disposed = false;
    //    protected override void Dispose(bool disposing)
    //    {
    //        if (disposed)
    //        {
    //            return;
    //        }

    //        disposed = true;
    //        if (disposing)
    //        {
    //            if (Output != null) Output.Dispose(); Output = null;
    //            if (GateO != null) GateO.Dispose(); GateO = null;
    //            if (GateI != null) GateI.Dispose(); GateI = null;
    //            if (CHat != null) CHat.Dispose(); CHat = null;
    //            if (GateF != null) GateF.Dispose(); GateF = null;
    //            if (C != null) C.Dispose(); C = null;
    //            if (TanhC != null) TanhC.Dispose(); TanhC = null;
    //        }

    //        base.Dispose(disposing);
    //    }
    //    #endregion.

    //}
}
