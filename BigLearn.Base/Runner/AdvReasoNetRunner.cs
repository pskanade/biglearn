﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class AdvReasoNetRunner : StructRunner
    {
        public HiddenBatchData[] StatusData = null;
        public HiddenBatchData[] AnsProb = null;

        /// <summary>
        /// Input Query;
        /// </summary>
        HiddenBatchData InitStatus { get; set; }

        //BiMatchBatchData GMemMatch { get; set; }
        MLPAttentionStructure GAttStruct { get; set; }

        GRUCell GruCell = null;
        List<LayerStructure> TermStruct = null;
        int MaxIterationNum { get; set; }

        List<StructRunner> SubRunners = new List<StructRunner>();
        HiddenBatchData[] TermData = null;

        float RewardDiscount = 1;
        public float MaxClipTerm = 1 - Util.GPUEpsilon;
        public float MinClipTerm = Util.GPUEpsilon;

        Att_Type Atttype { get; set; }

        List<int> StepStat = new List<int>();
        List<int> StocStepStat = new List<int>();

        bool IsSkipInit = false;
        public override void Init()
        {
            StepStat = new List<int>();
            for (int i = 0; i < MaxIterationNum; i++) { StepStat.Add(0); }

            StocStepStat = new List<int>();
            for (int i = 0; i < MaxIterationNum; i++) { StocStepStat.Add(0); }
        }

        public override void Complete()
        {
            int totalNum = 0;
            for (int i = 0; i < MaxIterationNum; i++) totalNum += StepStat[i];
            for (int i = 0; i < MaxIterationNum; i++)
            {
                Logger.WriteLog("Maximum Step {0} Stat {1}, Percentage {2}", i, StepStat[i], StepStat[i] * 1.0f / totalNum);
            }

            for (int i = 0; i < MaxIterationNum; i++)
            {
                Logger.WriteLog("Stocas Step {0} Stat {1}, Percentage {2}", i, StocStepStat[i], StocStepStat[i] * 1.0f / totalNum);
            }
        }

        /// <summary>
        /// maximum iteration number.
        /// </summary>
        public AdvReasoNetRunner(HiddenBatchData initStatus, int maxIterateNum,
            SeqDenseBatchData mem, BiMatchBatchData matchData, MLPAttentionStructure gAttStruct, float gGamma, float rewardDiscount,
            GRUCell gruCell, List<LayerStructure> termStruct, RunnerBehavior behavior, Att_Type type = Att_Type.W_INNER_PRODUCT, bool isSkipInit = false) : base(Structure.Empty, null, behavior)
        {
            InitStatus = initStatus;

            //GMemMatch = gMemMatch;
            GAttStruct = gAttStruct;

            GruCell = gruCell;
            TermStruct = termStruct;

            Atttype = type;

            MaxIterationNum = maxIterateNum;
            RewardDiscount = rewardDiscount;

            IsSkipInit = isSkipInit;

            // Memory Matrix;
            SeqMatrixData MemoryMatrix = new SeqMatrixData(mem);

            // InitState is between -1 to 1;
            HiddenBatchData initState = InitStatus;

            // Memory Address Project.
            MatrixData MemoryAddressProject = new MatrixData(gAttStruct.HiddenDim, gAttStruct.MemoryDim, gAttStruct.Wm, gAttStruct.WmGrad, gAttStruct.DeviceType);

            // Memory Address.
            MatrixMultiplicationRunner MemoryAddressRunner = new MatrixMultiplicationRunner(MemoryMatrix, MemoryAddressProject, Behavior);
            SubRunners.Add(MemoryAddressRunner);
            MatrixData MemoryAddress = MemoryAddressRunner.Output;

            // State Address Project.
            MatrixData StateAddressProject = new MatrixData(gAttStruct.HiddenDim, gAttStruct.InputDim, gAttStruct.Wi, gAttStruct.WiGrad, gAttStruct.DeviceType);

            // Address Score Project.
            VectorData AddressScoreProject = new VectorData(gAttStruct.HiddenDim, gAttStruct.Wa, gAttStruct.WaGrad, gAttStruct.DeviceType);

            // For Forward/Backward Propagation.
            {
                StatusData = new HiddenBatchData[MaxIterationNum];
                TermData = new HiddenBatchData[MaxIterationNum];
            }
            HiddenBatchData terminateOutput = null;

            if (!IsSkipInit)
            {
                StatusData[0] = InitStatus;
                terminateOutput = StatusData[0];
                for (int l = 0; l < TermStruct.Count; l++)
                {
                    FullyConnectHiddenRunner<HiddenBatchData> tlRunner = new FullyConnectHiddenRunner<HiddenBatchData>(TermStruct[l], terminateOutput, Behavior);
                    SubRunners.Add(tlRunner);
                    terminateOutput = tlRunner.Output;
                }
                TermData[0] = terminateOutput;
            }

            for (int i = IsSkipInit ? 0 : 1; i < MaxIterationNum; i++)
            {
                MatrixMultiplicationRunner StateAddressRunner = new MatrixMultiplicationRunner(new MatrixData(initState), StateAddressProject, Behavior);
                SubRunners.Add(StateAddressRunner);
                MatrixData stateAddress = StateAddressRunner.Output;

                VectorData attScores = null;
                switch (Atttype)
                {
                    case Att_Type.W_INNER_PRODUCT:
                        VecAlignmentRunner scoreRunner = new VecAlignmentRunner(stateAddress, MemoryAddress, matchData, AddressScoreProject, Behavior, 1, A_Func.Tanh);
                        SubRunners.Add(scoreRunner);
                        attScores = new VectorData(matchData.Stat.MAX_MATCH_BATCHSIZE, scoreRunner.Output.Output.Data, scoreRunner.Output.Deriv.Data, Behavior.Device);
                        break;
                    case Att_Type.W_ADD:
                        VecAlignmentRunner addScoreRunner = new VecAlignmentRunner(stateAddress, MemoryAddress, matchData, AddressScoreProject, Behavior, 0, A_Func.Tanh);
                        SubRunners.Add(addScoreRunner);
                        attScores = new VectorData(matchData.Stat.MAX_MATCH_BATCHSIZE, addScoreRunner.Output.Output.Data, addScoreRunner.Output.Deriv.Data, Behavior.Device);
                        break;
                    case Att_Type.INNER_PRODUCT:
                        MatrixInnerProductRunner mRunner = new MatrixInnerProductRunner(stateAddress, MemoryAddress, matchData, Behavior);
                        SubRunners.Add(mRunner);
                        attScores = mRunner.Output;
                        break;
                    case Att_Type.COSINE_PRODUCT:
                        SimilarityRunner cosRunner = new SimilarityRunner(new HiddenBatchData(stateAddress), new HiddenBatchData(MemoryAddress), matchData, SimilarityType.CosineSimilarity, Behavior);
                        SubRunners.Add(cosRunner);
                        attScores = new VectorData(matchData.Stat.MAX_MATCH_BATCHSIZE, cosRunner.Output.Output.Data, cosRunner.Output.Deriv.Data, Behavior.Device);
                        break;
                }

                // Attention Softmax.
                SeqVectorData seqAttScores = new SeqVectorData(mem.MAX_SENTSIZE, mem.MAX_BATCHSIZE, attScores.Output, attScores.Deriv, mem.SampleIdx, mem.SentMargin, Behavior.Device);
                SeqVecSoftmaxRunner attSoftmaxRunner = new SeqVecSoftmaxRunner(seqAttScores, gGamma, Behavior);
                SubRunners.Add(attSoftmaxRunner);
                SeqVectorData attSoftScore = attSoftmaxRunner.Output;

                // Attention Retrieve.
                SeqVecMultiplicationRunner retrieveRunner = new SeqVecMultiplicationRunner(attSoftScore, MemoryMatrix, Behavior);
                SubRunners.Add(retrieveRunner);
                MatrixData inputMatrix = retrieveRunner.Output;

                // RNN State.

                if (IsSkipInit && i == 0)
                {
                    StatusData[i] = new HiddenBatchData(inputMatrix);
                }
                else
                {
                    GRUQueryRunner stateRunner = new GRUQueryRunner(GruCell, initState, new HiddenBatchData(inputMatrix), Behavior);
                    SubRunners.Add(stateRunner);
                    StatusData[i] = stateRunner.Output;
                }

                HiddenBatchData newState = StatusData[i];
                terminateOutput = StatusData[i];
                for (int l = 0; l < TermStruct.Count; l++)
                {
                    FullyConnectHiddenRunner<HiddenBatchData> tlRunner = new FullyConnectHiddenRunner<HiddenBatchData>(TermStruct[l], terminateOutput, Behavior);
                    SubRunners.Add(tlRunner);
                    terminateOutput = tlRunner.Output;
                }
                TermData[i] = terminateOutput;

                // State Switch.
                initState = newState;
            }
            {
                AnsProb = new HiddenBatchData[MaxIterationNum];
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    AnsProb[i] = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                }
            }
        }

        public override void Forward()
        {
            foreach (StructRunner runner in SubRunners) { runner.Forward(); }

            for (int i = 0; i < MaxIterationNum; i++)
            {
                ComputeLib.Logistic(TermData[i].Output.Data, 0, TermData[i].Output.Data, 0, TermData[i].Output.BatchSize, 1);
                ComputeLib.ClipVector(TermData[i].Output.Data, TermData[i].BatchSize, MaxClipTerm, MinClipTerm); // 1 - Util.GPUEpsilon, Util.GPUEpsilon);
                TermData[i].Output.Data.SyncToCPU(TermData[i].BatchSize);
            }

            for (int b = 0; b < InitStatus.BatchSize; b++)
            {
                int max_iter = 0;
                double max_p = 0;
                float acc_log_t = 0;

                float randomP = (float)ParameterSetting.Random.NextDouble();
                int stoc_iter = MaxIterationNum - 1;

                for (int i = 0; i < MaxIterationNum; i++)
                {
                    float t_i = TermData[i].Output.Data.MemPtr[b];
                    if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                    float p = acc_log_t + (float)Math.Log(t_i);
                    if (i == MaxIterationNum - 1) p = acc_log_t;

                    float prob = (float)Math.Exp(p);
                    AnsProb[i].Output.Data.MemPtr[b] = prob;
                    acc_log_t += (float)Math.Log(1 - t_i);
                    if (prob > max_p)
                    {
                        max_p = prob;
                        max_iter = i;
                    }

                    if(randomP > 0 && randomP <= prob) { stoc_iter = i; }
                    randomP = randomP - prob;
                }

                StepStat[max_iter] += 1;
                StocStepStat[stoc_iter] += 1;
            }

            for (int i = 0; i < MaxIterationNum; i++)
            {
                AnsProb[i].BatchSize = InitStatus.BatchSize;
                AnsProb[i].Output.Data.SyncFromCPU(AnsProb[i].BatchSize);
            }
        }

        public override void CleanDeriv()
        {
            foreach (StructRunner runner in SubRunners) { runner.CleanDeriv(); }
            for (int i = 0; i < MaxIterationNum; i++)
            {
                ComputeLib.Zero(AnsProb[i].Deriv.Data, AnsProb[i].BatchSize);
            }
        }

        /// <summary>
        /// Stage 1 : Iterative Attention Model (Supervised Training).
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            for (int i = 0; i < MaxIterationNum; i++)
            {
                Array.Clear(TermData[i].Deriv.Data.MemPtr, 0, TermData[i].BatchSize);
            }

            for (int b = 0; b < InitStatus.BatchSize; b++)
            {
                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    float reward = AnsProb[i].Deriv.Data.MemPtr[b];
                    TermData[i].Deriv.Data.MemPtr[b] += reward * (1 - TermData[i].Output.Data.MemPtr[b]);
                    for (int hp = 0; hp < i; hp++)
                    {
                        TermData[hp].Deriv.Data.MemPtr[b] += -(float)Math.Pow(RewardDiscount, i - hp) * reward * TermData[hp].Output.Data.MemPtr[b];
                    }

                    if (TermData[i].Deriv.Data.MemPtr[b] > Math.Abs(1))
                    {
                        Console.WriteLine("Start TerminalRunner Deriv is too large {0}, step {1}", TermData[i].Deriv.Data.MemPtr[b], i);
                    }
                }
            }
            
            for (int i = 0; i < MaxIterationNum; i++)
            {
                for (int b = 0; b < InitStatus.BatchSize; b++)
                {
                    if (TermData[i].Output.Data.MemPtr[b] >= MaxClipTerm - Util.GPUEpsilon) TermData[i].Deriv.Data.MemPtr[b] = 0;
                    if (TermData[i].Output.Data.MemPtr[b] <= MinClipTerm + Util.GPUEpsilon) TermData[i].Deriv.Data.MemPtr[b] = 0;
                }
                TermData[i].Deriv.Data.SyncFromCPU(TermData[i].BatchSize);
            }

            foreach (StructRunner runner in Enumerable.Reverse(SubRunners)) { runner.Backward(false); }
        }


        public override void Update()
        {
            foreach (StructRunner runner in SubRunners) { runner.IsDelegateOptimizer = IsDelegateOptimizer; runner.Update(); }
        }
    }

}
