using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class NdArrayTransposeRunner : StructRunner
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }

        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }

        CudaPieceInt InDim;
        CudaPieceInt TransDim;
        int[] Trans;
        int[] RTrans;
        int TotalLength = 1;
        public NdArrayTransposeRunner(NdArrayData data, int[] transpose, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = data;
            Trans = transpose;
            RTrans = new int[Trans.Length];
            for (int i = 0; i < Trans.Length; i++)
            {
                RTrans[Trans[i]] = i;
            }

            InDim = new CudaPieceInt(Input.Dimensions.Length, behavior.Device);
            TransDim = new CudaPieceInt(Input.Dimensions.Length, behavior.Device);

            List<IntArgument> dimension = new List<IntArgument>();
            foreach (int p in Trans) { dimension.Add(data.Dimensions[p]); }
            Output = new NdArrayData(dimension.ToArray(), Input.Output, Input.Deriv, Behavior.Device);
        }

        /// <summary>
        /// output TotalLength.
        /// </summary>
        /// <param name="dims"></param>
        /// <param name="trans"></param>
        static int ProcessTranspose(IntArgument[] dims, int[] trans, CudaPieceInt inDim, CudaPieceInt transDim)
        {
            int length = 1;
            for (int i = 0; i < dims.Length; i++)
            {
                inDim.MemPtr[i] = dims[i].Value;
                length *= inDim.MemPtr[i];
            }

            int d = 1;
            for (int i = dims.Length - 1; i >= 0; i--)
            {
                transDim.MemPtr[i] = d;
                int newi = trans[i];
                d = d * inDim.MemPtr[newi];
            }
            inDim.SyncFromCPU();
            transDim.SyncFromCPU();

            return length;
        }
        public override void Forward()
        {
            TotalLength = ProcessTranspose(Input.Dimensions, Trans, InDim, TransDim);
            ComputeLib.NDArrayTranspose(Input.Output, InDim, TransDim, Output.Output, Input.Dimensions.Length, TotalLength, 0, 1);
        }

        public override void Backward(bool cleanDeriv)
        {
            TotalLength = ProcessTranspose(Output.Dimensions, RTrans, InDim, TransDim);
            ComputeLib.NDArrayTranspose(Output.Deriv, InDim, TransDim, Input.Deriv, Output.Dimensions.Length, TotalLength, 1, 1);
        }
        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, TotalLength);
        }
    }
}
