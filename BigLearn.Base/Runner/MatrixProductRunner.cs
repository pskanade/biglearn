﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MatrixProductRunner : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        MatrixData InputA { get; set; }
        MatrixData InputB { get; set; }

        bool IsProcessor = false;
        float Alpha = 0;
        /// <summary>
        /// O = A * B';
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public MatrixProductRunner(MatrixData inputA, MatrixData inputB, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            Output = new MatrixData(InputB.MaxRow, InputA.MaxRow, Behavior.Device);
        }

        /// <summary>
        /// o = A * B'
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="output"></param>
        /// <param name="overwriter"></param>
        /// <param name="behavior"></param>
        public MatrixProductRunner(MatrixData inputA, MatrixData inputB, MatrixData output, bool overwriter, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            Output = output;
            IsProcessor = true;
            Alpha = overwriter ? 0 : 1;
        }

        public override void Forward()
        {
            iteration++;
            Output.Row = InputA.Row;
            Output.Column = InputB.Row;
            if (InputA.Column != InputB.Column) { throw new Exception(string.Format("Matrix A Dim1 {0} should be equals to Matrix B Dim1 {1}", InputA.Column, InputB.Column)); }
            if (InputA.Row < 0) { throw new Exception(string.Format("Matrix A Row {0} should be larger than zero", InputA.Row)); }
            if (InputA.Column < 0) { throw new Exception(string.Format("Matrix A Column {0} should be larger than zero", InputA.Column)); }
            if (InputB.Row < 0) { throw new Exception(string.Format("Matrix B Row {0} should be larger than zero", InputB.Row)); }
            if (InputB.Column < 0) { throw new Exception(string.Format("Matrix B Column {0} should be larger than zero", InputB.Column)); }

            ComputeLib.Sgemm(InputA.Output, 0, InputB.Output, 0, Output.Output, 0, InputA.Row, InputA.Column, InputB.Row, IsProcessor ? Alpha : 0, 1, false, true);
        }

        public override void CleanDeriv()
        {
            if (!IsProcessor && Output.Deriv != null && !Output.Deriv.IsEmpty) ComputeLib.Zero(Output.Deriv, Output.Column * Output.Row);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (InputA.Deriv != null && !InputA.Deriv.IsEmpty)
            {
                ComputeLib.Sgemm(Output.Deriv, 0, InputB.Output, 0, InputA.Deriv, 0, Output.Row, Output.Column, InputB.Column, 1, 1, false, false);
            }
            if (InputB.Deriv != null && !InputB.Deriv.IsEmpty)
            {
                ComputeLib.Sgemm(Output.Deriv, 0, InputA.Output, 0, InputB.Deriv, 0, Output.Row, Output.Column, InputB.Column, 1, 1, true, false);
            }
        }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }

}
