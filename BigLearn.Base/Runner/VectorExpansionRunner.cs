﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class VectorExpansionRunner : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        public new VectorData Input { get { return (VectorData)base.Input; } set { base.Input = value; } }

        public CudaPieceFloat BiasOne;

        IntArgument ExpanNum { get; set; }
        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public VectorExpansionRunner(VectorData input, int maxRow, IntArgument expanNum, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            ExpanNum = expanNum;
            //BiasOne = new CudaPieceFloat(maxRow, Behavior.Device);
            //BiasOne.Init(1);
            Output = new MatrixData(Input.MaxLength, maxRow, Behavior.Device);
        }

        public override void Forward()
        {
            Output.Column = Input.Length;
            Output.Row = ExpanNum.Value;

            if (Output.Row <= 0) { throw new Exception(string.Format("expansion row number {0} should be larger than zero", Output.Row)); }
            if(Output.Column <= 0) { throw new Exception(string.Format("output col number {0} should be larger than zero", Output.Column)); }

            ComputeLib.Matrix_AdditionEx(Input.Output, 0, 0, Input.Output, 0, 0, Output.Output, 0, Output.Column, Output.Column, Output.Row, 1, 0, 0);
            //ComputeLib.Sgemm(BiasOne, 0, Input.Output, 0, Output.Output, 0, Output.Row, 1, Output.Column, 0, 1, false, false);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Column * Output.Row);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.ColumnWiseSum(Output.Deriv, Input.Deriv, Output.Row, Output.Column, 1);
            //ComputeLib.Sgemv(Output.Deriv, BiasOne, Output.Row, Output.Column, Input.Deriv, true, 1, 1);
        }

        public override void Update() { }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }

    public class VectorExpansionProcessor : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        public VectorData InputBias;
        public CudaPieceFloat BiasOne;

        /// <summary>
        /// O = O + bias;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public VectorExpansionProcessor(VectorData inputBias, MatrixData outputMatrix, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputBias = inputBias;
            Output = outputMatrix;
            BiasOne = new CudaPieceFloat(Output.MaxRow, true, Behavior.Device == DeviceType.GPU);
            BiasOne.Init(1);
        }

        public override void Forward()
        {
            ComputeLib.Sgemm(BiasOne, 0, InputBias.Output, 0, Output.Output, 0, Output.Row, 1, Output.Column, 1, 1, false, false);
        }

        public override void CleanDeriv()
        { }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Sgemv(Output.Deriv, BiasOne, Output.Row, Output.Column, InputBias.Deriv, true, 1, 1);
        }

        public override void Update() { }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }
}
