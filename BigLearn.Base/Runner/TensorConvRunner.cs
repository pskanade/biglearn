using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorConvRunner : StructRunner<TensorConvStructure, Tensor4DData>
    {
        public new Tensor4DData Output { get { return (Tensor4DData)base.Output; } set { base.Output = value; } }
        public new Tensor4DData Input { get { return (Tensor4DData)base.Input; } set { base.Input = value; } }

        public int Pad { get; set; }
        public int Stride { get; set; }
        public A_Func Af { get; set; }

        public TensorConvRunner(TensorConvStructure model, Tensor4DData input, int pad, int stride, A_Func af, RunnerBehavior behavior) : base(model, input, behavior)
        {
            Pad = pad;
            Stride = stride;
            Af = af;

            int outputW = TensorConvUtil.FilterOutput(Input.Width, Model.FilterWidth, Pad, Stride);
            int outputH = TensorConvUtil.FilterOutput(Input.Height, Model.FilterHeight, Pad, Stride);

            Output = new Tensor4DData(outputW, outputH, Model.OutDepth, Input.MaxBatchSize, Behavior.Device);
        }


        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;

            ComputeLib.CNNForwardPropagate(Input.Output, Input.BatchSize, Input.Depth, Input.Height, Input.Width,
                Model.Filter, Model.OutDepth, Model.FilterHeight, Model.FilterWidth, Pad, Stride,
                Output.Output, Output.Height, Output.Width);

            ActivationFuncLib.Image_ActiveOutput(Output.Output, Model.Bias, Af, Output.BatchSize, Output.Width, Output.Height, Output.Depth);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            // Backpropagate Deriv.
            ActivationFuncLib.Image_DeactiveOutput(Output.Output, Output.Deriv, Af, Output.BatchSize, Output.Width, Output.Height, Output.Depth);
            
            if(Input.Deriv != null)
            {
                ComputeLib.CuDNNBackwardPropagateData(Input.Deriv, Output.BatchSize, Input.Depth, Input.Height, Input.Width,
                        Model.Filter, Output.Depth, Model.FilterHeight, Model.FilterWidth, Pad, Stride,
                        Output.Deriv, Output.Height, Output.Width);
            }

            ComputeLib.CuDNNBackwardPropagateFilter(Input.Output, Input.BatchSize, Input.Depth, Input.Height, Input.Width,
                    Model.FilterGrad, Output.Depth, Model.FilterHeight, Model.FilterWidth, Pad, Stride,
                    Output.Deriv, Output.Height, Output.Width, 1, 1);

            ComputeLib.CuDNNBackwardPropagateBias(Output.Deriv, Model.BiasGrad, Output.BatchSize,
                 Output.Width, Output.Height, Output.Depth, 1, 1);

        }

        #region Dispose function.
        ~TensorConvRunner()
        {
            this.Dispose(false);
        }

        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }

            base.Dispose(disposing);
        }
        #endregion.


    }
}
