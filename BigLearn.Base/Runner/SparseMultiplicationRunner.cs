﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SparseMultiplicationRunner : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        SparseMatrixData InputA { get; set; }
        MatrixData InputB { get; set; }

        CudaPieceInt ReMap { get; set; }

        CudaPieceInt MaskA;
        CudaPieceInt MaskB;
        CudaPieceFloat Tmp;
        public SparseMultiplicationRunner(SparseMatrixData inputA, MatrixData inputB, RunnerBehavior behavior) : 
            this(inputA, inputB, CudaPieceInt.Empty, behavior)
        {
        }

        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public SparseMultiplicationRunner(SparseMatrixData inputA, MatrixData inputB, CudaPieceInt reMap, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            ReMap = reMap;
            Output = new MatrixData(InputB.Column, InputA.MaxRow, Behavior.Device);

            if(InputA.Deriv != null)
            {
                MaskA = new CudaPieceInt(InputA.MaxLength, Behavior.Device);
                MaskB = new CudaPieceInt(InputA.MaxLength, Behavior.Device);
                Tmp = new CudaPieceFloat(InputA.MaxLength, Behavior.Device);
            }
        }

        public override void Forward()
        {
            Output.Row = InputA.Row;
            if (InputA.Column != InputB.Row) { throw new Exception(string.Format("Matrix A Dim1 {0} should be equals to Matrix B Dim2 {1}", InputA.Column, InputB.Row)); }

            if(Output.Row == 0) { return; }

            ComputeLib.SparseSgemmMask(InputA.SampleIdx, InputA.FeatureIdx, InputA.FeatureValue,
                                        InputB.Output, 0, 
                                        Output.Output, 0, 
                                        InputA.Row, InputA.Column, InputB.Column,
                                        CudaPieceInt.Empty, 0,
                                        CudaPieceInt.Empty, 0,
                                        ReMap, 0,
                                        0, 1, false, false);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Column * Output.Row);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Row == 0) { return; }

            ComputeLib.SparseSgemmMask(InputA.SampleIdx, InputA.FeatureIdx, InputA.FeatureValue,
                                       Output.Deriv, 0,
                                       InputB.Deriv, 0,
                                       InputA.Row, InputA.Column, InputB.Column,
                                       CudaPieceInt.Empty, 0, ReMap, 0, CudaPieceInt.Empty, 0, 
                                       1, 1, true, false);

            if(InputA.Deriv != null && !InputA.Deriv.IsEmpty)
            {
                InputA.SampleIdx.SyncToCPU();
                InputA.FeatureIdx.SyncToCPU();
                if(ReMap != null && !ReMap.IsEmpty) ReMap.SyncToCPU();

                int m = 0;
                for (int i = 0; i < InputA.Row; i++)
                {
                    int feaStart = i == 0 ? 0 : InputA.SampleIdx.MemPtr[i - 1];
                    int feaEnd = InputA.SampleIdx.MemPtr[i];
                    int iid = (ReMap == null || ReMap.IsEmpty ) ? i : ReMap.MemPtr[i];
                    for (int f = feaStart; f < feaEnd; f++)
                    {
                        int fid = InputA.FeatureIdx.MemPtr[f];
                        MaskA.MemPtr[m] = iid;
                        MaskB.MemPtr[m] = fid;
                        m += 1;
                    }
                }
                MaskA.SyncFromCPU();
                MaskB.SyncFromCPU();
                ComputeLib.Inner_Product_Matching(Output.Deriv, 0, InputB.Output, 0, Tmp, 0, MaskA, MaskB, Output.Row, InputB.Row, InputA.Length, Output.Column, 0.000000001f);
                ComputeLib.Add_Vector(InputA.Deriv, Tmp, InputA.Length, 1, 1);
            }
        }

        public override void Update() { }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }

    

}
