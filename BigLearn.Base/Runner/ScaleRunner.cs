using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ScaleRunner<T> : StructRunner<Structure, T> where T : VectorData
    {
        float Scale { get; set; }
        public new T Input { get { return (T)base.Input; } set { base.Input = value; } }
        public new T Output { get { return (T)base.Output; } set { base.Output = value; } }
        public ScaleRunner(VectorData input, float scale, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = (T)input;
            Output = (T)(new VectorData(Input.MaxLength, behavior.Device));
            Scale = scale;
        }

        public ScaleRunner(MatrixData input, float scale, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = (T)(VectorData)input;
            Output = (T)(VectorData)(new MatrixData(input.Column, input.MaxRow, behavior.Device));
            Scale = scale;
        }
        
        public override void Forward()
        {
            Output.Length = Input.Length;
            ComputeLib.Scale_Vector(Input.Output, 0, Output.Output, 0, Input.Length, Scale, 0);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Output, Output.Length);
        }

        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Scale_Vector(Output.Deriv, 0, Input.Deriv, 0, Input.Length, Scale, 1);
        }
    }
}
