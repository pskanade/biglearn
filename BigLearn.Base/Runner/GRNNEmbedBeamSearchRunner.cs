﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class GRNNEmbedDecodingRunner : BasicDecodingRunner
    {
        List<HiddenBatchData> Initstatus { get; set; }

        CudaPieceInt SmpIndex;
        CudaPieceInt WordIndex;
        CudaPieceInt LagInfo;
        int[] BatchIdx;

        CudaPieceFloat IEmd;

        CudaPieceFloat GateZ;
        CudaPieceFloat GateR;
        CudaPieceFloat rS;
        CudaPieceFloat sP;

        List<BasicMLPAttentionRunner> AttentionLibs = null;

        List<CudaPieceFloat> BufferStatus;
        List<CudaPieceFloat> BufferStatus2;
        CudaPieceFloat tmpO;

        public new GRUStructure Model;

        LayerStructure Vo { get; set; }
        LayerStructure Co { get; set; }
        LayerStructure Uo { get; set; }

        CudaPieceFloat VoOutput { get; set; }
        CudaPieceFloat CoOutput { get; set; }
        CudaPieceFloat UoOutput { get; set; }
        CudaPieceFloat CombineOutput { get; set; }
        CudaPieceInt MaxoutIndex { get; set; }
        CudaPieceFloat MaxoutOutput { get; set; }


        public EmbedStructure OutputEmbed { get; set; }
        public EmbedStructure InputEmbed { get; set; }

        public override int BatchSize { get { return Initstatus[0].BatchSize; } }
        public override CudaPieceFloat WordProb { get; set; }


        List<CudaPieceFloat> input { get; set; }
        List<CudaPieceFloat> output { get; set; }
        List<CudaPieceFloat> another { get; set; }

        public GRNNEmbedDecodingRunner(GRUStructure model, EmbedStructure inputEmbed, EmbedStructure outputEmbed, 
            List<HiddenBatchData> initStatus, List<BasicMLPAttentionRunner> attentionLibs,
            LayerStructure vo, LayerStructure co, LayerStructure uo,
            RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Model = model;

            OutputEmbed = outputEmbed;
            InputEmbed = inputEmbed;
            Initstatus = initStatus;

            /// put attention here.
            AttentionLibs = attentionLibs;

            Vo = vo;
            Co = co;
            Uo = uo;
        }

        public override void InitMemory(int beamSize)
        {
            int maxBatchSize = Initstatus[0].MAX_BATCHSIZE;

            BufferStatus = new List<CudaPieceFloat>();
            BufferStatus2 = new List<CudaPieceFloat>();

            int maxDim = 0;
            for (int l = 0; l < Initstatus.Count; l++)
            {
                int dim = Initstatus[l].Dim;

                BufferStatus.Add(new CudaPieceFloat(beamSize * beamSize * maxBatchSize * dim, true, Behavior.Device == DeviceType.GPU));
                BufferStatus2.Add(new CudaPieceFloat(beamSize * beamSize * maxBatchSize * dim, true, Behavior.Device == DeviceType.GPU));
                if (maxDim < dim) maxDim = dim;
            }

            tmpO = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * maxDim, true, Behavior.Device == DeviceType.GPU);

            GateZ = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * maxDim, true, Behavior.Device == DeviceType.GPU);
            GateR = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * maxDim, true, Behavior.Device == DeviceType.GPU);
            rS = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * maxDim, true, Behavior.Device == DeviceType.GPU);
            sP = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * maxDim, true, Behavior.Device == DeviceType.GPU);

            IEmd = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * InputEmbed.Dim, true, DeviceType.GPU == Behavior.Device);

            CombineOutput = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * Vo.Neural_Out, true, Behavior.Device == DeviceType.GPU);
            MaxoutOutput = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * Vo.Neural_Out / 2, true, Behavior.Device == DeviceType.GPU);
            MaxoutIndex = new CudaPieceInt(beamSize * beamSize * maxBatchSize * Vo.Neural_Out / 2, true, Behavior.Device == DeviceType.GPU);
            LagInfo = new CudaPieceInt(beamSize * beamSize * maxBatchSize, true, Behavior.Device == DeviceType.GPU);
            WordIndex = new CudaPieceInt(beamSize * beamSize * maxBatchSize, true, Behavior.Device == DeviceType.GPU);
            SmpIndex = new CudaPieceInt(beamSize * beamSize * maxBatchSize, true, Behavior.Device == DeviceType.GPU);
            for (int i = 0; i < SmpIndex.Size; i++) SmpIndex.MemPtr[i] = i + 1; SmpIndex.SyncFromCPU();
            BatchIdx = new int[beamSize * beamSize * maxBatchSize];

            WordProb = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * OutputEmbed.VocabSize, true, Behavior.Device == DeviceType.GPU);
        }


        public override void InitStatus()
        {
            if (AttentionLibs != null)
            {
                for (int i = 0; i < AttentionLibs.Count; i++)
                {
                    if (AttentionLibs[i] != null)
                        AttentionLibs[i].Forward();
                }
            }

            input = Initstatus.Select(i => i.Output.Data).ToList();
            output = BufferStatus;
            another = BufferStatus2;
        }

        public override void TakeStep(List<BeamAction> action, int searchStep)
        {
            StatusUpdate(input, action, output, true);

            //** Calculate Word Probability **//
            ComputeLib.Sgemm(IEmd, 0, Vo.weight, 0, CombineOutput, 0, action.Count, InputEmbed.Dim, Vo.Neural_Out, 0, 1, false, false);
            ComputeLib.Sgemm(AttentionLibs.Last().Z.Output.Data, 0, Co.weight, 0, CombineOutput, 0, action.Count, AttentionLibs.Last().Z.Dim, Co.Neural_Out, 1, 1, false, false);
            ComputeLib.Sgemm(tmpO, 0, Uo.weight, 0, CombineOutput, 0, action.Count, Uo.Neural_In, Uo.Neural_Out, 1, 1, false, false);

            ComputeLib.MaxoutPooling(CombineOutput, action.Count, Uo.Neural_Out, MaxoutIndex, 2, Uo.Neural_Out / 2, MaxoutOutput, 0);

            //******* Generate Decode Vector **********/
            Behavior.Computelib.Sgemm(MaxoutOutput, 0, OutputEmbed.Embedding, 0, WordProb, 0,
                    action.Count, OutputEmbed.Dim, OutputEmbed.VocabSize, 0, 1, false, true);
            if (OutputEmbed.IsBias) Behavior.Computelib.Matrix_Add_Linear(WordProb, OutputEmbed.Bias, action.Count, OutputEmbed.VocabSize);
            Behavior.Computelib.SoftMax(WordProb, WordProb, OutputEmbed.VocabSize, action.Count, 1);

        }

        public override void TakeSwitch()
        {
            input = output;
            output = another;
            another = input;
        }


        public void StatusUpdate(List<CudaPieceFloat> currentStatus, List<BeamAction> actions, List<CudaPieceFloat> newStatus, bool isAttention)
        {
            for (int i = 0; i < actions.Count; i++)
            {
                WordIndex.MemPtr[i] = actions[i].Words.Last();
                LagInfo.MemPtr[i] = actions[i].COId;
                BatchIdx[i] = actions[i].BatchId;
            }
            WordIndex.SyncFromCPU();
            LagInfo.SyncFromCPU();

            /*Embeding Input -> Output*/
            ComputeLib.SparseSgemmMask(SmpIndex, WordIndex, CudaPieceFloat.Empty, InputEmbed.Embedding, 0, IEmd, 0, actions.Count,
                    InputEmbed.VocabSize, InputEmbed.Dim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 0, 1, false, false);

            CudaPieceFloat input = IEmd;
            int inputDim = InputEmbed.Dim;
            //for (int i = 0; i < Model.LSTMCells.Count; i++)
            //{
            //    if (AttentionLibs == null || !isAttention || AttentionLibs[i] == null)
            //        LSTMComputeDecodeLib.LSTMForwardDecode(ComputeLib, actions.Count, input, LagInfo, inputDim,
            //            currentStatus[i].Item1, currentStatus[i].Item2, Model.LSTMCells[i], newStatus[i].Item1, newStatus[i].Item2,
            //            tmpO, GateO, GateI, CHat, GateF, TanhC);
            //    else
            //        LSTMComputeDecodeLib.LSTMForwardAttentionDecode(ComputeLib, actions.Count, input, LagInfo, inputDim,
            //            currentStatus[i].Item1, currentStatus[i].Item2, Model.LSTMCells[i], newStatus[i].Item1, newStatus[i].Item2,
            //            tmpO, AttentionLibs[i], AttentionLibs[i].Z, BatchIdx,
            //            GateO, GateI, CHat, GateF, TanhC);
            //    input = newStatus[i].Item1.Output.Data;
            //    inputDim = newStatus[i].Item1.Dim;
            //}
            for (int i = 0; i < Model.GRUCells.Count; i++)
            {
                if (AttentionLibs == null || !isAttention || AttentionLibs[i] == null)
                    GRUDecodeComputeLib.GRNNForwardDecode(ComputeLib, actions.Count, input, inputDim,
                        currentStatus[i], LagInfo, Model.GRUCells[i], newStatus[i], tmpO, 
                        null, null, BatchIdx, GateZ, GateR, rS, sP);
                else
                    GRUDecodeComputeLib.GRNNForwardDecode(ComputeLib, actions.Count, input, inputDim,
                        currentStatus[i], LagInfo, Model.GRUCells[i], newStatus[i], tmpO,
                        AttentionLibs[i], AttentionLibs[i].Z.Output.Data, BatchIdx, GateZ, GateR, rS, sP);

                input = newStatus[i];
                inputDim = Model.GRUCells[i].HiddenStateDim;
            }
        }

    }
}
