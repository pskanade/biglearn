﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public interface IAttentionComputeLib
    {
        void AttentionForward(CudaPieceFloat input, int offsetInput, int[] batchIdIdx, int batchIdOffset, CudaPieceFloat output, int offsetOutput, int hopId, int batchSize);

        void AttentionBackwardData(CudaPieceFloat inputDeriv, int offsetInput, int[] batchIdIdx, int batchIdOffset, 
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize);

        void AttentionBackwardWeight(CudaPieceFloat init, CudaPieceFloat input, RecurrentSeqInfo HelpInput, int cellDim, CudaPieceFloat zDeriv, int attentionDim);
    }

    public class MLPAttentionStructure : Structure
    {
        public int InputDim;
        public int MemoryDim;
        public int HiddenDim;

        /// <summary>
        /// Input to Hidden State Matrix.
        /// </summary>
        public CudaPieceFloat Wi;
        public CudaPieceFloat Wm;
        public CudaPieceFloat B;
        public CudaPieceFloat Wa;

        public CudaPieceFloat WiGrad;
        public CudaPieceFloat WmGrad;
        public CudaPieceFloat BGrad;
        public CudaPieceFloat WaGrad;


        public bool IsBias = false;

        public GradientOptimizer WiMatrixOptimizer { get { return StructureOptimizer["Wi"].Optimizer; } }
        public GradientOptimizer WmMatrixOptimizer { get { return StructureOptimizer["Wm"].Optimizer; } }
        public GradientOptimizer BMatrixOptimizer { get { return StructureOptimizer["B"].Optimizer; } }
        public GradientOptimizer WaMatrixOptimizer { get { return StructureOptimizer["Wa"].Optimizer; } }

        public override DataSourceID Type { get { return DataSourceID.MLPAttentionStructure; } }

        public MLPAttentionStructure(int inputDim, int memoryDim, int hiddenDim, DeviceType device)
        {
            InputDim = inputDim;
            MemoryDim = memoryDim;
            HiddenDim = hiddenDim;

            DeviceType = device;

            Wi = new CudaPieceFloat(InputDim * HiddenDim, true, device == DeviceType.GPU);
            

            Wm = new CudaPieceFloat(MemoryDim * HiddenDim, true, device == DeviceType.GPU);


            B = new CudaPieceFloat(HiddenDim, true, device == DeviceType.GPU);
            
            Wa = new CudaPieceFloat(HiddenDim, true, device == DeviceType.GPU);

            Init();
        }

        public MLPAttentionStructure(BinaryReader reader, DeviceType device) : base(reader, device) { }

        public void Init()
        {
            Wi.Init((float)(Math.Sqrt(6.0 / (InputDim + HiddenDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputDim + HiddenDim))));
            Wm.Init((float)(Math.Sqrt(6.0 / (MemoryDim + HiddenDim)) * 2), (float)(-Math.Sqrt(6.0 / (MemoryDim + HiddenDim))));
            Wa.Init((float)(Math.Sqrt(6.0 / (HiddenDim + 1)) * 2), (float)(-Math.Sqrt(6.0 / (HiddenDim + 1))));
            B.Init(0);
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(InputDim);
            writer.Write(MemoryDim);
            writer.Write(HiddenDim);

            Wi.Serialize(writer);
            Wm.Serialize(writer);
            Wa.Serialize(writer);
            B.Serialize(writer);
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            InputDim = reader.ReadInt32();
            MemoryDim = reader.ReadInt32();
            HiddenDim = reader.ReadInt32();

            Wi = new CudaPieceFloat(reader, device);
            Wm = new CudaPieceFloat(reader, device);
            Wa = new CudaPieceFloat(reader, device);
            B = new CudaPieceFloat(reader, device);
        }

        protected override void InitStructureOptimizer()
        {
            WiGrad = new CudaPieceFloat(InputDim * HiddenDim, DeviceType);
            WmGrad = new CudaPieceFloat(MemoryDim * HiddenDim, DeviceType);
            BGrad = new CudaPieceFloat(HiddenDim, DeviceType);
            WaGrad = new CudaPieceFloat(HiddenDim, DeviceType);

            StructureOptimizer.Add("Wi", new ModelOptimizer() { Parameter = Wi, Gradient = WiGrad, Name = "MLPAttention_Wi" });
            StructureOptimizer.Add("Wm", new ModelOptimizer() { Parameter = Wm, Gradient = WmGrad, Name = "MLPAttention_Wm" });
            StructureOptimizer.Add("B", new ModelOptimizer() { Parameter = B, Gradient = BGrad, Name = "MLPAttention_B" });
            StructureOptimizer.Add("Wa", new ModelOptimizer() { Parameter = Wa, Gradient = WaGrad, Name = "MLPAttention_Wa" });
        }

    }

    public class BasicMLPAttentionRunner : StructRunner, IAttentionComputeLib
    {
        public virtual void AttentionForward(CudaPieceFloat input, int offsetInput, int[] batchIdIdx, int batchIdOffset, CudaPieceFloat output, int offsetOutput, int hopId, int batchSize)
        {
            throw new NotImplementedException();
        }

        public virtual void AttentionBackwardData(CudaPieceFloat inputDeriv, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize)
        {
            throw new NotImplementedException();
        }

        public virtual void AttentionBackwardWeight(CudaPieceFloat init, CudaPieceFloat input, RecurrentSeqInfo HelpInput, int cellDim, CudaPieceFloat zDeriv, int attentionDim)
        {
            throw new NotImplementedException();
        }

        public virtual SeqDenseBatchData ZSeq { get; set; }

        public virtual HiddenBatchData Z { get; set; }

        public int ZMode = 0; // 0:ZSeq; 1:Z;

        public bool IsDelegate = true;

        public MatrixData ZMemory {
            get {
                if (ZMode == 0) return new MatrixData(ZSeq.Dim, ZSeq.MAX_SENTSIZE, ZSeq.SentOutput, ZSeq.SentDeriv, ZSeq.DeviceType);
                else if (ZMode == 1) return new MatrixData(Z.Dim, Z.MAX_BATCHSIZE, Z.Output.Data, Z.Deriv.Data, Z.DeviceType);
                else return null;
            } }
        //protected virtual HiddenBatchData[] Attention { get; set; }
        //public virtual SeqDenseBatchData Memory { get; set; }
        
        public BasicMLPAttentionRunner(Structure model, RunnerBehavior behavior) : base(model, behavior)
        { }
    }

    public class MLPAttentionRunner : BasicMLPAttentionRunner
    {
        new MLPAttentionStructure Model { get; set; }

        SeqHelpData MemoryHelp;
        DenseBatchData MemoryRedirect = null;
        float[] RedirectMem { get; set; }

        public override HiddenBatchData Z { get; set; }
        protected  HiddenBatchData[] Attention { get; set; }
        public SeqDenseBatchData Memory { get; set; }

        public override SeqDenseBatchData ZSeq { get; set; }
        SeqDenseBatchData MemoryHidden;
        int MaxHop { get; set; }
        
        HiddenBatchData[] Hidden;
        HiddenBatchData[] Address;

        CudaPieceInt[] tmpBatchIdx;
        CudaPieceInt[] tmpSource;
        CudaPieceInt[] tmpTarget;

        public MLPAttentionRunner(MLPAttentionStructure model, SeqDenseBatchData memory, SeqHelpData memoryHelp, DenseBatchData redirect, int maxBatchSize, 
            int maxCandidateNum, RunnerBehavior behavior) : base(model, behavior)
        {
            Model = model;
            Memory = memory;
            MemoryHelp = memoryHelp;
            MemoryRedirect = redirect;
            MemoryHidden = new SeqDenseBatchData(Memory.Stat.MAX_BATCHSIZE, Memory.Stat.MAX_SEQUENCESIZE, Model.HiddenDim, Behavior.Device);
            MaxHop = 1;

            Hidden = new HiddenBatchData[MaxHop];
            Address = new HiddenBatchData[MaxHop];
            Attention = new HiddenBatchData[MaxHop];

            tmpBatchIdx = new CudaPieceInt[MaxHop];
            tmpSource = new CudaPieceInt[MaxHop];
            tmpTarget = new CudaPieceInt[MaxHop];

            for (int h = 0; h < MaxHop; h++)
            {
                Hidden[h] = new HiddenBatchData(maxBatchSize * maxCandidateNum, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Address[h] = new HiddenBatchData(Memory.Stat.MAX_SEQUENCESIZE * maxCandidateNum, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Attention[h] = new HiddenBatchData(Memory.Stat.MAX_SEQUENCESIZE * maxCandidateNum, 1, Behavior.RunMode, Behavior.Device);

                tmpBatchIdx[h] = new CudaPieceInt(maxBatchSize * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
                tmpSource[h] = new CudaPieceInt(Memory.Stat.MAX_SEQUENCESIZE * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
                tmpTarget[h] = new CudaPieceInt(Memory.Stat.MAX_SEQUENCESIZE * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
            }
            Z = new HiddenBatchData(maxBatchSize * maxCandidateNum, Model.MemoryDim, Behavior.RunMode, Behavior.Device);
            ZMode = 1;
        }

        public MLPAttentionRunner(MLPAttentionStructure model, SeqDenseBatchData memory, SeqHelpData memoryHelp, DenseBatchData redirect, int maxHop, 
            SequenceDataStat inputStat, RunnerBehavior behavior) :
            base(model, behavior)
        {
            Model = model;
            Memory = memory;
            MemoryHelp = memoryHelp;
            MemoryRedirect = redirect;
            MemoryHidden = new SeqDenseBatchData(Memory.Stat.MAX_BATCHSIZE, Memory.Stat.MAX_SEQUENCESIZE, Model.HiddenDim, Behavior.Device);
            //MemoryAddress = new CudaPieceFloat(Memory.Stat.MAX_SEQUENCESIZE * Model.HiddenDim, true, Behavior.Device == DeviceType.GPU);
            MaxHop = maxHop;

            Hidden = new HiddenBatchData[MaxHop];
            Address = new HiddenBatchData[MaxHop];
            Attention = new HiddenBatchData[MaxHop];

            tmpBatchIdx = new CudaPieceInt[MaxHop];
            tmpSource = new CudaPieceInt[MaxHop];
            tmpTarget = new CudaPieceInt[MaxHop];

            for (int h = 0; h < MaxHop; h++)
            {
                Hidden[h] = new HiddenBatchData(Memory.Stat.MAX_BATCHSIZE, Model.HiddenDim, Behavior.RunMode, Behavior.Device); 
                Address[h] = new HiddenBatchData(Memory.Stat.MAX_SEQUENCESIZE, Model.HiddenDim, Behavior.RunMode, Behavior.Device); 
                Attention[h] = new HiddenBatchData(Memory.Stat.MAX_SEQUENCESIZE, 1, Behavior.RunMode, Behavior.Device);

                tmpBatchIdx[h] = new CudaPieceInt(Memory.Stat.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                tmpSource[h] = new CudaPieceInt(Memory.Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);
                tmpTarget[h] = new CudaPieceInt(Memory.Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);
            }
            ZSeq = new SeqDenseBatchData(inputStat.MAX_BATCHSIZE, inputStat.MAX_SEQUENCESIZE, Model.MemoryDim, Behavior.Device);
            ZMode = 0;
        }

        public override void Forward()
        {
            ComputeLib.Sgemm(Memory.SentOutput, 0, Model.Wm, 0,  MemoryHidden.SentOutput, 0, Memory.SentSize, Model.MemoryDim, Model.HiddenDim, 0, 1, false, false);

            // MemoryAddress = MemoryAddress + Bias
            if(Model.IsBias) ComputeLib.Matrix_Add_Linear(MemoryHidden.SentOutput, Model.B, Memory.SentSize, Model.HiddenDim);

            if(MemoryRedirect != null)
            {
                MemoryRedirect.Data.SyncToCPU();
                RedirectMem = MemoryRedirect.Data.MemPtr.Skip(Memory.BatchSize).ToArray();
            }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(MemoryHidden.SentDeriv, Memory.SentSize * Model.HiddenDim);
            ComputeLib.Zero(ZSeq.SentDeriv, ZSeq.SentDeriv.Size);
        }

        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Sgemm(MemoryHidden.SentDeriv, 0,
                                      Model.Wm, 0,
                                      Memory.SentDeriv, 0,
                                      Memory.SentSize, Model.HiddenDim, Model.MemoryDim, 1, 1, false, true);
        }

        public override void Update()
        {
            if (!IsDelegateOptimizer)
            {
                if(Model.IsBias) Model.BMatrixOptimizer.BeforeGradient();
                Model.WmMatrixOptimizer.BeforeGradient();
            }

            if (Model.IsBias)
            {
                //Model.BMatrixOptimizer.BeforeGradient();
                ComputeLib.ColumnWiseSum(MemoryHidden.SentDeriv, Model.BMatrixOptimizer.Gradient, Memory.SentSize, Model.HiddenDim,
                    Model.BMatrixOptimizer.GradientStep);
                //Model.BMatrixOptimizer.AfterGradient();
            }

            //Model.WmMatrixOptimizer.BeforeGradient();
            ComputeLib.Sgemm(Memory.SentOutput, 0,
                             MemoryHidden.SentDeriv, 0,
                             Model.WmMatrixOptimizer.Gradient, 0,
                             Memory.SentSize, Model.MemoryDim, Model.HiddenDim,
                             1, Model.WmMatrixOptimizer.GradientStep, true, false);
            //Model.WmMatrixOptimizer.AfterGradient();

            if (!IsDelegateOptimizer)
            {
                if (Model.IsBias) Model.BMatrixOptimizer.AfterGradient();
                Model.WmMatrixOptimizer.AfterGradient();
            }
        }

        public override void AttentionForward(CudaPieceFloat input, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat output, int offsetOutput, int hopId, int batchSize)
        {
            // h = input * wi;
            Behavior.Computelib.Sgemm(input, offsetInput, Model.Wi, 0, Hidden[hopId].Output.Data, 0,
                    batchSize, Model.InputDim, Model.HiddenDim, 0, 1, false, false);
            
            for (int i=0; i < batchSize; i++)
            {
                int bid = (int)RedirectMem[batchIdIdx[i + batchIdOffset]];
                int smpEnd = MemoryHelp.SampleIdx[bid];
                int smpBegin = bid == 0 ? 0 : MemoryHelp.SampleIdx[bid - 1];
                int smpMemoryLen = smpEnd - smpBegin;

                int id =  (i == 0 ? 0 : tmpBatchIdx[hopId].MemPtr[i - 1]);
                tmpBatchIdx[hopId].MemPtr[i] = id + smpMemoryLen;

                for(int m = smpBegin; m < smpEnd; m++)
                {
                    tmpTarget[hopId].MemPtr[id] = MemoryHelp.TransSeqIndexMem == null ? m : MemoryHelp.TransSeqIndexMem[m];
                    tmpSource[hopId].MemPtr[id] = i;
                    id++;
                }
            }

            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];
            tmpSource[hopId].SyncFromCPU(memorySize);
            tmpTarget[hopId].SyncFromCPU(memorySize);
            tmpBatchIdx[hopId].SyncFromCPU(batchSize);

            // Address = h + memoryH
            Behavior.Computelib.Matrix_AdditionMask(Hidden[hopId].Output.Data, 0, tmpSource[hopId], 0,
                                                    MemoryHidden.SentOutput, 0, tmpTarget[hopId], 0,
                                                    Address[hopId].Output.Data, 0, CudaPieceInt.Empty, 0, 
                                                    Model.HiddenDim, memorySize, 1, 1, 0);

            // address = tanh(address)
            Behavior.Computelib.Tanh(Address[hopId].Output.Data, 0, Address[hopId].Output.Data, 0, memorySize * Model.HiddenDim);

            // attention = address * Wa.
            Behavior.Computelib.Sgemv(Address[hopId].Output.Data, Model.Wa, memorySize, Model.HiddenDim, Attention[hopId].Output.Data, false, 0, 1);
            
            // softmax of attention
            Behavior.Computelib.SparseSoftmax(tmpBatchIdx[hopId], Attention[hopId].Output.Data, Attention[hopId].Output.Data, 1, batchSize);

            // attention memory 2 output.
            Behavior.Computelib.ColumnWiseSumMask(Memory.SentOutput, 0, tmpTarget[hopId], 0,
                Attention[hopId].Output.Data, tmpBatchIdx[hopId], batchSize,
                output, offsetOutput, CudaPieceInt.Empty, 0, 
                memorySize, Model.MemoryDim, 0, 1);
        }

        public override void AttentionBackwardData(CudaPieceFloat inputDeriv, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize)
        {
            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];

            // outputDeriv -> memoryDeriv.
            Behavior.Computelib.Scale_MatrixMask(outputDeriv, offsetOutput, tmpSource[hopId], 0,
                                                Memory.SentDeriv, 0, tmpTarget[hopId], 0,
                                                Model.MemoryDim, memorySize, Attention[hopId].Output.Data, 1);

            // outputDeriv -> attentionDeriv.
            Behavior.Computelib.Inner_Product_Matching(outputDeriv, offsetOutput, Memory.SentOutput, 0, Attention[hopId].Deriv.Data, 0,
                tmpSource[hopId], tmpTarget[hopId], batchSize, Memory.SentSize, memorySize, Model.MemoryDim, Util.GPUEpsilon);

            // attentionDeriv = attentionDeriv * SoftMax(Attention)'
            Behavior.Computelib.DerivSparseMultiClassSoftmax(tmpBatchIdx[hopId], Attention[hopId].Output.Data, Attention[hopId].Deriv.Data, Attention[hopId].Deriv.Data, 1, batchSize);

            // addressDeriv = attentionDeriv * Wa'.
            Behavior.Computelib.Sgemm(Attention[hopId].Deriv.Data, 0, Model.Wa, 0, Address[hopId].Deriv.Data, 0, memorySize, 1, Model.HiddenDim, 0, 1, false, false);

            // addressDeriv = addressDeriv * tanh(address)'
            Behavior.Computelib.DerivTanh(Address[hopId].Output.Data, 0, Address[hopId].Deriv.Data, 0, Address[hopId].Deriv.Data, 0, memorySize * Model.HiddenDim, 0, 1);

            
            // hiddenDeriv = sum(addressDeriv)
            Behavior.Computelib.ColumnWiseSumMask(Address[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                CudaPieceFloat.Empty, tmpBatchIdx[hopId], batchSize,
                Hidden[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                memorySize, Model.HiddenDim, 0, 1);
            
            // inputDeriv += hiddenDeriv * wi'
            Behavior.Computelib.Sgemm(Hidden[hopId].Deriv.Data, 0,
                                          Model.Wi, 0,
                                          inputDeriv, offsetInput,
                                          batchSize, Model.HiddenDim, Model.InputDim, 1, 1, false, true);

            Behavior.Computelib.Matrix_AdditionMask(Address[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                    MemoryHidden.SentDeriv, 0, tmpTarget[hopId], 0,
                                                    MemoryHidden.SentDeriv, 0, tmpTarget[hopId], 0,
                                                    Model.HiddenDim, memorySize, 1, 1, 0);
            
        }

        public void AttentionBackwardWeight(CudaPieceFloat input, int offsetInput, CudaPieceInt inputMask, int inputMaskOffset, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize)
        {
            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];

            //  Wa  +=  attentionDeriv * address.
            Behavior.Computelib.Sgemm(Attention[hopId].Deriv.Data, 0, Address[hopId].Output.Data, 0, Model.WaMatrixOptimizer.Gradient, 0, 
                memorySize, 1, Model.HiddenDim, 1, Model.WaMatrixOptimizer.GradientStep, true, false);
            
            Behavior.Computelib.Sgemm(input, offsetInput,
                                          Hidden[hopId].Deriv.Data, 0,
                                          Model.WiMatrixOptimizer.Gradient, 0,
                                          batchSize, Model.InputDim, Model.HiddenDim,
                                          1, Model.WiMatrixOptimizer.GradientStep, true, false);
        }

        //public override void AttentionBackwardWeight(CudaPieceFloat init, CudaPieceFloat input, SeqHelpData HelpInput, int cellDim, CudaPieceFloat zDeriv, int attentionDim)
        //{
        //    if (!IsDelegateOptimizer)
        //    {
        //        Model.WiMatrixOptimizer.BeforeGradient();
        //        Model.WaMatrixOptimizer.BeforeGradient();
        //    }
        //    int preStartIdx = -1;
        //    for (int i = 0; i < HelpInput.MaxLag; i++)
        //    {
        //        int startIdx = i == 0 ? 0 : HelpInput.LagSeqIndex[i - 1];
        //        int Sum = HelpInput.LagSeqIndex[i] - startIdx;
        //        if (Sum == 0) break;

        //        if (i > 0)
        //        {
        //            AttentionBackwardWeight(input, preStartIdx * cellDim, CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
        //                    HelpInput.LagSeqElementMem, startIdx,
        //                    zDeriv, startIdx * attentionDim, i, Sum);
        //        }
        //        else if(init != null)
        //        {
        //            AttentionBackwardWeight(init, 0, CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
        //                    HelpInput.LagSeqElementMem, startIdx,
        //                    zDeriv, startIdx * attentionDim, i, Sum);
        //        }
        //        preStartIdx = startIdx;
        //    }

        //    if (!IsDelegateOptimizer)
        //    {
        //        Model.WiMatrixOptimizer.AfterGradient();
        //        Model.WaMatrixOptimizer.AfterGradient();
        //    }
        //}

        public override void AttentionBackwardWeight(CudaPieceFloat init, CudaPieceFloat input, RecurrentSeqInfo HelpInput, int cellDim, CudaPieceFloat zDeriv, int attentionDim)
        {
            if (!IsDelegateOptimizer)
            {
                Model.WiMatrixOptimizer.BeforeGradient();
                Model.WaMatrixOptimizer.BeforeGradient();
            }
            int preStartIdx = -1;
            for (int i = 0; i < HelpInput.MaxLag; i++)
            {
                int startIdx = i == 0 ? 0 : HelpInput.LagSeqIdx[i - 1];
                int Sum = HelpInput.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;

                if (i > 0)
                {
                    AttentionBackwardWeight(input, preStartIdx * cellDim, CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
                            HelpInput.LagSeqElement, startIdx,
                            zDeriv, startIdx * attentionDim, i, Sum);
                }
                else if (init != null)
                {
                    AttentionBackwardWeight(init, 0, CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
                            HelpInput.LagSeqElement, startIdx,
                            zDeriv, startIdx * attentionDim, i, Sum);
                }
                preStartIdx = startIdx;
            }

            if (!IsDelegateOptimizer)
            {
                Model.WiMatrixOptimizer.AfterGradient();
                Model.WaMatrixOptimizer.AfterGradient();
            }
        }
    }

    public class MLPAttentionV2Runner : BasicMLPAttentionRunner
    {
        new MLPAttentionStructure Model { get; set; }

        SeqDenseBatchData MemoryHidden;
        int MaxHop { get; set; }

        //RecurrentSeqInfo MemoryHelp;

        int[] SampleIdxMem;
        //int[] MapForwardMem;

        public override HiddenBatchData Z { get; set; }
        protected  HiddenBatchData[] Attention { get; set; }
        public  SeqDenseBatchData Memory { get; set; }
        public override SeqDenseBatchData ZSeq { get; set; }
        
        HiddenBatchData[] Hidden;
        HiddenBatchData[] Address;

        CudaPieceInt[] tmpBatchIdx;
        CudaPieceInt[] tmpSource;
        CudaPieceInt[] tmpTarget;

        //RecurrentSeqInfo memoryHelp, 
        public MLPAttentionV2Runner(MLPAttentionStructure model, SeqDenseBatchData memory, int maxBatchSize, int maxCandidateNum, RunnerBehavior behavior) :  base(model, behavior)
        {
            Model = model;
            Memory = memory;
            //MemoryHelp = memoryHelp;
            MemoryHidden = new SeqDenseBatchData(Memory.Stat.MAX_BATCHSIZE, Memory.Stat.MAX_SEQUENCESIZE, Model.HiddenDim, Behavior.Device);
            MaxHop = 1;

            Hidden = new HiddenBatchData[MaxHop];
            Address = new HiddenBatchData[MaxHop];
            Attention = new HiddenBatchData[MaxHop];

            tmpBatchIdx = new CudaPieceInt[MaxHop];
            tmpSource = new CudaPieceInt[MaxHop];
            tmpTarget = new CudaPieceInt[MaxHop];

            for (int h = 0; h < MaxHop; h++)
            {
                Hidden[h] = new HiddenBatchData(maxBatchSize * maxCandidateNum, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Address[h] = new HiddenBatchData(Memory.Stat.MAX_SEQUENCESIZE * maxCandidateNum, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Attention[h] = new HiddenBatchData(Memory.Stat.MAX_SEQUENCESIZE * maxCandidateNum, 1, Behavior.RunMode, Behavior.Device);

                tmpBatchIdx[h] = new CudaPieceInt(maxBatchSize * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
                tmpSource[h] = new CudaPieceInt(Memory.Stat.MAX_SEQUENCESIZE * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
                tmpTarget[h] = new CudaPieceInt(Memory.Stat.MAX_SEQUENCESIZE * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
            }
            Z = new HiddenBatchData(maxBatchSize * maxCandidateNum, Model.MemoryDim, Behavior.RunMode, Behavior.Device);
            ZMode = 1;
        }

        public MLPAttentionV2Runner(MLPAttentionStructure model, SeqDenseBatchData memory, int maxHop, int maxBatchSize, int maxSentSize, RunnerBehavior behavior) : base(model, behavior)
        {
            Model = model;
            Memory = memory;
            //MemoryHelp = memoryHelp;
            MemoryHidden = new SeqDenseBatchData(Memory.Stat.MAX_BATCHSIZE, Memory.Stat.MAX_SEQUENCESIZE, Model.HiddenDim, Behavior.Device);
            MaxHop = maxHop;

            Hidden = new HiddenBatchData[MaxHop];
            Address = new HiddenBatchData[MaxHop];
            Attention = new HiddenBatchData[MaxHop];

            tmpBatchIdx = new CudaPieceInt[MaxHop];
            tmpSource = new CudaPieceInt[MaxHop];
            tmpTarget = new CudaPieceInt[MaxHop];

            for (int h = 0; h < MaxHop; h++)
            {
                Hidden[h] = new HiddenBatchData(Memory.Stat.MAX_BATCHSIZE, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Address[h] = new HiddenBatchData(Memory.Stat.MAX_SEQUENCESIZE, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Attention[h] = new HiddenBatchData(Memory.Stat.MAX_SEQUENCESIZE, 1, Behavior.RunMode, Behavior.Device);

                tmpBatchIdx[h] = new CudaPieceInt(Memory.Stat.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                tmpSource[h] = new CudaPieceInt(Memory.Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);
                tmpTarget[h] = new CudaPieceInt(Memory.Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);
            }
            ZSeq = new SeqDenseBatchData(maxBatchSize, maxSentSize, Model.MemoryDim, Behavior.Device);
            ZMode = 0;
        }

        #region attention memory routing.

        public override void Forward()
        {
            ComputeLib.Sgemm(Memory.SentOutput, 0, Model.Wm, 0, MemoryHidden.SentOutput, 0, Memory.SentSize, Model.MemoryDim, Model.HiddenDim, 0, 1, false, false);
            if (Model.IsBias) ComputeLib.Matrix_Add_Linear(MemoryHidden.SentOutput, Model.B, Memory.SentSize, Model.HiddenDim);

            //if (MemoryHelp == null)
            { Memory.SampleIdx.SyncToCPU(); SampleIdxMem = Memory.SampleIdx.MemPtr; }// MapForwardMem = null; }
            //else { SampleIdxMem = MemoryHelp.SampleIdxMem; MapForwardMem = MemoryHelp.MapForwardMem; }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(MemoryHidden.SentDeriv, Memory.SentSize * Model.HiddenDim);
            ComputeLib.Zero(ZSeq.SentDeriv, ZSeq.SentDeriv.Size);
        }

        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Sgemm(MemoryHidden.SentDeriv, 0,
                             Model.Wm, 0,
                             Memory.SentDeriv, 0,
                             Memory.SentSize, Model.HiddenDim, Model.MemoryDim, 1, 1, false, true);
        }

        public override void Update()
        {
            if (!IsDelegateOptimizer)
            {
                if (Model.IsBias) Model.BMatrixOptimizer.BeforeGradient();
                Model.WmMatrixOptimizer.BeforeGradient();
            }

            if (Model.IsBias)
            {
                ComputeLib.ColumnWiseSum(MemoryHidden.SentDeriv, Model.BMatrixOptimizer.Gradient, Memory.SentSize, Model.HiddenDim,
                    Model.BMatrixOptimizer.GradientStep);
            }

            ComputeLib.Sgemm(Memory.SentOutput, 0,
                             MemoryHidden.SentDeriv, 0,
                             Model.WmMatrixOptimizer.Gradient, 0,
                             Memory.SentSize, Model.MemoryDim, Model.HiddenDim,
                             1, Model.WmMatrixOptimizer.GradientStep, true, false);

            if (!IsDelegateOptimizer)
            {
                if (Model.IsBias) Model.BMatrixOptimizer.AfterGradient();
                Model.WmMatrixOptimizer.AfterGradient();
            }
        }
        #endregion

        public override void AttentionForward(CudaPieceFloat input, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat output, int offsetOutput, int hopId, int batchSize)
        {
            // h = input * wi;
            ComputeLib.Sgemm(input, offsetInput, Model.Wi, 0, Hidden[hopId].Output.Data, 0,
                    batchSize, Model.InputDim, Model.HiddenDim, 0, 1, false, false);

            for (int i = 0; i < batchSize; i++)
            {
                int bid = batchIdIdx[i + batchIdOffset];
                int smpEnd = SampleIdxMem[bid];
                int smpBegin = bid == 0 ? 0 : SampleIdxMem[bid - 1];
                int smpMemoryLen = smpEnd - smpBegin;

                int id = (i == 0 ? 0 : tmpBatchIdx[hopId].MemPtr[i - 1]);
                tmpBatchIdx[hopId].MemPtr[i] = id + smpMemoryLen;

                for (int m = smpBegin; m < smpEnd; m++)
                {
                    tmpTarget[hopId].MemPtr[id] = m; // MapForwardMem == null ? m : MapForwardMem[m];
                    tmpSource[hopId].MemPtr[id] = i;
                    id++;
                }
            }

            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];
            tmpSource[hopId].SyncFromCPU(memorySize);
            tmpTarget[hopId].SyncFromCPU(memorySize);
            tmpBatchIdx[hopId].SyncFromCPU(batchSize);

            // Address = h + memoryH
            Behavior.Computelib.Matrix_AdditionMask(Hidden[hopId].Output.Data, 0, tmpSource[hopId], 0,
                                                    MemoryHidden.SentOutput, 0, tmpTarget[hopId], 0,
                                                    Address[hopId].Output.Data, 0, CudaPieceInt.Empty, 0,
                                                    Model.HiddenDim, memorySize, 1, 1, 0);

            // address = tanh(address)
            Behavior.Computelib.Tanh(Address[hopId].Output.Data, 0, Address[hopId].Output.Data, 0, memorySize * Model.HiddenDim);

            // attention = address * Wa.
            Behavior.Computelib.Sgemv(Address[hopId].Output.Data, Model.Wa, memorySize, Model.HiddenDim, Attention[hopId].Output.Data, false, 0, 1);

            // softmax of attention
            Behavior.Computelib.SparseSoftmax(tmpBatchIdx[hopId], Attention[hopId].Output.Data, Attention[hopId].Output.Data, 1, batchSize);

            // attention memory 2 output.
            Behavior.Computelib.ColumnWiseSumMask(Memory.SentOutput, 0, tmpTarget[hopId], 0,
                Attention[hopId].Output.Data, tmpBatchIdx[hopId], batchSize,
                output, offsetOutput, CudaPieceInt.Empty, 0,
                memorySize, Model.MemoryDim, 0, 1);
        }

        public override void AttentionBackwardData(CudaPieceFloat inputDeriv, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize)
        {
            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];

            // outputDeriv -> memoryDeriv.
            Behavior.Computelib.Scale_MatrixMask(outputDeriv, offsetOutput, tmpSource[hopId], 0,
                                                Memory.SentDeriv, 0, tmpTarget[hopId], 0,
                                                Model.MemoryDim, memorySize, Attention[hopId].Output.Data, 1);

            // outputDeriv -> attentionDeriv.
            Behavior.Computelib.Inner_Product_Matching(outputDeriv, offsetOutput, Memory.SentOutput, 0, Attention[hopId].Deriv.Data, 0,
                tmpSource[hopId], tmpTarget[hopId], batchSize, Memory.SentSize, memorySize, Model.MemoryDim, Util.GPUEpsilon);

            // attentionDeriv = attentionDeriv * SoftMax(Attention)'
            Behavior.Computelib.DerivSparseMultiClassSoftmax(tmpBatchIdx[hopId], Attention[hopId].Output.Data, Attention[hopId].Deriv.Data, Attention[hopId].Deriv.Data, 1, batchSize);

            // addressDeriv = attentionDeriv * Wa'.
            Behavior.Computelib.Sgemm(Attention[hopId].Deriv.Data, 0, Model.Wa, 0, Address[hopId].Deriv.Data, 0, memorySize, 1, Model.HiddenDim, 0, 1, false, false);

            // addressDeriv = addressDeriv * tanh(address)'
            Behavior.Computelib.DerivTanh(Address[hopId].Output.Data, 0, Address[hopId].Deriv.Data, 0, Address[hopId].Deriv.Data, 0, memorySize * Model.HiddenDim, 0, 1);


            // hiddenDeriv = sum(addressDeriv)
            Behavior.Computelib.ColumnWiseSumMask(Address[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                CudaPieceFloat.Empty, tmpBatchIdx[hopId], batchSize,
                Hidden[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                memorySize, Model.HiddenDim, 0, 1);

            // inputDeriv += hiddenDeriv * wi'
            Behavior.Computelib.Sgemm(Hidden[hopId].Deriv.Data, 0,
                                          Model.Wi, 0,
                                          inputDeriv, offsetInput,
                                          batchSize, Model.HiddenDim, Model.InputDim, 1, 1, false, true);

            Behavior.Computelib.Matrix_AdditionMask(Address[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                    MemoryHidden.SentDeriv, 0, tmpTarget[hopId], 0,
                                                    MemoryHidden.SentDeriv, 0, tmpTarget[hopId], 0,
                                                    Model.HiddenDim, memorySize, 1, 1, 0);

        }

        public void AttentionBackwardWeight(CudaPieceFloat input, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize)
        {
            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];

            //  Wa  +=  attentionDeriv * address.
            Behavior.Computelib.Sgemm(Attention[hopId].Deriv.Data, 0, Address[hopId].Output.Data, 0, Model.WaMatrixOptimizer.Gradient, 0,
                memorySize, 1, Model.HiddenDim, 1, Model.WaMatrixOptimizer.GradientStep, true, false);

            Behavior.Computelib.Sgemm(input, offsetInput,
                                          Hidden[hopId].Deriv.Data, 0,
                                          Model.WiMatrixOptimizer.Gradient, 0,
                                          batchSize, Model.InputDim, Model.HiddenDim,
                                          1, Model.WiMatrixOptimizer.GradientStep, true, false);
        }

        public override void AttentionBackwardWeight(CudaPieceFloat init, CudaPieceFloat input, RecurrentSeqInfo HelpInput, int cellDim, CudaPieceFloat zDeriv, int attentionDim)
        {
            if (!IsDelegateOptimizer)
            {
                Model.WiMatrixOptimizer.BeforeGradient();
                Model.WaMatrixOptimizer.BeforeGradient();
            }
            int preStartIdx = -1;
            for (int i = 0; i < HelpInput.MaxLag; i++)
            {
                int startIdx = i == 0 ? 0 : HelpInput.LagSeqIdx[i - 1];
                int Sum = HelpInput.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;

                if (i > 0)
                {
                    AttentionBackwardWeight(input, preStartIdx * cellDim, //CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
                            HelpInput.LagSeqElement, startIdx,
                            zDeriv, startIdx * attentionDim, i, Sum);
                }
                else if (init != null)
                {
                    AttentionBackwardWeight(init, 0, //CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
                            HelpInput.LagSeqElement, 0,
                            zDeriv, 0, i, Sum);
                }
                preStartIdx = startIdx;
            }

            if (!IsDelegateOptimizer)
            {
                Model.WiMatrixOptimizer.AfterGradient();
                Model.WaMatrixOptimizer.AfterGradient();
            }
        }
    }

    public class MLPAttentionV5Runner : BasicMLPAttentionRunner
    {
        new MLPAttentionStructure Model { get; set; }

        SeqDenseBatchData MemoryHidden;
        int MaxHop { get; set; }

        //RecurrentSeqInfo MemoryHelp;

        int[] SampleIdxMem;
        //int[] MapForwardMem;

        public override HiddenBatchData Z { get; set; }
        protected HiddenBatchData[] Attention { get; set; }
        public SeqDenseBatchData Memory { get; set; }
        public override SeqDenseBatchData ZSeq { get; set; }

        HiddenBatchData[] Hidden;
        HiddenBatchData[] Address;

        CudaPieceInt[] tmpBatchIdx;
        CudaPieceInt[] tmpSource;
        CudaPieceInt[] tmpTarget;

        //RecurrentSeqInfo memoryHelp, 
        public MLPAttentionV5Runner(MLPAttentionStructure model, SeqDenseBatchData memory, SeqDenseBatchData memoryHidden, int maxBatchSize, int maxCandidateNum, RunnerBehavior behavior) : base(model, behavior)
        {
            Model = model;
            Memory = memory;
            MemoryHidden = memoryHidden;
            MaxHop = 1;

            Hidden = new HiddenBatchData[MaxHop];
            Address = new HiddenBatchData[MaxHop];
            Attention = new HiddenBatchData[MaxHop];

            tmpBatchIdx = new CudaPieceInt[MaxHop];
            tmpSource = new CudaPieceInt[MaxHop];
            tmpTarget = new CudaPieceInt[MaxHop];

            for (int h = 0; h < MaxHop; h++)
            {
                Hidden[h] = new HiddenBatchData(maxBatchSize * maxCandidateNum, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Address[h] = new HiddenBatchData(Memory.Stat.MAX_SEQUENCESIZE * maxCandidateNum, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Attention[h] = new HiddenBatchData(Memory.Stat.MAX_SEQUENCESIZE * maxCandidateNum, 1, Behavior.RunMode, Behavior.Device);

                tmpBatchIdx[h] = new CudaPieceInt(maxBatchSize * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
                tmpSource[h] = new CudaPieceInt(Memory.Stat.MAX_SEQUENCESIZE * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
                tmpTarget[h] = new CudaPieceInt(Memory.Stat.MAX_SEQUENCESIZE * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
            }
            Z = new HiddenBatchData(maxBatchSize * maxCandidateNum, Model.MemoryDim, Behavior.RunMode, Behavior.Device);
            ZMode = 1;
        }

        public MLPAttentionV5Runner(MLPAttentionStructure model, SeqDenseBatchData memory, SeqDenseBatchData memoryHidden, int maxHop, int maxBatchSize, int maxSentSize, RunnerBehavior behavior) : base(model, behavior)
        {
            Model = model;
            Memory = memory;
            MemoryHidden = memoryHidden;
            MaxHop = maxHop;

            Hidden = new HiddenBatchData[MaxHop];
            Address = new HiddenBatchData[MaxHop];
            Attention = new HiddenBatchData[MaxHop];

            tmpBatchIdx = new CudaPieceInt[MaxHop];
            tmpSource = new CudaPieceInt[MaxHop];
            tmpTarget = new CudaPieceInt[MaxHop];

            for (int h = 0; h < MaxHop; h++)
            {
                Hidden[h] = new HiddenBatchData(Memory.Stat.MAX_BATCHSIZE, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Address[h] = new HiddenBatchData(Memory.Stat.MAX_SEQUENCESIZE, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Attention[h] = new HiddenBatchData(Memory.Stat.MAX_SEQUENCESIZE, 1, Behavior.RunMode, Behavior.Device);

                tmpBatchIdx[h] = new CudaPieceInt(Memory.Stat.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                tmpSource[h] = new CudaPieceInt(Memory.Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);
                tmpTarget[h] = new CudaPieceInt(Memory.Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);
            }
            ZSeq = new SeqDenseBatchData(maxBatchSize, maxSentSize, Model.MemoryDim, Behavior.Device);
            ZMode = 0;
        }

        #region attention memory routing.

        public override void Forward()
        {
            //if (MemoryHelp == null)
            { Memory.SampleIdx.SyncToCPU(); SampleIdxMem = Memory.SampleIdx.MemPtr; }// MapForwardMem = null; }
            //else { SampleIdxMem = MemoryHelp.SampleIdxMem; MapForwardMem = MemoryHelp.MapForwardMem; }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(ZSeq.SentDeriv, ZSeq.SentDeriv.Size);
        }

        public override void Backward(bool cleanDeriv)
        { }

        public override void Update()
        { }
        #endregion

        public override void AttentionForward(CudaPieceFloat input, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat output, int offsetOutput, int hopId, int batchSize)
        {
            // h = input * wi;
            ComputeLib.Sgemm(input, offsetInput, Model.Wi, 0, Hidden[hopId].Output.Data, 0,
                    batchSize, Model.InputDim, Model.HiddenDim, 0, 1, false, false);

            for (int i = 0; i < batchSize; i++)
            {
                int bid = batchIdIdx[i + batchIdOffset];
                int smpEnd = SampleIdxMem[bid];
                int smpBegin = bid == 0 ? 0 : SampleIdxMem[bid - 1];
                int smpMemoryLen = smpEnd - smpBegin;

                int id = (i == 0 ? 0 : tmpBatchIdx[hopId].MemPtr[i - 1]);
                tmpBatchIdx[hopId].MemPtr[i] = id + smpMemoryLen;

                for (int m = smpBegin; m < smpEnd; m++)
                {
                    tmpTarget[hopId].MemPtr[id] = m; // MapForwardMem == null ? m : MapForwardMem[m];
                    tmpSource[hopId].MemPtr[id] = i;
                    id++;
                }
            }

            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];
            tmpSource[hopId].SyncFromCPU(memorySize);
            tmpTarget[hopId].SyncFromCPU(memorySize);
            tmpBatchIdx[hopId].SyncFromCPU(batchSize);

            // Address = h + memoryH
            Behavior.Computelib.Matrix_AdditionMask(Hidden[hopId].Output.Data, 0, tmpSource[hopId], 0,
                                                    MemoryHidden.SentOutput, 0, tmpTarget[hopId], 0,
                                                    Address[hopId].Output.Data, 0, CudaPieceInt.Empty, 0,
                                                    Model.HiddenDim, memorySize, 1, 1, 0);

            // address = tanh(address)
            Behavior.Computelib.Tanh(Address[hopId].Output.Data, 0, Address[hopId].Output.Data, 0, memorySize * Model.HiddenDim);

            // attention = address * Wa.
            Behavior.Computelib.Sgemv(Address[hopId].Output.Data, Model.Wa, memorySize, Model.HiddenDim, Attention[hopId].Output.Data, false, 0, 1);

            // softmax of attention
            Behavior.Computelib.SparseSoftmax(tmpBatchIdx[hopId], Attention[hopId].Output.Data, Attention[hopId].Output.Data, 1, batchSize);

            // attention memory 2 output.
            Behavior.Computelib.ColumnWiseSumMask(Memory.SentOutput, 0, tmpTarget[hopId], 0,
                Attention[hopId].Output.Data, tmpBatchIdx[hopId], batchSize,
                output, offsetOutput, CudaPieceInt.Empty, 0,
                memorySize, Model.MemoryDim, 0, 1);
        }

        public override void AttentionBackwardData(CudaPieceFloat inputDeriv, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize)
        {
            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];

            // outputDeriv -> memoryDeriv.
            Behavior.Computelib.Scale_MatrixMask(outputDeriv, offsetOutput, tmpSource[hopId], 0,
                                                Memory.SentDeriv, 0, tmpTarget[hopId], 0,
                                                Model.MemoryDim, memorySize, Attention[hopId].Output.Data, 1);

            // outputDeriv -> attentionDeriv.
            Behavior.Computelib.Inner_Product_Matching(outputDeriv, offsetOutput, Memory.SentOutput, 0, Attention[hopId].Deriv.Data, 0,
                tmpSource[hopId], tmpTarget[hopId], batchSize, Memory.SentSize, memorySize, Model.MemoryDim, Util.GPUEpsilon);

            // attentionDeriv = attentionDeriv * SoftMax(Attention)'
            Behavior.Computelib.DerivSparseMultiClassSoftmax(tmpBatchIdx[hopId], Attention[hopId].Output.Data, Attention[hopId].Deriv.Data, Attention[hopId].Deriv.Data, 1, batchSize);

            // addressDeriv = attentionDeriv * Wa'.
            Behavior.Computelib.Sgemm(Attention[hopId].Deriv.Data, 0, Model.Wa, 0, Address[hopId].Deriv.Data, 0, memorySize, 1, Model.HiddenDim, 0, 1, false, false);

            // addressDeriv = addressDeriv * tanh(address)'
            Behavior.Computelib.DerivTanh(Address[hopId].Output.Data, 0, Address[hopId].Deriv.Data, 0, Address[hopId].Deriv.Data, 0, memorySize * Model.HiddenDim, 0, 1);


            // hiddenDeriv = sum(addressDeriv)
            Behavior.Computelib.ColumnWiseSumMask(Address[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                CudaPieceFloat.Empty, tmpBatchIdx[hopId], batchSize,
                Hidden[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                memorySize, Model.HiddenDim, 0, 1);

            // inputDeriv += hiddenDeriv * wi'
            Behavior.Computelib.Sgemm(Hidden[hopId].Deriv.Data, 0,
                                          Model.Wi, 0,
                                          inputDeriv, offsetInput,
                                          batchSize, Model.HiddenDim, Model.InputDim, 1, 1, false, true);

            Behavior.Computelib.Matrix_AdditionMask(Address[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                    MemoryHidden.SentDeriv, 0, tmpTarget[hopId], 0,
                                                    MemoryHidden.SentDeriv, 0, tmpTarget[hopId], 0,
                                                    Model.HiddenDim, memorySize, 1, 1, 0);

        }

        public void AttentionBackwardWeight(CudaPieceFloat input, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize)
        {
            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];

            //  Wa  +=  attentionDeriv * address.
            Behavior.Computelib.Sgemm(Attention[hopId].Deriv.Data, 0, Address[hopId].Output.Data, 0, Model.WaMatrixOptimizer.Gradient, 0,
                memorySize, 1, Model.HiddenDim, 1, Model.WaMatrixOptimizer.GradientStep, true, false);

            Behavior.Computelib.Sgemm(input, offsetInput,
                                          Hidden[hopId].Deriv.Data, 0,
                                          Model.WiMatrixOptimizer.Gradient, 0,
                                          batchSize, Model.InputDim, Model.HiddenDim,
                                          1, Model.WiMatrixOptimizer.GradientStep, true, false);
        }

        public override void AttentionBackwardWeight(CudaPieceFloat init, CudaPieceFloat input, RecurrentSeqInfo HelpInput, int cellDim, CudaPieceFloat zDeriv, int attentionDim)
        {
            if (!IsDelegateOptimizer)
            {
                Model.WiMatrixOptimizer.BeforeGradient();
                Model.WaMatrixOptimizer.BeforeGradient();
            }
            int preStartIdx = -1;
            for (int i = 0; i < HelpInput.MaxLag; i++)
            {
                int startIdx = i == 0 ? 0 : HelpInput.LagSeqIdx[i - 1];
                int Sum = HelpInput.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;

                if (i > 0)
                {
                    AttentionBackwardWeight(input, preStartIdx * cellDim, //CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
                            HelpInput.LagSeqElement, startIdx,
                            zDeriv, startIdx * attentionDim, i, Sum);
                }
                else if (init != null)
                {
                    AttentionBackwardWeight(init, 0, //CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
                            HelpInput.LagSeqElement, 0,
                            zDeriv, 0, i, Sum);
                }
                preStartIdx = startIdx;
            }

            if (!IsDelegateOptimizer)
            {
                Model.WiMatrixOptimizer.AfterGradient();
                Model.WaMatrixOptimizer.AfterGradient();
            }
        }
    }

    public class MLPAttentionV3Runner : BasicMLPAttentionRunner
    {
        new MLPAttentionStructure Model { get; set; }

        HiddenBatchData MemoryHidden { get { return EnsembleMemHiddenRunner.Output; } }
        int HiddenSeqNum { get; set; }
        EnsembleMatrixRunner EnsembleMemHiddenRunner = null;
        int MaxHop { get; set; }

        public override HiddenBatchData Z { get; set; }
        protected  HiddenBatchData[] Attention { get; set; }
        public override SeqDenseBatchData ZSeq { get; set; }

        HiddenBatchData[] Hidden;
        HiddenBatchData[] Address;

        CudaPieceInt[] tmpBatchIdx;
        CudaPieceInt[] tmpSource;
        CudaPieceInt[] tmpTarget;

        public MLPAttentionV3Runner(MLPAttentionStructure model, HiddenBatchData[] memHidden, int maxBatchSize, int maxCandidateNum, RunnerBehavior behavior) :
            base(model, behavior)
        {
            Model = model;
            EnsembleMemHiddenRunner = new EnsembleMatrixRunner(memHidden.ToList(), behavior);
            HiddenSeqNum = memHidden.Length;

            MaxHop = 1;

            Hidden = new HiddenBatchData[MaxHop];
            Address = new HiddenBatchData[MaxHop];
            Attention = new HiddenBatchData[MaxHop];

            tmpBatchIdx = new CudaPieceInt[MaxHop];
            tmpSource = new CudaPieceInt[MaxHop];
            tmpTarget = new CudaPieceInt[MaxHop];

            for (int h = 0; h < MaxHop; h++)
            {
                Hidden[h] = new HiddenBatchData(maxBatchSize * maxCandidateNum, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Address[h] = new HiddenBatchData(maxBatchSize * HiddenSeqNum * maxCandidateNum, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Attention[h] = new HiddenBatchData(maxBatchSize * HiddenSeqNum * maxCandidateNum, 1, Behavior.RunMode, Behavior.Device);

                tmpBatchIdx[h] = new CudaPieceInt(maxBatchSize * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
                tmpSource[h] = new CudaPieceInt(maxBatchSize * HiddenSeqNum * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
                tmpTarget[h] = new CudaPieceInt(maxBatchSize * HiddenSeqNum * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
            }
            Z = new HiddenBatchData(maxBatchSize * maxCandidateNum, Model.MemoryDim, Behavior.RunMode, Behavior.Device);
            ZMode = 1;
        }

        public MLPAttentionV3Runner(MLPAttentionStructure model, HiddenBatchData[] memHidden, int maxHop, int maxBatchSize, int maxSentSize, RunnerBehavior behavior) :
            base(model, behavior)
        {
            Model = model;

            EnsembleMemHiddenRunner = new EnsembleMatrixRunner(memHidden.ToList(), behavior);
            HiddenSeqNum = memHidden.Length;
            MaxHop = maxHop;

            Hidden = new HiddenBatchData[MaxHop];
            Address = new HiddenBatchData[MaxHop];
            Attention = new HiddenBatchData[MaxHop];

            tmpBatchIdx = new CudaPieceInt[MaxHop];
            tmpSource = new CudaPieceInt[MaxHop];
            tmpTarget = new CudaPieceInt[MaxHop];

            for (int h = 0; h < MaxHop; h++)
            {
                Hidden[h] = new HiddenBatchData(maxBatchSize, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Address[h] = new HiddenBatchData(maxBatchSize * HiddenSeqNum, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Attention[h] = new HiddenBatchData(maxBatchSize * HiddenSeqNum, 1, Behavior.RunMode, Behavior.Device);

                tmpBatchIdx[h] = new CudaPieceInt(maxBatchSize, true, Behavior.Device == DeviceType.GPU);
                tmpSource[h] = new CudaPieceInt(maxBatchSize * HiddenSeqNum, true, Behavior.Device == DeviceType.GPU);
                tmpTarget[h] = new CudaPieceInt(maxBatchSize * HiddenSeqNum, true, Behavior.Device == DeviceType.GPU);
            }
            ZSeq = new SeqDenseBatchData(maxBatchSize, maxSentSize, Model.MemoryDim, Behavior.Device);
            ZMode = 0;
        }

        #region attention memory routing.

        public override void Forward()
        {
            EnsembleMemHiddenRunner.Forward();
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(ZSeq.SentDeriv, ZSeq.SentDeriv.Size);
            EnsembleMemHiddenRunner.CleanDeriv();
        }

        public override void Backward(bool cleanDeriv)
        {
            EnsembleMemHiddenRunner.Backward(cleanDeriv);
        }

        public override void Update()
        {
            if (!IsDelegateOptimizer)
            {
                if (Model.IsBias) Model.BMatrixOptimizer.BeforeGradient();
            }

            if (Model.IsBias)
            {
                ComputeLib.ColumnWiseSum(MemoryHidden.Deriv.Data, Model.BMatrixOptimizer.Gradient, MemoryHidden.BatchSize * HiddenSeqNum, Model.HiddenDim,
                    Model.BMatrixOptimizer.GradientStep);
            }
            
            if (!IsDelegateOptimizer)
            {
                if (Model.IsBias) Model.BMatrixOptimizer.AfterGradient();
            }
        }
        #endregion

        public override void AttentionForward(CudaPieceFloat input, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat output, int offsetOutput, int hopId, int batchSize)
        {
            // h = input * wi;
            ComputeLib.Sgemm(input, offsetInput, Model.Wi, 0, Hidden[hopId].Output.Data, 0, batchSize, Model.InputDim, Model.HiddenDim, 0, 1, false, false);

            for (int i = 0; i < batchSize; i++)
            {
                int bid = batchIdIdx[i + batchIdOffset];
                int smpBegin = bid * HiddenSeqNum;
                int smpEnd = (bid + 1) * HiddenSeqNum;

                int id = (i == 0 ? 0 : tmpBatchIdx[hopId].MemPtr[i - 1]);
                tmpBatchIdx[hopId].MemPtr[i] = id + HiddenSeqNum;

                for (int m = smpBegin; m < smpEnd; m++)
                {
                    tmpSource[hopId].MemPtr[id] = i;
                    tmpTarget[hopId].MemPtr[id] = m;
                    id++;
                }
            }

            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];
            tmpSource[hopId].SyncFromCPU(memorySize);
            tmpTarget[hopId].SyncFromCPU(memorySize);
            tmpBatchIdx[hopId].SyncFromCPU(batchSize);

            // Address = h + memoryH
            ComputeLib.Matrix_AdditionMask(Hidden[hopId].Output.Data, 0, tmpSource[hopId], 0,
                                           MemoryHidden.Output.Data, 0, tmpTarget[hopId], 0,
                                           Address[hopId].Output.Data, 0, CudaPieceInt.Empty, 0,
                                           Model.HiddenDim, memorySize, 1, 1, 0);

            // address = tanh(address)
            ComputeLib.Tanh(Address[hopId].Output.Data, 0, Address[hopId].Output.Data, 0, memorySize * Model.HiddenDim);

            // attention = address * Wa.
            ComputeLib.Sgemv(Address[hopId].Output.Data, Model.Wa, memorySize, Model.HiddenDim, Attention[hopId].Output.Data, false, 0, 1);

            // softmax of attention
            ComputeLib.SparseSoftmax(tmpBatchIdx[hopId], Attention[hopId].Output.Data, Attention[hopId].Output.Data, 1, batchSize);

            // attention memory 2 output.
            Behavior.Computelib.ColumnWiseSumMask(MemoryHidden.Output.Data, 0, tmpTarget[hopId], 0,
                Attention[hopId].Output.Data, tmpBatchIdx[hopId], batchSize,
                output, offsetOutput, CudaPieceInt.Empty, 0,
                memorySize, Model.HiddenDim, 0, 1);
        }

        public override void AttentionBackwardData(CudaPieceFloat inputDeriv, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize)
        {
            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];

            // outputDeriv -> memoryDeriv.
            Behavior.Computelib.Scale_MatrixMask(outputDeriv, offsetOutput, tmpSource[hopId], 0,
                                                MemoryHidden.Deriv.Data, 0, tmpTarget[hopId], 0,
                                                Model.MemoryDim, memorySize, Attention[hopId].Output.Data, 1);

            // outputDeriv -> attentionDeriv.
            Behavior.Computelib.Inner_Product_Matching(outputDeriv, offsetOutput, MemoryHidden.Output.Data, 0, Attention[hopId].Deriv.Data, 0,
                tmpSource[hopId], tmpTarget[hopId], batchSize, MemoryHidden.BatchSize * HiddenSeqNum, memorySize, Model.MemoryDim, Util.GPUEpsilon);

            // attentionDeriv = attentionDeriv * SoftMax(Attention)'
            Behavior.Computelib.DerivSparseMultiClassSoftmax(tmpBatchIdx[hopId], Attention[hopId].Output.Data, Attention[hopId].Deriv.Data, Attention[hopId].Deriv.Data, 1, batchSize);

            // addressDeriv = attentionDeriv * Wa'.
            Behavior.Computelib.Sgemm(Attention[hopId].Deriv.Data, 0, Model.Wa, 0, Address[hopId].Deriv.Data, 0, memorySize, 1, Model.HiddenDim, 0, 1, false, false);

            // addressDeriv = addressDeriv * tanh(address)'
            Behavior.Computelib.DerivTanh(Address[hopId].Output.Data, 0, Address[hopId].Deriv.Data, 0, Address[hopId].Deriv.Data, 0, memorySize * Model.HiddenDim, 0, 1);


            // hiddenDeriv = sum(addressDeriv)
            Behavior.Computelib.ColumnWiseSumMask(Address[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                CudaPieceFloat.Empty, tmpBatchIdx[hopId], batchSize,
                Hidden[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                memorySize, Model.HiddenDim, 0, 1);

            // inputDeriv += hiddenDeriv * wi'
            Behavior.Computelib.Sgemm(Hidden[hopId].Deriv.Data, 0,
                                          Model.Wi, 0,
                                          inputDeriv, offsetInput,
                                          batchSize, Model.HiddenDim, Model.InputDim, 1, 1, false, true);

            Behavior.Computelib.Matrix_AdditionMask(Address[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                    MemoryHidden.Deriv.Data, 0, tmpTarget[hopId], 0,
                                                    MemoryHidden.Deriv.Data, 0, tmpTarget[hopId], 0,
                                                    Model.HiddenDim, memorySize, 1, 1, 0);

        }

        public void AttentionBackwardWeight(CudaPieceFloat input, int offsetInput, CudaPieceInt inputMask, int inputMaskOffset, 
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize)
        {
            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];

            //  Wa  +=  attentionDeriv * address.
            Behavior.Computelib.Sgemm(Attention[hopId].Deriv.Data, 0, Address[hopId].Output.Data, 0, Model.WaMatrixOptimizer.Gradient, 0,
                memorySize, 1, Model.HiddenDim, 1, Model.WaMatrixOptimizer.GradientStep, true, false);

            Behavior.Computelib.Sgemm(input, offsetInput,
                                          Hidden[hopId].Deriv.Data, 0,
                                          Model.WiMatrixOptimizer.Gradient, 0,
                                          batchSize, Model.InputDim, Model.HiddenDim,
                                          1, Model.WiMatrixOptimizer.GradientStep, true, false);
        }


        public override void AttentionBackwardWeight(CudaPieceFloat init, CudaPieceFloat input, RecurrentSeqInfo HelpInput, int cellDim, CudaPieceFloat zDeriv, int attentionDim)
        {
            if (!IsDelegateOptimizer)
            {
                Model.WiMatrixOptimizer.BeforeGradient();
                Model.WaMatrixOptimizer.BeforeGradient();
            }
            int preStartIdx = -1;
            for (int i = 0; i < HelpInput.MaxLag; i++)
            {
                int startIdx = i == 0 ? 0 : HelpInput.LagSeqIdx[i - 1];
                int Sum = HelpInput.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;

                if (i > 0)
                {
                    AttentionBackwardWeight(input, preStartIdx * cellDim, CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
                            //HelpInput.LagSeqElement, startIdx,
                            zDeriv, startIdx * attentionDim, i, Sum);
                }
                else if (init != null)
                {
                    AttentionBackwardWeight(init, 0, CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
                            //HelpInput.LagSeqElement, 0,
                            zDeriv, 0, i, Sum);
                }
                preStartIdx = startIdx;
            }

            if (!IsDelegateOptimizer)
            {
                Model.WiMatrixOptimizer.AfterGradient();
                Model.WaMatrixOptimizer.AfterGradient();
            }
        }

    }

    public class MLPAttentionV4Runner : BasicMLPAttentionRunner
    {
        new MLPAttentionStructure Model { get; set; }

        EnsembleMatrixRunner EnsembleMemHiddenRunner = null;
        HiddenBatchData MemoryHidden { get { return EnsembleMemHiddenRunner.Output; } }
        int HiddenSeqNum { get; set; }

        EnsembleMatrixRunner EnsembleMemRunner = null;
        HiddenBatchData NewMemory { get { return EnsembleMemRunner.Output; } }

        int MaxHop { get; set; }

        public override HiddenBatchData Z { get; set; }
        protected  HiddenBatchData[] Attention { get; set; }
        // public override SeqDenseBatchData Memory { get; set; }
        public override SeqDenseBatchData ZSeq { get; set; }

        HiddenBatchData[] Hidden;
        HiddenBatchData[] Address;

        CudaPieceInt[] tmpBatchIdx;
        CudaPieceInt[] tmpSource;
        CudaPieceInt[] tmpTarget;

        public MLPAttentionV4Runner(MLPAttentionStructure model, HiddenBatchData[] mem, HiddenBatchData[] memHidden, int maxBatchSize,
            int maxCandidateNum, RunnerBehavior behavior) :
            base(model, behavior)
        {
            Model = model;
            //Memory = memory;
            //MemoryHidden = new SeqDenseBatchData(Memory.Stat.MAX_BATCHSIZE, Memory.Stat.MAX_SEQUENCESIZE, Model.HiddenDim, Behavior.Device);

            EnsembleMemRunner = new EnsembleMatrixRunner(mem.ToList(), behavior);
            EnsembleMemHiddenRunner = new EnsembleMatrixRunner(memHidden.ToList(), behavior);
            HiddenSeqNum = memHidden.Length;

            MaxHop = 1;

            Hidden = new HiddenBatchData[MaxHop];
            Address = new HiddenBatchData[MaxHop];
            Attention = new HiddenBatchData[MaxHop];

            tmpBatchIdx = new CudaPieceInt[MaxHop];
            tmpSource = new CudaPieceInt[MaxHop];
            tmpTarget = new CudaPieceInt[MaxHop];

            for (int h = 0; h < MaxHop; h++)
            {
                Hidden[h] = new HiddenBatchData(maxBatchSize * maxCandidateNum, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Address[h] = new HiddenBatchData(maxBatchSize * HiddenSeqNum * maxCandidateNum, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Attention[h] = new HiddenBatchData(maxBatchSize * HiddenSeqNum * maxCandidateNum, 1, Behavior.RunMode, Behavior.Device);

                tmpBatchIdx[h] = new CudaPieceInt(maxBatchSize * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
                tmpSource[h] = new CudaPieceInt(maxBatchSize * HiddenSeqNum * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
                tmpTarget[h] = new CudaPieceInt(maxBatchSize * HiddenSeqNum * maxCandidateNum, true, Behavior.Device == DeviceType.GPU);
            }
            Z = new HiddenBatchData(maxBatchSize * maxCandidateNum, Model.MemoryDim, Behavior.RunMode, Behavior.Device);
            ZMode = 1;
        }

        public MLPAttentionV4Runner(MLPAttentionStructure model, HiddenBatchData[] mem, HiddenBatchData[] memHidden, int maxHop,
            int maxBatchSize, int maxSentSize, RunnerBehavior behavior) :
            base(model, behavior)
        {
            Model = model;

            EnsembleMemRunner = new EnsembleMatrixRunner(mem.ToList(), behavior);
            EnsembleMemHiddenRunner = new EnsembleMatrixRunner(memHidden.ToList(), behavior);
            HiddenSeqNum = memHidden.Length;
            MaxHop = maxHop;

            Hidden = new HiddenBatchData[MaxHop];
            Address = new HiddenBatchData[MaxHop];
            Attention = new HiddenBatchData[MaxHop];

            tmpBatchIdx = new CudaPieceInt[MaxHop];
            tmpSource = new CudaPieceInt[MaxHop];
            tmpTarget = new CudaPieceInt[MaxHop];

            for (int h = 0; h < MaxHop; h++)
            {
                Hidden[h] = new HiddenBatchData(maxBatchSize, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Address[h] = new HiddenBatchData(maxBatchSize * HiddenSeqNum, Model.HiddenDim, Behavior.RunMode, Behavior.Device);
                Attention[h] = new HiddenBatchData(maxBatchSize * HiddenSeqNum, 1, Behavior.RunMode, Behavior.Device);

                tmpBatchIdx[h] = new CudaPieceInt(maxBatchSize, true, Behavior.Device == DeviceType.GPU);
                tmpSource[h] = new CudaPieceInt(maxBatchSize * HiddenSeqNum, true, Behavior.Device == DeviceType.GPU);
                tmpTarget[h] = new CudaPieceInt(maxBatchSize * HiddenSeqNum, true, Behavior.Device == DeviceType.GPU);
            }
            ZSeq = new SeqDenseBatchData(maxBatchSize, maxSentSize, Model.MemoryDim, Behavior.Device);
            ZMode = 0;
        }

        #region attention memory routing.

        public override void Forward()
        {
            EnsembleMemRunner.Forward();
            EnsembleMemHiddenRunner.Forward();
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(ZSeq.SentDeriv, ZSeq.SentDeriv.Size);
            EnsembleMemRunner.CleanDeriv();
            EnsembleMemHiddenRunner.CleanDeriv();
        }

        public override void Backward(bool cleanDeriv)
        {
            EnsembleMemRunner.Backward(cleanDeriv);
            EnsembleMemHiddenRunner.Backward(cleanDeriv);
        }

        public override void Update()
        {
        }
        #endregion

        public override void AttentionForward(CudaPieceFloat input, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat output, int offsetOutput, int hopId, int batchSize)
        {
            // h = input * wi;
            ComputeLib.Sgemm(input, offsetInput, Model.Wi, 0, Hidden[hopId].Output.Data, 0,
                    batchSize, Model.InputDim, Model.HiddenDim, 0, 1, false, false);

            for (int i = 0; i < batchSize; i++)
            {
                int bid = batchIdIdx[i + batchIdOffset];
                int smpBegin = bid * HiddenSeqNum;
                int smpEnd = (bid + 1) * HiddenSeqNum;

                int id = (i == 0 ? 0 : tmpBatchIdx[hopId].MemPtr[i - 1]);
                tmpBatchIdx[hopId].MemPtr[i] = id + HiddenSeqNum;

                for (int m = smpBegin; m < smpEnd; m++)
                {
                    tmpTarget[hopId].MemPtr[id] = m;
                    tmpSource[hopId].MemPtr[id] = i;
                    id++;
                }
            }

            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];
            tmpSource[hopId].SyncFromCPU(memorySize);
            tmpTarget[hopId].SyncFromCPU(memorySize);
            tmpBatchIdx[hopId].SyncFromCPU(batchSize);

            // Address = h + memoryH
            Behavior.Computelib.Matrix_AdditionMask(Hidden[hopId].Output.Data, 0, tmpSource[hopId], 0,
                                                    MemoryHidden.Output.Data, 0, tmpTarget[hopId], 0,
                                                    Address[hopId].Output.Data, 0, CudaPieceInt.Empty, 0,
                                                    Model.HiddenDim, memorySize, 1, 1, 0);

            // address = tanh(address)
            Behavior.Computelib.Tanh(Address[hopId].Output.Data, 0, Address[hopId].Output.Data, 0, memorySize * Model.HiddenDim);

            // attention = address * Wa.
            Behavior.Computelib.Sgemv(Address[hopId].Output.Data, Model.Wa, memorySize, Model.HiddenDim, Attention[hopId].Output.Data, false, 0, 1);

            // softmax of attention
            Behavior.Computelib.SparseSoftmax(tmpBatchIdx[hopId], Attention[hopId].Output.Data, Attention[hopId].Output.Data, 1, batchSize);

            // attention memory 2 output.
            Behavior.Computelib.ColumnWiseSumMask(NewMemory.Output.Data, 0, tmpTarget[hopId], 0,
                Attention[hopId].Output.Data, tmpBatchIdx[hopId], batchSize,
                output, offsetOutput, CudaPieceInt.Empty, 0,
                memorySize, Model.MemoryDim, 0, 1);
        }

        public override void AttentionBackwardData(CudaPieceFloat inputDeriv, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize)
        {
            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];

            // outputDeriv -> memoryDeriv.
            Behavior.Computelib.Scale_MatrixMask(outputDeriv, offsetOutput, tmpSource[hopId], 0,
                                                NewMemory.Deriv.Data, 0, tmpTarget[hopId], 0,
                                                Model.MemoryDim, memorySize, Attention[hopId].Output.Data, 1);

            // outputDeriv -> attentionDeriv.
            Behavior.Computelib.Inner_Product_Matching(outputDeriv, offsetOutput, NewMemory.Output.Data, 0, Attention[hopId].Deriv.Data, 0,
                tmpSource[hopId], tmpTarget[hopId], batchSize, MemoryHidden.BatchSize * HiddenSeqNum, memorySize, Model.MemoryDim, Util.GPUEpsilon);

            // attentionDeriv = attentionDeriv * SoftMax(Attention)'
            Behavior.Computelib.DerivSparseMultiClassSoftmax(tmpBatchIdx[hopId], Attention[hopId].Output.Data, Attention[hopId].Deriv.Data, Attention[hopId].Deriv.Data, 1, batchSize);

            // addressDeriv = attentionDeriv * Wa'.
            Behavior.Computelib.Sgemm(Attention[hopId].Deriv.Data, 0, Model.Wa, 0, Address[hopId].Deriv.Data, 0, memorySize, 1, Model.HiddenDim, 0, 1, false, false);

            // addressDeriv = addressDeriv * tanh(address)'
            Behavior.Computelib.DerivTanh(Address[hopId].Output.Data, 0, Address[hopId].Deriv.Data, 0, Address[hopId].Deriv.Data, 0, memorySize * Model.HiddenDim, 0, 1);


            // hiddenDeriv = sum(addressDeriv)
            Behavior.Computelib.ColumnWiseSumMask(Address[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                CudaPieceFloat.Empty, tmpBatchIdx[hopId], batchSize,
                Hidden[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                memorySize, Model.HiddenDim, 0, 1);

            // inputDeriv += hiddenDeriv * wi'
            Behavior.Computelib.Sgemm(Hidden[hopId].Deriv.Data, 0,
                                          Model.Wi, 0,
                                          inputDeriv, offsetInput,
                                          batchSize, Model.HiddenDim, Model.InputDim, 1, 1, false, true);

            Behavior.Computelib.Matrix_AdditionMask(Address[hopId].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                    MemoryHidden.Deriv.Data, 0, tmpTarget[hopId], 0,
                                                    MemoryHidden.Deriv.Data, 0, tmpTarget[hopId], 0,
                                                    Model.HiddenDim, memorySize, 1, 1, 0);

        }

        public void AttentionBackwardWeight(CudaPieceFloat input, int offsetInput, CudaPieceInt inputMask, int inputMaskOffset, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize)
        {
            int memorySize = tmpBatchIdx[hopId].MemPtr[batchSize - 1];

            //  Wa  +=  attentionDeriv * address.
            Behavior.Computelib.Sgemm(Attention[hopId].Deriv.Data, 0, Address[hopId].Output.Data, 0, Model.WaMatrixOptimizer.Gradient, 0,
                memorySize, 1, Model.HiddenDim, 1, Model.WaMatrixOptimizer.GradientStep, true, false);

            Behavior.Computelib.Sgemm(input, offsetInput,
                                          Hidden[hopId].Deriv.Data, 0,
                                          Model.WiMatrixOptimizer.Gradient, 0,
                                          batchSize, Model.InputDim, Model.HiddenDim,
                                          1, Model.WiMatrixOptimizer.GradientStep, true, false);
        }


        public override void AttentionBackwardWeight(CudaPieceFloat init, CudaPieceFloat input, RecurrentSeqInfo HelpInput, int cellDim, CudaPieceFloat zDeriv, int attentionDim)
        {
            if (!IsDelegateOptimizer)
            {
                Model.WiMatrixOptimizer.BeforeGradient();
                Model.WaMatrixOptimizer.BeforeGradient();
            }
            int preStartIdx = -1;
            for (int i = 0; i < HelpInput.MaxLag; i++)
            {
                int startIdx = i == 0 ? 0 : HelpInput.LagSeqIdx[i - 1];
                int Sum = HelpInput.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;

                if (i > 0)
                {
                    AttentionBackwardWeight(input, preStartIdx * cellDim, CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
                            HelpInput.LagSeqElement, startIdx,
                            zDeriv, startIdx * attentionDim, i, Sum);
                }
                else if (init != null)
                {
                    AttentionBackwardWeight(init, 0, CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
                            HelpInput.LagSeqElement, 0,
                            zDeriv, 0, i, Sum);
                }
                preStartIdx = startIdx;
            }

            if (!IsDelegateOptimizer)
            {
                Model.WiMatrixOptimizer.AfterGradient();
                Model.WaMatrixOptimizer.AfterGradient();
            }
        }
    }

    public class EnsembleAttentionRunner : BasicMLPAttentionRunner
    {
        public override void AttentionForward(CudaPieceFloat input, int offsetInput, int[] batchIdIdx, int batchIdOffset, CudaPieceFloat output, int offsetOutput, int hopId, int batchSize)
        {
            int offsetOutputIdx = offsetOutput / ZMemory.Column;
            int offsetDim = 0;
            foreach (BasicMLPAttentionRunner runner in AttRunners)
            {
                runner.AttentionForward(input, offsetInput, batchIdIdx, batchIdOffset, runner.ZMemory.Output, offsetOutputIdx * runner.ZMemory.Column, hopId, batchSize);

                ComputeLib.Matrix_AdditionEx(runner.ZMemory.Output, offsetOutputIdx * runner.ZMemory.Column, runner.ZMemory.Column,
                                             ZMemory.Output, offsetOutput + offsetDim, ZMemory.Column,
                                             ZMemory.Output, offsetOutput + offsetDim, ZMemory.Column,
                                             runner.ZMemory.Column, batchSize, 1, 0, 0);
                offsetDim += runner.ZMemory.Column;
            }

        }

        public override void AttentionBackwardData(CudaPieceFloat inputDeriv, int offsetInput, int[] batchIdIdx, int batchIdOffset,
            CudaPieceFloat outputDeriv, int offsetOutput, int hopId, int batchSize)
        {
            int offsetOutputIdx = offsetOutput / ZMemory.Column;
            int offsetDim = 0;

            foreach (BasicMLPAttentionRunner runner in AttRunners)
            {
                ComputeLib.Matrix_AdditionEx(ZMemory.Deriv, offsetOutput + offsetDim, ZMemory.Column,
                                             runner.ZMemory.Deriv, offsetOutputIdx * runner.ZMemory.Column, runner.ZMemory.Column,
                                             runner.ZMemory.Deriv, offsetOutputIdx * runner.ZMemory.Column, runner.ZMemory.Column,
                                             runner.ZMemory.Column, batchSize, 1, 0, 1); 
                                             
                runner.AttentionBackwardData(inputDeriv, offsetInput, batchIdIdx, batchIdOffset, runner.ZMemory.Deriv, offsetOutputIdx * runner.ZMemory.Column, hopId, batchSize);
                offsetDim += runner.ZMemory.Column;
            }
        }

        public override void AttentionBackwardWeight(CudaPieceFloat init, CudaPieceFloat input, RecurrentSeqInfo HelpInput, int cellDim, CudaPieceFloat zDeriv, int attentionDim)
        {
            foreach (BasicMLPAttentionRunner runner in AttRunners)
            {
                runner.AttentionBackwardWeight(init, input, HelpInput, cellDim, runner.ZMemory.Deriv, runner.ZMemory.Column); // Data(inputDeriv, offsetInput, batchIdIdx, batchIdOffset, runner.ZMemory.Deriv, offsetOutputIdx * runner.ZMemory.Column, hopId, batchSize);
            }
        }
        public override SeqDenseBatchData ZSeq { get; set; }

        public override HiddenBatchData Z { get; set; }

        List<BasicMLPAttentionRunner> AttRunners = null;

        public EnsembleAttentionRunner(List<BasicMLPAttentionRunner> attRunners, int maxBatchSize, int maxCandidateNum, int memoryDim, RunnerBehavior behavior) :
            base(Structure.Empty, behavior)
        {
            AttRunners = attRunners;
            Z = new HiddenBatchData(maxBatchSize * maxCandidateNum, memoryDim, Behavior.RunMode, Behavior.Device);
            ZMode = 1;
        }

        public EnsembleAttentionRunner(List<BasicMLPAttentionRunner> attRunners, int maxHop, int maxBatchSize, int maxSentSize, int memoryDim, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            AttRunners = attRunners;
            ZSeq = new SeqDenseBatchData(maxBatchSize, maxSentSize, memoryDim, Behavior.Device);
            ZMode = 0;
        }

        #region attention memory routing.
        public override void Forward()
        {
            foreach (BasicMLPAttentionRunner runner in AttRunners)
            {
                if(runner.IsDelegate) runner.Forward();
            }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(ZSeq.SentDeriv, ZSeq.SentDeriv.Size);
            foreach (BasicMLPAttentionRunner runner in AttRunners)
            {
                if (runner.IsDelegate) runner.CleanDeriv();
            }
        }

        public override void Backward(bool cleanDeriv)
        {
            foreach (BasicMLPAttentionRunner runner in Enumerable.Reverse(AttRunners))
            {
                if (runner.IsDelegate) runner.Backward(cleanDeriv);
            }
        }

        public override void Update()
        {
            foreach (BasicMLPAttentionRunner runner in AttRunners)
            {
                if (runner.IsDelegate) runner.Update();
            }
        }
        #endregion
    }

    public enum CrossSimType { Addition = 0, Product = 1, Cosine = 2, SubCosine = 3,  };

    public class SoftLinearSimRunner : StructRunner
    {
        HiddenBatchData Status { get; set; }

        SeqDenseBatchData Mem { get; set; }

        BiMatchBatchData MatchData { get; set; }

        HiddenBatchData HiddenStatus = null;
        HiddenBatchData HiddenMatch = null;
        CudaPieceFloat HiddenMatchTmp = null;

        MLPAttentionStructure AttStruct;

        /// <summary>
        /// softmax on address given query.
        /// </summary>
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        CrossSimType AttType = CrossSimType.Addition;
        A_Func AttAf = A_Func.Tanh;

        SimilarityRunner SimRunner = null;

        public SoftLinearSimRunner(HiddenBatchData status, SeqDenseBatchData mem, BiMatchBatchData matchData, CrossSimType attType,
            MLPAttentionStructure attStruct, RunnerBehavior behavior, A_Func afunc = A_Func.Tanh) : base(Structure.Empty, behavior)
        {
            Status = status;
            Mem = mem;
            MatchData = matchData;

            AttType = attType;
            AttStruct = attStruct;
            AttAf = afunc;

            HiddenStatus = new HiddenBatchData(status.MAX_BATCHSIZE, AttStruct.HiddenDim, Behavior.RunMode, Behavior.Device);

            switch (AttType)
            {
                case CrossSimType.Product:
                case CrossSimType.Addition:
                    HiddenMatchTmp = new CudaPieceFloat(MatchData.Stat.MAX_MATCH_BATCHSIZE * AttStruct.HiddenDim, true, Behavior.Device == DeviceType.GPU);
                    HiddenMatch = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, AttStruct.HiddenDim, Behavior.RunMode, Behavior.Device);
                    Output = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                    break;
                case CrossSimType.SubCosine:
                    SimRunner = new SimilarityRunner(HiddenStatus, new HiddenBatchData(mem.MAX_SENTSIZE, mem.Dim, mem.SentOutput, mem.SentDeriv, Behavior.Device),
                        MatchData, SimilarityType.CosineSimilarity, Behavior);
                    Output = SimRunner.Output;
                    break;
            }
        }

        public override void Forward()
        {
            HiddenStatus.BatchSize = Status.BatchSize;
            ComputeLib.Sgemm(Status.Output.Data, 0, AttStruct.Wi, 0, HiddenStatus.Output.Data, 0, Status.BatchSize, AttStruct.InputDim, AttStruct.HiddenDim, 0, 1, false, false);

            switch (AttType)
            {
                case CrossSimType.Addition:
                case CrossSimType.Product:
                    if (AttType == CrossSimType.Addition)
                    {
                        // hiddenMatch = hiddenStatus + Mem
                        ComputeLib.Matrix_AdditionMask(HiddenStatus.Output.Data, 0, MatchData.SrcIdx, 0,
                                                       Mem.SentOutput, 0, MatchData.TgtIdx, 0,
                                                       HiddenMatch.Output.Data, 0, CudaPieceInt.Empty, 0,
                                                       AttStruct.HiddenDim, MatchData.MatchSize, 1, 1, 0);
                    }
                    else if (AttType == CrossSimType.Product)
                    {
                        ComputeLib.ElementwiseProductMask(HiddenStatus.Output.Data, 0,
                                                          Mem.SentOutput, 0,
                                                          HiddenMatch.Output.Data, 0,
                                                          MatchData.SrcIdx, 0, MatchData.TgtIdx, 0, CudaPieceInt.Empty, 0,
                                                          MatchData.MatchSize, AttStruct.HiddenDim, 0, 1);
                    }
                    ActivationFuncLib.ActivationFunc(ComputeLib, HiddenMatch.Output.Data, MatchData.MatchSize, AttStruct.HiddenDim, A_Func.Tanh, AttStruct.B);
                    //ComputeLib.Tanh(HiddenMatch.Output.Data, 0, HiddenMatch.Output.Data, 0, AttStruct.HiddenDim * MatchData.MatchSize);
                    Output.BatchSize = MatchData.MatchSize;

                    // attention = address * Wa.
                    ComputeLib.Sgemv(HiddenMatch.Output.Data, AttStruct.Wa, MatchData.MatchSize, HiddenMatch.Dim, Output.Output.Data, false, 0, 1);

                    ActivationFuncLib.ActivationFunc(ComputeLib, Output.Output.Data, Output.BatchSize, 1, AttAf, AttStruct.B);
                    //ComputeLib.Tanh(Output.Output.Data, 0, Output.Output.Data, 0, Output.BatchSize);
                    break;
                case CrossSimType.SubCosine:
                    SimRunner.Forward();
                    break;
            }
        }

        public override void CleanDeriv()
        {
            if (AttType == CrossSimType.SubCosine)
            {
                ComputeLib.Zero(HiddenStatus.Deriv.Data, HiddenStatus.BatchSize * HiddenStatus.Dim);
            }
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            switch (AttType)
            {
                case CrossSimType.Addition:
                case CrossSimType.Product:
                    //ComputeLib.DerivTanh(Output.Output.Data, 0, Output.Deriv.Data, 0, Output.Deriv.Data, 0, Output.BatchSize, 0, 1);
                    ActivationFuncLib.DerivActivationFunc(ComputeLib, Output.Deriv.Data, Output.Output.Data, Output.BatchSize, 1, AttAf);

                    // HiddenOutput = attentionDeriv * Wa'.
                    ComputeLib.Sgemm(Output.Deriv.Data, 0, AttStruct.Wa, 0, HiddenMatch.Deriv.Data, 0, MatchData.MatchSize, 1, HiddenMatch.Dim, 0, 1, false, false);

                    // HiddenOutput = HiddenOutput * tanh(address)'
                    //ComputeLib.DerivTanh(HiddenMatch.Output.Data, 0, HiddenMatch.Deriv.Data, 0, HiddenMatch.Deriv.Data, 0, MatchData.MatchSize * HiddenMatch.Dim, 0, 1);
                    ActivationFuncLib.DerivActivationFunc(ComputeLib, HiddenMatch.Deriv.Data, HiddenMatch.Output.Data, MatchData.MatchSize, HiddenMatch.Dim, A_Func.Tanh);

                    if (AttType == CrossSimType.Addition)
                    {
                        // hiddenDeriv = sum(addressDeriv)
                        ComputeLib.Matrix_AdditionMask(HiddenMatch.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                       Mem.SentDeriv, 0, MatchData.TgtIdx, 0,
                                                       Mem.SentDeriv, 0, MatchData.TgtIdx, 0,
                                                       Mem.Dim, MatchData.MatchSize, 1, 1, 0);

                        //HiddenStatus.Deriv.Data.SyncToCPU();
                        ComputeLib.ColumnWiseSumMask(HiddenMatch.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                              CudaPieceFloat.Empty, MatchData.Src2MatchIdx, MatchData.SrcSize,
                                                              HiddenStatus.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                              MatchData.MatchSize, HiddenStatus.Dim, 0, 1);
                    }
                    else if (AttType == CrossSimType.Product)
                    {
                        ComputeLib.ElementwiseProductMask(HiddenMatch.Deriv.Data, 0,
                                                          HiddenStatus.Output.Data, 0,
                                                          Mem.SentDeriv, 0,
                                                          CudaPieceInt.Empty, 0, MatchData.SrcIdx, 0, MatchData.TgtIdx, 0,
                                                          MatchData.MatchSize, Mem.Dim, 1, 1);

                        ComputeLib.ElementwiseProductMask(HiddenMatch.Deriv.Data, 0,
                                                          Mem.SentOutput, 0,
                                                          HiddenMatchTmp, 0,
                                                          CudaPieceInt.Empty, 0, MatchData.TgtIdx, 0, CudaPieceInt.Empty, 0,
                                                          MatchData.MatchSize, Mem.Dim, 0, 1);

                        ComputeLib.ColumnWiseSumMask(HiddenMatchTmp, 0, CudaPieceInt.Empty, 0,
                                                              CudaPieceFloat.Empty, MatchData.Src2MatchIdx, MatchData.SrcSize,
                                                              HiddenStatus.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                              MatchData.MatchSize, HiddenStatus.Dim, 0, 1);
                    }
                    break;
                case CrossSimType.SubCosine:
                    SimRunner.Backward(cleanDeriv);
                    break;
            }

            //float hiddenStatusDerivNorm = ComputeLib.L2Norm(HiddenStatus.Deriv.Data, HiddenStatus.Dim * MatchData.SrcSize);
            //Console.WriteLine("hidden status deriv norm {0}, len {1}, avg {2}", hiddenStatusDerivNorm, HiddenStatus.Dim * MatchData.SrcSize,
            //    hiddenStatusDerivNorm / Math.Sqrt(HiddenStatus.Dim * MatchData.SrcSize));
            //HiddenStatus.Deriv.Data.SyncToCPU();

            // inputDeriv += hiddenDeriv * wi'
            ComputeLib.Sgemm(HiddenStatus.Deriv.Data, 0,
                                      AttStruct.Wi, 0,
                                      Status.Deriv.Data, 0,
                                      Status.BatchSize, AttStruct.HiddenDim, AttStruct.InputDim, 1, 1, false, true);

            //Status.Deriv.Data.SyncToCPU();
            //Mem.SentDeriv.SyncToCPU();
        }

        public override void Update()
        {
            if (!IsDelegateOptimizer)
            {
                AttStruct.WiMatrixOptimizer.BeforeGradient();
                AttStruct.WaMatrixOptimizer.BeforeGradient();
            }

            if (AttType == CrossSimType.Addition || AttType == CrossSimType.Product)
            {
                //  Wa  +=  attentionDeriv * address.
                ComputeLib.Sgemm(Output.Deriv.Data, 0, HiddenMatch.Output.Data, 0, AttStruct.WaMatrixOptimizer.Gradient, 0,
                                          1, MatchData.MatchSize, HiddenMatch.Dim, 1, AttStruct.WaMatrixOptimizer.GradientStep, false, false);
            }

            ComputeLib.Sgemm(Status.Output.Data, 0,
                                      HiddenStatus.Deriv.Data, 0,
                                      AttStruct.WiMatrixOptimizer.Gradient, 0,
                                      Status.BatchSize, AttStruct.InputDim, AttStruct.HiddenDim,
                                      1, AttStruct.WiMatrixOptimizer.GradientStep, true, false);
            if (!IsDelegateOptimizer)
            {
                AttStruct.WiMatrixOptimizer.AfterGradient();
                AttStruct.WaMatrixOptimizer.AfterGradient();
            }
        }
    }

    public class SoftSeqLinearSimRunner : StructRunner
    {
        SeqDenseBatchData Src { get; set; }

        SeqDenseBatchData Mem { get; set; }

        public BiMatchBatchData MatchData { get; set; }

        HiddenBatchData HiddenStatus = null;

        LayerStructure AttStruct = null;

        /// <summary>
        /// softmax on address given query.
        /// </summary>
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        CudaPieceInt EnsembleMask;
        CudaPieceInt NewEnsembleMask;
        CudaPieceFloat tmpHidden;
        public SoftSeqLinearSimRunner(SeqDenseBatchData src, SeqDenseBatchData mem, int maxSrcTgtCrossLen, //BiMatchBatchData matchData, //CrossSimType attType,
            LayerStructure attStruct, RunnerBehavior behavior) : base(Structure.Empty, behavior)  //, A_Func afunc = A_Func.Tanh
        {
            Src = src;
            Mem = mem;
            MatchData = new BiMatchBatchData(new BiMatchBatchDataStat()
            {
                MAX_SRC_BATCHSIZE = Src.Stat.MAX_SEQUENCESIZE,
                MAX_TGT_BATCHSIZE = Mem.Stat.MAX_SEQUENCESIZE,
                MAX_MATCH_BATCHSIZE = maxSrcTgtCrossLen,
            }, Behavior.Device);

            AttStruct = attStruct;

            EnsembleMask = new CudaPieceInt(MatchData.Stat.MAX_MATCH_BATCHSIZE * 3, true, Behavior.Device == DeviceType.GPU);
            for (int i = 0; i < 3; i++)
                for (int s = 0; s < MatchData.Stat.MAX_MATCH_BATCHSIZE; s++)
                    EnsembleMask.MemPtr[i * MatchData.Stat.MAX_MATCH_BATCHSIZE + s] = (i + s * 3);
            EnsembleMask.SyncFromCPU(MatchData.Stat.MAX_MATCH_BATCHSIZE * 3);

            NewEnsembleMask = new CudaPieceInt(MatchData.Stat.MAX_MATCH_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);

            HiddenStatus = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, Src.Dim + Mem.Dim + Src.Dim, Behavior.RunMode, Behavior.Device);
            tmpHidden = new CudaPieceFloat(MatchData.Stat.MAX_MATCH_BATCHSIZE * Src.Dim, true, Behavior.Device == DeviceType.GPU);
            Output = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
        }


        public override void Forward()
        {
            Src.SampleIdx.SyncToCPU(Src.BatchSize);
            Mem.SampleIdx.SyncToCPU(Mem.BatchSize);
            List<Tuple<int, int, float>> matchList = new List<Tuple<int, int, float>>();
            for (int i = 0; i < Src.BatchSize; i++)
            {
                int qmemBgn = i == 0 ? 0 : Src.SampleIdx.MemPtr[i - 1];
                int qmemEnd = Src.SampleIdx.MemPtr[i];

                int dmemBgn = i == 0 ? 0 : Mem.SampleIdx.MemPtr[i - 1];
                int dmemEnd = Mem.SampleIdx.MemPtr[i];

                for (int q = qmemBgn; q < qmemEnd; q++)
                {
                    for (int d = dmemBgn; d < dmemEnd; d++)
                    {
                        matchList.Add(new Tuple<int, int, float>(q, d, 1));
                    }
                }
            }
            MatchData.SetMatch(matchList);
            HiddenStatus.BatchSize = MatchData.MatchSize;

            ComputeLib.Matrix_AdditionMask(Src.SentOutput, 0, MatchData.SrcIdx, 0,
                                           Src.SentOutput, 0, MatchData.SrcIdx, 0,
                                           HiddenStatus.Output.Data, 0, EnsembleMask, 0,
                                           Src.Dim, MatchData.MatchSize, 1, 0, 0);

            ComputeLib.Matrix_AdditionMask(Mem.SentOutput, 0, MatchData.TgtIdx, 0,
                                           Mem.SentOutput, 0, MatchData.TgtIdx, 0,
                                           HiddenStatus.Output.Data, 0, EnsembleMask, MatchData.Stat.MAX_MATCH_BATCHSIZE,
                                           Mem.Dim, MatchData.MatchSize, 1, 0, 0);

            ComputeLib.ElementwiseProductMask(Src.SentOutput, 0, Mem.SentOutput, 0, HiddenStatus.Output.Data, 0,
                                              MatchData.SrcIdx, 0, MatchData.TgtIdx, 0,
                                              EnsembleMask, 2 * MatchData.Stat.MAX_MATCH_BATCHSIZE,
                                              MatchData.MatchSize, Mem.Dim, 0, 1);

            Output.BatchSize = MatchData.MatchSize;
            ComputeLib.Sgemm(HiddenStatus.Output.Data, 0, AttStruct.weight, 0, Output.Output.Data, 0, MatchData.MatchSize, HiddenStatus.Dim, 1, 0, 1, false, false);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize);
        }

        public override void Backward(bool cleanDeriv)
        {
            
            ComputeLib.Sgemm(Output.Deriv.Data, 0, AttStruct.weight, 0, HiddenStatus.Deriv.Data, 0, MatchData.MatchSize, 1, HiddenStatus.Dim, 0, 1, false, false);

            //Src.SentDeriv.Zero();
            //Mem.SentDeriv.Zero();
            ComputeLib.ElementwiseProductMask(HiddenStatus.Deriv.Data, 0, Src.SentOutput, 0, tmpHidden, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask, 2 * MatchData.Stat.MAX_MATCH_BATCHSIZE, MatchData.SrcIdx, 0, CudaPieceInt.Empty, 0,
                                              MatchData.MatchSize, Mem.Dim, 0, 1);

            ComputeLib.ColumnWiseSumMask(tmpHidden, 0, MatchData.Tgt2MatchElement, 0, CudaPieceFloat.Empty,
                                            MatchData.Tgt2MatchIdx, MatchData.TgtSize, Mem.SentDeriv, 0, CudaPieceInt.Empty, 0, MatchData.MatchSize, Src.Dim, 1, 1);

            //ComputeLib.AccurateElementwiseProduct(HiddenStatus.Deriv.Data, 0, Src.SentOutput, 0, Mem.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
            //                                  EnsembleMask, 2 * MatchData.Stat.MAX_MATCH_BATCHSIZE, MatchData.SrcIdx, 0, MatchData.TgtIdx, 0,
            //                                  MatchData.MatchSize, Mem.Dim, 1);

            ComputeLib.ElementwiseProductMask(HiddenStatus.Deriv.Data, 0, Mem.SentOutput, 0, tmpHidden, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask, 2 * MatchData.Stat.MAX_MATCH_BATCHSIZE, MatchData.TgtIdx, 0, CudaPieceInt.Empty, 0,
                                              MatchData.MatchSize, Src.Dim, 0, 1);

            ComputeLib.ColumnWiseSumMask(tmpHidden, 0, MatchData.Src2MatchElement, 0, CudaPieceFloat.Empty,
                                            MatchData.Src2MatchIdx, MatchData.SrcSize, Src.SentDeriv, 0, CudaPieceInt.Empty, 0, MatchData.MatchSize, Src.Dim, 1, 1);

            //ComputeLib.AccurateElementwiseProduct(HiddenStatus.Deriv.Data, 0, Mem.SentOutput, 0, Src.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
            //                                  EnsembleMask, 2 * MatchData.Stat.MAX_MATCH_BATCHSIZE, MatchData.TgtIdx, 0, MatchData.SrcIdx, 0,
            //                                  MatchData.MatchSize, Src.Dim, 1);


            for (int i = 0; i < MatchData.MatchSize; i++)
            {
                int newMatch = MatchData.Tgt2MatchElement.MemPtr[i];
                int newPos = EnsembleMask.MemPtr[newMatch + MatchData.Stat.MAX_MATCH_BATCHSIZE];
                NewEnsembleMask.MemPtr[i] = newPos;
            }
            NewEnsembleMask.SyncFromCPU(MatchData.MatchSize);
            ComputeLib.ColumnWiseSumMask(HiddenStatus.Deriv.Data, 0, NewEnsembleMask, 0, CudaPieceFloat.Empty,
                                         MatchData.Tgt2MatchIdx, MatchData.TgtSize, Mem.SentDeriv, 0, CudaPieceInt.Empty, 0, MatchData.MatchSize, Mem.Dim, 1, 1);

            //ComputeLib.AccurateScale_Matrix(HiddenStatus.Deriv.Data, 0, EnsembleMask, MatchData.Stat.MAX_MATCH_BATCHSIZE, 
            //                               Mem.SentDeriv, 0, MatchData.TgtIdx, 0,
            //                               Mem.Dim, MatchData.MatchSize, CudaPieceFloat.Empty);


            ComputeLib.ColumnWiseSumMask(HiddenStatus.Deriv.Data, 0, EnsembleMask, 0, CudaPieceFloat.Empty,
                                         MatchData.Src2MatchIdx, MatchData.SrcSize, Src.SentDeriv, 0, CudaPieceInt.Empty, 0, MatchData.MatchSize, Src.Dim, 1, 1);
                                         
            //ComputeLib.AccurateScale_Matrix(HiddenStatus.Deriv.Data, 0, EnsembleMask, 0,
            //                               Src.SentDeriv, 0, MatchData.SrcIdx, 0,
            //                               Src.Dim, MatchData.MatchSize, CudaPieceFloat.Empty);
            
        }

        public override void Update()
        {
            if (!IsDelegateOptimizer) { AttStruct.WeightOptimizer.BeforeGradient(); }
            ComputeLib.Sgemm(Output.Deriv.Data, 0, HiddenStatus.Output.Data, 0, AttStruct.WeightGrad, 0,
                                      1, MatchData.MatchSize, HiddenStatus.Dim, 1, AttStruct.WeightOptimizer.GradientStep, false, false);
            if (!IsDelegateOptimizer) { AttStruct.WeightOptimizer.AfterGradient(); }
        }
    }

    public class SeqAlignmentRunner : StructRunner
    {
        SeqDenseBatchData Src { get; set; }

        SeqDenseBatchData Mem { get; set; }

        public BiMatchBatchData MatchData { get; set; }

        LayerStructure AttStruct = null;

        //HiddenBatchData HiddenStatus = null;

        HiddenBatchData SrcScore = null;
        HiddenBatchData MemScore = null;

        /// <summary>
        /// softmax on address given query.
        /// </summary>
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }


        /// <summary>
        /// q_i, d_j
        /// </summary>
        /// <param name="src"></param>
        /// <param name="mem"></param>
        /// <param name="match"></param>
        /// <param name="attStruct"></param>
        /// <param name="behavior"></param>
        public SeqAlignmentRunner(SeqDenseBatchData src, SeqDenseBatchData mem, BiMatchBatchData match, // int maxSrcTgtCrossLen, //BiMatchBatchData matchData, //CrossSimType attType,
            LayerStructure attStruct, RunnerBehavior behavior) : base(Structure.Empty, behavior)  //, A_Func afunc = A_Func.Tanh
        {
            Src = src;
            Mem = mem;
            MatchData = match;
            AttStruct = attStruct;

            SrcScore = new HiddenBatchData(Src.MAX_SENTSIZE, 1, Behavior.RunMode, Behavior.Device);
            MemScore = new HiddenBatchData(Mem.MAX_SENTSIZE, 1, Behavior.RunMode, Behavior.Device);

            Output = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
        }


        public override void Forward()
        {
            SrcScore.BatchSize = Src.SentSize;
            MemScore.BatchSize = Mem.SentSize;
            Output.BatchSize = MatchData.MatchSize;

            
            ComputeLib.Sgemm(Src.SentOutput, 0, AttStruct.weight, 0, SrcScore.Output.Data, 0, Src.SentSize, Src.Dim, 1, 0, 1, false, false);

            ComputeLib.Sgemm(Mem.SentOutput, 0, AttStruct.weight, Src.Dim, MemScore.Output.Data, 0, Mem.SentSize, Mem.Dim, 1, 0, 1, false, false);


            ComputeLib.Matrix_AdditionMask(SrcScore.Output.Data, 0, MatchData.SrcIdx, 0,
                                           MemScore.Output.Data, 0, MatchData.TgtIdx, 0,
                                           Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                                           1, MatchData.MatchSize, 1, 1, 0);

            ComputeLib.SoftAttention_Matching(Src.SentOutput, MatchData.SrcIdx,
                                              Mem.SentOutput, MatchData.TgtIdx,
                                              Src.Dim, (int)A_Func.Linear, 1, AttStruct.weight.SubArray(Src.Dim + Mem.Dim),
                                              Output.Output.Data, CudaPieceInt.Empty,
                                              MatchData.MatchSize, 1, 1);
            
            if (MatchData.SrcSize != Src.SentSize) { throw new Exception(string.Format("AttentionRunner MatchData SrcSize {0} not equals to SentSize {1}", MatchData.SrcSize, Src.SentSize)); }
            if (MatchData.TgtSize != Mem.SentSize) { throw new Exception(string.Format("AttentionRunner MatchData MemSize {0} not equals to SentSize {1}", MatchData.TgtSize, Mem.SentSize)); }

        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize);
        }

        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.DerivSoftAttention_Matching(Src.SentOutput, Src.SentDeriv, MatchData.SrcIdx,
                                                   Mem.SentOutput, Mem.SentDeriv, MatchData.TgtIdx,
                                                   Src.Dim, (int)A_Func.Linear, 1, AttStruct.weight.SubArray(Src.Dim + Mem.Dim),
                                                   AttStruct.WeightGrad.SubArray(Src.Dim + Mem.Dim), Output.Output.Data, Output.Deriv.Data, CudaPieceInt.Empty,
                                                   MatchData.Src2MatchIdx, MatchData.Src2MatchElement, MatchData.SrcSize,
                                                   MatchData.Tgt2MatchIdx, MatchData.Tgt2MatchElement, MatchData.TgtSize,
                                                   MatchData.MatchSize, 1, 1, 1);

            ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, MatchData.Tgt2MatchElement, 0, CudaPieceFloat.Empty,
                                        MatchData.Tgt2MatchIdx, MatchData.TgtSize, MemScore.Deriv.Data, 0, MatchData.Tgt2Idx, 0,
                                        MatchData.MatchSize, 1, 0, 1);

            ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, MatchData.Src2MatchElement, 0, CudaPieceFloat.Empty,
                                        MatchData.Src2MatchIdx, MatchData.SrcSize, SrcScore.Deriv.Data, 0, MatchData.Src2Idx, 0,
                                        MatchData.MatchSize, 1, 0, 1);

            ComputeLib.Sgemm(MemScore.Deriv.Data, 0, AttStruct.weight, Src.Dim, Mem.SentDeriv, 0, MatchData.TgtSize, 1, Mem.Dim, 1, 1, false, false);

            ComputeLib.Sgemm(SrcScore.Deriv.Data, 0, AttStruct.weight, 0, Src.SentDeriv, 0, MatchData.SrcSize, 1, Src.Dim, 1, 1, false, false);

            
            ComputeLib.Sgemm(MemScore.Deriv.Data, 0, Mem.SentOutput, 0, AttStruct.WeightGrad, Src.Dim, 1, MatchData.TgtSize, Mem.Dim, 1, 1, false, false);

            ComputeLib.Sgemm(SrcScore.Deriv.Data, 0, Src.SentOutput, 0, AttStruct.WeightGrad, 0, 1, MatchData.SrcSize, Src.Dim, 1, 1, false, false);
        }

    }

    public class VecAlignmentRunner : StructRunner
    {
        MatrixData Src { get; set; }

        MatrixData Mem { get; set; }

        public BiMatchBatchData MatchData { get; set; }

        VectorData AttWeight = null;
        
        /// <summary>
        /// softmax on address given query.
        /// </summary>
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        int OP;
        A_Func Af;

        public VecAlignmentRunner(MatrixData src, MatrixData mem, BiMatchBatchData match, // int maxSrcTgtCrossLen, //BiMatchBatchData matchData, //CrossSimType attType,
            LayerStructure attStruct, RunnerBehavior behavior, int op = 0, A_Func af = A_Func.Tanh) : 
            this(src, mem, match, new VectorData(attStruct.Neural_In * attStruct.Neural_Out, attStruct.weight, attStruct.WeightGrad, attStruct.DeviceType), behavior, op, af)
        { }

        /// <summary>
        /// o_{ij} = op(src_i, mem_j) * att
        /// </summary>
        /// <param name="src"></param>
        /// <param name="mem"></param>
        /// <param name="match"></param>
        /// <param name="attStruct"></param>
        /// <param name="behavior"></param>
        /// <param name="op"></param>
        /// <param name="af"></param>
        public VecAlignmentRunner(MatrixData src, MatrixData mem, BiMatchBatchData match, // int maxSrcTgtCrossLen, //BiMatchBatchData matchData, //CrossSimType attType,
            VectorData attStruct, RunnerBehavior behavior, int op = 0, A_Func af = A_Func.Tanh) : base(Structure.Empty, behavior)  //, A_Func afunc = A_Func.Tanh
        {
            Src = src;
            Mem = mem;
            MatchData = match;
            AttWeight = attStruct;

            OP = op;
            Af = af;
            Output = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
        }


        public override void Forward()
        {
            Output.BatchSize = MatchData.MatchSize;

            if(Output.BatchSize == 0) { return; }

            ComputeLib.SoftAttention_Matching(Src.Output, MatchData.SrcIdx,
                                              Mem.Output, MatchData.TgtIdx,
                                              Src.Column, (int)Af, OP, AttWeight.Output,
                                              Output.Output.Data, CudaPieceInt.Empty,
                                              MatchData.MatchSize, 0, 1);

            if (MatchData.SrcSize != Src.Row) { throw new Exception(string.Format("AttentionRunner MatchData SrcSize {0} not equals to SentSize {1}", MatchData.SrcSize, Src.Row)); }
            if (MatchData.TgtSize != Mem.Row) { throw new Exception(string.Format("AttentionRunner MatchData MemSize {0} not equals to SentSize {1}", MatchData.TgtSize, Mem.Row)); }

        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            if(Output.BatchSize == 0) { return; }
            ComputeLib.DerivSoftAttention_Matching(Src.Output, Src.Deriv, MatchData.SrcIdx,
                                                   Mem.Output, Mem.Deriv, MatchData.TgtIdx,
                                                   Src.Column, (int)Af, OP, AttWeight.Output,
                                                   AttWeight.Deriv, Output.Output.Data, Output.Deriv.Data, CudaPieceInt.Empty,
                                                   MatchData.Src2MatchIdx, MatchData.Src2MatchElement, MatchData.SrcSize,
                                                   MatchData.Tgt2MatchIdx, MatchData.Tgt2MatchElement, MatchData.TgtSize,
                                                   MatchData.MatchSize, 1, 1, 1);
        }

    }




    public class SeqAlignmentRunnerV3 : StructRunner
    {
        MatrixData Src { get; set; }

        MatrixData Mem { get; set; }

        VectorData AttWeight = null;

        /// <summary>
        /// softmax on address given query.
        /// </summary>
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        int OP;
        A_Func Af;

        /// <summary>
        /// s_{i,j} =  op(src_i, mem_j) * att
        /// </summary>
        /// <param name="src"></param>
        /// <param name="mem"></param>
        /// <param name="attStruct"></param>
        /// <param name="behavior"></param>
        /// <param name="op"></param>
        /// <param name="af"></param>
        public SeqAlignmentRunnerV3(MatrixData src, MatrixData mem, 
            VectorData attStruct, RunnerBehavior behavior, int op = 0, A_Func af = A_Func.Tanh) : base(Structure.Empty, behavior)  //, A_Func afunc = A_Func.Tanh
        {
            Src = src;
            Mem = mem;
            AttWeight = attStruct;

            OP = op;
            Af = af;
            Output = new HiddenBatchData(src.MaxRow, mem.MaxRow, Behavior.RunMode, Behavior.Device);
        }


        public override void Forward()
        {
            Output.BatchSize = Src.Row;
            if (Src.Column != Mem.Column) { throw new Exception(string.Format("Attention column {0} not equals to Mem column {1}", Src.Column, Mem.Column)); }
            ComputeLib.SoftAttention(Src.Output, Mem.Output, 
                                     Src.Column, (int)Af, OP, AttWeight.Output,
                                     Output.Output.Data, Src.Row, Mem.Row, 0, 1);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.DerivSoftAttention(Src.Output, Src.Deriv, Mem.Output, Mem.Deriv, Src.Column, (int)Af, OP, 
                                            AttWeight.Output, AttWeight.Deriv, Output.Output.Data, Output.Deriv.Data, Src.Row, Mem.Row, 1, 1, 1);

        }

    }

    /// <summary>
    /// Memory Retrieval Runner.
    /// </summary>
    public class MemoryRetrievalRunner : StructRunner
    {
        HiddenBatchData Address { get; set; }
        public HiddenBatchData SoftmaxAddress { get; set; }

        SeqDenseBatchData Memory { get; set; }
        BiMatchBatchData MatchData { get; set; }
        float Gamma { get; set; }
        /// <summary>
        /// softmax on address given query.
        /// </summary>
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public MemoryRetrievalRunner(HiddenBatchData address, SeqDenseBatchData memory, BiMatchBatchData matchData, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Address = address;
            Memory = memory;
            MatchData = matchData;
            Gamma = gamma;
            SoftmaxAddress = new HiddenBatchData(Address.MAX_BATCHSIZE, Address.Dim, Behavior.RunMode, Behavior.Device);
            Output = new HiddenBatchData(MatchData.Stat.MAX_SRC_BATCHSIZE, Memory.Dim, Behavior.RunMode, Behavior.Device);
        }

        public override void Forward()
        {
            Output.BatchSize = MatchData.SrcSize;

            // softmax 
            // softmax of attention
            ComputeLib.SparseSoftmax(MatchData.Src2MatchIdx, Address.Output.Data, SoftmaxAddress.Output.Data, Gamma, MatchData.SrcSize);

            ComputeLib.ColumnWiseSumMask(Memory.SentOutput, 0, MatchData.TgtIdx, 0,
                SoftmaxAddress.Output.Data, MatchData.Src2MatchIdx, MatchData.SrcSize,
                Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                Memory.SentSize, Memory.Dim, 0, 1);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Scale_MatrixMask(Output.Deriv.Data, 0, MatchData.SrcIdx, 0,
                                        Memory.SentDeriv, 0, MatchData.TgtIdx, 0,
                                        Memory.Dim, MatchData.MatchSize, SoftmaxAddress.Output.Data, 1);

            
            // outputDeriv -> AddressDeriv.
            ComputeLib.Inner_Product_Matching(Output.Deriv.Data, 0, Memory.SentOutput, 0, SoftmaxAddress.Deriv.Data, 0,
                MatchData.SrcIdx, MatchData.TgtIdx, Output.BatchSize, Memory.SentSize, MatchData.MatchSize,
                Memory.Dim, Util.GPUEpsilon);

            ComputeLib.DerivSparseMultiClassSoftmax(MatchData.Src2MatchIdx,
                SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, Gamma, MatchData.SrcSize);

            ComputeLib.Add_Vector(Address.Deriv.Data, SoftmaxAddress.Deriv.Data, MatchData.MatchSize, 1, 1);
        }
    }

    /// <summary>
    /// GRU Query Runner.
    /// </summary>
    public class GRUQueryRunner : StructRunner<GRUCell, HiddenBatchData>
    {
        public HiddenBatchData Query { get; set; }

        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        HiddenBatchData GateR; //reset gate
        HiddenBatchData GateZ; //update gate
        HiddenBatchData HHat; //new memory
        HiddenBatchData RestH; //new memory

        CudaPieceFloat Ones;
        CudaPieceFloat Tmp;
        public GRUQueryRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
        {
            Query = query;

            Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

            GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
            GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
            HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
            RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

            Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
            Ones.Init(1);

            Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
        }

        public override void Forward()
        {
            //SubQuery.BatchSize = 1;
            /*Wr X -> GateR*/
            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
            /*h_t-1 -> GateR*/
            ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
            ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Query.BatchSize, Model.HiddenStateDim);

            /*Wz X -> GateZ*/
            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
            /*h_t-1 -> GateZ*/
            ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
            ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Query.BatchSize, Model.HiddenStateDim);

            /*Wh X -> HHat*/
            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
            ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Query.BatchSize, Model.HiddenStateDim, 0);
            ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
            ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Query.BatchSize, Model.HiddenStateDim);

            Output.BatchSize = Query.BatchSize;

            //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
            // tmp = (1 - GateZ(t))
            ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
                                           GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
                                           Tmp, 0, CudaPieceInt.Empty, 0,
                                           Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

            // NewQuery = GateZ(t) * HHat
            ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);

            // NewQuery += tmp * h_t-1
            ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
        }

        public override void Backward(bool cleanDeriv)
        {
            if (Query.Deriv != null)
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Query.BatchSize, Model.HiddenStateDim, 1);
            

            ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
            // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
            ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
                                           Query.Output.Data, 0, CudaPieceInt.Empty, 0,
                                           Tmp, 0, CudaPieceInt.Empty, 0,
                                           Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
            ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

            ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
            ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

            ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

            if(Query.Deriv != null)
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);

            ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
            ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

            if(Query.Deriv != null)
                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);

            if(Query.Deriv != null)
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);

            ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
        }

        public override void Update()
        {
            if (!IsDelegateOptimizer)
            {
                Model.WhMatrixOptimizer.BeforeGradient();
                Model.WrMatrixOptimizer.BeforeGradient();
                Model.WzMatrixOptimizer.BeforeGradient();

                Model.UhMatrixOptimizer.BeforeGradient();
                Model.UrMatrixOptimizer.BeforeGradient();
                Model.UzMatrixOptimizer.BeforeGradient();

                Model.BhMatrixOptimizer.BeforeGradient();
                Model.BrMatrixOptimizer.BeforeGradient();
                Model.BzMatrixOptimizer.BeforeGradient();

            }
            ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WhMatrixOptimizer.GradientStep, true, false);
            ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WrMatrixOptimizer.GradientStep, true, false);
            ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WzMatrixOptimizer.GradientStep, true, false);

            ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UhMatrixOptimizer.GradientStep, true, false);
            ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UrMatrixOptimizer.GradientStep, true, false);
            ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UzMatrixOptimizer.GradientStep, true, false);

            ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
            ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
            ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);

            if (!IsDelegateOptimizer)
            {
                Model.WhMatrixOptimizer.AfterGradient();
                Model.WrMatrixOptimizer.AfterGradient();
                Model.WzMatrixOptimizer.AfterGradient();

                Model.UhMatrixOptimizer.AfterGradient();
                Model.UrMatrixOptimizer.AfterGradient();
                Model.UzMatrixOptimizer.AfterGradient();

                Model.BhMatrixOptimizer.AfterGradient();
                Model.BrMatrixOptimizer.AfterGradient();
                Model.BzMatrixOptimizer.AfterGradient();
            }
        }
    }

}
