﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Fully Connected Layer Runner.  (HiddenBatchData + LinkStructure)
    /// </summary>
    /// <typeparam name="IN"></typeparam>
    public class FullyConnectHiddenRunner<IN> : StructRunner<LayerStructure, IN> where IN : HiddenBatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat BiasOne;

        /// <summary>
        /// It is staled, please do not use it.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="behavior"></param>
        //public FullyConnectHiddenRunner(LayerStructure model, RunnerBehavior behavior) : base(model, behavior)
        //{
        //    Output = new HiddenBatchData();
        //}

        DropOutProcessor<HiddenBatchData> DropOutRegularized = null;

        public FullyConnectHiddenRunner(LayerStructure model, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
        {
            if(input.Dim != model.Neural_In)
            {
                throw new Exception(string.Format("FullyConnectHiddenRunner Input Feature doesn't match {0} , {1}", input.Dim, model.Neural_In)); 
            }

            Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
            if (Model.IsBias)
            {
                BiasOne = new CudaPieceFloat(Input.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                BiasOne.Init(1);
            }
            if (Model.DropOut > 0) DropOutRegularized = new DropOutProcessor<HiddenBatchData>(Output, Model.DropOut, Behavior);
        }

        /// <summary>
        /// This will not be used anymore.
        /// </summary>
        //public override void InitMemory()
        //{
        //    if (Output.MAX_BATCHSIZE < Input.BatchSize) Output.Init(Input.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
        //}

        public override void Forward()
        {
            //InitMemory();
            Output.BatchSize = Input.BatchSize;

            if(Output.BatchSize == 0) { return; }

            ComputeLib.Sgemm(Input.Output.Data, 0, Model.weight, 0, Output.Output.Data, 0, Input.BatchSize,
                            Model.Neural_In, Model.Neural_Out, 0, 1, false, false);

            if (Behavior.RunMode == DNNRunMode.Train && Model.DropOut > 0) DropOutRegularized.Forward();
            ActivationFuncLib.ActivationFunc(ComputeLib, Output.Output.Data, Input.BatchSize, Model.Neural_Out, Model.Af, Model.bias);
            
        }

        /// <summary>
        /// cleanDeriv: will be always false.
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.BatchSize == 0) { return; }
            // Backpropagate Deriv.
            ActivationFuncLib.DerivActivationFunc(ComputeLib, Output.Deriv.Data, Output.Output.Data, Input.BatchSize, Model.Neural_Out, Model.Af);
            if (Model.DropOut > 0) DropOutRegularized.Backward(false);
            if(Input.Deriv != null)
                ComputeLib.Sgemm(Output.Deriv.Data, 0, Model.weight, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.Neural_Out, Model.Neural_In, 1, 1, false, true);
            //MathOperatorManager.GlobalInstance.MatrixMultiplicationTranspose(Output.Deriv.Data, Model.weight, Input.Deriv.Data, Input.BatchSize,
            //        Model.Neural_Out, Model.Neural_In, cleanDeriv ? 0 : 1);
        }

        public override void CleanDeriv()
        {
            if (Output.BatchSize == 0) { return; }

            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Update()
        {
            if (Output.BatchSize == 0) { return; }

            //Backpropagate Parameter Update.
            //if (!IsDelegateOptimizer) { Model.WeightOptimizer.BeforeGradient(); }
            ComputeLib.Sgemm(Input.Output.Data, 0, Output.Deriv.Data, 0, Model.WeightGrad, 0, Input.BatchSize, Model.Neural_In, Model.Neural_Out, 1,
                1, true, false);
            //MathOperatorManager.GlobalInstance.Matrix_Product_Weight(Input.Output.Data, Output.Deriv.Data, Model.WeightOptimizer.Gradient,
            //            Input.BatchSize, Model.Neural_In, Model.Neural_Out, Model.WeightOptimizer.GradientStep);
            //if (!IsDelegateOptimizer) { Model.WeightOptimizer.AfterGradient(); }

            if (Model.IsBias)
            {
                //if (!IsDelegateOptimizer) { Model.BiasOptimizer.BeforeGradient(); }
                ComputeLib.Sgemv(Output.Deriv.Data, BiasOne, Input.BatchSize, Model.Neural_Out, Model.BiasGrad, true, 1, 1);
                //MathOperatorManager.GlobalInstance.ColumnWiseSum(Output.Deriv.Data, Model.BiasOptimizer.Gradient, Input.BatchSize, Model.Neural_Out, Model.BiasOptimizer.GradientStep);
                //if (!IsDelegateOptimizer) { Model.BiasOptimizer.AfterGradient(); }
            }
        }

        #region Dispose Function is not necessary.
        ~FullyConnectHiddenRunner()
        {
            this.Dispose(false);
        }

        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }

            base.Dispose(disposing);
        }
        #endregion.
    }
}
