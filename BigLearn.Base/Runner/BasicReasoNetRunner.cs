﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum Att_Type { INNER_PRODUCT = 0 , W_INNER_PRODUCT = 1, COSINE_PRODUCT = 2, W_ADD = 3}
    public class BasicReasoNetRunner : StructRunner
    {
        public HiddenBatchData[] StatusData = null;
        public HiddenBatchData[] AnsProb = null;

        /// <summary>
        /// Input Query;
        /// </summary>
        HiddenBatchData InitStatus { get; set; }

        //BiMatchBatchData GMemMatch { get; set; }
        MLPAttentionStructure GAttStruct { get; set; }

        GRUCell GruCell = null;
        List<LayerStructure> TermStruct = null;
        int MaxIterationNum { get; set; }

        List<StructRunner> SubRunners = new List<StructRunner>();
        HiddenBatchData[] TermData = null;

        float RewardDiscount = 1;
        public float MaxClipTerm = 1 - Util.GPUEpsilon;
        public float MinClipTerm = Util.GPUEpsilon;

        Att_Type Atttype { get; set; }

        List<int> StepStat = new List<int>();
        List<int> StocStepStat = new List<int>();

        public override void Init()
        {
            StepStat = new List<int>();
            for (int i = 0; i < MaxIterationNum; i++) { StepStat.Add(0); }

            StocStepStat = new List<int>();
            for (int i = 0; i < MaxIterationNum; i++) { StocStepStat.Add(0); }
        }

        public override void Complete()
        {
            int totalNum = 0;
            for (int i = 0; i < MaxIterationNum; i++) totalNum += StepStat[i];
            //for (int i = 0; i < MaxIterationNum; i++)
            //{
            //    Logger.WriteLog("Maximum Step {0} Stat {1}, Percentage {2}", i, StepStat[i], StepStat[i] * 1.0f / totalNum);
            //}

            for (int i = 0; i < MaxIterationNum; i++)
            {
                Logger.WriteLog("Stocas Step {0} Stat {1}, Percentage {2}", i, StocStepStat[i], StocStepStat[i] * 1.0f / totalNum);
            }
        }

        /// <summary>
        /// maximum iteration number.
        /// </summary>
        public BasicReasoNetRunner(HiddenBatchData initStatus, int maxIterateNum, MatrixStructure gMem, MLPAttentionStructure gAttStruct, float gGamma, float rewardDiscount,
            GRUCell gruCell, List<LayerStructure> termStruct, RunnerBehavior behavior, Att_Type type = Att_Type.W_INNER_PRODUCT) : base(Structure.Empty, null, behavior)
        {
            InitStatus = initStatus;

            //GMemMatch = gMemMatch;
            GAttStruct = gAttStruct;

            GruCell = gruCell;
            TermStruct = termStruct;

            Atttype = type;

            MaxIterationNum = maxIterateNum;
            RewardDiscount = rewardDiscount;
            // Memory Matrix;
            MatrixData MemoryMatrix = new MatrixData(gMem);

            // InitState is between -1 to 1;
            //MatrixData initState = new MatrixData(InitStatus);
            HiddenBatchData initState = InitStatus;

            // Memory Address Project.
            MatrixData MemoryAddressProject = new MatrixData(gAttStruct.HiddenDim, gAttStruct.MemoryDim, gAttStruct.Wm, gAttStruct.WmGrad, gAttStruct.DeviceType);

            // Memory Address.
            MatrixMultiplicationRunner MemoryAddressRunner = new MatrixMultiplicationRunner(MemoryMatrix, MemoryAddressProject, Behavior);
            SubRunners.Add(MemoryAddressRunner);
            MatrixData MemoryAddress = MemoryAddressRunner.Output;

            // State Address Project.
            MatrixData StateAddressProject = new MatrixData(gAttStruct.HiddenDim, gAttStruct.InputDim, gAttStruct.Wi, gAttStruct.WiGrad, gAttStruct.DeviceType);

            // Address Score Project.
            VectorData AddressScoreProject = new VectorData(gAttStruct.HiddenDim, gAttStruct.Wa, gAttStruct.WaGrad, gAttStruct.DeviceType);

            // For Forward/Backward Propagation.
            {
                StatusData = new HiddenBatchData[MaxIterationNum];
                TermData = new HiddenBatchData[MaxIterationNum];
            }

            StatusData[0] = InitStatus;
            HiddenBatchData terminateOutput = StatusData[0];
            for (int l = 0; l < TermStruct.Count; l++)
            {
                FullyConnectHiddenRunner<HiddenBatchData> tlRunner = new FullyConnectHiddenRunner<HiddenBatchData>(TermStruct[l], terminateOutput, Behavior);
                SubRunners.Add(tlRunner);
                terminateOutput = tlRunner.Output;
            }
            TermData[0] = terminateOutput;

            for (int i = 1; i < MaxIterationNum; i++)
            {
                MatrixMultiplicationRunner StateAddressRunner = new MatrixMultiplicationRunner(new MatrixData(initState), StateAddressProject, Behavior);
                SubRunners.Add(StateAddressRunner);
                MatrixData stateAddress = StateAddressRunner.Output;

                MatrixData attMatrixScore = null;
                switch (Atttype)
                {
                    case Att_Type.W_INNER_PRODUCT:
                        // Attention Input.
                        SeqAlignmentRunnerV3 scoreRunner = new SeqAlignmentRunnerV3(stateAddress, MemoryAddress, AddressScoreProject, Behavior, 1, A_Func.Tanh);
                        SubRunners.Add(scoreRunner);
                        attMatrixScore = new MatrixData(gMem.Size, InitStatus.MAX_BATCHSIZE, scoreRunner.Output.Output.Data, scoreRunner.Output.Deriv.Data, Behavior.Device);
                        break;
                    case Att_Type.INNER_PRODUCT:
                        MatrixProductRunner mRunner = new MatrixProductRunner(stateAddress, MemoryAddress, Behavior);
                        SubRunners.Add(mRunner);
                        attMatrixScore = mRunner.Output;
                        break;
                }

                // Attention Softmax.
                MatrixSoftmaxProcessor attScoreSoftmaxRunner = new MatrixSoftmaxProcessor(attMatrixScore, gGamma, Behavior);
                SubRunners.Add(attScoreSoftmaxRunner);

                MatrixMultiplicationRunner inputXRunner = new MatrixMultiplicationRunner(attMatrixScore, MemoryMatrix, Behavior);
                SubRunners.Add(inputXRunner);
                MatrixData inputMatrix = inputXRunner.Output;

                // RNN State.
                GRUQueryRunner stateRunner = new GRUQueryRunner(GruCell, initState, new HiddenBatchData(inputMatrix), Behavior);
                SubRunners.Add(stateRunner);
                StatusData[i] = stateRunner.Output;

                HiddenBatchData newState = StatusData[i];
                terminateOutput = StatusData[i];
                for (int l = 0; l < TermStruct.Count; l++)
                {
                    FullyConnectHiddenRunner<HiddenBatchData> tlRunner = new FullyConnectHiddenRunner<HiddenBatchData>(TermStruct[l], terminateOutput, Behavior);
                    SubRunners.Add(tlRunner);
                    terminateOutput = tlRunner.Output;
                }
                TermData[i] = terminateOutput;

                // State Switch.
                initState = newState;
            }

            {
                AnsProb = new HiddenBatchData[MaxIterationNum];
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    AnsProb[i] = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                }
            }
        }

        public BasicReasoNetRunner(HiddenBatchData initStatus, int maxIterateNum, MemoryStructure gMem, MLPAttentionStructure gAttStruct, float gGamma, float rewardDiscount,
            GRUCell gruCell, List<LayerStructure> termStruct, RunnerBehavior behavior, Att_Type type = Att_Type.W_INNER_PRODUCT) : base(Structure.Empty, null, behavior)
        {
            InitStatus = initStatus;

            GAttStruct = gAttStruct;

            GruCell = gruCell;
            TermStruct = termStruct;

            Atttype = type;

            MaxIterationNum = maxIterateNum;
            RewardDiscount = rewardDiscount;
            
            // InitState is between -1 to 1;
            //MatrixData initState = new MatrixData(InitStatus);
            HiddenBatchData initState = InitStatus;

            // Memory Address.
            MatrixData MemoryAddress = new MatrixData(gMem.InputDim, gMem.MemoryLen, gMem.Address, gMem.AddressGrad, Behavior.Device);
            // Memory Matrix;
            MatrixData MemoryMatrix = new MatrixData(gMem.OutputDim, gMem.MemoryLen, gMem.Content, gMem.ContentGrad, Behavior.Device);

            // State Address Project.
            MatrixData StateAddressProject = new MatrixData(gAttStruct.HiddenDim, gAttStruct.InputDim, gAttStruct.Wi, gAttStruct.WiGrad, gAttStruct.DeviceType);
            // Address Score Project.
            VectorData AddressScoreProject = new VectorData(gAttStruct.HiddenDim, gAttStruct.Wa, gAttStruct.WaGrad, gAttStruct.DeviceType);

            // For Forward/Backward Propagation.
            {
                StatusData = new HiddenBatchData[MaxIterationNum];
                TermData = new HiddenBatchData[MaxIterationNum];
            }

            StatusData[0] = InitStatus;
            HiddenBatchData terminateOutput = StatusData[0];
            for (int l = 0; l < TermStruct.Count; l++)
            {
                FullyConnectHiddenRunner<HiddenBatchData> tlRunner = new FullyConnectHiddenRunner<HiddenBatchData>(TermStruct[l], terminateOutput, Behavior);
                SubRunners.Add(tlRunner);
                terminateOutput = tlRunner.Output;
            }
            TermData[0] = terminateOutput;

            for (int i = 1; i < MaxIterationNum; i++)
            {
                MatrixMultiplicationRunner StateAddressRunner = new MatrixMultiplicationRunner(new MatrixData(initState), StateAddressProject, Behavior);
                SubRunners.Add(StateAddressRunner);
                MatrixData stateAddress = StateAddressRunner.Output;

                MatrixData attMatrixScore = null;
                switch (Atttype)
                {
                    case Att_Type.W_INNER_PRODUCT:
                        // Attention Input.
                        SeqAlignmentRunnerV3 scoreRunner = new SeqAlignmentRunnerV3(stateAddress, MemoryAddress, AddressScoreProject, Behavior, 1, A_Func.Tanh);
                        SubRunners.Add(scoreRunner);
                        attMatrixScore = new MatrixData(gMem.MemoryLen, InitStatus.MAX_BATCHSIZE, scoreRunner.Output.Output.Data, scoreRunner.Output.Deriv.Data, Behavior.Device);
                        break;
                    case Att_Type.INNER_PRODUCT:
                        MatrixProductRunner mRunner = new MatrixProductRunner(stateAddress, MemoryAddress, Behavior);
                        SubRunners.Add(mRunner);
                        attMatrixScore = mRunner.Output;
                        break;
                }

                // Attention Softmax.
                MatrixSoftmaxProcessor attScoreSoftmaxRunner = new MatrixSoftmaxProcessor(attMatrixScore, gGamma, Behavior);
                SubRunners.Add(attScoreSoftmaxRunner);

                MatrixMultiplicationRunner inputXRunner = new MatrixMultiplicationRunner(attMatrixScore, MemoryMatrix, Behavior);
                SubRunners.Add(inputXRunner);
                MatrixData inputMatrix = inputXRunner.Output;

                // RNN State.
                GRUQueryRunner stateRunner = new GRUQueryRunner(GruCell, initState, new HiddenBatchData(inputMatrix), Behavior);
                SubRunners.Add(stateRunner);
                StatusData[i] = stateRunner.Output;

                HiddenBatchData newState = StatusData[i];
                terminateOutput = StatusData[i];
                for (int l = 0; l < TermStruct.Count; l++)
                {
                    FullyConnectHiddenRunner<HiddenBatchData> tlRunner = new FullyConnectHiddenRunner<HiddenBatchData>(TermStruct[l], terminateOutput, Behavior);
                    SubRunners.Add(tlRunner);
                    terminateOutput = tlRunner.Output;
                }
                TermData[i] = terminateOutput;

                // State Switch.
                initState = newState;
            }

            {
                AnsProb = new HiddenBatchData[MaxIterationNum];
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    AnsProb[i] = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                }
            }
        }


        public override void Forward()
        {
            foreach (StructRunner runner in SubRunners) { runner.Forward(); }

            for (int i = 0; i < MaxIterationNum; i++)
            {
                ComputeLib.Logistic(TermData[i].Output.Data, 0, TermData[i].Output.Data, 0, TermData[i].Output.BatchSize, 1);
                ComputeLib.ClipVector(TermData[i].Output.Data, TermData[i].BatchSize, MaxClipTerm, MinClipTerm); // 1 - Util.GPUEpsilon, Util.GPUEpsilon);
                TermData[i].Output.Data.SyncToCPU(TermData[i].BatchSize);
            }

            for (int b = 0; b < InitStatus.BatchSize; b++)
            {
                int max_iter = 0;
                double max_p = 0;
                float acc_log_t = 0;

                float randomP = (float)ParameterSetting.Random.NextDouble();
                int stoc_iter = MaxIterationNum - 1;

                for (int i = 0; i < MaxIterationNum; i++)
                {
                    float t_i = TermData[i].Output.Data.MemPtr[b];
                    if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                    float p = acc_log_t + (float)Math.Log(t_i);
                    if (i == MaxIterationNum - 1) p = acc_log_t;

                    float prob = (float)Math.Exp(p);
                    AnsProb[i].Output.Data.MemPtr[b] = prob;
                    acc_log_t += (float)Math.Log(1 - t_i);
                    if (prob > max_p)
                    {
                        max_p = prob;
                        max_iter = i;
                    }

                    if(randomP > 0 && randomP <= prob) { stoc_iter = i; }
                    randomP = randomP - prob;
                }

                StepStat[max_iter] += 1;
                StocStepStat[stoc_iter] += 1;
            }

            for (int i = 0; i < MaxIterationNum; i++)
            {
                AnsProb[i].BatchSize = InitStatus.BatchSize;
                AnsProb[i].Output.Data.SyncFromCPU(AnsProb[i].BatchSize);
            }
        }

        public override void CleanDeriv()
        {
            foreach (StructRunner runner in SubRunners) { runner.CleanDeriv(); }
            for (int i = 0; i < MaxIterationNum; i++)
            {
                ComputeLib.Zero(AnsProb[i].Deriv.Data, AnsProb[i].BatchSize);
            }
        }

        /// <summary>
        /// Stage 1 : Iterative Attention Model (Supervised Training).
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            for (int i = 0; i < MaxIterationNum; i++)
            {
                Array.Clear(TermData[i].Deriv.Data.MemPtr, 0, TermData[i].BatchSize);
            }

            for (int b = 0; b < InitStatus.BatchSize; b++)
            {
                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    float reward = AnsProb[i].Deriv.Data.MemPtr[b];
                    TermData[i].Deriv.Data.MemPtr[b] += reward * (1 - TermData[i].Output.Data.MemPtr[b]);
                    for (int hp = 0; hp < i; hp++)
                    {
                        TermData[hp].Deriv.Data.MemPtr[b] += -(float)Math.Pow(RewardDiscount, i - hp) * reward * TermData[hp].Output.Data.MemPtr[b];
                    }

                    if (TermData[i].Deriv.Data.MemPtr[b] > Math.Abs(1))
                    {
                        Console.WriteLine("Start TerminalRunner Deriv is too large {0}, step {1}", TermData[i].Deriv.Data.MemPtr[b], i);
                    }
                }
            }
            
            for (int i = 0; i < MaxIterationNum; i++)
            {
                for (int b = 0; b < InitStatus.BatchSize; b++)
                {
                    if (TermData[i].Output.Data.MemPtr[b] >= MaxClipTerm - Util.GPUEpsilon) TermData[i].Deriv.Data.MemPtr[b] = 0;
                    if (TermData[i].Output.Data.MemPtr[b] <= MinClipTerm + Util.GPUEpsilon) TermData[i].Deriv.Data.MemPtr[b] = 0;
                }
                TermData[i].Deriv.Data.SyncFromCPU(TermData[i].BatchSize);
            }

            foreach (StructRunner runner in Enumerable.Reverse(SubRunners)) { runner.Backward(false); }
        }


        public override void Update()
        {
            foreach (StructRunner runner in SubRunners) { runner.IsDelegateOptimizer = IsDelegateOptimizer; runner.Update(); }
        }
    }

}
