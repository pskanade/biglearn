﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ImageFullyConnectRunner<IN> : StructRunner<LayerStructure, IN> where IN : ImageDataSource
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat BiasOne;

        public ImageFullyConnectRunner(Structure model, ImageDataSource input, RunnerBehavior behavior)
            : base(model, input, behavior)
        {
            Output = new HiddenBatchData(Input.Stat.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
            if (Model.IsBias)
            {
                BiasOne = new CudaPieceFloat(Input.Stat.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                BiasOne.Init(1);
            }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
            //Output.Deriv.Data.Zero();
        }

        public override void Forward()
        {
            //InitMemory();
            //MathOperatorManager.MathDevice = Behavior.Device;
            Output.BatchSize = Input.BatchSize;
            ComputeLib.Sgemm(Input.Data, 0, Model.weight, 0, Output.Output.Data, 0, Input.BatchSize, Model.Neural_In, Model.Neural_Out, 0, 1, false, false);
            //ComputeLib.Matrix_Multipy(Input.Data, Model.weight, Output.Output.Data, Input.BatchSize,
            //                Model.Neural_In, Model.Neural_Out, 0);

            //Model.bias.SyncToCPU();
            ActivationFuncLib.ActivationFunc(Behavior.Computelib, Output.Output.Data, Output.BatchSize, Output.Dim, Model.Af, Model.bias);

            //Output.Output.Data.SyncToCPU();
            //Model.ActiveOutput(Output.Output, Behavior.RunMode);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            // Backpropagate Deriv.
            //Model.DeactiveOutput(Output.Output, Output.Deriv, Behavior.RunMode);
            ActivationFuncLib.DerivActivationFunc(ComputeLib, Output.Deriv.Data, Output.Output.Data, Output.BatchSize, Output.Dim, Model.Af);

            ComputeLib.Sgemm(Output.Deriv.Data, 0, Model.weight, 0, Input.Deriv, 0, Input.BatchSize, Model.Neural_Out, Model.Neural_In, 1, 1, false, true);
            //ComputeLib.MatrixMultiplicationTranspose(Output.Deriv.Data, Model.weight, Input.Deriv, Input.BatchSize,
            //        Model.Neural_Out, Model.Neural_In, cleanDeriv ? 0 : 1);
            //Cudalib.Matrix_Multipy_Weight(Output.Deriv.Data.CudaPtr, Model.weight.CudaPtr, Input.Deriv.CudaPtr, Input.BatchSize,
            //        Model.Neural_Out, Model.Neural_In, 1, cleanDeriv ? 0 : 1);
        }

        public override void Update()
        {
            //Backpropagate Parameter Update.
            if(!IsDelegateOptimizer) Model.WeightOptimizer.BeforeGradient();
            //Cudalib.Matrix_Product_Weight(Input.Data.CudaPtr, Output.Deriv.Data.CudaPtr, Model.WeightOptimizer.Gradient.CudaPtr,
            //            Input.BatchSize, Model.Neural_In, Model.Neural_Out, Model.WeightOptimizer.GradientStep);

            ComputeLib.Sgemm(Input.Data, 0, Output.Deriv.Data, 0, Model.WeightOptimizer.Gradient, 0,
                        Input.BatchSize, Model.Neural_In, Model.Neural_Out, 1, Model.WeightOptimizer.GradientStep, true, false);

            //ComputeLib.Matrix_Product_Weight(Input.Data, Output.Deriv.Data, Model.WeightOptimizer.Gradient,
            //            Input.BatchSize, Model.Neural_In, Model.Neural_Out, Model.WeightOptimizer.GradientStep);
            if (!IsDelegateOptimizer) Model.WeightOptimizer.AfterGradient();

            if (Model.IsBias)
            {
                if (!IsDelegateOptimizer) Model.BiasOptimizer.BeforeGradient();
                //Output.Deriv.Data.SyncToCPU();
                //Model.BiasOptimizer.Gradient.SyncToCPU();
                //Cudalib.Matrix_Aggragate_Weight(Output.Deriv.Data.CudaPtr, Model.BiasOptimizer.Gradient.CudaPtr, Input.BatchSize, Model.Neural_Out, Model.BiasOptimizer.GradientStep);
                ComputeLib.Sgemv(Output.Deriv.Data, BiasOne, Input.BatchSize, Model.Neural_Out, Model.BiasOptimizer.Gradient, true, 1, Model.BiasOptimizer.GradientStep);
                //Model.BiasOptimizer.Gradient.SyncToCPU();
                if (!IsDelegateOptimizer) Model.BiasOptimizer.AfterGradient();

            }
        }

        #region dispose function.
        ~ImageFullyConnectRunner()
        {
            this.Dispose(false);
        }

        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            disposed = true;

            if (disposing)
            {
                if (Output != null)
                {
                    Output.Dispose();
                }
            }

            Output = null;
            base.Dispose(disposing);
        }
        #endregion.
    }
}
