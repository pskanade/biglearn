﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{

    public class BeamAction
    {
        public double Prob;
        public int[] Words;
        public double[] WordProb;

        public List<string> HelpInfo = new List<string>();
        public int BatchId;
        public int COId;
        public HashSet<int> SeletedBeam = new HashSet<int>();
        public BeamAction(int bID, int coID, int widx, int BeamK)
        {
            Prob = 0;
            Words = new int[] { widx };
            WordProb = new double[] { 1 };
            BatchId = bID;
            COId = coID;
            for (int i = 1; i <= BeamK; i++) SeletedBeam.Add(i);
        }

        public BeamAction(BeamAction beam, int coID, int wordIdx, double prob, HashSet<int> selectedBeam)
        {
            Prob = beam.Prob + Math.Log(prob);

            Words = new int[beam.Words.Length + 1];
            Array.Copy(beam.Words, Words, beam.Words.Length);
            Words[beam.Words.Length] = wordIdx;

            WordProb = new double[beam.Words.Length + 1];
            Array.Copy(beam.WordProb, WordProb, beam.Words.Length);
            WordProb[beam.WordProb.Length] = prob;

            BatchId = beam.BatchId;
            COId = coID;
            SeletedBeam = selectedBeam;
        }

        public BeamAction(BeamAction beam, int coID, int wordIdx, double prob) : this(beam, coID, wordIdx, prob, new HashSet<int>())
        { }
    }



    public class EnsembleBeamSearchRunner : StructRunner
    {
        public int BeginWordIndex { get; set; }
        public int EndWordIndex { get; set; }

        public int Beam { get; set; }
        public int MaxSearchLen { get; set; }
        public int VocabSize { get; set; }
        public int MaxBatchSize { get; set; }
        public CudaPieceFloat tmpWordProb;
        public CudaPieceFloat tmpWordIndex;
        public CudaPieceFloat WordProb;

        public bool BeamPerWord = false;
        public List<List<BeamAction>> BatchResult = new List<List<BeamAction>>();

        public EnsembleBeamSearchRunner(int bgnWordIdx, int endWordIdx, int beam, int maxSearchLen, int vocabSize, int maxBatchSize, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            BeginWordIndex = bgnWordIdx;
            EndWordIndex = endWordIdx;
            Beam = beam;
            MaxSearchLen = maxSearchLen;
            VocabSize = vocabSize;
            MaxBatchSize = maxBatchSize;

            WordProb = new CudaPieceFloat(Beam * Beam * maxBatchSize * VocabSize, true, Behavior.Device == DeviceType.GPU);
            tmpWordIndex = new CudaPieceFloat(Beam * Beam * maxBatchSize * Beam * Beam, true, Behavior.Device == DeviceType.GPU);
            tmpWordProb = new CudaPieceFloat(Beam * Beam * maxBatchSize * Beam * Beam, true, Behavior.Device == DeviceType.GPU);
        }


        public List<BasicDecodingRunner> ensembleSearch = new List<BasicDecodingRunner>();
        public void AddBeamSearch(BasicDecodingRunner beamSearchRunner)
        {
            ensembleSearch.Add(beamSearchRunner);
            beamSearchRunner.InitMemory(Beam);
            //beamSearchRunner.
        }


        public override void Forward()
        {
            BatchResult.Clear();
            for (int b = 0; b < ensembleSearch[0].BatchSize; b++) BatchResult.Add(new List<BeamAction>());
            for (int i = 0; i < ensembleSearch.Count; i++) ensembleSearch[i].InitStatus();
            
            List<BeamAction> action = new List<BeamAction>();
            for (int b = 0; b < ensembleSearch[0].BatchSize; b++) action.Add(new BeamAction(b, b, BeginWordIndex, Beam));

            for (int searchStep = 0; searchStep < MaxSearchLen; searchStep++)
            {
                for (int i = 0; i < ensembleSearch.Count; i++) ensembleSearch[ensembleSearch.Count - 1 - i].TakeStep(action, searchStep);

                for (int i = 0; i < ensembleSearch.Count; i++)
                    ensembleSearch[i].WordProb.SyncToCPU();

                //ComputeLib.Add_Vector(WordProb, ensembleSearch[0].WordProb, VocabSize * action.Count, 0, 1);
                //for (int i = 1; i < ensembleSearch.Count; i++)
                //    ComputeLib.ElementwiseProduct(WordProb, ensembleSearch[i].WordProb, WordProb, action.Count, VocabSize, 0);

                ComputeLib.Add_Vector(WordProb, ensembleSearch[0].WordProb, VocabSize * action.Count, 0, 1);
                for (int i = 1; i < ensembleSearch.Count; i++)
                    ComputeLib.Add_Vector(WordProb, ensembleSearch[i].WordProb, VocabSize * action.Count, 1, 1);
                ComputeLib.Scale_Matrix(WordProb, action.Count, VocabSize, 1.0f / ensembleSearch.Count);
                

                Cudalib.KLargestValueBatch(WordProb.CudaPtr, action.Count, VocabSize, Beam, 1, tmpWordProb.CudaPtr, tmpWordIndex.CudaPtr);

                tmpWordProb.SyncToCPU();
                tmpWordIndex.SyncToCPU();

                List<List<int>> batchActionIdxs = new List<List<int>>();
                for (int i = 0; i < ensembleSearch[0].BatchSize; i++) batchActionIdxs.Add(new List<int>());
                for (int b = 0; b < action.Count; b++) batchActionIdxs[action[b].BatchId].Add(b);

                List<BeamAction> newActionList = new List<BeamAction>();

                for (int i = 0; i < ensembleSearch[0].BatchSize; i++)
                {
                    Dictionary<int, HashSet<int>> beamSelection = new Dictionary<int, HashSet<int>>();

                    for (int k = 1; k <= Beam; k++)
                    {
                        IEnumerable<Tuple<int, BeamAction>> beamSearchKCandidate =
                            batchActionIdxs[i].Select(idx => new Tuple<int, BeamAction>(idx, action[idx])).Where(beam => beam.Item2.SeletedBeam.Contains(k));
                        List<KeyValuePair<int, double>> kCandidates = new List<KeyValuePair<int, double>>();
                        foreach (Tuple<int, BeamAction> item in beamSearchKCandidate)
                        {
                            for (int p = 0; p < Beam; p++)
                            {
                                if (BeamPerWord)
                                {
                                    kCandidates.Add(new KeyValuePair<int, double>(item.Item1 * Beam + p,
                                        item.Item2.Prob + Math.Log(tmpWordProb.MemPtr[item.Item1 * Beam + p]) - Math.Log(item.Item2.Words.Length + 1) ));
                                }
                                else
                                {
                                    kCandidates.Add(new KeyValuePair<int, double>(item.Item1 * Beam + p,
                                        item.Item2.Prob + Math.Log(tmpWordProb.MemPtr[item.Item1 * Beam + p]) ));
                                }
                            }
                        }
                        kCandidates.Sort((a, b) => (b.Value).CompareTo(a.Value ));

                        foreach (KeyValuePair<int, double> item in kCandidates.Take(k)) // - BatchResult[i].Count))
                        {
                            if (!beamSelection.ContainsKey(item.Key)) beamSelection.Add(item.Key, new HashSet<int>());
                            beamSelection[item.Key].Add(k);
                        }
                    }

                    foreach (KeyValuePair<int, HashSet<int>> item in beamSelection)
                    {
                        int b = item.Key / Beam;
                        BeamAction newBeam = new BeamAction(action[b], b,
                            (int)tmpWordIndex.MemPtr[item.Key], tmpWordProb.MemPtr[item.Key], item.Value);
                        
                        if ((int)tmpWordIndex.MemPtr[item.Key] == EndWordIndex || searchStep == MaxSearchLen - 1)
                        {
                            BatchResult[i].Add(newBeam);
                        }
                        else if (BatchResult[i].Count == 0 || newBeam.Prob > BatchResult[i].Select(nb => nb.Prob).Max())
                        {
                            newActionList.Add(newBeam);
                        }
                    }
                }
                for (int i = 0; i < ensembleSearch.Count; i++) ensembleSearch[i].TakeSwitch();
                action = newActionList;
                if (action.Count == 0) break;
            }
        }
    }
}
