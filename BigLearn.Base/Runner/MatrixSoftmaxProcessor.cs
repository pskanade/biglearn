﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MatrixSoftmaxProcessor : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        public new MatrixData Input { get { return (MatrixData)base.Input; } set { base.Input = value; } }

        //CudaPieceFloat tmpDeriv;
        float Gamma;

        /// <summary>
        /// Gamma must be 1.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="gamma"></param>
        /// <param name="behavior"></param>
        public MatrixSoftmaxProcessor(MatrixData input, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Gamma = gamma;
            Output = input;
        }

        public override void Forward()
        {
            Output.Row = Input.Row;
            if (Input.Row == 0) return;
            //Output.Output.SyncToCPU();
            //float t = ComputeLib.L2Norm(Output.Output, Output.Output.EffectiveSize);
            ComputeLib.SoftMax(Input.Output, Output.Output, Input.Column, Input.Row, 1);
            //float t2 = ComputeLib.L2Norm(Output.Output, Output.Output.EffectiveSize);
            //Console.WriteLine("{0}\t{1}", t, t2);
        }

        public override void CleanDeriv()
        {
            if (Input.Row == 0) return;
            //ComputeLib.Zero(Output.Deriv, Output.Row * Output.Column);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Input.Row == 0) return;

            //Output.Deriv.SyncToCPU();
            //float t = ComputeLib.L2Norm(Output.Deriv, Output.Deriv.EffectiveSize);

            ComputeLib.DerivSoftmax(Output.Output, Output.Deriv, Input.Deriv, Input.Column, Input.Row, 0);
            //Output.Deriv.SyncToCPU();
            //float t2 = ComputeLib.L2Norm(Output.Deriv, Output.Deriv.EffectiveSize);
            //Console.WriteLine("{0}\t{1}", t, t2);
        }

        public override void Update() { }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }

    public class MatrixSoftmaxRunner : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        public new MatrixData Input { get { return (MatrixData)base.Input; } set { base.Input = value; } }

        /// <summary>
        /// Gamma must be 1.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="gamma"></param>
        /// <param name="behavior"></param>
        public MatrixSoftmaxRunner(MatrixData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Output = new MatrixData(Input.Column, Input.MaxRow, behavior.Device);
        }

        public override void Forward()
        {
            Output.Row = Input.Row;
            if (Input.Row == 0) return;
            //Output.Output.SyncToCPU();
            //float t = ComputeLib.L2Norm(Output.Output, Output.Output.EffectiveSize);
            ComputeLib.SoftMax(Input.Output, Output.Output, Input.Column, Input.Row, 1);
            //float t2 = ComputeLib.L2Norm(Output.Output, Output.Output.EffectiveSize);
            //Console.WriteLine("{0}\t{1}", t, t2);
        }

        public override void CleanDeriv()
        {
            if (Input.Row == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Row * Output.Column);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Input.Row == 0) return;

            //Output.Deriv.SyncToCPU();
            //float t = ComputeLib.L2Norm(Output.Deriv, Output.Deriv.EffectiveSize);

            ComputeLib.DerivSoftmax(Output.Output, Output.Deriv, Input.Deriv, Input.Column, Input.Row, 1);
            //Output.Deriv.SyncToCPU();
            //float t2 = ComputeLib.L2Norm(Output.Deriv, Output.Deriv.EffectiveSize);
            //Console.WriteLine("{0}\t{1}", t, t2);
        }

        public override void Update() { }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }

}
