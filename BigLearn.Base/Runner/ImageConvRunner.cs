﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ImageConvRunner<IN> : StructRunner<ImageLinkStructure, IN> where IN : ImageDataSource
    {
        public new ImageDataSource Output { get { return (ImageDataSource)base.Output; } set { base.Output = value; } }
        ImageDataSource LayerPoolingOutput = null;
        CudaPieceInt LayerMaxPoolingIndex = null;

        public ImageConvRunner(Structure model, ImageDataSource input, RunnerBehavior behavior) : base(model, input, behavior)
        {
            LayerPoolingOutput = new ImageDataSource(Input.Stat.MAX_BATCHSIZE,
                                               Model.Param.PoolingX(Input.Stat.Width),
                                               Model.Param.PoolingY(Input.Stat.Height),
                                               Model.Param.O,
                                               Behavior.Device);

            if (Model.Param.PoolingParameter.Stride == 1 && Model.Param.PoolingParameter.SX == 1)
            {
                Output = LayerPoolingOutput;
                LayerMaxPoolingIndex = null;
            }
            else
            {
                Output = new ImageDataSource(Input.Stat.MAX_BATCHSIZE,
                                                   Model.Param.OutputWidth(Input.Stat.Width),
                                                   Model.Param.OutputHeight(Input.Stat.Height),
                                                   Model.Param.O,
                                                   Behavior.Device);
                LayerMaxPoolingIndex = new CudaPieceInt(Output.Stat.MAX_BATCHSIZE * Output.Stat.Width * Output.Stat.Height * Output.Stat.Depth, true, Behavior.Device == DeviceType.GPU);
            }
        }


        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Size);
            //Output.Deriv.Zero();
        }
        public override void Forward()
        {
            //var forwardCounter = PerfCounter.Manager.Instance["Image_Conv_Forward_V1"].Begin();
            LayerPoolingOutput.BatchSize = Input.BatchSize;
            Output.BatchSize = Input.BatchSize;

            //Cudalib.ImageConvolution(Input.Data.CudaPtr, Input.BatchSize, Input.Stat.Width, Input.Stat.Height, Input.Stat.Depth,
            //                            Model.Filter.CudaPtr, Model.Param.SX, Model.Param.C, Model.Param.O, Model.Param.Pad, Model.Param.Stride,
            //                            LayerPoolingOutput.Data.CudaPtr, LayerPoolingOutput.Stat.Width, LayerPoolingOutput.Stat.Height);
            ComputeLib.CNNForwardPropagate(Input.Data, Input.BatchSize, Input.Stat.Depth, Input.Stat.Height, Input.Stat.Width,
                Model.Filter, Model.Param.O, Model.Param.SX, Model.Param.SX, Model.Param.Pad, Model.Param.Stride,
                LayerPoolingOutput.Data, LayerPoolingOutput.Stat.Height, LayerPoolingOutput.Stat.Width);
            if (LayerMaxPoolingIndex != null)
            {
                //ComputeLib.Zero(Output.Data, Output.Size);
                Cudalib.MaxImagePooling(LayerPoolingOutput.Data.CudaPtr, LayerPoolingOutput.BatchSize, LayerPoolingOutput.Stat.Width, LayerPoolingOutput.Stat.Height, LayerPoolingOutput.Stat.Depth,
                                        LayerMaxPoolingIndex.CudaPtr,
                                        Model.Param.PoolingParameter.SX, Model.Param.PoolingParameter.Stride,
                                        Output.Data.CudaPtr, Output.Stat.Width, Output.Stat.Height);
            }
            ActivationFuncLib.Image_ActiveOutput(Output.Data, Model.Bias, Model.Param.Af, Output.BatchSize, Output.Stat.Width, Output.Stat.Height, Output.Stat.Depth);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            // Backpropagate Deriv.
            ActivationFuncLib.Image_DeactiveOutput(Output.Data, Output.Deriv, Model.Param.Af, Output.BatchSize, Output.Stat.Width, Output.Stat.Height, Output.Stat.Depth);
            if (!Input.IsSource)
            {
                if (LayerMaxPoolingIndex != null)
                {
                    Cudalib.ImageConvMatrixMultipy(Output.Deriv.CudaPtr, LayerMaxPoolingIndex.CudaPtr,
                                                Input.BatchSize, Output.Stat.Width, Output.Stat.Height, Output.Stat.Depth,
                                                Model.Param.PoolingParameter.SX, Model.Param.PoolingParameter.Stride,
                                                Model.Filter.CudaPtr, Model.Param.SX, Model.Param.C, Model.Param.Pad,
                                                Model.Param.Stride, Input.Deriv.CudaPtr, Input.Stat.Width, Input.Stat.Height, Input.Stat.Depth);
                }
                else
                {
                    //Cudalib.ImageDenseConvMatrixMultipy(Output.Deriv.CudaPtr, 
                    //                            Input.BatchSize, Output.Stat.Width, Output.Stat.Height, Output.Stat.Depth,
                    //                            Model.Filter.CudaPtr, Model.Param.SX, Model.Param.C, Model.Param.Pad,
                    //                            Model.Param.Stride, Input.Deriv.CudaPtr, Input.Stat.Width, Input.Stat.Height, Input.Stat.Depth);
                    ComputeLib.CuDNNBackwardPropagateData(Input.Deriv, Output.BatchSize, Input.Stat.Depth, Input.Stat.Height, Input.Stat.Width,
                        Model.Filter, Output.Stat.Depth, Model.Param.SX, Model.Param.SX, Model.Param.Pad, Model.Param.Stride,
                        Output.Deriv, Output.Stat.Height, Output.Stat.Width);

                    //Cudalib.CuDNNBackwardPropagateData(Input.Deriv.CudaPtr, Output.BatchSize, Input.Stat.Depth, Input.Stat.Height, Input.Stat.Width,
                    //    Model.Filter.CudaPtr, Output.Stat.Depth, Model.Param.SX, Model.Param.SX, Model.Param.Pad, Model.Param.Stride,
                    //    Output.Deriv.CudaPtr, Output.Stat.Height, Output.Stat.Width);
                }
            }
        }

        public override void Update()
        {
            if (!IsDelegateOptimizer) { Model.FilterOptimizer.BeforeGradient(); throw new Exception("IsDelegateOptimizer is set to false"); }

            if (LayerMaxPoolingIndex != null)
            {
                Cudalib.Convolution_Image_Matrix_Product_INTEX_Weight(
                                           Output.Deriv.CudaPtr, LayerMaxPoolingIndex.CudaPtr,
                                           Input.BatchSize, Output.Stat.Width, Output.Stat.Height, Output.Stat.Depth,
                                           Model.Param.PoolingParameter.SX, Model.Param.PoolingParameter.Stride,
                                           Model.FilterOptimizer.Gradient.CudaPtr, Model.Param.SX, Model.Param.C, Model.Param.Pad,
                                           Model.Param.Stride, Input.Data.CudaPtr, Input.Stat.Width, Input.Stat.Height, Input.Stat.Depth, Model.FilterOptimizer.GradientStep);
            }
            else
            {
                //Cudalib.Convolution_Dense_Image_Matrix_Product_INTEX_Weight(Output.Deriv.CudaPtr, 
                //                           Input.BatchSize, Output.Stat.Width, Output.Stat.Height, Output.Stat.Depth,
                //                           Model.FilterOptimizer.Gradient.CudaPtr, Model.Param.SX, Model.Param.C, Model.Param.Pad,
                //                           Model.Param.Stride, Input.Data.CudaPtr, Input.Stat.Width, Input.Stat.Height, Input.Stat.Depth, Model.FilterOptimizer.GradientStep);
                ComputeLib.CuDNNBackwardPropagateFilter(Input.Data, Input.BatchSize, Input.Stat.Depth, Input.Stat.Height, Input.Stat.Width,
                    Model.FilterOptimizer.Gradient, Output.Stat.Depth, Model.Param.SX, Model.Param.SX, Model.Param.Pad, Model.Param.Stride,
                    Output.Deriv, Output.Stat.Height, Output.Stat.Width, Model.FilterOptimizer.GradientStep, 1);

                //Cudalib.CuDNNBackwardPropagateFilter(Input.Data.CudaPtr, Input.BatchSize, Input.Stat.Depth, Input.Stat.Height, Input.Stat.Width,
                //Model.FilterOptimizer.Gradient.CudaPtr, Output.Stat.Depth, Model.Param.SX, Model.Param.SX, Model.Param.Pad, Model.Param.Stride,
                //Output.Deriv.CudaPtr, Output.Stat.Height, Output.Stat.Width, Model.FilterOptimizer.GradientStep, 1);
            }
            if (!IsDelegateOptimizer) Model.FilterOptimizer.AfterGradient();

            
            if (!IsDelegateOptimizer) Model.BiasOptimizer.BeforeGradient();
            ComputeLib.CuDNNBackwardPropagateBias(Output.Deriv, Model.BiasOptimizer.Gradient, Output.BatchSize,
                 Output.Stat.Width, Output.Stat.Height, Output.Stat.Depth, Model.BiasOptimizer.GradientStep, 1);
            //Cudalib.CuDNNBackwardPropagateBias(Model.BiasOptimizer.Gradient.CudaPtr,
            //        Output.Deriv.CudaPtr, Output.BatchSize, Output.Stat.Depth, Output.Stat.Height, Output.Stat.Width, Model.BiasOptimizer.GradientStep, 1);
            //Cudalib.Matrix_Image_Aggragate_Weight(Output.Deriv.CudaPtr, Model.BiasOptimizer.Gradient.CudaPtr, Output.BatchSize,
            //     Output.Stat.Width, Output.Stat.Height, Output.Stat.Depth, Model.BiasOptimizer.GradientStep);
            if (!IsDelegateOptimizer) Model.BiasOptimizer.AfterGradient();
        }


        #region Dispose function.
        ~ImageConvRunner()
        {
            this.Dispose(false);
        }

        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
                if (LayerPoolingOutput != null) LayerPoolingOutput.Dispose(); LayerPoolingOutput = null;
                if (LayerMaxPoolingIndex != null) LayerMaxPoolingIndex.Dispose(); LayerMaxPoolingIndex = null;
            }

            base.Dispose(disposing);
        }
        #endregion.


    }
}
