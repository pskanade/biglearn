﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class DotProductRunner : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        MatrixData InputA { get; set; }
        MatrixData InputB { get; set; }

        BiMatchBatchData MatchData = null;

        CudaPieceFloat tmp = null;

        //bool IsProcessor = false;
        //float Alpha = 0;
        
        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public DotProductRunner(MatrixData inputA, MatrixData inputB, BiMatchBatchData matchData, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            MatchData = matchData;
            Output = new MatrixData(InputA.Column, MatchData.Stat.MAX_MATCH_BATCHSIZE, Behavior.Device);
            tmp = new CudaPieceFloat(MatchData.Stat.MAX_MATCH_BATCHSIZE * InputA.Column, Behavior.Device);
        }

        public override void Forward()
        {
            if (InputA.Column != InputB.Column) { throw new Exception(string.Format("Matrix A Dim2 {0} should be equals to Matrix B Dim2 {1}", InputA.Column, InputB.Column)); }

            if (InputA.Row <= 0) { throw new Exception(string.Format("Matrix A Row {0} should be larger than zero", InputA.Row)); }
            if (InputA.Column <= 0) { throw new Exception(string.Format("Matrix A Column {0} should be larger than zero", InputA.Column)); }
            if (InputB.Row <= 0) { throw new Exception(string.Format("Matrix B Row {0} should be larger than zero", InputB.Row)); }
            if (InputB.Column <= 0) { throw new Exception(string.Format("Matrix B Column {0} should be larger than zero", InputB.Column)); }
            if (MatchData.MatchSize <= 0) { throw new Exception(string.Format("Match Size {0} should be larger than zero", MatchData.MatchSize)); }

            Output.Row = MatchData.MatchSize;
            Output.Column = InputA.Column;

            ComputeLib.ElementwiseProductMask(InputA.Output, 0, InputB.Output, 0, Output.Output, 0,
                                              MatchData.SrcIdx, 0, MatchData.TgtIdx, 0, CudaPieceInt.Empty, 0,
                                              MatchData.MatchSize, InputA.Column, 0, 1);
            //ComputeLib.Sgemm(InputA.Output, 0, InputB.Output, 0, Output.Output, 0, InputA.Row, InputA.Column, InputB.Column, IsProcessor ? Alpha : 0, 1, false, false);
        }

        public override void CleanDeriv()
        {
            if (Output.Deriv != null && !Output.Deriv.IsEmpty) ComputeLib.Zero(Output.Deriv, Output.Column * Output.Row);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (InputA.Deriv != null && !InputA.Deriv.IsEmpty)
            {
                ComputeLib.ElementwiseProductMask(Output.Deriv, 0, InputB.Output, 0, tmp, 0, //HiddenStatus.Output.Data, 0,
                                                  CudaPieceInt.Empty, 0, MatchData.TgtIdx, 0, CudaPieceInt.Empty, 0,
                                                  Output.Row, Output.Column, 0, 1);

                ComputeLib.ColumnWiseSumMask(tmp, 0, MatchData.Src2MatchElement, 0, CudaPieceFloat.Empty,
                                             MatchData.Src2MatchIdx, MatchData.SrcSize, InputA.Deriv, 0, CudaPieceInt.Empty, 0,
                                             MatchData.MatchSize, InputA.Column, 1, 1);
                //ComputeLib.Sgemm(Output.Deriv, 0, InputB.Output, 0, InputA.Deriv, 0, Output.Row, Output.Column, InputB.Row, 1, 1, false, true);
            }

            if (InputB.Deriv != null && !InputB.Deriv.IsEmpty)
            {
                ComputeLib.ElementwiseProductMask(Output.Deriv, 0, InputA.Output, 0, tmp, 0, //HiddenStatus.Output.Data, 0,
                                                  CudaPieceInt.Empty, 0, MatchData.SrcIdx, 0, CudaPieceInt.Empty, 0,
                                                  Output.Row, Output.Column, 0, 1);

                ComputeLib.ColumnWiseSumMask(tmp, 0, MatchData.Tgt2MatchElement, 0, CudaPieceFloat.Empty,
                                             MatchData.Tgt2MatchIdx, MatchData.TgtSize, InputB.Deriv, 0, CudaPieceInt.Empty, 0,
                                             MatchData.MatchSize, InputB.Column, 1, 1);
                //ComputeLib.Sgemm(InputA.Output, 0, Output.Deriv, 0, InputB.Deriv, 0, InputA.Row, InputB.Row, InputB.Column, 1, 1, true, false);
            }
        }

        public override void Update() { }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }
}
