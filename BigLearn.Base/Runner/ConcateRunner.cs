﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class VectorConcateRunner : StructRunner<Structure, BatchData>
    {
        public new VectorData Output { get { return (VectorData)base.Output; } set { base.Output = value; } }

        public List<VectorData> InputList { get; set; }

        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public VectorConcateRunner(List<VectorData> inputList, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputList = inputList;
            int dim = 0;
            foreach (VectorData vector in InputList) dim += vector.MaxLength;
            Output = new VectorData(dim, Behavior.Device);
        }

        public override void Forward()
        {
            int dim = 0;
            foreach (VectorData vector in InputList)
            {
                ComputeLib.Add_Vector(Output.Output, dim, vector.Output, 0, vector.Length, 0, 1);
                dim += vector.Length;
            }
            Output.Length = dim;
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            int dim = 0;
            foreach (VectorData vector in InputList)
            {
                ComputeLib.Add_Vector(vector.Deriv, 0, Output.Deriv, dim, vector.Length, 1, 1);
                dim += vector.Length;
            }
        }

        public override void Update() { }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }

    public class MatrixConcateRunner : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        public List<MatrixData> InputList { get; set; }

        /// <summary>
        /// O = [i1, i2, ... in]
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public MatrixConcateRunner(List<MatrixData> inputList, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputList = inputList;
            int dim = 0;
            foreach (MatrixData vector in InputList) dim += vector.Column;
            Output = new MatrixData(dim, InputList[0].MaxRow, Behavior.Device);
        }

        public override void Forward()
        {
            Output.Row = InputList.Select(i => i.Row).Min(); // [0].Row;
            if (Output.Row == 0) { return; }
            int offsetDim = 0;
            for (int i = 0; i < InputList.Count; i++)
            {
                if (InputList[i].Row != Output.Row) { throw new Exception("BatchSize doesn't matach in EnsembleMatrixRunner"); }
                ComputeLib.Matrix_AdditionEx(InputList[i].Output, 0, InputList[i].Column,
                                             InputList[i].Output, 0, InputList[i].Column,
                                             Output.Output, offsetDim, Output.Column,
                                             InputList[i].Column, InputList[i].Row, 1, 0, 0);
                offsetDim += InputList[i].Column;
            }

        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Row * Output.Column);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            int offsetDim = 0;
            for (int i = 0; i < InputList.Count; i++)
            {
                ComputeLib.Matrix_AdditionEx(Output.Deriv, offsetDim, Output.Column,
                                             InputList[i].Deriv, 0, InputList[i].Column,
                                             InputList[i].Deriv, 0, InputList[i].Column,
                                             InputList[i].Column, InputList[i].Row, 1, 0, 1);
                offsetDim += InputList[i].Column;
            }

        }

        public override void Update() { }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }

    public class SeqMatrixConcateRunner : StructRunner<Structure, BatchData>
    {
        public new SeqMatrixData Output { get { return (SeqMatrixData)base.Output; } set { base.Output = value; } }

        public MatrixData InputA { get; set; }

        public SeqMatrixData InputB { get; set; }

        CudaPieceFloat tmpDeriv;
        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public SeqMatrixConcateRunner(MatrixData inputA, SeqMatrixData inputB, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;

            Output = new SeqMatrixData(inputA.Column + inputB.Column, inputB.MaxRow, inputB.MaxSegment, inputB.SegmentIdx, inputB.SegmentMargin, Behavior.Device);
            tmpDeriv = new CudaPieceFloat(inputA.Column * inputB.MaxRow, behavior.Device);
        }

        public override void Forward()
        {
            Output.Row = InputB.Row;

            //if (InputA.Column != InputB.Column) { throw new Exception(string.Format("the dimension of inputA  {0} and inputB  {1} should be the same", InputA.Column, InputB.Column)); }
            ComputeLib.Matrix_AdditionExMask(InputA.Output, InputB.SegmentMargin, InputA.Column,
                                             InputA.Output, InputB.SegmentMargin, InputA.Column,
                                             Output.Output, CudaPieceInt.Empty, Output.Column,
                                             InputA.Column, Output.Row, 1, 0, 0);


            ComputeLib.Matrix_AdditionExMask(InputB.Output, CudaPieceInt.Empty, InputB.Column,
                                             InputB.Output, CudaPieceInt.Empty, InputB.Column,
                                             Output.Output.SubArray(InputA.Column), CudaPieceInt.Empty, Output.Column,
                                             InputB.Column, Output.Row, 1, 0, 0);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Row * Output.Column);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Matrix_AdditionExMask(Output.Deriv.SubArray(InputA.Column), CudaPieceInt.Empty, Output.Column,
                                             InputB.Deriv, CudaPieceInt.Empty, InputB.Column,
                                             InputB.Deriv, CudaPieceInt.Empty, InputB.Column,
                                             InputB.Column, Output.Row, 1, 0, 1);

            ComputeLib.Matrix_AdditionExMask(Output.Deriv, CudaPieceInt.Empty, Output.Column,
                                             tmpDeriv, CudaPieceInt.Empty, InputA.Column,
                                             tmpDeriv, CudaPieceInt.Empty, InputA.Column,
                                             InputA.Column, Output.Row, 1, 0, 0);


            ComputeLib.ColumnWiseSumMask(tmpDeriv, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
                                            Output.SegmentIdx, Output.Segment, 
                                            InputA.Deriv, 0, CudaPieceInt.Empty, 0, Output.Row, InputA.Column, 1, 1);
        }

        public override void Update() { }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }

    public class ConcateRunner : StructRunner
    {
        MatrixData Src { get; set; }

        MatrixData Mem { get; set; }

        public BiMatchBatchData MatchData { get; set; }

        /// <summary>
        /// softmax on address given query.
        /// </summary>
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }


        public ConcateRunner(MatrixData src, MatrixData mem, BiMatchBatchData match, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Src = src;
            Mem = mem;
            MatchData = match;
            Output = new MatrixData(src.Column + mem.Column, MatchData.Stat.MAX_MATCH_BATCHSIZE, Behavior.Device);
        }

        public override void Forward()
        {
            Output.Row = MatchData.MatchSize;
            if (Output.Row == 0) { return; }

            ComputeLib.Matrix_AdditionExMask(Src.Output, MatchData.SrcIdx, Src.Column,
                                             Src.Output, MatchData.SrcIdx, Src.Column,
                                             Output.Output, CudaPieceInt.Empty, Output.Column,
                                             Src.Column, MatchData.MatchSize, 1, 0, 0);

            ComputeLib.Matrix_AdditionExMask(Mem.Output, MatchData.TgtIdx, Mem.Column,
                                             Mem.Output, MatchData.TgtIdx, Mem.Column,
                                             Output.Output.SubArray(Src.Column), CudaPieceInt.Empty, Output.Column,
                                             Mem.Column, MatchData.MatchSize, 1, 0, 0);

            if (MatchData.SrcSize != Src.Row) { throw new Exception(string.Format("AttentionRunner MatchData SrcSize {0} not equals to Src Row {1}", MatchData.SrcSize, Src.Row)); }
            if (MatchData.TgtSize != Mem.Row) { throw new Exception(string.Format("AttentionRunner MatchData MemSize {0} not equals to Mem Row {1}", MatchData.TgtSize, Mem.Row)); }
        }

        public override void CleanDeriv()
        {
            if(Output.Row == 0) { return; }
            ComputeLib.Zero(Output.Deriv, Output.Row * Output.Column);
        }

        public override void Backward(bool cleanDeriv)
        {
            if (Output.Row == 0) { return; }

            ComputeLib.ColumnWiseSumMaskEx(Output.Deriv, 0, MatchData.Src2MatchElement, 0, CudaPieceFloat.Empty, Output.Column,
                                        MatchData.Src2MatchIdx, MatchData.SrcSize, Src.Deriv, 0, MatchData.Src2Idx, 0, Src.Column,
                                        MatchData.MatchSize, Src.Column, 1, 1);

            ComputeLib.ColumnWiseSumMaskEx(Output.Deriv, Src.Column, MatchData.Tgt2MatchElement, 0, CudaPieceFloat.Empty, Output.Column,
                                       MatchData.Tgt2MatchIdx, MatchData.TgtSize, Mem.Deriv, 0, MatchData.Tgt2Idx, 0, Mem.Column,
                                       MatchData.MatchSize, Mem.Column, 1, 1);


        }

    }
}
