﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{

    public class VecNormRunner : StructRunner
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        new HiddenBatchData Input { get; set; }

        CudaPieceFloat tmp = null;
        CudaPieceFloat tmp2 = null;
        CudaPieceFloat tmp3 = null;
        public VecNormRunner(HiddenBatchData data, RunnerBehavior behavior)
       : base(Structure.Empty, behavior)
        {
            Input = data;
            Output = data;
            tmp = new CudaPieceFloat(Output.MAX_BATCHSIZE, behavior.Device);
            tmp2 = new CudaPieceFloat(Output.MAX_BATCHSIZE, behavior.Device);
            tmp3 = new CudaPieceFloat(Output.MAX_BATCHSIZE, behavior.Device);

        }

        public override void Forward()
        {
            ComputeLib.MatrixL2Norm(Input.Output.Data, Input.Dim, Input.BatchSize, tmp);
            tmp.SyncToCPU(Output.BatchSize);
            for (int i = 0; i < Output.BatchSize; i++)
            {
                tmp.MemPtr[i] = 1.0f / (tmp.MemPtr[i] + Util.GPUEpsilon);
                tmp3.MemPtr[i] = -tmp.MemPtr[i];
            }
            tmp.SyncFromCPU(Output.BatchSize);
            tmp3.SyncFromCPU(Output.BatchSize);

            ComputeLib.Scale_MatrixMask(Input.Output.Data, 0, CudaPieceInt.Empty, 0,
                    Output.Output.Data, 0, CudaPieceInt.Empty, 0, Output.Dim, Output.BatchSize, tmp, 0);
        }

        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.InnerProduct_Similarity(Output.Deriv.Data, Output.Output.Data, tmp2, Output.BatchSize, Output.Dim);
            
            ComputeLib.Scale_MatrixMask(Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                                        Output.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                        Output.Dim, Output.BatchSize, tmp2, -1);

            ComputeLib.Scale_MatrixMask(Output.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                        Output.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                        Output.Dim, Output.BatchSize, tmp3, 0);

            
        }
    }


    public class MatrixMultiplicationRunner : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        MatrixData InputA { get; set; }
        MatrixData InputB { get; set; }

        bool IsProcessor = false;
        float Alpha = 0;
        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public MatrixMultiplicationRunner(MatrixData inputA, MatrixData inputB, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            Output = new MatrixData(InputB.Column, InputA.MaxRow, Behavior.Device);
        }

        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public MatrixMultiplicationRunner(MatrixData inputA, VectorData inputB, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = new MatrixData(inputB);
            Output = new MatrixData(InputB.Column, InputA.MaxRow, Behavior.Device);
        }


        public MatrixMultiplicationRunner(MatrixData inputA, MatrixData inputB, MatrixData output, float alpha, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            Output = output;
            IsProcessor = true;
            Alpha = alpha;
        }

        public override void Forward()
        {
            iteration++;
            //if(iteration == 31 && name.Equals("TerminateRunner"))
            //{
            //    InputA.Output.SyncToCPU();
            //    InputB.Output.SyncToCPU();
            //}

            Output.Row = InputA.Row;
            Output.Column = InputB.Column;

            if (Output.Row == 0) { return; }

            if (InputA.Column != InputB.Row) { throw new Exception(string.Format("Matrix A Dim1 {0} should be equals to Matrix B Dim2 {1}", InputA.Column, InputB.Row)); }
            if (InputA.Row <= 0) { throw new Exception(string.Format("Matrix A Row {0} should be larger than zero", InputA.Row)); }
            if (InputA.Column <= 0) { throw new Exception(string.Format("Matrix A Column {0} should be larger than zero", InputA.Column)); }
            if (InputB.Row <= 0) { throw new Exception(string.Format("Matrix B Row {0} should be larger than zero", InputB.Row)); }
            if (InputB.Column <= 0) { throw new Exception(string.Format("Matrix B Column {0} should be larger than zero", InputB.Column)); }

            ComputeLib.Sgemm(InputA.Output, 0, InputB.Output, 0, Output.Output, 0, InputA.Row, InputA.Column, InputB.Column, IsProcessor ? Alpha : 0, 1, false, false);
            

        }

        public override void CleanDeriv()
        {
            if (Output.Row == 0) { return; }

            if (!IsProcessor && Output.Deriv != null && !Output.Deriv.IsEmpty) ComputeLib.Zero(Output.Deriv, Output.Column * Output.Row);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Row == 0) { return; }

            if (InputA.Deriv != null && !InputA.Deriv.IsEmpty)
            {
                ComputeLib.Sgemm(Output.Deriv, 0, InputB.Output, 0, InputA.Deriv, 0, Output.Row, Output.Column, InputB.Row, 1, 1, false, true);
            }

            if (InputB.Deriv != null && !InputB.Deriv.IsEmpty)
            {
                ComputeLib.Sgemm(InputA.Output, 0, Output.Deriv, 0, InputB.Deriv, 0, InputA.Row, InputB.Row, InputB.Column, 1, 1, true, false);
            }
        }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }

}
