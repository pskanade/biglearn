﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    //public class BatchNormRunner<IN>: StructRunner<LayerStructure, IN> where IN : HiddenBatchData
    //{
    //    public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

    //    //BN special variable
    //    FastVector beta = null;
    //    FastVector gamma = null;
    //    FastVector mu = null;
    //    FastVector sigma = null;

    //    FastVector delta_beta = null;
    //    FastVector delta_gamma = null;
    //    FastVector delta_mu = null;
    //    FastVector delta_SigmaS = null;
    //    int Dim = -1;

    //    FastVector AdaGrad_Gamma = null;
    //    FastVector AdaGrad_Beta = null;
    //    public BatchNormRunner(LayerStructure model, RunnerBehavior behavior) : base(model, behavior)
    //    {
    //        //Output = new HiddenBatchData();
    //    }

    //    //public override void InitMemory()
    //    //{
    //    //    if (Output.MAX_BATCHSIZE < Input.BatchSize)
    //    //    {
    //    //        Output.Init(Input.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
    //    //    }

    //    //    int Dim = Input.Dim;
    //    //    if (mu == null)
    //    //    {
    //    //        beta = FastVector.Create(Dim);
    //    //        gamma = FastVector.Create(Dim);
    //    //        for (int i = 0; i < Dim; i++)
    //    //            gamma[i] = 1.0f;
    //    //        mu = FastVector.Create(Dim);
    //    //        sigma = FastVector.Create(Dim);
    //    //        delta_beta = FastVector.Create(Dim);
    //    //        delta_gamma = FastVector.Create(Dim);
    //    //        delta_mu = FastVector.Create(Dim);
    //    //        delta_SigmaS = FastVector.Create(Dim);
    //    //        AdaGrad_Gamma = FastVector.Create(Dim);
    //    //        AdaGrad_Beta = FastVector.Create(Dim);
    //    //    }
    //    //    else
    //    //    {
    //    //        mu.ZeroItems();
    //    //        sigma.ZeroItems();
    //    //        delta_beta.ZeroItems();
    //    //        delta_gamma.ZeroItems();
    //    //        delta_mu.ZeroItems();
    //    //        delta_SigmaS.ZeroItems();
    //    //    }
    //    //}

    //    public override void CleanDeriv()
    //    {
    //        Output.Deriv.Data.Zero();
    //    }

    //    /// <summary>
    //    /// Refer to the 4th page of http://arxiv.org/pdf/1502.03167v3.pdf
    //    /// </summary>
    //    /// <param></param>
    //    public override void Forward()
    //    {
    //        InitMemory();

    //        Input.Output.Data.CopyOutFromCuda();
    //        int BatchSize = Input.BatchSize;
    //        int totalSize = Input.Output.Data.Size;
    //        for (int i = 0; i < Dim; i++)
    //        {
    //            for (int j = 0; j < totalSize; j += Dim)
    //            {
    //                mu[i] += Input.Output.Data.MemPtr[j + i];
    //            }
    //            mu[i] /= BatchSize;

    //            for (int j = 0; j < totalSize; j += Dim)
    //            {
    //                float deviation = Input.Output.Data.MemPtr[j + i] - mu[i];
    //                sigma[i] = deviation * deviation;
    //            }
    //            sigma[i] /= (BatchSize - 1);
    //            sigma[i] = (float)(Math.Sqrt(sigma[i] + 0.05));
    //            for (int j = 0; j < totalSize; j += Dim )
    //            {
    //                //$\hat{x} = \frac{x-\mu}{\sigma}, y = \gamma * \hat{x} + \beta$
    //                Output.Output.Data.MemPtr[j + i] = gamma[i] * (Input.Output.Data.MemPtr[j + i] - mu[i]) / sigma[i] + beta[i];
    //            }
    //        }
    //        Output.BatchSize = Input.BatchSize;
    //        Output.Output.Data.CopyIntoCuda();
    //    }

    //    /// <summary>
    //    /// Refer to the 4th page of http://arxiv.org/pdf/1502.03167v3.pdf
    //    /// </summary>
    //    /// <param name="cleanDeriv"></param>
    //    public override void Backward(bool cleanDeriv)
    //    {
    //        // Backpropagate Deriv.
    //        if (cleanDeriv)
    //        {
    //            Input.Deriv.Data.Zero();
    //        }

    //        Output.Deriv.Data.CopyOutFromCuda();
    //        int BatchSize = Input.BatchSize;
    //        int totalSize = Input.Output.Data.Size;

    //        for (int i = 0; i < Dim; i++)
    //        {
    //            for (int j = 0; j < totalSize; j += Dim)
    //            {
    //                float xHat = (Input.Output.Data.MemPtr[i + j] - mu[i]) / sigma[i];
    //                delta_beta[i] += Output.Deriv.Data.MemPtr[i + j];
    //                delta_gamma[i] += xHat * Output.Deriv.Data.MemPtr[i + j];

    //                float gammaOverSigma = gamma[i] / sigma[i];
    //                delta_mu[i] -= gammaOverSigma * Output.Deriv.Data.MemPtr[i + j];
    //                delta_SigmaS[i] -= gammaOverSigma * xHat * Output.Deriv.Data.MemPtr[i + j];

    //                Input.Deriv.Data.MemPtr[i + j] += gammaOverSigma * Output.Deriv.Data.MemPtr[i + j];
    //            }
    //            delta_SigmaS[i] = (float)(delta_SigmaS[i] /(2.0 * sigma[i]));

    //            for (int j = 0; j < totalSize; j+= Dim)
    //            {
    //                Input.Deriv.Data.MemPtr[i + j] += 2 * (Input.Output.Data.MemPtr[i + j] - mu[i]) * delta_SigmaS[i] / (BatchSize - 1) + delta_mu[i] / BatchSize;
    //            }
    //        }
    //    }

    //    /// <summary>
    //    /// we need to update \gamma,\beta based on delta_gamma and delta_beta here
    //    /// no bias here
    //    /// no need to update weight here
    //    /// </summary>
    //    public override void Update()
    //    {
    //        //update. AdaGrad update on 
    //        AdaGrad_Gamma.ApplyAdaGrad(delta_gamma);
    //        AdaGrad_Beta.ApplyAdaGrad(delta_beta);
    //        beta.Sub(delta_beta);
    //        gamma.Sub(delta_gamma);

    //        //Backpropagate Parameter Update.
    //        //Model.WeightOptimizer.BeforeGradient();
    //        //Cudalib.Matrix_Product_Weight(Input.Output.Data.CudaPtr, Output.Deriv.Data.CudaPtr, Model.WeightOptimizer.Gradient.CudaPtr,
    //        //            Input.BatchSize, Model.Neural_In, Model.Neural_Out, Model.WeightOptimizer.GradientStep);
    //        //Model.WeightOptimizer.AfterGradient();
    //    }

    //    #region Dispose Function.
    //    ~BatchNormRunner()
    //    {
    //        this.Dispose(false);
    //    }

    //    private bool disposed = false;
    //    protected override void Dispose(bool disposing)
    //    {
    //        if (disposed)
    //        {
    //            return;
    //        }

    //        disposed = true;

    //        if (disposing)
    //        {
    //            if (Output != null) Output.Dispose(); Output = null;
    //            beta.Dispose();
    //            gamma.Dispose();
    //            mu.Dispose();
    //            sigma.Dispose();
    //            delta_beta.Dispose();
    //            delta_gamma.Dispose();
    //            delta_SigmaS.Dispose();
    //            delta_mu.Dispose();
    //        }

    //        base.Dispose(disposing);
    //    }
    //    #endregion.


    //}
}
