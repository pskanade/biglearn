﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SimilarityRunner: StructRunner<Structure, BatchData> 
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        HiddenBatchData InputA { get; set; }
        HiddenBatchData InputB { get; set; }

        BiMatchBatchData MatchData { get; set; }

        SimilarityType SimType { get; set; }

        CudaPieceFloat InputASquare;
        CudaPieceFloat InputBSquare;

        public SimilarityRunner(HiddenBatchData inputA, HiddenBatchData inputB, BiMatchBatchData matchData, SimilarityType similarityType, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            MatchData = matchData;
            SimType = similarityType;

            Output = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE / MatchData.Dim, MatchData.Dim, Behavior.RunMode, Behavior.Device);

            if (SimType == SimilarityType.CosineSimilarity)
            {
                InputASquare = new CudaPieceFloat(InputA.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                InputBSquare = new CudaPieceFloat(InputB.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
            }
        }

        public override void Forward()
        {
            Output.BatchSize = MatchData.MatchSize / (MatchData.Dim == 0 ? 1 : MatchData.Dim);

            switch (SimType)
            {
                case SimilarityType.CosineSimilarity:

                    InputASquare.EffectiveSize = InputA.BatchSize;
                    InputBSquare.EffectiveSize = InputB.BatchSize;

                    ComputeLib.Square_Matrix(InputA.Output.Data, InputA.BatchSize, InputA.Dim, InputASquare);
                    ComputeLib.Square_Matrix(InputB.Output.Data, InputB.BatchSize, InputB.Dim, InputBSquare);
                    ComputeLib.Cosine_Similarity_Matching(InputA.Output.Data, InputB.Output.Data, Output.Output.Data,
                                                          MatchData.SrcIdx, MatchData.TgtIdx,
                                                          InputA.BatchSize, InputB.BatchSize, MatchData.MatchSize, InputA.Dim,
                                                          InputASquare, InputBSquare, Util.GPUEpsilon);
                    break;
                case SimilarityType.InnerProduct:
                    ComputeLib.Inner_Product_Matching(InputA.Output.Data, InputB.Output.Data, Output.Output.Data,
                                                          MatchData.SrcIdx, MatchData.TgtIdx,
                                                          InputA.BatchSize, InputB.BatchSize, MatchData.MatchSize, InputA.Dim, Util.GPUEpsilon);
                    break;
            }
            
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if(cleanDeriv)
            {
                //InputA.Deriv.Data.Zero();
                //InputB.Deriv.Data.Zero();
            }
            switch (SimType)
            {
                case SimilarityType.CosineSimilarity:
                    ComputeLib.Deriv_CosineSimilarity_Matching(InputA.Output.Data, InputB.Output.Data, InputASquare, InputBSquare, InputA.Dim,
                                            MatchData.Src2MatchIdx, MatchData.Src2MatchElement, MatchData.Tgt2MatchIdx, MatchData.Tgt2MatchElement,
                                            MatchData.SrcIdx, MatchData.TgtIdx, MatchData.SrcSize, MatchData.TgtSize, MatchData.MatchSize, 
                                            Output.Output.Data, Output.Deriv.Data, InputA.Deriv.Data, InputB.Deriv.Data, Util.GPUEpsilon);
                    break;
                case SimilarityType.InnerProduct:
                    ComputeLib.Deriv_InnerProduct_Matching(InputA.Output.Data, InputB.Output.Data, InputA.Dim,
                                            MatchData.Src2MatchIdx, MatchData.Src2MatchElement,
                                            MatchData.Tgt2MatchIdx, MatchData.Tgt2MatchElement,
                                            MatchData.SrcIdx, MatchData.TgtIdx,
                                            MatchData.SrcSize, MatchData.TgtSize, MatchData.MatchSize,
                                            Output.Deriv.Data, InputA.Deriv.Data, InputB.Deriv.Data, Util.GPUEpsilon);

                    //Cudalib.Deriv_InnerProduct_Matching(InputA.Output.Data.CudaPtr, InputB.Output.Data.CudaPtr, InputA.Dim,
                    //                        MatchData.Src2MatchIdx.CudaPtr, MatchData.Src2MatchElement.CudaPtr, 
                    //                        MatchData.Tgt2MatchIdx.CudaPtr, MatchData.Tgt2MatchElement.CudaPtr,
                    //                        MatchData.SrcIdx.CudaPtr, MatchData.TgtIdx.CudaPtr, 
                    //                        MatchData.SrcSize, MatchData.TgtSize, MatchData.MatchSize,
                    //                        Output.Deriv.Data.CudaPtr, InputA.Deriv.Data.CudaPtr, InputB.Deriv.Data.CudaPtr, Util.GPUEpsilon);
                    break;
            }
        }
    }

    public class CosineSimilarityRunner : StructRunner
    {
        HiddenBatchData Status { get; set; }

        SeqDenseBatchData Mem { get; set; }

        BiMatchBatchData MatchData { get; set; }

        SimilarityRunner SimRunner = null;

        /// <summary>
        /// softmax on address given query.
        /// </summary>
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public CosineSimilarityRunner(HiddenBatchData status, SeqDenseBatchData mem, BiMatchBatchData matchData, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Status = status;
            Mem = mem;
            MatchData = matchData;

            SimRunner = new SimilarityRunner(status, new HiddenBatchData(mem.MAX_SENTSIZE, mem.Dim, mem.SentOutput, mem.SentDeriv, Behavior.Device),
                matchData, SimilarityType.CosineSimilarity, Behavior);

            Output = SimRunner.Output;
        }

        public override void Forward()
        {
            SimRunner.Forward();
        }

        public override void CleanDeriv()
        {
            SimRunner.CleanDeriv();
        }

        public override void Backward(bool cleanDeriv)
        {
            SimRunner.Backward(cleanDeriv);
        }

        public override void Update()
        {
            SimRunner.IsDelegateOptimizer = IsDelegateOptimizer;
            SimRunner.Update();
        }
    }
}
