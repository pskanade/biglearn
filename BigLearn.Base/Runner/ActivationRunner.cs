﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ActivationRunner : StructRunner<Structure, HiddenBatchData>
    {
        public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        public A_Func AFunc { get; private set; }
        bool IsProcess = true;
        public ActivationRunner(HiddenBatchData input, A_Func aFunc, RunnerBehavior behavior)
            : base(Structure.Empty, input, behavior)
        {
            Input = input;
            AFunc = aFunc;

            //int maxBatchSize, int dim, DNNRunMode mode, DeviceType device)
            //Output = new HiddenBatchData(input.MAX_BATCHSIZE, input.Dim, behavior.RunMode, behavior.Device);
            //IsProcess = false;
            Output = input;
        }

        public ActivationRunner(HiddenBatchData input, A_Func aFunc, HiddenBatchData output, RunnerBehavior behavior)
            : base(Structure.Empty, input, behavior)
        {
            Input = input;
            AFunc = aFunc;
            Output = output;
            IsProcess = false;
        }

        public ActivationRunner(MatrixData input, A_Func aFunc, RunnerBehavior behavior)
            : this(new HiddenBatchData(input), aFunc, behavior)
        { }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            if (Output.BatchSize == 0) return;

            switch (AFunc)
            {
                case A_Func.Tanh:
                    ComputeLib.Tanh(Input.Output.Data, 0, Output.Output.Data, 0, Input.BatchSize * Input.Dim);
                    break;
                case A_Func.Linear:
                    if(!IsProcess)
                    {
                        ComputeLib.Matrix_AdditionMask(Input.Output.Data, 0, CudaPieceInt.Empty, 0,
                                                       Input.Output.Data, 0, CudaPieceInt.Empty, 0,
                                                       Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                                                       Output.Dim, Input.BatchSize, 1, 0, 0);
                    }
                    break;
                case A_Func.Rectified:
                    ComputeLib.ReLU(Input.Output.Data, 0, Output.Output.Data, 0, Input.BatchSize * Input.Dim);
                    break;
                case A_Func.Sigmoid:
                    ComputeLib.Logistic(Input.Output.Data, 0, Output.Output.Data, 0, Input.BatchSize * Input.Dim, 1.0f);
                    break;
            }
        }

        public override void CleanDeriv()
        {
            if (Output.BatchSize == 0) return;
            if (!IsProcess) ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.BatchSize == 0) return;

            switch (AFunc)
            {
                case A_Func.Tanh:
                    ComputeLib.DerivTanh(Output.Output.Data, 0, Output.Deriv.Data, 0, Input.Deriv.Data, 0, Input.BatchSize * Input.Dim, IsProcess ? 0 : 1, 1);
                    break;
                case A_Func.Linear:
                    if (!IsProcess)
                    {
                        ComputeLib.Matrix_AdditionMask(Output.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                       Output.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                       Input.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                       Output.Dim, Input.BatchSize, 1, 0, 1);
                    }
                    break;
                case A_Func.Rectified:
                    ComputeLib.DerivReLU(Output.Output.Data, 0, Output.Deriv.Data, 0, Input.Deriv.Data, 0, Input.BatchSize * Input.Dim, IsProcess ? 0 : 1, 1);
                    break;
                case A_Func.Sigmoid:
                    ComputeLib.DerivLogistic(Output.Output.Data, 0, Output.Deriv.Data, 0, Input.Deriv.Data, 0, Input.BatchSize * Input.Dim, IsProcess ? 0 : 1, 1);
                    break;
            }
        }

    }
}
