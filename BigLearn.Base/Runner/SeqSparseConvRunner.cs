﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SeqSparseConvRunner<IN> : StructRunner<LayerStructure, IN> where IN : SeqSparseBatchData
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        DropOutProcessor<HiddenBatchData> DropOutRegularized = null;

        public SeqSparseConvRunner(LayerStructure model, SeqSparseBatchData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            Output = new SeqDenseBatchData(Input.SampleIdx, Input.SentMargin, Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.Neural_Out, Behavior.Device);

            if (Model.DropOut > 0)
                DropOutRegularized = new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(Output.MAX_SENTSIZE, Output.Dim, Output.SentOutput, Output.SentDeriv, Behavior.Device), Model.DropOut, Behavior);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }

        public override void Forward()
        {
            iteration++;

            //if((name.Equals("char_cnn_doc") || name.Equals("char_cnn_query")) && (iteration == 30 || iteration == 31))
            //{
            //    Input.SyncToCPU();
            //    Model.SyncToCPU();
            //}

            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            //Output.SampleIdx = Input.SampleIdx;
            //Output.SentMargin = Input.SentMargin;
            ComputeLib.Convolution_Sparse_Matrix_Multiply_INTEX(Input, Model.weight, Output.SentOutput, Model.Neural_In, Model.Neural_Out, Model.N_Winsize);
            if (Behavior.RunMode == DNNRunMode.Train && Model.DropOut > 0) DropOutRegularized.Forward();
            ActivationFuncLib.ActivationFunc(ComputeLib, Output.SentOutput, Output.SentSize, Output.Dim, Model.Af, Model.bias);
            //if ((name.Equals("char_cnn_doc") || name.Equals("char_cnn_query")) && (iteration == 30 || iteration == 31))
            //{
            //    Output.SyncToCPU();
            //}
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            // Backpropagate Deriv.
            ActivationFuncLib.DerivActivationFunc(ComputeLib, Output.SentDeriv, Output.SentOutput, Output.SentSize, Output.Dim, Model.Af);
            if (Model.DropOut > 0) DropOutRegularized.Backward(false);
        }

        public override void Update()
        {
            //if ((name.Equals("char_cnn_doc") || name.Equals("char_cnn_query")) && (iteration == 30 || iteration == 31))
            //{
            //    Output.SentDeriv.SyncToCPU();
            //    Model.WeightOptimizer.Gradient.SyncToCPU();
            //}
            if (!IsDelegateOptimizer) { Model.WeightOptimizer.BeforeGradient(); }
            Cudalib.convolution_sparse_matrix_gradient(Output.SentDeriv.CudaPtr, Input.SequenceIdx.CudaPtr, Input.SentMargin.CudaPtr, Input.SentSize,
                        Model.N_Winsize, Input.BatchSize, Model.Neural_Out, Input.FeaIdx.CudaPtr, Input.FeaValue.CudaPtr, Model.WeightOptimizer.Gradient.CudaPtr, Model.Neural_In, Model.WeightOptimizer.GradientStep);
            if (!IsDelegateOptimizer) { Model.WeightOptimizer.AfterGradient(); }
            //if ((name.Equals("char_cnn_doc") || name.Equals("char_cnn_query")) && (iteration == 30 || iteration == 31))
            //{
            //    Model.WeightOptimizer.Gradient.SyncToCPU();
            //}
            if (Model.IsBias)
            {
                if (!IsDelegateOptimizer) { Model.BiasOptimizer.BeforeGradient(); }
                ComputeLib.ColumnWiseSum(Output.SentDeriv, Model.BiasOptimizer.Gradient, Input.SentSize, Model.Neural_Out, Model.BiasOptimizer.GradientStep);
                if (!IsDelegateOptimizer) { Model.BiasOptimizer.AfterGradient(); }
            }
        }

        //private bool disposed = false;
        //~SeqSparseConvRunner()
        //{
        //    this.Dispose(false);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (this.disposed) { return; }
        //    this.disposed = true;

        //    Output.Dispose();
        //    base.Dispose(disposing);
        //}

    }
}
