﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SeqVecMultiplicationRunner : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        SeqVectorData InputA { get; set; }
        MatrixData InputB { get; set; }

        CudaPieceFloat tmpA { get; set; }
        public SeqVecMultiplicationRunner(SeqVectorData inputA, MatrixData inputB, RunnerBehavior behavior) :
            base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            Output = new MatrixData(InputB.Column, InputA.MaxSegment, Behavior.Device);
            tmpA = new CudaPieceFloat(InputA.MaxLength, behavior.Device);
        }

        public override void Forward()
        {
            if(InputB.Row != InputA.Length) { throw new Exception(string.Format("InputA Length {0} should equal to InputB Row {1}", InputA.Length, InputB.Row)); }
            Output.Row = InputA.Segment;
            ComputeLib.ColumnWiseSumMask(InputB.Output, 0, CudaPieceInt.Empty, 0,
                    InputA.Output, InputA.SegmentIdx, InputA.Segment,
                    Output.Output, 0, CudaPieceInt.Empty, 0,
                    InputB.Row, InputB.Column, 0, 1);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Column * Output.Row);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Scale_MatrixMask(Output.Deriv, 0, InputA.SegmentMargin, 0,
                                        InputB.Deriv, 0, CudaPieceInt.Empty, 0,
                                        InputB.Column, InputB.Row, InputA.Output, 1);

            ComputeLib.Inner_Product_Matching(Output.Deriv, 0,
                                              InputB.Output, 0,
                                              tmpA, 0,
                                              InputA.SegmentMargin, CudaPieceInt.Empty,
                                              Output.Row, InputB.Row, InputA.Length, InputB.Column, 0);
            ComputeLib.Add_Vector(InputA.Deriv, tmpA, InputA.Length, 1, 1);
        }

        public override void Update() { }

        #region Disposing Function.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
            }
            base.Dispose(disposing);
        }
        #endregion.
    }
}
