﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class IntArgument
    {
        public string Name;
        public int Value;
        public IntArgument(string name, int defaultValue = 0)
        {
            Name = name;
            Value = defaultValue;
        }
    }


    public class HiddenDataBatchSizeRunner : StructRunner
    {
        HiddenBatchData Data;
        IntArgument Arg;
        public HiddenDataBatchSizeRunner(HiddenBatchData data, IntArgument arg, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Data = data;
            Arg = arg;
        }
        public override void Forward() { Arg.Value = Data.BatchSize; }
    }


    
        public class ZeroMatrixRunner : StructRunner
        {
            CudaPieceFloat zeroMat =  null;
            IntArgument SizeArg { get; set; }
            
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            public ZeroMatrixRunner(int maxSize, int dim, IntArgument sizeArg, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                zeroMat = new CudaPieceFloat(maxSize * dim, Behavior.Device); 
                zeroMat.Zero();
                SizeArg = sizeArg;
                Output = new HiddenBatchData(maxSize, dim, zeroMat, null, Behavior.Device);
            }
            public override void Forward()
            {
                Output.BatchSize = SizeArg.Value;
            }
        }


}
