﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BigLearn
{
    
    public enum ObjectiveType
    {
        MMI,
        MXE,
        NCE,
        NCE2,
        PAIRRANK,
        MULTIREGRESSION,
        PAIRCLASSIFICATION,
        PAIRREGRESSION_MSE,
        PAIRREGRESSION_LR,
        DNN_AE_MSE,
        DNN_AE_MSE2,
        DNN_AE_Rank,
    }

    public class ParameterSetting
    {
        public static bool PSEUDO_RANDOM = true;
        private static int random_seed = 0;
        //public static int[] RANDOM_SEED = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        public static int RANDOM_SEED {
            get { 
                return random_seed;
            }
            set
            {
                random_seed = value;
            }
        }
        public static Random rnd = null;
        public static Random Random
        {
            get
            {
                if (rnd == null) rnd = new Random(RANDOM_SEED);
                return rnd;
            }
        }
        public static Random MirrorRandom = new Random(RANDOM_SEED);

        public static bool DEBUG = false;
        public const float MissingLabel = 10001;

        public static int BasicMathLibThreadNum = 128;
    }
}
