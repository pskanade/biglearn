﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class DataRandomShuffling
    {
        int Idx = 0;
        int Num = 0;
        //List<int> RandomIdxs = new List<int>();
        int[] RandomIdxs = null;
        Random mRandom = null;
        public DataRandomShuffling(int num, Random random = null)
        {
            Num = num;
            RandomIdxs = new int[Num];
            
            Init();

            if (random == null) { mRandom = new Random(13); }
            else { mRandom = random; }
        }
        public void Init()
        {
            Idx = 0;
            for(int i=0; i < Num; i++) RandomIdxs[i] = i;
        }
        public int RandomNext()
        {
            if (Idx >= Num) return -1;
            int randomPos = mRandom.Next(Idx, Num);

            int id = RandomIdxs[randomPos];
            RandomIdxs[randomPos] = RandomIdxs[Idx];
            RandomIdxs[Idx] = id;
            Idx++;
            return id;
        }

        public int OrderNext()
        {
            if (Idx >= Num) return -1;
            int id = RandomIdxs[Idx];
            Idx++;
            return id;
        }
    }

}
