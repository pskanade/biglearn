﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class VIntHelper
    {
        public static ushort ToUShort(byte byte1, byte byte2)
        {
            return (ushort)((byte2 << 8) + byte1);
        }

        public static void FromUShort(ushort number, out byte byte1, out byte byte2)
        {
            byte2 = (byte)(number >> 8);
            byte1 = (byte)(number & 255);
        }
        public static byte[] WriteSortedVIntList(uint[] ids)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            byte byte1, byte2;
            FromUShort((ushort)ids.Length, out byte1, out byte2);
            //bw.Write((ushort)ids.Length);
            bw.Write(byte1);
            bw.Write(byte2);
            WriteVInt(ids[0], bw);

            for (int i = 1; i < ids.Length; i++)
            {
                WriteVInt(ids[i] - ids[i - 1], bw);
            }
            return ms.ToArray();
        }
        public static void WriteSortedVIntList(int[] ids, BinaryWriter bw)
        {
            byte byte1, byte2;
            FromUShort((ushort)ids.Length, out byte1, out byte2);
            //bw.Write((ushort)ids.Length);
            bw.Write(byte1);
            bw.Write(byte2);
            WriteVInt((uint)ids[0], bw);

            for (int i = 1; i < ids.Length; i++)
            {
                WriteVInt((uint)ids[i] - (uint)ids[i - 1], bw);
            }
        }
        /// <summary>
        /// We need to be very careful when you have negative integer, make sure the sorted is based on uint
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public static byte[] WriteSortedVIntList(int[] ids)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            byte byte1, byte2;
            FromUShort((ushort)ids.Length, out byte1, out byte2);
            //bw.Write((ushort)ids.Length);
            bw.Write(byte1);
            bw.Write(byte2);
            WriteVInt((uint)ids[0], bw);

            for (int i = 1; i < ids.Length; i++)
            {
                WriteVInt((uint)ids[i] - (uint)ids[i - 1], bw);
            }
            return ms.ToArray();
        }

        public static void WriteVIntList(int[] ids, BinaryWriter bw)
        {
            byte byte1, byte2;
            FromUShort((ushort)ids.Length, out byte1, out byte2);
            //bw.Write((ushort)ids.Length);
            bw.Write(byte1);
            bw.Write(byte2);

            for (int i = 0; i < ids.Length; i++)
            {
                WriteVInt((uint)ids[i], bw);
            }
        }

        private static void WriteVInt(uint i, BinaryWriter bw)
        {
            while ((i & ~0x7F) != 0)
            {
                byte b = ((byte)((i & 0x7f) | 0x80));
                bw.Write(b);
                i = i >> 7;
            }

            byte lastb = ((byte)(i & 0x7f));

            bw.Write(lastb);
        }

        private static uint ReadVInt(byte[] buffer, ref int pos)
        {
            byte b = buffer[pos++];

            int i = b & 0x7F;
            for (int shift = 7; (b & 0x80) != 0; shift += 7)
            {
                b = buffer[pos++];
                i |= (b & 0x7F) << shift;
            }
            return (uint)i;
        }

        private static uint ReadVInt(BinaryReader br)
        {
            byte b = br.ReadByte();

            int i = b & 0x7F;
            for (int shift = 7; (b & 0x80) != 0; shift += 7)
            {
                b = br.ReadByte();
                i |= (b & 0x7F) << shift;
            }
            return (uint)i;
        }

        public static uint[] ReadSortedVIntList(byte[] buffer)
        {
            int length = ToUShort(buffer[0], buffer[1]);
            int pos = 2;

            uint[] ids = new uint[length];
            int num = 0;
            uint pid = ReadVInt(buffer, ref pos);
            ids[num++] = pid;
            while (num < length)
            {
                uint id = ReadVInt(buffer, ref pos);
                id += pid;
                pid = id;
                ids[num++] = pid;
            }

            return ids;
        }

        /// <summary>
        /// Need to be very careful when you have negative integer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static int[] ReadSortedVIntListInt(byte[] buffer)
        {
            int length = ToUShort(buffer[0], buffer[1]);
            int pos = 2;

            int[] ids = new int[length];
            int num = 0;
            uint pid = ReadVInt(buffer, ref pos);
            ids[num++] = (int)pid;
            while (num < length)
            {
                uint id = ReadVInt(buffer, ref pos);
                id += pid;
                pid = id;
                ids[num++] = (int)pid;
            }

            return ids;
        }
        public static int[] ReadSortedVIntListInt(BinaryReader br)
        {
            int length = ToUShort(br.ReadByte(), br.ReadByte());

            int[] ids = new int[length];
            int num = 0;
            uint pid = ReadVInt(br);
            ids[num++] = (int)pid;
            while (num < length)
            {
                uint id = ReadVInt(br);
                id += pid;
                pid = id;
                ids[num++] = (int)pid;
            }

            return ids;
        }

        public static void ReadSortedVIntListInt(BinaryReader br, int[] ids)
        {
            int length = ToUShort(br.ReadByte(), br.ReadByte());
            if (length != ids.Length)
            {
                throw new Exception("length unmatched");
            }
            int num = 0;
            uint pid = ReadVInt(br);
            ids[num++] = (int)pid;
            while (num < length)
            {
                uint id = ReadVInt(br);
                id += pid;
                pid = id;
                ids[num++] = (int)pid;
            }
        }

        public static int[] ReadVIntList(BinaryReader br)
        {
            int length = ToUShort(br.ReadByte(), br.ReadByte());

            int[] ids = new int[length];
            for (int i = 0; i < length; i++)
            {
                ids[i] = (int)ReadVInt(br);
            }

            return ids;
        }

        public static void ReadVIntList(BinaryReader br, int[] ids)
        {
            int length = ToUShort(br.ReadByte(), br.ReadByte());
            if (length != ids.Length)
            {
                throw new Exception("length unmatched");
            }
            for (int i = 0; i < length; i++)
            {
                ids[i] = (int)ReadVInt(br);
            }
        }
        public static uint[] ReadSortedVIntList(byte[] buffer, ref int pos)
        {
            int length = ToUShort(buffer[pos++], buffer[pos++]);

            uint[] ids = new uint[length];
            int num = 0;
            uint pid = ReadVInt(buffer, ref pos);
            ids[num++] = pid;
            while (num < length)
            {
                uint id = ReadVInt(buffer, ref pos);
                id += pid;
                pid = id;
                ids[num++] = pid;
            }

            return ids;
        }
    }
}
