﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class RandomUtil
    {
        public Random rng;
        const int hqrndm1 = 2147483563;
        const int hqrndm2 = 2147483399;
        const int hqrndmagic = 1634357784;

        const float machineepsilon = 5E-16f;
        const float maxrealnumber = float.MaxValue;
        const float minrealnumber = float.MinValue;

        public RandomUtil(int randomSeed)
        {
            rng = new Random(randomSeed);
        }

        class HQRNDState
        {
            public int S1;
            public int S2;
            public int Magicv;
        };

        public void RandomShuffle<T>(T[] array)
        {
            RandomShuffle(array, array.Length);
        }

        public void RandomShuffle<T>(T[] array, int num)
        {
            int batIdx = 0;
            while (batIdx < num)
            {
                int randomPos = rng.Next(batIdx, array.Length);
                T temp = array[randomPos];
                array[randomPos] = array[batIdx];
                array[batIdx] = temp;
                batIdx++;
            }
        }


        void hqrndRandomize(HQRNDState state)
        {
            int s0 = 0;
            int s1 = 0;

            s0 = rng.Next();
            s1 = rng.Next();
            hqrndSeed(s0, s1, state);
        }

        /*************************************************************************
        Random number generator: normal numbers

        This function generates two independent random numbers from normal
        distribution. Its performance is equal to that of HQRNDNormal()

        State structure must be initialized with HQRNDRandomize() or HQRNDSeed().

          -- ALGLIB --
             Copyright 02.12.2009 by Bochkanov Sergey
        *************************************************************************/
        void hqrndNormal2(HQRNDState state, ref float x1, ref float x2)
        {
            float u = 0;
            float v = 0;
            float s = 0;

            x1 = 0;
            x2 = 0;

            while (true)
            {
                u = 2 * (float)rng.NextDouble() - 1;
                v = 2 * (float)rng.NextDouble() - 1;
                s = u * u + v * v;
                if ((float)(s) > (float)(0) && (float)(s) < (float)(1))
                {

                    //
                    // two Sqrt's instead of one to
                    // avoid overflow when S is too small
                    //
                    s = (float)(Math.Sqrt(-(2 * Math.Log(s))) / Math.Sqrt(s));
                    x1 = u * s;
                    x2 = v * s;
                    return;
                }
            }
        }

        void hqrndSeed(int s1, int s2, HQRNDState state)
        {

            //
            // Protection against negative seeds:
            //
            //     SEED := -(SEED+1)
            //
            // We can use just "-SEED" because there exists such integer number  N
            // that N<0, -N=N<0 too. (This number is equal to 0x800...000).   Need
            // to handle such seed correctly forces us to use  a  bit  complicated
            // formula.
            //
            if (s1 < 0)
            {
                s1 = -(s1 + 1);
            }
            if (s2 < 0)
            {
                s2 = -(s2 + 1);
            }
            state.S1 = s1 % (hqrndm1 - 1) + 1;
            state.S2 = s2 % (hqrndm2 - 1) + 1;
            state.Magicv = hqrndmagic;
        }

        /*************************************************************************
        Generation of a random uniformly distributed (Haar) orthogonal matrix

        INPUT PARAMETERS:
            N   -   matrix size, N>=1
            
        OUTPUT PARAMETERS:
            A   -   orthogonal NxN matrix, array[0..N-1,0..N-1]

        NOTE: this function uses algorithm  described  in  Stewart, G. W.  (1980),
              "The Efficient Generation of  Random  Orthogonal  Matrices  with  an
              Application to Condition Estimators".
              
              Speaking short, to generate an (N+1)x(N+1) orthogonal matrix, it:
              * takes an NxN one
              * takes uniformly distributed unit vector of dimension N+1.
              * constructs a Householder reflection from the vector, then applies
                it to the smaller matrix (embedded in the larger size with a 1 at
                the bottom right corner).

          -- ALGLIB routine --
             04.12.2009
             Bochkanov Sergey
        *************************************************************************/
        public float[,] GetRandomOrthogonalMatrix(int n)
        {
            int i = 0;
            int j = 0;
            float[,] a = new float[n, n];
            for (i = 0; i <= n - 1; i++)
            {
                for (j = 0; j <= n - 1; j++)
                {
                    if (i == j)
                    {
                        a[i, j] = 1;
                    }
                    else
                    {
                        a[i, j] = 0;
                    }
                }
            }
            rMatrixRndOrthogonalFromTheRight(ref a, n, n);
            return a;
        }


        /*************************************************************************
        Multiplication of MxN matrix by NxN random Haar distributed orthogonal matrix

        INPUT PARAMETERS:
            A   -   matrix, array[0..M-1, 0..N-1]
            M, N-   matrix size

        OUTPUT PARAMETERS:
            A   -   A*Q, where Q is random NxN orthogonal matrix

          -- ALGLIB routine --
             04.12.2009
             Bochkanov Sergey
        *************************************************************************/
        void rMatrixRndOrthogonalFromTheRight(ref float[,] a, int m, int n)
        {
            float tau = 0;
            float lambdav = 0;
            int s = 0;
            int i = 0;
            float u1 = 0;
            float u2 = 0;
            float[] w = new float[0];
            float[] v = new float[0];
            HQRNDState state = new HQRNDState();
            int i_ = 0;
            if (n == 1)
            {

                //
                // Special case
                //
                tau = 2 * rng.Next(2) - 1;
                for (i = 0; i <= m - 1; i++)
                {
                    a[i, 0] = a[i, 0] * tau;
                }
                return;
            }

            //
            // General case.
            // First pass.
            //
            w = new float[m];
            v = new float[n + 1];
            hqrndRandomize(state);
            for (s = 2; s <= n; s++)
            {

                //
                // Prepare random normal v
                //
                do
                {
                    i = 1;
                    while (i <= s)
                    {
                        hqrndNormal2(state, ref u1, ref u2);
                        v[i] = u1;
                        if (i + 1 <= s)
                        {
                            v[i + 1] = u2;
                        }
                        i = i + 2;
                    }
                    lambdav = 0.0f;
                    for (i_ = 1; i_ <= s; i_++)
                    {
                        lambdav += v[i_] * v[i_];
                    }
                }
                while ((float)(lambdav) == (float)(0));

                //
                // Prepare and apply reflection
                //
                generateReflection(ref v, s, ref tau);
                v[1] = 1;
                applyReflectionFromTheRight(ref a, tau, v, 0, m - 1, n - s, n - 1, ref w);
            }

            //
            // Second pass.
            //
            for (i = 0; i <= n - 1; i++)
            {
                tau = 2 * rng.Next(2) - 1;
                for (i_ = 0; i_ <= m - 1; i_++)
                {
                    a[i_, i] = tau * a[i_, i];
                }
            }
        }

        /*************************************************************************
        Generation of an elementary reflection transformation

        The subroutine generates elementary reflection H of order N, so that, for
        a given X, the following equality holds true:

            ( X(1) )   ( Beta )
        H * (  ..  ) = (  0   )
            ( X(n) )   (  0   )

        where
                      ( V(1) )
        H = 1 - Tau * (  ..  ) * ( V(1), ..., V(n) )
                      ( V(n) )

        where the first component of vector V equals 1.

        Input parameters:
            X   -   vector. Array whose index ranges within [1..N].
            N   -   reflection order.

        Output parameters:
            X   -   components from 2 to N are replaced with vector V.
                    The first component is replaced with parameter Beta.
            Tau -   scalar value Tau. If X is a null vector, Tau equals 0,
                    otherwise 1 <= Tau <= 2.

        This subroutine is the modification of the DLARFG subroutines from
        the LAPACK library.

        MODIFICATIONS:
            24.12.2005 sign(Alpha) was replaced with an analogous to the Fortran SIGN code.

          -- LAPACK auxiliary routine (version 3.0) --
             Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
             Courant Institute, Argonne National Lab, and Rice University
             September 30, 1994
        *************************************************************************/
        void generateReflection(ref float[] x, int n, ref float tau)
        {
            int j = 0;
            float alpha = 0;
            float xnorm = 0;
            float v = 0;
            float beta = 0;
            float mx = 0;
            float s = 0;
            int i_ = 0;

            tau = 0;

            if (n <= 1)
            {
                tau = 0;
                return;
            }

            //
            // Scale if needed (to avoid overflow/underflow during intermediate
            // calculations).
            //
            mx = 0;
            for (j = 1; j <= n; j++)
            {
                mx = Math.Max(Math.Abs(x[j]), mx);
            }
            s = 1;
            if ((float)(mx) != (float)(0))
            {
                if ((float)(mx) <= (float)(minrealnumber / machineepsilon))
                {
                    s = minrealnumber / machineepsilon;
                    v = 1 / s;
                    for (i_ = 1; i_ <= n; i_++)
                    {
                        x[i_] = v * x[i_];
                    }
                    mx = mx * v;
                }
                else
                {
                    if ((float)(mx) >= (float)(maxrealnumber * machineepsilon))
                    {
                        s = maxrealnumber * machineepsilon;
                        v = 1 / s;
                        for (i_ = 1; i_ <= n; i_++)
                        {
                            x[i_] = v * x[i_];
                        }
                        mx = mx * v;
                    }
                }
            }

            //
            // XNORM = DNRM2( N-1, X, INCX )
            //
            alpha = x[1];
            xnorm = 0;
            if ((float)(mx) != (float)(0))
            {
                for (j = 2; j <= n; j++)
                {
                    xnorm = xnorm + x[j] / mx * x[j] / mx;
                }
                xnorm = (float)(Math.Sqrt(xnorm) * mx);
            }
            if ((float)(xnorm) == (float)(0))
            {

                //
                // H  =  I
                //
                tau = 0;
                x[1] = x[1] * s;
                return;
            }

            //
            // general case
            //
            mx = Math.Max(Math.Abs(alpha), Math.Abs(xnorm));
            beta = (float)(-(mx * Math.Sqrt(alpha / mx * alpha / mx + xnorm / mx * xnorm / mx)));
            if ((float)(alpha) < (float)(0))
            {
                beta = -beta;
            }
            tau = (beta - alpha) / beta;
            v = 1 / (alpha - beta);
            for (i_ = 2; i_ <= n; i_++)
            {
                x[i_] = v * x[i_];
            }
            x[1] = beta;

            //
            // Scale back outputs
            //
            x[1] = x[1] * s;
        }

        /*************************************************************************
        Application of an elementary reflection to a rectangular matrix of size MxN

        The algorithm post-multiplies the matrix by an elementary reflection transformation
        which is given by column V and scalar Tau (see the description of the
        GenerateReflection procedure). Not the whole matrix but only a part of it
        is transformed (rows from M1 to M2, columns from N1 to N2). Only the
        elements of this submatrix are changed.

        Input parameters:
            C       -   matrix to be transformed.
            Tau     -   scalar defining the transformation.
            V       -   column defining the transformation.
                        Array whose index ranges within [1..N2-N1+1].
            M1, M2  -   range of rows to be transformed.
            N1, N2  -   range of columns to be transformed.
            WORK    -   working array whose indexes goes from M1 to M2.

        Output parameters:
            C       -   the result of multiplying the input matrix C by the
                        transformation matrix which is given by Tau and V.
                        If N1>N2 or M1>M2, C is not modified.

          -- LAPACK auxiliary routine (version 3.0) --
             Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
             Courant Institute, Argonne National Lab, and Rice University
             September 30, 1994
        *************************************************************************/
        void applyReflectionFromTheRight(ref float[,] c, float tau, float[] v, int m1, int m2, int n1, int n2, ref float[] work)
        {
            float t = 0;
            int i = 0;
            int vm = 0;
            int i_ = 0;
            int i1_ = 0;

            if (((float)(tau) == (float)(0) || n1 > n2) || m1 > m2)
            {
                return;
            }
            vm = n2 - n1 + 1;
            for (i = m1; i <= m2; i++)
            {
                i1_ = (1) - (n1);
                t = 0.0f;
                for (i_ = n1; i_ <= n2; i_++)
                {
                    t += c[i, i_] * v[i_ + i1_];
                }
                t = t * tau;
                i1_ = (1) - (n1);
                for (i_ = n1; i_ <= n2; i_++)
                {
                    c[i, i_] = c[i, i_] - t * v[i_ + i1_];
                }
            }
        }

        /*************************************************************************
        Returns random normal number using low-quality system-provided generator
        Marsaglia polar
          -- ALGLIB --
             Copyright 20.03.2009 by Bochkanov Sergey
        *************************************************************************/
        public float RandomNormal(float mu = 0, float sigma = 1)
        {
            float result = 0;
            float u = 0;
            float v = 0;
            float s = 0;

            while (true)
            {
                u = 2 * (float)rng.NextDouble() - 1;
                v = 2 * (float)rng.NextDouble() - 1;
                s = u * u + v * v;
                if ((float)(s) > (float)(0) && (float)(s) < (float)(1))
                {
                    s = (float)(Math.Sqrt(-(2 * Math.Log(s))) / Math.Sqrt(s));
                    result = u * s;
                    //if(result > -1.0f && result < 1.0f)
                    break;
                }
            }
            result = mu + sigma * result;
            return (result / 3 + 1.0f) / 2;
        }

        /// <summary>
        /// Box-Muller
        /// </summary>
        /// <param name="mu"></param>
        /// <param name="sigma"></param>
        /// <returns></returns>
        public double NextGaussian(double mu = 0, double sigma = 1)
        {
            var u1 = rng.NextDouble();
            var u2 = rng.NextDouble();

            var rand_std_normal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                                Math.Sin(2.0 * Math.PI * u2);

            var rand_normal = mu + sigma * rand_std_normal;

            return rand_normal;
        }

        /// <summary>
        /// Ornstein–Uhlenbeck process
        /// dx_{t}=\theta (\mu -x_{t})\,dt+\sigma \,dW_{t}
        /// </summary>
        /// <param name="x"></param>
        /// <param name="theta"></param>
        /// <param name="delta"></param>
        /// <param name="mu"></param>
        /// <param name="sigma"></param>
        /// <returns></returns>
        public double NextDiscreteOU(double x, double theta, double delta, double mu = 0)
        {
            double dx = theta * (mu - x) + delta * RandomNormal(0, 1);
            return x + dx;
        }
    }

}
