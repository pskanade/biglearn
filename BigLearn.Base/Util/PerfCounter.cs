﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BigLearn
{
    public class PerfCounter
    {
        public delegate void PerfOperation();

        protected string counterName;
        protected long totalTicks;
        protected int count;
        protected long? min;
        protected long? max;

        protected PerfCounter(string name)
        {
            this.counterName = name;
            this.totalTicks = 0;
            this.count = 0;
        }

        public double TotalMilliSeconds
        {
            get
            {
                return this.totalTicks / (double)(Stopwatch.Frequency / 1000.0);
            }
        }

        public int Count
        {
            get { return count; }
        }

        public double Average
        {
            get
            {
                if (this.count == 0)
                {
                    return 0;
                }
                else
                {
                    return this.TotalMilliSeconds / this.count;
                }
            }
        }

        public double Min
        {
            get
            {
                return this.min == null ? 0 : this.min.Value / (double)(Stopwatch.Frequency / 1000.0);
            }
        }

        public double Max
        {
            get
            {
                return this.max == null ? 0 : this.max.Value / (double)(Stopwatch.Frequency / 1000.0);
            }
        }

        public void Reset()
        {
            this.count = 0;
            this.totalTicks = 0;
            this.min = this.max = null;
        }

        public void CountOperation(PerfOperation op)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            op();
            sw.Stop();
            this.Add(sw.ElapsedTicks);
            this.count++;
        }

        public Stopwatch Begin()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            return sw;
        }

        public void TakeCount(Stopwatch sw)
        {
            sw.Stop();
            this.Add(sw.ElapsedTicks);
            this.count++;
        }

        public void Add(long ticks)
        {
            this.totalTicks += ticks;
            if (this.max == null || ticks > this.max.Value)
            {
                this.max = ticks;
            }

            if (this.min == null || ticks < this.min.Value)
            {
                this.min = ticks;
            }
        }

        public override string ToString()
        {
            return string.Format("Counter: {0}, Count: {1}, Total: {2}ms, Avg: {3}ms, Max: {4}ms, Min: {5}ms", this.counterName, this.Count, this.TotalMilliSeconds, this.Average, this.Max, this.Min);
        }

        public class Manager
        {
            private Timer timer;

            public Manager()
            {
                if (this.verbose)
                {
                    this.timer = new Timer(s => this.DumpStatistics(), null, 0, 60000);
                }
            }

            static void CurrentDomain_DomainUnload(object sender, EventArgs e)
            {
                Manager.Instance.DumpStatistics();
            }

            public void DumpStatistics()
            {
                if (!verbose)
                {
                    return;
                }

                Console.WriteLine();
                Console.WriteLine("***Performance counters statistics***");
                Console.WriteLine("*****************Start***************");
                var counters = this.perfCounters.Values.ToArray().OrderByDescending(c => c.TotalMilliSeconds);
                foreach (var cnt in counters)
                {
                    Console.WriteLine(cnt);
                }

                Console.WriteLine("*****************End***************");
                Console.WriteLine();
            }

            private Dictionary<string, PerfCounter> perfCounters = new Dictionary<string, PerfCounter>(StringComparer.OrdinalIgnoreCase);
            private bool verbose = false;

            public bool Verbose
            {
                get
                {
                    return this.verbose;
                }
                set
                {
                    this.verbose = value;
                }
            }

            public PerfCounter this[string name]
            {
                get
                {
                    if (perfCounters.ContainsKey(name))
                    {
                        return perfCounters[name];
                    }
                    else
                    {
                        PerfCounter cnt = new PerfCounter(name);
                        perfCounters.Add(name, cnt);
                        return cnt;
                    }
                }
            }

            private static Manager instance = new Manager();

            public static Manager Instance
            {
                get
                {
                    return instance;
                }
            }

            private static int defaultMaxThreads = 0;
            private static int defaultMaxCPThreads = 0;
            private static int defaultMinThreads = 0;
            private static int defaultMinCPThreads = 0;

            public static void DisablePLINQ(bool disable)
            {
                if (disable)
                {
                    System.Threading.ThreadPool.GetMaxThreads(out defaultMaxThreads, out defaultMaxCPThreads);
                    System.Threading.ThreadPool.GetMinThreads(out defaultMinThreads, out defaultMinCPThreads);
                    System.Threading.ThreadPool.SetMaxThreads(1, 1);
                    System.Threading.ThreadPool.SetMinThreads(1, 1);
                }
                else
                {
                    if (defaultMaxThreads == 0)
                    {
                        return;
                    }

                    System.Threading.ThreadPool.SetMinThreads(defaultMinThreads, defaultMinCPThreads);
                    System.Threading.ThreadPool.SetMaxThreads(defaultMaxThreads, defaultMaxCPThreads);
                }
            }
        }
    }
}
