﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BigLearn
{
    public class CommonExtractor
    {
        public static IEnumerable<string> StreamRead(StreamReader mreader, int column = -1)
        {
            while (!mreader.EndOfStream)
            {
                string line = mreader.ReadLine();
                if (column != -1) { yield return line.Split('\t')[column]; }
                else yield return line;
            }
        }

        public static List<Tuple<List<int>, List<int>>> LoadPairSequence(StreamReader srcReader, StreamReader tgtReader)
        {
            List<Tuple<List<int>, List<int>>> Data = new List<Tuple<List<int>, List<int>>>();
            while (!srcReader.EndOfStream && !tgtReader.EndOfStream)
            {
                string[] srcItems = srcReader.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                string[] tgtItems = tgtReader.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                List<int> srcInts = srcItems.Select(i => int.Parse(i)).ToList();
                List<int> tgtInts = tgtItems.Select(i => int.Parse(i)).ToList();

                Data.Add(new Tuple<List<int>, List<int>>(srcInts, tgtInts));
            }
            return Data;
        }

        /// <summary>
        /// inputPairStream is "src string"\t"tgt string"
        /// srcBinaryWriter : SeqSparseBatchData
        /// tgtBinarywriter : SeqSparseBatchData
        /// </summary>
        /// <param name="pairText"></param>
        /// <param name="srcHash"></param>
        /// <param name="tgtHash"></param>
        public static IEnumerable<Tuple<List<Dictionary<int, float>>, List<Dictionary<int, float>>>> ExtractPairText(IEnumerable<Tuple<string, string>> pairText,
            BasicTextHash srcHash, BasicTextHash tgtHash, bool srcEmptyFilter, bool tgtEmptyFilter)
        {
            int rowIdx = 0;
            foreach (Tuple<string, string> item in pairText)
            {
                string src_string = item.Item1;
                string tgt_string = item.Item2;

                List<Dictionary<int, float>> srcfea = srcHash.SeqFeatureExtract(src_string);
                List<Dictionary<int, float>> tgtfea = tgtHash.SeqFeatureExtract(tgt_string);

                if (srcfea.Count == 0)
                {
                    if (srcEmptyFilter) continue;
                    Dictionary<int, float> empty = new Dictionary<int, float>();
                    empty[0] = 0; srcfea.Add(empty);
                }
                if (tgtfea.Count == 0)
                {
                    if (tgtEmptyFilter) continue;
                    Dictionary<int, float> empty = new Dictionary<int, float>();
                    empty[0] = 0;
                    tgtfea.Add(empty);
                }

                if (++rowIdx % 100000 == 0) { Console.WriteLine("Processing text row {0} at {1}", rowIdx, DateTime.Now); }

                yield return new Tuple<List<Dictionary<int, float>>, List<Dictionary<int, float>>>(srcfea, tgtfea);
            }
        }


        /// <summary>
        /// inputPairStream is "src string"\t"tgt string"
        /// srcBinaryWriter : SeqSparseBatchData
        /// tgtBinarywriter : SeqSparseBatchData
        /// </summary>
        /// <param name="pairText"></param>
        /// <param name="srcHash"></param>
        /// <param name="tgtHash"></param>
        public static IEnumerable<Tuple<List<Dictionary<int, float>>, List<Dictionary<int, float>>, float>> ExtractPairText(IEnumerable<Tuple<string, string, float>> pairText, 
            BasicTextHash srcHash, BasicTextHash tgtHash, bool srcEmptyFilter, bool tgtEmptyFilter)
        {
            int rowIdx = 0;
            foreach (Tuple<string, string, float> item in pairText)
            {
                string src_string = item.Item1;
                string tgt_string = item.Item2;

                List<Dictionary<int, float>> srcfea = srcHash.SeqFeatureExtract(src_string);
                List<Dictionary<int, float>> tgtfea = tgtHash.SeqFeatureExtract(tgt_string);

                if (srcfea.Count == 0) { if (srcEmptyFilter) continue; Dictionary<int, float> empty = new Dictionary<int, float>(); empty[0] = 0; srcfea.Add(empty); }
                if (tgtfea.Count == 0) { if (tgtEmptyFilter) continue; Dictionary<int, float> empty = new Dictionary<int, float>(); empty[0] = 0; tgtfea.Add(empty); }

                if (++rowIdx % 100000 == 0) { Console.WriteLine("Processing text row {0} at:{1}", rowIdx, DateTime.Now); }

                yield return new Tuple<List<Dictionary<int, float>>, List<Dictionary<int, float>>, float>(srcfea, tgtfea, item.Item3);
            }
        }


        public static IEnumerable<Tuple<List<Dictionary<int, float>>, List<List<Dictionary<int, float>>>>> ExtractPairText_TgtSent(IEnumerable<Tuple<string, string>> pairText,
           BasicTextHash srcHash, BasicTextHash tgtHash, bool srcEmptyFilter, bool tgtEmptyFilter)
        {
            int rowIdx = 0;
            foreach (Tuple<string, string> item in pairText)
            {
                string src_string = item.Item1;
                string tgt_string = item.Item2;

                List<Dictionary<int, float>> srcfea = srcHash.SeqFeatureExtract(src_string);
                List<List<Dictionary<int, float>>> tgtfeas = tgtHash.MultiSeqFeatureExtract(tgt_string);

                if (srcfea.Count == 0) { if (srcEmptyFilter) continue; Dictionary<int, float> empty = new Dictionary<int, float>(); empty[0] = 0; srcfea.Add(empty); }
                if (tgtfeas.Count == 0)
                {
                    if (tgtEmptyFilter) continue; Dictionary<int, float> empty = new Dictionary<int, float>(); empty[0] = 0;
                    List<Dictionary<int, float>> emps = new List<Dictionary<int, float>>(); emps.Add(empty); tgtfeas.Add(emps);
                }

                if (++rowIdx % 100000 == 0) { Console.WriteLine("Processing text row {0} at :{1}", rowIdx, DateTime.Now); }

                yield return new Tuple<List<Dictionary<int, float>>, List<List<Dictionary<int, float>>>>(srcfea, tgtfeas);
            }
        }

        /// <summary>
        /// inputPairStream is "src string"
        /// srcBinaryWriter : SeqSparseBatchData
        /// </summary>
        /// <param name="pairText"></param>
        /// <param name="srcHash"></param>
        /// <param name="tgtHash"></param>
        public static IEnumerable<List<Dictionary<int, float>>> ExtractSeqTextFea(IEnumerable<string> text, BasicTextHash hash, bool emptyFilter)
        {
            int rowIdx = 0;
            foreach (string src_string in text)
            {
                List<Dictionary<int, float>> srcfea = hash.SeqFeatureExtract(src_string);
                if (srcfea.Count == 0) { if (emptyFilter) continue; Dictionary<int, float> empty = new Dictionary<int, float>(); empty[0] = 0; srcfea.Add(empty); }
                if (++rowIdx % 100000 == 0) { Console.WriteLine("Processing text row {0}", rowIdx); }
                yield return srcfea;
            }
        }


        public static IEnumerable<T> RandomShuffle<T>(IEnumerable<T> inputList, int shuffleCache, int randomSeed)
        {
            List<T> Cache = new List<T>();
            Random random = new Random(randomSeed);
            foreach (T item in inputList)
            {
                if (Cache.Count >= shuffleCache)
                {
                    int idx = random.Next(Cache.Count);
                    yield return Cache[idx];
                    Cache.RemoveAt(idx);
                }
                Cache.Add(item);
            }

            DataRandomShuffling shuffle = new DataRandomShuffling(Cache.Count);
            for (int i = 0; i < Cache.Count; i++)
            {
                int idx = shuffle.RandomNext();
                yield return Cache[idx];
            }
        }

        /// <summary>
        /// Task 1 : List<Dictionary<int, float>> --> binary file.
        /// Task 2 : List<Dictionary<int, float>> --> list<binary file>  --> binary.
        /// </summary>
        /// <param name="seqDict"></param>
        /// <param name="featureDim"></param>
        /// <param name="miniBatchSize"></param>
        /// <returns></returns>
        //public static IEnumerable<SeqSparseBatchData> ExtractSeqSparseDataBinary(IEnumerable<List<Dictionary<int, float>>> seqDict, int featureDim, int miniBatchSize)
        //{
        //    //var globalStat = ;
        //    int rowIdx = 0;
        //    SeqSparseBatchData binaryData = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = featureDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
        //    foreach (List<Dictionary<int, float>> item in seqDict)
        //    {
        //        binaryData.PushSample(item);
        //        if (++rowIdx % 100000 == 0) { Console.WriteLine("Processing text row {0}", rowIdx); }
        //        if (binaryData.BatchSize >= miniBatchSize)
        //        {
        //            binaryData.MakeOneBatchStat();
        //            yield return binaryData;
        //            binaryData = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = featureDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
        //        }
        //    }
        //    binaryData.MakeOneBatchStat();
        //    yield return binaryData;
        //}

        public static MemoryStream ExtractSeqSparseDataBinary(IEnumerable<List<Dictionary<int, float>>> seqDict, int featureDim, int miniBatchSize)
        {
            MemoryStream mem = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(mem); //FileUtil.CreateBinaryWrite(LogFile + isSrc ? ".src.tmp.writer" : ".tgt.tmp.writer");
            SeqSparseBatchData binaryData = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = featureDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);

            int rowIdx = 0;
            //SeqSparseBatchData binaryData = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = featureDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
            foreach (List<Dictionary<int, float>> item in seqDict)
            {
                binaryData.PushSample(item);
                if (++rowIdx % 100000 == 0) { Console.WriteLine("Processing text row {0}", rowIdx); }
                if (binaryData.BatchSize >= miniBatchSize) { binaryData.PopBatchToStat(writer); }
            }
            binaryData.PopBatchToStat(writer);
            binaryData.Stat.Save(writer);
            return mem;
        }


        public static SeqSparseBatchData ExtractSeqSparseDataBinary(IEnumerable<List<Dictionary<int, float>>> seqDict, int featureDim, int miniBatchSize, BinaryWriter writer)
        {
            int rowIdx = 0;
            SeqSparseBatchData binaryData = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = featureDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
            foreach (List<Dictionary<int, float>> item in seqDict)
            {
                binaryData.PushSample(item);
                if (++rowIdx % 100000 == 0) { Console.WriteLine("Processing text row {0}", rowIdx); }
                if (binaryData.BatchSize >= miniBatchSize)
                {
                    binaryData.PopBatchToStat(writer);
                }
            }
            binaryData.PopBatchCompleteStat(writer);
            return binaryData;
        }

        public static SeqSparseDataSource ExtractSeqSparseDataBinary(IEnumerable<IEnumerable<int>> sentences, int vocabDim, int miniBatchSize, BinaryWriter writer)
        {
            int rowIdx = 0;
            SeqSparseDataSource binaryData = new SeqSparseDataSource(new SequenceDataStat() { FEATURE_DIM = vocabDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
            foreach (List<int> item in sentences)
            {
                List<Tuple<Dictionary<int, float>, float>> fea = new List<Tuple<Dictionary<int, float>, float>>();
                for (int sIdx = 0; sIdx < item.Count - 1; sIdx++)
                {
                    int srcId = item[sIdx];
                    Dictionary<int, float> d = new Dictionary<int, float>();
                    d.Add(srcId, 1);
                    fea.Add(new Tuple<Dictionary<int, float>, float>(d, item[sIdx + 1]));
                }
                if (fea.Count == 0)
                {
                    Dictionary<int, float> d = new Dictionary<int, float>();
                    d.Add(0, 0);
                    fea.Add(new Tuple<Dictionary<int, float>, float>(d, 1));
                }
                binaryData.PushSample(fea); 

                if (++rowIdx % 100000 == 0) { Console.WriteLine("Processing text row {0}", rowIdx); }
                if (binaryData.BatchSize >= miniBatchSize) { binaryData.PopBatchToStat(writer); }
            }
            binaryData.PopBatchCompleteStat(writer);
            return binaryData;
        }

        public static DenseBatchData ExtractLabelDataBinary(IEnumerable<float> label, int miniBatchSize, BinaryWriter writer)
        {
            int rowIdx = 0;
            DenseBatchData binaryData = new DenseBatchData(new DenseDataStat() { Dim = 1 }, DeviceType.CPU);
            foreach (float l in label)
            {
                binaryData.PushSample(new float[] { l }, 1);
                if (++rowIdx % 100000 == 0) { Console.WriteLine("Processing text row {0}", rowIdx); }
                if (binaryData.BatchSize >= miniBatchSize) { binaryData.PopBatchToStat(writer); }
            }
            binaryData.PopBatchCompleteStat(writer);
            return binaryData;
        }

        public static Tuple<SeqSparseBatchData, SeqSparseBatchData> ExtractPairSeqSparseDataBinary(
            IEnumerable<Tuple<List<Dictionary<int, float>>, List<Dictionary<int, float>>>> seqDict,
            int feaDim1, BinaryWriter writer1, int feaDim2, BinaryWriter writer2, int miniBatchSize)
        {
            int rowIdx = 0;
            SeqSparseBatchData binaryData1 = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = feaDim1, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
            SeqSparseBatchData binaryData2 = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = feaDim2, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);

            foreach (Tuple<List<Dictionary<int, float>>, List<Dictionary<int, float>>> item in seqDict)
            {
                binaryData1.PushSample(item.Item1);
                binaryData2.PushSample(item.Item2);

                if (++rowIdx % 100000 == 0) { Console.WriteLine("Processing text row {0}", rowIdx); }
                if (binaryData1.BatchSize >= miniBatchSize && binaryData2.BatchSize >= miniBatchSize)
                {
                    binaryData1.PopBatchToStat(writer1);
                    binaryData2.PopBatchToStat(writer2);
                }
            }
            binaryData1.PopBatchCompleteStat(writer1);
            binaryData2.PopBatchCompleteStat(writer2);
            return new Tuple<SeqSparseBatchData, SeqSparseBatchData>(binaryData1, binaryData2);
        }
        
        public static Tuple<SeqSparseBatchData, SeqSparseBatchData, DenseBatchData> ExtractPairSeqSparseDataBinary(
            IEnumerable<Tuple<List<Dictionary<int, float>>, List<Dictionary<int, float>>, float>> seqDict,
            int feaDim1, BinaryWriter writer1, int feaDim2, BinaryWriter writer2, BinaryWriter weiWriter, int miniBatchSize)
        {
            int rowIdx = 0;
            SeqSparseBatchData binaryData1 = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = feaDim1, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
            SeqSparseBatchData binaryData2 = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = feaDim2, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
            DenseBatchData scoreBatchData = new DenseBatchData(new DenseDataStat() { Dim = 1, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
            foreach (Tuple<List<Dictionary<int, float>>, List<Dictionary<int, float>>, float> item in seqDict)
            {
                binaryData1.PushSample(item.Item1);
                binaryData2.PushSample(item.Item2);
                scoreBatchData.PushSample(new float[] { item.Item3 }, 1);

                if (++rowIdx % 100000 == 0) { Console.WriteLine("Processing text row {0}", rowIdx); }
                if (binaryData1.BatchSize >= miniBatchSize && binaryData2.BatchSize >= miniBatchSize)
                {
                    binaryData1.PopBatchToStat(writer1);
                    binaryData2.PopBatchToStat(writer2);
                    if (weiWriter != null)
                        scoreBatchData.PopBatchToStat(weiWriter);
                }
            }
            binaryData1.PopBatchCompleteStat(writer1);
            binaryData2.PopBatchCompleteStat(writer2);
            if (weiWriter != null)
                scoreBatchData.PopBatchCompleteStat(weiWriter);
            return new Tuple<SeqSparseBatchData, SeqSparseBatchData, DenseBatchData>(binaryData1, binaryData2, scoreBatchData);
        }


        public static S DataStatAllReducer<S>(IEnumerable<S> subStat) where S : DataStat, new()
        {
            S reduceStat = new S();
            foreach (S stat in subStat) reduceStat.Add(stat);
            return reduceStat;
        }


    }
}
