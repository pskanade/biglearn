﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace BigLearn
{
    public static class Cudalib
    {
        /**************Cuda Basic Information************************/
        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static void CudaDeviceSynchronize();


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static int CudaDeviceCount();

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static IntPtr CudaCtxGetCurrent();

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static int CudaCtxSetCurrent(IntPtr ctx);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.BStr)]
        public unsafe static extern string CudaDeviceProperties(int i);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static int CudaSetDevice(int device);

        /**************Cuda  Memory Operation************************/
        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern IntPtr CudaAllocInt(int e);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern IntPtr CudaAllocFloat(int e);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern int CudaMemGetInfo(IntPtr free, IntPtr total);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CudaDeallocFloat(IntPtr gpu_floats);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CudaDeallocInt(IntPtr gpu_ints);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern int CudaCopyInInt(IntPtr gpu_ints, IntPtr int_array, int len);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern int CudaCopyInFloat(IntPtr gpu_floats, IntPtr float_array, int len);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CudaCopy(IntPtr gpu_floats_a, IntPtr gpu_float_b, int m, int aIdx, int bIdx);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern int CudaCopyOutFloat(IntPtr gpu_floats, IntPtr float_array, int len);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern int CudaCopyOutInt(IntPtr gpu_ints, IntPtr int_array, int len);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern int Zero(IntPtr gpu_floats, int len);

        /*******************Activation Function *****************************************/
        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Logistic(IntPtr a, IntPtr b, int batchsize, float gamma);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Tanh(IntPtr a, IntPtr b, int batchsize);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void ReLU(IntPtr a, IntPtr b, int batchsize);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Log(IntPtr a, IntPtr b, float alpha, float beta, int size);

        /*******************RNN Math Computation Lib *****************************************/
        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RecursiveForwardPropagateBatch(IntPtr InstanceIndex, int batchSize,
                                    IntPtr SeqInputMatrixs, int dim,
                                    IntPtr WMatrix, int lag, int af, IntPtr SeqOutputMatrix);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RecursiveBackwardPropagateBatch(IntPtr InstanceIndex, int batchSize,
                                            IntPtr SeqOutputMatrixs, int dim,
                                            IntPtr WMatrix, int lag, int af, IntPtr SeqDerivMatrixs);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RecursiveUpdateBatch(IntPtr InstanceIndex, int batchSize, int seqSize, IntPtr SeqOutputMatrixs, int dim,
                                           IntPtr SeqDerivMatrixs, int lag, IntPtr gradient, float weight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RecursiveExtendUpdateBatch(IntPtr InstanceIndex, int batchSize, int seqSize, IntPtr SeqOutputMatrixs,
                                            IntPtr pre_Idx, //-> previous output,
                                            int pre_batchSize,  //->x;
                                            IntPtr pre_O,
                                            int dim, IntPtr SeqDerivMatrixs, int lag, IntPtr gradient, float weight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RNNForwardPropagate_Simple(IntPtr smpIdx, int batchSize, int seqSize, IntPtr Wmatrix, int lag, int hiddenDim, int af, IntPtr seqOut);


        /*******************LSTM Math Computation Lib *****************************************/
        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMExtendForwardPropagateBatch(IntPtr srcInstanceIdx, int srcBatchSize,  //->src_x;
            IntPtr pre_o, //-> previous output,
            IntPtr pre_c, //-> previous c,
            IntPtr InstanceIndex, int batchSize,  //->x;
            IntPtr o, //-> output,
            IntPtr gate_i, //->gate i,
            IntPtr c_hat, //->candidate memory,
            IntPtr gate_f, //->forget gate,
            IntPtr c, //->memory,
            IntPtr gate_o, //->gate o,
            IntPtr tanhc, //->tanh memory
            int cell, IntPtr Ui, IntPtr Uc, IntPtr Uf, IntPtr Uo, IntPtr Vo);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMForwardPropagateBatch(IntPtr InstanceIndex, int batchSize,  //->x;
            IntPtr o, //-> output,
            IntPtr gate_i, //->gate i,
            IntPtr c_hat, //->candidate memory,
            IntPtr gate_f, //->forget gate,
            IntPtr c, //->memory,
            IntPtr gate_o, //->gate o,
            IntPtr tanhc, //->tanh memory
            int cell, IntPtr Ui, IntPtr Uc, IntPtr Uf, IntPtr Uo, IntPtr Vo);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMForwardPropagateBatchOpt(IntPtr InstanceIndex, int batchSize,  //->x;
            IntPtr o, //-> output,
            IntPtr gate_i, //->gate i,
            IntPtr c_hat, //->candidate memory,
            IntPtr gate_f, //->forget gate,
            IntPtr c, //->memory,
            IntPtr gate_o, //->gate o,
            IntPtr tanhc, //->tanh memory
            int cell, IntPtr Ui, IntPtr Uc, IntPtr Uf, IntPtr Uo, IntPtr Vo);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMLinkExtendForwardPropagateBatch(
            IntPtr pre_o, //-> previous output,
            IntPtr pre_c, //-> previous c,
            IntPtr InstanceIndex, int batchSize,  //->x;
            IntPtr pre_idx,
            IntPtr o, //-> output,
            IntPtr gate_i, //->gate i,
            IntPtr c_hat, //->candidate memory,
            IntPtr gate_f, //->forget gate,
            IntPtr c, //->memory,
            IntPtr gate_o, //->gate o,
            IntPtr tanhc, //->tanh memory
            int cell,
            IntPtr Ui,
            IntPtr Uc,
            IntPtr Uf,
            IntPtr Uo, IntPtr Vo);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMBackwardPropagateBatch(IntPtr InstanceIndex, int batchSize, int seqSize, int cell,
            IntPtr o, IntPtr gate_i, IntPtr c_hat, IntPtr gate_f, IntPtr c, IntPtr gate_o, IntPtr tanhc,
            IntPtr deriv_o, IntPtr deriv_gate_i, IntPtr deriv_cHat,
            IntPtr deriv_gate_f, IntPtr deriv_c, IntPtr deriv_gate_o, IntPtr deiv_tanhc,
            IntPtr Ui, IntPtr Uc, IntPtr Uf, IntPtr Uo, IntPtr Vo);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMBackwardPropagateBatchOpt(IntPtr InstanceIndex, int batchSize, int seqSize, int cell,
            IntPtr o, IntPtr gate_i, IntPtr c_hat, IntPtr gate_f, IntPtr c, IntPtr gate_o, IntPtr tanhc,
            IntPtr deriv_o, IntPtr deriv_gate_i, IntPtr deriv_cHat,
            IntPtr deriv_gate_f, IntPtr deriv_c, IntPtr deriv_gate_o, IntPtr deiv_tanhc,
            IntPtr Ui, IntPtr Uc, IntPtr Uf, IntPtr Uo, IntPtr Vo);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMExtendBackwardPropagateBatch(IntPtr preInstanceIdx, int preBatchSize,  //->src_x;
            IntPtr pre_o, //-> previous output,
            IntPtr pre_c, //-> previous c,
            IntPtr deriv_pre_o,	// -> previous deriv_o
            IntPtr deriv_pre_c,	// -> previous deriv_c
            IntPtr InstanceIndex, int batchSize, int seqSize, int cell,
            IntPtr o, IntPtr gate_i, IntPtr c_hat, IntPtr gate_f, IntPtr c, IntPtr gate_o, IntPtr tanhc,
            IntPtr deriv_o, IntPtr deriv_gate_i, IntPtr deriv_cHat, IntPtr deriv_gate_f, IntPtr deriv_c, IntPtr deriv_gate_o, IntPtr deiv_tanhc,
            IntPtr Ui, IntPtr Uc, IntPtr Uf, IntPtr Uo, IntPtr Vo);

        /*******************GRU Math Computation Lib *****************************************/
        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void GRUForwardPropagateBatch(IntPtr InstanceIndex, int batchSize, int Dim,
            IntPtr H, //->hidden,
            IntPtr GateR, //->reset gate,
            IntPtr GateZ, //->update gate,
            IntPtr HHat, //->chat
            IntPtr RestH,
            IntPtr Ur,
            IntPtr Uz,
            IntPtr Uh);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void GRUBackwardPropagateBatch(
            IntPtr InstanceIndex, int batchSize, int Dim,//->x
            IntPtr H, //->hidden
            IntPtr GateR, //->reset gate,
            IntPtr GateZ, //->update gate,
            IntPtr HHat, //->hat
            IntPtr RestH, //H @ R
            IntPtr DerivH,
            IntPtr DerivGateR,
            IntPtr DerivGateZ,
            IntPtr DerivHHat,
            IntPtr DerivRestH,
            IntPtr Ur,
            IntPtr Uz,
            IntPtr Uh);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern float L2Norm(IntPtr x, int size);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RMSPropGradient(IntPtr ada, IntPtr grad, float gamma, float epsilon, int m);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RMSPropV2_Gradient(IntPtr ada, IntPtr g, IntPtr grad, float gamma, float epsilon, int m);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void VectorSquareAdd(IntPtr Z, IntPtr X, IntPtr Y, float wz, float wx, float wy, int m);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void AdaMGradient(IntPtr M, IntPtr V, IntPtr G, float alpha, float beta1, float beta2, float epsilon, int t, int size);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void AdaDeltaGradient(IntPtr deltaX, IntPtr AccumGrad, IntPtr AccumUpdate, IntPtr Grad, float epsilon, int size);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void AdaMax(IntPtr V, IntPtr M, IntPtr G, float alpha, float beta, float epsilon, int m);

        /*******************Embedding Learning Lib *****************************************/
        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void HierarcialSoftmaxPairEmbedding(
            IntPtr srcLabels, int srcLabelCount, IntPtr srcEmbedding, IntPtr srcDeriv,
            IntPtr tgtLabels, int tgtLabelCount, IntPtr tgtEmbedding, IntPtr tgtDeriv,
            int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            IntPtr cSpace, IntPtr cBias, int cSize,
            float step, // step size for update vSpace.
            IntPtr v2c, // word 2 class,
            IntPtr classIdx, // class 2 word number
            IntPtr wordIdx, // class 2 word index
            IntPtr wordClassIdx, //word 2 class segment index.
            float gamma,
            IntPtr classAct,
            IntPtr wordAct,
            IntPtr wordActIdx1,
            IntPtr wordActIdx2,
            int wordSegSize, // word Max SegmentSize,
            IntPtr targetProb);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void HierarcialSoftmaxPairProbability(
            IntPtr srcLabels, int srcLabelCount, IntPtr srcEmbedding, IntPtr srcDeriv,
            IntPtr tgtLabels, int tgtLabelCount, IntPtr tgtEmbedding, IntPtr tgtDeriv,
            int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            IntPtr cSpace, IntPtr cBias, int cSize,
            IntPtr v2c, // word 2 class,
            IntPtr classIdx, // class 2 word number
            IntPtr wordIdx, // class 2 word index
            IntPtr wordClassIdx, //word 2 class segment index.
            float gamma,
            IntPtr classAct,
            IntPtr wordAct,
            IntPtr wordActIdx1,
            IntPtr wordActIdx2,
            int wordSegSize, // word Max SegmentSize,
            IntPtr targetProb);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void FullySoftmaxPairProbability(
            IntPtr srcLabels, int srcLabelCount, IntPtr srcEmbedding,
            IntPtr tgtLabels, int tgtLabelCount, IntPtr tgtEmbedding,
            int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            float step, // step size for update vSpace.
            float gamma,
            IntPtr wordAct,
            IntPtr targetProb);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void HierarcicalSoftmaxProbability(IntPtr targets, int targetNum,
            IntPtr embedding, int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            IntPtr cSpace, IntPtr cBias, int cSize,

            IntPtr v2c, // word 2 class,
            IntPtr classIdx, // class 2 word number
            IntPtr wordIdx, // class 2 word index
            IntPtr wordClassIdx, //word 2 class segment index.
            float gamma,
            IntPtr classOutput,
            IntPtr wordOutput,
            IntPtr wordActIdx1,
            IntPtr wordActIdx2,
            int wordSegSize, // word Max SegmentSize,
            IntPtr targetProb);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void HierarcialSoftmaxEmbeddingV2(IntPtr targets, int targetNum,
            IntPtr embedding, IntPtr deriv, int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            IntPtr cSpace, IntPtr cBias, int cSize,
            float step,
            IntPtr v2c, // word 2 class,
            IntPtr classIdx, // class 2 word number
            IntPtr wordIdx, // class 2 word index
            IntPtr wordClassIdx, //word 2 class segment index.
            float gamma,
            IntPtr classOutput,
            IntPtr wordOutput,
            IntPtr wordActIdx1,
            IntPtr wordActIdx2,
            int wordSegSize, // word Max SegmentSize,
            IntPtr targetProb);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void HierarcialSoftmaxDecoding(IntPtr embedding, int batchSize, int dim,
            IntPtr vSpace, IntPtr vBias, int vSize,
            IntPtr cSpace, IntPtr cBias, int cSize,
            int bestN, float gamma,
            IntPtr v2c, IntPtr tmpClassOutput, IntPtr tmpWordOutput,
            IntPtr wordSet, IntPtr wordProb);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void FullySoftmaxPairEmbedding(
            IntPtr srcLabels, int srcLabelCount, IntPtr srcEmbedding, IntPtr srcDeriv,
            IntPtr tgtLabels, int tgtLabelCount, IntPtr tgtEmbedding, IntPtr tgtDeriv,
            int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            float step, // step size for update vSpace.
            float gamma,
            IntPtr wordAct,
            IntPtr targetProb);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void FullySoftmaxDecoding(IntPtr embedding, int batchSize, int dim,
            IntPtr vSpace, IntPtr vBias, int vSize,
            int bestN, float gamma, IntPtr tmpWordOutput,
            IntPtr wordSet, IntPtr wordProb);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void HierarcialSoftmaxDecoding(IntPtr embedding, int batchSize, int dim,
            IntPtr vSpace, IntPtr vBias, int vSize,
            IntPtr cSpace, IntPtr cBias, int cSize,
            int bestN, float gamma,
            IntPtr v2c,
            IntPtr classIdx,
            IntPtr wordIdx,
            IntPtr tmpClassOutput, IntPtr tmpWordOutput,
            IntPtr wordSet, IntPtr wordProb);

        /*******************Embedding Learning Lib *****************************************/

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RectifiedGaussian(IntPtr locationForcusValue, int ensembleLen, int batchSize, IntPtr smpIdx, int windowSize,
            float gamma, IntPtr locationIdx, IntPtr locationValue);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Gaussian(IntPtr locationForcusValue, int ensembleLen, int batchSize, IntPtr smpIdx, float gamma, IntPtr locationValue);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivGaussian(IntPtr locationForcusValue, int ensembleLen, int batchSize, IntPtr smpIdx,
            float gamma, IntPtr locationValue, IntPtr locationForcusDeriv, IntPtr locationDeriv);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivRectifiedGaussian(IntPtr locationForcusValue, int ensembleLen, int batchSize, IntPtr smpIdx, int windowSize,
            float gamma, IntPtr locationIdx, IntPtr locationValue, IntPtr locationForcusDeriv, IntPtr locationDeriv);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void MemoryAddressing(IntPtr smpIdx, int batchSize, IntPtr memoryInput, IntPtr locationIdx,
            IntPtr locationValue, int ensembleLen, int windowSize, int memoryVecDim, IntPtr memoryOutput);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void GaussianMemoryAddressing(IntPtr smpIdx, int batchSize, IntPtr memoryInput,
            IntPtr locationValue, int ensembleLen, int memoryVecDim, IntPtr memoryOutput);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivGaussianMemoryAddressing(IntPtr sentMargin, int sequenceSize, IntPtr memoryInput, IntPtr locationValueDeriv,
            int memoryVecDim, IntPtr memoryOutputDeriv);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivMemoryAddressing(IntPtr smpIdx, int batchSize, IntPtr memoryInput, IntPtr locationIdx, IntPtr locationValueDeriv,
            int ensembleLen, int windowSize, int memoryVecDim, IntPtr memoryOutputDeriv);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivMemory(IntPtr smpIdx, int batchSize, IntPtr memoryInputDeriv, IntPtr locationIdx, IntPtr locationValue,
            int ensembleLen, int windowSize, int memoryVecDim, IntPtr memoryOutputDeriv);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivGaussianMemory(IntPtr sentMargin, int sequenceSize, IntPtr memoryInputDeriv, IntPtr locationValue,
            int ensembleLen, int memoryVecDim, IntPtr memoryOutputDeriv);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Add(IntPtr gpu_floats_a, IntPtr gpu_floats_b, int m, int n, float mweight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Addition(IntPtr pLeft, IntPtr pRight, IntPtr pDst, int row, int column, float weiLeft, float weiRight, float weiDst);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Scale_Matrix(IntPtr gpu_floats_a, int m, int n, float mweight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Accurate_Scale_Matrix(IntPtr a, IntPtr aMask, IntPtr b, IntPtr bMask, int dim, int batchSize, IntPtr awei);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Add_Tanh(IntPtr gpu_floats_a, IntPtr gpu_floats_b, int m, int n);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Add_Vector(IntPtr gpu_floats_a, IntPtr gpu_floats_b, int batchsize, int dimension);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Rectified_Vector(IntPtr gpu_floats_a, IntPtr gpu_floats_b, int batchsize, int dimension);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_Cosine(IntPtr q, IntPtr d, IntPtr dcq, IntPtr dcd, int batchsize, int m, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_Cosine_EX(IntPtr q, IntPtr d, IntPtr neg_list, IntPtr dcq, IntPtr dcd, int batchsize, int m, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Derive_Cosine_Linear(IntPtr q, IntPtr d, IntPtr dcq, IntPtr dcd, int batchsize, int m, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Derive_Cosine_Linear_EX(IntPtr q, IntPtr d, IntPtr neg_list, IntPtr dcq, IntPtr dcd, int batchsize, int m, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Derive_Cosine_Rectified(IntPtr q, IntPtr d, IntPtr dcq, IntPtr dcd, int batchsize, int m, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Derive_Cosine_Rectified_EX(IntPtr q, IntPtr d, IntPtr neg_list, IntPtr dcq, IntPtr dcd, int batchsize, int m, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_Rectified(IntPtr delta, IntPtr layer_output, int batchsize, int m);



        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_Tanh(IntPtr delta, IntPtr layer_output, int batchsize, int m);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivSigmoid(IntPtr inputDeriv, IntPtr outputDeriv, IntPtr output, int batchsize, int m, float wei_a);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivRectified(IntPtr inputDeriv, IntPtr outputDeriv, IntPtr output, int batchsize, int m, float wei_a);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivTanh(IntPtr inputDeriv, IntPtr outputDeriv, IntPtr output, int batchsize, int m, float wei_a);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Multipy(IntPtr delta, IntPtr weight, IntPtr delta_low, int batchsize, int m, int n, int inverse);



        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Cosine_Similarity(IntPtr a, IntPtr b, IntPtr c, int nTrial, int BATCHSIZE, int mindex,
                                       int batchsize, int dimension, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Cosine_Similarity_EX(IntPtr a, IntPtr b, IntPtr neg_list, IntPtr c, int nTrial, int BATCHSIZE, int mindex,
                                       int batchsize, int dimension, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Inner_Product_EX_Full(IntPtr a, IntPtr b, IntPtr neg_list, IntPtr c, int nTrial, int BATCHSIZE,
                                        int batchsize, int dimension, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_InnerProduct_Linear_EX_Full(IntPtr q, IntPtr d, IntPtr neg_list, IntPtr dcq, IntPtr dcd, int nTrail,
                                        int BATCHSIZE, int batchsize, int m, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_InnerProduct_Linear(IntPtr q, IntPtr d, IntPtr dcq, IntPtr dcd, int batchsize, int m, float eps);



        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Calculate_Alpha(IntPtr alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Calculate_Alpha_MXE(IntPtr alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Calculate_Alpha_NCE(IntPtr alpha, IntPtr dist, int nTrial, int BATCHSIZE, int batchsize, float gamma);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Calculate_Alpha_NCE2(IntPtr alpha, IntPtr dist, int nTrial, int BATCHSIZE, int batchsize, float gamma);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void FillOut_Dist_NCE(IntPtr dist, IntPtr neg_list, int nTrial, int BATCHSIZE, int mindex, int batchsize);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Calculate_Alpha_PAIRRANK(IntPtr alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Calculate_SampleLRDeriv(IntPtr output, IntPtr deriv, int trial, int BATCHSIZE, IntPtr label, int batchsize, float gamma);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Product(IntPtr a, IntPtr b, IntPtr c, int batchsize, int m, int n);
        //, int kept, IntPtr alpha, int ntrial, int BATCH_SIZE, int alpha_index);




        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Convolution_Sparse_Matrix_Product_INTEX(IntPtr deriv, IntPtr maxpooling_index, IntPtr Seg_Index, IntPtr SegMargin_Index, int seg_size, int win_size,
                                        int batchsize, int output_dimension, IntPtr Fea_Index, IntPtr Fea_Value, IntPtr grad, int Feature_Dimension);



        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SEQ_Sparse_Matrix_Multiply_INTEX(IntPtr Smp_Index, int batchsize, IntPtr Seg_Index, IntPtr Seg_Margin, int seg_size, IntPtr Fea_Index,
                                                   IntPtr Fea_Value, int elementsize, IntPtr mul_weight, IntPtr output, int Feature_dimension, int output_dimension, int win_size);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SEQ_Sparse_Matrix_Multiply_INT(IntPtr Smp_Index, int batchsize, IntPtr Seg_Index, int seg_size, IntPtr Fea_Index,
                                                    IntPtr Fea_Value, int elementsize, IntPtr mul_weight, IntPtr output, int Feature_dimension, int output_dimension);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(IntPtr Smp_Index, int batchsize, IntPtr Seg_Index, int seg_size, IntPtr Fea_Index,
                IntPtr Fea_Value, int elementsize, IntPtr mul_weight, IntPtr output, int Feature_dimension, int output_dimension, float weight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(IntPtr Smp_Index, int batchsize, IntPtr Seg_Index, IntPtr Seg_Margin, int seg_size, IntPtr Fea_Index,
                                                   IntPtr Fea_Value, int elementsize, IntPtr mul_weight, IntPtr output, int Feature_dimension, int output_dimension, int win_size);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Convolution_Sparse_Matrix_Multiply_INTEX(IntPtr Smp_Index, int batchsize, IntPtr Seg_Index, IntPtr Seg_Margin, int seg_size, IntPtr Fea_Index,
                                                   IntPtr Fea_Value, int elementsize, IntPtr con_weight, IntPtr output, int Feature_dimension, int output_dimension, int win_size);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_WeightAdd(IntPtr gpu_floats_a, IntPtr gpu_floats_b, int batchsize, int dimension, IntPtr mweight, int start, int keep);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_WeightAdd_EX(IntPtr gpu_floats_a, IntPtr gpu_floats_b, IntPtr inver_neg_index, IntPtr inver_neg_value, int batchsize, int dimension, IntPtr mweight, int start, int keep);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Sparse2Dense_Matrix(IntPtr Smp_Idx, IntPtr Fea_Idx, IntPtr Fea_Value, IntPtr matrix, int batchsize, int dimension);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Aggragate(IntPtr a, IntPtr b, int batchsize, int m);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Add_OFFSET(IntPtr gpu_floats_a, int offset_a, IntPtr gpu_floats_b, int offset_b, int len, float mweight);

        //[DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        //public unsafe static extern void CUBLAS_Init();

        //[DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        //public unsafe static extern void CUBLAS_Destroy();

        
        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CUBLAS_Matrix_Multipy(IntPtr delta, IntPtr weight, IntPtr delta_low, int batchsize, int m, int n, int inverse);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Cosine_Similarity_EX_Full(IntPtr a, IntPtr b, IntPtr neg_list, IntPtr c, int nTrial, int BATCHSIZE, int batchsize, int dimension, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void FillOut_Dist_NCE_Full(IntPtr dist, IntPtr neg_list, int nTrail, int BATCH_SIZE, int batchsize);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_Cosine_EX_Full(IntPtr q, IntPtr d, IntPtr neg_list, IntPtr dcq, IntPtr dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_Cosine_Linear_EX_Full(IntPtr q, IntPtr d, IntPtr neg_list, IntPtr dcq, IntPtr dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_Cosine_Rectified_EX_Full(IntPtr q, IntPtr d, IntPtr neg_list, IntPtr dcq, IntPtr dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_WeightAdd_Full(IntPtr gpu_floats_a, IntPtr gpu_floats_b, int nTrail, int BATCHSIZE, int batchsize, int dimension, IntPtr mweight, int start, int keep);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_WeightAdd_EX_Full(IntPtr gpu_floats_a, IntPtr gpu_floats_b, IntPtr inver_neg_index, IntPtr inver_neg_value, int nTrial, int BATCHSIZE, int batchsize, int dimension, IntPtr mweight, int start, int keep);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Cosine_Similarity_SubSpace(IntPtr a, IntPtr b, IntPtr c, int labelDim, int BATCHSIZE, int batchsize, int subspaceDim, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SoftMax(IntPtr a, IntPtr b, int labelDim, int batchsize, float gamma);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_Cosine_Subspace(IntPtr q, IntPtr d, IntPtr dcq, IntPtr dcd, IntPtr alpha, int act_type, int batchsize, int labelDim, int subspaceDim, float gamma, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void InnerProduct_Similarity(IntPtr a, IntPtr b, IntPtr c, int batchsize, int dimension);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_InnerProduct(IntPtr q, IntPtr d, IntPtr dcq, IntPtr dcd, IntPtr alpha, int act_type, int batchsize, int Dim, float gamma, float eps);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Aggragate_Weight(IntPtr a, IntPtr b, int batchsize, int m, float weight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SEQ_Sparse_Matrix_Transpose_Multiply_INTEX_Weight(IntPtr Smp_Index, int batchsize, IntPtr Seg_Index, IntPtr Seg_Margin, int seg_size, IntPtr Fea_Index,
                                                   IntPtr Fea_Value, int elementsize,
                                                   IntPtr mul_weight, IntPtr output, int Feature_dimension, int output_dimension, int win_size, float weight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Product_Weight(IntPtr a, IntPtr b, IntPtr c, int batchsize, int m, int n, float weight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SGD_Train(IntPtr SampleIdx, IntPtr FeatureIdx, IntPtr FeatureValue, IntPtr Label, int BatchSize, IntPtr L1Weight, IntPtr L1Bias, IntPtr L2Weight, IntPtr L2Bias, int HiddenNum, float learnRate);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Predict(IntPtr SampleIdx, IntPtr FeatureIdx, IntPtr FeatureValue, IntPtr result, int BatchSize, IntPtr L1Weight, IntPtr L1Bias, IntPtr L2Weight, IntPtr L2Bias, int HiddenNum, float learnRate);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SGD_Train_New(IntPtr SampleIdx, IntPtr FeatureIdx, IntPtr FeatureValue, IntPtr result, int BatchSize, IntPtr L1Weight, IntPtr L1Bias, IntPtr L2Weight, IntPtr L2Bias, int HiddenNum, float learnRate);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Mask(IntPtr gpu_floats_a, IntPtr gpu_mask, int batchsize, int dimension);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Convolution_Sparse_Matrix_Product_INTEX_Weight(IntPtr deriv, IntPtr maxpooling_index, IntPtr Seg_Index, IntPtr SegMargin_Index, int seg_size, int win_size,
                                        int batchsize, int output_dimension, IntPtr Fea_Index, IntPtr Fea_Value, IntPtr grad, int Feature_Dimension, float learnRate);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Cosine_Similarity_Matching(IntPtr a, IntPtr b, IntPtr c, IntPtr matchIdxA, IntPtr matchIdxB,
                                                    int aSize, int bSize, int matchSize, int dimension, IntPtr aSquare, IntPtr bSquare, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Joint_Sparse_Matrix_Multiply_INTEX(
            IntPtr Src_Input, int SrcSize, IntPtr Tgt_Input, int TgtSize,
                                             IntPtr Src_Match_Idx, IntPtr Tgt_Match_Idx, IntPtr Match_Score, int MatchSize,
                                             IntPtr ConSmp_Idx, IntPtr ConFea_Idx, IntPtr ConFea_Value,
                                             int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
                                             IntPtr Weight, IntPtr Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Joint_Backward_Sim(IntPtr outputDeriv, int MatchSize, int outputDim, IntPtr Weight, IntPtr simDeriv);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Square_Matrix(IntPtr a, int batchSize, int dim, IntPtr aSquare);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Joint_Backward_Src_Tgt(
                IntPtr outputDeriv, int MatchSize, int outputDim,
                IntPtr Weight, IntPtr SimDeriv,
                IntPtr matchSrc, IntPtr matchTgt,
                IntPtr Src_Input, int SrcSize, int SrcDim,
                IntPtr Tgt_Input, int TgtSize, int TgtDim,
                IntPtr Sim, IntPtr aa, IntPtr bb,
                int IsSrc, int IsTgt, int IsSim,
                IntPtr SrcMatchIndex, IntPtr SrcMatchElement,
                IntPtr TgtMatchIndex, IntPtr TgtMatchElement,
                IntPtr Src_Deriv, IntPtr Tgt_Deriv, float eps, int SimType);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Joint_Sparse_Matrix_Product_INTEX(
                IntPtr Src_Input, int SrcSize,
                IntPtr Tgt_Input, int TgtSize,
                IntPtr Src_Match_Idx, IntPtr Tgt_Match_Idx, IntPtr Match_Score, int MatchSize,
                IntPtr ConSmp_Idx, IntPtr ConFea_Idx, IntPtr ConFea_Value,
                int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
                IntPtr WeightUpdate, IntPtr OutputDeriv, int OutputDimension, int IsSrc, int IsTgt, int IsSim, float lr);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Joint_Dense_Matrix_Multiply_INTEX(
                IntPtr Src_Input, int SrcSize,
                IntPtr Tgt_Input, int TgtSize,
                IntPtr Src_Match_Idx, IntPtr Tgt_Match_Idx, IntPtr Match_Score, int MatchSize,
                IntPtr ConFea_Value,
                int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
                IntPtr Weight, IntPtr Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Joint_Dense_Matrix_Product_INTEX(
                IntPtr Src_Input, int SrcSize,
                IntPtr Tgt_Input, int TgtSize,
                IntPtr Src_Match_Idx, IntPtr Tgt_Match_Idx, IntPtr Match_Score, int MatchSize,
                IntPtr ConFea_Value,
                int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
                IntPtr WeightUpdate, IntPtr OutputDeriv, int OutputDimension, int IsSrc, int IsTgt, int IsSim, float lr);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Add_Sigmoid(IntPtr gpu_floats_a, IntPtr gpu_floats_b, int m, int n);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseForward(IntPtr inputSmpIdx, IntPtr inputItemIdx, int batchSize, IntPtr weight, IntPtr output, int inputDim, int outputDim);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseBackward(IntPtr inputSmpIdx, IntPtr inputItemIdx, int batchSize, int inputItemNum,
            IntPtr deriv, int inputDim, int outputDim, IntPtr output, float lr);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseL0(IntPtr outputSmpIdx, IntPtr outputItemIdx, int batchSize, int itemNum, IntPtr L0, IntPtr output, int inputDim);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseL1(IntPtr inputSmpIdx, IntPtr inputItemIdx, IntPtr outputSmpIdx, IntPtr outputItemIdx, IntPtr outputItemMapSmp, int batchSize, int inputItemNum, int outputItemNum,
                    IntPtr L1, IntPtr output, int inputDim);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseLk(IntPtr hidden, int hiddenDim, IntPtr outputSmpIdx, IntPtr outputItemIdx, int batchSize, int outputItemNum, IntPtr outputItemMapSmp,
            IntPtr Lk, IntPtr output);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_Sigmoid(IntPtr delta, IntPtr layer_output, int batchsize, int m);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Multipy_Weight(IntPtr delta, IntPtr weight, IntPtr delta_low, int batchsize, int m, int n, int inverse, float low_wei);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CrossEntropyLoss(IntPtr outputScore, IntPtr outputLabel, IntPtr outputDeriv, int outputSize);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RegressionLoss(IntPtr outputScore, IntPtr outputLabel, IntPtr outputDeriv, int outputSize);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void PairwiseRankingLoss(
                    IntPtr src2MatchIdx, IntPtr src2MatchElement, int srcBatchSize, IntPtr srcBatchLoss,
                    IntPtr outputScore, IntPtr outputLabel, IntPtr outputDeriv, int outputSize, float eplison);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void ListwiseRankingLoss(
                    IntPtr src2MatchIdx, IntPtr src2MatchElement, int srcBatchSize, IntPtr srcBatchLoss,
                    IntPtr outputScore, IntPtr outputLabel, IntPtr outputDeriv, int outputSize, float eplison, float gamma);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LambdaRankingLoss(
                    IntPtr src2MatchIdx, IntPtr src2MatchElement, int srcBatchSize, IntPtr srcBatchLoss,
                    IntPtr outputScore, IntPtr outputLabel, IntPtr outputDeriv, int outputSize, float eplison, int K);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void BayesianRatingLoss(
                    IntPtr src2MatchIdx, IntPtr src2MatchElement, int srcBatchSize, IntPtr srcBatchLoss,
                    IntPtr outputScore, IntPtr outputLabel, IntPtr outputDeriv, int outputSize, float eplison, float gamma);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void BayesianRatingProb(IntPtr src2MatchIdx, IntPtr src2MatchElement, int srcBatchSize, IntPtr srcBatchLoss,
                    IntPtr outputScore, IntPtr outputLabel, IntPtr outputProb, int outputSize, float eplison, float gamma);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Sparse_Matrix_Multiply_INTEX_Weight(IntPtr outputSmpIndex, IntPtr outputItemIndex, IntPtr outputDeriv,
            int batchsize, int outputItemNum, IntPtr weight, IntPtr hidden_deriv, int hiddenDim, float wei);



        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseL0Update(IntPtr outputSmpIdx, IntPtr outputItemIdx, IntPtr outputScore, int batchSize, int outputItemNum, IntPtr L0,
                int inputDim, float lr);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseL1Update(IntPtr inputSmpIdx, IntPtr inputItemIdx, IntPtr outputSmpIdx, IntPtr outputItemIdx,
                IntPtr outputItemMapSmp, IntPtr outputScore, int batchSize, int inputItemNum, int outputItemNum, IntPtr L1, int inputDim, float lr);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseL0Full(int batchSize, IntPtr L0, IntPtr output, int inputDim);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseL1Full(int batchSize, IntPtr L1, IntPtr inputSmpIdx, IntPtr inputItemIdx, IntPtr outputScore, int inputDim);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseLkFull(IntPtr hidden, int hiddenDim, int batchSize, IntPtr Lk, IntPtr outputScore, int inputDim);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseLkUpdate(IntPtr outputSmpIdx, IntPtr outputItemIdx, IntPtr outputMapSample, IntPtr outputDeriv, int batchSize,
                            int outputItemNum, IntPtr hidden, IntPtr Lk, int hiddenDim, float lr);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Sparse_SoftMax(IntPtr outputSmpIdx, IntPtr outputItemIdx, IntPtr outputScore, IntPtr outputLabel, int batchSize,
                         int itemNum, IntPtr deriv, float gamma, int isLog, IntPtr loss);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Add_Vector(IntPtr gpu_floats_a, IntPtr gpu_float_b, uint m, float awei, float bwei);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Init_Vector(IntPtr gpu_floats_a, float b, int m, float awei);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Scale_Vector(IntPtr gpu_floats_a, int m, float mweight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Ada_Gradient(IntPtr ada, IntPtr grad, int m);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Inner_Product_Matching(IntPtr a, IntPtr b, IntPtr c, IntPtr matchIdxA, IntPtr matchIdxB,
            int aSize, int bSize, int matchSize, int dimension, float eps);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Sparse_Matrix_Transpose_Multiply_INTEX_Weight(IntPtr Smp_Index, int batchsize, IntPtr Fea_Index,
            IntPtr Fea_Value, int elementsize,
            IntPtr mul_weight, IntPtr output, int Feature_dimension, int output_dimension, float weight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void BinomialSampling(IntPtr prob, int batchSize, int Dim, IntPtr sample, int randomSeed);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Sparse_Matrix_Aggragate_Weight(IntPtr smpIdx, IntPtr feaIdx, IntPtr feaValue, IntPtr b, int batchsize, int m, float weight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void ReconstructionLoss(IntPtr outputScore, IntPtr smpIdx, IntPtr feaIdx, IntPtr feaValue, IntPtr loss, int batchSize, int dimension, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseUpdateRBM(IntPtr outputSmpIdx, IntPtr outputItemIdx, IntPtr outputItemLabel, int batchSize, IntPtr weight,
                    IntPtr output, IntPtr bias_v, int inputDim, int outputDim,
                    IntPtr inputSmpIdx, IntPtr inputItemIdx,
                    IntPtr grad_bias_v, IntPtr grad_bias_h, IntPtr grad_vh, float vStep, float hStep, float vhStep,
                    IntPtr loss, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparsePredictRBM(IntPtr outputScore, int batchSize, IntPtr weight,
                    IntPtr output, IntPtr bias_v, int inputDim, int outputDim);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void MultiClassSoftmax(IntPtr outputScore, IntPtr outputDeriv, int dim, float gamma, int batchSize);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Convolution_Dense_Matrix_Multiply_INTEX(IntPtr Smp_Index, int batchsize, IntPtr Seg_Margin,
                                                  int Seg_size, IntPtr Fea_Value, int Feature_dimension,
                                                  IntPtr con_weight, IntPtr output, int output_dimension, int win_size);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Convolution_Dense_Matrix_Product_INTEX_Weight(IntPtr deriv, IntPtr maxpooling_index, IntPtr SegMargin_Index, int seg_size, int win_size,
                            int batchsize, int output_dimension, IntPtr Fea_Value, IntPtr grad, int Feature_Dimension, float learnRate);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Conv_Matrix_Multipy(IntPtr upperDeriv, IntPtr con_weight, IntPtr deriv, int batchSize, int sentSize,
                    IntPtr sentMargin, IntPtr layerPoolingIndex, int neuralOut, int neuralIn, int winSize);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void ImageConvolution(IntPtr inputImage, int batchSize, int width, int height, int depth,
                           IntPtr filter, int sx, int c, int output, int pad, int stride,
                           IntPtr poolingOutput, int poolingWidth, int poolingHeight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void MaxImagePooling(IntPtr inputImage, int batchSize, int width, int height, int depth,
                                     IntPtr maxpoolingIndex,
                                     int sx, int stride,
                                     IntPtr output, int outputWidth, int outputHeight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void ImageConvMatrixMultipy(IntPtr outputDeriv, IntPtr maxpoolingIndex,
                                            int batchSize, int outputWidth, int outputHeight, int outputDepth,
                                            int poolingSX, int poolingStride,
                                            IntPtr filter, int sx, int c, int pad, int stride,
                                            IntPtr inputDeriv, int width, int height, int depth);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Add_Image_FType(IntPtr gpu_floats_a, IntPtr gpu_floats_b,
            int batchSize, int width, int height, int depth, int af);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_Image_FType(IntPtr delta, IntPtr layer_output, int batchSize, int width,
                int height, int depth, int af);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        ///This function is dangerous, please do not call it. 
        public unsafe static extern void Matrix_Image_Aggragate_Weight(IntPtr a, IntPtr b, int batchSize, int width,
                                                    int height, int depth, float weight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Convolution_Image_Matrix_Product_INTEX_Weight(IntPtr outputDeriv, IntPtr maxpoolingIndex,
                                                                 int batchsize, int outputWidth, int outputHeight, int outputDepth,
                                                                 int poolingSX, int poolingStride,
                                                                 IntPtr grad, int sx, int c, int pad, int stride,
                                                                 IntPtr input, int width, int height, int depth, float learnRate);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RNNForwardPropagate(IntPtr smpIdx, int batchSize, IntPtr seqIdx, int seqSize, IntPtr feaIdx, IntPtr feaValue, IntPtr Imatrix,
                            IntPtr Wmatrix, int lag, int feaDim, int hiddenDim, IntPtr bias, int af, IntPtr seqOut);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RNNBackwardPropagate(IntPtr smpIdx, int batchSize, IntPtr derivOutput, IntPtr output,
            int seqSize, IntPtr wMatrix, int lag, int hiddenDim, int af);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RNNBackwardPropagateSlot(IntPtr smpIdx, int batchSize, IntPtr derivOutput, IntPtr output, int seqSize, IntPtr wMatrix,
                                        int hiddenDim, int af, IntPtr earlyStop);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RecurrentUpdate(IntPtr smpIdx, int batchSize, IntPtr output, IntPtr derivOutput, int hiddenDim, int seqSize, int lag, IntPtr gradient, float weight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RecurrentUpdateSlot(IntPtr smpIdx, int batchSize, IntPtr earlyStop, IntPtr output, IntPtr derivOutput, int hiddenDim,
                                int seqSize, int lag, IntPtr gradient, float weight);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RNNForwardPropagate_Dense(IntPtr smpIdx, int batchSize, IntPtr seqInput, int seqSize, IntPtr Imatrix,
                            IntPtr Wmatrix, int lag, int feaDim, int hiddenDim, IntPtr bias, int af, IntPtr seqOut);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RNNForwardPropagateSlot_Dense(IntPtr smpIdx, int batchSize, IntPtr seqInput, int seqSize, IntPtr Imatrix,
                            IntPtr Wmatrix, int lag, int feaDim, int hiddenDim, IntPtr bias, int af, IntPtr seqOut, IntPtr groundOut, IntPtr sampleEnd);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RNNForwardPropagateSlot(IntPtr smpIdx, int batchSize, IntPtr seqIdx, int seqSize, IntPtr feaIdx, IntPtr feaValue, IntPtr Imatrix,
                            IntPtr Wmatrix, int lag, int feaDim, int hiddenDim, IntPtr bias, int af, IntPtr seqOut, IntPtr groundTruth, IntPtr earlyStopIdx);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Calculate_MSE(IntPtr label, IntPtr score, int length, IntPtr result, int resultIdx);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern int ElementwiseProduct(IntPtr pLeft, IntPtr pRight, IntPtr pDst, int row, int column, float weiDst);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern int CuDNNVectorMul(IntPtr handle, IntPtr a, IntPtr b, IntPtr c, float alpha, float beta, int length);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void L1Reg_CheckSign(IntPtr src, IntPtr dst, int len, int keepZero, int reverse);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void L1Reg_CalcPseudoGradient(IntPtr weight, IntPtr grad, IntPtr pseudoGradient, float cl1, float cl2, int len);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void L1Norm(IntPtr weight, IntPtr norm, int len);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CountZero(IntPtr weight, IntPtr norm, int len);
        //public static bool TestLongVector()
        //{
        //    CudaPieceFloat m1 = new CudaPieceFloat(63000 * 500, true, true);
        //    CudaPieceFloat m2 = new CudaPieceFloat(63000 * 500, true, true);
        //    for (int i = 0; i < 63000 * 500; i++)
        //    {
        //        m1.MemPtr[i] = i + 1;
        //        m2.MemPtr[i] = 2 * i;
        //    }
        //    m1.CopyIntoCuda();
        //    m2.CopyIntoCuda();
        //    Cudalib.Add_Vector(m1.CudaPtr, m2.CudaPtr, 63000 * 500, 1, 1);
        //    m1.CopyOutFromCuda();
        //    m1.Dispose();
        //    m2.Dispose();
        //    return m1.MemPtr[1] == 4;
        //}

        public static bool TestCuda()
        {
            CudaPieceInt m = new CudaPieceInt(2, true, true);
            m.MemPtr[0] = 100;
            m.SyncFromCPU();
            m.MemPtr[0] = 10;
            m.CopyOutFromCuda();
            bool result = true;
            if (m.MemPtr[0] != 100) result = false;
            m.Dispose();
            return result;
        }

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Tensor_Matrix_Multiply_Weight(IntPtr input, IntPtr weight, IntPtr output, int inputRow, int inputCol, int outputCol, int inverse, float wei);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Tensor_Product_Weight(IntPtr input, IntPtr deriv, IntPtr gradient, int batchsize, int inputDim, int outputDim, float weight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RNNLabelOutput(IntPtr smpIdx, int batchSize, IntPtr seqInput, int seqSize, IntPtr Wmatrix, int lag, int feaDim, int hiddenDim, IntPtr seqOut);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DeRNNLabelOutput(IntPtr smpIdx, int batchSize, IntPtr seqInputDeriv, int seqSize, IntPtr Wmatrix,
            int lag, int feaDim, int hiddenDim, IntPtr seqOutDeriv);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void UpdateRNNLabelOutput(IntPtr smpIdx, int batchSize, IntPtr seqInput, int seqSize, IntPtr Wmatrix, int lag,
            int feaDim, int hiddenDim, IntPtr seqOutDeriv, float wei);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Clip_Vector(IntPtr gpu_floats_a, int m, float maxThreshold, float minThreshold);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LogBayesianRatingLoss(
            IntPtr src2MatchIdx, IntPtr src2MatchElement, int srcBatchSize, IntPtr srcBatchLoss,
            IntPtr outputScore, IntPtr outputLabel, IntPtr outputDeriv, uint outputSize, float eplison, float gamma);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMForwardPropagate(IntPtr smpIdx, int batchSize, IntPtr seqIdx, int seqSize, IntPtr feaIdx, IntPtr feaValue, //->x;
                               IntPtr h, //-> hiddenState,
                               IntPtr gate_i, //->gate i,
                               IntPtr c_hat, //->candidate memory,
                               IntPtr gate_f, //->forget gate,
                               IntPtr c, //->memory,
                               IntPtr output, //->output,
                               int cell,
                               IntPtr Wi, IntPtr Ui, IntPtr bi,
                               IntPtr Wc, IntPtr Uc, IntPtr bc,
                               IntPtr Wf, IntPtr Uf, IntPtr bf,
                               IntPtr Wo, IntPtr Uo, IntPtr Vo, IntPtr bo);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMForwardPropagateStep2(IntPtr smpIdx, int batchSize, //int * seqIdx, int seqSize, int * feaIdx, float * feaValue, //->x;
                               IntPtr h, //-> hiddenState,
                               IntPtr gate_i, //->gate i,
                               IntPtr c_hat, //->candidate memory,
                               IntPtr gate_f, //->forget gate,
                               IntPtr c, //->memory,
                               IntPtr output, //->output,
                               int cell,
                               IntPtr Wi, IntPtr Ui, IntPtr bi,
                               IntPtr Wc, IntPtr Uc, IntPtr bc,
                               IntPtr Wf, IntPtr Uf, IntPtr bf,
                               IntPtr Wo, IntPtr Uo, IntPtr Vo, IntPtr bo);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMForwardPropagateDense(IntPtr smpIdx, int batchSize, IntPtr seqInput, int seqSize, int inputDim,
                                    IntPtr h,
                                    IntPtr gate_i,
                                    IntPtr c_hat,
                                    IntPtr gate_f,
                                    IntPtr c,
                                    IntPtr output,
                                    int cell,
                                    IntPtr Wi, IntPtr Ui, IntPtr bi,
                                    IntPtr Wc, IntPtr Uc, IntPtr bc,
                                    IntPtr Wf, IntPtr Uf, IntPtr bf,
                                    IntPtr Wo, IntPtr Uo, IntPtr Vo, IntPtr bo);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMBackwardPropagate(IntPtr smpIdx, int batchSize, int seqSize, int cell,
                                    IntPtr h,
                                    IntPtr gate_i,
                                    IntPtr c_hat,
                                    IntPtr gate_f,
                                    IntPtr c,
                                    IntPtr gate_o,

                                    IntPtr deriv_h,
                                    IntPtr deriv_gate_i,
                                    IntPtr deriv_cHat,
                                    IntPtr deriv_gate_f,
                                    IntPtr deriv_c,
                                    IntPtr deriv_gate_o,
                                    IntPtr Wi, IntPtr Ui, IntPtr bi,
                                    IntPtr Wc, IntPtr Uc, IntPtr bc,
                                    IntPtr Wf, IntPtr Uf, IntPtr bf,
                                    IntPtr Wo, IntPtr Uo, IntPtr Vo, IntPtr bo);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void HierarcialSoftmaxEmbedding(IntPtr targets, int targetNum,
            IntPtr embedding, IntPtr deriv, int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            IntPtr cSpace, IntPtr cBias, int cSize,
            float step,
            IntPtr v2c, // word 2 class,
            IntPtr classIdx, // class 2 word number
            IntPtr wordIdx, // class 2 word index
            IntPtr wordClassIdx, //word 2 class segment index.
            float gamma,
            IntPtr classOutput,
            IntPtr wordOutput,
            int wordSegSize, // word Max SegmentSize,
            IntPtr targetProb
            );

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void HierarcicalProbability(IntPtr targets, int targetNum,
                                 IntPtr embedding, int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            IntPtr cSpace, IntPtr cBias, int cSize,

            IntPtr v2c, // word 2 class,
            IntPtr classIdx, // class 2 word number
            IntPtr wordIdx, // class 2 word index
            IntPtr wordClassIdx, //word 2 class segment index.
            float gamma,
            IntPtr classOutput,
            IntPtr wordOutput,
            int wordSegSize, // word Max SegmentSize,
            IntPtr targetProb
            );


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void UpdateBinaryEmbedding_NegIds(IntPtr targets, int targetNum,
            IntPtr embedding, IntPtr deriv, int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            float step,	// step size for update vSpace.
            IntPtr simi, // similarity for negatives.
            IntPtr alpha,
            int negativeSample, float gamma,
            IntPtr negIds);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void UpdateSoftmaxEmbedding_NegIds(IntPtr targets, int targetNum,
            IntPtr embedding, IntPtr deriv, int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            float step,	// step size for update vSpace.
            IntPtr simi, // similarity for negatives.
            IntPtr alpha,
            int negativeSample, float gamma,
            IntPtr negIds);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_Normalize_By_Col(IntPtr a, int col, int row);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Bp_Lda_Forward(
            IntPtr phi,
            IntPtr theta,
            IntPtr b,
            IntPtr U,
            IntPtr T,
            IntPtr d_TLayerValue,
            IntPtr d_MaxColValue,
            IntPtr d_SumColValue,
            IntPtr iterT,
            IntPtr output,
            IntPtr matchLabelValue,
            IntPtr d_Loss_Per_Epoch,
            IntPtr matchIdx,
            IntPtr featureIdx,
            IntPtr featureValue,
            int elementSize,
            float eta,
            int useAdaptivenHidLayer,
            int batchsize,
            int inputDim,
            int hiddenDim,
            int outputDim,
            int layerDim,
            IntPtr d_Phitheta,
            IntPtr d_tempSparseMat,
            IntPtr d_tempSparseMatTemp1,
            IntPtr d_tempDenseMatTemp,
            IntPtr d_NegGradTemp,
            IntPtr d_LogThetaTemp,
            IntPtr d_loss_pre,
            IntPtr d_loss_post,
            IntPtr d_sum,
            IntPtr d_sumGradProj,
            IntPtr d_flagWhileBreak, // batchsize
            IntPtr d_whileBreakCount, // 1
            int[] whileBreakCount, //1
            float gamma,
            int calculateSoftMax,
            int MaxLineSearchIter
            );

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Bp_Lda_Backward(
            IntPtr output,
            IntPtr matchLabelValue,
            IntPtr theta,
            IntPtr phi,
            IntPtr b,
            IntPtr U,
            IntPtr T,
            IntPtr iterT,
            IntPtr grad_Q_U,
            IntPtr grad_Q_Phi,
            IntPtr matchIdx,
            IntPtr featureIdx,
            IntPtr featureValue,
            float gamma,
            float negativeBetaMinusOneOverTotalSamples,
            int batchsize,
            int inputDim,
            int hiddenDim,
            int outputDim,
            int layerDim,
            int elementSize,
            IntPtr d_xi,
            IntPtr d_tempDenseMat,
            IntPtr d_temp_theta_xi,
            IntPtr d_temp_theta_xi_b_T_OVER_theta_lm1_2,
            IntPtr d_thetaRatio,
            IntPtr d_sumGrad_Q_U,
            IntPtr d_temp_Xt_OVER_Phitheta, // elementSize
            IntPtr d_tempSparseMatValue, // elementSize
            IntPtr d_sumTempDenseMat,
            IntPtr d_tempSparseMat, // elementSize
            IntPtr d_tempResBatch,
            int calculateSoftMax);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Bp_Lda_Update(
            IntPtr grad_Q_U,
            IntPtr grad_Q_Phi,
            IntPtr U,
            IntPtr phi,
            IntPtr adaGradSum,
            IntPtr d_sumAdaGradSum,
            IntPtr d_sumTempColSum,
            IntPtr muPhiSearch,
            float mu_U,
            float mu_Phi,
            int cntModelUpdate,
            int inputDim,
            int hiddenDim,
            int outputDim,
            IntPtr d_update,
            IntPtr d_tempColSum,
            IntPtr d_tempColMax
            );


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void RandomNumber(IntPtr value, int size);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void UpdateSoftmaxEmbedding(IntPtr targets, int targetNum,
            IntPtr embedding, IntPtr deriv, int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            float step,	// step size for update vSpace.
            IntPtr simi, // similarity for negatives.
            IntPtr alpha,
            int negativeSample, float gamma,
            IntPtr negIds,
            IntPtr randVar, int randLen, int randStart);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void UpdateEmbedding(IntPtr targets, int targetNum,
            IntPtr embedding, IntPtr deriv, int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            float step,	// step size for update vSpace.
            IntPtr simi, // similarity for negatives.
            IntPtr alpha,
            int negativeSample, float gamma,
            IntPtr negIds,
            IntPtr randVar, int randLen, int randStart);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void UpdateBalanceEmbedding(IntPtr targets, int targetNum,
            IntPtr embedding, IntPtr deriv, int dim,
            IntPtr vSpace, IntPtr vBias, int vocabSize,
            float step,	// step size for update vSpace.
            IntPtr simi, // similarity for negatives.
            IntPtr alpha,
            int negativeSample, float gamma,
            IntPtr negIds,
            IntPtr randVar, int randLen, int randStart,
            IntPtr sampleTable, int tableSize);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern int Max(IntPtr int_array, int len);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Cosine_Similarity_Full(IntPtr a, IntPtr b, IntPtr neg_list, IntPtr c, int nTrialplus1, int batchsize, int dimension, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Inner_Product_Full(IntPtr a, IntPtr b, IntPtr neg_list, IntPtr c, int nTrialplus1, int batchsize, int dimension, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_CosineSimilarity_Matching(IntPtr q, IntPtr d, IntPtr qSquare, IntPtr dSquare, int dim,
            IntPtr src2MatchIdx, IntPtr src2MatchElement, IntPtr tgt2MatchIdx, IntPtr tgt2MatchElement, IntPtr srcIdx, IntPtr tgtIdx, int srcSize, int tgtSize, int matchSize,
            IntPtr simi, IntPtr derivSimi, IntPtr dcq, IntPtr dcd, float eps);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Deriv_InnerProduct_Matching(IntPtr q, IntPtr d, int dim,
            IntPtr src2MatchIdx, IntPtr src2MatchElement, IntPtr tgt2MatchIdx, IntPtr tgt2MatchElement, IntPtr srcIdx, IntPtr tgtIdx, int srcSize, int tgtSize, int matchSize,
            IntPtr derivSimi, IntPtr dcq, IntPtr dcd, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Calculate_SampleCrossEntropyDeriv(IntPtr output, IntPtr deriv, int dim, IntPtr label, int batchsize, float gamma);


        public static unsafe void GetCudaMemoryInfo(out long free, out long total)
        {
            long[] memInfo = new long[2];
            fixed (long* pFree = memInfo)
            {
                Cudalib.CudaMemGetInfo((IntPtr)pFree, (IntPtr)(pFree + 1));
            }

            free = memInfo[0];
            total = memInfo[1];
        }

        public static IntPtr InitContext { get; private set; }

        /// <summary>
        /// To Do : Need to support multiple GPUs.
        /// </summary>
        /// <param name="deviceId"></param>
        public static void CudaInit(int deviceId)
        {
            Console.WriteLine("Try to set cuda device to {0}", deviceId);
            int rlt = CudaSetDevice(deviceId);

            InitContext = CudaCtxGetCurrent();
            if (rlt != 0 || InitContext == IntPtr.Zero)
            {
                throw new InvalidOperationException("Failed to initialize Cuda");
            }
        }

        public static void AttachToInitialContext()
        {
            int rlt = CudaCtxSetCurrent(InitContext);
            if (rlt != 0)
            {
                throw new InvalidOperationException("Failed to set Cuda current context");
            }
        }




        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMForwardPropagateBatchV2(IntPtr InstanceIndex, int batchSize,  //->x;
            IntPtr o, //-> output,
            IntPtr gate_i, //->gate i,
            IntPtr c_hat, //->candidate memory,
            IntPtr gate_f, //->forget gate,
            IntPtr c, //->memory,
            IntPtr gate_o, //->gate o,
            IntPtr tanhc, //->tanh memory
            int cell,
            IntPtr Ui,
            IntPtr Uc,
            IntPtr Uf,
            IntPtr Uo, IntPtr Vo,
            IntPtr RecursiveIdx, IntPtr RecursiveMatrix, int MaxLag);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void LSTMBackwardPropagateBatchV2(IntPtr InstanceIndex, int batchSize, int seqSize, int cell,
            IntPtr o,
            IntPtr gate_i,
            IntPtr c_hat,
            IntPtr gate_f,
            IntPtr c,
            IntPtr gate_o,
            IntPtr tanhc,

            IntPtr deriv_o,
            IntPtr deriv_gate_i,
            IntPtr deriv_cHat,
            IntPtr deriv_gate_f,
            IntPtr deriv_c,
            IntPtr deriv_gate_o,
            IntPtr deriv_tanhc,

            IntPtr Ui, IntPtr Uc, IntPtr Uf, IntPtr Uo, IntPtr Vo,
            IntPtr RecursiveIdx, IntPtr RecursiveMatrix, int MaxLag);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void convolution_sparse_matrix_gradient(IntPtr deriv, IntPtr Seg_Index, IntPtr SegMargin_Index, int seg_size, int win_size,
            int batchsize, int output_dimension, IntPtr Fea_Index, IntPtr Fea_Value, IntPtr grad, int Feature_Dimension, float learnRate);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void convolution_dense_matrix_gradient(IntPtr deriv, IntPtr SegMargin_Index, int seg_size, int win_size,
            int batchsize, int output_dimension, IntPtr input, IntPtr grad, int Feature_Dimension, float learnRate);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Convolution_Matrix_Transpose_Multiply(IntPtr upperDeriv, IntPtr con_weight, IntPtr deriv, int batchSize, int sentSize,
            IntPtr sentMargin, int neuralOut, int neuralIn, int winSize);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void MatrixTran_AddVector(IntPtr gpu_floats_a, IntPtr gpu_floats_b, IntPtr gpu_floats_c, int batchsize, int dimension, float weight);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void ImageDenseConvMatrixMultipy(IntPtr outputDeriv, //int * maxpoolingIndex,
                int batchSize, int outputWidth, int outputHeight, int outputDepth,
                //int poolingSX, int poolingStride,
                IntPtr filter, int sx, int c, int pad, int stride,
                IntPtr inputDeriv, int width, int height, int depth);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Convolution_Dense_Image_Matrix_Product_INTEX_Weight(IntPtr outputDeriv,
                int batchsize, int outputWidth, int outputHeight, int outputDepth,
                //int poolingSX, int poolingStride,
                IntPtr grad, int sx, int c, int pad, int stride,
                IntPtr input, int width, int height, int depth, float learnRate);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Convolution_Index_Matrix_Multiply(int batchsize, IntPtr Seg_Index, IntPtr Seg_Margin,
                int seg_size, IntPtr Fea_Index, int elementsize, IntPtr con_weight, IntPtr output, int Feature_dimension, int output_dimension, int win_size);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Convolution_Index_Matrix_Product_Weight(IntPtr deriv, IntPtr maxpooling_index, IntPtr Seg_Index, IntPtr SegMargin_Index, int seg_size, int win_size,
                int batchsize, int output_dimension, IntPtr Fea_Index, IntPtr grad, int Feature_Dimension, float learnRate);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuDNNForwardPropagate(IntPtr handle, IntPtr imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth,
        IntPtr filterMap, int filterMapNum, int filterHeight, int filterWidth, int pad, int stride,
        IntPtr imageOutBatch, int outHeight, int outWidth);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuDNNBackwardPropagateData(IntPtr handle, IntPtr imageInBatchDeriv, int nBatch, int inFeatureMap, int inHeight, int inWidth,
            IntPtr filterMap, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride,
            IntPtr imageOutBatchDeriv, int outHeight, int outWidth);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuDNNBackwardPropagateFilter(IntPtr handle, IntPtr imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth,
            IntPtr filterMapDeriv, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride,
            IntPtr imageOutBatchDeriv, int outHeight, int outWidth, float alphaValue, float betaValue);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuDNNBackwardPropagateBias(IntPtr handle, IntPtr biasDeriv, IntPtr imageOutBatchDeriv, int batch, int outDepth, int outHeight, int outWidth, float alphaValue, float betaValue);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuBLAS_Matrix_Multiplication(IntPtr A, IntPtr B, IntPtr C, int batchsize, int m, int n, float alpha, float beta, bool transA, bool transB);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void ElementwiseProductMask(IntPtr pLeft, IntPtr pRight, IntPtr pDst,
            IntPtr leftmask, IntPtr rightmask, IntPtr dstmask, int row, int column, float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern IntPtr CudaCreateCuBlas();

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CudaDestroyCuBlas(IntPtr handle);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern IntPtr CudaCreateCuSparse();

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CudaDestroyCuSparse(IntPtr handle);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseVectorAdd(IntPtr handle, IntPtr idx, IntPtr value, IntPtr dst, float beta, int length);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseVectorGivens(IntPtr handle, IntPtr idx, IntPtr value, IntPtr dst, float alpha, float beta, int length);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuBLAS_Sgemm(IntPtr handle, IntPtr A, IntPtr B, IntPtr C, int batchsize, int m, int n, float alpha, float beta, bool transA, bool transB);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern float CUBLAS_Sasum(IntPtr handle, IntPtr x, int len, int norm);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern float CuBLAS_LogLossDeriv(IntPtr handle, IntPtr outputProb, int dim, int batchSize, IntPtr smpProb,
            IntPtr label, IntPtr labelwei, IntPtr labelMask, float gamma, float eps);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern IntPtr CudaCreateCuDnn();

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CudaDestroyCuDnn(IntPtr handle);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SgemmMask(IntPtr A, IntPtr B, IntPtr C, int batchsize, int m, int n,
            IntPtr aMask, IntPtr bMask, IntPtr cMask, float alpha, float beta, bool transA, bool transB);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseSgemmMask(IntPtr AIndex, IntPtr AFeaIndex, IntPtr AFeaValue, IntPtr B, IntPtr C, int batchsize, int m, int n,
            IntPtr aMask, IntPtr bMask, IntPtr cMask, float alpha, float beta, bool transA, bool transB);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Max_Pooling(IntPtr pooling_feas, IntPtr Smp_Index, int batchsize, IntPtr output, IntPtr maxpooling_index, int output_dimension);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivMaxPooling(IntPtr deriv, IntPtr maxpooling_index, IntPtr pooling_deriv, int batchsize, int output_dimension, float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void MaxPoolingMask(IntPtr pooling_feas, IntPtr Smp_Index, IntPtr Seq_Index,
            int batchsize, IntPtr output, IntPtr maxpooling_index, int output_dimension);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CalculateLastMatrixs(IntPtr InstanceIndex, int batchSize, IntPtr SeqMatrixs, int dim, IntPtr matrix);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CalculateLastMatrixsMask(IntPtr InstanceIndex, IntPtr seqTransIdx, int batchSize, IntPtr SeqMatrixs, int dim, IntPtr matrix, IntPtr matrixMask);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivCalculateLastMatrixs(IntPtr InstanceIndex, int batchSize, IntPtr SeqMatrixs, int dim, IntPtr matrix, float alpha);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivCalculateLastMatrixsMask(IntPtr InstanceIndex, IntPtr seqTransIdx, int batchSize, IntPtr SeqMatrixs, int dim, IntPtr matrix, IntPtr matrixMask, float alpha);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void KLargestValueBatch(IntPtr float_array, int batchSize, int dim, int K, int mode, IntPtr bestValues, IntPtr bestIndexes);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_AdditionMask(IntPtr a, IntPtr aMask, IntPtr b, IntPtr bMask, IntPtr c, IntPtr cMask,
            int dim, int batchSize, float awei, float bwei, float cwei);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SparseMutliClassSoftmax(IntPtr smpIdx, IntPtr outputScore, IntPtr outputProb, float gamma, int batchSize);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void ColumnWiseSumMask(IntPtr matrix, IntPtr matrixMask, IntPtr weight,
                IntPtr smpIdx, int batchSize,
                IntPtr output, IntPtr outputMask,
                int row, int col,
                float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void ColumnWiseSumMaskV2(IntPtr matrix, IntPtr matrixMask, IntPtr weight,
                IntPtr smpIdx, int batchSize,
                IntPtr output, IntPtr outputMask,
                int row, int col,
                float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void ColumnWiseSumMaskV3(IntPtr matrix, IntPtr matrixMask, IntPtr weight,
                int skipMatrix,
                IntPtr smpIdx, int batchSize,
                IntPtr output, IntPtr outputMask,
                int skipOutput,
                int row, int col,
                float alpha, float beta);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Scale_MatrixMask(IntPtr a, IntPtr aMask, IntPtr b, IntPtr bMask, int dim, int batchSize, IntPtr awei, float bwei);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivSparseMultiClassSoftmax(IntPtr smpIdx, IntPtr outputProb, IntPtr probDeriv, IntPtr outputDeriv, float gamma, float alpha, float beta, int batchSize);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuBLAS_Sgemv(IntPtr handle, IntPtr matrix, IntPtr x, int row, int col, IntPtr y, bool transM, float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuDNNSoftmax(IntPtr handle, IntPtr input, IntPtr output, int batchSize, int dim);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CudaClipAdamUpdate(
               IntPtr gpu_floats_Gradient,
               IntPtr gpu_floats_M,
               IntPtr gpu_floats_V,
               IntPtr gpu_floats_Parameter,
               float Beta1,
               float Beta2,
               float Epsilon,
               float updateRate,
               float weightClip,
               float gradClip,
               int iter,
                uint m);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void GetSeqOrderMatrixs(IntPtr InstanceIndex, int batchSize, IntPtr SeqMatrixs, IntPtr MapForward, int dim, int isReverse, int order, IntPtr matrix, float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SetSeqOrderMatrixs(IntPtr InstanceIndex, int batchSize, IntPtr SeqMatrixs, IntPtr MapForward, int dim, int isReverse, int order, IntPtr matrix, float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void GetWindowMatrixs(IntPtr InstanceMargin, int sentsize, IntPtr SeqMatrixs, IntPtr mapForward, int dim, int winsize, IntPtr matrix, float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SetWindowMatrixs(IntPtr InstanceMargin, int sentsize, IntPtr SeqMatrixs, IntPtr mapForward, int dim, int winsize, IntPtr matrix, float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuDNNDerivSoftmax(IntPtr handle, IntPtr softmaxData, IntPtr softmaxDeriv, IntPtr gradData, int batchSize, int dim, float alpha);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void ColumnWiseSum(IntPtr matrix, IntPtr result, int row, int col, float wei);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivSparseSoftmax(IntPtr word_candidate_count, IntPtr wordAct, IntPtr wordDeriv, IntPtr wordLabel,
            float gamma, int batchSize, IntPtr targetProb);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuBLAS_SgbmvDiagonal(IntPtr handle, IntPtr A, IntPtr x, IntPtr y, int batchSize, float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuBLAS_Scal(IntPtr handle, IntPtr A, float alpha, int n);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuBLAS_Scopy(IntPtr handle, IntPtr src, IntPtr dst, int n);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuBLAS_Saxpy(IntPtr handle, IntPtr src, float alpha, IntPtr dst, int n);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void CuBLAS_Sger(IntPtr handle, IntPtr x, IntPtr y, IntPtr matrix, int row, int col, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void MaxoutPooling(IntPtr input, int batchSize, int dim, IntPtr maxpoolingIndex, int maxoutDim, int outDim, IntPtr output, int format);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivMaxoutPooling(IntPtr input_deriv, int batchSize, int dim, IntPtr maxpoolingIndex, int maxoutDim, int outDim, IntPtr output_deriv);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void UCTStateSampling(IntPtr count, IntPtr success, IntPtr status, int statusNum, IntPtr weight, int weightLen, float C);

        //[DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        //public unsafe static extern void StateUpdating(IntPtr count, IntPtr success, IntPtr status, int statusNum, IntPtr weight, int weightLen, float sampleNum, float win);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void BanditStateUpdating(IntPtr fail, IntPtr success, IntPtr status, int armNum, int banditNum, float f, float s, float alpha);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern int MPI_Initialization(int deviceId);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void MPI_Aggregation(
            IntPtr gpu_floats_weight,
            uint m);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void MatrixL2Norm(IntPtr x, int dim, int batchSize, IntPtr l2Score);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void MatrixL1Norm(IntPtr x, int dim, int batchSize, IntPtr l1Score);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivMatrixL2Norm(IntPtr x, int dim, int batchSize, IntPtr l2Score, IntPtr l2ScoreDeriv, IntPtr xDeriv);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivMatrixL1Norm(IntPtr x, int dim, int batchSize, IntPtr l1Score, IntPtr l1ScoreDeriv, IntPtr xDeriv);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SpanMaxPool(IntPtr inputSent, IntPtr smpIdx, IntPtr start, IntPtr end, IntPtr sentMargin, int matchSize, IntPtr outputSent, IntPtr outMaxIndex, int dim);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivSpanMaxPool(IntPtr outputSentDeriv, IntPtr smpIdx, int batchSize, IntPtr inputSentDeriv, IntPtr outMaxIndex, int dim, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SpanLastPool(IntPtr inputSent, IntPtr smpIdx, IntPtr start, IntPtr end, IntPtr sentMargin, int matchSize, IntPtr outputSent, int dim);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivSpanLastPool(IntPtr outputSentDeriv, IntPtr outSmpIdx, IntPtr start, IntPtr end, int batchSize, IntPtr inputSentDeriv, IntPtr inSmpIdx, int dim, float beta);
        //IntPtr outputSentDeriv, IntPtr smpIdx, IntPtr start, IntPtr end, int batchSize, IntPtr inputSentDeriv, int dim, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_AdditionEx(IntPtr a, int skipa, IntPtr b, int skipB, IntPtr c, int skipC, int dim, int batchSize, float awei, float bwei, float cwei);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void AccurateElementwiseProduct(IntPtr pLeft, IntPtr pRight, IntPtr pDst, IntPtr leftmask, IntPtr rightmask, IntPtr dstmask, int row, int column, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void Matrix_AdditionExMask(IntPtr a, IntPtr aMask, int skipA, IntPtr b, IntPtr bMask, int skipB, IntPtr c, IntPtr cMask, int skipC, int dim, int batchSize, float awei, float bwei, float cwei);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SoftAttention_Matching(IntPtr a, IntPtr aMask, IntPtr b, IntPtr bMask, int dim, int a_func, int op, IntPtr vec, IntPtr c, IntPtr cMask, int batchSize, float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivSoftAttention_Matching(IntPtr a, IntPtr aDeriv, IntPtr aMask, IntPtr b, IntPtr bDeriv, IntPtr bMask,
            int dim, int a_func, int op, IntPtr vec, IntPtr vecDeriv, IntPtr c, IntPtr cDeriv, IntPtr cMask,
            IntPtr aSmpIdx, IntPtr aElementIdx, int aSize, IntPtr bSmpIdx, IntPtr bElementIdx, int bSize, int batchSize, float awei, float bwei, float vwei);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void SoftAttention(IntPtr a, IntPtr b, int dim, int a_func, int op, IntPtr vec, IntPtr c, int aBatchSize, int bBatchSize, float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivSoftAttention(IntPtr a, IntPtr aDeriv, IntPtr b, IntPtr bDeriv, int dim, int a_func, int op, IntPtr vec, IntPtr vecDeriv,
            IntPtr c, IntPtr cDeriv, int aBatchSize, int bBatchSize, float awei, float bwei, float vwei);


        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void NDArrayTranspose(IntPtr input, IntPtr indim, IntPtr transDim, IntPtr output, int dimNum, int length, float alpha, float beta);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivLogSparseSoftmax(IntPtr smpIdx, IntPtr outputProb, IntPtr probDeriv, IntPtr outputDeriv, float gamma, float alpha, float beta, int batchSize);

        [DllImport("Cudalib", CallingConvention = CallingConvention.StdCall)]
        public unsafe static extern void DerivLogProbEntropy(IntPtr smpIdx, IntPtr outputProb, IntPtr logProbDeriv, float alpha, float beta, float epislon, int batchSize);

    }
}
