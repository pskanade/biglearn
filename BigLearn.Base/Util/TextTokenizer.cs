﻿using BigLearn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Hierarchy Text Tokenizer
    /// </summary>
    public interface ITextTokenizer 
    {
        IEnumerable<string> LetterTokenize(string text);

        IEnumerable<string> WordTokenize(string text);

        IEnumerable<string> LnGTokenize(string text, int ngram);

        IEnumerable<string> SentenceTokenize(string text);

        int GetMaxWordLength();
    }

    public class TextTokenizeParameters 
    {
        public const int MaximumWordNumber = 20;
        public const int MaximumWordLength = 20;
        public const int MaximumSentenceNumber = 20;
        public static string[] WordTokens = { " ", ",", ";", "'", "_", "(", ")", ".", "/", "<", ">", "[", "]", "&", "*", "`", "\\", ":", "\"", "{", "}", "|", "’", "•", "“", "”", "´", "，" /*, "#"*/ };
        public static string[] SentenceTokens = { ".", ";", "?", "!" };
        public static string[] SimpleSentenceTokens = {", ",". ", "? ","; "};//{ "###" };
        public static string[] SimpleWordTokens = { " " };
    }

    public class SimpleTextTokenizer : ITextTokenizer
    {
        public bool IgnoreCase = true;
        public bool TrimSpace = true;

        /// <summary>
        /// Maximum Number of Words.
        /// </summary>
        public int MaxWordNum { get; set; }

        /// <summary>
        /// Maximum Length of each word.
        /// </summary>
        public int MaxWordLength { get; set; }

        /// <summary>
        /// Maximum number of Sequence.
        /// </summary>
        public int MaxSentenceNum { get; set; }

        public string[] WordSplitter { get; set; }
        public string[] SentenceSplitter { get; set; }

        public SimpleTextTokenizer() : this(TextTokenizeParameters.SimpleWordTokens, TextTokenizeParameters.SimpleSentenceTokens)
        { }

        public SimpleTextTokenizer(string[] wordSplitter, string[] sentenceSplitter, 
            int maxWordNum = int.MaxValue, int maxWorLength = int.MaxValue, int maxSentenceNumber = int.MaxValue, bool ignoreCase = true, bool trimSpace = true)
        {
            WordSplitter = wordSplitter;
            SentenceSplitter = sentenceSplitter;
            IgnoreCase = ignoreCase;
            TrimSpace = trimSpace;
            MaxWordNum = maxWordNum;
            MaxWordLength = maxWorLength;
            MaxSentenceNum = maxSentenceNumber;
        }

        string Process(string text)
        {
            string result = text;
            if (IgnoreCase) result = text.ToLower();
            if (TrimSpace) result = result.Trim();
            return result;
        }

        public int GetMaxWordLength()
        {
            return MaxWordLength;
        }

        public IEnumerable<string> LetterTokenize(string text)
        {
            foreach (char c in Process(text).Take(MaxWordLength)) yield return c.ToString();
        }

        public IEnumerable<string> WordTokenize(string text)
        {
            foreach (string token in Process(text).Split(WordSplitter, StringSplitOptions.RemoveEmptyEntries).Take(MaxWordNum)) yield return token;
        }

        public IEnumerable<string> LnGTokenize(string text, int ngram)
        {
            string mword = "#" + Process(text) + "#";
            for (int i = 0; i < Math.Min(mword.Length - ngram + 1, MaxWordLength); i++) yield return mword.Substring(i, ngram);
        }

        public IEnumerable<string> SentenceTokenize(string text)
        {
            foreach (string token in Process(text).Split(SentenceSplitter, StringSplitOptions.RemoveEmptyEntries).Take(MaxSentenceNum)) yield return token;
        }
    }
}
