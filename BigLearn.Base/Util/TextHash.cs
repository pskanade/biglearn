﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum TextHashType
    {
        AdvanceLetter = 0,
        TriLetterWithFreqWord = 1, 
        AdvanceText = 2,
        AdvanceLetter_FreqTerm = 3,
    };

    public class TextHashFactory
    {
        public static BasicTextHash Create(TextHashType hashtype, SimpleTextTokenizer tokenizer, string Vocab, 
            string TriLetterFile = "", string WordTSV = "", bool handleOOV = false, int lng = 3)
        {
            switch(hashtype)
            {
                case TextHashType.AdvanceLetter:
                    {
                        AdvanceLetterTextHash texthash = null;
                        if (File.Exists(Vocab))
                        {
                            texthash = new AdvanceLetterTextHash(Vocab, tokenizer);
                        }
                        else
                        {
                            texthash = new AdvanceLetterTextHash(tokenizer);
                        }
                        return texthash;
                    }
                case TextHashType.TriLetterWithFreqWord:
                    {
                        L3GFreqWordTextHash texthash = new L3GFreqWordTextHash(WordTSV, TriLetterFile,  tokenizer, handleOOV);
                        return texthash;
                    }
                case TextHashType.AdvanceText:
                    {
                        AdvanceTextHash texthash = new AdvanceTextHash(tokenizer, lng, /* This is a temporary solution */WordTSV != string.Empty, TriLetterFile != string.Empty);
                        return texthash;
                    }
                default:
                    {
                        throw new Exception("no support text hash");
                    }
            }
        }
    }

    /// <summary>
    /// Hash text into Index.
    /// </summary>
    public class BasicTextHash
    {
        public virtual int VocabSize { get; }

        public ITextTokenizer TextTokenizer { get; set; }

        public BasicTextHash() { TextTokenizer = new SimpleTextTokenizer(); }

        public BasicTextHash(ITextTokenizer textTokenizer) { TextTokenizer = textTokenizer; }

        public virtual void Serialize(StreamWriter writer) { throw new NotImplementedException(); }
        public virtual void Deserialize(StreamReader reader) { throw new NotImplementedException(); }
        private bool remove_html_tag = true;
        public bool Remove_HTML_Tag
        {
            get
            {
                return this.remove_html_tag;
            }
            set
            {
                this.remove_html_tag = value;
            }
        }

        public virtual Dictionary<int, float> FeatureExtract(string text)
        {
            throw new NotImplementedException();
        }

        public virtual List<Dictionary<int, float>> SeqFeatureExtract(string text)
        {
            string newtext = text;
            if (this.Remove_HTML_Tag)
                newtext = TextUtil.CleanHTMLText(newtext);
            List<Dictionary<int, float>> results = new List<Dictionary<int, float>>();
            foreach (string word in TextTokenizer.WordTokenize(newtext))
            {
                Dictionary<int, float> fea = FeatureExtract(word);
                if (fea.Count > 0) results.Add(fea);
            }
            return results;
        }

        public virtual List<List<Dictionary<int, float>>> MultiSeqFeatureExtract(string text)
        {
            string newtext = text;
            if (this.Remove_HTML_Tag)
                newtext = TextUtil.CleanHTMLText(newtext);
            List<List<Dictionary<int, float>>> results = new List<List<Dictionary<int, float>>>();
            foreach (string sentence in TextTokenizer.SentenceTokenize(newtext))
            {
                List<Dictionary<int, float>> fea = SeqFeatureExtract(sentence);
                if (fea.Count > 0) results.Add(fea);
            }
            return results;
        }

        public virtual void VocabExtract(string text)
        {
            throw new NotImplementedException();
        }
    }

    public class L3GTextHash : BasicTextHash
    {
        public int LG = 3;
        public ItemFreqIndexDictionary Letter3GFreq = new ItemFreqIndexDictionary("Letter3GramFreq");

        public L3GTextHash(string fileName) { Deserialize(new StreamReader(new FileStream(fileName, FileMode.Open, FileAccess.Read), Encoding.UTF8)); }
        public L3GTextHash(ITextTokenizer textTokenizer) : base(textTokenizer) { }
        public L3GTextHash(string fileName, ITextTokenizer textTokenizer) : base(textTokenizer) { Deserialize(new StreamReader(new FileStream(fileName, FileMode.Open, FileAccess.Read), Encoding.UTF8)); }

        public override int VocabSize { get { return Letter3GFreq.ItemDictSize; } }

        public override Dictionary<int, float> FeatureExtract(string text)
        {
            Dictionary<int, float> fea = new Dictionary<int, float>();
            foreach (string l3g in TextTokenizer.LnGTokenize(text, LG))
            {
                int idx = Letter3GFreq.IndexItem(l3g);
                if (idx >= 0)
                {
                    if (fea.ContainsKey(idx)) fea[idx] = fea[idx] + 1;
                    else fea[idx] = 1;
                }
            }
            return fea;
        }

        public override void VocabExtract(string text)
        {
            foreach (string word in TextTokenizer.WordTokenize(text))
            {
                foreach (string l3g in TextTokenizer.LnGTokenize(word, LG))
                {
                    Letter3GFreq.PushItem(l3g);
                }
            }
        }

        public override void Serialize(StreamWriter writer)
        {
            Letter3GFreq.Save(writer, false);
        }

        public override void Deserialize(StreamReader reader)
        {
            Letter3GFreq = new ItemFreqIndexDictionary(reader, false);
        }
    }

    public class WordTextHash : BasicTextHash
    {
        public int LG = 3;
        public ItemFreqIndexDictionary WordFreq = new ItemFreqIndexDictionary("WordFreq");

        public WordTextHash(string fileName) { Deserialize(new StreamReader(new FileStream(fileName, FileMode.Open, FileAccess.Read), Encoding.UTF8)); }
        public WordTextHash(ITextTokenizer textTokenizer) : base(textTokenizer) { }
        public WordTextHash(string fileName, ITextTokenizer textTokenizer) : base(textTokenizer) { Deserialize(new StreamReader(new FileStream(fileName, FileMode.Open, FileAccess.Read), Encoding.UTF8)); }

        public override int VocabSize { get { return WordFreq.ItemDictSize; } }

        public override Dictionary<int, float> FeatureExtract(string text)
        {
            Dictionary<int, float> fea = new Dictionary<int, float>();
            foreach (string word in TextTokenizer.WordTokenize(text))
            {
                int idx = WordFreq.IndexItem(word);
                if (idx >= 0)
                {
                    if (fea.ContainsKey(idx)) fea[idx] = fea[idx] + 1;
                    else fea[idx] = 1;
                }
            }
            return fea;
        }

        public override void VocabExtract(string text)
        {
            foreach (string word in TextTokenizer.WordTokenize(text))
            {
                WordFreq.PushItem(word);
            }
        }

        public override void Serialize(StreamWriter writer)
        {
            WordFreq.Save(writer, false);
        }

        public override void Deserialize(StreamReader reader)
        {
            WordFreq = new ItemFreqIndexDictionary(reader, false);
        }
    }


    /// <summary>
    /// TO DO: Clean the function  SeqFeatureExtract and Word2Feature.
    /// </summary>
    public class L3GFreqWordTextHash : BasicTextHash
    {
        int L3GVocabSize
        {
            get
            {
                int count = L3GVocab.Count;
                if (handleOOV)
                    count++;
                return count;
            }
        }
        int WordVocabSize { get { return wordIdx - L3GVocabSize; } }
        Dictionary<string, int> L3GVocab = new Dictionary<string, int>();
        Dictionary<string, int> WordVocab = new Dictionary<string, int>();
        HashSet<string> WordPrefix = new HashSet<string>(); // entity's word prefix, like "windows 10" is a prefix of "windows 10 support". this is to speed up entity(word) detection

        int wordIdx;
        int LG = 3;
        int maxNumOfWordsInAnEntity = 0;
        bool handleOOV = false;
        string[] wordSplitters = TextTokenizeParameters.WordTokens;
        int maxWordNum = int.MaxValue;
        int maxSeqNum = int.MaxValue;
        int maxWordLength = int.MaxValue;
        public L3GFreqWordTextHash(string wordFileName, string l3gFileName, bool handleOOV = false, int LetterLength = 3,
                string[] wordSplitters = null, int maxWordNum = int.MaxValue,
                int maxSeqNum = int.MaxValue, int maxWordLength = int.MaxValue)
        {
            if (wordSplitters == null)
            {
                wordSplitters = TextTokenizeParameters.WordTokens;
            }
            this.handleOOV = handleOOV;
            this.LG = LetterLength;
            Console.WriteLine("### handle OOV : {0} ###", this.handleOOV);
            this.maxWordLength = maxWordLength;
            this.maxSeqNum = maxSeqNum;
            this.maxWordNum = maxWordNum;
            if (!l3gFileName.Equals(string.Empty))
            {
                string[] lines = File.ReadAllLines(l3gFileName, Encoding.UTF8);
                for (int i = 0; i < lines.Length; i++)
                {
                    string[] items = lines[i].Split('\t');
                    if (items[0].Length == LG)//L3GVocab.ContainsKey(items[0])== false)
                    {
                        if (L3GVocab.ContainsKey(items[0]))
                        {
                            //Console.WriteLine("Careful, conflict in {0} for L3g:{1}", l3gFileName, items[0]);
                            continue;
                        }
                        L3GVocab.Add(items[0], L3GVocab.Count);
                    }
                }
            }
            wordIdx = L3GVocab.Count;
            if (!wordFileName.Equals(string.Empty))
            {
                string[] lines = File.ReadAllLines(wordFileName);
                int lineIdx = 0;
                foreach (string l in lines)
                {
                    string lineRead = l.Trim().ToLower();

                    string[] parts = lineRead.Split(new char[] { '\t' });
                    string line = parts[0];

                    int pos = line.IndexOf(':');
                    if (pos > 0)
                    {
                        int freq = 0;
                        if (int.TryParse(line.Substring(pos + 1), out freq))
                            line = line.Substring(0, pos);
                    }
                    lineIdx += 1;

                    string entity = CleanEntity(line);
                    if (WordVocab.ContainsKey(entity))
                    {
                        //Console.WriteLine("Vocab Line {0}, entity {1} duplicated !", lineIdx, entity);
                        continue;
                    }
                    WordVocab.Add(entity, wordIdx++);

                    string repWord = parts.Length > 1 ? parts[1] : line;
                    string[] matchedTexts = repWord.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < matchedTexts.Length; ++i)
                    {
                        string representation = CleanEntity(matchedTexts[i]);
                        if (!WordVocab.ContainsKey(representation)) WordVocab.Add(representation, wordIdx - 1);

                        string[] words = representation.Split(new char[] { ' ' });

                        int numTerm = words.Length;
                        if (numTerm > maxNumOfWordsInAnEntity) maxNumOfWordsInAnEntity = numTerm;

                        string cur = "";
                        for (int j = 0; j < numTerm; ++j)
                        {
                            if (cur.Length > 0) cur = cur + " ";
                            cur = cur + words[j];
                            if (!WordPrefix.Contains(cur)) WordPrefix.Add(cur);
                        }
                    }

                }
            }
            if (this.handleOOV)
                wordIdx++;
            Console.WriteLine("############# Vocabulary size is :{0} ###################", wordIdx);
        }

        public L3GFreqWordTextHash(Dictionary<string, int> letterGramVocab, Dictionary<string, int> wordVocab, string[] wordSplitters = null, int maxWordNum = int.MaxValue,
                int maxSeqNum = int.MaxValue, int maxWordLength = int.MaxValue)
        {
            this.L3GVocab = letterGramVocab ?? new Dictionary<string, int>();
            this.WordVocab = wordVocab ?? new Dictionary<string, int>();

            wordIdx = 0;
            maxNumOfWordsInAnEntity = 0;
            foreach (KeyValuePair<string, int> kvp in this.L3GVocab)
            {
                if (kvp.Value + 1 > wordIdx) wordIdx = kvp.Value + 1;
            }

            foreach (KeyValuePair<string, int> kvp in this.WordVocab)
            {
                if (kvp.Value + 1 > wordIdx) wordIdx = kvp.Value + 1;
                int wordsNum = kvp.Key.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Length;
                if (wordsNum > maxNumOfWordsInAnEntity) maxNumOfWordsInAnEntity = wordsNum;
            }

            if (wordSplitters == null)
            {
                wordSplitters = TextTokenizeParameters.WordTokens;
            }

            this.wordSplitters = wordSplitters;
            this.maxWordNum = maxWordNum;
            this.maxSeqNum = maxSeqNum;
            this.maxWordLength = maxWordLength;
        }

        public L3GFreqWordTextHash(string wordFileName, string l3gFileName, SimpleTextTokenizer tokenizer, 
            bool handleOOV = false, int LetterLength = 3) : this(wordFileName, l3gFileName, handleOOV, LetterLength, tokenizer.WordSplitter, tokenizer.MaxWordNum, 
                tokenizer.MaxSentenceNum, tokenizer.MaxWordLength)
        {

        }

        public override List<Dictionary<int, float>> SeqFeatureExtract(string text)
        {
            string newtext = text;
            if (this.Remove_HTML_Tag)
                newtext = TextUtil.CleanHTMLText(newtext);
            string[] words = newtext.Trim().ToLower().Split(wordSplitters, StringSplitOptions.RemoveEmptyEntries);
            List<Dictionary<int, float>> feas = new List<Dictionary<int, float>>();
            Dictionary<int, float> tmpFea = null;
            int wordNum = 0;
            List<string> filterwords = new List<string>();
            for (int i = 0; i < words.Length; ++i)
            {
                string word = words[i];
                if (feas.Count < maxSeqNum) //TextHashParameters.MaximumSeqTextLength)
                    tmpFea = new Dictionary<int, float>();

                // this deals with 1. single-word entity, and 2. multi-word entity in urls
                Word2Feature(word, tmpFea, maxWordLength);

                string proposedEntity = word;
                for (int len = 2; len <= maxNumOfWordsInAnEntity && i + len <= words.Length; ++len)
                {
                    if (proposedEntity.Length > 0) proposedEntity = proposedEntity + " ";
                    proposedEntity = proposedEntity + words[i + len - 1].ToLower().Trim();
                    if (!WordPrefix.Contains(proposedEntity)) break; // important speedup
                    if (WordVocab.ContainsKey(proposedEntity))
                    {
                        int id = WordVocab[proposedEntity];
                        if (tmpFea.ContainsKey(id))
                        {
                            tmpFea[id] += 5; // entity of length > 1 has more weight
                        }
                        else
                        {
                            tmpFea.Add(id, 5); // entity of length > 1 has more weight
                        }
                    }
                }

                if (tmpFea.Count > 0 && feas.Count < maxSeqNum) //TextHashParameters.MaximumSeqTextLength)
                    feas.Add(tmpFea);
                if (++wordNum >= maxWordNum) // TextHashParameters.MaximumWordNumber)
                    break;
            }
            return feas;
        }

        public List<List<Dictionary<int, float>>> MultiSeqFeatureExtract(string text, string[] wordSplitters, string[] sentSplitters,
            int maxWordNum, int maxSeqNum, int maxWordLength = int.MaxValue)
        {
            string newtext = text;
            if (this.Remove_HTML_Tag)
                newtext = TextUtil.CleanHTMLText(newtext);
            List<List<Dictionary<int, float>>> feaList = new List<List<Dictionary<int, float>>>();
            ITextTokenizer textTokenizer = new SimpleTextTokenizer(wordSplitters, sentSplitters, maxWordNum, maxWordLength, maxSeqNum);
            foreach (string sentence in textTokenizer.SentenceTokenize(newtext))
            {
                List<Dictionary<int, float>> fea = SeqFeatureExtract(sentence);
                if (fea.Count > 0) feaList.Add(fea);
            }
            return feaList;
        }

        public override List<List<Dictionary<int, float>>> MultiSeqFeatureExtract(string text)
        {
            List<List<Dictionary<int, float>>> feaList = new List<List<Dictionary<int, float>>>();
            foreach (string sentence in TextTokenizer.SentenceTokenize(text))
            {
                List<Dictionary<int, float>> fea = SeqFeatureExtract(sentence);
                if (fea.Count > 0) feaList.Add(fea);
            }
            return feaList;
        }

        private string CleanEntity(string entity)
        {
            return string.Join(" ", entity.ToLower().Trim().Split(TextTokenizeParameters.WordTokens, StringSplitOptions.RemoveEmptyEntries));
        }

        public override int VocabSize { get { return wordIdx; } }

        protected void Word2Feature(string word, Dictionary<int, float> fea, int wordLengthLimit = int.MaxValue)
        {
            if (word.Length < wordLengthLimit)
            {
                string mword = "#" + word + "#";
                for (int i = 0; i < mword.Length - LG + 1; i++)
                {
                    string letterNGram = mword.Substring(i, LG);
                    int vindex = -1;
                    if (L3GVocab.TryGetValue(letterNGram, out vindex))
                    {
                        if (fea.ContainsKey(vindex)) fea[vindex] = fea[vindex] + 1;
                        else fea[vindex] = 1;
                    }
                    else if (handleOOV)
                    {
                        vindex = wordIdx - 1;
                        if (fea.ContainsKey(vindex)) fea[vindex] = fea[vindex] + 1;
                        else fea[vindex] = 1;
                    }
                }
                if (WordVocab.ContainsKey(word))
                {
                    fea[WordVocab[word]] = 2;
                }
            }
            else
            {
                string[] parts = word.Split("-=".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < parts.Length; ++i)
                {
                    string proposedEntity = "";
                    for (int len = 1; len <= maxNumOfWordsInAnEntity && i + len <= parts.Length; ++len)
                    {
                        if (proposedEntity.Length > 0) proposedEntity = proposedEntity + " ";
                        proposedEntity = proposedEntity + parts[i + len - 1].ToLower().Trim();
                        if (!WordPrefix.Contains(proposedEntity)) break; // important speedup
                        if (WordVocab.ContainsKey(proposedEntity))
                        {
                            int id = WordVocab[proposedEntity];
                            if (fea.ContainsKey(id))
                            {
                                fea[id] += 2; // single-term entity word has a weight of 2
                            }
                            else
                            {
                                fea.Add(id, 2);
                            }
                        }
                    }
                }
            }
        }
    }

    public class AdvanceTextHash : BasicTextHash
    {
        public ItemFreqIndexDictionary letterFreq = new ItemFreqIndexDictionary("LetterFreq");
        public ItemFreqIndexDictionary letterNFreq = new ItemFreqIndexDictionary("LetterNgramFreq");
        public ItemFreqIndexDictionary wordFreq = new ItemFreqIndexDictionary("WordFreq");
        public bool withWordFreq, withLetterNGram, withLetter;
        public int LnG { get; set; }
        public AdvanceTextHash(string fileName) { Deserialize(new StreamReader(new FileStream(fileName, FileMode.Open, FileAccess.Read), Encoding.UTF8)); }

        public AdvanceTextHash(ITextTokenizer textTokenizer, int lng, bool enableWord = true, bool enableLetterNGram = true, bool enableLetter = true) : base(textTokenizer)
        {
            LnG = lng;
            withWordFreq = enableWord;
            withLetterNGram = enableLetterNGram;
            withLetter = enableLetter;
        }

        public AdvanceTextHash(string fileName, ITextTokenizer textTokenizer) : base(textTokenizer) { Deserialize(new StreamReader(new FileStream(fileName, FileMode.Open, FileAccess.Read), Encoding.UTF8)); }

        public override void Serialize(StreamWriter writer)
        {
            writer.WriteLine("LNG\t{0}", LnG);
            writer.WriteLine("LetterNum\t{0}", letterFreq.ItemDictSize);
            writer.WriteLine("LetterNGram\t{0}", letterNFreq.ItemDictSize);
            writer.WriteLine("wordNum\t{0}", wordFreq.ItemDictSize);
            letterFreq.Save(writer);
            letterNFreq.Save(writer);
            wordFreq.Save(writer);
        }

        public override void Deserialize(StreamReader reader)
        {
            LnG = int.Parse(reader.ReadLine().Split('\t')[1]);
            reader.ReadLine();
            reader.ReadLine();
            reader.ReadLine();
            letterFreq = new ItemFreqIndexDictionary(reader);
            letterNFreq = new ItemFreqIndexDictionary(reader);
            wordFreq = new ItemFreqIndexDictionary(reader);
        }

        public override void VocabExtract(string text)
        {
            if (withLetter)
            {
                foreach (string c in TextTokenizer.LetterTokenize(text))
                {
                    letterFreq.PushItem(c.ToString());
                }
            }
            foreach (string word in TextTokenizer.WordTokenize(text))
            {
                if (withWordFreq)
                    wordFreq.PushItem(word);
                if (withLetterNGram)
                    foreach (string lng in TextTokenizer.LnGTokenize(word, LnG))
                        letterNFreq.PushItem(lng);
            }
        }

    }

    public class AdvanceWordTextHash : AdvanceTextHash
    {
        public AdvanceWordTextHash(string fileName) : base(fileName) { }

        public AdvanceWordTextHash(ITextTokenizer textTokenizer, int lng) : base(textTokenizer, lng) { }

        public AdvanceWordTextHash(string fileName, ITextTokenizer textTokenizer) : base(fileName, textTokenizer) { }

        public override int VocabSize { get { return letterNFreq.ItemDictSize + wordFreq.ItemDictSize; } }

        public override Dictionary<int, float> FeatureExtract(string word)
        {
            Dictionary<int, float> fea = new Dictionary<int, float>();
            foreach (string lng in TextTokenizer.LnGTokenize(word, LnG))
            {
                int idx = letterNFreq.IndexItem(lng);
                if (idx >= 0)
                {
                    if (fea.ContainsKey(idx)) fea[idx] = fea[idx] + 1;
                    else fea[idx] = 1;
                }
            }
            int wIdx = wordFreq.IndexItem(word);
            if (wIdx >= 0) fea[wIdx + letterNFreq.ItemDictSize] = 1;
            return fea;
        }
    }


    public class AdvanceLetterTextHash : AdvanceTextHash
    {
        public AdvanceLetterTextHash(string fileName) : base(fileName) { }

        public AdvanceLetterTextHash(ITextTokenizer textTokenizer) : base(textTokenizer, 0, false, false) { }

        public AdvanceLetterTextHash(string fileName, ITextTokenizer textTokenizer) : base(fileName, textTokenizer) { }

        public AdvanceLetterTextHash(string fileName, ITextTokenizer textTokenizer, string freqWordFileName, int topKFreq) : base(fileName, textTokenizer)
        {
            LoadFreqWordFile(freqWordFileName, topKFreq);
        }

        public void LoadFreqWordFile(string freqWordFileName, int topkFreq = int.MaxValue)
        {
            string[] lines = File.ReadAllLines(freqWordFileName);
            for (int i = 0; i < Math.Min(lines.Length, topkFreq); i++)
            {
                FreqWordDict.PushItem(lines[i].Split('\t')[0]);
            }
        }

        public ItemFreqIndexDictionary FreqWordDict = new ItemFreqIndexDictionary("FreqWord");

        public override int VocabSize { get { return letterFreq.ItemDictSize + FreqWordDict.ItemDictSize; } }

        public override Dictionary<int, float> FeatureExtract(string word)
        {
            Dictionary<int, float> fea = new Dictionary<int, float>();
            foreach (string letter in TextTokenizer.LetterTokenize(word))
            {
                int idx = letterFreq.IndexItem(letter);
                if (idx >= 0)
                {
                    if (fea.ContainsKey(idx)) fea[idx] = fea[idx] + 1;
                    else fea[idx] = 1;
                }
            }
            return fea;
        }

        public override List<Dictionary<int, float>> SeqFeatureExtract(string text)
        {
            string newtext = text;
            if (this.Remove_HTML_Tag)
                newtext = TextUtil.CleanHTMLText(newtext);
            List<Dictionary<int, float>> feaList = new List<Dictionary<int, float>>();

            if (FreqWordDict.ItemDictSize == 0)
            {
                foreach (string letter in TextTokenizer.LetterTokenize(newtext))
                {
                    int idx = letterFreq.IndexItem(letter);
                    Dictionary<int, float> dict = new Dictionary<int, float>();
                    if (idx >= 0) { dict[idx] = 1; feaList.Add(dict); }
                }
            }
            else
            {
                for (int i = 0; i < text.Length; i++)
                {
                    Dictionary<int, float> dict = new Dictionary<int, float>();
                    char c = text[i];
                    char[] cstr = new char[1] { c };
                    int idx = letterFreq.IndexItem(new string(cstr));
                    if (idx >= 0)
                    {
                        dict.Add(idx, 1);
                    }

                    for (int e = 1; e <= 8; e++)
                    {
                        if (i + e > text.Length || i + e >= TextTokenizer.GetMaxWordLength()) break;
                        string subStr = text.Substring(i, e);
                        idx = FreqWordDict.IndexItem(subStr);
                        if (idx >= 0)
                        {
                            dict.Add(idx + letterFreq.ItemDictSize, 1);
                        }
                    }
                    if (dict.Count > 0)
                        feaList.Add(dict);
                    if(i >= TextTokenizer.GetMaxWordLength()) break;
                }
            }
            return feaList;
        }
        
        public List<int> SeqIndexExtract(string text, bool oov, bool isInsertBgn, bool isInsertEnd)
        {
            List<int> result = new List<int>();
            if (isInsertBgn) result.Add(0);
            foreach (string letter in TextTokenizer.LetterTokenize(text))
            {
                int idx = letterFreq.IndexItem(letter);
                if (idx >= 0) { result.Add(idx + 2); }
                else { result.Add(1); }
            }
            if (isInsertEnd) result.Add(0);
            return result;
        }
    }




    public class AdvanceBiCharacterGramTextHash : AdvanceTextHash
    {
        public AdvanceBiCharacterGramTextHash(string fileName) : base(fileName) { }

        public AdvanceBiCharacterGramTextHash(ITextTokenizer textTokenizer) : base(textTokenizer, 0) { }

        public AdvanceBiCharacterGramTextHash(string fileName, ITextTokenizer textTokenizer) : base(fileName, textTokenizer) { }

        public override int VocabSize { get { return letterFreq.ItemDictSize + letterNFreq.ItemDictSize; } }

        public override Dictionary<int, float> FeatureExtract(string word)
        {
            throw new Exception("not implemented");
            //Dictionary<int, float> fea = new Dictionary<int, float>();
            //foreach (string letter in TextTokenizer.LetterTokenize(word))
            //{
            //    int idx = letterFreq.IndexItem(letter);
            //    if (idx >= 0)
            //    {
            //        if (fea.ContainsKey(idx)) fea[idx] = fea[idx] + 1;
            //        else fea[idx] = 1;
            //    }
            //}
            //return fea;
        }

        public override List<Dictionary<int, float>> SeqFeatureExtract(string text)
        {
            List<Dictionary<int, float>> feaList = new List<Dictionary<int, float>>();
            for (int i = 0; i < text.Length; i++)
            {
                Dictionary<int, float> dict = new Dictionary<int, float>();
                char c = text[i];
                char[] cstr = new char[1] { c };
                int idx = letterFreq.IndexItem(new string(cstr));
                if (idx >= 0)
                {
                    dict.Add(idx, 1);
                }
                if (i < text.Length - 1)
                {
                    string bicharater = text.Substring(i, 2);
                    idx = letterNFreq.IndexItem(bicharater);
                    if (idx >= 0)
                    {
                        dict.Add(idx + letterFreq.ItemDictSize, 1);
                    }
                }
                // need to deal with OOV in next step
                if (dict.Count > 0)
                    feaList.Add(dict);
            }
            //foreach (string letter in TextTokenizer.LetterTokenize(text))
            //{
            //    int idx = letterFreq.IndexItem(letter);
            //    Dictionary<int, float> dict = new Dictionary<int, float>();
            //    if (idx >= 0) { dict[idx] = 1; feaList.Add(dict); }
            //}
            return feaList;
        }
    }

    public class Word2Freq
    {
        public Dictionary<string, int> Vocab { get; private set; }
        public float AvgDocLen { get; set; }
        public void SaveVocab(string fileName)
        {
            using (StreamWriter writer = new StreamWriter(fileName))
            {
                writer.Write(TextUtil.Dict2Str<string, int>(Vocab));
            }
        }

        public void LoadVocab(string fileName)
        {
            using (StreamReader reader = new StreamReader(fileName))
            {
                Vocab = TextUtil.Str2Dict<string, int>(reader.ReadLine());
            }
        }

        public Word2Freq(string fileName)
        {
            LoadVocab(fileName);
        }

        public Word2Freq(string fileName, int[] columnIdx, int vocabSize, string[] spliters, string outputFileName, int threshould, bool englishOnly)
        {
            float totalDocLen = 0;
            int docNum = 0;
            Vocab = new Dictionary<string, int>();
            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string[] recordItems = reader.ReadLine().Split('\t');

                    HashSet<string> wordSet = new HashSet<string>();
                    foreach (int cid in columnIdx)
                    {
                        if (recordItems.Length <= cid) continue;
                        string text = recordItems[cid].Trim().ToLower();
                        string[] words = text.Trim().ToLower().Split(spliters, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string w in words)
                        {
                            if (englishOnly && !TextUtil.IsEnglishNumber(w))
                                continue;
                            wordSet.Add(w.Trim());
                            totalDocLen += 1;
                        }
                    }

                    foreach (string word in wordSet)
                    {
                        if (Vocab.ContainsKey(word)) Vocab[word] = Vocab[word] + 1;
                        else Vocab[word] = 1;
                    }
                    docNum += 1;
                }
            }
            AvgDocLen = totalDocLen * 1.0f / (docNum + float.Epsilon);

            Vocab = Vocab.Where(entry => entry.Value >= threshould).OrderByDescending(entry => entry.Value).Take(vocabSize)
                                            .ToDictionary(pair => pair.Key, pair => pair.Value);
            SaveVocab(outputFileName);
        }

        public Word2Freq(IEnumerable<string> doc, string[] spliters, bool englishOnly)
        {
            float totalDocLen = 0;
            Vocab = new Dictionary<string, int>();
            foreach (string item in doc)
            {
                IEnumerable<string> strList = item.Trim().ToLower().Split(spliters, StringSplitOptions.RemoveEmptyEntries).Where(i => englishOnly && !TextUtil.IsEnglishNumber(i) ? false : true).
                        Select(i => i.Trim());
                HashSet<string> wordSet = new HashSet<string>(strList);
                totalDocLen += strList.Count();
                foreach (string word in wordSet)
                {
                    if (Vocab.ContainsKey(word)) Vocab[word] = Vocab[word] + 1;
                    else Vocab[word] = 1;
                }
            }
            AvgDocLen = totalDocLen * 1.0f / (doc.Count() + float.Epsilon);
        }

        //public Word2Freq(string fileName, int[] columnIdx, int vocabSize, string[] spliters, string outputFileName)
        //    : this(fileName, columnIdx, vocabSize, spliters, outputFileName, 1, false)
        //{ }

        public Word2Freq(string fileName, int columnIdx, int vocabSize, string[] spliters, string outputFileName)
            : this(fileName, new int[] { columnIdx }, vocabSize, spliters, outputFileName, 1, false)
        { }
    }

    //public class L3G2Freq
    //{
    //    public Dictionary<string, int> Vocab { get; private set; }

    //    public void SaveVocab(string fileName)
    //    {
    //        using (StreamWriter writer = new StreamWriter(fileName))
    //        {
    //            writer.Write(string.Join("\n", Vocab.Select(i => i.Key + "\t" + i.Value.ToString())));
    //        }
    //    }

    //    public void LoadVocab(string fileName)
    //    {
    //        using (StreamReader reader = new StreamReader(fileName))
    //        {
    //            string feaStr = reader.ReadToEnd();
    //            Vocab = feaStr == null || feaStr.Trim().Equals("") ? new Dictionary<string, int>() : feaStr.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries).
    //                Select(item => item.Split('\t')).ToDictionary(kv => kv[0], kv => int.Parse(kv[1]));
    //        }
    //    }

    //    public L3G2Freq(string fileName)
    //    {
    //        LoadVocab(fileName);
    //    }

    //    public L3G2Freq(Word2Freq wordFreq, string outputFileName)
    //    {
    //        Vocab = new Dictionary<string, int>();

    //        foreach (KeyValuePair<string, int> word in wordFreq.Vocab)
    //        {
    //            string mword = "#" + word.Key + "#";
    //            for (int i = 0; i < mword.Length - 3 + 1; i++)
    //            {
    //                string letterNGram = mword.Substring(i, 3);
    //                if (Vocab.ContainsKey(letterNGram)) Vocab[letterNGram] = Vocab[letterNGram] + word.Value;
    //                else Vocab[letterNGram] = word.Value;
    //            }
    //        }

    //        Vocab = Vocab.OrderByDescending(entry => entry.Value)
    //                             .ToDictionary(pair => pair.Key, pair => pair.Value);
    //        SaveVocab(outputFileName);
    //    }
    //}

    public class Vocab2Freq
    {
        public Dictionary<int, int> Vocab { get; private set; }

        public Dictionary<int, string> VocabTerm { get; private set; }

        public Dictionary<string, int> VocabTermIndex { get; private set; }

        public string VocabString(int id)
        {
            return VocabTerm[id];
        }

        public void LoadVocab(string fileName)
        {
            Vocab = new Dictionary<int, int>();
            VocabTerm = new Dictionary<int, string>();
            VocabTermIndex = new Dictionary<string, int>();

            using (StreamReader reader = new StreamReader(fileName, Encoding.Default, true))
            {
                while (!reader.EndOfStream)
                {
                    string[] items = reader.ReadLine().Split('\t');
                    string term = items[0];
                    int vID = int.Parse(items[1]);
                    if (items.Length >= 3)
                    {
                        int vNum = int.Parse(items[2]);
                        if (Vocab.ContainsKey(vID)) Vocab[vID] += vNum;
                        else Vocab[vID] = vNum;
                    }
                    else
                    {
                        Vocab[vID] = 0;
                    }
                    VocabTerm[vID] = term;
                    if (!VocabTermIndex.ContainsKey(term)) VocabTermIndex[term] = vID;
                }
            }

            int maxKey = Vocab.Keys.Max();
            for (int i = 0; i <= maxKey; i++)
            {
                if (!Vocab.ContainsKey(i))
                {
                    Vocab[i] = 0;

                    VocabTerm[i] = "Unknown";
                }
            }
        }

        public Vocab2Freq(string fileName)
        {
            LoadVocab(fileName);
        }

        public List<List<int>> Vocab2ClassV2(int classNum)
        {
            List<List<int>> class2Idx = new List<List<int>>();
            List<KeyValuePair<int, int>> vocabSort = Vocab.OrderByDescending(m => m.Value).ToList();

            double a = 0;
            double df = 0;
            double b = vocabSort.Select(i => i.Value).Sum();

            List<int> cluster = new List<int>();
            for (int i = 0; i < vocabSort.Count; i++)
            {
                if (vocabSort[i].Value == 0)
                    continue;
                df += vocabSort[i].Value / (double)b;
                if (df > 1) df = 1;
                if (df > (a + 1) / (double)classNum)
                {
                    //vocab[i].class_index = a;
                    cluster.Add(vocabSort[i].Key);

                    if (a < classNum - 1)
                    {
                        a++;
                        class2Idx.Add(cluster);
                        cluster = new List<int>();
                    }
                }
                else
                {
                    cluster.Add(vocabSort[i].Key);
                    //vocab[i].class_index = a;
                }
            }

            if (cluster.Count > 0)
                class2Idx.Add(cluster);

            return class2Idx;
        }

        public List<List<int>> Vocab2Class(int classNum)
        {
            List<List<int>> class2Idx = new List<List<int>>();
            List<KeyValuePair<int, int>> vocabSort = Vocab.OrderByDescending(m => m.Value).ToList();
            int vocabNum = vocabSort.Count / classNum; // (vocabSort.Count - 1) / classNum + 1;

            for (int i = 0; i < classNum; i++)
            {
                List<int> vocabList = new List<int>();
                for (int s = i * vocabNum; s < (i + 1) * vocabNum; s++)
                {
                    vocabList.Add(vocabSort[s].Key);
                }

                if (i == classNum - 1)
                {
                    for (int s = (i + 1) * vocabNum; s < vocabSort.Count; s++)
                    {
                        vocabList.Add(vocabSort[s].Key);
                    }
                }

                class2Idx.Add(vocabList);
            }

            return class2Idx;
        }
    }
}
