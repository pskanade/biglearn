﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{

    public class ComputationGraph
    {
        /// <summary>
        /// Computation Flow.
        /// </summary>
        public List<StructRunner> DataRunners = new List<StructRunner>();

        public List<StructRunner> SequenceRunners = new List<StructRunner>();

        public List<ObjectiveRunner> ObjectiveRunners = new List<ObjectiveRunner>();

        public List<GradientOptimizer> OptimizerRunners = new List<GradientOptimizer>();

        public List<StructRunner> ParameterRunners = new List<StructRunner>();

        public BatchData AddRunner(StructRunner runner)
        {
            SequenceRunners.Add(runner);
            return runner.Output;
        }

        public void AddObjective(ObjectiveRunner runner)
        {
            ObjectiveRunners.Add(runner);
        }

        public BatchData AddDataRunner(StructRunner runner)
        {
            DataRunners.Add(runner);
            return runner.Output;
        }

        public BatchData AddParameterRunner(StructRunner runner)
        {
            ParameterRunners.Add(runner);
            return runner.Output;
        }

        /// <summary>
        /// P 
        /// </summary>
        public bool IsTerminate = false;   /// Sign for end the outer loop.

        /// Break
        public bool IsContinue = true;     /// Sign for end the inner loop.

        int StepNum = 0;
        double AvgLoss = 0;
        double totalLoss = 0;
        public double StepLoss { get; set; }

        public CompositeNNStructure Model { get; set; }
        
        /// <summary>
        /// Forward. 
        /// </summary>
        /// <returns></returns>
        public bool Forward()
        {
            IsTerminate = false;
            IsContinue = true;

            foreach (StructRunner runner in DataRunners)
            {
                runner.Forward();

                IsTerminate = IsTerminate || runner.IsTerminate;
                IsContinue = IsContinue && runner.IsContinue;
            }

            if (IsTerminate || !IsContinue) return false;

            int idx = 0;
            foreach (StructRunner runner in SequenceRunners)
            {
                runner.Forward();
                IsTerminate = runner.IsTerminate;
                IsContinue = runner.IsContinue;
                if (IsTerminate || !IsContinue) return false;
                idx += 1;
            }
            return true;
        }

        public void BeforeOptimization()
        {
            foreach (GradientOptimizer runner in OptimizerRunners) runner.BeforeGradient();
        }

        public void AfterOptimization()
        {
            foreach (GradientOptimizer runner in OptimizerRunners)
            {
                runner.PrepareAfterGradient();
                runner.AfterGradient();
                runner.ConfirmAfterGradient();
            }
        }

        public void CleanDeriv()
        {
            foreach (StructRunner runner in SequenceRunners) { runner.CleanDeriv(); }
            foreach (StructRunner runner in DataRunners) { runner.CleanDeriv(); }
        }

        public void Backward()
        {
            foreach (StructRunner runner in Enumerable.Reverse(SequenceRunners)) { if (runner.IsBackProp) runner.Backward(false); }
            foreach (StructRunner runner in Enumerable.Reverse(DataRunners)) { if (runner.IsBackProp) runner.Backward(false); }
        }

        public void Update()
        {
            foreach (StructRunner runner in SequenceRunners) { if(runner.IsBackProp && runner.IsUpdate) runner.Update(); }
            foreach (StructRunner runner in DataRunners) { if (runner.IsBackProp && runner.IsUpdate) runner.Update(); }
        }

        public double Loss()
        {
            double loss = 1;
            foreach (ObjectiveRunner runner in ObjectiveRunners) { runner.Forward(); loss = loss * runner.ObjectiveScore; }
            return loss;
        }

        public void ParameterRegular()
        {
            foreach(StructRunner runner in ParameterRunners) { runner.Forward(); }
        }

        public Dictionary<string,double> MultiLoss()
        {
            Dictionary<string, double> loss = new Dictionary<string, double>();
            foreach (ObjectiveRunner runner in ObjectiveRunners)
            {
                runner.Forward();
                foreach (KeyValuePair<string, double> item in runner.ObjectiveDict)
                {
                    if (!loss.ContainsKey(item.Key))
                    {
                        loss[runner.name + "-" + item.Key] = item.Value;
                    }
                    else
                    {
                        loss[runner.name + "-" + item.Key] += item.Value;
                    }
                }
                loss[runner.name] = runner.ObjectiveScore;
            }
            return loss;
        }

        public void Complete()
        {
            foreach (StructRunner runner in DataRunners) runner.Complete();
            foreach (StructRunner runner in SequenceRunners) runner.Complete();
            foreach (ObjectiveRunner runner in ObjectiveRunners) runner.Complete();
            foreach (GradientOptimizer runner in OptimizerRunners) runner.Complete();
            foreach (StructRunner runner in ParameterRunners) runner.Complete();
        }

        public void Init()
        {
            foreach (StructRunner runner in DataRunners) runner.Init();
            foreach (StructRunner runner in SequenceRunners) runner.Init(); 
            foreach (ObjectiveRunner runner in ObjectiveRunners) runner.Init();
            foreach (GradientOptimizer runner in OptimizerRunners) runner.Init();
            foreach (StructRunner runner in ParameterRunners) runner.Init();

            IsTerminate = false;
            IsContinue = true;
            StepNum = 0;
            totalLoss = 0;
        }

        public bool Step()
        {
            if (!IsTerminate)
            {
                //var forwardCounter = PerfCounter.Manager.Instance["Forward"].Begin();
                Forward();
                //PerfCounter.Manager.Instance["Forward"].TakeCount(forwardCounter);

                if (!IsTerminate && IsContinue)
                {
                    ++StepNum;
                    /// no objective function.
                    if (ObjectiveRunners.Count > 0)
                    {
                        //var cleanCounter = PerfCounter.Manager.Instance["Clean"].Begin();
                        CleanDeriv();
                        //PerfCounter.Manager.Instance["Clean"].TakeCount(cleanCounter);
                        BeforeOptimization();

                        StepLoss = Loss();
                        totalLoss += StepLoss;
                        AvgLoss = (StepNum - 1) * 1.0f / (StepNum) * AvgLoss + 1.0f / (StepNum) * StepLoss;

                        //var backCounter = PerfCounter.Manager.Instance["Backward"].Begin();
                        Backward();
                        //PerfCounter.Manager.Instance["Backward"].TakeCount(backCounter);

                        //var updateCounter = PerfCounter.Manager.Instance["Update"].Begin();
                        Update();
                        //PerfCounter.Manager.Instance["Update"].TakeCount(updateCounter);

                        AfterOptimization();

                        ParameterRegular();
                        /*
                        if(StepNum % 10 == 0)
                        {
                            Console.WriteLine(PerfCounter.Manager.Instance["Forward"].ToString());
                            Console.WriteLine(PerfCounter.Manager.Instance["Clean"].ToString());
                            Console.WriteLine(PerfCounter.Manager.Instance["Backward"].ToString());
                            Console.WriteLine(PerfCounter.Manager.Instance["Update"].ToString());
                        }*/

                        if (StepNum % 50 == 0)
                            Logger.WriteLog("Train Batch Num {0}, AvgLoss {1}", StepNum, AvgLoss);
                    }
                    else if (StepNum % 500 == 0) Console.WriteLine("MiniBatch {0}", StepNum);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public int StatusReportSteps = 100;
        public bool IsReportObjectives = true;
        
        /// <summary>
        /// Execute Computation Graph without data source.
        /// or Data Source is another data runner.
        /// </summary>
        /// <returns></returns>
        public double Execute(bool withUpdate = true)
        {
            DateTime pre = DateTime.Now;
            //double loss = 0;

            Dictionary<string, double> multiObj = new Dictionary<string, double>();

            int batchIdx = 0;
            var trainingCounter = PerfCounter.Manager.Instance["Running"].Begin();

            int trackMiniBatch = -1;
            if (ParameterSetting.DEBUG && DeepNet != null)
            {
                DeepNet.TrackModelParameter();
                trackMiniBatch = 1;
            }

            Init();
            
            while (!IsTerminate)
            {
                PerfCounter.Manager.Instance["Forward"].CountOperation(() => Forward());
                if(!IsTerminate && IsContinue)
                {
                    /// no objective function.
                    if (ObjectiveRunners.Count > 0)
                    {
                        PerfCounter.Manager.Instance["CleanDerivation"].CountOperation(() => CleanDeriv());

                        //delegate the model optimizer in Computation Graph.
                        if ( withUpdate) { BeforeOptimization(); }
                        //foreach (ModelOptimizer optimizer in DeepNet.ModelOptimizers) optimizer.Optimizer.BeforeGradient();

                        //double tmpLoss = 0;
                        Dictionary<string, double> tmpLoss = null;
                        PerfCounter.Manager.Instance["LossDerivFunction"].CountOperation(() => tmpLoss = MultiLoss());

                        //loss = batchIdx * 1.0f / (batchIdx + 1) * loss + 1.0f / (batchIdx + 1) * tmpLoss["OBJ-LOSS"];
                        foreach (KeyValuePair<string, double> item in tmpLoss)
                        {
                            if(multiObj.ContainsKey(item.Key))
                            {
                                multiObj[item.Key] = batchIdx * 1.0f / (batchIdx + 1) * multiObj[item.Key] + 1.0f / (batchIdx + 1) * item.Value;
                            }
                            else
                            {
                                multiObj[item.Key] = 1.0f / (batchIdx + 1) * item.Value;
                            }
                        }

                        //totalLoss += tmpLoss["OBJ-LOSS"];
                        if (withUpdate)
                        {
                            PerfCounter.Manager.Instance["Backward"].CountOperation(() => Backward());
                            PerfCounter.Manager.Instance["Update"].CountOperation(() => Update());
                        }
                        if (withUpdate) { AfterOptimization(); }
                        
                        if (withUpdate) { ParameterRegular(); }
                        //
                        // after optimization. need to be done soon.
                        //
                        if (trackMiniBatch > 0) trackMiniBatch--;
                        if (trackMiniBatch == 0)
                        {
                            DeepNet.TrackModelParameter();
                            Console.WriteLine("batch idx {0}", batchIdx);
                            Console.WriteLine("Enter next minibatch count : ");
                            trackMiniBatch = int.Parse(Console.ReadLine());
                        }
                        //foreach (ModelOptimizer optimizer in DeepNet.ModelOptimizers) optimizer.Optimizer.AfterGradient(); }

                        if (++batchIdx % StatusReportSteps == 0)
                        {
                            Logger.WriteLog("Train Batch Num {0}:", batchIdx);
                            foreach(string key in multiObj.Keys)
                            {
                                Logger.WriteLog(" {0},  Avg  {1}, Current  {2}", key, multiObj[key], tmpLoss.ContainsKey(key) ? tmpLoss[key] : 0);
                            }
                        }

                        if (DeepNet != null && ParameterSetting.DEBUG)
                        {
                            if(DeepNet.TrackParameterNAN())
                            {
                                Console.WriteLine("From Batch Num {0} ,nan shows up.", batchIdx);
                            }

                            if(batchIdx % StatusReportSteps == 0)
                            {
                                DeepNet.TrackModelParameter();
                            }
                        }
                    }
                    else if (++batchIdx % StatusReportSteps == 0) Console.WriteLine("MiniBatch {0} at {1}", batchIdx, DateTime.Now);
                }
            }

            if (ObjectiveRunners.Count > 0 && IsReportObjectives)
            {
                Logger.WriteLog("Running Batch:{0}, Time cost: {1}s", batchIdx, (DateTime.Now - pre).TotalSeconds);
                foreach (string key in multiObj.Keys)
                {
                    Logger.WriteLog("{0},  Avg  {1}", key, multiObj[key]);
                }
            }

            Complete();

            //if (ObjectiveRunners.Count == 0) 
            //{
            //    foreach (StructRunner runner in SequenceRunners)
            //        if (typeof(ObjectiveRunner).IsAssignableFrom(runner.GetType()))
            //            loss += ((ObjectiveRunner)runner).ObjectiveScore;
            //}
            PerfCounter.Manager.Instance["Running"].TakeCount(trainingCounter);
            return 0;
        }




        Structure DeepNet = null;
        public void InitOptimizer(Structure deepNet, StructureLearner optimizer, RunnerBehavior behavior)
        {
            deepNet.InitOptimizer(optimizer, behavior);
            SetDelegateModel(deepNet);
        }

        public void SetDelegateModel(Structure deepNet)
        {
            DeepNet = deepNet;
            foreach (StructRunner runner in SequenceRunners) { runner.IsDelegateOptimizer = true; }
            foreach (StructRunner runner in DataRunners) { runner.IsDelegateOptimizer = true; }
            foreach (StructRunner runner in ObjectiveRunners) { runner.IsDelegateOptimizer = true; }
            OptimizerRunners.AddRange(deepNet.ModelOptimizers.Select(i => i.Optimizer));
        }
    }
}
