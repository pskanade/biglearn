﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class Logger
    {
        static StreamWriter logWriter = null;
        static bool useConsoleOnly = true;
        public static void OpenLog(string logFile, bool isOpenFile = true)
        {
            useConsoleOnly = !isOpenFile;
            if (useConsoleOnly == false)
            {
                int idx = 0;
                while (true)
                {
                    try
                    {
                        string filename = logFile.Substring(0, Math.Min(logFile.Length, 128)) + "." + idx.ToString();
                        logWriter = new StreamWriter(filename);
                        logWriter.AutoFlush = true;
                        break;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        idx++;
                    }
                }
            }
        }
        public static void CloseLog()
        {
            if (useConsoleOnly == false)
            {
                logWriter.Close();
            }
        }
        public static void WriteLog(string message)
        {
            Console.WriteLine(message);
            if (useConsoleOnly == false)
            {
                logWriter.Write(DateTime.Now.ToString() + "\t\t");
                logWriter.WriteLine(message);
            }
        }
        public static void WriteLog(string stringformat, params object[] objs)
        {
            Console.WriteLine(stringformat, objs);
            if (useConsoleOnly == false)
            {
                logWriter.Write(DateTime.Now.ToString() + "\t\t");
                logWriter.WriteLine(stringformat, objs);
            }
        }
        public static void WriteLog(string stringformat, object obj0)
        {
            Console.WriteLine(stringformat, obj0);
            if (useConsoleOnly == false)
            {
                logWriter.Write(DateTime.Now.ToString() + "\t\t");
                logWriter.WriteLine(stringformat, obj0);
            }
        }
        public static void WriteLog(string stringformat, object obj0, object obj1)
        {
            Console.WriteLine(stringformat, obj0, obj1);
            if (useConsoleOnly == false)
            {
                logWriter.Write(DateTime.Now.ToString() + "\t\t");
                logWriter.WriteLine(stringformat, obj0, obj1);
            }
        }
    }
}
