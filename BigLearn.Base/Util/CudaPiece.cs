﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class CudaPieceFloatDebug
    {
        public static void Print(string name, CudaPieceFloat data, int topK, bool halt = true)
        {
            Console.WriteLine("Variable Name " + name);
            Console.WriteLine("EffectiveSize " + data.EffectiveSize.ToString());
            Console.WriteLine("Size " + data.Size.ToString());
            data.SyncToCPU();
            for(int i = 0; i < Math.Min(topK, data.EffectiveSize); i++)
            {
                Console.WriteLine(data.MemPtr[i]);
            }   
            if(halt)
            {
                Console.WriteLine("Press to Continue!");
                Console.ReadLine();
            }
        }
    }

    /// <summary>
    /// xinson, 3/20/14. A cuda piece serves as a bridge/connect of the same piece of data between CPU and GPU.
    /// </summary>
    public class CudaPieceFloat : IDisposable
    {
        int _size = 0;
        int _bufferIncr = 1024;
        int _effectiveSize = 0;
        int _offset = 0;

        DeviceType deviceType = DeviceType.GPU;
        public DeviceType DeviceType { get { return deviceType; } private set { deviceType = value; } }
        /// <summary>
        /// 1. Disable user to set the actual effective size of data
        /// 2. Please try to use EffectiveSize, Size and Resize function will not be exposed to outside in the future.
        /// </summary>
        public int Size { get { return _size; } }
        public int Offset { get { return _offset; } private set { _offset = value; } }


        /// <summary>
        /// This function is very dangerous when running in computation graph.
        /// </summary>
        /// <param name="newSize"></param>
        public void Resize(int newSize)
        {
            if (cpuMemArray != null) Array.Resize(ref cpuMemArray, newSize);
            else cpuMemArray = new float[newSize];
            if (cudaPiecePointer != IntPtr.Zero)
            {
                CopyOutFromCuda(_size);
                Cudalib.CudaDeallocFloat(cudaPiecePointer);
                cudaPiecePointer = Cudalib.CudaAllocFloat(newSize);
                CopyIntoCuda(newSize);
                throw new Exception("Resize GPU Cuda Float is not allowed.");
            }
            _size = newSize;
        }

        public int EffectiveSize { get { return _effectiveSize; } set { if (value > Size) { Resize(value + _bufferIncr); } _effectiveSize = value; } }


        float[] cpuMemArray = null;

        /// <summary>
        /// MemPtr will be staled. Please use CpuPtr instead.
        /// </summary>
        public float[] MemPtr { get { return cpuMemArray; } }
        IntPtr cudaPiecePointer = IntPtr.Zero;
        public unsafe float * CpuPtr { get { if (MemPtr == null) return null; fixed (float* p = &MemPtr[Offset]) return p; } }
        public IntPtr CudaPtr
        {
            get { return cudaPiecePointer; }
        }

        static CudaPieceFloat empty = new CudaPieceFloat() { _size = 0, cpuMemArray = null, cudaPiecePointer = IntPtr.Zero };
        public static CudaPieceFloat Empty { get { return empty; } }
        public bool IsEmpty { get { return _size == 0; } }
        bool IsFakeMem = false;
        public CudaPieceFloat SubArray(int suboffset, int subsize)
        {
            CudaPieceFloat m = new CudaPieceFloat();
            m.cudaPiecePointer = IntPtr.Add(CudaPtr, sizeof(float) * suboffset);
            m.cpuMemArray = cpuMemArray;
            m.Offset = _offset + suboffset;
            m._size = subsize;
            m._effectiveSize = subsize;
            m.deviceType = deviceType;
            m.IsFakeMem = true;
            return m;
        }

        public CudaPieceFloat SubArray(int suboffset)
        {
            return SubArray(suboffset, _effectiveSize - suboffset);
        }

        /// <summary>
        /// Sync value from CPU.
        /// </summary>
        public void SyncFromCPU()
        {
            SyncFromCPU(_effectiveSize);
        }
        /// <summary>
        /// Sync value from CPU.
        /// </summary>
        public void SyncFromCPU(int msize)
        {
            if (cudaPiecePointer == null) return;
            else CopyIntoCuda(msize);
        }

        unsafe public void SyncFromCPU(int offset, float[] data, int offsetdata, int length)
        {
            if (length == 0 || data.Length == 0) return;
            if (cudaPiecePointer == IntPtr.Zero) { Buffer.BlockCopy(data, offsetdata * sizeof(float), MemPtr, offset * sizeof(float), length * sizeof(float)); }
            else
            {
                fixed (float* pb = &data[offsetdata])
                {
                    Cudalib.CudaCopyInFloat(IntPtr.Add(cudaPiecePointer, sizeof(float) * offset) , (IntPtr)pb, length);
                }
            }
        }

        /// <summary>
        /// Sync value to CPU
        /// </summary>
        public void SyncToCPU() { SyncToCPU(_effectiveSize); }

        /// <summary>
        /// Sync value to CPU
        /// </summary>
        public void SyncToCPU(int msize)
        {
            if (cudaPiecePointer == null) return;
            else CopyOutFromCuda(msize);
        }

        
        /// <summary>
        /// MemoryPointer.
        /// </summary>
        private CudaPieceFloat() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="length"></param>
        /// <param name="needCpuMem"></param>
        public CudaPieceFloat(int length, bool needCpuMem, bool needGpuMem)
        {
            _size = length;

            if (needCpuMem)
            {
                cpuMemArray = new float[_size];
                DeviceType = DeviceType.CPU_FAST_VECTOR;
            }
            if (needGpuMem)
            {
                if ((Int64)(cudaPiecePointer = Cudalib.CudaAllocFloat(_size > 0 ? _size : 1)) == 0)
                {
                    throw new Exception("Out of GPU Memo, use a smaller model!");
                }
                if(cudaPiecePointer == null)
                {
                    throw new Exception("allocate cuda memory fail.");
                }
                DeviceType = DeviceType.GPU;
            }
            _effectiveSize = _size;
            Zero();
        }

        public CudaPieceFloat(int length, DeviceType device) : this(length, true, device == DeviceType.GPU)
        {  }

        public CudaPieceFloat(BinaryReader reader, DeviceType device)
        {
            DeviceType = device;
            Deserialize(reader, device);
        }


        public unsafe void Serialize(BinaryWriter writer)
        {
            writer.Write(_effectiveSize);
            SyncToCPU();
            for (int i = 0; i < _size; i++) writer.Write(CpuPtr[i]);
        }

        public void Deserialize(BinaryReader reader, DeviceType device)
        {
            _size = reader.ReadInt32();
            cpuMemArray = new float[_size];
            if (device == DeviceType.GPU) cudaPiecePointer = Cudalib.CudaAllocFloat(_size);
            for (int i = 0; i < _size; i++) MemPtr[i] = reader.ReadSingle();
            EffectiveSize = _size;
            SyncFromCPU();
            
        }

        #region Dispose Function.
        private bool disposed = false;
        ~CudaPieceFloat()
        {
            this.Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (!IsFakeMem && cudaPiecePointer != IntPtr.Zero)
            {
                Cudalib.CudaDeallocFloat(cudaPiecePointer);
                cudaPiecePointer = IntPtr.Zero;
            }

            this.cpuMemArray = null;

            this._size = 0;

            this.disposed = true;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion.

        /// <summary>
        /// Copy data from GPU to CPU
        /// </summary>
        unsafe public void CopyOutFromCuda()
        {
            CopyOutFromCuda(_effectiveSize);
        }

        /// <summary>
        /// Copy data from GPU to CPU
        /// </summary>
        unsafe public void CopyOutFromCuda(int SpecifiedSize)
        {
            if (SpecifiedSize == 0)
                return;
            if (cudaPiecePointer == IntPtr.Zero)
            {
                return;
            }
            if (cpuMemArray == null)
            {
                throw new Exception("Error! Must set needCpuMem=true for CopyOutFromCuda() operation!");
            }

            Cudalib.CudaCopyOutFloat(cudaPiecePointer, (IntPtr)CpuPtr, SpecifiedSize);
        }

        /// <summary>
        /// Copy data from CPU to GPU
        /// </summary>
        unsafe public void CopyIntoCuda()
        {
            CopyIntoCuda(EffectiveSize);
        }

        /// <summary>
        /// Copy data from CPU to GPU
        /// </summary>
        unsafe public void CopyIntoCuda(int SpecifiedSize)
        {
            if (SpecifiedSize == 0) return;

            if (cudaPiecePointer == IntPtr.Zero) { return; }

            if (cpuMemArray == null)
            {
                throw new Exception("Error! Must set needCpuMem=true for CopyIntoCuda() operation!");
            }
            Cudalib.CudaCopyInFloat(cudaPiecePointer, (IntPtr)CpuPtr, SpecifiedSize);
        }

        public void InitByMultiplyingBias(float scale, float bias)
        {
            if (cpuMemArray == null)
            {
                throw new Exception("Error! Must set needCpuMem=true for Init() operation!");
            }
            Random random = ParameterSetting.Random;
            for (int i = 0; i < Size; ++i)
            {
                cpuMemArray[i] = (float)(random.NextDouble() * scale * bias);
            }
            CopyIntoCuda();
        }


        public void Normalize(int col, int row)
        {
            if (cpuMemArray == null)
            {
                throw new Exception("Error! Must set needCpuMem=true for Init() operation!");
            }
            Cudalib.Matrix_Normalize_By_Col(cudaPiecePointer, col, row);
            //CopyOutFromCuda();
        }

        public unsafe void Norm(int col, int row, VecNormType norm)
        {
            if (cpuMemArray == null)
            {
                throw new Exception("Error! Must set needCpuMem=true for Init() operation!");
            }
            if(norm == VecNormType.NoNorm) { return; }

            SyncToCPU(row * col);
            //Parallel.For(0, row, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount}, b =>
            for (int b = 0; b < row; b++)
            {
                //AVXFastVector p = new AVXFastVector(cpuMemArray, b * col, col, false);
                if (norm == VecNormType.L2Norm || norm == VecNormType.L2BallNorm)
                {
                    fixed(float * pcpu =  &cpuMemArray[b*col])
                    {
                        double x = SSElib.SSE_SumSq(pcpu, col) + float.Epsilon;
                        //double x = p.L2Norm + float.Epsilon;
                        if (x > 1 || norm == VecNormType.L2Norm)
                        {
                            SSElib.SSE_Scale((float)(1.0 / x), pcpu, col);
                            //p.Scale((float)(1.0 / x));
                        }
                    }
                }
            }
            //);
            SyncFromCPU(row * col);
        }

        public void CopyFrom(CudaPieceFloat refVector)
        {
            EffectiveSize = refVector.EffectiveSize;
            CopyFrom(0, refVector, 0, refVector.EffectiveSize);
        }

        unsafe public void CopyFrom(int offset, CudaPieceFloat refVector, int refOffset, int size)
        {
            /// Copy From GPU to CPU.
            if (CudaPtr == IntPtr.Zero && refVector.CudaPtr != IntPtr.Zero)
            {
                //fixed (float* pMemPtr = &MemPtr[offset])
                //{
                Cudalib.CudaCopyOutFloat((refVector.CudaPtr + sizeof(float) * refOffset), (IntPtr)(CpuPtr + offset), size);
                //}
            }

            /// Copy From GPU To GPU.
            if (CudaPtr != IntPtr.Zero && refVector.CudaPtr != IntPtr.Zero)
                Cudalib.CudaCopy(CudaPtr, refVector.CudaPtr, size, offset, refOffset);

            /// Copy From CPU to GPU.
            if (CudaPtr != IntPtr.Zero && refVector.CudaPtr == IntPtr.Zero)
            {
                //fixed (float* refMemPtr = &refVector.MemPtr[refOffset])
                //{
                //    Cudalib.CudaCopyInFloat(CudaPtr + sizeof(float) * offset, (IntPtr)refMemPtr, size);
                //}
                Cudalib.CudaCopyInFloat(CudaPtr + sizeof(float) * offset, (IntPtr)(refVector.CpuPtr + refOffset), size);
            }

            /// Copy From CPU to CPU.
            if (CudaPtr == IntPtr.Zero && refVector.CudaPtr == IntPtr.Zero)
                Buffer.BlockCopy(refVector.MemPtr, refOffset * sizeof(float), MemPtr, offset * sizeof(float), size * sizeof(float));
        }


        public unsafe void Init(float scale, float bias)
        {
            if (cpuMemArray == null)
            {
                throw new Exception("Error! Must set needCpuMem=true for Init() operation!");
            }
            if (_size != EffectiveSize)
            {
                throw new Exception(string.Format("size {0} should be equals to effectiveSize {1} in Init(scale, bias) function.", _size, _effectiveSize));
            }

            Random random = ParameterSetting.Random;
            for (int i = 0; i < EffectiveSize; ++i)
            {
                CpuPtr[i] = (float)(random.NextDouble() * scale + bias);
            }
            EffectiveSize = Size;
            CopyIntoCuda();
        }

        public unsafe void InitGaussian(float mean, float variance)
        {
            if (cpuMemArray == null)
            {
                throw new Exception("Error! Must set needCpuMem=true for Init() operation!");
            }
            if (_size != EffectiveSize)
            {
                throw new Exception(string.Format("size {0} should be equals to effectiveSize {1} in Init(scale, bias) function.", _size, _effectiveSize));
            }

            RandomUtil util = new RandomUtil(ParameterSetting.Random.Next());
            for (int i = 0; i < EffectiveSize; ++i)
            {
                CpuPtr[i] = (float)(util.RandomNormal(mean, variance)); // random.NextDouble() * scale + bias);
            }
            EffectiveSize = Size;
            CopyIntoCuda();
        }

        public void InitNorm(float norm)
        {
            if (cpuMemArray == null)
            {
                throw new Exception("Error! Must set needCpuMem=true for Init() operation!");
            }
            if (_size != EffectiveSize)
            {
                throw new Exception(string.Format("size {0} should be equals to effectiveSize {1} in InitNorm(float value) function.", _size, _effectiveSize));
            }
            Random random = ParameterSetting.Random;
            
            for (int i = 0; i < Size; ++i)
            {
                cpuMemArray[i] = (float)(random.NextDouble() - 0.5);
            }
            float l2Norm = (float)Math.Sqrt(cpuMemArray.Select(i => i * i).Sum());
            if (l2Norm < float.Epsilon) l2Norm = float.Epsilon;
            for (int i = 0; i < Size; ++i)
            {
                cpuMemArray[i] = norm * cpuMemArray[i] / l2Norm;
            }
            EffectiveSize = Size;
            CopyIntoCuda();
        }

        public unsafe void InitRandomOrthogonalMatrix(int n)
        {
            RandomUtil random = new RandomUtil(ParameterSetting.Random.Next());
            float[,] tmp = random.GetRandomOrthogonalMatrix(n);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    CpuPtr[i * n + j] = tmp[i, j];
                }
            }
            if (_size != EffectiveSize)
            {
                throw new Exception(string.Format("size {0} should be equals to effectiveSize {1} in InitRndOrthogonalMatrix(float value) function.", _size, _effectiveSize));
            }
            EffectiveSize = n * n;
            CopyIntoCuda(n * n);
            
        }

        public unsafe void Init(float value)
        {
            if (cpuMemArray == null)
            {
                throw new Exception("Error! Must set needCpuMem=true for Init() operation!");
            }
            if(_size != EffectiveSize)
            {
                throw new Exception(string.Format("size {0} should be equals to effectiveSize {1} in Init(float value) function.", _size, _effectiveSize));
            }
            for (int i = 0; i < EffectiveSize; ++i)
            {
                CpuPtr[i] = value;
            }
            CopyIntoCuda(EffectiveSize);
        }
        public void Init(float[] data)
        {
            if (cpuMemArray == null) {  throw new Exception("Error! Must set needCpuMem=true for Init() operation!"); }
            if (_size != data.Length) { throw new Exception("Error! Init(float[]). Input float array has different size than expected!"); }
            if (_size != EffectiveSize) { throw new Exception(string.Format("size {0} should be equals to effectiveSize {1} in Init(float[]) function.", _size, _effectiveSize)); }

            data.CopyTo(cpuMemArray, _offset);
            CopyIntoCuda();
            EffectiveSize = _size;
        }

        public void Zero()
        {
            if (_size != EffectiveSize) { throw new Exception(string.Format("size {0} should be equals to effectiveSize {1} in Zero() function.", _size, _effectiveSize)); }
            if (cpuMemArray != null) { Array.Clear(cpuMemArray, Offset, _size); }
            if (cudaPiecePointer != IntPtr.Zero) { Cudalib.Zero(cudaPiecePointer, _size); }
        }

    }

    /// <summary>
    /// xinson, 3/
    /// /14. A cuda piece serves as a bridge/connect of the same piece of data between CPU and GPU.
    /// </summary>
    public class CudaPieceInt : IDisposable
    {
        int size;
        /// <summary>
        /// 1. Disable user to set the actual effective size of data
        /// 2. Please try to use EffectiveSize, Size will not be exposed to outside in the future.
        /// </summary>
        public int Size { get { return size; } }

        DeviceType deviceType = DeviceType.GPU;
        public DeviceType DeviceType { get { return deviceType; } private set { deviceType = value; } }
        public void Resize(int newSize)
        {
            if (cpuMemArray != null) Array.Resize(ref cpuMemArray, newSize);
            else cpuMemArray = new int[newSize];

            if (cudaPiecePointer != IntPtr.Zero)
            {
                CopyOutFromCuda(size);
                Cudalib.CudaDeallocInt(cudaPiecePointer);
                cudaPiecePointer = Cudalib.CudaAllocInt(newSize);
                CopyIntoCuda(newSize);
                throw new Exception("Resize GPU Memory is not allowed for CudaPieceInt.");
            }
            size = newSize;
        }
        int bufferIncr = 1024;
        int effectiveSize;
        public int EffectiveSize { get { return effectiveSize; } set { if (value > Size) { Resize(value + bufferIncr); } effectiveSize = value; } }

        int offset = 0;
        public int Offset { get { return offset; } private set { offset = value; } }

        int[] cpuMemArray = null;
        public int[] MemPtr { get { return cpuMemArray; } }

        IntPtr cudaPiecePointer = IntPtr.Zero;
        public IntPtr CudaPtr { get { return cudaPiecePointer; } }
        public unsafe int* CpuPtr { get { if (MemPtr == null) return null; fixed (int* p = &MemPtr[Offset]) return p; } }
        public bool IsEmpty { get { return size == 0; } }
        static CudaPieceInt empty = new CudaPieceInt() { size = 0 };
        public static CudaPieceInt Empty { get { return empty; } }



        /// <summary>
        /// Sync value from CPU.
        /// </summary>
        public void SyncFromCPU()
        {
            SyncFromCPU(effectiveSize);
        }

        public void SyncFromCPU(int size)
        {
            if (cudaPiecePointer == null) return;
            else CopyIntoCuda(size);
        }

        unsafe public void SyncFromCPU(int offset, int[] data, int offsetdata, int length)
        {
            if (length == 0 || data.Length == 0) return;
            if (cudaPiecePointer == IntPtr.Zero)
            {
                if (data == MemPtr) return;
                Buffer.BlockCopy(data, offsetdata * sizeof(int), MemPtr, offset * sizeof(int), length * sizeof(int));
            }
            else
            {
                fixed (int* pb = &data[offsetdata])
                {
                    Cudalib.CudaCopyInFloat(IntPtr.Add(cudaPiecePointer, sizeof(int) * offset), (IntPtr)pb, length);
                }
            }
        }

        /// <summary>
        /// Sync value to CPU
        /// </summary>
        public void SyncToCPU(int size)
        {
            if (cudaPiecePointer == null) return;
            else CopyOutFromCuda(size);
        }

        /// <summary>
        /// Sync value to CPU
        /// </summary>
        public void SyncToCPU()
        {
            if (cudaPiecePointer == null) return;
            else CopyOutFromCuda(effectiveSize);
        }

        public void CopyFrom(CudaPieceInt refVector)
        {
            CopyFrom(0, refVector, 0, refVector.size);
        }

        unsafe public void CopyFrom(int offset, CudaPieceInt refVector, int refOffset, int size)
        {
            /// Copy From GPU to CPU.
            if (CudaPtr == IntPtr.Zero && refVector.CudaPtr != IntPtr.Zero)
            {
                fixed (int* pMemPtr = &MemPtr[offset])
                {
                    Cudalib.CudaCopyOutInt((refVector.CudaPtr + sizeof(int) * refOffset), (IntPtr)pMemPtr, size);
                }
            }

            /// Copy From GPU To GPU.
            if (CudaPtr != IntPtr.Zero && refVector.CudaPtr != IntPtr.Zero)
                Cudalib.CudaCopy(CudaPtr, refVector.CudaPtr, size, offset, refOffset);

            /// Copy From CPU to GPU.
            if (CudaPtr != IntPtr.Zero && refVector.CudaPtr == IntPtr.Zero)
            {
                fixed (int* refMemPtr = &refVector.MemPtr[refOffset])
                {
                    Cudalib.CudaCopyInInt(CudaPtr + sizeof(int) * offset, (IntPtr)refMemPtr, size);
                }
            }

            /// Copy From CPU to CPU.
            if (CudaPtr == IntPtr.Zero && refVector.CudaPtr == IntPtr.Zero)
                Buffer.BlockCopy(refVector.MemPtr, refOffset * sizeof(int), MemPtr, offset * sizeof(int), size * sizeof(int));
        }


        /// <summary>
        /// MemoryPointer.
        /// </summary>
        private CudaPieceInt() { }
        public CudaPieceInt(int length, bool needCpuMem, bool needGpuMem)
        {
            size = length;

            if (needCpuMem) { cpuMemArray = new int[size];  DeviceType = DeviceType.CPU_FAST_VECTOR; }
            if (needGpuMem)
            {
                if ((Int64)(cudaPiecePointer = Cudalib.CudaAllocInt(size > 0 ? size : 1)) == 0)
                {
                    throw new Exception("Out of GPU Memo, use a smaller model!");
                }
                DeviceType = DeviceType.GPU;
            }
            effectiveSize = size;
        }

        public CudaPieceInt(int length, DeviceType device) : this(length, true, device == DeviceType.GPU)
        { }

        public CudaPieceInt(int[] data, bool needCpuMem, bool needGpuMem)
            : this(data.Length, needCpuMem, needGpuMem)
        {
            if (needCpuMem) Array.Copy(data, cpuMemArray, data.Length);
            if (needGpuMem) CopyIntoCuda(data, data.Length);
        }

        private bool disposed = false;
        ~CudaPieceInt()
        {
            this.Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (cudaPiecePointer != IntPtr.Zero)
            {
                Cudalib.CudaDeallocInt(cudaPiecePointer);
                cudaPiecePointer = IntPtr.Zero;
            }

            this.cpuMemArray = null;

            this.size = 0;

            this.disposed = true;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        //unsafe public void CopyIntoCuda()
        //{
        //    CopyIntoCuda(Size);
        //}

        unsafe private void CopyIntoCuda(int SpecifiedSize)
        {
            if (SpecifiedSize == 0) return;
            if (cudaPiecePointer == IntPtr.Zero) { return; }
            if (cpuMemArray == null) { throw new Exception("Error! Must set needCpuMem=true for CopyIntoCuda() operation!"); }
            fixed (int* gpu_ptr = cpuMemArray) { Cudalib.CudaCopyInInt(cudaPiecePointer, (IntPtr)gpu_ptr, SpecifiedSize); }
        }

        unsafe public void CopyIntoCuda(int[] data, int length)
        {
            if (length == 0 || data.Length == 0) return;

            if (cudaPiecePointer == IntPtr.Zero) { return; }

            fixed (int* pb = &data[0])
            {
                Cudalib.CudaCopyInInt(cudaPiecePointer, (IntPtr)pb, Math.Min(length, data.Length));
            }
        }


        /// <summary>
        /// Copy data from GPU to CPU
        /// </summary>
        unsafe public void CopyOutFromCuda()
        {
            CopyOutFromCuda(effectiveSize);
        }

        /// <summary>
        /// Copy data from GPU to CPU
        /// </summary>
        unsafe public void CopyOutFromCuda(int SpecifiedSize)
        {
            if (cudaPiecePointer == IntPtr.Zero)
            {
                return;
            }
            if (cpuMemArray == null)
            {
                throw new Exception("Error! Must set needCpuMem=true for CopyOutFromCuda() operation!");
            }
            fixed (int* gpu_ptr = cpuMemArray)
            {
                Cudalib.CudaCopyOutInt(cudaPiecePointer, (IntPtr)gpu_ptr, SpecifiedSize);
            }
        }

    }


}
