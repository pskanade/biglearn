﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    //public class GRUSeqLabelRunner<IN> : StructRunner<GRULabelStructure, IN> where IN : SeqSparseDataSource
    //{
    //    public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

    //    //BatchData input,  input, 
    //    public List<StructRunner> LinkRunners = new List<StructRunner>();

    //    public SeqDenseBatchData GRUOutput { get { return ((SeqDenseBatchData)LinkRunners.Last().Output); } }

    //    public GRUSeqLabelRunner(GRULabelStructure model, RunnerBehavior behavior)
    //        : base(model, behavior)
    //    {
    //        Type inputType = typeof(SeqSparseBatchData);
    //        foreach (GRUCell linkModel in Model.GRU.GRUCells)
    //        {
    //            StructRunner linkRunner = StructRunner.CreateRunner(linkModel, inputType, behavior);
    //            LinkRunners.Add(linkRunner);
    //            inputType = linkRunner.Output.GetType();
    //        }
    //        for (int i = 0; i < LinkRunners.Count - 1; i++)
    //        {
    //            LinkRunners[i + 1].Input = LinkRunners[i].Output;
    //        }
    //        //Output = new HiddenBatchData();
    //    }

    //    public GRUSeqLabelRunner(GRULabelStructure model, SeqSparseDataSource inputData, RunnerBehavior behavior)
    //        : this(model, behavior)
    //    {
    //        Input = (IN)inputData;
    //    }

    //    public SeqSparseDataSource input;
    //    public override IN Input
    //    {
    //        get
    //        {
    //            return (IN)input;
    //        }
    //        set
    //        {
    //            input = value;
    //            LinkRunners.First().Input = value.SequenceData;
    //        }
    //    }

    //    ~GRUSeqLabelRunner()
    //    {
    //        this.Dispose(false);
    //    }

    //    private bool disposed = false;
    //    protected override void Dispose(bool disposing)
    //    {
    //        if (disposed)
    //        {
    //            return;
    //        }

    //        disposed = true;
    //        if (disposing)
    //        {
    //            foreach (StructRunner runner in LinkRunners) runner.Dispose();
    //        }

    //        base.Dispose(disposing);
    //    }

    //    public override void InitMemory()
    //    {
    //        //if (Output.MAX_BATCHSIZE < Input.BatchSize)
    //        //{
    //        //    Output.Init(Input.Stat.MAX_BATCHSIZE, Model.OutputDim, Behavior.RunMode, Behavior.Device);
    //        //}
    //    }

    //    public override string GetOutput(OutputParameter outputParameter)
    //    {
    //        //copy out to cup
    //        if (Behavior.Device == DeviceType.GPU)
    //        {
    //            Output.Output.Data.CopyOutFromCuda();
    //            Input.SequenceLabel.CopyOutFromCuda();
    //        }

    //        List<string> Lines = new List<string>();
    //        for (int i = 0; i < Input.BatchSize; i++)
    //        {
    //            StringBuilder line = new StringBuilder();
    //            //SequenceLabel size = batchSize * timeStep * ouputDim
    //            line.Append(String.Join(",", Input.SequenceLabel.MemPtr.Skip(i * Model.OutputDim * (Input.SeqSize / Input.BatchSize)).Take(Model.OutputDim)));
    //            line.Append("\t" + String.Join(",", Output.Output.Data.MemPtr.Skip(i * Model.OutputDim).Take(Model.OutputDim)));
    //            Lines.Add(line.ToString());
    //        }
    //        return String.Join("\n", Lines);
    //    }

    //    public override void Forward()
    //    {
    //        MathOperatorManager.MathDevice = Behavior.Device;

    //        foreach (StructRunner runner in LinkRunners) runner.Forward();

    //        InitMemory();
    //        MathOperatorManager.GlobalInstance.RNNLabelOutput(GRUOutput.SampleIdx, Input.BatchSize, GRUOutput.SentOutput, GRUOutput.SentSize,
    //                                Model.Output.weight, Model.StackLen, GRUOutput.Dim, Model.OutputDim, Output.Output.Data);

    //        Output.BatchSize = Input.BatchSize;

    //        ActivationFuncLib.ActivationFunc(Behavior.Computelib, Output.Output.Data, Output.BatchSize, Output.Dim, Model.Output.Af, Model.Output.bias);
    //        //Model.Output.ActiveOutput(Output.Output, Behavior.RunMode);
    //    }

    //    public override void Backward(bool cleanDeriv)
    //    {
    //        Model.Output.DeactiveOutput(Output.Output, Output.Deriv, Behavior.RunMode);
    //        GRUOutput.SentDeriv.Zero();
    //        MathOperatorManager.GlobalInstance.DerivRNNLabelOutput(GRUOutput.SampleIdx, Input.BatchSize, GRUOutput.SentDeriv, GRUOutput.SentSize,
    //                                Model.Output.weight, Model.StackLen, GRUOutput.Dim, Model.OutputDim, Output.Deriv.Data);
    //        LinkRunners.Reverse();
    //        foreach (StructRunner runner in LinkRunners) runner.Backward(cleanDeriv);
    //        LinkRunners.Reverse();

    //    }

    //    public override void Update()
    //    {
    //        Model.Output.WeightOptimizer.BeforeGradient();

    //        MathOperatorManager.GlobalInstance.UpdateRNNLabelOutput(GRUOutput.SampleIdx, Input.BatchSize, GRUOutput.SentOutput, GRUOutput.SentSize,
    //                                Model.Output.WeightOptimizer.Gradient, Model.StackLen, GRUOutput.Dim, Model.OutputDim,
    //                                    Output.Deriv.Data, Model.Output.WeightOptimizer.GradientStep);
    //        Model.Output.WeightOptimizer.AfterGradient();

    //        if (Model.Output.IsBias)
    //        {
    //            Model.Output.BiasOptimizer.BeforeGradient();
    //            Cudalib.Matrix_Aggragate_Weight(Output.Deriv.Data.CudaPtr, Model.Output.BiasOptimizer.Gradient.CudaPtr, Input.BatchSize,
    //                Model.Output.Neural_Out, Model.Output.BiasOptimizer.GradientStep);
    //            Model.Output.BiasOptimizer.AfterGradient();
    //        }
    //        foreach (StructRunner runner in LinkRunners) runner.Update();
    //    }

    //    public Tuple<float[], float[]> RNNOutput(IN sample)
    //    {
    //        Input = sample;
    //        Forward();
    //        float[] finalLabels = new float[Input.BatchSize * Model.OutputDim];
    //        float[] finalEmbeds = new float[Input.BatchSize * Model.GRU.GRUCells.Last().HiddenStateDim];

    //        if (Behavior.Device == DeviceType.GPU)
    //        {
    //            Input.SampleIdx.CopyOutFromCuda();
    //            ((SeqDenseBatchData)LinkRunners.Last().Input).SentOutput.CopyOutFromCuda();
    //            ((SeqDenseBatchData)LinkRunners.Last().Output).SentOutput.CopyOutFromCuda();
    //        }

    //        Parallel.For(0, Input.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = 8 }, i =>
    //        {
    //            int seqEnd = Input.SampleIdx.MemPtr[i] - 1;
    //            int seqStart = i == 0 ? 0 : Input.SampleIdx.MemPtr[i - 1];

    //            if (seqEnd >= seqStart)
    //            {
    //                for (int d = 0; d < Output.Dim; d++)
    //                {
    //                    finalLabels[i * Model.OutputDim + d] =
    //                        Output.Output.Data.MemPtr[i * (Input.SeqSize / Input.BatchSize) * Model.OutputDim + d];
    //                }

    //                for (int d = 0; d < GRUOutput.Dim; d++)
    //                {
    //                    finalEmbeds[i * Model.GRU.GRUCells.Last().HiddenStateDim + d] =
    //                        GRUOutput.SentOutput.MemPtr[i * (Input.SeqSize / Input.BatchSize) * Model.GRU.GRUCells.Last().HiddenStateDim + d];
    //                }
    //            }
    //        });

    //        return new Tuple<float[], float[]>(finalLabels, finalEmbeds);
    //    }

    //}
}
