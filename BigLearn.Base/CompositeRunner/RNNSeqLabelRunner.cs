﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Take Input:
    ///     Instance 1 : (x1, x2, x3,...xm) y1 
    ///     Instance 2 : (x1, x2, x3,...xn) y2 
    /// Predict : (x1, x2, x3,...xk) -> y
    /// </summary>
    /// <typeparam name="IN"></typeparam>
    //public class RNNSeqLabelRunner<IN> : StructRunner<RNNLabelStructure, IN> where IN : SeqBatchInputLabelData
    //{
    //    public HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

    //    //BatchData input,  input, 
    //    public List<StructRunner> LinkRunners = new List<StructRunner>();

    //    public SeqDenseBatchData RNNOutput { get { return ((SeqDenseBatchData)LinkRunners.Last().Output); } }

    //    public RNNSeqLabelRunner(RNNLabelStructure model, RunnerBehavior behavior)
    //        : base(model, behavior)
    //    {
    //        Type inputType = typeof(SeqSparseBatchData);
    //        foreach (RNNCell linkModel in Model.RNN.RNNCells)
    //        {
    //            StructRunner linkRunner = StructRunner.CreateRunner(linkModel, inputType, behavior);
    //            LinkRunners.Add(linkRunner);
    //            inputType = linkRunner.Output.GetType();
    //        }
    //        for (int i = 0; i < LinkRunners.Count - 1; i++)
    //        {
    //            LinkRunners[i + 1].Input = LinkRunners[i].Output;
    //        }
    //        Output = new HiddenBatchData();
    //    }

    //    public SeqBatchInputLabelData input;
    //    public override IN Input
    //    {
    //        get
    //        {
    //            return (IN)input;
    //        }
    //        set
    //        {
    //            LinkRunners.First().Input = value; // new SeqSparseBatchData(value);
    //            input = value;
    //        }
    //    }

    //    ~RNNSeqLabelRunner()
    //    {
    //        this.Dispose(false);
    //    }

    //    private bool disposed = false;
    //    protected override void Dispose(bool disposing)
    //    {
    //        if (disposed)
    //        {
    //            return;
    //        }

    //        disposed = true;
    //        if (disposing)
    //        {
    //            foreach (StructRunner runner in LinkRunners) runner.Dispose();
    //        }

    //        base.Dispose(disposing);
    //    }

    //    public override void InitMemory()
    //    {
    //        if (Output == null || Output.Stat.MAX_BATCHSIZE < Input.Stat.MAX_BATCHSIZE)
    //        {
    //            Output = new HiddenBatchData(Input.Stat.MAX_BATCHSIZE, Model.OutputDim, Behavior.RunMode);
    //        }
    //    }

    //    public override void Forward()
    //    {
    //        MathOperatorManager.MathDevice = Behavior.Device;
    //        InitMemory();

    //        foreach (StructRunner runner in LinkRunners) runner.Forward();

    //        Output.BatchSize = Input.BatchSize;

    //        MathOperatorManager.GlobalInstance.RNNLabelOutput(RNNOutput.SampleIdx, Input.BatchSize, RNNOutput.SentOutput, RNNOutput.SentSize,
    //                                Model.Output.weight, Model.StackLen, RNNOutput.Dim, Model.OutputDim, Output.Output.Data);


    //        Model.Output.ActiveOutput(Output.Output, Behavior.RunMode);
    //    }

    //    public override void Backward(bool cleanDeriv)
    //    {
    //        Model.Output.DeactiveOutput(Output.Output, Output.Deriv, Behavior.RunMode);
    //        RNNOutput.SentDeriv.Zero();

    //        MathOperatorManager.GlobalInstance.DerivRNNLabelOutput(RNNOutput.SampleIdx, Input.BatchSize, RNNOutput.SentDeriv, RNNOutput.SentSize,
    //                                Model.Output.weight, Model.StackLen, RNNOutput.Dim, Model.OutputDim, Output.Deriv.Data);
    //        LinkRunners.Reverse();
    //        foreach (StructRunner runner in LinkRunners) runner.Backward(cleanDeriv);
    //        LinkRunners.Reverse();

    //    }

    //    public override void Update()
    //    {
    //        Model.Output.WeightOptimizer.BeforeGradient();
    //        MathOperatorManager.GlobalInstance.UpdateRNNLabelOutput(RNNOutput.SampleIdx, Input.BatchSize, RNNOutput.SentOutput, RNNOutput.SentSize,
    //                                Model.Output.WeightOptimizer.Gradient, Model.StackLen, RNNOutput.Dim, Model.OutputDim,
    //                                    Output.Deriv.Data, Model.Output.WeightOptimizer.GradientStep);

    //        Model.Output.WeightOptimizer.AfterGradient();


    //        if (Model.Output.IsBias)
    //        {
    //            Model.Output.BiasOptimizer.BeforeGradient();
    //            Cudalib.Matrix_Aggragate_Weight(Output.Deriv.Data.CudaPtr, Model.Output.BiasOptimizer.Gradient.CudaPtr, Input.BatchSize,
    //                Model.Output.Neural_Out, Model.Output.BiasOptimizer.GradientStep);
    //            Model.Output.BiasOptimizer.AfterGradient();
    //        }
    //        foreach (StructRunner runner in LinkRunners) runner.Update();
    //    }


    //    public float[] EmbeddingForward()
    //    {
    //        MathOperatorManager.MathDevice = Behavior.Device;

    //        foreach (StructRunner runner in LinkRunners) runner.Forward();
            
    //        float[] embedOutput = new float[Input.BatchSize * RNNOutput.Dim * (Model.StackLen + 1) ];

    //        Input.SampleIdx.CopyOutFromCuda();
    //        RNNOutput.SentOutput.CopyOutFromCuda();
            
    //        //Parallel.for(int i=0;i<Input.BatchSize;i++)
    //        //{
    //        Parallel.For(0, Input.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = 8 }, i =>
    //        {
    //            int seqEnd = Input.SampleIdx.MemPtr[i] - 1;
    //            int seqStart = i == 0 ? 0 : Input.SampleIdx.MemPtr[i - 1];
    //            if (seqStart < (seqEnd - Model.StackLen)) seqStart = (seqEnd - Model.StackLen);

    //            for (int l = seqEnd; l >= seqStart; l--)
    //            {
    //                for (int d = 0; d < RNNOutput.Dim; d++)
    //                {
    //                    embedOutput[i * RNNOutput.Dim * (Model.StackLen + 1) + (seqEnd - l) * RNNOutput.Dim + d] = RNNOutput.SentOutput.MemPtr[l * RNNOutput.Dim + d];
    //                }
    //            }
    //        });
    //        return embedOutput;
    //    }

    //    public void ExtractEmbedding(IEnumerable<IN> data, EvaluationParameter EvalParameter)
    //    {
    //        VECTOROUTPUTEvaluationSet evaluator = new VECTOROUTPUTEvaluationSet();
    //        evaluator.Init((BasicEvalParameter)EvalParameter.Param);

    //        DateTime pre = DateTime.Now;
    //        double PutTime = 0;
    //        double ForwardTime = 0;
    //        double EvaluationTime = 0;
    //        int id = 0;
    //        foreach (IN sample in data)
    //        {
    //            DateTime t1 = DateTime.Now;
    //            PutTime += DateTime.Now.Subtract(t1).TotalMilliseconds;
    //            Input = sample;

    //            DateTime t2 = DateTime.Now;
    //            float[] embeds = EmbeddingForward();

    //            ForwardTime += DateTime.Now.Subtract(t2).TotalMilliseconds;

    //            DateTime t3 = DateTime.Now;
    //            //PushScore(float[] score, int dim, int batchSize)
    //            evaluator.PushScore(embeds, RNNOutput.Dim * (Model.StackLen + 1), Input.BatchSize);
    //            EvaluationTime += DateTime.Now.Subtract(t3).TotalMilliseconds;

    //            if (id % 500 == 0) Console.WriteLine("MiniBatch {0}", id);
    //            id += 1;
    //        }
    //        Console.WriteLine("Predict Done!");
    //        List<string> evalInfo = new List<string>();
    //        float score = evaluator.Evaluation(out evalInfo);
    //        evaluator.Dispose();
    //        Logger.WriteLog("Evaluation : \n{0} \n End-Score : {1}, Time : {2}", string.Join("\n", evalInfo), score, DateTime.Now.Subtract(pre).TotalSeconds);

    //        Logger.WriteLog("Date IO Time {0}; Forward Time {1}; Evaluation Time {2}", PutTime, ForwardTime, EvaluationTime);
    //    }
    //}
}
