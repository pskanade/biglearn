﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    
    /// <summary>
    /// Take 
    /// </summary>
    /// <typeparam name="IN"></typeparam>
    public class RNNSeqSlotRunner<IN> : StructRunner<RNNStructure, IN> where IN : SeqSparseSlotDataSource
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        //BatchData input,  input, 
        public List<StructRunner> LinkRunners = new List<StructRunner>();

        public RNNSeqSlotRunner(RNNStructure model, RunnerBehavior behavior)
            : base(model, behavior)
        {
            if (Model.RNNCells.Count == 1)
            {
                StructRunner linkRunner = StructRunner.CreateRunner(Model.RNNCells[0], typeof(SeqSparseSlotBatchData), behavior);
                LinkRunners.Add(linkRunner);

            }
            else
            {
                Type inputType = typeof(SeqSparseBatchData);
                int layerID = 0;
                foreach (RNNCell linkModel in Model.RNNCells)
                {
                    StructRunner linkRunner = null;
                    if (layerID == Model.RNNCells.Count - 1)
                    {
                        linkRunner = StructRunner.CreateRunner(linkModel, typeof(SeqDenseSlotBatchData), behavior);
                    }
                    else
                    {
                        linkRunner = StructRunner.CreateRunner(linkModel, inputType, behavior);
                    }
                    LinkRunners.Add(linkRunner);
                    inputType = linkRunner.Output.GetType();
                    layerID += 1;
                }

                for (int i = 0; i < LinkRunners.Count - 1; i++)
                {
                    if (i == LinkRunners.Count - 2)
                    {
                        LinkRunners[i + 1].Input = new SeqDenseSlotBatchData((SeqDenseBatchData)LinkRunners[i].Output);
                    }
                    else
                    {
                        LinkRunners[i + 1].Input = LinkRunners[i].Output;
                    }
                }
            }
            Output = (SeqDenseBatchData)LinkRunners.Last().Output;
        }

        //public CudaPieceFloat GroundTruth;
        public override IN Input
        {
            get
            {
                //if( LinkRunners.Last().Input.GetType
                //return (IN)new SeqSparseSlotDataSource((SeqSparseBatchData)LinkRunners.First().Input, LinkRunners.Last().Input );
                if (Model.RNNCells.Count == 1)
                {
                    return (IN)LinkRunners.First().Input;
                }
                else
                {
                    return (IN)new SeqSparseSlotDataSource((SeqSparseBatchData)LinkRunners.First().Input, 
                                                    ((SeqDenseSlotBatchData)LinkRunners.Last().Input).SentGroundTruth,
                                                    ((SeqDenseSlotBatchData)LinkRunners.Last().Input).SampleEarlyStop);
                }
            }
            set
            {
                if (Model.RNNCells.Count == 1)
                {
                    LinkRunners.First().Input = new SeqSparseSlotBatchData(value.SequenceData, value.SequenceLabel, value.SampleEarlyStop);
                }
                else
                {
                    LinkRunners.First().Input = value.SequenceData;
                    ((SeqDenseSlotBatchData)LinkRunners.Last().Input).SentGroundTruth = value.SequenceLabel;
                    ((SeqDenseSlotBatchData)LinkRunners.Last().Input).SampleEarlyStop = value.SampleEarlyStop;
                }
            }
        }

        ~RNNSeqSlotRunner()
        {
            this.Dispose(false);
        }

        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            disposed = true;
            if (disposing)
            {
                foreach (StructRunner runner in LinkRunners) runner.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Train 
        /// </summary>
        /// <returns></returns>
        public double Validation(IEnumerable<IN> data)
        {
            double TrainLoss = 0;
            double PredictLoss = 0;

            int batchIdx = 0;

            DateTime pre = DateTime.Now;
            foreach (IN sample in data)
            {
                Input = sample;
                Forward();

                if (mark == null) mark = new CudaPieceInt(Input.Stat.MAX_SEQUENCESIZE, true, false);
                GenerateMarkSample(Input.SampleIdx, Input.SampleEarlyStop, Input.BatchSize);

                Output.SentOutput.CopyOutFromCuda();
                Input.SequenceLabel.CopyOutFromCuda();
                
                float trainLoss = LossFunction.RegressionSlotLoss(Output.SentOutput.MemPtr, Input.SequenceLabel.MemPtr, mark.MemPtr, 
                                Output.SentDeriv.MemPtr, Input.SeqSize, true, true);
                TrainLoss += trainLoss;

                Output.SentDeriv.CopyIntoCuda(Input.SeqSize);


                float validLoss = LossFunction.RegressionSlotLoss(Output.SentOutput.MemPtr, Input.SequenceLabel.MemPtr, mark.MemPtr,
                                null, Input.SeqSize, false, false);
                PredictLoss += validLoss;

                //loss += LossFunction.RegressionSlotLoss(
                Backward(true);
                Update();
                if (++batchIdx % 1000 == 0)
                {
                    Logger.WriteLog("Train Batch Num {0}, Train AvgLoss {1}, Valid AvgLoss{2}", batchIdx, TrainLoss / batchIdx, PredictLoss / batchIdx);
                }
            }

            Logger.WriteLog("Train Loss:{0}, Valid Loss {1}, Time:{2}", TrainLoss / batchIdx,  PredictLoss / batchIdx, DateTime.Now.Subtract(pre).TotalSeconds);
            return TrainLoss;
        }


        CudaPieceInt mark;
        void GenerateMarkSample(CudaPieceInt sampleIdx, CudaPieceInt sampleEnd, int batchSize)
        {
            sampleIdx.CopyOutFromCuda();
            sampleEnd.CopyOutFromCuda();
            for (int b = 0; b < batchSize; b++)
            {
                int start = b == 0 ? 0 : sampleIdx.MemPtr[b - 1];
                int end = sampleIdx.MemPtr[b];
                int stop = sampleEnd.MemPtr[b];

                for (int s = start; s < stop; s++)
                {
                    mark.MemPtr[s] = 1;
                }
                for (int s = stop; s < end; s++)
                {
                    mark.MemPtr[s] = 0;
                }
            }
        }

        public override void Forward()
        {
            MathOperatorManager.MathDevice = Behavior.Device;
            foreach (StructRunner runner in LinkRunners) runner.Forward();
        }

        public override void Backward(bool cleanDeriv)
        {
            LinkRunners.Reverse();
            foreach (StructRunner runner in LinkRunners) runner.Backward(cleanDeriv);
            LinkRunners.Reverse();
        }

        public override void Update()
        {
            foreach (StructRunner runner in LinkRunners) runner.Update();
        }

    }
}
