﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    //public class MatchGRURunner : StructRunner
    //{
    //    public new SeqDenseRecursiveData Output { get { return (SeqDenseRecursiveData)base.Output; } set { base.Output = value; } }

    //    List<StructRunner> SubRunners = new List<StructRunner>();
    //    List<StructRunner> RuntimeFlow = new List<StructRunner>();

    //    GRUCell GRU { get; set; }
    //    LayerStructure AttInput { get; set; }
    //    LayerStructure AttState { get; set; }
    //    LayerStructure AttMem { get; set; }

    //    SeqDenseRecursiveData Data { get; set; }
    //    SeqDenseRecursiveData DataAtt { get; set; }

    //    SeqDenseBatchData Memory { get; set; }
    //    SeqDenseBatchData MemoryAtt { get; set; }

    //    SeqDenseRecursiveData OutputAtt { get; set; }

    //    //public HiddenBatchData LastStatus { get; set; }
    //    public MatchGRURunner(GRUCell gru, SeqDenseRecursiveData input, SeqDenseBatchData memory,
    //        LayerStructure attInput, LayerStructure attState, LayerStructure attMem, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //    {
    //        Data = input;
    //        Memory = memory;

    //        GRU = gru;
    //        AttInput = attInput;
    //        AttState = attState;
    //        AttMem = attMem;

    //        MatrixMultiplicationRunner memoryAtt = new MatrixMultiplicationRunner(new MatrixData(memory),
    //            new MatrixData(attMem.Neural_Out, attMem.Neural_In, attMem.weight, attMem.WeightGrad, attMem.DeviceType), behavior);
    //        SubRunners.Add(memoryAtt);
    //        MemoryAtt = new SeqDenseBatchData(new SequenceDataStat(memory.MAX_BATCHSIZE, memory.MAX_SENTSIZE, attMem.Neural_Out),
    //            memory.SampleIdx, memory.SentMargin, memoryAtt.Output.Output, memoryAtt.Output.Deriv, Behavior.Device);


    //        MatrixMultiplicationRunner inputAtt = new MatrixMultiplicationRunner(new MatrixData(Data),
    //            new MatrixData(attInput.Neural_Out, attInput.Neural_In, attInput.weight, attInput.WeightGrad, attInput.DeviceType), behavior);
    //        SubRunners.Add(inputAtt);
    //        DataAtt = new SeqDenseRecursiveData(new SequenceDataStat(Data.MAX_BATCHSIZE, Data.MAX_SENTSIZE, attInput.Neural_Out),
    //            Data.SampleIdx, Data.SentMargin, inputAtt.Output.Output, inputAtt.Output.Deriv, Data.RecurrentInfo, Behavior.Device);

    //        //OutputAtt = new SeqDenseRecursiveData(new SequenceDataStat(Data.MAX_BATCHSIZE, Data.MAX_SENTSIZE, attState.Neural_Out),
    //        //    Data.SampleIdx, Data.SentMargin, Data.RecurrentInfo, Behavior.Device);

    //        SequenceDataStat stat = new SequenceDataStat(Data.Stat.MAX_BATCHSIZE, Data.Stat.MAX_SEQUENCESIZE, GRU.HiddenStateDim);
    //        Output = new SeqDenseRecursiveData(stat, Data.SampleIdx, Data.SentMargin, Data.RecurrentInfo, Behavior.Device);
    //    }

    //    public override void Forward()
    //    {
    //        iteration++;
    //        MatrixData AttS = new MatrixData(AttState.Neural_Out, GRU.HiddenStateDim, AttState.weight, AttState.WeightGrad, Behavior.Device);

    //        foreach (StructRunner runner in SubRunners) { runner.Forward(); }

    //        int preStartIdx = -1;
    //        for (int i = 0; i < Data.RecurrentInfo.MaxLag; i++)
    //        {
    //            int startIdx = i == 0 ? 0 : Data.RecurrentInfo.LagSeqIdx[i - 1];
    //            int Sum = Data.RecurrentInfo.LagSeqIdx[i] - startIdx;
    //            if (Sum == 0) break;

    //            int batchSize = Sum;

    //            MatrixData initO = null;
    //            MatrixData initOAtt = null;
    //            if (i > 0)
    //            {
    //                initO = new MatrixData(GRU.HiddenStateDim, batchSize,
    //                    Output.SentOutput.SubArray(preStartIdx * GRU.HiddenStateDim, batchSize * GRU.HiddenStateDim),
    //                    Output.SentDeriv.SubArray(preStartIdx * GRU.HiddenStateDim, batchSize * GRU.HiddenStateDim),
    //                    Behavior.Device);

    //                initOAtt = new MatrixData(AttState.Neural_Out, batchSize,
    //                    OutputAtt.SentOutput.SubArray(preStartIdx * AttState.Neural_Out, batchSize * AttState.Neural_Out),
    //                    OutputAtt.SentDeriv.SubArray(preStartIdx * AttState.Neural_Out, batchSize * AttState.Neural_Out),
    //                    Behavior.Device);
    //            }

    //            MatrixData x = new MatrixData(Data.Dim, batchSize,
    //                    Data.SentOutput.SubArray(startIdx * GRU.HiddenStateDim, batchSize * GRU.HiddenStateDim),
    //                    Data.SentOutput.SubArray(startIdx * GRU.HiddenStateDim, batchSize * GRU.HiddenStateDim),
    //                    Behavior.Device);

    //            MatrixData xAtt = new MatrixData(AttInput.Neural_Out, batchSize,
    //                    DataAtt.SentOutput.SubArray(startIdx * AttInput.Neural_Out, batchSize * AttInput.Neural_Out),
    //                    DataAtt.SentOutput.SubArray(startIdx * AttInput.Neural_Out, batchSize * AttInput.Neural_Out),
    //                    Behavior.Device);

    //            MatrixData att = xAtt;
    //            if (initO != null)
    //            {
    //                RuntimeFlow.Add(new MatrixMultiplicationRunner(initO, AttS, initOAtt, true, Behavior));
    //                RuntimeFlow.Add(new AdditionRunner(initOAtt, null, att, false, Behavior));
    //            }


    //            AdditionMatrixRunner additional = new AdditionMatrixRunner()
    //            ComputeLib.Matrix_AdditionMask(Src.SentOutput, 0, MatchData.SrcIdx, 0,
    //                                       Src.SentOutput, 0, MatchData.SrcIdx, 0,
    //                                       HiddenStatus.Output.Data, 0, EnsembleMask, 0,
    //                                       Src.Dim, MatchData.MatchSize, 1, 0, 0);

    //            ComputeLib.Matrix_AdditionMask(Mem.SentOutput, 0, MatchData.TgtIdx, 0,
    //                                           Mem.SentOutput, 0, MatchData.TgtIdx, 0,
    //                                           HiddenStatus.Output.Data, 0, EnsembleMask, MatchData.Stat.MAX_MATCH_BATCHSIZE,
    //                                           Mem.Dim, MatchData.MatchSize, 1, 0, 0);



    //            if (i > 0)
    //            {
    //                GRURecurrentForward(Computelib, Sum, Output, preStartIdx * Model.HiddenStateDim, Model, Output, startIdx * Model.HiddenStateDim, attentLib, Z, startIdx * Model.AttentionDim,
    //                    RecurrentInfo.LagSeqElement, startIdx, GateZ, GateR, RestH, HHat, i);
    //            }
    //            else
    //            {
    //                GRURecurrentForward(Computelib, Sum, CudaPieceFloat.Empty, 0, Model, Output, 0, attentLib, Z, 0,
    //                    RecurrentInfo.LagSeqElement, 0, GateZ, GateR, RestH, HHat, i);
    //            }
    //            preStartIdx = startIdx;
    //        }

    //        GateZ_N.Init(1);
    //        Computelib.Matrix_AdditionMask(GateZ_N, 0, CudaPieceInt.Empty, 0,
    //                                       GateZ, 0, CudaPieceInt.Empty, 0,
    //                                       GateZ_N, 0, CudaPieceInt.Empty, 0,
    //                                       Model.HiddenStateDim, RecurrentInfo.SentSize, 1, -1, 0);
    //    }

    //    public override void Backward(bool cleanDeriv)
    //    {
    //        foreach (StructRunner runner in Enumerable.Reverse(SubRunners)) { runner.Backward(false); }
    //    }

    //    public override void CleanDeriv()
    //    {
    //        foreach (StructRunner runner in SubRunners) { runner.CleanDeriv(); }
    //    }

    //    public override void Update()
    //    {
    //        foreach (StructRunner runner in SubRunners) { runner.IsDelegateOptimizer = IsDelegateOptimizer; runner.Update(); }
    //    }
    //}
}
