﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class BiLSTMRunner : StructRunner
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        List<StructRunner> SubRunners = new List<StructRunner>();

        LSTMStructure FWLSTM { get; set; }
        LSTMStructure BWLSTM { get; set; }
        SeqDenseBatchData DATA { get; set; }

        public HiddenBatchData LastStatus { get; set; }
        public BiLSTMRunner(LSTMStructure fwLSTM, LSTMStructure bwLSTM, SeqDenseBatchData input, RunnerBehavior behavior, bool lastState = false)
            : base(Structure.Empty, behavior)
        {
            DATA = input;
            FWLSTM = fwLSTM;
            BWLSTM = bwLSTM;

            if (input.Dim != fwLSTM.LSTMCells[0].InputFeatureDim)
            { throw new Exception(string.Format("fw LSTM input dim {0}, input dim {1} not matched", fwLSTM.LSTMCells[0].InputFeatureDim, input.Dim)); }

            if (input.Dim != bwLSTM.LSTMCells[0].InputFeatureDim)
            { throw new Exception(string.Format("bw LSTM input dim {0}, input dim {1} not matched", bwLSTM.LSTMCells[0].InputFeatureDim, input.Dim)); }


            Cook_Seq2TransposeSeqRunner fwInputRunner = new Cook_Seq2TransposeSeqRunner(input, false, Behavior);
            SubRunners.Add(fwInputRunner);
            SeqDenseRecursiveData fwInputData = fwInputRunner.Output;
            for (int i = 0; i < fwLSTM.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> fwRunner = 
                    new FastLSTMDenseRunner<SeqDenseRecursiveData>(fwLSTM.LSTMCells[i], fwInputData, Behavior);
                SubRunners.Add(fwRunner);
                fwInputData = fwRunner.Output;
            }
            Cook_TransposeSeq2SeqRunner fwOutputRunner = new Cook_TransposeSeq2SeqRunner(fwInputData, Behavior);
            SubRunners.Add(fwOutputRunner);
            SeqDenseBatchData fwOutputData = fwOutputRunner.Output;

            Cook_Seq2TransposeSeqRunner bwInputRunner = new Cook_Seq2TransposeSeqRunner(input, true, Behavior);
            SubRunners.Add(bwInputRunner);
            SeqDenseRecursiveData bwInputData = bwInputRunner.Output;
            for (int i = 0; i < bwLSTM.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> bwRunner =
                    new FastLSTMDenseRunner<SeqDenseRecursiveData>(bwLSTM.LSTMCells[i], bwInputData, Behavior);
                SubRunners.Add(bwRunner);
                bwInputData = bwRunner.Output;
            }
            Cook_TransposeSeq2SeqRunner bwOutputRunner = new Cook_TransposeSeq2SeqRunner(bwInputData, Behavior);
            SubRunners.Add(bwOutputRunner);
            SeqDenseBatchData bwOutputData = bwOutputRunner.Output;

            EnsembleConcateSeqRunner concateRunner = new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { fwOutputData, bwOutputData }, Behavior);
            SubRunners.Add(concateRunner);

            if(lastState)
            {
                SeqOrderMatrixRunner fwlastRunner = new SeqOrderMatrixRunner(fwInputData, true, 0, fwInputData.MapForward, Behavior);
                SubRunners.Add(fwlastRunner);

                SeqOrderMatrixRunner bwfirstRunner = new SeqOrderMatrixRunner(bwInputData, false, 0, bwInputData.MapForward, Behavior);
                SubRunners.Add(bwfirstRunner);

                EnsembleMatrixRunner stateConcateRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { fwlastRunner.Output, bwfirstRunner.Output }, Behavior);
                SubRunners.Add(stateConcateRunner);
                LastStatus = stateConcateRunner.Output;
            }

            Output = concateRunner.Output;
        }

        public override void Forward()
        {
            iteration++;
            //if(name.Equals("MemorySeqO") && iteration == 31)
            //{
            //    DATA.SyncToCPU();
            //    FWLSTM.SyncToCPU();
            //    BWLSTM.SyncToCPU();
            //}

            foreach (StructRunner runner in SubRunners) { runner.Forward(); }

            //if (name.Equals("MemorySeqO") && iteration == 31)
            //{
            //    Output.SyncToCPU();
            //}
        }

        public override void Backward(bool cleanDeriv)
        {
            foreach (StructRunner runner in Enumerable.Reverse(SubRunners)) { runner.Backward(false); }
        }

        public override void CleanDeriv()
        {
            foreach (StructRunner runner in SubRunners) { runner.CleanDeriv(); }
        }

        public override void Update()
        {
            foreach (StructRunner runner in SubRunners) { runner.IsDelegateOptimizer = IsDelegateOptimizer; runner.Update(); }
        }
    }

    public class LSTMRunner : StructRunner
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        List<StructRunner> SubRunners = new List<StructRunner>();

        LSTMStructure FWLSTM { get; set; }
        SeqDenseBatchData DATA { get; set; }

        public HiddenBatchData LastStatus { get; set; }
        public LSTMRunner(LSTMStructure fwLSTM, SeqDenseBatchData input, RunnerBehavior behavior, bool lastState = false)
            : base(Structure.Empty, behavior)
        {
            DATA = input;
            FWLSTM = fwLSTM;

            if (input.Dim != fwLSTM.LSTMCells[0].InputFeatureDim)
            { throw new Exception(string.Format("fw LSTM input dim {0}, input dim {1} not matched", fwLSTM.LSTMCells[0].InputFeatureDim, input.Dim)); }

            Cook_Seq2TransposeSeqRunner fwInputRunner = new Cook_Seq2TransposeSeqRunner(input, false, Behavior);
            SubRunners.Add(fwInputRunner);
            SeqDenseRecursiveData fwInputData = fwInputRunner.Output;
            for (int i = 0; i < fwLSTM.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> fwRunner =
                    new FastLSTMDenseRunner<SeqDenseRecursiveData>(fwLSTM.LSTMCells[i], fwInputData, Behavior);
                SubRunners.Add(fwRunner);
                fwInputData = fwRunner.Output;
            }
            Cook_TransposeSeq2SeqRunner fwOutputRunner = new Cook_TransposeSeq2SeqRunner(fwInputData, Behavior);
            SubRunners.Add(fwOutputRunner);
            SeqDenseBatchData fwOutputData = fwOutputRunner.Output;

            if (lastState)
            {
                SeqOrderMatrixRunner fwlastRunner = new SeqOrderMatrixRunner(fwInputData, true, 0, fwInputData.MapForward, Behavior);
                SubRunners.Add(fwlastRunner);
                LastStatus = fwlastRunner.Output;
            }

            Output = fwOutputData;
        }

        public override void Forward()
        {
            iteration++;
            foreach (StructRunner runner in SubRunners) { runner.Forward(); }
        }

        public override void Backward(bool cleanDeriv)
        {
            foreach (StructRunner runner in Enumerable.Reverse(SubRunners)) { runner.Backward(false); }
        }

        public override void CleanDeriv()
        {
            foreach (StructRunner runner in SubRunners) { runner.CleanDeriv(); }
        }

        public override void Update()
        {
            foreach (StructRunner runner in SubRunners) { runner.IsDelegateOptimizer = IsDelegateOptimizer; runner.Update(); }
        }
    }

}
