﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class CompositeNetRunner : StructRunner
    {
        public List<StructRunner> LinkRunners = new List<StructRunner>();

        public CompositeNetRunner(RunnerBehavior behavior) : base(Structure.Empty, behavior)
        { }

        ~CompositeNetRunner()
        {
            this.Dispose(false);
        }

        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            disposed = true;

            if (disposing)
            {
                foreach (StructRunner runner in LinkRunners) runner.Dispose();
            }

            base.Dispose(disposing);
        }

        public override void CleanDeriv()
        {
            foreach (StructRunner runner in LinkRunners) runner.CleanDeriv();
        }

        public override void Forward()
        {
            IsTerminate = false;
            IsContinue = true;
            int idx = 0;
            foreach (StructRunner runner in LinkRunners)
            {
                runner.Forward();
                IsTerminate = runner.IsTerminate;
                IsContinue = runner.IsContinue;
                if (IsTerminate || !IsContinue) break;

                idx += 1;
            }

        }

        public override void Backward(bool cleanDeriv)
        {
            LinkRunners.Reverse();
            foreach (StructRunner runner in LinkRunners) { if (runner.IsBackProp) runner.Backward(cleanDeriv); }
            LinkRunners.Reverse();
        }

        public override void Update()
        {
            foreach (StructRunner runner in LinkRunners) { runner.IsDelegateOptimizer = IsDelegateOptimizer; if (runner.IsBackProp && runner.IsUpdate) { runner.Update(); } }
        }

        public override void Init()
        {
            foreach (StructRunner runner in LinkRunners) { runner.Init(); }
        }

        public override void Complete()
        {
            foreach (StructRunner runner in LinkRunners) { runner.Complete(); }
        }
    }
}
