﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Take Input:
    ///     Instance 1 : (x1, y1), ... (xn, yn)
    ///     Instance 2 : (x1, y1), ... (xm, ym)
    /// Predict : (x1, x2, x3,...xk) -> (y1, y2, ... yk)
    /// </summary>
    /// <typeparam name="IN"></typeparam>
    public class RNNSeqPredRunner<IN> : StructRunner<RNNStructure, IN> where IN : SeqSparseDataSource
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        // BatchData input,  input, 
        public List<StructRunner> LinkRunners = new List<StructRunner>();
        public RNNSeqPredRunner(RNNStructure model, RunnerBehavior behavior)
            : base(model, behavior)
        {
            // Sparse Input.
            Type inputType = typeof(SeqSparseBatchData);
            foreach (RNNCell linkModel in Model.RNNCells)
            {
                StructRunner linkRunner = StructRunner.CreateRunner(linkModel, inputType, behavior);
                LinkRunners.Add(linkRunner);
                inputType = linkRunner.Output.GetType();
            }

            for (int i = 0; i < LinkRunners.Count - 1; i++)
            {
                LinkRunners[i + 1].Input = LinkRunners[i].Output;
            }

            Output = (SeqDenseBatchData)LinkRunners.Last().Output;
        }

        public CudaPieceFloat GroundTruth;

        private SeqSparseDataSource input;

        public override IN Input
        {
            get
            {
                return (IN)input;
            }
            set
            {
                input = value;
                LinkRunners.First().Input = value.SequenceData;
                GroundTruth = value.SequenceLabel;
            }
        }

        ~RNNSeqPredRunner()
        {
            this.Dispose(false);
        }

        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            disposed = true;
            if (disposing)
            {
                foreach (StructRunner runner in LinkRunners) runner.Dispose();
            }

            base.Dispose(disposing);
        }

        public override void Forward()
        {
            MathOperatorManager.MathDevice = Behavior.Device;
            foreach (StructRunner runner in LinkRunners) runner.Forward();
        }

        public override void Backward(bool cleanDeriv)
        {
            LinkRunners.Reverse();
            foreach (StructRunner runner in LinkRunners) runner.Backward(cleanDeriv);
            LinkRunners.Reverse();
        }

        public override void Update()
        {
            foreach (StructRunner runner in LinkRunners)
                runner.Update();
        }


        /*********Forward Predict**********/
        /// <summary>
        /// Input Sample; 
        /// Output Label and embedding of the last time step of the sample.
        /// </summary>
        /// <param name="sample"></param>
        /// <returns> Tuple Item1 : label; Item2 : embedding; </returns>
        //public Tuple<float[], float[]> RNNOutput(IN sample)
        //{
        //    Input = sample;
        //    Forward();
        //    float[] finalLabels = new float[Input.BatchSize * Model.RNNCells.Last().HiddenStateDim];
        //    float[] finalEmbeds = new float[Input.BatchSize * Model.RNNCells.Last().InputFeatureDim];

        //    Input.SampleIdx.CopyOutFromCuda();
        //    ((SeqDenseBatchData)LinkRunners.Last().Input).SentOutput.CopyOutFromCuda();
        //    ((SeqDenseBatchData)LinkRunners.Last().Output).SentOutput.CopyOutFromCuda();

        //    Parallel.For(0, Input.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = 8 }, i =>
        //    {
        //        int seqEnd = Input.SampleIdx.MemPtr[i] - 1;
        //        int seqStart = i == 0 ? 0 : Input.SampleIdx.MemPtr[i - 1];

        //        if (seqEnd >= seqStart)
        //        {
        //            for (int d = 0; d < ((SeqDenseBatchData)LinkRunners.Last().Output).Dim; d++)
        //            {
        //                finalLabels[i * Model.RNNCells.Last().HiddenStateDim + d] = 
        //                    ((SeqDenseBatchData)LinkRunners.Last().Output).SentOutput.MemPtr[seqEnd * Model.RNNCells.Last().HiddenStateDim + d];
        //            }

        //            for (int d = 0; d < ((SeqDenseBatchData)LinkRunners.Last().Input).Dim; d++)
        //            {
        //                finalEmbeds[i * Model.RNNCells.Last().InputFeatureDim + d] =
        //                    ((SeqDenseBatchData)LinkRunners.Last().Input).SentOutput.MemPtr[seqEnd * Model.RNNCells.Last().InputFeatureDim + d];
        //            }
        //        }
        //    });
        //    return new Tuple<float[], float[]>(finalLabels, finalEmbeds);
        //}

        //public void ExtractRNNOutput(IEnumerable<IN> data, EvaluationParameter EvalParameter)
        //{
        //    using (StreamWriter writer = new StreamWriter(new FileStream(((BasicEvalParameter)EvalParameter.Param).ScoreFile, FileMode.Create, FileAccess.Write)))
        //    {
        //        int id = 0;
        //        foreach (IN sample in data)
        //        {
        //            Tuple<float[], float[]> output = RNNOutput(sample);
                    
        //            int labelDim = output.Item1.Length / sample.BatchSize;
        //            int embedDim = output.Item2.Length / sample.BatchSize;

        //            for (int i = 0; i < sample.BatchSize; i++)
        //                writer.WriteLine(string.Join(",", output.Item1.Skip(i * labelDim).Take(labelDim)) + "\t" +
        //                                string.Join(",", output.Item2.Skip(i * embedDim).Take(embedDim)));

        //            if (++id % 500 == 0) Console.WriteLine("MiniBatch {0}", id);
        //        }
        //        Console.WriteLine("Predict Done!");
        //    }
        //}

        //public float[] EmbeddingForward()
        //{
        //    Forward();

        //    float[] embedOutput = new float[Input.BatchSize * Model.RNNCells.Last().InputFeatureDim];
        //    Input.SampleIdx.CopyOutFromCuda();
        //    ((SeqDenseBatchData)LinkRunners.Last().Input).SentOutput.CopyOutFromCuda();

        //    //Parallel.for(int i=0;i<Input.BatchSize;i++)
        //    //{
        //    Parallel.For(0, Input.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = 8 }, i =>
        //    {
        //        int seqEnd = Input.SampleIdx.MemPtr[i] - 1;
        //        int seqStart = i == 0 ? 0 : Input.SampleIdx.MemPtr[i - 1];
                
        //        if(seqEnd >= seqStart)
        //        {
        //            for (int d = 0; d < ((SeqDenseBatchData)LinkRunners.Last().Input).Dim; d++)
        //            {
        //                embedOutput[i * Model.RNNCells.Last().InputFeatureDim + d] = ((SeqDenseBatchData)LinkRunners.Last().Input).SentOutput.MemPtr[seqEnd * Model.RNNCells.Last().InputFeatureDim + d];
        //            }
        //        }
        //    });
        //    return embedOutput;
        //}

        //public float[] FinalLabelForward()
        //{
        //    Forward();

        //    float[] embedOutput = new float[Input.BatchSize * Model.RNNCells.Last().HiddenStateDim];

        //    Input.SampleIdx.CopyOutFromCuda();
        //    ((SeqDenseBatchData)LinkRunners.Last().Output).SentOutput.CopyOutFromCuda();

        //    //Parallel.for(int i=0;i<Input.BatchSize;i++)
        //    //{
        //    Parallel.For(0, Input.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = 8 }, i =>
        //    {
        //        int seqEnd = Input.SampleIdx.MemPtr[i] - 1;
        //        int seqStart = i == 0 ? 0 : Input.SampleIdx.MemPtr[i - 1];

        //        if (seqEnd >= seqStart)
        //        {
        //            for (int d = 0; d < ((SeqDenseBatchData)LinkRunners.Last().Output).Dim; d++)
        //            {
        //                embedOutput[i * Model.RNNCells.Last().HiddenStateDim + d] = ((SeqDenseBatchData)LinkRunners.Last().Output).SentOutput.MemPtr[seqEnd * Model.RNNCells.Last().HiddenStateDim + d];
        //            }
        //        }
        //    });
        //    return embedOutput;
        //}

        //public void ExtractFinalLabel(IEnumerable<IN> data, EvaluationParameter EvalParameter)
        //{
        //    VECTOROUTPUTEvaluationSet evaluator = new VECTOROUTPUTEvaluationSet();
        //    evaluator.Init((BasicEvalParameter)EvalParameter.Param);
        //    DateTime pre = DateTime.Now;
        //    double PutTime = 0;
        //    double ForwardTime = 0;
        //    double EvaluationTime = 0;
        //    int id = 0;
        //    foreach (IN sample in data)
        //    {
        //        DateTime t1 = DateTime.Now;
        //        PutTime += DateTime.Now.Subtract(t1).TotalMilliseconds;
        //        Input = sample;

        //        DateTime t2 = DateTime.Now;
        //        float[] labels = FinalLabelForward();
        //        ForwardTime += DateTime.Now.Subtract(t2).TotalMilliseconds;

        //        DateTime t3 = DateTime.Now;
        //        //PushScore(float[] score, int dim, int batchSize)
        //        evaluator.PushScore(labels, Model.RNNCells.Last().InputFeatureDim, Input.BatchSize);
        //        EvaluationTime += DateTime.Now.Subtract(t3).TotalMilliseconds;

        //        if (id % 500 == 0) Console.WriteLine("MiniBatch {0}", id);
        //        id += 1;
        //    }
        //    Console.WriteLine("Predict Done!");
        //    List<string> evalInfo = new List<string>();
        //    float score = evaluator.Evaluation(out evalInfo);
        //    evaluator.Dispose();
        //    Logger.WriteLog("Evaluation : \n{0} \n End-Score : {1}, Time : {2}", string.Join("\n", evalInfo), score, DateTime.Now.Subtract(pre).TotalSeconds);
        //    Logger.WriteLog("Date IO Time {0}; Forward Time {1}; Evaluation Time {2}", PutTime, ForwardTime, EvaluationTime);
        //}

        //public void ExtractEmbedding(IEnumerable<IN> data, EvaluationParameter EvalParameter)
        //{
        //    VECTOROUTPUTEvaluationSet evaluator = new VECTOROUTPUTEvaluationSet();
        //    evaluator.Init((BasicEvalParameter)EvalParameter.Param);

        //    DateTime pre = DateTime.Now;
        //    double PutTime = 0;
        //    double ForwardTime = 0;
        //    double EvaluationTime = 0;
        //    int id = 0;
        //    foreach (IN sample in data)
        //    {
        //        DateTime t1 = DateTime.Now;
        //        PutTime += DateTime.Now.Subtract(t1).TotalMilliseconds;
        //        Input = sample;

        //        DateTime t2 = DateTime.Now;
        //        float[] embeds = EmbeddingForward();

        //        ForwardTime += DateTime.Now.Subtract(t2).TotalMilliseconds;

        //        DateTime t3 = DateTime.Now;
        //        //PushScore(float[] score, int dim, int batchSize)
        //        evaluator.PushScore(embeds, Model.RNNCells.Last().InputFeatureDim, Input.BatchSize);
        //        EvaluationTime += DateTime.Now.Subtract(t3).TotalMilliseconds;

        //        if (id % 500 == 0) Console.WriteLine("MiniBatch {0}", id);
        //        id += 1;
        //    }
        //    Console.WriteLine("Predict Done!");
        //    List<string> evalInfo = new List<string>();
        //    float score = evaluator.Evaluation(out evalInfo);
        //    evaluator.Dispose();
        //    Logger.WriteLog("Evaluation : \n{0} \n End-Score : {1}, Time : {2}", string.Join("\n", evalInfo), score, DateTime.Now.Subtract(pre).TotalSeconds);

        //    Logger.WriteLog("Date IO Time {0}; Forward Time {1}; Evaluation Time {2}", PutTime, ForwardTime, EvaluationTime);
        //}

    }
}
