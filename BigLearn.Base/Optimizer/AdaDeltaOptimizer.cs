﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// 
    /// </summary>
    public class AdaDeltaOptimizer : GradientOptimizer
    {
        float UpdateClip = 0;
        float WeightClip = 0;
        public float Rho { get; set; }
        public float Epsilon { get; set; }
     
        CudaPieceFloat AccumGrads = null;
        CudaPieceFloat AccumUpdates = null;
        CudaPieceFloat WeightUpdate = null;

        public AdaDeltaOptimizer(int weightSize, float  rho, float epsilon, float clipDelta, float clipWeight, RunnerBehavior behavior) : base(behavior)
        {
            UpdateClip = clipDelta;
            WeightClip = clipWeight;

            AccumGrads = new CudaPieceFloat(weightSize, true, Behavior.Device == DeviceType.GPU);
            AccumGrads.Zero();
            WeightUpdate = new CudaPieceFloat(weightSize, true, Behavior.Device == DeviceType.GPU);
            WeightUpdate.Zero();
            AccumUpdates = new CudaPieceFloat(weightSize, true, Behavior.Device == DeviceType.GPU);
            AccumUpdates.Zero();

            Rho = rho; // 0.9995f;
            Epsilon = epsilon;// 1.0e-6f;
        }


        //public override void Init(CudaPieceFloat weight, StructureLearner learner)
        //{
        //    base.Init(weight, learner);

        //    UpdateClip = learner.ClipDelta;
        //    WeightClip = learner.ClipWeight;

        //    AccumGrads = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    AccumGrads.Zero();
        //    WeightUpdate = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    WeightUpdate.Zero();
        //    AccumUpdates = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    AccumUpdates.Zero();

        //    Parameter = weight;
        //    GradientStep = 1;
        //    UpdateRate = learner.LearnRate;
        //    Gradient = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    Gradient.Zero();
        //    Rho = learner.AdaDelta_Rho; // 0.9995f;
        //    Epsilon = learner.AdaDelta_Epsilon;// 1.0e-6f;
        //}
        
        public override void AfterGradient()
        {
            if (UpdateClip > 0) ComputeLib.ClipVector(Gradient, Gradient.Size, UpdateClip, -UpdateClip);

            //AccumGrad = AccumGrad * Rho + (1 - Rho) * Gradient ^ 2
            ComputeLib.VectorSquareAdd(AccumGrads, AccumGrads, Gradient, Rho, 1-Rho, Parameter.Size);

            //WeightUpdate = Gradient * Sqrt(AccumUpdates + Epsilon) / Sqrt(AccumGrad + Epsilon)
            ComputeLib.AdaDeltaGradient(WeightUpdate, AccumGrads, AccumUpdates, Gradient, Epsilon, Parameter.Size);
            //W = W + WeightUpdate
            ComputeLib.Add_Vector(Parameter, WeightUpdate, Parameter.Size, 1, UpdateRate);

            //AccumUpdates = AccumUpdates * Rho + (1 - Rho) * WeightUpdate ^2 
            ComputeLib.VectorSquareAdd(AccumUpdates, AccumUpdates, WeightUpdate, Rho, 1 - Rho, Parameter.Size);

            if (WeightClip > 0) ComputeLib.ClipVector(Parameter, Parameter.Size, WeightClip, -WeightClip);

            ComputeLib.Zero(Gradient, Gradient.Size);
        }



        protected override void Dispose(bool disposing)
        {
            if (AccumGrads != null)
            {
                AccumGrads.Dispose(); AccumGrads = null;
            }

            if (WeightUpdate != null)
            {
                WeightUpdate.Dispose();
                WeightUpdate = null;
            }

            if (AccumUpdates != null) 
            {
                AccumUpdates.Dispose();
                AccumUpdates = null;
            }

            if (Gradient != null)
            {
                Gradient.Dispose();
                Gradient = null;
            }
        }
    }
}
