﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    //public class GradientCheck : GlobalOptimizer
    //{
    //    public GradientCheck(RunnerBehavior behavior) : base(behavior) { }
    //    public override void InitGlobalOptimizer(int featureNum, StructureLearner learner)
    //    {
    //        Parameter = new CudaPieceFloat(featureNum, true, learner.device == DeviceType.GPU);
    //        Gradient = new CudaPieceFloat(featureNum, true, learner.device == DeviceType.GPU);
    //    }
    //}
    ///// <summary>
    ///// Consider Flow as Gradient Check.
    ///// </summary>
    //public class GradientCheckFlow
    //{
    //    CudaPieceFloat Grad;
    //    CudaPieceFloat Param;

    //    Structure Model { get; set; }
    //    ComputationGraph CG { get; set; }
    //    DeviceType Device { get; set; }
    //    public GradientCheckFlow(Structure model, DeviceType device, ComputationGraph cg)
    //    {
            
    //        Model = model;
    //        CG = cg;
    //        Device = device;
    //    }

    //    public void Run()
    //    {
    //        Grad = new CudaPieceFloat(Model.ParamNumber, true, false);
    //        Param = new CudaPieceFloat(Model.ParamNumber, true, false);

    //        /// **************** Init Model-Weight Optimizer; ***************** ///
    //        foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
    //        {
    //            optimizer.Optimizer = new AccumulateGradOptimizer(optimizer.Parameter, 1.0f, new RunnerBehavior() { Device = Device });
    //        }

    //        Random randomPoint = new Random(13);
    //        int CheckFailRate = 0;
    //        int CheckSuccessRate = 0;
    //        int CheckPoint = 0;
    //        CG.Init();
    //        while(CG.FetchData())
    //        {
    //            if (!CG.IsContinue) continue;

    //            double Loss = CG.ForwardLoss();
    //            CG.BackwardGrad();

    //            Model.GetModelGradient(Grad, 0);
    //            Model.GetModelParameter(Param, 0);
    //            //Grad.SyncToCPU();
    //            //Param.SyncToCPU();
    //            float eps = 0.001f;
    //            for (int i = 0; i < 1000; i++)
    //            {
    //                //float delta = Util.URandom.NextDouble() > 0.5 ? eps : -eps;
    //                int xIdx = randomPoint.Next(Param.Size);
    //                //if (Grad.MemPtr[xIdx] == 0) continue;
    //                Param.MemPtr[xIdx] += eps;
    //                //Param.SyncFromCPU();
    //                Model.SetModelParameter(Param, 0);
    //                double loss_t1 = CG.ForwardLoss();

    //                Param.MemPtr[xIdx] -= 2 * eps;
    //                //Param.SyncFromCPU();
    //                Model.SetModelParameter(Param, 0);
    //                double loss_t2 = CG.ForwardLoss();

    //                double deltaLoss1 = -loss_t1 + Loss;
    //                double deltaLoss2 = loss_t2 - Loss;
    //                double deltaLossTwoSide = (deltaLoss1 + deltaLoss2) / 2;

    //                double deltaEst = eps * Grad.MemPtr[xIdx];
    //                if (Math.Abs((deltaEst - deltaLossTwoSide) / Math.Max(Math.Abs(deltaEst), Math.Abs(deltaLossTwoSide))) > 0.05 &&
    //                    Math.Abs((deltaEst - deltaLoss2) / Math.Max(Math.Abs(deltaEst), Math.Abs(deltaLoss2))) > 0.05 &&
    //                    Math.Abs((deltaEst - deltaLoss1) / Math.Max(Math.Abs(deltaEst), Math.Abs(deltaLoss1))) > 0.05 &&
    //                   (deltaEst < Math.Min(Math.Min(deltaLoss1, deltaLoss2), deltaLossTwoSide) || deltaEst > Math.Max(Math.Max(deltaLoss1, deltaLoss2), deltaLossTwoSide)))
    //                {
    //                    CheckFailRate += 1;
    //                    Console.WriteLine("Checking Gradient DeltaLoss1 {0}, DeltaLoss2 {1}, Delta Loss TwoSide {2}, DeltaEst {3}, SuccessRate {4}, Fail *******************",
    //                        deltaLoss1, deltaLoss2, deltaLossTwoSide, deltaEst, CheckSuccessRate * 1.0 / (CheckSuccessRate + CheckFailRate));
    //                }
    //                else
    //                {
    //                    CheckSuccessRate += 1;
    //                }

    //                if (++CheckPoint % 100 == 0)
    //                {
    //                    Console.WriteLine("Checking Gradient DeltaLoss1 {0}, DeltaLoss2 {1}, Delta Loss TwoSide {2}, DeltaEst {3}, SuccessRate {4} Success",
    //                        deltaLoss1, deltaLoss2, deltaLossTwoSide, deltaEst, CheckSuccessRate * 1.0 / (CheckSuccessRate + CheckFailRate));
    //                }
    //                if (Math.Abs(deltaEst) > 1000)
    //                {
    //                    Console.WriteLine("Feature Idx {0} hug error {1}!!\n Press Enter to Continue", xIdx, deltaEst);
    //                    Console.ReadLine();
    //                }
    //                Param.MemPtr[xIdx] += eps;
    //                Param.SyncFromCPU();
    //                Model.SetModelParameter(Param, 0);
    //            }
    //            foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
    //                optimizer.Optimizer.Gradient.Zero();
    //        }
    //        CG.Complete();
    //    }
    //}
}
