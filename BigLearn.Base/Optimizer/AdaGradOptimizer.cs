﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class AdaGradOptimizer : GradientOptimizer
    {
        float UpdateClip = 0;
        float WeightClip = 0;

        CudaPieceFloat Ada = null;

        public AdaGradOptimizer(RunnerBehavior behavior) : base(behavior) { }

        public AdaGradOptimizer(int weightSize, float updateClip, float weightClip, RunnerBehavior behavior) : base(behavior)
        {
            UpdateClip = updateClip;
            WeightClip = weightClip;

            Ada = new CudaPieceFloat(weightSize, true, behavior.Device == DeviceType.GPU);
            Ada.Init(1e-6f);
        }

        //public override void Init(CudaPieceFloat weight, StructureLearner learner)
        //{
        //    UpdateClip = learner.ClipDelta;
        //    WeightClip = learner.ClipWeight;
        //    Ada = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    Ada.Init(1e-6f);
        //    Parameter = weight;
        //    GradientStep = 1;
        //    UpdateRate = learner.LearnRate;
        //    Gradient = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    Gradient.Zero();
        //    Aggregator = learner.GradientAggFactory == null ? null : learner.GradientAggFactory.CreateAggregator(weight.Size);
        //}

        public override void AfterGradient()
        {
            var t = PerfCounter.Manager.Instance["AdaAfter"].Begin();
            ComputeLib.ClipAdaGradUpdate(Ada, Gradient, Parameter, UpdateRate, UpdateClip, WeightClip);
            PerfCounter.Manager.Instance["AdaAfter"].TakeCount(t);
        }

        protected override void Dispose(bool disposing)
        {
            if (Ada != null)
            {
                Ada.Dispose(); Ada = null;
            }

            if (Gradient != null)
            {
                Gradient.Dispose();
                Gradient = null;
            }
        }
    }
}
