﻿using BigLearn;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SGDOptimizer : GradientOptimizer
    {
        public SGDOptimizer(RunnerBehavior behavior) : base(behavior) { }

        public SGDOptimizer(CudaPieceFloat weight, float lr, RunnerBehavior behavior) : base(behavior)
        {
            SetupConfig(weight, new CudaPieceFloat(weight.Size, behavior.Device), new StructureLearner() { LearnRate = lr, device = behavior.Device });
        }

        public override void AfterGradient()
        {    
            ComputeLib.AddAndClear(Parameter, Gradient, Parameter.Size, UpdateRate);
        }
    }
}
