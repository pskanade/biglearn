﻿using BigLearn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum DNNOptimizerType
    {
        SGD = 0,
        AdaGrad = 1,
        Momentum = 2,
        AdjustSGD = 3,
        BatchLBFGS = 4,
        ClipSGD = 5,
        GlobalGradientCheck = 6,
        GradParameterServer = 7,
        SoftClipSGD = 8,
        Adam = 9,
        AdaDelta = 10,
        RMSProp = 11,
        AdaMax = 12,
        NVMomentum = 13,
        RMSPropV2 = 14,
        DistASGD = 100,
        DistMASGD = 101,
        BlockMomentum = 102,
        BlockAdam = 103,
    }

    public enum VecNormType { NoNorm, L2Norm, L2BallNorm, L1Norm, L1BallNorm }
    public interface ISparseGradientOptimizer
    {
        void PushSparseGradient(int offset, int size);
        void ClearSparseGradient();
    }

    public abstract class GradientOptimizer : StructRunner, ISparseGradientOptimizer
    {
        /// <summary>
        /// This function will be staled.
        /// </summary>
        /// <param name="Weight"></param>
        /// <param name="learner"></param>
        /// <returns></returns>
        public static GradientOptimizer CreateLocalOptimizer(CudaPieceFloat Weight, StructureLearner learner, RunnerBehavior behavior)
        {
            throw new Exception("CreateLocalOptimizer(CudaPieceFloat Weight, StructureLearner learner, RunnerBehavior behavior) is staled, please call GradientOptimizer CreateLocalOptimizer(CudaPieceFloat Weight, CudaPieceFloat Gradient, StructureLearner learner, RunnerBehavior behavior)");
            //GradientOptimizer optimizer = null;
            //switch (learner.Optimizer)
            //{
            //    case DNNOptimizerType.SGD:
            //        optimizer = new SGDOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.AdaGrad:
            //        optimizer = new AdaGradOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.NVMomentum:
            //        optimizer = new NAGOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.Momentum:
            //        optimizer = new MomentumOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.AdjustSGD:
            //        optimizer = null;// new AdjustSGDOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.ClipSGD:
            //        optimizer = new ClipSGDOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.BatchLBFGS:
            //        optimizer = new GlobalOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.GlobalGradientCheck:
            //        optimizer = new GlobalOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.SoftClipSGD:
            //        optimizer = new SoftClipSGDOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.Adam:
            //        optimizer = new AdamOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.AdaDelta:
            //        optimizer = new AdaDeltaOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.RMSProp:
            //        optimizer = new RMSPropOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.AdaMax:
            //        optimizer = new AdaMaxOptimizer(behavior);
            //        break;
            //    case DNNOptimizerType.DistASGD:
            //        optimizer = new DistASGDOptimizer(behavior);                
            //        break;
            //    default:
            //        throw new NotSupportedException(string.Format("Optimizer of type {0} is not supported yet.", learner.Optimizer));
            //}

            //optimizer.Init(Weight, learner);
            //return optimizer;
        }

        /// <summary>
        /// This function will be staled.
        /// </summary>
        /// <param name="Weight"></param>
        /// <param name="learner"></param>
        /// <returns></returns>
        public static GradientOptimizer CreateLocalOptimizer(CudaPieceFloat Weight, CudaPieceFloat Gradient, StructureLearner learner, RunnerBehavior behavior)
        {
            GradientOptimizer optimizer = null;
            switch (learner.Optimizer)
            {
                case DNNOptimizerType.SGD:
                    optimizer = new SGDOptimizer(behavior);
                    break;
                case DNNOptimizerType.AdaGrad:
                    optimizer = new AdaGradOptimizer(Weight.Size, learner.ClipDelta, learner.ClipWeight, behavior);
                    break;
                case DNNOptimizerType.NVMomentum:
                    optimizer = new NAGOptimizer(Weight.Size, learner.MomemtumRate, behavior);
                    break;
                case DNNOptimizerType.Momentum:
                    optimizer = new MomentumOptimizer(learner.MomemtumRate, behavior);
                    break;
                case DNNOptimizerType.AdjustSGD:
                    throw new NotImplementedException();
                    //optimizer = null;// new AdjustSGDOptimizer(behavior);
                case DNNOptimizerType.ClipSGD:
                    optimizer = new ClipSGDOptimizer(learner.ClipDelta, learner.ClipWeight, behavior);
                    break;
                case DNNOptimizerType.BatchLBFGS:
                    throw new NotImplementedException();
                    //optimizer = new LBFGSBatchOptimizer(behavior);
                    //break;
                case DNNOptimizerType.GlobalGradientCheck:
                    throw new NotImplementedException();
                case DNNOptimizerType.SoftClipSGD:
                    optimizer = new SoftClipSGDOptimizer(learner.ClipDelta, learner.ClipWeight, behavior);
                    break;
                case DNNOptimizerType.Adam:
                    optimizer = new AdamOptimizer(Weight.Size, learner.Adam_Beta1, learner.Adam_Beta2, learner.Adam_Epsilon, learner.ClipDelta, learner.ClipWeight, behavior);
                    break;
                case DNNOptimizerType.AdaDelta:
                    optimizer = new AdaDeltaOptimizer(Weight.Size, learner.AdaDelta_Rho, learner.AdaDelta_Epsilon, learner.ClipDelta, learner.ClipWeight, behavior);
                    break;
                case DNNOptimizerType.RMSProp:
                    optimizer = new RMSPropOptimizer(Weight.Size, learner.RmsProp_Decay, learner.RmsProp_Epsilon, learner.ClipDelta, learner.ClipWeight, behavior);
                    break;
                case DNNOptimizerType.RMSPropV2:
                    optimizer = new RMSPropV2Optimizer(Weight.Size, learner.RmsProp_Decay, learner.RmsProp_Decay, learner.ClipDelta, learner.ClipWeight, behavior);
                    break;
                case DNNOptimizerType.AdaMax:
                    optimizer = new AdaMaxOptimizer(Weight.Size, learner.ClipDelta, learner.ClipWeight, behavior);
                    break;
                case DNNOptimizerType.DistASGD:
                    throw new NotImplementedException();
                default:
                    throw new NotSupportedException(string.Format("Optimizer of type {0} is not supported yet.", learner.Optimizer));
            }
            //optimizer.InitEMAConfig(learner.EMA_Decay);
            optimizer.SetupConfig(Weight, Gradient, learner);
            //optimizer.Init(Weight, learner);
            return optimizer;
        }


        /// <summary>
        /// This function will be staled.
        /// </summary>
        /// <param name="feaNumber"></param>
        /// <param name="learner"></param>
        /// <returns></returns>
        //public static GlobalOptimizer CreateGlobalOptimizer(int feaNumber, StructureLearner learner, RunnerBehavior behavior)
        //{
        //    GlobalOptimizer optimizer = null;
        //    switch (learner.Optimizer)
        //    {
        //        case DNNOptimizerType.BatchLBFGS:
        //            optimizer = new LBFGSBatchOptimizer(behavior);
        //            break;
        //        case DNNOptimizerType.GlobalGradientCheck:
        //            optimizer = new GradientCheck(behavior);
        //            break;
        //    }
        //    optimizer.InitGlobalOptimizer(feaNumber, learner);
        //    return optimizer;
        //}

        /// <summary>
        /// please make sure GradientStep is always one. 
        /// </summary>
        public float GradientStep = 1;
        public virtual CudaPieceFloat Gradient { get; set; }
        public virtual CudaPieceFloat Parameter { get; set; }

        public float EMAStep = 1;
        public virtual CudaPieceFloat Shadow { get; set; }
        public virtual CudaPieceFloat BackupParameter { get; set; }
        protected bool IsBackup = false;
        protected IGradientAggregator Aggregator;
        protected float UpdateRate;
        protected StructureLearner HyperParameter;

        protected GradientOptimizer(RunnerBehavior behavior) : base(Structure.Empty, behavior) { }

        protected void SetupConfig(CudaPieceFloat weight, CudaPieceFloat gradient, StructureLearner learner)
        {
            HyperParameter = learner;

            Parameter = weight;
            GradientStep = 1;
            UpdateRate = learner.LearnRate;

            if (gradient == null) { Gradient = new CudaPieceFloat(weight.Size, learner.device); Console.WriteLine("WARNING !!! Gradient is not assigned."); }
            else Gradient = gradient;
            ComputeLib.Zero(Gradient, Gradient.Size);

            Aggregator = learner.GradientAggFactory == null ? null : learner.GradientAggFactory.CreateAggregator(weight.Size);

            EMAStep = learner.EMA_Decay;
            if (EMAStep < 1)
            {
                Shadow = new CudaPieceFloat(Parameter.Size, true, Behavior.Device == DeviceType.GPU);
                Shadow.CopyFrom(Parameter);
                BackupParameter = new CudaPieceFloat(Parameter.Size, true, Behavior.Device == DeviceType.GPU);
            }
        }

        public override void Init()
        {
            if (HyperParameter != null)
            {
                UpdateRate = HyperParameter.DynamicLR(iteration);
            }

            iteration++;
            if (IsBackup && BackupParameter != null)
            {
                ComputeLib.Add_Vector(Parameter, BackupParameter, Parameter.Size, 0, 1);
            }
        }

        public override void Complete()
        {
            if (Shadow != null)
            {
                ComputeLib.Add_Vector(BackupParameter, Parameter, Parameter.Size, 0, 1);
                ComputeLib.Add_Vector(Parameter, Shadow, Parameter.Size, 0, 1);
                IsBackup = true;
            }
        }

        //public virtual void Init(CudaPieceFloat weight, StructureLearner learner)
        //{
        //    EMAStep = learner.EMA_Decay;
        //    if (EMAStep < 1)
        //    {
        //        Shadow = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //        Shadow.CopyFrom(weight);

        //        BackupParameter = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    }
        //}
      
        public virtual void BeforeGradient()
        {

        }

        public virtual void PrepareAfterGradient()
        {
            if (Aggregator != null) Aggregator.Aggregate(Gradient);
        }
        public virtual void AfterGradient()
        { }
        public virtual void ConfirmAfterGradient()
        {
            if (EMAStep < 1)
            {
                ComputeLib.Add_Vector(Shadow, Parameter, Parameter.Size, EMAStep, 1 - EMAStep);
            }
        }

        public virtual void Reset()
        { }

        protected Dictionary<int, int> SparseGradientDict = new Dictionary<int, int>();
        public void PushSparseGradient(int offset, int size)
        {
            if (SparseGradientDict.ContainsKey(offset)) SparseGradientDict[offset] = Math.Max(SparseGradientDict[offset], size);
            else SparseGradientDict[offset] = size;
        }

        public void ClearSparseGradient()
        {
            SparseGradientDict.Clear();
        }
    }


    /// <summary>
    /// Loss Function.
    /// </summary>
    /// <param name="parameter"></param>
    /// <returns></returns>
    public delegate float ObjectiveFunction(object obj, CudaPieceFloat parameter);

    public interface IGlobalOptimizer
    {
        //void InitGlobalOptimizer(StructureLearner learner);

        //void ResetGlobalOptimization();

        int GetModelGradient(CudaPieceFloat grad, int index);

        int GetModelParameter(CudaPieceFloat param, int index);

        int SetModelParameter(CudaPieceFloat param, int index);

        //void EndGlobalOptimization(double loss, object par, ObjectiveFunction func);

        //GlobalOptimizer GlobalOptimizer { get; set; }
    }

    public class OptimizationLib
    {
        public static Tuple<float, float> LineSearch(CudaPieceFloat parameter, CudaPieceFloat direction, ObjectiveFunction func, object par, double currentV, float initStepSize, float discount, float threshouldStepSize)
        {
            float stepSize = initStepSize;
            double currentf = currentV; //.currentBestloss;
            Cudalib.Add_Vector(parameter.CudaPtr, direction.CudaPtr, (uint)parameter.Size, 1, stepSize);

            int idx = 0;
            Console.WriteLine("Start Linesearch at:{0}", DateTime.Now);
            DateTime pre = DateTime.Now;
            while (Math.Abs(stepSize) > threshouldStepSize)
            {
                double newf = func(par, parameter);
                double Armijo_threshold = currentf;
                if (newf < Armijo_threshold)
                {
                    currentf = newf;
                    break;
                }
                else
                {
                    Cudalib.Add_Vector(parameter.CudaPtr, direction.CudaPtr, (uint)parameter.Size, 1, -stepSize + discount * stepSize);
                    stepSize *= discount;
                    idx++;
                }

            }
            Console.WriteLine("finish linesearch take:{0} seconds, {1} times with step size:{2}", DateTime.Now.Subtract(pre).TotalSeconds, idx, stepSize);
            return Tuple.Create((float)currentf, stepSize);
        }
    }


    /// <summary>
    /// Accmulate Gradient.
    /// </summary>
    //public class AccumulateGradOptimizer : GradientOptimizer
    //{
    //    #region To be compatible with previous design. This will be re-designed. 
    //    public AccumulateGradOptimizer(RunnerBehavior behavior) : base(behavior) { }
    //    public override void Init(CudaPieceFloat weight, StructureLearner learner)
    //    {
    //        Parameter = weight;
    //        Gradient = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
    //        Gradient.Zero();
    //        GradientStep = learner.LearnRate;
    //    }
    //    #endregion.

    //    public AccumulateGradOptimizer(CudaPieceFloat weight, float learnRate, RunnerBehavior behavior) : base(behavior)
    //    {
    //        Parameter = weight;
    //        Gradient = new CudaPieceFloat(weight.Size, true, behavior.Device == DeviceType.GPU);
    //        Gradient.Zero();
    //        GradientStep = learnRate;
    //    }
    //    public AccumulateGradOptimizer(Structure model, float learnRate, RunnerBehavior behavior) : base(behavior)
    //    {
    //        foreach (Structure subStruct in model.SubStructure)
    //        {
    //            AccumulateGradOptimizer optimizer = new AccumulateGradOptimizer(subStruct, learnRate, behavior);
    //        }

    //        foreach (ModelOptimizer subOptimizer in model.StructureOptimizer)
    //        {
    //            subOptimizer.Optimizer = new AccumulateGradOptimizer(subOptimizer.Parameter, learnRate, behavior);
    //        }
    //    }

    //    public override void BeforeGradient()
    //    {
    //        Gradient.Zero();
    //    }

    //    //public override void Reset()
    //    //{
    //    //    Gradient.Zero();
    //    //}
    //}

    //public class GlobalOptimizer : AccumulateGradOptimizer
    //{

    //    protected override void Dispose(bool disposing)
    //    {
    //        if (Gradient != null) Gradient.Dispose(); Gradient = null;
    //    }

    //    public GlobalOptimizer(RunnerBehavior behavior) : base(behavior) { }

    //    public virtual void InitGlobalOptimizer(int featureNum, StructureLearner learner) { }

    //    public virtual void UpdateGlobalOptimizer(double loss, object par, ObjectiveFunction func)
    //    { }
    //}

    public class LBFGSBatchOptimizer : GradientOptimizer
    {
        LBFGS2 Lbfgs = new LBFGS2();
        int BatchNum = 0;
        int Iteration = 0;

        public LBFGSBatchOptimizer(int featureDim, RunnerBehavior behavior) : base(behavior)
        {
            Lbfgs.Init(10, featureDim, behavior.Device, 0, 0);
        }

        public override void BeforeGradient()
        { }

        public override void AfterGradient()
        {
            BatchNum += 1;
        }

        public override void Init()
        {
            BatchNum = 0;
        }

        public override void Complete()
        {
            //double currentLoss = loss / BatchNum + Lbfgs.GetRegLoss(Parameter);
            ComputeLib.Add_Vector(Gradient, Gradient, Gradient.Size, 0,  -1.0f / BatchNum);
            Iteration += 1;
        }

        //public override void UpdateGlobalOptimizer(double loss, object par, ObjectiveFunction func)
        //{
        //    Cudalib.Scale_Vector(Gradient.CudaPtr, Gradient.Size, -1.0f / BatchNum);
        //    CudaPieceFloat direct = Lbfgs.CalculateDirection(Parameter, Gradient);
        //    float stepSize = 1.0f;
        //    Tuple<float, float> lossStep = Lbfgs.LineSearch(Parameter, direct, func, par, currentLoss, stepSize, 0.25f, (float)1e-12, BatchNum);
        //    Console.WriteLine("[Line Search] LBFGS Loss Drop {0},{1}", currentLoss, lossStep.Item1);
        //    Iteration += 1;
        //}
    }
}
