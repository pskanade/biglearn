﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Nesterov's Accelerated Gradient
    /// </summary>
    public class NAGOptimizer : GradientOptimizer
    {
        float MomentumRate = 0;
        public CudaPieceFloat TrueWeight = null;
        public int ShrinkBucket = 1000;
        public float ShrinkRate = 0.8f;
        public NAGOptimizer(RunnerBehavior behavior) : base(behavior) { }
        public NAGOptimizer(int weightSize, float momentum, RunnerBehavior behavior) : base(behavior)
        {
            MomentumRate = momentum;

            TrueWeight = new CudaPieceFloat(weightSize, true, this.Behavior.Device == DeviceType.GPU);
            ComputeLib.Zero(TrueWeight, TrueWeight.Size);
            //TrueWeight.CopyFrom(Parameter);
        }

        //public override void Init(CudaPieceFloat weight, StructureLearner learner)
        //{
        //    GradientStep = learner.LearnRate;
        //    Parameter = weight;
        //    Gradient = new CudaPieceFloat(weight.Size, true, this.Behavior.Device == DeviceType.GPU);
        //    TrueWeight = new CudaPieceFloat(weight.Size, true, this.Behavior.Device == DeviceType.GPU);
        //    ComputeLib.Zero(TrueWeight, TrueWeight.Size);
        //    //TrueWeight.CopyFrom(Parameter);
        //    this.SetArgs(learner.OptArgs);
        //}

        /// <summary>
        /// http://cs231n.github.io/neural-networks-3/#ada NAG Implementation.
        /// </summary>
        public override void AfterGradient()
        {
            ComputeLib.Add_Vector(Parameter, 0, Gradient, 0, Gradient.Size, 1, (1 + MomentumRate) * UpdateRate );
            ComputeLib.Add_Vector(Parameter, 0, TrueWeight, 0, Parameter.Size, 1, -MomentumRate * UpdateRate);
            TrueWeight.CopyFrom(Gradient);
            ComputeLib.Scale_Matrix(Gradient, 1, Gradient.Size, UpdateRate);

            //momentum.CudaPtr, trueWeight.CudaPtr, weight.CudaPtr, 1, weight.EffectiveSize, 1, 1, 0);
            //float beta = MomentumRate;
            //ComputeLib.NAGUpdate(Parameter, Gradient, TrueWeight, beta);
        }

        protected override void Dispose(bool disposing)
        {
            if (Gradient != null)
            {
                Gradient.Dispose();
            }

            Gradient = null;
        }

        //private void SetArgs(string args)
        //{
        //    args = args ?? string.Empty;
        //    string[] argPairs = args.Split(',');
        //    NamedArgsParser parser = new NamedArgsParser(argPairs);
        //    this.MomentumRate = parser.Get("MomentumRate", 0.99f);
        //}
    }
}
