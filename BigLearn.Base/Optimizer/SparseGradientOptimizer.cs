﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    //public class SparseGradientOptimizer : GradientOptimizer
    //{
    //    Dictionary<int, int> GradientIndex = new Dictionary<int, int>();
    //    bool IsNorm = true;
    //    public SparseGradientOptimizer(CudaPieceFloat weight, float learnRate, RunnerBehavior behavior) : base(behavior)
    //    {
    //        UpdateRate = learnRate;
    //        GradientStep = 1.0f;
    //        Parameter = weight;
    //        Gradient = new CudaPieceFloat(weight.Size, true, behavior.Device == DeviceType.GPU);
    //    }

    //    public virtual void PushGradientIndex(int idx, int range)
    //    {
    //        if (GradientIndex.ContainsKey(idx)) GradientIndex[idx] = Math.Max(GradientIndex[idx], range);
    //        else GradientIndex[idx] = range;
    //    }

    //    public override void BeforeGradient()
    //    {
    //        MathOperatorManager.GlobalInstance.Zero(Gradient, Gradient.Size);
    //        GradientIndex.Clear();
    //    }

    //    public override void AfterGradient()
    //    {
    //        Gradient.SyncToCPU();
    //        Parallel.ForEach(GradientIndex, gIdx =>
    //        {
    //            for (int s = 0; s < gIdx.Value; s++)
    //            {
    //                Parameter.MemPtr[gIdx.Key + s] += UpdateRate * Gradient.MemPtr[gIdx.Key + s];
    //            }

    //            if (IsNorm)
    //            {
    //                double x = 0;
    //                for (int s = 0; s < gIdx.Value; s++)
    //                {
    //                    x = x + Parameter.MemPtr[gIdx.Key + s] * Parameter.MemPtr[gIdx.Key + s];
    //                }
    //                x = Math.Sqrt(x);
    //                if (x > 1)
    //                    for (int ii = 0; ii < gIdx.Value; ii++)
    //                        Parameter.MemPtr[gIdx.Key + ii] = (float)(Parameter.MemPtr[gIdx.Key + ii] / x);
    //            }

    //        });

    //        Parameter.SyncFromCPU();
    //    }
    //}

    //public class SparseSGDOptimizerFlow : OptimizerFlow
    //{
    //    float LearnRate { get; set; }

    //    public override void Init(Structure model, RunnerBehavior behavior, ComputationGraph cg, StructureLearner learner)
    //    {
    //        Model = model;
    //        CG = cg;
    //        Device = behavior.Device;
    //        LearnRate = learner.LearnRate;

    //        /// **************** Init Model-Weight Optimizer; ***************** ///
    //        foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
    //            optimizer.Optimizer = new SparseGradientOptimizer(optimizer.Parameter, LearnRate, behavior);
    //    }

    //    public override double Run()
    //    {
    //        double Loss = 0;
    //        int batchIdx = 0;
    //        CG.Init();
    //        while (CG.FetchData())
    //        {
    //            if (!CG.IsContinue) continue;

    //            Loss = batchIdx * 1.0f / (batchIdx + 1) * Loss + 1.0f / (batchIdx + 1) * CG.ForwardLoss();

    //            if (++batchIdx % 50 == 0) Logger.WriteLog("Train Batch Num {0}, AvgLoss {1}", batchIdx, Loss);

    //            foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
    //                optimizer.Optimizer.BeforeGradient();

    //            CG.BackwardGrad();

    //            foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
    //                optimizer.Optimizer.AfterGradient();
    //        }
    //        CG.Complete();

    //        return Loss;
    //    }
    //}
}
