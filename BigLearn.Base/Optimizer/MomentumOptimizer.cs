﻿using BigLearn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MomentumOptimizer : GradientOptimizer
    {
        float MomentumRate = 0;
        public MomentumOptimizer(RunnerBehavior behavior) : base(behavior) { }

        public MomentumOptimizer(float momentum, RunnerBehavior behavior) : base(behavior)
        {
            MomentumRate = momentum;
        }

        //public override void Init(CudaPieceFloat weight, StructureLearner learner)
        //{
        //    GradientStep = 1;
        //    UpdateRate = learner.LearnRate;
        //    Parameter = weight;
        //    Gradient = new CudaPieceFloat(weight.Size, true, this.Behavior.Device == DeviceType.GPU);
        //    this.SetArgs(learner.OptArgs);
        //}

        public override void AfterGradient()
        {
            ComputeLib.Add_Vector(Parameter, Gradient, Parameter.Size, 1, UpdateRate);
            ComputeLib.Scale_Matrix(Gradient, Gradient.Size, 1, MomentumRate);
            //float beta = this.MomentumRate;
            //ComputeLib.MomentumUpdate(Parameter, Gradient, beta);
        }

        protected override void Dispose(bool disposing)
        {
            if (Gradient != null) Gradient.Dispose(); Gradient = null;
        }

        //private void SetArgs(string args)
        //{
        //    args = args ?? string.Empty;
        //    string[] argPairs = args.Split(',');
        //    NamedArgsParser parser = new NamedArgsParser(argPairs);
        //    this.MomentumRate = parser.Get("MomentumRate", 0.99f);
        //}
    }
}
