﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// ada = gamma * ada + (1 - gamma) * g^2
    /// w = w - (alpha/(sqrt(ada) + Epsilon)) * g
    /// </summary>
    public class RMSPropV2Optimizer: GradientOptimizer
    {
        public float Gamma {get; set;}
        public float Epsilon {get; set;}
        CudaPieceFloat Ada = null;
        CudaPieceFloat G = null;

        float UpdateClip = 0;
        float WeightClip = 0;


        public RMSPropV2Optimizer(int weightSize, float decay, float epsilon, float clipDelta, float clipWeight, RunnerBehavior behavior) : base(behavior)
        {
            UpdateClip = clipDelta;
            WeightClip = clipWeight;

            Ada = new CudaPieceFloat(weightSize, Behavior.Device);
            Ada.Init(epsilon);

            G = new CudaPieceFloat(weightSize, Behavior.Device);
            G.Init(0);

            Gamma = decay; // 0.995f; //Gamma = 0, it is the same as AdaGrad
            Epsilon = epsilon;
        }
        
        public override void BeforeGradient()
        {
        }

        /// <summary>
        /// https://groups.google.com/forum/#!topic/deep-q-learning/_RFrmUALBQo
        /// </summary>
        public override void AfterGradient()
        {
            if (UpdateClip > 0) ComputeLib.ClipVector(Gradient, Gradient.Size, UpdateClip, -UpdateClip);
            ComputeLib.RMSPropV2_Gradient(Ada, G, Gradient, Gamma, Epsilon, Gradient.Size);
            ComputeLib.Add_Vector(Parameter, Gradient, Parameter.Size, 1, UpdateRate);
            if (WeightClip > 0) ComputeLib.ClipVector(Parameter, Parameter.Size, WeightClip, -WeightClip);
            ComputeLib.Zero(Gradient, Gradient.Size);
        }

        protected override void Dispose(bool disposing)
        {
            if (Ada != null)
            {
                Ada.Dispose(); Ada = null;
            }

            if(G!=null)
            {
                G.Dispose(); G = null;
            }

            if (Gradient != null)
            {
                Gradient.Dispose();
                Gradient = null;
            }
        }
   }
}
