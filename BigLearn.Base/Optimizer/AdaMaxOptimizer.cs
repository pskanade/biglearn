﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Rf ADAM: A METHOD FOR STOCHASTIC OPTIMIZATION, Kingma et al., ICLR 2015
    /// </summary>
    public class AdaMaxOptimizer : GradientOptimizer
    {
        float UpdateClip = 0;
        float WeightClip = 0;
        public float Beta2 { get; set; }
        public float Epsilon { get; set; }
        public float Alpha { get; set; }
        public float Beta1 { get; set; }
        public int Count { get; set; }

        CudaPieceFloat M = null;
        CudaPieceFloat V = null;

        public AdaMaxOptimizer(int weightSize, float clipDelta, float clipWeight, RunnerBehavior behavior) : base(behavior)
        {
            UpdateClip = clipDelta;
            WeightClip = clipWeight;

            M = new CudaPieceFloat(weightSize, true, Behavior.Device == DeviceType.GPU);
            M.Zero();
            V = new CudaPieceFloat(weightSize, true, Behavior.Device == DeviceType.GPU);
            V.Zero();

            Alpha = 1.0e-2f;
            Beta2 = 0.999f;
            Beta1 = 0.95f;
            Epsilon = 1.0e-8f;
            Count = 0;
        }

        //public override void Init(CudaPieceFloat weight, StructureLearner learner)
        //{
        //    UpdateClip = learner.ClipDelta;
        //    WeightClip = learner.ClipWeight;
        //    M = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    M.Zero();
        //    V = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    V.Zero();

        //    Parameter = weight;
        //    UpdateRate = learner.LearnRate;

        //    Gradient = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    Gradient.Zero();
        //    Alpha = 1.0e-2f;
        //    Beta2 = 0.999f;
        //    Beta1 = 0.95f;
        //    Epsilon = 1.0e-8f;
        //    Count = 0;
        //    GradientStep = 1;
        //    Aggregator = learner.GradientAggFactory == null ? null : learner.GradientAggFactory.CreateAggregator(weight.Size);
        //}

        public override void AfterGradient()
        {
            Count += 1;

            if (UpdateClip > 0) { ComputeLib.ClipVector(Gradient, Gradient.Size, UpdateClip, -UpdateClip); }

            float Alpha = (float)(UpdateRate / (1 - Math.Pow(Beta1, Count)));

            //M = Beta1 * M + ( 1 - Beta1 ) * g
            ComputeLib.Add_Vector(M, Gradient, Parameter.Size, Beta1, 1 - Beta1);
            //V = max(beta2 * V, |g|)
            //Gradient = Alpha * M/V
            ComputeLib.AdaMax(V, M, Gradient, Alpha, Beta2, Epsilon, Parameter.Size);
            ComputeLib.Add_Vector(Parameter, Gradient, Parameter.Size, 1, 1);
            if (WeightClip > 0) { ComputeLib.ClipVector(Parameter, Parameter.Size, WeightClip, -WeightClip); }

            ComputeLib.Zero(Gradient, Gradient.Size);
        }

        protected override void Dispose(bool disposing)
        {
            if (M != null)
            {
                M.Dispose(); M = null;
            }

            if (V != null)
            {
                V.Dispose(); V = null;
            }

            if (Gradient != null)
            {
                Gradient.Dispose();
                Gradient = null;
            }
        }
    }
}
