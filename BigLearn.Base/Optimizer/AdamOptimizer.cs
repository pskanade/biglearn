﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace BigLearn
{
    /// <summary>
    /// Rf ADAM: A METHOD FOR STOCHASTIC OPTIMIZATION, Kingma et al., ICLR 2015
    /// </summary>
    public class AdamOptimizer : GradientOptimizer
    {
        float UpdateClip = 0;
        float WeightClip = 0;

        /// <summary>
        /// Default: 1e-2
        /// </summary>

        private float Beta2 { get; set; }

        private float Epsilon { get; set; }

        private float Beta1 { get; set; }

        private CudaPieceFloat M = null;

        private CudaPieceFloat V = null;


        public int Count { get; set; }

        public bool IsBugOptimize = false;

        public AdamOptimizer(int weightSize, float beta1, float beta2, float epsilon, float clipDelta, float clipWeight, RunnerBehavior behavior) : base(behavior)
        {
            UpdateClip = clipDelta;
            WeightClip = clipWeight;
            
            Beta1 = beta1;
            Beta2 = beta2;
            Epsilon = epsilon;
            Count = 0;
            
            M = new CudaPieceFloat(weightSize, true, Behavior.Device == DeviceType.GPU);
            M.Zero();
            V = new CudaPieceFloat(weightSize, true, Behavior.Device == DeviceType.GPU);
            V.Zero();
        }


        //public override void Init(CudaPieceFloat weight, StructureLearner learner)
        //{
        //    UpdateClip = learner.ClipDelta;
        //    WeightClip = learner.ClipWeight;
        //    M = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    M.Zero();
        //    V = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    V.Zero();
        //    Parameter = weight;
        //    UpdateRate = learner.LearnRate;

        //    Gradient = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    Gradient.Zero();
        //    Beta1 = learner.Adam_Beta1;
        //    Beta2 = learner.Adam_Beta2;
        //    Epsilon = learner.Adam_Epsilon;
        //    Count = 0;
        //    GradientStep = 1;
        //    Aggregator = learner.GradientAggFactory == null ? null : learner.GradientAggFactory.CreateAggregator(weight.Size);
        //}

        public override void BeforeGradient()
        {
            //Count += 1;
        }

        public override void AfterGradient()
        {
            Count += 1;
            ComputeLib.ClipAdamUpdate(M, V, Gradient, Parameter, this.Beta1, this.Beta2, this.Count, this.UpdateRate, this.UpdateClip, this.WeightClip);
            if(IsBugOptimize)
            {
                Count += 1;
                ComputeLib.ClipAdamUpdate(M, V, Gradient, Parameter, this.Beta1, this.Beta2, this.Count, this.UpdateRate, this.UpdateClip, this.WeightClip);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (M != null)
            {
                M.Dispose(); M = null;
            }
            
            if (V != null) 
            {
                V.Dispose(); V = null;
            }

            if (Gradient != null)
            {
                Gradient.Dispose();
                Gradient = null;
            }
        }

        public override void Reset()
        {
            //Console.WriteLine("Adam Optimizer reset!!");
            Console.WriteLine("Warning! Adam has been reset!!!!!!!!!!!!!!!!!!!!");
            ComputeLib.Zero(M, M.EffectiveSize);
            ComputeLib.Zero(V, V.EffectiveSize);
            Count = 0;
        }
    }
}
