﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    
    public class SparseSGDOptimizer : GradientOptimizer
    {
        VecNormType Norm = VecNormType.NoNorm;
        
        public SparseSGDOptimizer(CudaPieceFloat weight, float learnRate, VecNormType norm, RunnerBehavior behavior) : base(behavior)
        {
            UpdateRate = learnRate;
            Parameter = weight;
            Gradient = new CudaPieceFloat(weight.Size, true, Behavior.Device == DeviceType.GPU);
            Norm = norm;
        }

        public override void BeforeGradient()
        { }

        public unsafe override void AfterGradient()
        {
            //ComputeLib.Add_Vector(Parameter, Gradient, Gradient.Size, 1, UpdateRate);
            Gradient.SyncToCPU();
            Parameter.SyncToCPU();

            //Parallel.ForEach(SparseGradientDict, gIdx =>
            foreach (KeyValuePair<int, int> gIdx in SparseGradientDict)
            {
                fixed (float* g = &Gradient.MemPtr[gIdx.Key])
                fixed (float* p = &Parameter.MemPtr[gIdx.Key])
                {
                    SSElib.SSE_AddScale(UpdateRate, g, p, gIdx.Value);

                    if (Norm == VecNormType.L2Norm || Norm == VecNormType.L2BallNorm)
                    {
                        double x = Math.Sqrt(SSElib.SSE_SumSq(p, gIdx.Value));
                        if (x > 1 || Norm == VecNormType.L2Norm)
                        {
                            SSElib.SSE_Scale((float)(1.0 / x), g, gIdx.Value);
                        }
                    }
                }
                //g.ZeroItems();
            }
            //);
            Parameter.SyncFromCPU();
            ClearSparseGradient();
            ComputeLib.Zero(Gradient, Gradient.Size);
        }
    }
}
