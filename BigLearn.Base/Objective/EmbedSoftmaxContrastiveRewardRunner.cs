﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class EmbedSoftmaxContrastiveRewardRunner : ObjectiveRunner
    {
        public new EmbedStructure Model { get { return (EmbedStructure)base.Model; } set { base.Model = value; } }
        new SeqDenseBatchData[] Input { get; set; }
        HiddenBatchData[] TreeProb { get; set; }
        CudaPieceFloat Label { get; set; }
        CudaPieceInt LabelMask = null;
        CudaPieceInt InputMask = null;
        public float[] Post;


        CudaPieceFloat SmpProb;

        //new CudaPieceFloat Output;
        CudaPieceFloat[] Deriv;
        CudaPieceFloat[] DerivWei;

        float Gamma;
        bool IsDeriv = true;
        CudaPieceFloat biasOne;

        string OutputFilePath = string.Empty;
        StreamWriter dumpWriter;
        int batchNum = 0;

        public EmbedSoftmaxContrastiveRewardRunner(EmbedStructure model, SeqDenseBatchData[] input, CudaPieceInt inputMask, CudaPieceInt labelMask, HiddenBatchData[] treeprob, CudaPieceFloat label, float gamma,
            RunnerBehavior behavior, bool isDeriv = true)
            : base(model, behavior)
        {
            Input = input;
            LabelMask = labelMask;
            InputMask = inputMask;
            TreeProb = treeprob;
            Label = label;
            Gamma = gamma;

            biasOne = new CudaPieceFloat(Input[0].Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);
            biasOne.Init(1);

            Post = new float[Input[0].MAX_BATCHSIZE * Input.Length];
            SmpProb = new CudaPieceFloat(Input[0].Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);


            Deriv = new CudaPieceFloat[Input.Length];
            DerivWei = new CudaPieceFloat[Input.Length];
            for (int m = 0; m < Input.Length; m++)
            {
                Deriv[m] = new CudaPieceFloat(Input[0].Stat.MAX_SEQUENCESIZE * Model.VocabSize, true, Behavior.Device == DeviceType.GPU);
                DerivWei[m] = new CudaPieceFloat(Input[0].Stat.MAX_SEQUENCESIZE, true, Behavior.Device == DeviceType.GPU);
            }

            //Output = new CudaPieceFloat(Input[0].Stat.MAX_SEQUENCESIZE * Model.VocabSize, true, Behavior.Device == DeviceType.GPU);
            
            IsDeriv = isDeriv;
        }

        public EmbedSoftmaxContrastiveRewardRunner(EmbedStructure model, SeqDenseBatchData[] input, CudaPieceInt inputMask, CudaPieceInt labelMask, HiddenBatchData[] treeprob, CudaPieceFloat label, float gamma,
            string outputFile, RunnerBehavior behavior) : this(model, input, inputMask, labelMask, treeprob, label, gamma, behavior, false)
        {
            OutputFilePath = outputFile;
        }

        public override void Init()
        {
            if (OutputFilePath != string.Empty)
            {
                dumpWriter = new StreamWriter(OutputFilePath);
                batchNum = 0;
                ObjectiveScore = 0;
            }
        }

        public override void Complete()
        {
            if (OutputFilePath != string.Empty)
            {
                dumpWriter.Close();
                ObjectiveScore = ObjectiveScore / (batchNum + float.Epsilon);
                Logger.WriteLog("PPL per miniBatch {0}", ObjectiveScore);
            }
        }

        public override void Forward()
        {
            ObjectiveScore = 0;
            
            int BatchSize = Input[0].BatchSize;
            int SentSize = Input[0].SentSize;
            Input[0].SampleIdx.SyncToCPU(BatchSize);
            Label.SyncToCPU(SentSize);
            InputMask.SyncToCPU(SentSize);

            double wordPerPlexity = 0;

            for (int d = 0; d < Input.Length; d++)
            {
                ComputeLib.Sgemm(Input[d].SentOutput, 0, Model.Embedding, 0, Deriv[d], 0, Input[d].SentSize, Model.Dim, Model.VocabSize, 0, 1, false, true);
                if (Model.IsBias) ComputeLib.Matrix_Add_Linear(Deriv[d], Model.Bias, Input[d].SentSize, Model.VocabSize);
                ComputeLib.SoftMax(Deriv[d], Deriv[d], Model.VocabSize, Input[d].SentSize, Gamma);

                /////HelpInput.LagForward
                float batchprob = ComputeLib.LogLossDeriv(Deriv[d], Model.VocabSize, Input[d].SentSize, SmpProb, Label,
                    CudaPieceFloat.Empty, LabelMask, Gamma * 1.0f / BatchSize, Util.GPUEpsilon);

                // // * 1.0f / Input[0].BatchSize

                float miniBatchLoss = 0;
                
                TreeProb[d].Output.Data.SyncToCPU(Input[d].BatchSize);
                
                SmpProb.SyncToCPU(Input[d].SentSize);

                for (int i = 0; i < BatchSize; i++)
                {
                    int bgn = i == 0 ? 0 : Input[0].SampleIdx.MemPtr[i - 1];
                    int end = Input[0].SampleIdx.MemPtr[i];
                    double logP = 0;
                    double log2P = 0;
                    for (int k = bgn; k < end; k++)
                    {
                        double loss = SmpProb.MemPtr[k];
                        logP += Math.Log(Math.Pow(2, loss));
                        log2P += -loss;
                    }
                    Post[i * Input.Length + d] = (float)(logP + Math.Log(TreeProb[d].Output.Data.MemPtr[i]));

                    wordPerPlexity += TreeProb[d].Output.Data.MemPtr[i] * log2P;
                    miniBatchLoss += (float)Math.Exp(Post[i * Input.Length + d]);
                }
                ObjectiveScore += miniBatchLoss / BatchSize;
            }
            wordPerPlexity = wordPerPlexity / SentSize;
            ObjectiveScore = wordPerPlexity;

            if (IsDeriv)
            {
                for (int b = 0; b < BatchSize; b++)
                {
                    float logsum = 0;
                    logsum = Post[b * Input.Length + 0];
                    for (int t = 1; t < Input.Length; t++) logsum = Util.LogAdd(logsum, Post[b * Input.Length + t]);

                    int bgn = b == 0 ? 0 : Input[0].SampleIdx.MemPtr[b - 1];
                    int end = Input[0].SampleIdx.MemPtr[b];

                    for (int d = 0; d < Input.Length; d++)
                    {
                        float posterior = (float)Math.Exp(Post[b * Input.Length + d] - logsum);
                        for (int k = bgn; k < end; k++)
                        {
                            int newP = InputMask.MemPtr[k];
                            DerivWei[d].MemPtr[newP] = posterior;
                        }
                        TreeProb[d].Deriv.Data.MemPtr[b] = (float)(posterior - TreeProb[d].Output.Data.MemPtr[b]) * 1.0f / BatchSize;
                    }
                }

                //if (!IsDelegateOptimizer)
                //{
                //    Model.EmbeddingOptimizer.BeforeGradient();
                //    if (Model.IsBias) Model.BiasOptimizer.BeforeGradient();
                //}

                for (int d = 0; d < Input.Length; d++)
                {
                    TreeProb[d].Deriv.Data.SyncFromCPU(BatchSize);

                    DerivWei[d].SyncFromCPU(Input[0].SentSize);
                    ComputeLib.Scale_MatrixMask(Deriv[d], 0, CudaPieceInt.Empty, 0, Deriv[d], 0, CudaPieceInt.Empty, 0, Model.VocabSize, Input[0].SentSize, DerivWei[d], 0);

                    ComputeLib.Sgemm(Deriv[d], 0, Model.Embedding, 0, Input[d].SentDeriv, 0, Input[d].SentSize, Model.VocabSize, Model.Dim, 0, 1, false, false);

                    /// update vocab embedding.
                    ComputeLib.Sgemm(Deriv[d], 0, Input[d].SentOutput, 0, Model.EmbeddingGrad, 0, Input[d].SentSize, Model.VocabSize, Model.Dim, 1, 1, true, false);
                    
                    if (Model.IsBias)
                    {
                        ComputeLib.Sgemv(Deriv[d], biasOne, Input[d].SentSize, Model.VocabSize, Model.BiasGrad, true, 1, 1);
                    }
                }
                //if (!IsDelegateOptimizer)
                //{
                //    Model.EmbeddingOptimizer.AfterGradient();
                //    if (Model.IsBias) Model.BiasOptimizer.AfterGradient();
                //}

            }
        }
    }

}
