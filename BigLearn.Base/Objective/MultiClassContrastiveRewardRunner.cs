﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{

    /// <summary>
    /// Right Version.
    /// </summary>
    public class MultiClassContrastiveRewardRunner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public HiddenBatchData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;

        CudaPieceFloat tmpOutput;
        CudaPieceFloat SmpProb;
        CudaPieceFloat[] SmpWei;

        public float Gamma { get; set; }

        /// <summary>
        /// label could be null, if the all the positives are at the first position.  
        /// </summary>
        /// <param name="label"></param>
        /// <param name="data"></param>
        /// <param name="prob"></param>
        /// <param name="gamma"></param>
        /// <param name="behavior"></param>
        public MultiClassContrastiveRewardRunner(CudaPieceFloat label, HiddenBatchData[] data, HiddenBatchData[] prob, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Data = data;
            Prob = prob;
            Label = label;
            Gamma = gamma;
            Post = new float[Data[0].MAX_BATCHSIZE * Data.Length];

            tmpOutput = new CudaPieceFloat(Data[0].MAX_BATCHSIZE * Data[0].Dim, Behavior.Device);
            SmpProb = new CudaPieceFloat(Data[0].MAX_BATCHSIZE, Behavior.Device);

            SmpWei = new CudaPieceFloat[Data.Length];
            for (int m = 0; m < Data.Length; m++) { SmpWei[m] = new CudaPieceFloat(Data[0].MAX_BATCHSIZE, Behavior.Device); }
        }

        public override void Forward()
        {
            int BatchSize = Data[0].BatchSize;
            if (Label != null && !Label.IsEmpty) Label.SyncToCPU(BatchSize);
            ObjectiveScore = 0;

            double wordPerPlexity = 0;

            for (int d = 0; d < Data.Length; d++)
            {
                ComputeLib.Add_Vector(tmpOutput, Data[d].Output.Data, BatchSize * Data[d].Dim, 0, Gamma);

                ComputeLib.SoftMax(tmpOutput, Data[d].Deriv.Data, Data[d].Dim, BatchSize, 1);

                ComputeLib.LogLossDeriv(Data[d].Deriv.Data, Data[d].Dim, BatchSize, SmpProb, Label, CudaPieceFloat.Empty, CudaPieceInt.Empty, Gamma * 1.0f / BatchSize, Util.GPUEpsilon);

                //Data[d].Deriv.Data.SyncToCPU(BatchSize * Data[d].Dim);
                Prob[d].Output.Data.SyncToCPU(Data[d].BatchSize);
                SmpProb.SyncToCPU(Data[d].BatchSize);

                double miniBatchLoss = 0;
                for (int i = 0; i < BatchSize; i++)
                {
                    double loss = SmpProb.MemPtr[i];
                    double logP = Math.Log(Math.Pow(2, loss));

                    //int l = Label == null ? 0 : (int)Label.MemPtr[i];

                    //double loss = Math.Log(Math.Max(Data[d].Deriv.Data.MemPtr[i * Data[d].Dim + l], float.Epsilon));
                    //double log2P = Math.Log(Math.Max(Data[d].Deriv.Data.MemPtr[i * Data[d].Dim + l], float.Epsilon), 2);

                    Post[i * Data.Length + d] = (float)(logP + Math.Log(Prob[d].Output.Data.MemPtr[i]));
                    miniBatchLoss += (float)Math.Exp(Post[i * Data.Length + d]);
                    wordPerPlexity += Prob[d].Output.Data.MemPtr[i] * loss;
                }
                ObjectiveScore += miniBatchLoss / BatchSize; 
            }
            wordPerPlexity = wordPerPlexity / BatchSize;
            ObjectiveScore = wordPerPlexity;

            for (int b = 0; b < BatchSize; b++)
            {
                float logsum = 0;
                logsum = Post[b * Data.Length + 0];
                for (int d = 1; d < Data.Length; d++) logsum = Util.LogAdd(logsum, Post[b * Data.Length + d]);

                int l = Label == null || Label.IsEmpty ? 0 : (int)Label.MemPtr[b];

                for (int d = 0; d < Data.Length; d++)
                {
                    float posterior = (float)Math.Exp(Post[b * Data.Length + d] - logsum);
                    Prob[d].Deriv.Data.MemPtr[b] = (float)(posterior - Prob[d].Output.Data.MemPtr[b]) * 1.0f / BatchSize;
                    SmpWei[d].MemPtr[b] = posterior;
                    //Data[d].Deriv.Data.MemPtr[b * Data[d].Dim + l] -= 1;
                    //for (int dim = 0; dim < Data[d].Dim; dim++)
                    //{
                    //    Data[d].Deriv.Data.MemPtr[b * Data[d].Dim + dim] = -Gamma * posterior * Data[d].Deriv.Data.MemPtr[b * Data[d].Dim + dim] * 1.0f / BatchSize;
                    //}
                }
            }

            for (int d = 0; d < Data.Length; d++)
            {
                SmpWei[d].SyncFromCPU(BatchSize);
                ComputeLib.Scale_MatrixMask(Data[d].Deriv.Data, 0, CudaPieceInt.Empty, 0, Data[d].Deriv.Data, 0, CudaPieceInt.Empty, 0, Data[d].Dim, BatchSize, SmpWei[d], 0);

                //Data[d].Deriv.Data.SyncFromCPU(BatchSize * Data[d].Dim);
                Prob[d].Deriv.Data.SyncFromCPU(BatchSize);
            }

        }
    }


    public class MultiOutputClassContrastiveRewardRunner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public HiddenBatchData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;

        public CudaPieceFloat tmpOutput;

        public float Gamma { get; set; }

        int OutputNum { get; set; }
        int ClassNum { get; set; }
        /// <summary>
        /// label could be null, if the all the positives are at the first position.  
        /// </summary>
        /// <param name="label"></param>
        /// <param name="data"></param>
        /// <param name="prob"></param>
        /// <param name="gamma"></param>
        /// <param name="behavior"></param>
        public MultiOutputClassContrastiveRewardRunner(CudaPieceFloat label, HiddenBatchData[] data, HiddenBatchData[] prob, int outputNum, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Data = data;
            Prob = prob;
            Label = label;
            Gamma = gamma;
            Post = new float[Data[0].MAX_BATCHSIZE * Data.Length];
            OutputNum = outputNum;
            ClassNum = Data[0].Dim / outputNum;

            tmpOutput = new CudaPieceFloat(Data[0].MAX_BATCHSIZE * Data[0].Dim, true, Behavior.Device == DeviceType.GPU);
        }

        public override void Forward()
        {
            int BatchSize = Data[0].BatchSize;
            if (Label != null) Label.SyncToCPU(BatchSize * OutputNum);
            ObjectiveScore = 0;

            for (int d = 0; d < Data.Length; d++)
            {
                ComputeLib.Add_Vector(tmpOutput, Data[d].Output.Data, BatchSize * Data[d].Dim, 0, Gamma);

                ComputeLib.SoftMax(tmpOutput, Data[d].Deriv.Data, ClassNum, OutputNum * BatchSize, 1);

                Data[d].Deriv.Data.SyncToCPU(BatchSize * Data[d].Dim);
                Prob[d].Output.Data.SyncToCPU(Data[d].BatchSize);

                double miniBatchLoss = 0;
                for (int i = 0; i < BatchSize; i++)
                {
                    double logP = 0;
                    for (int k = 0; k < OutputNum; k++)
                    {
                        int l = Label == null ? 0 : (int)Label.MemPtr[i * OutputNum + k];
                        double loss = Math.Log(Math.Max(Data[d].Deriv.Data.MemPtr[i * Data[d].Dim + k * ClassNum + l], float.Epsilon));
                        logP += loss;
                    }
                    Post[i * Data.Length + d] = (float)(logP + Math.Log(Prob[d].Output.Data.MemPtr[i]));

                    miniBatchLoss += (float)Math.Exp(Post[i * Data.Length + d]);
                }

                ObjectiveScore += miniBatchLoss / BatchSize;
            }

            for (int b = 0; b < BatchSize; b++)
            {
                float logsum = 0;
                logsum = Post[b * Data.Length + 0];
                for (int d = 1; d < Data.Length; d++) logsum = Util.LogAdd(logsum, Post[b * Data.Length + d]);

                for (int k = 0; k < OutputNum; k++)
                {
                    int l = Label == null ? 0 : (int)Label.MemPtr[b * OutputNum + k];

                    for (int d = 0; d < Data.Length; d++)
                    {
                        float posterior = (float)Math.Exp(Post[b * Data.Length + d] - logsum);

                        Data[d].Deriv.Data.MemPtr[b * Data[d].Dim + k * ClassNum + l] -= 1;
                        for (int dim = 0; dim < ClassNum; dim++)
                        {
                            Data[d].Deriv.Data.MemPtr[b * Data[d].Dim + k * ClassNum + dim] = -Gamma * posterior * Data[d].Deriv.Data.MemPtr[b * Data[d].Dim + k * ClassNum + dim] * 1.0f / BatchSize;
                        }
                        Prob[d].Deriv.Data.MemPtr[b] = (float)(posterior - Prob[d].Output.Data.MemPtr[b]) * 1.0f / BatchSize;
                    }
                }
            }

            for (int d = 0; d < Data.Length; d++)
            {
                Data[d].Deriv.Data.SyncFromCPU(BatchSize * Data[d].Dim);
                Prob[d].Deriv.Data.SyncFromCPU(BatchSize);
            }

        }
    }
}
