﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class BayesianContrastiveRewardRunner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public BiMatchBatchData MatchData { get; set; }
        public float Gamma { get; set; }

        public HiddenBatchData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;


        public CudaPieceFloat[] SrcBatchLoss;

        public BayesianContrastiveRewardRunner(CudaPieceFloat label, HiddenBatchData[] data, HiddenBatchData[] prob,
            BiMatchBatchData matchData, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Label = label;
            MatchData = matchData;
            Gamma = gamma;

            Data = data;
            Prob = prob;

            Post = new float[MatchData.Stat.MAX_SRC_BATCHSIZE * Data.Length];
            SrcBatchLoss = new CudaPieceFloat[Data.Length];
            for (int d = 0; d < Data.Length; d++)
            {
                SrcBatchLoss[d] = new CudaPieceFloat(MatchData.Stat.MAX_SRC_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
            }
        }

        public override void Forward()
        {
            if (MatchData.SrcSize == 0) { ObjectiveScore = 0; return; }

            Label.SyncToCPU(MatchData.MatchSize);
            ObjectiveScore = 0;
            MatchData.Src2MatchIdx.SyncToCPU(MatchData.SrcSize);
            for (int d = 0; d < Data.Length; d++)
            {
                Data[d].Output.Data.SyncToCPU();
                ComputeLib.BayesianRatingProb(MatchData.Src2MatchIdx, MatchData.Src2MatchElement, MatchData.SrcSize,
                                      SrcBatchLoss[d], Data[d].Output.Data, Label, Data[d].Deriv.Data, MatchData.MatchSize, Util.GPUEpsilon, Gamma);

                Prob[d].Output.Data.SyncToCPU(MatchData.SrcSize);
                SrcBatchLoss[d].SyncToCPU(MatchData.SrcSize);

                Data[d].Deriv.Data.SyncToCPU(MatchData.MatchSize);
                
                double miniBatchLoss = 0;
                for (int b = 0; b < MatchData.SrcSize; b++)
                {
                    float loss = SrcBatchLoss[d].MemPtr[b];
                    
                    Post[b * Data.Length + d] = (float)Math.Exp(loss) * Prob[d].Output.Data.MemPtr[b];
                    miniBatchLoss += Post[b * Data.Length + d];
                }
                ObjectiveScore += miniBatchLoss / MatchData.SrcSize;
            }

            for (int b = 0; b < MatchData.SrcSize; b++)
            {
                float sum = 0; // float.Epsilon;
                for (int d = 0; d < Data.Length; d++) sum += Post[b * Data.Length + d];
                for (int d = 0; d < Data.Length; d++)
                {
                    float loss = SrcBatchLoss[d].MemPtr[b];
                    float posterior = Post[b * Data.Length + d] / sum; 
                    Prob[d].Deriv.Data.MemPtr[b] = (posterior - Prob[d].Output.Data.MemPtr[b]) * 1.0f / MatchData.SrcSize;

                    int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                    int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];
                    for (int m = matchBgn; m < matchEnd; m++)
                    {
                        float pm = Prob[d].Output.Data.MemPtr[b] * Data[d].Deriv.Data.MemPtr[m];
                        if (Label.MemPtr[m] > 0)
                            Data[d].Deriv.Data.MemPtr[m] = (float)(Gamma * pm / sum * (1 - Math.Exp(loss))) / MatchData.SrcSize;
                        else
                            Data[d].Deriv.Data.MemPtr[m] = (float)(Gamma * pm / sum * (-Math.Exp(loss))) / MatchData.SrcSize;

                        //float pm = Prob[d].Output.Data.MemPtr[b] * (float)Math.Exp(loss); // Data[d].Deriv.Data.MemPtr[m];
                        //if (Label.MemPtr[m] > 0)
                        //    Data[d].Deriv.Data.MemPtr[m] = (float)(Gamma * pm / sum * (1 - Data[d].Deriv.Data.MemPtr[m])) / MatchData.SrcSize;
                        //else
                        //    Data[d].Deriv.Data.MemPtr[m] = (float)(Gamma * pm / sum * (-Data[d].Deriv.Data.MemPtr[m])) / MatchData.SrcSize;
                    }
                }
            }

            for (int d = 0; d < Data.Length; d++)
            {
                Prob[d].Deriv.Data.SyncFromCPU(MatchData.SrcSize);
                Data[d].Deriv.Data.SyncFromCPU(MatchData.MatchSize);
            }
        }
    }

    public class BayesianContrastiveRewardV2Runner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public float Gamma { get; set; }

        public SeqVectorData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;

        public CudaPieceFloat[] SrcBatchLoss;

        public BayesianContrastiveRewardV2Runner(CudaPieceFloat label, SeqVectorData[] data, HiddenBatchData[] prob, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Label = label;
            Gamma = gamma;

            Data = data;
            Prob = prob;

            Post = new float[Data.Last().MaxSegment * Data.Length];
            SrcBatchLoss = new CudaPieceFloat[Data.Length];
            for (int d = 0; d < Data.Length; d++)
            {
                SrcBatchLoss[d] = new CudaPieceFloat(Data.Last().MaxSegment, true, behavior.Device == DeviceType.GPU);
            }
        }

        public override void Forward()
        {
            if (Label.EffectiveSize == 0) { ObjectiveScore = 0; return; }

            Label.SyncToCPU();
            ObjectiveScore = 0;
            Data.Last().SegmentIdx.SyncToCPU(Data.Last().Segment);
            int[] SegIdx = Data.Last().SegmentIdx.MemPtr;

            for (int d = 0; d < Data.Length; d++)
            {
                ComputeLib.BayesianRatingProb(Data[d].SegmentIdx, CudaPieceInt.Empty, Data[d].Segment,
                                      SrcBatchLoss[d], Data[d].Output, Label, Data[d].Deriv, Data[d].Length, Util.GPUEpsilon, Gamma);

                //Data[d].Output.Data.SyncToCPU();
                Prob[d].Output.Data.SyncToCPU(Prob[d].BatchSize);
                SrcBatchLoss[d].SyncToCPU(Prob[d].BatchSize);
                Data[d].Deriv.SyncToCPU(Data[d].Length);

                double miniBatchLoss = 0;
                for (int b = 0; b < Prob[d].BatchSize; b++)
                {
                    float loss = SrcBatchLoss[d].MemPtr[b];

                    Post[b * Data.Length + d] = (float)Math.Exp(loss) * Prob[d].Output.Data.MemPtr[b];
                    miniBatchLoss += Post[b * Data.Length + d];
                }
                ObjectiveScore += miniBatchLoss / Prob[d].BatchSize;
            }

            for (int b = 0; b < Data.Last().Segment; b++)
            {
                float sum = 0; // float.Epsilon;
                for (int d = 0; d < Data.Length; d++) sum += Post[b * Data.Length + d];
                for (int d = 0; d < Data.Length; d++)
                {
                    float loss = SrcBatchLoss[d].MemPtr[b];
                    float posterior = Post[b * Data.Length + d] / sum;
                    Prob[d].Deriv.Data.MemPtr[b] = (posterior - Prob[d].Output.Data.MemPtr[b]) * 1.0f / Prob[d].BatchSize;

                    int matchBgn = b == 0 ? 0 : SegIdx[b - 1];
                    int matchEnd = SegIdx[b];
                    for (int m = matchBgn; m < matchEnd; m++)
                    {
                        float pm = Prob[d].Output.Data.MemPtr[b] * Data[d].Deriv.MemPtr[m];
                        if (Label.MemPtr[m] > 0)
                            Data[d].Deriv.MemPtr[m] = (float)(Gamma * pm / sum * (1 - Math.Exp(loss))) / Prob[d].BatchSize;
                        else
                            Data[d].Deriv.MemPtr[m] = (float)(Gamma * pm / sum * (-Math.Exp(loss))) / Prob[d].BatchSize;

                    }
                }
            }

            for (int d = 0; d < Data.Length; d++)
            {
                Prob[d].Deriv.Data.SyncFromCPU(Prob[d].BatchSize);
                Data[d].Deriv.SyncFromCPU(Data[d].Length);
            }
        }
    }



    public class BayesianContrastivePairRewardRunner : ObjectiveRunner
    {
        public CudaPieceFloat LabelStart { get; set; }
        public CudaPieceFloat LabelEnd { get; set; }

        public BiMatchBatchData MatchData { get; set; }
        public float Gamma { get; set; }

        public HiddenBatchData[] DataStart;
        public HiddenBatchData[] DataEnd;

        public HiddenBatchData[] Prob;
        public float[] Post;


        public CudaPieceFloat[] SrcBatchLoss_Start;
        public CudaPieceFloat[] SrcBatchLoss_End;

        public BayesianContrastivePairRewardRunner(CudaPieceFloat labelStart, CudaPieceFloat labelEnd, HiddenBatchData[] dataStart, HiddenBatchData[] dataEnd, HiddenBatchData[] prob,
            BiMatchBatchData matchData, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            LabelStart = labelStart;
            LabelEnd = labelEnd;
            MatchData = matchData;
            Gamma = gamma;

            DataStart = dataStart;
            DataEnd = dataEnd;
            Prob = prob;

            Post = new float[MatchData.Stat.MAX_SRC_BATCHSIZE * DataStart.Length];

            SrcBatchLoss_Start = new CudaPieceFloat[DataStart.Length];
            SrcBatchLoss_End = new CudaPieceFloat[DataEnd.Length];

            for (int d = 0; d < DataStart.Length; d++)
            {
                SrcBatchLoss_Start[d] = new CudaPieceFloat(MatchData.Stat.MAX_SRC_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
            }
            for (int d = 0; d < DataEnd.Length; d++)
            {
                SrcBatchLoss_End[d] = new CudaPieceFloat(MatchData.Stat.MAX_SRC_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
            }
        }

        public override void Forward()
        {
            if (MatchData.SrcSize == 0) { ObjectiveScore = 0; return; }

            LabelStart.SyncToCPU(MatchData.MatchSize);
            LabelEnd.SyncToCPU(MatchData.MatchSize);

            ObjectiveScore = 0;

            for (int d = 0; d < DataStart.Length; d++)
            {
                ComputeLib.BayesianRatingProb(MatchData.Src2MatchIdx, MatchData.Src2MatchElement, MatchData.SrcSize,
                                      SrcBatchLoss_Start[d], DataStart[d].Output.Data, LabelStart, DataStart[d].Deriv.Data, 
                                      MatchData.MatchSize, Util.GPUEpsilon, Gamma);

                ComputeLib.BayesianRatingProb(MatchData.Src2MatchIdx, MatchData.Src2MatchElement, MatchData.SrcSize,
                                      SrcBatchLoss_End[d], DataEnd[d].Output.Data, LabelEnd, DataEnd[d].Deriv.Data,
                                      MatchData.MatchSize, Util.GPUEpsilon, Gamma);

                Prob[d].Output.Data.SyncToCPU(MatchData.SrcSize);
                DataStart[d].Deriv.Data.SyncToCPU(MatchData.MatchSize);
                DataEnd[d].Deriv.Data.SyncToCPU(MatchData.MatchSize);
                
                SrcBatchLoss_Start[d].SyncToCPU(MatchData.SrcSize);
                SrcBatchLoss_End[d].SyncToCPU(MatchData.SrcSize);

                double miniBatchLoss = 0;
                for (int b = 0; b < MatchData.SrcSize; b++)
                {
                    float loss1 = SrcBatchLoss_Start[d].MemPtr[b];
                    float loss2 = SrcBatchLoss_End[d].MemPtr[b];

                    //int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                    //int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];
                    //for (int m = matchBgn; m < matchEnd; m++)
                    //{
                    //    Data[d].Deriv.Data.MemPtr[m] = Gamma *  Data[d].Deriv.Data.MemPtr[m] * Prob[d].Output.Data.MemPtr[b] / MatchData.SrcSize;
                    //}
                    Post[b * DataStart.Length + d] = (float)(Math.Exp(loss1) * Math.Exp(loss2) * Prob[d].Output.Data.MemPtr[b]);

                    miniBatchLoss += Post[b * DataStart.Length + d]; // Prob[d].Output.Data.MemPtr[b];
                }
                ObjectiveScore += miniBatchLoss / MatchData.SrcSize;
            }

            //Logger.WriteLog("MiniBatch {0}, Loss {1}, BatchSize {2}, Word Number {3}", iteration, ObjectiveScore, MatchData.SrcSize, MatchData.MatchSize);

            iteration++;
            //if (double.IsNaN(ObjectiveScore ))
            //{
            //    Console.WriteLine("Nan at minibatch " + iteration.ToString());
            //    Console.ReadLine();
            //}
            for (int b = 0; b < MatchData.SrcSize; b++)
            {
                float sum = 0; 
                for (int d = 0; d < DataStart.Length; d++) sum += Post[b * DataStart.Length + d];

                for (int d = 0; d < DataStart.Length; d++)
                {
                    float loss1 = SrcBatchLoss_Start[d].MemPtr[b];
                    float loss2 = SrcBatchLoss_End[d].MemPtr[b];

                    float posterior = Post[b * DataStart.Length + d] / sum;
                    Prob[d].Deriv.Data.MemPtr[b] = (posterior - Prob[d].Output.Data.MemPtr[b]) * 1.0f / MatchData.SrcSize;

                    float pm = Post[b * DataStart.Length + d]; // (float)(Prob[d].Output.Data.MemPtr[b] * Math.Exp(loss1) * Math.Exp(loss2));
                        //DataStart[d].Deriv.Data.MemPtr[m] * DataEnd[d].Deriv.Data.MemPtr[m];

                    int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                    int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];
                    for (int m = matchBgn; m < matchEnd; m++)
                    {
                        if (LabelStart.MemPtr[m] > 0)
                        {
                            DataStart[d].Deriv.Data.MemPtr[m] = (float)(Gamma * posterior * (1 - DataStart[d].Deriv.Data.MemPtr[m])) / MatchData.SrcSize;
                        }
                        else
                        {
                            DataStart[d].Deriv.Data.MemPtr[m] = (float)(Gamma * posterior * (-DataStart[d].Deriv.Data.MemPtr[m])) / MatchData.SrcSize;
                        }

                        if (LabelEnd.MemPtr[m] > 0)
                        {
                            DataEnd[d].Deriv.Data.MemPtr[m] = (float)(Gamma * posterior * (1 - DataEnd[d].Deriv.Data.MemPtr[m])) / MatchData.SrcSize;
                        }
                        else
                        {
                            DataEnd[d].Deriv.Data.MemPtr[m] = (float)(Gamma * posterior * (-DataEnd[d].Deriv.Data.MemPtr[m])) / MatchData.SrcSize;
                        }
                    }
                }
            }

            for (int d = 0; d < DataStart.Length; d++)
            {
                Prob[d].Deriv.Data.SyncFromCPU(MatchData.SrcSize);
                DataStart[d].Deriv.Data.SyncFromCPU(MatchData.MatchSize);
                DataEnd[d].Deriv.Data.SyncFromCPU(MatchData.MatchSize);
            }
        }
    }


    public class BayesianPairRunner : ObjectiveRunner
    {
        public CudaPieceFloat LabelStart { get; set; }
        public CudaPieceFloat LabelEnd { get; set; }

        public BiMatchBatchData MatchData { get; set; }
        public float Gamma { get; set; }

        public HiddenBatchData DataStart;
        public HiddenBatchData DataEnd;

        public CudaPieceFloat SrcBatchLoss_Start;
        public CudaPieceFloat SrcBatchLoss_End;
        public BayesianPairRunner(CudaPieceFloat labelStart, CudaPieceFloat labelEnd, HiddenBatchData dataStart, HiddenBatchData dataEnd, 
            BiMatchBatchData matchData, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            LabelStart = labelStart;
            LabelEnd = labelEnd;
            MatchData = matchData;
            Gamma = gamma;

            DataStart = dataStart;
            DataEnd = dataEnd;

            SrcBatchLoss_Start = new CudaPieceFloat(MatchData.Stat.MAX_SRC_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
            SrcBatchLoss_End = new CudaPieceFloat(MatchData.Stat.MAX_SRC_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
        }

        public override void Forward()
        {
            if (MatchData.SrcSize == 0) { ObjectiveScore = 0; return; }

            LabelStart.SyncToCPU(MatchData.MatchSize);
            LabelEnd.SyncToCPU(MatchData.MatchSize);

            ObjectiveScore = 0;

            ComputeLib.BayesianRatingProb(MatchData.Src2MatchIdx, MatchData.Src2MatchElement, MatchData.SrcSize,
                                  SrcBatchLoss_Start, DataStart.Output.Data, LabelStart, DataStart.Deriv.Data,
                                  MatchData.MatchSize, Util.GPUEpsilon, Gamma);

            ComputeLib.BayesianRatingProb(MatchData.Src2MatchIdx, MatchData.Src2MatchElement, MatchData.SrcSize,
                                  SrcBatchLoss_End, DataEnd.Output.Data, LabelEnd, DataEnd.Deriv.Data,
                                  MatchData.MatchSize, Util.GPUEpsilon, Gamma);

            DataStart.Deriv.Data.SyncToCPU(MatchData.MatchSize);
            DataEnd.Deriv.Data.SyncToCPU(MatchData.MatchSize);

            SrcBatchLoss_Start.SyncToCPU(MatchData.SrcSize);
            SrcBatchLoss_End.SyncToCPU(MatchData.SrcSize);

            double miniBatchLoss = 0;
            for (int b = 0; b < MatchData.SrcSize; b++)
            {
                float loss1 = SrcBatchLoss_Start.MemPtr[b];
                float loss2 = SrcBatchLoss_End.MemPtr[b];

                miniBatchLoss += (float)(Math.Exp(loss1) * Math.Exp(loss2));
            }
            ObjectiveScore += miniBatchLoss / MatchData.SrcSize;

            for (int b = 0; b < MatchData.SrcSize; b++)
            {
                int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];
                for (int m = matchBgn; m < matchEnd; m++)
                {
                    if (LabelStart.MemPtr[m] > 0) DataStart.Deriv.Data.MemPtr[m] = (float)(Gamma * (1 - DataStart.Deriv.Data.MemPtr[m])) / MatchData.SrcSize;
                    else DataStart.Deriv.Data.MemPtr[m] = (float)(Gamma * (-DataStart.Deriv.Data.MemPtr[m])) / MatchData.SrcSize;

                    if (LabelEnd.MemPtr[m] > 0) DataEnd.Deriv.Data.MemPtr[m] = (float)(Gamma * (1 - DataEnd.Deriv.Data.MemPtr[m])) / MatchData.SrcSize;
                    else DataEnd.Deriv.Data.MemPtr[m] = (float)(Gamma * (-DataEnd.Deriv.Data.MemPtr[m])) / MatchData.SrcSize;
                }
            }

            DataStart.Deriv.Data.SyncFromCPU(MatchData.MatchSize);
            DataEnd.Deriv.Data.SyncFromCPU(MatchData.MatchSize);
        }
    }

    public class BayesianSpanBoundaryRunner : ObjectiveRunner
    {
        public CudaPieceFloat LabelStart { get; set; }
        public CudaPieceFloat LabelEnd { get; set; }

        public BiMatchBatchData MatchData { get; set; }
        public float Gamma { get; set; }

        public HiddenBatchData DataStart;
        public HiddenBatchData DataEnd;

        public CudaPieceFloat SrcBatchLoss_Start;
        public CudaPieceFloat SrcBatchLoss_End;

        int MaxSpan = 1024;
        /// <summary>
        /// matchData must be in CPU. maxSpanLength must be larger than 1.
        /// </summary>
        /// <param name="labelStart"></param>
        /// <param name="labelEnd"></param>
        /// <param name="dataStart"></param>
        /// <param name="dataEnd"></param>
        /// <param name="matchData"></param>
        /// <param name="gamma"></param>
        /// <param name="behavior"></param>
        public BayesianSpanBoundaryRunner(CudaPieceFloat labelStart, CudaPieceFloat labelEnd, HiddenBatchData dataStart, HiddenBatchData dataEnd,
            BiMatchBatchData matchData, float gamma, int maxSpanLength, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            LabelStart = labelStart;
            LabelEnd = labelEnd;
            MatchData = matchData;
            Gamma = gamma;

            DataStart = dataStart;
            DataEnd = dataEnd;

            MaxSpan = maxSpanLength;

            SrcBatchLoss_Start = new CudaPieceFloat(MatchData.Stat.MAX_SRC_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
            SrcBatchLoss_End = new CudaPieceFloat(MatchData.Stat.MAX_SRC_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
        }

        public override void Forward()
        {
            if (MatchData.SrcSize == 0) { ObjectiveScore = 0; return; }

            LabelStart.SyncToCPU(MatchData.MatchSize);
            LabelEnd.SyncToCPU(MatchData.MatchSize);

            ObjectiveScore = 0;
            DataStart.Output.SyncToCPU();
            DataEnd.Output.SyncToCPU();
            double miniBatchLoss = 0;
            for (int b = 0; b < MatchData.SrcSize; b++)
            {
                int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];
                int length = matchEnd - matchBgn;
                
                Tuple<int, int, float> optimSpan = Util.ScanMaxValueSpan(DataStart.Output.Data.MemPtr, DataEnd.Output.Data.MemPtr, matchBgn, matchEnd, MaxSpan);

                //double[] startMap = new double[length];
                //double[] endMap = new double[length];
                //for (int m = matchBgn; m < matchEnd; m++)
                //{
                //    int i = m - matchBgn;
                //    double sp = DataStart.Output.Data.MemPtr[m] * Gamma; // Math.Exp((DataStart.Output.Data.MemPtr[m] - maxStartV) * Gamma);
                //    double ep = DataEnd.Output.Data.MemPtr[m] * Gamma;  //Math.Exp((DataEnd.Output.Data.MemPtr[m] - maxEndV) * Gamma);
                //    startMap[i] = (i == 0 ? sp : Util.LogAdd(startMap[i - 1], sp));
                //    endMap[i] = (i == 0 ? ep : Util.LogAdd(endMap[i - 1], ep));
                //}

                double[] startProb = new double[length];
                for (int m = matchBgn; m < matchEnd; m++)
                {
                    int i = m - matchBgn;
                    double startP = DataStart.Output.Data.MemPtr[m] * Gamma;
                    double logSum_end = double.MinValue;
                    for(int n = m; n < Math.Min(matchEnd, m + MaxSpan); n++)
                    {
                        int j = n - matchBgn;
                        logSum_end = Util.LogAdd(DataEnd.Output.Data.MemPtr[n] * Gamma, logSum_end);
                    }
                    startProb[i] = startP + logSum_end - optimSpan.Item3 * Gamma;
                }
                double startSum = Util.LogSum(startProb); // startProb.Sum();

                double[] endProb = new double[length];
                for (int m = matchBgn; m < matchEnd; m++)
                {
                    int i = m - matchBgn;
                    double endP = DataEnd.Output.Data.MemPtr[m] * Gamma;
                    double logSum_start = double.MinValue;
                    for (int n = m; n >= Math.Max(matchBgn, m - MaxSpan + 1); n--)
                    {
                        int j = n - matchBgn;
                        logSum_start = Util.LogAdd(DataStart.Output.Data.MemPtr[n] * Gamma, logSum_start);
                    }
                    endProb[i] = logSum_start + endP - optimSpan.Item3 * Gamma;
                }
                double endSum = Util.LogSum(endProb); //endProb.Sum();

                if (Math.Abs(startSum - endSum) >= 1e-7)
                {
                    throw new Exception(string.Format("error happens at BayesianSpanBoundaryRunner, startsum {0} : endsum {1}", startSum, endSum));
                }

                int startIdx = -1;
                int endIdx = -1;
                for (int m = matchBgn; m < matchEnd; m++)
                {
                    int i = m - matchBgn;

                    if (LabelStart.MemPtr[m] > 0)
                    {
                        DataStart.Deriv.Data.MemPtr[m] = (float)(Gamma * (1 - Math.Exp(startProb[i] - startSum)) / MatchData.SrcSize);
                        startIdx = m;
                    }
                    else DataStart.Deriv.Data.MemPtr[m] = (float)(Gamma * (-Math.Exp(startProb[i] - startSum)) / MatchData.SrcSize);

                    if (LabelEnd.MemPtr[m] > 0)
                    {
                        DataEnd.Deriv.Data.MemPtr[m] = (float)(Gamma * (1 - Math.Exp(endProb[i] - endSum)) / MatchData.SrcSize);
                        endIdx = m;
                    }
                    else DataEnd.Deriv.Data.MemPtr[m] = (float)(Gamma * (-Math.Exp(endProb[i] - endSum)) / MatchData.SrcSize);
                }

                if(startIdx == -1 || endIdx == -1) { throw new Exception(string.Format("startIdx {0} and endIdx {1} should be greater than zero ", startIdx, endIdx)); }

                miniBatchLoss += (float)Math.Exp((DataStart.Output.Data.MemPtr[startIdx] + DataEnd.Output.Data.MemPtr[endIdx] - optimSpan.Item3) * Gamma - startSum);
            }
            ObjectiveScore += miniBatchLoss / MatchData.SrcSize;

            DataStart.Deriv.Data.SyncFromCPU(MatchData.MatchSize);
            DataEnd.Deriv.Data.SyncFromCPU(MatchData.MatchSize);
        }
    }

}
