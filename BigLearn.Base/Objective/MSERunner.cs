using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{

	public class MSERunner : ObjectiveRunner
    {
    	public List<float> LabelList = null;
    	public CudaPieceFloat Label { get; set; }

        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        
        public DenseBatchData Deriv { get; set; }

        public CudaPieceFloat Index = null;

        public CudaPieceFloat tmpDeriv = null;

        public bool IsAvgLoss = true;
        float ClipDelta = 0;
        float Loss = 0;
        int BatchSize = 0;
        int Epoch = 0;
        public MSERunner(DenseBatchData output, DenseBatchData label, CudaPieceFloat index, DenseBatchData deriv, RunnerBehavior behavior, bool isAvgLoss = true, float clip_delta = 0)
            : base(Structure.Empty, behavior)
        {
            ClipDelta = clip_delta;
            Output = output;
            Label = label.Data;
            Index = index;
            Deriv = deriv;
            IsAvgLoss = isAvgLoss;
            if (Index == null || Index == CudaPieceFloat.Empty)
            {
                tmpDeriv = new CudaPieceFloat(Output.Stat.Dim * Output.Stat.MAX_BATCHSIZE, Behavior.Device);
            }
        }

        public MSERunner(CudaPieceFloat label, DenseBatchData output, DenseBatchData deriv, float clipDelta, bool isAvg, RunnerBehavior behavior)
        	: base(Structure.Empty, behavior)
        {
        	Label = label;
        	Output = output;
        	Deriv = deriv;
        	
        	ClipDelta = clipDelta;
        	IsAvgLoss = isAvg;

        	tmpDeriv = new CudaPieceFloat(Output.Stat.Dim * Output.Stat.MAX_BATCHSIZE, Behavior.Device);
        }

        public MSERunner(List<float> label, HiddenBatchData output, float clipDelta, bool isAvg,  RunnerBehavior behavior) 
            : this(new CudaPieceFloat(output.MAX_BATCHSIZE * output.Dim, behavior.Device), output.Output, output.Deriv, clipDelta, isAvg, behavior)
        {
            LabelList = label;
        }

        public override void Complete()
            {
                Epoch += 1;
                if(Epoch % 200 == 0)
                {
                    Logger.WriteLog("MSE Learner Epoch : {0}", Epoch);
                    {
                        Logger.WriteLog("Loss {0};", Math.Sqrt(Loss) * 1.0f / BatchSize );
                    }
                    BatchSize = 0;
                    Loss = 0;
                }
            }

        public unsafe override void Forward()
        {
        	if(LabelList != null)
        	{
        		LabelList.CopyTo(Label.MemPtr);
                Label.EffectiveSize = LabelList.Count;
                Label.SyncFromCPU();
        	}
            if (Index != null && Index != CudaPieceFloat.Empty)
                ObjectiveScore = RegressionLoss(Output.Data, Index, Label, Deriv.Data, Output.Stat.Dim, Output.BatchSize);
            else
                ObjectiveScore = RegressionLoss(Output.Data, Label, Deriv.Data, Output.Stat.Dim, Output.BatchSize);
            
            BatchSize += Label.EffectiveSize;
            Loss += (float)ObjectiveScore;
        }

        unsafe float RegressionLoss(CudaPieceFloat output, CudaPieceFloat index, CudaPieceFloat label, CudaPieceFloat deriv, int dim, int batchSize)
        {
            ///Compute Gradient.
            output.SyncToCPU();
            index.SyncToCPU();
            label.SyncToCPU();
            deriv.SyncToCPU();

            float miniBatchLoss = 0;
            for (int i = 0; i < batchSize; i++)
            {
                int regressIdx = (index == null || index == CudaPieceFloat.Empty) ? 0 : (int)index.CpuPtr[i];
                float bias = (label.CpuPtr[i] - output.CpuPtr[i * dim + regressIdx]);
                float delta = bias;
                if (ClipDelta > 0)
                {
                    float quadratic_part = Math.Min(Math.Abs(bias), ClipDelta);
                    float linear_part = Math.Abs(bias) - quadratic_part;
                    miniBatchLoss += quadratic_part * quadratic_part * 0.5f + linear_part;
                    delta = bias > 0 ? quadratic_part : -quadratic_part;
                }
                else
                {
                    miniBatchLoss += bias * bias * 0.5f;
                }
                if (IsAvgLoss) { deriv.CpuPtr[i * dim + regressIdx] += delta / batchSize; }
                else { deriv.CpuPtr[i * dim + regressIdx] += delta; }
            }
            deriv.SyncFromCPU();

            if (IsAvgLoss) return miniBatchLoss / batchSize;
            else return miniBatchLoss;
        }

        unsafe float RegressionLoss(CudaPieceFloat output, CudaPieceFloat label, CudaPieceFloat deriv, int dim, int batchSize)
        {
            if (batchSize == 0) return 0;

            ///Compute Gradient.
            if (IsAvgLoss) ComputeLib.Matrix_AdditionEx(output, 0, dim, label, 0, dim, tmpDeriv, 0, dim, dim, batchSize, -1.0f / batchSize, 1.0f / batchSize, 0);
            else ComputeLib.Matrix_AdditionEx(output, 0, dim, label, 0, dim, tmpDeriv, 0, dim, dim, batchSize, -1.0f, 1.0f, 0);

            ///Calculate Loss.
            float miniBatchLoss = 0;
            

            if (ClipDelta > 0)
            {
                tmpDeriv.SyncToCPU(batchSize * dim);
                for (int i = 0; i < batchSize * dim; i++)
                {

                    float bias = tmpDeriv.CpuPtr[i];
                    float delta = bias;
                    float quadratic_part = Math.Min(Math.Abs(bias), ClipDelta);
                    float linear_part = Math.Abs(bias) - quadratic_part;
                    miniBatchLoss += quadratic_part * quadratic_part * 0.5f + linear_part;
                    delta = bias > 0 ? quadratic_part : -quadratic_part;
                    tmpDeriv.CpuPtr[i] = delta;
                }
                tmpDeriv.SyncFromCPU(batchSize * dim);
            }
            else
            {
                miniBatchLoss = ComputeLib.VectorSum(tmpDeriv, 0,  batchSize * dim , 1);
                //for (int i = 0; i < batchSize * dim; i++) miniBatchLoss += (float)(tmpDeriv.CpuPtr[i] * tmpDeriv.CpuPtr[i] * 0.5);
            }

            ComputeLib.Matrix_Add(deriv, tmpDeriv, dim, batchSize, 1);
            
            if (IsAvgLoss) return miniBatchLoss / batchSize;
            else return miniBatchLoss;
        }

    }
}
