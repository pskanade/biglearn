﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ObjectiveRunner : StructRunner
    {
        public double ObjectiveScore { get; protected set; }
        public Dictionary<string, double> ObjectiveDict = new Dictionary<string, double>();
        public ObjectiveRunner(Structure model, RunnerBehavior behavior) : base(model, behavior)
        { }
    }

    public class BayesianRatingRunner : ObjectiveRunner
    {
        public new CudaPieceFloat Output { get; set; }
        public CudaPieceFloat Deriv { get; set; }
        public CudaPieceFloat Label { get; set; }

        public BiMatchBatchData MatchData { get; set; }

        public CudaPieceFloat SrcBatchLoss;

        public float Gamma { get; set; }
        bool IsLog { get; set; }
        public BayesianRatingRunner(CudaPieceFloat output, CudaPieceFloat deriv, CudaPieceFloat label,
            BiMatchBatchData matchData, float gamma, RunnerBehavior behavior, bool isLog = true)
            : base(Structure.Empty, behavior)
        {
            Output = output;
            Deriv = deriv;
            Label = label;

            MatchData = matchData;
            Gamma = gamma;
            IsLog = isLog;

            SrcBatchLoss = new CudaPieceFloat(MatchData.Stat.MAX_SRC_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
        }

        public override void Forward()
        {
            if (MatchData.SrcSize == 0) { ObjectiveScore = 0; return; }
            ComputeLib.LogBayesianRatingLoss(MatchData.Src2MatchIdx, MatchData.Src2MatchElement, MatchData.SrcSize,
                                      SrcBatchLoss, Output, Label, Deriv, (uint)MatchData.MatchSize, Util.GPUEpsilon, Gamma);



            ComputeLib.Scale_Matrix(Deriv, MatchData.MatchSize, 1, 1.0f / MatchData.SrcSize);

            float miniBatchLoss = 0;
            SrcBatchLoss.SyncToCPU(MatchData.SrcSize);

            if (IsLog)
            {
                for (int i = 0; i < MatchData.SrcSize; i++) miniBatchLoss += (float)SrcBatchLoss.MemPtr[i];
            }
            else
            {
                Deriv.SyncToCPU(MatchData.MatchSize);
                MatchData.Src2MatchIdx.SyncToCPU(MatchData.SrcSize);
                for (int i = 0; i < MatchData.SrcSize; i++)
                {
                    float avgScore = (float)Math.Exp(SrcBatchLoss.MemPtr[i]);
                    miniBatchLoss += -avgScore;
                    int end = MatchData.Src2MatchIdx.MemPtr[i];
                    int bgn = i == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[i - 1];
                    for(int k = bgn; k <end; k++) Deriv.MemPtr[k] = Deriv.MemPtr[k] * avgScore;
                }
                Deriv.SyncFromCPU(MatchData.MatchSize);
            }
            ObjectiveScore = -miniBatchLoss / MatchData.SrcSize;
        }
    }

    public class MultiInstanceBayesianRatingRunner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public BiMatchBatchData MatchData { get; set; }
        public float Gamma { get; set; }

        public HiddenBatchData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;


        public CudaPieceFloat SrcBatchLoss;


        public MultiInstanceBayesianRatingRunner(CudaPieceFloat label, HiddenBatchData[] data, HiddenBatchData[] prob,
            BiMatchBatchData matchData, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Label = label;
            MatchData = matchData;
            Gamma = gamma;

            Data = data;
            Prob = prob;

            Post = new float[MatchData.Stat.MAX_SRC_BATCHSIZE * Data.Length];
            SrcBatchLoss = new CudaPieceFloat(MatchData.Stat.MAX_SRC_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
        }

        public override void Forward()
        {
            if (MatchData.SrcSize == 0) { ObjectiveScore = 0; return; }

            Label.SyncToCPU(MatchData.MatchSize);
            ObjectiveScore = 0;

            for (int d = 0; d < Data.Length; d++)
            {
                ComputeLib.LogBayesianRatingLoss(MatchData.Src2MatchIdx, MatchData.Src2MatchElement, MatchData.SrcSize,
                                      SrcBatchLoss, Data[d].Output.Data, Label, Data[d].Deriv.Data, (uint)MatchData.MatchSize, Util.GPUEpsilon, Gamma);

                Prob[d].Output.Data.SyncToCPU(MatchData.SrcSize);
                Data[d].Deriv.Data.SyncToCPU(MatchData.MatchSize);
                SrcBatchLoss.SyncToCPU(MatchData.SrcSize);

                double miniBatchLoss = 0;
                for (int b = 0; b < MatchData.SrcSize; b++)
                {
                    float loss = SrcBatchLoss.MemPtr[b];

                    int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                    int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];
                    for (int m = matchBgn; m < matchEnd; m++)
                    {
                        Data[d].Deriv.Data.MemPtr[m] = Data[d].Deriv.Data.MemPtr[m] * Prob[d].Output.Data.MemPtr[b] / MatchData.SrcSize;
                    }

                    Post[b * Data.Length + d] = (float)Math.Exp(loss) * Prob[d].Output.Data.MemPtr[b];
                    miniBatchLoss += loss * Prob[d].Output.Data.MemPtr[b];
                }
                ObjectiveScore += -miniBatchLoss / MatchData.SrcSize;
                Data[d].Deriv.Data.SyncFromCPU(MatchData.MatchSize);
            }

            for (int b = 0; b < MatchData.SrcSize; b++)
            {
                float sum = 0;
                for (int d = 0; d < Data.Length; d++) sum += Post[b * Data.Length + d];

                for (int d = 0; d < Data.Length; d++)
                {
                    Post[b * Data.Length + d] = Post[b * Data.Length + d] / sum;
                    Prob[d].Deriv.Data.MemPtr[b] = Post[b * Data.Length + d] - Prob[d].Output.Data.MemPtr[b];
                }
            }

            for (int d = 0; d < Data.Length; d++)
            {
                Prob[d].Deriv.Data.SyncFromCPU(MatchData.SrcSize);
            }
        }
    }

    public class MultiInstanceCrossEntropyRunner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public HiddenBatchData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;

        public CudaPieceFloat tmpOutput;

        public float Gamma { get; set; }

        public MultiInstanceCrossEntropyRunner(CudaPieceFloat label, HiddenBatchData[] data, HiddenBatchData[] prob, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Data = data;
            Prob = prob;
            Post = new float[Data[0].MAX_BATCHSIZE * Data.Length];

            Label = label;
            Gamma = gamma;

            if (Gamma != 1) { tmpOutput = new CudaPieceFloat(Data[0].MAX_BATCHSIZE * Data[0].Dim, true, Behavior.Device == DeviceType.GPU); }
        }

        public override void Forward()
        {
            int BatchSize = Data[0].BatchSize;
            Label.SyncToCPU(BatchSize);
            ObjectiveScore = 0;

            for (int d = 0; d < Data.Length; d++)
            {
                if (Gamma != 1) ComputeLib.Add_Vector(tmpOutput, Data[d].Output.Data, Data[d].BatchSize * Data[d].Dim, 0, Gamma);
                else tmpOutput = Data[d].Output.Data;

                ComputeLib.DerivCrossEntropy(tmpOutput, Label, Data[d].Deriv.Data, Data[d].Output.BatchSize);

                tmpOutput.SyncToCPU(Data[d].BatchSize);
                Prob[d].Output.Data.SyncToCPU(Data[d].BatchSize);

                double miniBatchLoss = 0;
                for (int b = 0; b < BatchSize; b++)
                {
                    double score = (Math.Tanh(tmpOutput.MemPtr[b] / 2) + 1) / 2;

                    double loss = 0;
                    if (Label.MemPtr[b] != ParameterSetting.MissingLabel)
                    {
                        if (Label.MemPtr[b] > 0) loss = score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(score);
                        else loss = 1 - score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(1 - score);
                    }

                    Post[b * Data.Length + d] = (float)Math.Exp(loss) * Prob[d].Output.Data.MemPtr[b];
                    miniBatchLoss += loss * Prob[d].Output.Data.MemPtr[b];
                }
                ObjectiveScore += -miniBatchLoss / Data[d].Output.BatchSize; /// batchSize;

                ComputeLib.Scale_Matrix(Data[d].Deriv.Data, Data[d].BatchSize, Data[d].Dim, (float)1.0 / Data[d].BatchSize * Gamma);
                ComputeLib.ElementwiseProduct(Data[d].Deriv.Data, Prob[d].Output.Data, Data[d].Deriv.Data, Data[d].BatchSize, 1, 0);
            }

            for (int b = 0; b < BatchSize; b++)
            {
                float sum = 0;
                for (int d = 0; d < Data.Length; d++) sum += Post[b * Data.Length + d];

                for (int d = 0; d < Data.Length; d++)
                {
                    Post[b * Data.Length + d] = Post[b * Data.Length + d] / sum;
                    Prob[d].Deriv.Data.MemPtr[b] = Post[b * Data.Length + d] - Prob[d].Output.Data.MemPtr[b];
                }
            }
        }
    }

    public class BinaryStandardRewardRunner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public HiddenBatchData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;

        //public CudaPieceFloat tmpOutput;

        public float Gamma { get; set; }

        public BinaryStandardRewardRunner(CudaPieceFloat label, HiddenBatchData[] data, HiddenBatchData[] prob, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Data = data;
            Prob = prob;
            Post = new float[Data[0].MAX_BATCHSIZE * Data.Length];

            Label = label;
            Gamma = gamma;

            //tmpOutput = new CudaPieceFloat(Data[0].MAX_BATCHSIZE * Data[0].Dim, true, Behavior.Device == DeviceType.GPU);
            //if (Gamma != 1) { }
        }

        public override void Forward()
        {
            int BatchSize = Data[0].BatchSize;
            Label.SyncToCPU(BatchSize);
            ObjectiveScore = 0;

            for (int d = 0; d < Data.Length; d++)
            {
                //if (Gamma != 1) ComputeLib.Add_Vector(tmpOutput, Data[d].Output.Data, Data[d].BatchSize * Data[d].Dim, 0, Gamma);
                //else tmpOutput = Data[d].Output.Data;

                ComputeLib.Logistic(Data[d].Output.Data, 0, Data[d].Deriv.Data, 0, Data[d].Output.BatchSize * Data[d].Dim, Gamma);
                ComputeLib.ClipVector(Data[d].Deriv.Data, Data[d].Output.BatchSize * Data[d].Dim, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                //ComputeLib.DerivCrossEntropy(tmpOutput, Label, Data[d].Deriv.Data, Data[d].Output.BatchSize);

                //tmpOutput.SyncToCPU(Data[d].BatchSize);
                Prob[d].Output.Data.SyncToCPU(Data[d].BatchSize);
                Data[d].Deriv.Data.SyncToCPU(Data[d].Output.BatchSize * Data[d].Dim);

                double miniBatchLoss = 0;
                for (int b = 0; b < BatchSize; b++)
                {
                    double score = Data[d].Deriv.Data.MemPtr[b];  // (Math.Tanh(tmpOutput.MemPtr[b] / 2) + 1) / 2;

                    double loss = 0;
                    {
                        if (Label.MemPtr[b] > 0) loss = score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(score);
                        else loss = 1 - score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(1 - score);
                    }

                    Post[b * Data.Length + d] = (float)Math.Exp(loss) * Prob[d].Output.Data.MemPtr[b];
                    miniBatchLoss += Post[b * Data.Length + d]; // loss * Prob[d].Output.Data.MemPtr[b];
                }
                ObjectiveScore += miniBatchLoss / Data[d].Output.BatchSize; /// batchSize;
                //ComputeLib.Scale_Matrix(Data[d].Deriv.Data, Data[d].BatchSize, Data[d].Dim, (float)1.0 / Data[d].BatchSize * Gamma);
                //ComputeLib.ElementwiseProduct(Data[d].Deriv.Data, Prob[d].Output.Data, Data[d].Deriv.Data, Data[d].BatchSize, 1, 0);
            }

            for (int b = 0; b < BatchSize; b++)
            {
                float sum = float.Epsilon;
                for (int d = 0; d < Data.Length; d++) sum += Post[b * Data.Length + d];

                for (int d = 0; d < Data.Length; d++)
                {
                    //Post[b * Data.Length + d] = Post[b * Data.Length + d] / sum;
                    //Prob[d].Deriv.Data.MemPtr[b] = (Post[b * Data.Length + d] - Prob[d].Output.Data.MemPtr[b]) / BatchSize;

                    Prob[d].Deriv.Data.MemPtr[b] = Post[b * Data.Length + d] * 1.0f / BatchSize;


                    if (Label.MemPtr[b] > 0)
                        Data[d].Deriv.Data.MemPtr[b] = Gamma * (1 - Data[d].Deriv.Data.MemPtr[b]) * Post[b * Data.Length + d] / BatchSize;
                    else
                        Data[d].Deriv.Data.MemPtr[b] = -Gamma * Data[d].Deriv.Data.MemPtr[b] * Post[b * Data.Length + d] / BatchSize;
                }
            }

            for (int d = 0; d < Data.Length; d++)
            {
                Prob[d].Deriv.Data.SyncFromCPU(BatchSize);
                Data[d].Deriv.Data.SyncFromCPU(BatchSize);
            }
        }
    }

    public class BinaryMovingAvgRewardRunner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public HiddenBatchData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;

        //public CudaPieceFloat tmpOutput;

        public float Gamma { get; set; }

        float[] AvgB;
        float[] AvgA;
        int MovingBatchNum = 0;
        public BinaryMovingAvgRewardRunner(CudaPieceFloat label, HiddenBatchData[] data, HiddenBatchData[] prob, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Data = data;
            Prob = prob;
            Post = new float[Data[0].MAX_BATCHSIZE * Data.Length];

            Label = label;
            Gamma = gamma;

            MovingBatchNum = 0;
            AvgB = new float[Data.Length];
            AvgA = new float[Data.Length];
            //tmpOutput = new CudaPieceFloat(Data[0].MAX_BATCHSIZE * Data[0].Dim, true, Behavior.Device == DeviceType.GPU);
            //if (Gamma != 1) { }
        }

        public override void Forward()
        {
            int BatchSize = Data[0].BatchSize;
            Label.SyncToCPU(BatchSize);
            ObjectiveScore = 0;

            for (int d = 0; d < Data.Length; d++)
            {
                //if (Gamma != 1) ComputeLib.Add_Vector(tmpOutput, Data[d].Output.Data, Data[d].BatchSize * Data[d].Dim, 0, Gamma);
                //else tmpOutput = Data[d].Output.Data;

                ComputeLib.Logistic(Data[d].Output.Data, 0, Data[d].Deriv.Data, 0, Data[d].Output.BatchSize * Data[d].Dim, Gamma);
                ComputeLib.ClipVector(Data[d].Deriv.Data, Data[d].Output.BatchSize * Data[d].Dim, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                //ComputeLib.DerivCrossEntropy(tmpOutput, Label, Data[d].Deriv.Data, Data[d].Output.BatchSize);

                //tmpOutput.SyncToCPU(Data[d].BatchSize);
                Prob[d].Output.Data.SyncToCPU(Data[d].BatchSize);
                Data[d].Deriv.Data.SyncToCPU(Data[d].Output.BatchSize * Data[d].Dim);

                double miniBatchLoss = 0;
                for (int b = 0; b < BatchSize; b++)
                {
                    double score = Data[d].Deriv.Data.MemPtr[b];  // (Math.Tanh(tmpOutput.MemPtr[b] / 2) + 1) / 2;

                    double loss = 0;
                    {
                        if (Label.MemPtr[b] > 0) loss = score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(score);
                        else loss = 1 - score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(1 - score);
                    }

                    Post[b * Data.Length + d] = (float)Math.Exp(loss) * Prob[d].Output.Data.MemPtr[b];
                    miniBatchLoss += Post[b * Data.Length + d]; // loss * Prob[d].Output.Data.MemPtr[b];
                }
                ObjectiveScore += miniBatchLoss / Data[d].Output.BatchSize; /// batchSize;
                //ComputeLib.Scale_Matrix(Data[d].Deriv.Data, Data[d].BatchSize, Data[d].Dim, (float)1.0 / Data[d].BatchSize * Gamma);
                //ComputeLib.ElementwiseProduct(Data[d].Deriv.Data, Prob[d].Output.Data, Data[d].Deriv.Data, Data[d].BatchSize, 1, 0);
            }

            for (int b = 0; b < BatchSize; b++)
            {
                float sum = float.Epsilon;
                for (int d = 0; d < Data.Length; d++) sum += Post[b * Data.Length + d];

                for (int d = 0; d < Data.Length; d++)
                {
                    //Post[b * Data.Length + d] = Post[b * Data.Length + d] / sum;
                    //Prob[d].Deriv.Data.MemPtr[b] = (Post[b * Data.Length + d] - Prob[d].Output.Data.MemPtr[b]) / BatchSize;

                    double score = Data[d].Deriv.Data.MemPtr[b];  // (Math.Tanh(tmpOutput.MemPtr[b] / 2) + 1) / 2;
                    double p_pos = Label.MemPtr[b] > 0 ? score : 1 - score;
                    
                    Prob[d].Deriv.Data.MemPtr[b] = Prob[d].Output.Data.MemPtr[b] * 1.0f / BatchSize * (float)(p_pos - AvgB[d]);

                    if (Label.MemPtr[b] > 0)
                        Data[d].Deriv.Data.MemPtr[b] = (float) ( Gamma * (1 - score) * Post[b * Data.Length + d] / BatchSize );
                    else
                        Data[d].Deriv.Data.MemPtr[b] = -(float) ( Gamma * score * Post[b * Data.Length + d] / BatchSize );
                }
            }

            for (int d = 0; d < Data.Length; d++)
            {
                float totalReward = 0;
                for (int b = 0; b < BatchSize; b++)
                {
                    totalReward += Post[b * Data.Length + d];
                }
                float avgReward = totalReward / BatchSize;

                AvgB[d] = 0.995f * AvgB[d] + 0.005f * avgReward;
            }
            MovingBatchNum = MovingBatchNum + 1;


            for (int d = 0; d < Data.Length; d++)
            {
                Prob[d].Deriv.Data.SyncFromCPU(BatchSize);
                Data[d].Deriv.Data.SyncFromCPU(BatchSize);
            }
        }
    }

    public class BinaryCrossEntropyRunner : ObjectiveRunner
    {
        public SparseVectorData PosLabel { get; set; }
        List<List<int>> PosLabelList = null;
        public HiddenBatchData Score { get; set; }
        public SparseVectorData Mask = null;

        public BinaryCrossEntropyRunner(HiddenBatchData score, SparseVectorData posLabel, SparseVectorData mask, RunnerBehavior behavior) 
            : base(Structure.Empty, behavior)
        {
            Score = score;
            PosLabel = posLabel;
            Mask = mask;
        }


        public BinaryCrossEntropyRunner(HiddenBatchData score, SparseVectorData posLabel, RunnerBehavior behavior) 
            : this(score, posLabel, null, behavior)
        { }


        public BinaryCrossEntropyRunner(HiddenBatchData score, List<List<int>> labels, RunnerBehavior behavior)
            : this(score, new SparseVectorData(score.MAX_BATCHSIZE * score.Dim, behavior.Device), behavior)
        {
            PosLabelList = labels;
        }

        public override void Forward()
        {
            if(PosLabelList != null)
            {
                int cursor = 0;
                for(int b =0 ; b < PosLabelList.Count; b++)
                {
                    int labelBase = b * Score.Dim;
                    foreach(int t in PosLabelList[b].Distinct())
                    {
                        PosLabel.Idx.MemPtr[cursor] = labelBase + t;
                        PosLabel.Value.MemPtr[cursor] = 0.999f;
                        cursor += 1;
                    }
                }
                PosLabel.Length = cursor;
                PosLabel.SyncFromCPU();
            }
            ComputeLib.Logistic(Score.Output.Data, 0, Score.Deriv.Data, 0, Score.BatchSize * Score.Dim, 1);

            

            ComputeLib.Add_Vector(Score.Deriv.Data, Score.Deriv.Data, Score.Dim * Score.BatchSize, 0, -1);

            if(Mask != null)
            {
                ComputeLib.SparseVectorGivens(Mask.Idx, Mask.Value, Score.Deriv.Data, 0, 0, Mask.Length);
            }

            ComputeLib.SparseVectorAdd(PosLabel.Idx, PosLabel.Value, Score.Deriv.Data, 1, PosLabel.Length);
            ObjectiveScore = ComputeLib.VectorSum(Score.Deriv.Data, 0,  Score.BatchSize * Score.Dim , 1) / (Score.BatchSize * Score.Dim);
        }
    }

    public class CrossEntropyRunner : ObjectiveRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat Label { get; set; }
        
        public List<float> LabelList = null;

        public DenseBatchData Deriv { get; set; }

        public float Gamma { get; set; }
        public CudaPieceFloat tmpOutput;
        int Epoch = 0;
        int BatchSize = 0;
        float Loss = 0;
        public CrossEntropyRunner(CudaPieceFloat label, DenseBatchData output, DenseBatchData deriv, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            name = "cross entropy loss";
            Output = output;
            Label = label;
            Deriv = deriv;
        }

        public CrossEntropyRunner(List<float> label, HiddenBatchData output, RunnerBehavior behavior) 
            : this(new CudaPieceFloat(output.MAX_BATCHSIZE * output.Dim, behavior.Device), output.Output, output.Deriv, 1, behavior)
        {
            LabelList = label;
        }

        public override void Complete()
            {
                Epoch += 1;
                if(Epoch % 200 == 0)
                {
                    Logger.WriteLog("cross entropy Learner Epoch : {0}", Epoch);
                    {
                        Logger.WriteLog("Loss {0};", Loss * 1.0f / BatchSize );
                    }
                    BatchSize = 0;
                    Loss = 0;
                }
            }

        public override void Forward()
        {
            int num = Output.BatchSize * Output.Stat.Dim;

            if(LabelList != null)
            {
                LabelList.CopyTo(Label.MemPtr);
                Label.EffectiveSize = LabelList.Count;
                Label.SyncFromCPU();
            }

            ComputeLib.Logistic(Output.Data, 0, Deriv.Data, 0, num, 1);
            ComputeLib.Add_Vector(Deriv.Data, Label, num, -1, 1);
            
            //CudaPieceFloatDebug.Print("results", Deriv.Data, 10, true);

            ObjectiveScore = ComputeLib.VectorSum(Deriv.Data, 0, num , 1) / (num * 1.0f);

            BatchSize += 1;
            Loss += (float)ObjectiveScore;
        }
    }
    
    /// <summary>
    /// Label values only work on the First Dim. For other Dimension, label values are zero by default.
    /// </summary>
    public class SampleCrossEntropyRunner : ObjectiveRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        public DenseBatchData Label { get; set; }
        public DenseBatchData Deriv { get; set; }
        public float Gamma { get; set; }
        public SampleCrossEntropyRunner(DenseBatchData label, DenseBatchData output, DenseBatchData deriv, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Output = output;
            Label = label;
            Deriv = deriv;
            Gamma = gamma;
        }

        public override void Forward()
        {
            ComputeLib.Calculate_SampleCrossEntropyDeriv(Output.Data, Deriv.Data, Output.Stat.Dim, Label.Data, Output.BatchSize, Gamma);
            ComputeLib.Scale_Matrix(Deriv.Data, Output.Stat.Dim, Output.BatchSize, (float)1.0f / Output.BatchSize);
            ObjectiveScore = 0;
            Label.Data.SyncToCPU();
            Deriv.Data.SyncToCPU();
            for (int i = 0; i < Output.BatchSize * Output.Stat.Dim; i++)
            {
                float label = i % Output.Stat.Dim == 0 ? Label.Data.MemPtr[i / Output.Stat.Dim] : 0;
                double value = label - Deriv.Data.MemPtr[i] / Gamma;

                if (float.IsInfinity(Deriv.Data.MemPtr[i])) Console.WriteLine("SampleCrossEntropyRunner Infinity in Deriv.Data");
                if(value < 0) value = Util.GPUEpsilon;
                if(value >= 1) value = 1 - Util.GPUEpsilon;

                double l = (label * Math.Log(value + Util.GPUEpsilon) + (1 - label) * Math.Log(-value + 1.0 + Util.GPUEpsilon));
                if (double.IsNaN(l) || double.IsInfinity(l)) Console.ReadLine();

                ObjectiveScore -= l;
            }
            //Console.WriteLine("Loss {0}", Loss);
        }
    }

    public class MultiClassSoftmaxRunner : ObjectiveRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value;  } }
        public CudaPieceFloat Label { get; set; }
        public DenseBatchData Deriv { get; set; }
        public DenseBatchData Weight { get; set; }

        public float Gamma { get; set; }


        public double ScoreTotal = 0;
        public int BatchTotal = 0;

        /// <summary>
        /// Label could be null, the first element will be viewed as positive, others are negative.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="output"></param>
        /// <param name="deriv"></param>
        /// <param name="gamma"></param>
        /// <param name="behavior"></param>
        public MultiClassSoftmaxRunner(CudaPieceFloat label, DenseBatchData output, DenseBatchData deriv, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Output = output;
            Label = label;
            Deriv = deriv;
            Gamma = gamma;
            Weight = null;
        }


        /// <summary>
        /// Weighted Log Softmax
        /// </summary>
        /// <param name="label"></param>
        /// <param name="weight"></param>
        /// <param name="output"></param>
        /// <param name="deriv"></param>
        /// <param name="gamma"></param>
        /// <param name="behavior"></param>
        public MultiClassSoftmaxRunner(CudaPieceFloat label, DenseBatchData weight, DenseBatchData output, DenseBatchData deriv, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Output = output;
            Label = label;
            Deriv = deriv;
            Gamma = gamma;
            Weight = weight;
        }

        public override void Init()
        {
            BatchTotal = 0;
            ScoreTotal = 0;
        }

        public override void Complete()
        {
            Logger.WriteLog("Average Score {0},  Batch {1}", ScoreTotal / BatchTotal, BatchTotal);
        }

        public override void Forward()
        {
            if(Output.BatchSize == 0)
            {
                ObjectiveScore = 0; return;
            }

            if (Label != null) Label.SyncToCPU(Output.BatchSize);
            if (Weight != null) Weight.Data.SyncToCPU(Output.BatchSize);


            Output.Data.SyncToCPU();
            for (int i = 0; i < Output.BatchSize; i++)
            {
                int l = Label == null ? 0 : (int)Label.MemPtr[i];
                float weight = 1.0f;
                if (Weight != null)
                {
                    weight = Weight.Data.MemPtr[i];
                }
                //DSSMLossDerivParameter.COSINE_SIM_SCORE += weight * Output.Data.MemPtr[i * Output.Stat.Dim + l];
                ScoreTotal += Output.Data.MemPtr[i * Output.Stat.Dim + l];
            }
            BatchTotal += Output.BatchSize;

            //if(Gamma != 1) ComputeLib.Scale_Matrix(Output.Data, Output.BatchSize, Output.Stat.Dim, Gamma);
            ComputeLib.Add_Vector(Deriv.Data, Output.Data, Output.BatchSize * Output.Stat.Dim, 0, Gamma);
            ComputeLib.SoftMax(Deriv.Data, Deriv.Data, Output.Stat.Dim, Output.BatchSize, 1);
            //if (Gamma != 1) ComputeLib.Scale_Matrix(Output.Data, Output.BatchSize, Output.Stat.Dim, 1.0f / Gamma);

            Deriv.Data.SyncToCPU(Output.BatchSize * Output.Stat.Dim);
            
            double miniBatchLoss = 0;
            for (int i = 0; i < Output.BatchSize; i++)
            {
                int l = Label == null ? 0 : (int)Label.MemPtr[i];
                float wei = Weight == null ? 1 : Weight.Data.MemPtr[i];
                miniBatchLoss += wei * Math.Log(Math.Max(Deriv.Data.MemPtr[i * Output.Stat.Dim + l], Util.LargeEpsilon));
                Deriv.Data.MemPtr[i * Output.Stat.Dim + l] -= 1;
                if (Weight != null)
                    for (int d = 0; d < Output.Stat.Dim; d++)
                        Deriv.Data.MemPtr[i * Output.Stat.Dim + d] = Deriv.Data.MemPtr[i * Output.Stat.Dim + d] * wei;
            }
            Deriv.Data.SyncFromCPU(Output.BatchSize * Output.Stat.Dim);
            ComputeLib.Scale_Matrix(Deriv.Data, Output.BatchSize, Output.Stat.Dim, -Gamma * 1.0f / Output.BatchSize);
            ObjectiveScore = - miniBatchLoss / Output.BatchSize;
            
            //ObjectiveScore = -LossFunction.MulticlassSoftmaxLoss(Output.Data, Label == null ? null : Label, Deriv.Data, Weight == null ? null : Weight.Data, 
            //            Output.Stat.Dim, Gamma, Output.BatchSize);
            //ComputeLib.Scale_Matrix(Deriv.Data, Output.Stat.Dim, Output.BatchSize, (float)1.0 / Output.BatchSize);

            //Deriv.Data.SyncToCPU();
        }
    }


    


    public class SumCosineSimilarityRunner : ObjectiveRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat Label { get; set; }
        public DenseBatchData Deriv { get; set; }
        public DenseBatchData Weight { get; set; }



        public double CosineTotal { get; set; }
        public double WeightedCosineTotal { get; set; }

        public double TotalCount{ get; set; }

        public SumCosineSimilarityRunner(CudaPieceFloat label, DenseBatchData output,  RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Output = output;
            Label = label;
        }

        public override void Init()
        {
            this.CosineTotal = 0;
            this.WeightedCosineTotal = 0;
            this.TotalCount = 0;
        }

        public override void Complete()
        {
            Logger.WriteLog("Cosine Score {0},  weighted cosine:{1},  TotalCount {2}, Overall Cosine score:{3}, Overall Weighted Cosine score:{4}", 
                CosineTotal / TotalCount, this.WeightedCosineTotal / TotalCount, TotalCount, CosineTotal, this.WeightedCosineTotal);
        }
        public override void Forward()
        {
            if (Output.BatchSize == 0)
            {
                ObjectiveScore = 0; return;
            }

            if (Label != null) Label.SyncToCPU(Output.BatchSize);
            if (Weight != null) Weight.Data.SyncToCPU(Output.BatchSize);


            Output.Data.SyncToCPU();
            for (int i = 0; i < Output.BatchSize; i++)
            {
                int l = Label == null ? 0 : (int)Label.MemPtr[i];
                float weight = 1.0f;
                if (Weight != null)
                {
                    weight = Weight.Data.MemPtr[i];
                }
                WeightedCosineTotal += weight * Output.Data.MemPtr[i * Output.Stat.Dim + l];
                CosineTotal += Output.Data.MemPtr[i * Output.Stat.Dim + l];
            }
            TotalCount += Output.BatchSize;

        }
    }

    public class MarginLossRunner : ObjectiveRunner
    {
        public float Margin { get; set; }
        public HiddenBatchData PosData { get; set; }
        public HiddenBatchData NegData { get; set; }
        public MarginLossRunner(float margin, HiddenBatchData pos, HiddenBatchData neg, RunnerBehavior behavior) : base (Structure.Empty, behavior)
        {
            Margin = margin;
            PosData = pos;
            NegData = neg;
        }

        public override void Forward()
        {
            PosData.Output.Data.SyncToCPU();
            NegData.Output.Data.SyncToCPU();
            ObjectiveScore = 0;
            for (int i = 0; i < PosData.BatchSize; i++)
            {
                if (PosData.Output.Data.MemPtr[i] + Margin > NegData.Output.Data.MemPtr[i])
                {
                    PosData.Deriv.Data.MemPtr[i] = -1;
                    NegData.Deriv.Data.MemPtr[i] = 1;
                    ObjectiveScore += Margin + PosData.Output.Data.MemPtr[i] - NegData.Output.Data.MemPtr[i];
                }
                else 
                {
                    PosData.Deriv.Data.MemPtr[i] = 0;
                    NegData.Deriv.Data.MemPtr[i] = 0;
                }
            }
            PosData.Deriv.Data.SyncFromCPU();
            NegData.Deriv.Data.SyncFromCPU();
        }
    }

    public class HingeLossRunner : ObjectiveRunner
    {
        public float Margin { get; set; }
        public HiddenBatchData Score { get; set; }
        public HingeLossRunner(float margin, HiddenBatchData score, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Margin = margin;
            Score = score;
        }

        public override void Forward()
        {
            ObjectiveScore = 0;
            if (Score.BatchSize == 0) return;

            Score.Output.Data.SyncToCPU(Score.BatchSize * Score.Dim);
            Score.Deriv.Data.SyncToCPU(Score.BatchSize * Score.Dim);

            for (int i = 0; i < Score.BatchSize; i++)
            {
                //Score.Deriv.Data.MemPtr[i * Score.Dim] = 0;
                for (int d = 1; d < Score.Dim; d++)
                {
                    float v = Score.Output.Data.MemPtr[i * Score.Dim] + Margin - Score.Output.Data.MemPtr[i * Score.Dim + d];
                    if (v > 0)
                    {
                        Score.Deriv.Data.MemPtr[i * Score.Dim] += -1.0f / (Score.Dim - 1) / Score.BatchSize;
                        Score.Deriv.Data.MemPtr[i * Score.Dim + d] += 1.0f / (Score.Dim - 1) / Score.BatchSize;
                        ObjectiveScore += v;
                    }
                }
            }
            ObjectiveScore = ObjectiveScore / (Score.BatchSize * (Score.Dim - 1));
            Score.Deriv.Data.SyncFromCPU(Score.BatchSize * Score.Dim);
        }
    }
}
