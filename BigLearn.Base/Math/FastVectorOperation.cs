﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BigLearn
{
    unsafe public class FastVectorOperation : IMathOperationManager
    {
        static FastVectorOperation()
        {
            int minThreads;
            int minCPThreads;
            ThreadPool.GetMinThreads(out minThreads, out minCPThreads);
            int maxThreads;
            int maxCPThreads;
            ThreadPool.GetMaxThreads(out maxThreads, out maxCPThreads);
            int availableThreads;
            int availableCPThreads;
            ThreadPool.GetAvailableThreads(out availableThreads, out availableCPThreads);

            ThreadPool.SetMaxThreads(maxThreads, maxCPThreads);
            ThreadPool.SetMinThreads(Environment.ProcessorCount, minCPThreads);
        }
        #region Not implemented
        public void SEQ_Sparse_Matrix_Multiply_INTEX(SeqSparseBatchData data, CudaPieceFloat weight, CudaPieceFloat output, int inputDimension, int outputDimension, int winSize)
        {
            throw new NotImplementedException();
        }

        public void Matrix_Add_Rectified(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension)
        {
            BasicMathlib.Matrix_Rectified_Vector(output.MemPtr, bias.MemPtr, batchsize, outputDimension);
        }

        public void Cosine_Similarity_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize, int outputLayerSize, float eps)
        {
            throw new NotImplementedException();
        }

        public void Calculate_Alpha_MXE(CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        {
            throw new NotImplementedException();
        }

        public void Calculate_Alpha_NCE(CudaPieceFloat alpha, CudaPieceFloat dist, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        {
            throw new NotImplementedException();
        }

        public void Calculate_Alpha_NCE2(CudaPieceFloat alpha, CudaPieceFloat dist, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        {
            throw new NotImplementedException();
        }

        public void Calculate_Alpha_PAIRRANK(CudaPieceFloat alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
        {
            throw new NotImplementedException();
        }

        public void FillOut_Dist_NCE(CudaPieceFloat dist, CudaPieceInt GPU_negative_index, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize)
        {
            throw new NotImplementedException();
        }

        public void Deriv_Cosine(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        {
            throw new NotImplementedException();
        }

        public void Derive_Cosine_Rectified(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        {
            throw new NotImplementedException();
        }

        public void Deriv_Cosine_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        {
            throw new NotImplementedException();
        }

        public void Derive_Cosine_Linear_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        {
            throw new NotImplementedException();
        }

        public void Derive_Cosine_Rectified_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        {
            throw new NotImplementedException();
        }

        public void Matrix_WeightAdd_EX(CudaPieceFloat result, CudaPieceFloat addTerm, CudaPieceInt GPU_Inver_negative_index, CudaPieceInt GPU_Inver_negative_value, int batchsize, int outputLayerSize, CudaPieceFloat mweight, int start, int keep)
        {
            throw new NotImplementedException();
        }

        public void SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(SeqSparseBatchData input_batch, CudaPieceFloat weightDeriv, CudaPieceFloat upperOutputErrorDeriv, int inputDimension, int outputDimension, int winSize)
        {
            throw new NotImplementedException();
        }

        public void Convolution_Sparse_Matrix_Product_INTEX(CudaPieceFloat upperOutputErrorDeriv, CudaPieceInt layerMaxPooling_Index, SeqSparseBatchData input_batch, int winSize, int batchsize, int outputDimension, CudaPieceFloat weightDeriv, int inputDimension)
        {
            throw new NotImplementedException();
        }

        public void Matrix_Product(CudaPieceFloat lowerOutput, CudaPieceFloat upperOutputErrorDeriv, CudaPieceFloat weightDeriv, int batchsize, int inputDimension, int outputDimension)
        {
            throw new NotImplementedException();
        }

        public void Scale_Matrix(CudaPieceFloat matrix, int inputDimension, int outputDimnsion, float momentum)
        {
            fixed(float * pm = &matrix.MemPtr[0])
            {
                SSElib.SSE_Scale(momentum, pm, inputDimension*outputDimnsion);
            }
        }

        public void Matrix_Add(CudaPieceFloat matrix, CudaPieceFloat updates, int inputDimension, int outputDimnsion, float learning_rate)
        {
            throw new NotImplementedException();
        }

        public void Sparse2Dense_Matrix(SeqSparseBatchData data, CudaPieceFloat matrix, int batchsize, int outputDimension)
        {
            throw new NotImplementedException();
        }

        public void Matrix_Aggragate(CudaPieceFloat a, CudaPieceFloat b, int batchsize, int m)
        {
            throw new NotImplementedException();
        }

        public void FillOut_Dist_NCE_Full(CudaPieceFloat dist, CudaPieceInt neg_list, int nTrail, int BATCH_SIZE, int batchsize)
        {
            throw new NotImplementedException();
        }

        public void Deriv_Cosine_EX_Full(CudaPieceFloat q, CudaPieceFloat d, CudaPieceInt neg_list, CudaPieceFloat dcq, CudaPieceFloat dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
        {
            throw new NotImplementedException();
        }

        public void Deriv_Cosine_Rectified_EX_Full(CudaPieceFloat q, CudaPieceFloat d, CudaPieceInt neg_list, CudaPieceFloat dcq, CudaPieceFloat dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
        {
            throw new NotImplementedException();
        }

        public void Cosine_Similarity_SubSpace(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int labelDim, int BATCHSIZE, int batchsize, int subspaceDim, float eps)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Iter 1. 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="labelDim"></param>
        /// <param name="batchsize"></param>
        /// <param name="gamma"></param>
        public void SoftMax(CudaPieceFloat a, CudaPieceFloat b, int labelDim, int batchsize, float gamma)
        {
            BasicMathlib.SoftMax(a.MemPtr, b.MemPtr, labelDim, batchsize, gamma);
        }

        public void Deriv_Cosine_Subspace(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat dcq, CudaPieceFloat dcd, CudaPieceFloat alpha, int act_type, int batchsize, int labelDim, int subspaceDim, float gamma, float eps)
        {
            throw new NotImplementedException();
        }

        public void InnerProduct_Similarity(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int batchsize, int dimension)
        {
            throw new NotImplementedException();
        }

        public void Deriv_InnerProduct(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat dcq, CudaPieceFloat dcd, CudaPieceFloat alpha, int act_type, int batchsize, int Dim, float gamma, float eps)
        {
            throw new NotImplementedException();
        }

        public void Matrix_Add_OFFSET(CudaPieceFloat a, int offset_a, CudaPieceFloat b, int offset_b, int len, float mweight)
        {
            throw new NotImplementedException();
        }

        public void Matrix_Add_Sigmoid(CudaPieceFloat a, CudaPieceFloat b, int m, int n)
        {
            BasicMathlib.Matrix_Add_Sigmoid(a.MemPtr, b.MemPtr, m, n);
        }

        public void Square_Matrix(CudaPieceFloat a, int batchSize, int dim, CudaPieceFloat aSquare)
        {
            BasicMathlib.Square_Matrix(a.MemPtr, batchSize, dim, aSquare.MemPtr);
        }

        public void Cosine_Similarity_Matching(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB, int aSize, int bSize, int matchSize, int dimension, CudaPieceFloat aa, CudaPieceFloat bb, float eps)
        {
            BasicMathlib.Cosine_Similarity_Matching(a.MemPtr, b.MemPtr, c.MemPtr, matchIdxA.MemPtr, matchIdxB.MemPtr, aSize, bSize, matchSize, dimension, aa.MemPtr, bb.MemPtr, eps);
        }

        public void Inner_Product_Matching(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB, int aSize, int bSize, int matchSize, int dimension, float eps)
        {
            BasicMathlib.Inner_Product_Matching(a.MemPtr, b.MemPtr, c.MemPtr, matchIdxA.MemPtr, matchIdxB.MemPtr, aSize, bSize, matchSize, dimension, eps);
        }

        public void Joint_Sparse_Matrix_Multiply_INTEX(CudaPieceFloat Src_Input, int SrcSize, CudaPieceFloat Tgt_Input, int TgtSize, CudaPieceInt Src_Match_Idx, CudaPieceInt Tgt_Match_Idx, CudaPieceFloat Match_Score, int MatchSize, CudaPieceInt ConSmp_Idx, CudaPieceInt ConFea_Idx, CudaPieceFloat ConFea_Value, int SrcDimension, int TgtDimension, int ConDimension, int IsConScore, CudaPieceFloat Weight, CudaPieceFloat Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim)
        {
            throw new NotImplementedException();
        }

        public void Joint_Dense_Matrix_Multiply_INTEX(CudaPieceFloat Src_Input, int SrcSize, CudaPieceFloat Tgt_Input, int TgtSize, CudaPieceInt Src_Match_Idx, CudaPieceInt Tgt_Match_Idx, CudaPieceFloat Match_Score, int MatchSize, CudaPieceFloat ConFea_Value, int SrcDimension, int TgtDimension, int ConDimension, int IsConScore, CudaPieceFloat Weight, CudaPieceFloat Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim)
        {
            throw new NotImplementedException();
        }
        #endregion

        /// <summary>
        /// staled.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="weight"></param>
        /// <param name="output"></param>
        /// <param name="batchsize"></param>
        /// <param name="inputDimension"></param>
        /// <param name="outputDimension"></param>
        /// <param name="inverse"></param>
        public void Matrix_Multipy(CudaPieceFloat input, CudaPieceFloat weight, CudaPieceFloat output, int batchsize, int inputDimension, int outputDimension, int inverse)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["Matrix_Multipy"].Begin();
            //FastVector.MatrixMulRowMajor(FastVector.Create(input.MemPtr), FastVector.Create(weight.MemPtr), FastVector.Create(output.MemPtr), batchsize, inputDimension, outputDimension, false, inverse != 0, 0, 1);
            //PerfCounter.Manager.Instance["Matrix_Multipy"].TakeCount(h);
        }

        /// <summary>
        /// staled
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="pRight"></param>
        /// <param name="pDst"></param>
        /// <param name="leftRowCnt"></param>
        /// <param name="rightColumnCnt"></param>
        /// <param name="rightRowCnt"></param>
        /// <param name="weight"></param>
        public void MatrixMultiplicationTranspose(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt, float weight)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["MatrixMultiplicationTranspose"].Begin();
            //FastVector.MatrixMulRowMajor(FastVector.Create(pLeft.MemPtr), FastVector.Create(pRight.MemPtr), FastVector.Create(pDst.MemPtr), leftRowCnt, rightColumnCnt, rightRowCnt, false, true, weight, 1);
            //PerfCounter.Manager.Instance["MatrixMultiplicationTranspose"].TakeCount(h);
        }

        /// <summary>
        /// staled.
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="pRight"></param>
        /// <param name="pDst"></param>
        /// <param name="leftRowCnt"></param>
        /// <param name="leftColumnCnt"></param>
        /// <param name="rightColumnCnt"></param>
        /// <param name="weight"></param>
        public void MatrixMultiplicationLeftTranspose(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float weight)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["MatrixMultiplicationLeftTranspose"].Begin();
            //FastVector.MatrixMulRowMajor(FastVector.Create(pLeft.MemPtr), FastVector.Create(pRight.MemPtr), FastVector.Create(pDst.MemPtr), leftColumnCnt, leftRowCnt, rightColumnCnt, true, false, weight, 1);
            //PerfCounter.Manager.Instance["MatrixMultiplicationLeftTranspose"].TakeCount(h);
        }

        public void ColumnWiseSum(CudaPieceFloat matrix, CudaPieceFloat result, int row, int col, float weight)
        {

            var h = PerfCounter.Manager.Instance["ColumnWiseSum"].Begin();
            fixed(float * aM = &matrix.MemPtr[0])
            {
                fixed(float * bR = &result.MemPtr[0])
                {
                    SSElib.SSE_MatrixAggragateRowwise(aM, bR, row, col, 1, weight);
                    //FastVector.MatrixAggragateRowwise(FastVector.Create(matrix.MemPtr), FastVector.Create(result.MemPtr), row, col, 1, weight);
                }
            }
            PerfCounter.Manager.Instance["ColumnWiseSum"].TakeCount(h);
        }

        public void Add_Vector(CudaPieceFloat a, CudaPieceFloat b, int m, float awei, float bwei)
        {
            var h = PerfCounter.Manager.Instance["Add_Vector"].Begin();
            fixed(float * ap = &a.MemPtr[0])
            {
                fixed(float * bp = &b.MemPtr[0])
                {
                    SSElib.SSE_AddScaleEx(awei, bwei, ap, bp, ap, m); // float * ps, float * pd, int size);
                    //FastVector.Add_Vector(a.MemPtr, b.MemPtr, m, awei, bwei);
                }
            }
            
            PerfCounter.Manager.Instance["Add_Vector"].TakeCount(h);
        }

        public void Add_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int m, float awei, float bwei)
        {
            var h = PerfCounter.Manager.Instance["Add_Vector_Offset"].Begin();
            fixed(float * ap = &a.MemPtr[offsetA])
            {
                fixed(float * bp = &b.MemPtr[offsetB])
                {
                    SSElib.SSE_AddScaleEx(awei, bwei, ap, bp, ap, m); // float * ps, float * pd, int size);
                    //FastVector.Add_Vector(a.MemPtr, offsetA, b.MemPtr, offsetB, m, awei, bwei);
                }
            }
            
            PerfCounter.Manager.Instance["Add_Vector_Offset"].TakeCount(h);
        }

        public void Ada_Gradient(CudaPieceFloat ada, CudaPieceFloat grad, int m)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["Ada_Gradient"].Begin();
            //FastVector.AdagradUpdate(ada.MemPtr, grad.MemPtr);
            //PerfCounter.Manager.Instance["Ada_Gradient"].TakeCount(h);
        }

        public void ClipVector(CudaPieceFloat gpu_floats_a, int m, float maxThreshold, float minThreshold)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["ClipVector"].Begin();
            //FastVector.Clip(gpu_floats_a.MemPtr, m, maxThreshold, minThreshold);
            //PerfCounter.Manager.Instance["ClipVector"].TakeCount(h);
        }

        public void DerivCrossEntropy(CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputDeriv, int outputSize)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["DerivCrossEntropy"].Begin();
            //FastVector.DerivCrossEntropy(FastVector.Create(outputScore.MemPtr), FastVector.Create(outputLabel.MemPtr), FastVector.Create(outputDeriv.MemPtr), outputSize);
            //PerfCounter.Manager.Instance["DerivCrossEntropy"].TakeCount(h);
        }

        public void Deriv_Tanh(CudaPieceFloat errorDeriv, CudaPieceFloat output, int batchsize, int inputDimension)
        {
            throw new NotImplementedException();

            //var h = PerfCounter.Manager.Instance["Deriv_Tanh"].Begin();
            //FastVector errDriveVec = FastVector.Create(errorDeriv.MemPtr);
            //FastVector outputVec = FastVector.Create(output.MemPtr);
            //errDriveVec.ApplyTanhDerivativeInPlace(outputVec);
            //PerfCounter.Manager.Instance["Deriv_Tanh"].TakeCount(h);
        }

        public void Deriv_Rectified(CudaPieceFloat errorDeriv, CudaPieceFloat output, int batchsize, int inputDimension)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["Deriv_Rectified"].Begin();
            //FastVector errDriveVec = FastVector.Create(errorDeriv.MemPtr);
            //FastVector outputVec = FastVector.Create(output.MemPtr);
            //errDriveVec.ApplyRectifiedDerivativeInPlace(outputVec);
            //PerfCounter.Manager.Instance["Deriv_Rectified"].TakeCount(h);
        }

        public void Zero(CudaPieceFloat matrix, int size)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["Zero"].Begin();
            //FastVector.ZeroItems(matrix.MemPtr, size);
            //PerfCounter.Manager.Instance["Zero"].TakeCount(h);
        }

        public void MatrixMultiplicationSparseLeftTranspose(CudaPieceInt pRowIndex, int RowNum, CudaPieceInt pColumnIndex, CudaPieceFloat pValue, int elementsize, CudaPieceFloat pRight, CudaPieceFloat pDst, int leftColumnCnt, int rightColumnCnt, float weight)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["MatrixMultiplicationSparseLeftTranspose"].Begin();
            //FastVector.MatrixMultiplicationSparseLeftTranspose(pRowIndex.MemPtr, RowNum, pColumnIndex.MemPtr, pValue.MemPtr, elementsize, pRight.MemPtr, pDst.MemPtr, leftColumnCnt, rightColumnCnt, weight);
            //PerfCounter.Manager.Instance["MatrixMultiplicationSparseLeftTranspose"].TakeCount(h);
        }

        public void Sparse_Matrix_Multiply_INTEX_Weight(CudaPieceInt outputSmpIndex, CudaPieceInt outputItemIndex, CudaPieceFloat outputDeriv, int batchsize, int outputItemNum, CudaPieceFloat weight, CudaPieceFloat hidden_deriv, int hiddenDim, float wei)
        {
            var h = PerfCounter.Manager.Instance["Sparse_Matrix_Multiply_INTEX_Weight"].Begin();
            BasicMathlib.Sparse_matrix_multiply_INTEX_weight(outputSmpIndex.MemPtr, outputItemIndex.MemPtr, outputDeriv.MemPtr, batchsize, outputItemNum, weight.MemPtr, hidden_deriv.MemPtr, hiddenDim, wei);
            PerfCounter.Manager.Instance["Sparse_Matrix_Multiply_INTEX_Weight"].TakeCount(h);
        }

        public void Matrix_Add_Linear(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension)
        {
            var h = PerfCounter.Manager.Instance["Matrix_Add_Vector"].Begin();
            BasicMathlib.Matrix_Add_Vector(output.MemPtr, bias.MemPtr, batchsize, outputDimension);
            PerfCounter.Manager.Instance["Matrix_Add_Vector"].TakeCount(h);
        }

        public void RecursiveUpdateBatch(CudaPieceInt InstanceIndex, int batchSize, int seqSize, CudaPieceFloat SeqOutputMatrixs, int dim, CudaPieceFloat SeqDerivMatrixs, int lag, CudaPieceFloat gradient, float weight)
        {
            if (gradient.Size == 0 || lag == 0)
            {
                return;
            }
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["RecursiveUpdateBatch"].Begin();
            //FastVector.RecursiveUpdateBatch(InstanceIndex.MemPtr, batchSize, seqSize, SeqOutputMatrixs.MemPtr, dim, SeqDerivMatrixs.MemPtr, lag, gradient.MemPtr, weight);
            //PerfCounter.Manager.Instance["RecursiveUpdateBatch"].TakeCount(h);
        }

        public void RecursiveForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqInputMatrixs, int dim, CudaPieceFloat WMatrix, int lag, int af, CudaPieceFloat SeqOutputMatrix)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["RecursiveForwardPropagateBatch"].Begin();
            //FastVector.RecursiveForwardPropagateBatch(InstanceIndex.MemPtr, batchSize, SeqInputMatrixs.MemPtr, dim, WMatrix.MemPtr, lag, af, SeqOutputMatrix.MemPtr);
            //PerfCounter.Manager.Instance["RecursiveForwardPropagateBatch"].TakeCount(h);
        }

        public void RecursiveBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqOutputMatrixs, int dim, CudaPieceFloat WMatrix, int lag, int af, CudaPieceFloat SeqDerivMatrixs)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["RecursiveBackwardPropagateBatch"].Begin();
            //FastVector.RecursiveBackwardPropagateBatch(InstanceIndex.MemPtr, batchSize, SeqOutputMatrixs.MemPtr, dim, WMatrix.MemPtr, lag, af, SeqDerivMatrixs.MemPtr);
            //PerfCounter.Manager.Instance["RecursiveBackwardPropagateBatch"].TakeCount(h);
        }

        #region CDSSM
        public void Convolution_Sparse_Matrix_Multiply_INTEX(SeqSparseBatchData data, CudaPieceFloat weight, CudaPieceFloat layerPoolingOutput, int inputDimension, int outputDimension, int winSize)
        {
            throw new NotImplementedException();

            //PerfCounter.Manager.Instance["Convolution_Sparse_Matrix_Multiply_INTEX"].CountOperation(() =>
            //FastVector.Convolution_Sparse_Matrix_Multiply_INTEX(data.Sample_Idx_Mem, data.BatchSize, data.Seg_Idx_Mem, data.Seg_Margin_Mem, data.SentSize, data.Fea_Idx_Mem, data.Fea_Value_Mem, data.ElementSize,
            //                            weight.MemPtr, layerPoolingOutput.MemPtr, inputDimension, outputDimension, winSize));
        }

        public void Convolution_Sparse_Matrix_Product_INTEX_Weight(CudaPieceFloat deriv, CudaPieceInt maxpooling_index, CudaPieceInt Seg_Index, CudaPieceInt SegMargin_Index, int seg_size, int win_size, int batchsize, int output_dimension, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, CudaPieceFloat grad, int Feature_Dimension, float learnRate)
        {
            throw new NotImplementedException();
            //PerfCounter.Manager.Instance["Convolution_Sparse_Matrix_Product_INTEX_Weight"].CountOperation(() =>
            // FastVector.Convolution_Sparse_Matrix_Product_INTEX_Weight(deriv.MemPtr, maxpooling_index.MemPtr, Seg_Index.MemPtr, SegMargin_Index.MemPtr, seg_size, win_size, batchsize, output_dimension, Fea_Index.MemPtr, Fea_Value.MemPtr, grad.MemPtr, Feature_Dimension, learnRate));
        }

        public void Max_Pooling(CudaPieceFloat layerPoolingOutput, SeqSparseBatchData data, CudaPieceFloat output, CudaPieceInt layerMaxPooling_Index, int outputDimension)
        {
            PerfCounter.Manager.Instance["Max_Pooling"].CountOperation(() =>
            BasicMathlib.Max_Pooling(layerPoolingOutput.MemPtr, data.Sample_Idx_Mem, data.BatchSize, output.MemPtr, layerMaxPooling_Index.MemPtr, outputDimension));
        }

        public void SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat mul_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension, float weight)
        {
            throw new NotImplementedException();
            //PerfCounter.Manager.Instance["SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight"].CountOperation(() =>
            //FastVector.SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(Smp_Index.MemPtr, batchsize, Seg_Index.MemPtr, seg_size, Fea_Index.MemPtr, Fea_Value.MemPtr, elementsize, mul_weight.MemPtr, output.MemPtr, Feature_dimension, output_dimension, weight));
        }

        public void SEQ_Sparse_Matrix_Multiply_INT(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat mul_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension)
        {
            throw new NotImplementedException();
            //PerfCounter.Manager.Instance["SEQ_Sparse_Matrix_Multiply_INT"].CountOperation(() =>
            //FastVector.SEQ_Sparse_Matrix_Multiply_INT(Smp_Index.MemPtr, batchsize, Seg_Index.MemPtr, seg_size, Fea_Index.MemPtr, Fea_Value.MemPtr, elementsize, mul_weight.MemPtr, output.MemPtr, Feature_dimension, output_dimension));
        }

        public void Matrix_Product_Weight(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int batchsize, int m, int n, float weight)
        {
            PerfCounter.Manager.Instance["Matrix_Product_Weight"].CountOperation(() =>
            BasicMathlib.Matrix_Product_Weight(a.MemPtr, b.MemPtr, c.MemPtr, batchsize, m, n, weight));
        }

        public void Inner_Product_EX_Full(CudaPieceFloat a, CudaPieceFloat b, CudaPieceInt neg_list, CudaPieceFloat c, int nTrial, int MAX_BATCHSIZE, int batchsize, int dimension, float eps)
        {
            PerfCounter.Manager.Instance["Inner_Product_EX_Full"].CountOperation(() =>
            BasicMathlib.Inner_Product_EX_Full(a.MemPtr, b.MemPtr, neg_list.MemPtr, c.MemPtr, nTrial, MAX_BATCHSIZE, batchsize, dimension, eps));
        }

        public void Matrix_Add_Tanh(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension)
        {
            PerfCounter.Manager.Instance["Matrix_Add_Tanh"].CountOperation(() =>
            BasicMathlib.Matrix_Add_Tanh(output.MemPtr, bias.MemPtr, batchsize, outputDimension));
        }

        public void Cosine_Similarity(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize, int topLayerSize, float eps)
        {
            PerfCounter.Manager.Instance["Cosine_Similarity"].CountOperation(() =>
            BasicMathlib.Cosine_Similarity(srcTopLayerOutput.MemPtr,
                    tgtTopLayerOutput.MemPtr, alpha.MemPtr, nTrailPlus1, BATCH_SIZE, mIndex,
                    batchsize, topLayerSize, eps));
        }

        public void Cosine_Similarity_EX_Full(CudaPieceFloat a, CudaPieceFloat b, CudaPieceInt neg_list, CudaPieceFloat c, int nTrial, int BATCHSIZE,
        int batchsize, int dimension, float eps)
        {
            PerfCounter.Manager.Instance["Cosine_Similarity_EX_Full"].CountOperation(() =>
            BasicMathlib.Cosine_Similarity_EX_Full(a.MemPtr, b.MemPtr, neg_list.MemPtr, c.MemPtr, nTrial, BATCHSIZE, batchsize, dimension, eps));
        }

        public void Calculate_Alpha(CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        {
            PerfCounter.Manager.Instance["Calculate_Alpha"].CountOperation(() =>
            BasicMathlib.Calculate_Alpha(alpha.MemPtr, nTrailPlus1, BATCH_SIZE, batchsize, GAMMA));
        }

        public void Derive_Cosine_Linear(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        {
            PerfCounter.Manager.Instance["Derive_Cosine_Linear"].CountOperation(() =>
            BasicMathlib.Derive_Cosine_Linear(srcTopLayerOutput.MemPtr, tgtTopLayerOutput.MemPtr, srcTopLayerOutputDeriv.MemPtr, tgtTopLayerOutputDeriv.MemPtr, batchsize, outputLayerSize, eps));
        }

        public void Deriv_Cosine_Linear_EX_Full(CudaPieceFloat q, CudaPieceFloat d, CudaPieceInt neg_list, CudaPieceFloat dcq, CudaPieceFloat dcd,
                int nTrail, int BATCHSIZE, int batchsize, int outputDimension, float eps)
        {
            PerfCounter.Manager.Instance["Deriv_Cosine_Linear_EX_Full"].CountOperation(() =>
            BasicMathlib.Deriv_Cosine_Linear_EX_Full(q.MemPtr, d.MemPtr, neg_list.MemPtr, dcq.MemPtr, dcd.MemPtr, nTrail, BATCHSIZE, batchsize, outputDimension, eps));
        }

        public void Matrix_WeightAdd(CudaPieceFloat result, CudaPieceFloat addTerm, int batchsize, int outputLayerSize, CudaPieceFloat mweight, int start, int keep)
        {
            PerfCounter.Manager.Instance["Matrix_WeightAdd"].CountOperation(() =>
            BasicMathlib.Matrix_WeightAdd(result.MemPtr, addTerm.MemPtr, batchsize, outputLayerSize, mweight.MemPtr, start, keep));
        }

        public void Matrix_WeightAdd_Full(CudaPieceFloat gpu_floats_a, CudaPieceFloat gpu_floats_b, int nTrail, int BATCHSIZE, int batchsize, int dimension,
                CudaPieceFloat mweight, int start, int keep)
        {
            PerfCounter.Manager.Instance["Matrix_WeightAdd_Full"].CountOperation(() =>
            BasicMathlib.Matrix_WeightAdd_Full(gpu_floats_a.MemPtr, gpu_floats_b.MemPtr, nTrail, BATCHSIZE, batchsize, dimension,
                mweight.MemPtr, start, keep));
        }

        public void Matrix_WeightAdd_EX_Full(CudaPieceFloat gpu_floats_a, CudaPieceFloat gpu_floats_b, CudaPieceInt inver_neg_index,
                CudaPieceInt inver_neg_value, int nTrial, int BATCHSIZE, int batchsize, int dimension, CudaPieceFloat mweight,
                int start, int keep)
        {
            PerfCounter.Manager.Instance["Matrix_WeightAdd_EX_Full"].CountOperation(() =>
            BasicMathlib.Matrix_WeightAdd_EX_Full(gpu_floats_a.MemPtr, gpu_floats_b.MemPtr, inver_neg_index.MemPtr,
                inver_neg_value.MemPtr, nTrial, BATCHSIZE, batchsize, dimension, mweight.MemPtr, start, keep));
        }

        public void LSTMForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c,
            CudaPieceFloat gate_o, CudaPieceFloat tanhc, int cell, CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo)
        {
            throw new NotImplementedException();
            //FastVector.LSTMForwardPropagateBatch(InstanceIndex.MemPtr, batchSize, o.MemPtr, gate_i.MemPtr, c_hat.MemPtr, gate_f.MemPtr, c.MemPtr, gate_o.MemPtr, tanhc.MemPtr, cell, Ui.MemPtr, Uc.MemPtr, Uf.MemPtr, Uo.MemPtr, Vo.MemPtr);
        }

        public void LSTMBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int seqSize, int cell,
            CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc,
            CudaPieceFloat deriv_o, CudaPieceFloat deriv_gate_i, CudaPieceFloat deriv_cHat, CudaPieceFloat deriv_gate_f, CudaPieceFloat deriv_c, CudaPieceFloat deriv_gate_o, CudaPieceFloat deriv_tanhc,
            CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo)
        {
            throw new NotImplementedException();
        }

        public void RNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInput, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOut)
        {
            PerfCounter.Manager.Instance["RNNLabelOutput"].CountOperation(() =>
                BasicMathlib.RNNLabelOutput(smpIdx.MemPtr, batchSize, seqInput.MemPtr, seqSize, Wmatrix.MemPtr, lag, feaDim, hiddenDim, seqOut.MemPtr));
        }
        public void DerivRNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInputDeriv, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOutDeriv)
        {
            PerfCounter.Manager.Instance["DerivRNNLabelOutput"].CountOperation(() =>
                BasicMathlib.DerivRNNLabelOutput(smpIdx.MemPtr, batchSize, seqInputDeriv.MemPtr, seqSize, Wmatrix.MemPtr, lag, feaDim, hiddenDim, seqOutDeriv.MemPtr));
        }

        public void UpdateRNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInput, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOutDeriv, float wei)
        {
            PerfCounter.Manager.Instance["UpdateRNNLabelOutput"].CountOperation(() =>
                BasicMathlib.UpdateRNNLabeloutput(smpIdx.MemPtr, batchSize, seqInput.MemPtr, seqSize, Wmatrix.MemPtr, lag, feaDim, hiddenDim, seqOutDeriv.MemPtr, wei));
        }

        public void LastStepExtractSeqMatrix(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix)
        {
            BasicMathlib.LastTimeStepExtractSeqMatrix(InstanceIndex.MemPtr, batchSize, SeqMatrixs.MemPtr, dim, matrix.MemPtr);
        }

        public void GRUForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int Dim, //->x;
            CudaPieceFloat H, CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat HHat, CudaPieceFloat RestH,
            CudaPieceFloat Ur, CudaPieceFloat Uz, CudaPieceFloat Uh)
        {
            throw new NotImplementedException();
            //PerfCounter.Manager.Instance["GRUForwardPropagateBatch"].CountOperation(() =>
            //    FastVector.GRUForwardPropagateBatch(InstanceIndex.MemPtr, batchSize, Dim, H.MemPtr, GateR.MemPtr, GateZ.MemPtr, HHat.MemPtr, RestH.MemPtr, Ur.MemPtr, Uz.MemPtr, Uh.MemPtr));
        }

        public void GRUBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int Dim,//->x
            CudaPieceFloat H, CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat HHat, CudaPieceFloat RestH, //H @ R
            CudaPieceFloat DerivH, CudaPieceFloat DerivGateR, CudaPieceFloat DerivGateZ, CudaPieceFloat DerivHHat, CudaPieceFloat DerivRestH,
            CudaPieceFloat Ur, CudaPieceFloat Uz, CudaPieceFloat Uh)
        {
            throw new NotImplementedException();
            //PerfCounter.Manager.Instance["GRUBackwardPropagateBatch"].CountOperation(() =>
            //    FastVector.GRUBackwardPropagateBatch(InstanceIndex.MemPtr, batchSize, Dim, H.MemPtr, GateR.MemPtr, GateZ.MemPtr, HHat.MemPtr, RestH.MemPtr,
            //        DerivH.MemPtr, DerivGateR.MemPtr, DerivGateZ.MemPtr, DerivHHat.MemPtr, DerivRestH.MemPtr, Ur.MemPtr, Uz.MemPtr, Uh.MemPtr));
        }

        public float L2Norm(CudaPieceFloat x, int size)
        {
            fixed(float * ap = &x.MemPtr[0])
            {
                return SSElib.SSE_SumSq(ap, size);
            }
            //return FastVector.Create(x.MemPtr, 0, size, false).L2Norm;
        }

        public void RMSPropGradient(CudaPieceFloat ada, CudaPieceFloat grad, float gamma, float epsilon, int m)
        {
            throw new NotImplementedException();
        }

        public void VectorSquareAdd(CudaPieceFloat Z, CudaPieceFloat X, CudaPieceFloat Y, float wx, float wy, int m)
        {
            BasicMathlib.VectorSquareAdd(Z.MemPtr, X.MemPtr, Y.MemPtr, wx, wy, m);
        }
        public void AdaMGradient(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat G, float alpha, float beta1, float beta2, float epsilon, int t, int size)
        {
            BasicMathlib.AdaMGradient(M.MemPtr, V.MemPtr, G.MemPtr, alpha, beta1, beta2, epsilon, t, size);
        }
        public void AdaDeltaGradient(CudaPieceFloat deltaX, CudaPieceFloat AccumGrad, CudaPieceFloat AccumUpdate, CudaPieceFloat Grad, float epsilon, int size)
        {
            throw new NotImplementedException();
        }

        public void AdaMax(CudaPieceFloat V, CudaPieceFloat M, CudaPieceFloat G, float alpha, float beta, float epsilon, int size)
        {
            BasicMathlib.AdaMax(V.MemPtr, M.MemPtr, G.MemPtr, alpha, beta, epsilon, size);
        }
        #endregion


        public void Calculate_SampleCrossEntropyDeriv(CudaPieceFloat output, CudaPieceFloat deriv, int dim, CudaPieceFloat label, int batchsize, float gamma)
        {
            BasicMathlib.Calculate_SampleCrossEntropyDeriv(output.MemPtr, deriv.MemPtr, dim, label.MemPtr, batchsize, gamma);
        }

        public void ElementwiseProduct(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int row, int column, float weiDst)
        {
            BasicMathlib.ElementwiseProduct(pLeft.CpuPtr, pRight.CpuPtr, pDst.CpuPtr, row * column, weiDst);
        }

        public void Deriv_CosineSimilarity_Matching(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat qSquare, CudaPieceFloat dSquare, int dim, CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, CudaPieceInt tgt2MatchIdx, CudaPieceInt tgt2MatchElement, CudaPieceInt srcIdx, CudaPieceInt tgtIdx, int srcSize, int tgtSize, int matchSize, CudaPieceFloat simi, CudaPieceFloat derivSimi, CudaPieceFloat dcq, CudaPieceFloat dcd, float eps)
        {
            BasicMathlib.Deriv_CosineSimilarity_Matching(q.MemPtr, d.MemPtr, qSquare.MemPtr, dSquare.MemPtr, dim,
                src2MatchIdx.MemPtr, src2MatchElement.MemPtr, tgt2MatchIdx.MemPtr, tgt2MatchElement.MemPtr, srcIdx.MemPtr,
                tgtIdx.MemPtr, srcSize, tgtSize, matchSize, simi.MemPtr, derivSimi.MemPtr, dcq.MemPtr, dcd.MemPtr, eps);
        }

        public void Deriv_InnerProduct_Matching(CudaPieceFloat q, CudaPieceFloat d, int dim, CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, CudaPieceInt tgt2MatchIdx, CudaPieceInt tgt2MatchElement, CudaPieceInt srcIdx, CudaPieceInt tgtIdx, int srcSize, int tgtSize, int matchSize, CudaPieceFloat derivSimi, CudaPieceFloat dcq, CudaPieceFloat dcd, float eps)
        {
            BasicMathlib.Deriv_InnerProduct_Matching(q.MemPtr, d.MemPtr, dim, src2MatchIdx.MemPtr, src2MatchElement.MemPtr, tgt2MatchIdx.MemPtr, tgt2MatchElement.MemPtr,
                    srcIdx.MemPtr, tgtIdx.MemPtr, srcSize, tgtSize, matchSize, derivSimi.MemPtr, dcq.MemPtr, dcd.MemPtr, eps);
        }

        public void LSTMForwardPropagateBatchV2(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc, int cell, CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo, CudaPieceInt RecursiveIdx, CudaPieceInt RecursiveMatrix, int MaxLag)
        {
            throw new NotImplementedException();
            //PerfCounter.Manager.Instance["LSTMForwardPropagateBatchV2"].CountOperation(() =>
            //   FastVector.LSTMForwardPropagateBatch(InstanceIndex.MemPtr, batchSize, o.MemPtr, gate_i.MemPtr, c_hat.MemPtr, gate_f.MemPtr, c.MemPtr, gate_o.MemPtr, tanhc.MemPtr, cell, Ui.MemPtr, Uc.MemPtr, Uf.MemPtr, Uo.MemPtr, Vo.MemPtr));
        }

        public void LSTMBackwardPropagateBatchV2(CudaPieceInt InstanceIndex, int batchSize, int seqSize, int cell, CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc, CudaPieceFloat deriv_o, CudaPieceFloat deriv_gate_i, CudaPieceFloat deriv_cHat, CudaPieceFloat deriv_gate_f, CudaPieceFloat deriv_c, CudaPieceFloat deriv_gate_o, CudaPieceFloat deriv_tanhc, CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo, CudaPieceInt RecursiveIdx, CudaPieceInt RecursiveMatrix, int MaxLag)
        {
            throw new NotImplementedException();//
            //PerfCounter.Manager.Instance["LSTM//BackwardPropagateBatchV2"].CountOperation(() =>
            //    FastVector.LSTMBackwardPropaga//teBatch(InstanceIndex.MemPtr, batchSize, seqSize, cell, o.MemPtr, gate_i.MemPtr, c_hat.MemPtr, gate_f.MemPtr, c.MemPtr, gate_o.MemPtr, tanhc.MemPtr,
            //    deriv_o.MemPtr, deriv_gate_i.MemPtr, deriv_cHat.MemPtr, deriv_gate_f.MemPtr, deriv_c.MemPtr, deriv_gate_o.MemPtr, deriv_tanhc.MemPtr, Ui.MemPtr, Uc.MemPtr, Uf.MemPtr, Uo.MemPtr, Vo.MemPtr));
        }

        public void Max_Pooling(CudaPieceFloat pooling_feas, CudaPieceInt Smp_Index, int batchsize, CudaPieceFloat output, CudaPieceInt maxpooling_index, int output_dimension)
        {
            BasicMathlib.Max_Pooling(pooling_feas.MemPtr, Smp_Index.MemPtr, batchsize, output.MemPtr, maxpooling_index.MemPtr, output_dimension);
        }

        public void Tanh(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize)
        {
            throw new NotImplementedException();
            //FastVector.Tanh(FastVector.Create(a.MemPtr, offsetA + a.Offset, batchsize, false), FastVector.Create(b.MemPtr, offsetB + b.Offset, batchsize, false), batchsize);
        }

        public void Logistic(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize, float gamma)
        {
            for (int i = 0; i < batchsize; i++)
            {
                b.MemPtr[b.Offset + offsetB + i] = (float)(Math.Tanh(a.MemPtr[a.Offset + offsetA + i] * gamma / 2) + 1) / 2.0f;
            }
        }

        public void ReLU(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize)
        {
            for (int i = 0; i < batchsize; i++)
            {
                b.MemPtr[b.Offset + offsetB + i] = a.MemPtr[a.Offset + offsetA + i] > 0 ? a.MemPtr[a.Offset + offsetA + i] : 0;
            }
        }


        public void ElementwiseProduct(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst, int batchSize, int dim, float alpha)
        {
            for(int i=0;i<batchSize * dim;i++)
            {
                pDst.MemPtr[pDst.Offset + i + offsetDst] = alpha * pDst.MemPtr[pDst.Offset + i + offsetDst] +
                    pLeft.MemPtr[pLeft.Offset + i + offsetLeft] * pRight.MemPtr[pRight.Offset + i + offsetRight];
            }

        }


        public void ElementwiseProductMask(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst,
            CudaPieceInt leftmask, int offsetlm, CudaPieceInt rightmask, int offsetrm, CudaPieceInt dstmask, int offsetdm, int row, int column, float alpha, float beta)
        {
            for (int i = 0; i < row; i++)
            {
                int leftidx = leftmask.IsEmpty ? i : leftmask.MemPtr[leftmask.Offset+ offsetlm + i];
                int rightidx = rightmask.IsEmpty ? i : rightmask.MemPtr[rightmask.Offset + offsetrm + i];
                int dstidx = dstmask.IsEmpty ? i : dstmask.MemPtr[dstmask.Offset + offsetdm + i];

                for (int c = 0; c < column; c++)
                    pDst.MemPtr[pDst.Offset + offsetDst + dstidx * column + c] =
                        alpha * pDst.MemPtr[pDst.Offset + offsetDst + dstidx * column + c] +
                        beta * pLeft.MemPtr[pLeft.Offset + offsetLeft + leftidx * column + c] * pRight.MemPtr[pRight.Offset + offsetRight + rightidx * column + c];
            }

        }

        public unsafe void MatrixMulRowMajor(float* pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, bool transLeft, bool transRight, float w1, float w2)
        {
            if (!transLeft)
            {
                int blockSize = 32;
                int blockNum = (dstRowCnt + blockSize - 1) / blockSize;
                if (!transRight)
                {
                    Parallel.For(0, blockNum, (i) =>
                    {
                        int offset = i * blockSize;
                        if (offset >= dstRowCnt)
                        {
                            return;
                        }

                        int subRowCnt = Math.Min(blockSize, dstRowCnt - offset);

                        SSElib.ApplyMatMul(pLeft + offset * intermediateColumnCnt, pRight, pDst + offset * dstColumnCnt, subRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
                        //SseUtils.ApplyMatMul(
                    });
                }
                else
                {
                    Parallel.For(0, blockNum, (i) =>
                    {
                        int offset = i * blockSize;
                        if (offset >= dstRowCnt)
                        {
                            return;
                        }

                        int subRowCnt = Math.Min(blockSize, dstRowCnt - offset);
                        SSElib.ApplyMatMulTR(pLeft + offset * intermediateColumnCnt, pRight, pDst + offset * dstColumnCnt, subRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
                        //SseUtils.ApplyMatMulTR(pLeft + offset * intermediateColumnCnt, pRight, pDst + offset * dstColumnCnt, subRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
                    });
                }
            }
            else
            {
                int vBlockSize = 32;
                int blockNum = (dstColumnCnt + vBlockSize - 1) / vBlockSize;

                if (!transRight)
                {
                    if (intermediateColumnCnt == 1 && w1 == 1)
                    {
                        SSElib.ApplyOutterProduct(pLeft, pRight, pDst, dstRowCnt, dstColumnCnt, w2);
                        //SseUtils.ApplyOutterProduct(pLeft, pRight, pDst, dstRowCnt, dstColumnCnt, w2);
                    }
                    else
                    {
                        Parallel.For(0, blockNum, i =>
                        {
                            int vOffset = i * vBlockSize;
                            if (vOffset >= dstColumnCnt)
                            {
                                return;
                            }

                            int subColCnt = Math.Min(vBlockSize, dstColumnCnt - vOffset);

                            int hBlockSize = 128;
                            int hBlockNum = (intermediateColumnCnt + hBlockSize - 1) / hBlockSize;
                            object lockObj = new object();
                            float[][] tmpDst = new float[hBlockNum][];
                            Parallel.For(0, hBlockNum, new ParallelOptions(), (h) =>
                              {
                                  tmpDst[h] = new float[dstRowCnt * subColCnt];
                                  int hOffset = h * hBlockSize;
                                  if (hOffset >= intermediateColumnCnt)
                                  {
                                      return;
                                  }

                                  int hSubRowCnt = Math.Min(hBlockSize, intermediateColumnCnt - hOffset);

                                  fixed (float* pTmp = tmpDst[h])
                                  {
                                      SSElib.ApplyMatMulTLStride(pLeft + hOffset * dstRowCnt, pRight + vOffset + hOffset * dstColumnCnt, pTmp, dstRowCnt, hSubRowCnt, subColCnt, 0, w2, dstRowCnt, dstColumnCnt, subColCnt);
                                      //SseUtils.ApplyMatMulTL(pLeft + hOffset * dstRowCnt, pRight + vOffset + hOffset * dstColumnCnt, pTmp, dstRowCnt, hSubRowCnt, subColCnt, 0, w2, dstRowCnt, dstColumnCnt, subColCnt);
                                  }
                              });

                            for (int h = 0; h < hBlockNum; h++)
                            {
                                fixed (float* pTmp = tmpDst[h])
                                {
                                    for (int k = 0; k < dstRowCnt; k++)
                                    {
                                        int dstOff = k * dstColumnCnt + vOffset;
                                        int tmpOff = k * subColCnt;
                                        for (int l = 0; l < subColCnt; l++)
                                        {
                                            pDst[dstOff + l] += pTmp[tmpOff + l];
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
                else
                {
                    SSElib.ApplyMatMulTLTR(pLeft, pRight, pDst, dstRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
                    //SseUtils.ApplyMatMulTLTR(pLeft, pRight, pDst, dstRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
                }
            }
        }

        /// <summary>
        /// Dense matrix multiplication [C]=[C]*alpha + beta*[A]*[B]
        /// </summary>
        /// <param name="A"></param>
        /// <param name="offsetA"></param>
        /// <param name="B"></param>
        /// <param name="offsetB"></param>
        /// <param name="C"></param>
        /// <param name="offsetC"></param>
        /// <param name="rowA"></param>
        /// <param name="inDim"></param>
        /// <param name="outDim"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        /// <param name="transA"></param>
        /// <param name="transB"></param>
        public void Sgemm(CudaPieceFloat A, int offsetA, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC, int rowA, int inDim, int outDim, float alpha, float beta, bool transA, bool transB)
        {
            if (!transA && !transB)
            {
                var h = PerfCounter.Manager.Instance["Sgemm"].Begin();
                fixed (float* pA = &A.MemPtr[offsetA + A.Offset])
                fixed (float* pB = &B.MemPtr[offsetB + B.Offset])
                fixed (float* pC = &C.MemPtr[offsetC + C.Offset])
                {
                    MatrixMulRowMajor(pA, pB, pC, rowA, inDim, outDim, false, false, alpha, beta);
                }
                PerfCounter.Manager.Instance["Sgemm"].TakeCount(h);
            }
            else if (!transA && transB)
            {
                var h = PerfCounter.Manager.Instance["SgemmTB"].Begin();
                fixed (float* pA = &A.MemPtr[offsetA + A.Offset])
                fixed (float* pB = &B.MemPtr[offsetB + B.Offset])
                fixed (float* pC = &C.MemPtr[offsetC + C.Offset])
                {
                    MatrixMulRowMajor(pA, pB, pC, rowA, inDim, outDim, false, true, alpha, beta);
                }
                PerfCounter.Manager.Instance["SgemmTB"].TakeCount(h);
            }
            else if (transA && !transB)
            {
                var h = PerfCounter.Manager.Instance["SgemmTA"].Begin();
                
                fixed (float* pA = &A.MemPtr[offsetA + A.Offset])
                fixed (float* pB = &B.MemPtr[offsetB + B.Offset])
                fixed (float* pC = &C.MemPtr[offsetC + C.Offset])
                {
                    MatrixMulRowMajor(pA, pB, pC, inDim, rowA, outDim, true, false, alpha, beta);
                }

                PerfCounter.Manager.Instance["SgemmTA"].TakeCount(h);
            }
        }

        public void Sgemv(CudaPieceFloat matrix, CudaPieceFloat x, int row, int col, CudaPieceFloat y, bool transM, float alpha, float beta)
        {
            if (!transM)
            {
                Sgemm(matrix, 0, x, 0, y, 0, row, col, 1, alpha, beta, false, false);
            }
            else
            {
                Sgemm(x, 0, matrix, 0, y, 0, 1, row, col, alpha, beta, false, false);
            }
        }

        public void SgemmMask(CudaPieceFloat A, int offsetA, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC, int batchsize, int m, int n, CudaPieceInt aMask, int offsetAMask, CudaPieceInt bMask, int offsetBMask, CudaPieceInt cMask, int offsetCMask, float alpha, float beta, bool transA, bool transB)
        {
            throw new NotImplementedException();
        }


        public void SparseSgemmMask(CudaPieceInt AIndex, CudaPieceInt AFeaIndex, CudaPieceFloat AFeaValue, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC, int batchsize, int m, int n, CudaPieceInt aMask, int offsetAMask, CudaPieceInt bMask, int offsetBMask, CudaPieceInt cMask, int offsetCMask, float alpha, float beta, bool transA, bool transB)
        {
            throw new NotImplementedException();
        }


        public void DerivTanh(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta)
        {
            for (int i = 0; i < batchsize; i++)
            {
                c.MemPtr[offsetC + i] = c.MemPtr[offsetC + i] * alpha +
                    b.MemPtr[offsetB + i] * (1 + a.MemPtr[offsetA + i]) * (1 - a.MemPtr[offsetA + i]) * beta;
            }
        }

        public void DerivLogistic(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta)
        {
            for (int i = 0; i < batchsize; i++)
            {
                c.MemPtr[offsetC + i] = c.MemPtr[offsetC + i] * alpha +
                    b.MemPtr[offsetB + i] * (a.MemPtr[offsetA + i]) * (1 - a.MemPtr[offsetA + i]) * beta;
            }
        }

        public void DerivReLU(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta)
        {
            for (int i = 0; i < batchsize; i++)
            {
                c.MemPtr[offsetC + i] = c.MemPtr[offsetC + i] * alpha + (a.MemPtr[offsetA + i] <= 0 ? 0 : b.MemPtr[offsetB + i]) * beta;
            }
        }


        public void MaxPoolingMask(CudaPieceFloat poolingValues, CudaPieceInt smpIdx, CudaPieceInt smpMask, int batchsize, CudaPieceFloat output, CudaPieceInt maxpoolingIndex, int output_dimension)
        {
            if (smpMask.IsEmpty)
                Max_Pooling(poolingValues, smpIdx, batchsize, output, maxpoolingIndex, output_dimension);
            else
                BasicMathlib.Max_Pooling_Mask(poolingValues.MemPtr, smpIdx.MemPtr, batchsize, smpMask.MemPtr, output.MemPtr, maxpoolingIndex.MemPtr, output_dimension);
        }

        public void DerivMaxPooling(CudaPieceFloat deriv, CudaPieceInt maxpoolingIndex, CudaPieceFloat poolingDeriv, int batchsize, int output_dimension, float alpha, float beta)
        {
            BasicMathlib.Deriv_Max_Pooling(deriv.MemPtr, maxpoolingIndex.MemPtr, poolingDeriv.MemPtr, batchsize, output_dimension, alpha, beta);
        }


        public void LastStepExtractSeqMatrixMask(CudaPieceInt InstanceIndex, CudaPieceInt SeqTransIdx, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix, CudaPieceFloat matrixMask, int maskOffset)
        {
            throw new NotImplementedException();
        }

        public void DerivLastStepSeqMatrix(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix, float alpha)
        {
            throw new NotImplementedException();
        }

        public void DerivLastStepSeqMatrixMask(CudaPieceInt InstanceIndex, CudaPieceInt SeqTransIdx, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix,
            CudaPieceFloat matrixMask, int maskOffset, float alpha)
        {
            throw new NotImplementedException();
        }


        public float LogLossDeriv(CudaPieceFloat outputProb, int dim, int batchSize, CudaPieceFloat smpProb, CudaPieceFloat label, CudaPieceFloat labelwei, CudaPieceInt labelMask, float gamma, float eps)
        {
            throw new NotImplementedException();
        }


        public void Matrix_AdditionMask(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAMask, CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBMask, CudaPieceFloat c, int offsetC, CudaPieceInt cMask, int offsetCMask, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            BasicMathlib.Matrix_AdditionMask(a.CpuPtr + offsetA, aMask.CpuPtr + offsetAMask, b.CpuPtr + offsetB, bMask.CpuPtr + offsetBMask, c.CpuPtr + offsetC, cMask.CpuPtr + offsetCMask, dim, batchSize, awei, bwei, cwei);
        }


        public unsafe void SparseSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputScore, CudaPieceFloat outputProb, float gamma, int batchSize)
        {
            BasicMathlib.SparseSoftmax(smpIdx.CpuPtr, outputScore.CpuPtr, outputProb.CpuPtr, gamma, batchSize);
        }


        public void DerivSparseMultiClassSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat probDeriv, CudaPieceFloat outputDeriv, float gamma, int batchSize, float alpha = 0, float beta = 1)
        {
            throw new NotImplementedException();
        }

        public void ColumnWiseSumMask(CudaPieceFloat matrix, int offsetM, CudaPieceInt matrixMask, int offsetMM, 
            CudaPieceFloat weight, CudaPieceInt smpIdx, int batchSize, CudaPieceFloat output, int offsetO, 
            CudaPieceInt outputMask, int offsetOM, int row, int col, float alpha, float beta)
        {
            BasicMathlib.ColumnWiseSumMask(matrix.CpuPtr + offsetM, matrixMask.CpuPtr + offsetMM, weight.CpuPtr, smpIdx.CpuPtr, batchSize, output.CpuPtr + offsetO, outputMask.CpuPtr + offsetOM, row, col, alpha, beta);
        }

        public void Scale_MatrixMask(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAM, CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBM, int dim, int batchSize, CudaPieceFloat awei, float bwei)
        {
            throw new NotImplementedException();
        }

        public void Inner_Product_Matching(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB, int aSize, int bSize, int matchSize, int dimension, float eps)
        {
            throw new NotImplementedException();
        }

        public void ClipAdaGradUpdate(CudaPieceFloat ada, CudaPieceFloat grad, CudaPieceFloat param, float updateRate, float gradClip, float weightClip)
        {
            throw new NotImplementedException();
            //FastVector.ClipAdaGradUpdate(ada.MemPtr, grad.MemPtr, param.MemPtr, param.Size, updateRate, gradClip, weightClip);
        }

        public void ClipAdamUpdate(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat grad, CudaPieceFloat param, float beta1, float beta2, int iter, float updateRate, float gradClip, float weightClip)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["ClipAdamUpdate"].Begin();
            //FastVector.ClipAdamUpdate(M.MemPtr, V.MemPtr, grad.MemPtr, param.MemPtr, param.Size, beta1, beta2, iter, updateRate, gradClip, weightClip);
            //PerfCounter.Manager.Instance["ClipAdamUpdate"].TakeCount(h);
        }
        public void MomentumUpdate(CudaPieceFloat weight, CudaPieceFloat momentum, float momentumRate)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["MomentumUpdate"].Begin();
            //FastVector.MomentumUpdate(weight.MemPtr, momentum.MemPtr, weight.EffectiveSize, momentumRate);
            //PerfCounter.Manager.Instance["MomentumUpdate"].TakeCount(h);
        }

        public void NAGUpdate(CudaPieceFloat weight, CudaPieceFloat momentum, CudaPieceFloat trueWeight, float momentumRate)
        {
            throw new NotImplementedException();
            //var h = PerfCounter.Manager.Instance["MomentumUpdate"].Begin();
            //FastVector.NAGUpdate(weight.MemPtr, momentum.MemPtr, trueWeight.MemPtr, weight.EffectiveSize, momentumRate);
            //PerfCounter.Manager.Instance["MomentumUpdate"].TakeCount(h);
        }


        public void LogBayesianRatingLoss(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize, CudaPieceFloat srcBatchLoss, CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputDeriv, uint outputSize, float eplison, float gamma)
        {
            Enumerable.Range(0, srcBatchSize).AsParallel().ForAll(idx =>
            {
                int eleEnd = src2MatchIdx.MemPtr[idx];
                int eleBegin = idx > 0 ? src2MatchIdx.MemPtr[idx - 1] : 0;

                for (int i = eleBegin; i < eleEnd; i++)
                {
                    outputDeriv.MemPtr[src2MatchElement.MemPtr[i]] = 0;
                }

                int listNum = eleEnd - eleBegin;
                if (listNum <= 0) { srcBatchLoss.MemPtr[idx] = 0; return; }

                float maxValue = - float.MaxValue;
                for (int i = eleBegin; i < eleEnd; i++)
                {
                    float tmp = outputScore.MemPtr[src2MatchElement.MemPtr[i]];
                    if (tmp > maxValue) { maxValue = tmp; }
                }

                float pure_sum = 0;
                float rate_sum = 0;
                for (int i = eleBegin; i < eleEnd; i++)
                {
                    int matchI = src2MatchElement.MemPtr[i];
                    float tmpScore = (float)Math.Exp(gamma * (outputScore.MemPtr[matchI] - maxValue));
                    float rateTmpScore = outputLabel.MemPtr[matchI] * tmpScore;
                    pure_sum += tmpScore;
                    rate_sum += rateTmpScore;
                }
                float avgRating = rate_sum / (pure_sum + eplison);
                srcBatchLoss.MemPtr[idx] = (float)Math.Log(avgRating + eplison);

                for (int i = eleBegin; i < eleEnd; i++)
                {
                    int matchI = src2MatchElement.MemPtr[i];
                    float tmpScore = (float)Math.Exp(gamma * (outputScore.MemPtr[matchI] - maxValue));
                    outputDeriv.MemPtr[matchI] = gamma * (outputLabel.MemPtr[matchI] * tmpScore / (rate_sum + eplison) - tmpScore / (pure_sum + eplison));
                }
            });

        }

        public void AddAndClear(CudaPieceFloat a, CudaPieceFloat b, int m, float bwei)
        {
            throw new NotImplementedException();
            //PerfCounter.Manager.Instance["AddAndClear"].CountOperation(() =>
            //    FastVector.AddAndClear(a.MemPtr, b.MemPtr, m, bwei));
        }

        public unsafe void GetSeqOrderMatrixs(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, CudaPieceInt MapForward, int dim, int isReverse, int order, CudaPieceFloat matrix, float alpha, float beta)
        {
            BasicMathlib.GetSeqOrderMatrixs(InstanceIndex.CpuPtr, batchSize, SeqMatrixs.CpuPtr, MapForward.CpuPtr, dim, isReverse, order, matrix.CpuPtr, alpha, beta);
        }

        public void SetSeqOrderMatrixs(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, CudaPieceInt MapForward, int dim, int isReverse, int order, CudaPieceFloat matrix, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void GetWindowMatrixs(CudaPieceInt InstanceMargin, int sentsize, CudaPieceFloat SeqMatrixs, CudaPieceInt mapForward, int dim, int winsize, CudaPieceFloat matrix, float alpha, float beta)
        {
            BasicMathlib.GetWindowMatrixs(InstanceMargin.MemPtr, sentsize, SeqMatrixs.MemPtr, mapForward.MemPtr, dim, winsize, matrix.MemPtr, alpha, beta);
        }

        public void SetWindowMatrixs(CudaPieceInt InstanceMargin, int sentsize, CudaPieceFloat SeqMatrixs, CudaPieceInt mapForward, int dim, int winsize, CudaPieceFloat matrix, float alpha, float beta)
        {
            BasicMathlib.SetWindowMatrixs(InstanceMargin.MemPtr, sentsize, SeqMatrixs.MemPtr, mapForward.MemPtr, dim, winsize, matrix.MemPtr, alpha, beta);
        }

        public void DerivSoftmax(CudaPieceFloat softmaxData, CudaPieceFloat softmaxDeriv, CudaPieceFloat gradData, int dim, int batchsize, float alpha)
        {
            throw new NotImplementedException();
        }

        public void MaxoutPooling(CudaPieceFloat input, int batchSize, int dim, CudaPieceInt maxpoolingIndex, int maxoutDim, int outDim, CudaPieceFloat output, int format)
        {
            BasicMathlib.MaxoutPooling(input.CpuPtr, batchSize, dim, maxpoolingIndex.CpuPtr, maxoutDim, outDim, output.CpuPtr, format);
        }

        public void DerivMaxoutPooling(CudaPieceFloat input_deriv, int batchSize, int dim, CudaPieceInt maxpoolingIndex, int maxoutDim, int outDim, CudaPieceFloat output_deriv)
        {
            throw new NotImplementedException();
        }

        public void CNNForwardPropagate(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMap, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride, CudaPieceFloat imageOutBatch, int outHeight, int outWidth)
        {
            throw new NotImplementedException();
        }

        public void BayesianRatingProb(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize, CudaPieceFloat srcBatchLoss, CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputProb, int outputSize, float eplison, float gamma)
        {
            throw new NotImplementedException();
        }

        public void MatrixL1Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l1Score)
        {
            //Enumerable.Range(0, batchSize).AsParallel().ForAll(i =>
            for(int i=0;i<batchSize; i++)
            {
                fixed(float * p = &x.MemPtr[i * dim])
                {
                    l1Score.MemPtr[i] = SSElib.SSE_SumAbs(p, dim);
                }
                //AVXFastVector v = new AVXFastVector(x.MemPtr, i * dim, dim, false);
                //l1Score.MemPtr[i] = v.Norm(1);
            }//);
        }
        public void MatrixL2Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l2Score)
        {
            //Enumerable.Range(0, batchSize).AsParallel().ForAll(i =>
            for (int i = 0; i < batchSize; i++)
            {

                fixed(float * p = &x.MemPtr[i * dim])
                {
                    l2Score.MemPtr[i] = (float)Math.Sqrt(SSElib.SSE_SumSq(p, dim));
                }

                //AVXFastVector v = new AVXFastVector(x.MemPtr, i * dim, dim, false);
                //l2Score.MemPtr[i] = v.Norm(2);
            }
            //);
        }

        public void DerivMatrixL1Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l1Score, CudaPieceFloat l1ScoreDeriv, CudaPieceFloat xDeriv)
        {
            Enumerable.Range(0, batchSize).AsParallel().ForAll(idx =>
            {
                for (int i = 0; i < dim; i++)
                {
                    xDeriv.MemPtr[idx * dim + i] = x.MemPtr[idx * dim + i] >= 0 ? l1ScoreDeriv.MemPtr[idx] : -l1ScoreDeriv.MemPtr[idx];
                }
            });
        }

        public void DerivMatrixL2Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l2Score, CudaPieceFloat l2ScoreDeriv, CudaPieceFloat xDeriv)
        {
            Enumerable.Range(0, batchSize).AsParallel().ForAll(idx =>
            {
                for (int i = 0; i < dim; i++)
                {
                    xDeriv.MemPtr[idx * dim + i] = x.MemPtr[idx * dim + i] * l2ScoreDeriv.MemPtr[idx] / (l2Score.MemPtr[idx] + float.Epsilon) ;
                }
            });
        }

        public void Matrix_Mask(CudaPieceFloat output, CudaPieceInt dropoutMask, int batchSize, int dim)
        {
            BasicMathlib.Matrix_Mask(output.MemPtr, dropoutMask.MemPtr, batchSize, dim);
        }

        public void SpanMaxPool(CudaPieceFloat inputSent, CudaPieceInt smpIdx, CudaPieceFloat start, CudaPieceFloat end, CudaPieceInt sentMargin, int matchSize, CudaPieceFloat outputSent, CudaPieceInt outMaxIndex, int dim)
        {
            throw new NotImplementedException();
        }

        public void DerivSpanMaxPool(CudaPieceFloat outputSentDeriv, CudaPieceInt smpIdx, int batchSize, CudaPieceFloat inputSentDeriv, CudaPieceInt outMaxIndex, int dim, float beta)
        {
            throw new NotImplementedException();
        }

        public void SpanLastPool(CudaPieceFloat inputSent, CudaPieceInt smpIdx, CudaPieceFloat start, CudaPieceFloat end, CudaPieceInt sentMargin, int matchSize, CudaPieceFloat outputSent, int dim)
        {
            throw new NotImplementedException();
        }

        public void DerivSpanLastPool(CudaPieceFloat outputSentDeriv, CudaPieceInt outSmpIdx, CudaPieceFloat start, CudaPieceFloat end, int batchSize, CudaPieceFloat inputSentDeriv, CudaPieceInt inSmpIdx, int dim, float beta)
        {
            throw new NotImplementedException();
        }

        public void AccurateScale_Matrix(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAM, CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBM, int dim, int batchSize, CudaPieceFloat awei)
        {
            throw new NotImplementedException();
        }
        public void AccurateElementwiseProduct(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst, CudaPieceInt leftmask, int offsetlm, CudaPieceInt rightmask, int offsetrm, CudaPieceInt dstmask, int offsetdm, int row, int column, float beta)
        {
            throw new NotImplementedException();
        }

        public void Convolution_Sparse_Matrix_Multiply_INTEX(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, CudaPieceInt Seg_Margin, int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat con_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension, int win_size)
        {
            throw new NotImplementedException();
        }

        public unsafe void Matrix_AdditionEx(CudaPieceFloat a, int offsetA, int skipA, CudaPieceFloat b, int offsetB, int skipB, CudaPieceFloat c, int offsetC, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            BasicMathlib.Matrix_AdditionEx(a.CpuPtr + offsetA, skipA, b.CpuPtr + offsetB, skipB, c.CpuPtr + offsetC, skipC, dim, batchSize, awei, bwei, cwei);
            //Enumerable.Range(0, batchSize * dim).AsParallel().ForAll(id =>
            //{
            //    int x = id % dim;
            //    int y = id / dim;
            //    int aPos = y * skipA + x;
            //    int bPos = y * skipB + x;
            //    int cPos = y * skipC + x;
            //    c.MemPtr[c.Offset + offsetC + cPos] = cwei * c.MemPtr[c.Offset + offsetC + cPos] + awei * a.MemPtr[a.Offset + offsetA + aPos] + bwei * b.MemPtr[b.Offset + offsetB + bPos];
            //});
        }

        public unsafe void Matrix_AdditionExMask(CudaPieceFloat a, CudaPieceInt aMask, int skipA, CudaPieceFloat b, CudaPieceInt bMask, int skipB, CudaPieceFloat c, CudaPieceInt cMask, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            BasicMathlib.Matrix_AdditionExMask(a.CpuPtr, aMask.CpuPtr, skipA, b.CpuPtr, bMask.CpuPtr, skipB, c.CpuPtr, cMask.CpuPtr, skipC, dim, batchSize, awei, bwei, cwei);
            //Enumerable.Range(0, batchSize * dim).AsParallel().ForAll(id =>
            //{
            //    int x = id % dim;
            //    int y = id / dim;
            //    int ya = aMask.IsEmpty ? y : aMask.MemPtr[y];
            //    int yb = bMask.IsEmpty ? y : bMask.MemPtr[y];
            //    int yc = cMask.IsEmpty ? y : cMask.MemPtr[y];

            //    int aPos = ya * skipA + x;
            //    int bPos = yb * skipB + x;
            //    int cPos = yc * skipC + x;
            //    c.MemPtr[c.Offset + cPos] = cwei * c.MemPtr[c.Offset + cPos] + awei * a.MemPtr[a.Offset + aPos] + bwei * b.MemPtr[b.Offset + bPos];
            //});
        }

        public unsafe void SoftAttention_Matching(CudaPieceFloat a, CudaPieceInt aMask, CudaPieceFloat b, CudaPieceInt bMask, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat c, CudaPieceInt cMask, int batchSize, float alpha, float beta)
        {
            BasicMathlib.SoftAttention_Matching(a.CpuPtr, aMask.CpuPtr, b.CpuPtr, bMask.CpuPtr, dim, a_func, op, vec.CpuPtr, c.CpuPtr, cMask.CpuPtr, batchSize, alpha, beta);
        }

        public void DerivSoftAttention_Matching(CudaPieceFloat a, CudaPieceFloat aDeriv, CudaPieceInt aMask, CudaPieceFloat b, CudaPieceFloat bDeriv, CudaPieceInt bMask, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat vecDeriv, CudaPieceFloat c, CudaPieceFloat cDeriv, CudaPieceInt cMask, CudaPieceInt aSmpIdx, CudaPieceInt aElementIdx, int aSize, CudaPieceInt bSmpIdx, CudaPieceInt bElementIdx, int bSize, int batchSize, float awei, float bwei, float vwei)
        {
            throw new NotImplementedException();
        }

        public void SoftAttention(CudaPieceFloat a, CudaPieceFloat b, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat c, int aBatchSize, int bBatchSize, float alpha, float beta)
        {
            BasicMathlib.SoftAttention(a.CpuPtr, b.CpuPtr, dim, a_func, op, vec.CpuPtr, c.CpuPtr, aBatchSize, bBatchSize, alpha, beta);
        }

        public void DerivSoftAttention(CudaPieceFloat a, CudaPieceFloat aDeriv, CudaPieceFloat b, CudaPieceFloat bDeriv, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat vecDeriv, CudaPieceFloat c, CudaPieceFloat cDeriv, int aBatchSize, int bBatchSize, float awei, float bwei, float vwei)
        {
            throw new NotImplementedException();
        }

        public void CuDNNBackwardPropagateData(CudaPieceFloat imageInBatchDeriv, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMap, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride, CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth)
        {
            throw new NotImplementedException();
        }

        public void CuDNNBackwardPropagateFilter(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMapDeriv, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride, CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth, float alphaValue, float betaValue)
        {
            throw new NotImplementedException();
        }

        public void RMSPropV2_Gradient(CudaPieceFloat ada, CudaPieceFloat g, CudaPieceFloat grad, float gamma, float epsilon, int m)
        {
            throw new NotImplementedException();
        }

        public void CuDNNBackwardPropagateBias(CudaPieceFloat biasDeriv, CudaPieceFloat imageOutBatchDeriv, int batch, int outDepth, int outHeight, int outWidth, float alphaValue, float betaValue)
        {
            throw new NotImplementedException();
        }

        public void NDArrayTranspose(CudaPieceFloat input, CudaPieceInt indim, CudaPieceInt transDim, CudaPieceFloat output, int dimNum, int length, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void KLargestValueBatch(CudaPieceFloat float_array, int batchSize, int dim, int K, int mode, CudaPieceFloat bestValues, CudaPieceFloat bestIndexes)
        {
            throw new NotImplementedException();
        }

        public void DerivLogSparseSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat logProbDeriv, CudaPieceFloat outputDeriv, float gamma, float alpha, float beta, int batchSize)
        {
            throw new NotImplementedException();
        }

        public void ColumnWiseSumMaskEx(CudaPieceFloat matrix, int offsetM, CudaPieceInt matrixMask, int offsetMM, CudaPieceFloat weight, int skipMatrix, CudaPieceInt smpIdx, int batchSize, CudaPieceFloat output, int offsetO, CudaPieceInt outputMask, int offsetOM, int skipOutput, int row, int col, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void DerivLogProbEntropy(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat logProbDeriv, float alpha, float beta, float epislon, int batchSize)
        {
            throw new NotImplementedException();
        }
        
        public void LookupForward(CudaPieceInt inputSmpIdx, CudaPieceInt inputItemIdx, int batchSize, CudaPieceFloat weight, CudaPieceFloat output, int inputDim, int outputDim)
        {
            for(int idx = 0; idx < batchSize; idx ++)
            {
                int widx = inputItemIdx.MemPtr[idx];
                FastVector.Add_Vector(output.MemPtr, idx * outputDim, weight.MemPtr, widx * outputDim, outputDim, 0, 1);
            }
        }
        
        public void LookupBackward(CudaPieceInt inputSmpIdx, CudaPieceInt inputItemIdx, int batchSize, int inputItemNum, CudaPieceFloat deriv, int inputDim, int outputDim, CudaPieceFloat output, float lr)
        {
            for (int idx = 0; idx < batchSize; idx ++)
            {
                int widx = inputItemIdx.MemPtr[idx];
                FastVector.Add_Vector(output.MemPtr, widx * outputDim, deriv.MemPtr, idx * outputDim, outputDim, 1, lr);
            }
        }
        
        public void SparseVectorAdd(CudaPieceInt idx, CudaPieceFloat value, CudaPieceFloat dst, float beta, int length)
        {
            throw new NotImplementedException();
        }

        public float VectorSum(CudaPieceFloat x, int index, int len, int norm)
        {
            throw new NotImplementedException();
            return -1;
        }

        public void SparseVectorGivens(CudaPieceInt idx, CudaPieceFloat value, CudaPieceFloat dst, float alpha, float beta, int length)
        {
            throw new NotImplementedException();
        }

        public void Scale_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int len, float awei, float bwei)
        {
            throw new NotImplementedException();
        }
        public void Init_Vector(CudaPieceFloat a, int offsetA, float b, int m, float awei)
        {
            throw new NotImplementedException();
        }

        public void Log(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, float alpha, float beta, int size)
        {
            throw new NotImplementedException();
        }
        
    }
}
