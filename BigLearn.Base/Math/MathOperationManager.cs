﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace BigLearn
{
    public enum DeviceType
    {
        GPU = 0,
        CPU = 1,
        CPU_FAST_VECTOR = 2, //pengcheng's lib;
    }

    public interface IMathOperationManager
    {

        /************************* Basic Computation Lib.******************************/
        /// <summary>
        /// b = tanh(a)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="batchsize">size of a and b.</param>
        void Tanh(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize);

        /// <summary>
        /// c = alpha c + beta (1-a) (1 + a) * b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="offsetA"></param>
        /// <param name="b"></param>
        /// <param name="offsetB"></param>
        /// <param name="batchsize"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"> beta wil always be 1 in GPU Computation. </param>
        void DerivTanh(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta);

        /// <summary>
        /// errorDeriv = errorDeriv * (1 - output) * (1 + output)
        /// </summary>
        /// <param name="errorDeriv"></param>
        /// <param name="output"></param>
        /// <param name="batchsize"></param>
        /// <param name="inputDimension"></param>
        void Deriv_Tanh(CudaPieceFloat errorDeriv, CudaPieceFloat output, int batchsize, int inputDimension);


        /// b = alpha * b + alpha * log(a);
        void Log(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, float alpha, float beta, int size);

        /// <summary>
        /// b = sigmoid(a * gamma)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="batchsize">size of a and b.</param>
        void Logistic(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize, float gamma);

        /// <summary>
        /// c = alpha c + beta * a * (1 - a) * b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="offsetA"></param>
        /// <param name="b"></param>
        /// <param name="offsetB"></param>
        /// <param name="c"></param>
        /// <param name="offsetC"></param>
        /// <param name="batchsize"></param>
        /// <param name="gamma"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        void DerivLogistic(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta);


        /// <summary>
        /// b = ReLU(a)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="batchsize">size of a and b.</param>
        void ReLU(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize);

        /// <summary>
        /// c = alpha c + beta ReLU(a)' * b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="offsetA"></param>
        /// <param name="b"></param>
        /// <param name="offsetB"></param>
        /// <param name="c"></param>
        /// <param name="offsetC"></param>
        /// <param name="batchsize"></param>
        /// <param name="gamma"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        void DerivReLU(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta);

        /// <summary>
        /// errorDeriv = errorDeriv * ReLU(output)'
        /// </summary>
        /// <param name="errorDeriv"></param>
        /// <param name="output"></param>
        /// <param name="batchsize"></param>
        /// <param name="inputDimension"></param>
        void Deriv_Rectified(CudaPieceFloat errorDeriv, CudaPieceFloat output, int batchsize, int inputDimension);


        /// <summary>
        /// pDst = alpha * pDst + pLeft @ pRight.
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="offsetLeft"></param>
        /// <param name="pRight"></param>
        /// <param name="offsetRight"></param>
        /// <param name="pDst"></param>
        /// <param name="offsetDst"></param>
        /// <param name="batchSize"></param>
        /// <param name="alpha"></param>
        void ElementwiseProduct(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst, int batchSize, int dim, float alpha);

        /// <summary>
        /// pDst = alpha * pDst + pLeft @ pRight.
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="pRight"></param>
        /// <param name="pDst"></param>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="alpha"></param>
        void ElementwiseProduct(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int row, int column, float alpha);

        /// <summary>
        /// pDst = alpha * pDst + beta * pLeft @ pRight.
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="offsetLeft"></param>
        /// <param name="pRight"></param>
        /// <param name="offsetRight"></param>
        /// <param name="pDst"></param>
        /// <param name="offsetDst"></param>
        /// <param name="leftmask"></param>
        /// <param name="offsetlm"></param>
        /// <param name="rightmask"></param>
        /// <param name="offsetrm"></param>
        /// <param name="dstmask"></param>
        /// <param name="offsetdm"></param>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        void ElementwiseProductMask(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst,
            CudaPieceInt leftmask, int offsetlm, CudaPieceInt rightmask, int offsetrm, CudaPieceInt dstmask, int offsetdm, int row, int column, float alpha, float beta);


        /// <summary>
        /// pDst = pDst + beta * pLeft @ pRight.
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="offsetLeft"></param>
        /// <param name="pRight"></param>
        /// <param name="offsetRight"></param>
        /// <param name="pDst"></param>
        /// <param name="offsetDst"></param>
        /// <param name="leftmask"></param>
        /// <param name="offsetlm"></param>
        /// <param name="rightmask"></param>
        /// <param name="offsetrm"></param>
        /// <param name="dstmask"></param>
        /// <param name="offsetdm"></param>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="beta"></param>
        void AccurateElementwiseProduct(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst,
            CudaPieceInt leftmask, int offsetlm,  CudaPieceInt rightmask, int offsetrm, CudaPieceInt dstmask, int offsetdm, int row, int column, float beta);

        /// <summary>
        /// C = αlpha C +  βeta op ( A ) op ( B )
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="C"></param>
        /// <param name="rowA"></param>
        /// <param name="inDim"></param>
        /// <param name="outDim"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        /// <param name="transA"></param>
        /// <param name="transB"></param>
        void Sgemm(CudaPieceFloat A, int offsetA, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC, int rowA, int inDim, int outDim, float alpha, float beta, bool transA, bool transB);

        /// <summary>
        /// C = αlpha C +  βeta op ( A ) op ( B ) 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="offsetA"></param>
        /// <param name="B"></param>
        /// <param name="offsetB"></param>
        /// <param name="C"></param>
        /// <param name="offsetC"></param>
        /// <param name="batchsize"></param>
        /// <param name="m"></param>
        /// <param name="n"></param>
        /// <param name="aMask"></param>
        /// <param name="offsetAMask"></param>
        /// <param name="bMask"></param>
        /// <param name="offsetBMask"></param>
        /// <param name="cMask"></param>
        /// <param name="offsetCMask"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        /// <param name="transA"></param>
        /// <param name="transB"></param>
        void SgemmMask(CudaPieceFloat A, int offsetA, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC, int batchsize, int m, int n,
            CudaPieceInt aMask, int offsetAMask, CudaPieceInt bMask, int offsetBMask, CudaPieceInt cMask, int offsetCMask, float alpha, float beta, bool transA, bool transB);

        /// <summary>
        /// C = αlpha C +  βeta op ( A ) op ( B ) 
        /// </summary>
        /// <param name="AIndex">Sparse A Matrix </param>
        /// <param name="AFeaIndex">Sparse A Matrix </param>
        /// <param name="AFeaValue">Sparse A Matrix </param>
        /// <param name="B"></param>
        /// <param name="offsetB"></param>
        /// <param name="C"></param>
        /// <param name="offsetC"></param>
        /// <param name="batchsize"></param>
        /// <param name="m"></param>
        /// <param name="n"></param>
        /// <param name="aMask"></param>
        /// <param name="offsetAMask"></param>
        /// <param name="bMask"></param>
        /// <param name="offsetBMask"></param>
        /// <param name="cMask"></param>
        /// <param name="offsetCMask"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        /// <param name="transA"></param>
        /// <param name="transB"></param>
        void SparseSgemmMask(CudaPieceInt AIndex, CudaPieceInt AFeaIndex, CudaPieceFloat AFeaValue, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC, int batchsize, int m, int n,
            CudaPieceInt aMask, int offsetAMask, CudaPieceInt bMask, int offsetBMask, CudaPieceInt cMask, int offsetCMask, float alpha, float beta, bool transA, bool transB);

        /// <summary>
        /// (output, maxpoolingIndex) = MaxPooling(poolingvalues) 
        /// </summary>
        /// <param name="pooling_feas"></param>
        /// <param name="Smp_Index"></param>
        /// <param name="batchsize"></param>
        /// <param name="output"></param>
        /// <param name="maxpooling_index"></param>
        /// <param name="output_dimension"></param>
        void Max_Pooling(CudaPieceFloat pooling_feas, CudaPieceInt Smp_Index, int batchsize, CudaPieceFloat output, CudaPieceInt maxpooling_index, int output_dimension);

        /// <summary>
        /// (output, maxpoolingIndex) = MaxPooling(poolingvalues)
        /// </summary>
        /// <param name="poolingValues"></param>
        /// <param name="smpIdx"></param>
        /// <param name="smpMask"></param>
        /// <param name="batchsize"></param>
        /// <param name="output"></param>
        /// <param name="maxpoolingIndex"></param>
        /// <param name="output_dimension"></param>
        void MaxPoolingMask(CudaPieceFloat poolingValues, CudaPieceInt smpIdx, CudaPieceInt smpMask,
            int batchsize, CudaPieceFloat output, CudaPieceInt maxpoolingIndex, int output_dimension);

        /// <summary>
        /// poolingDeriv = alpha poolingDeriv + beta MaxPooling(output, maxpoolingIndex)'
        /// </summary>
        /// <param name="deriv"></param>
        /// <param name="maxpoolingIndex"></param>
        /// <param name="poolingValues"></param>
        /// <param name="batchsize"></param>
        /// <param name="output_dimension"></param>
        void DerivMaxPooling(CudaPieceFloat deriv, CudaPieceInt maxpoolingIndex, CudaPieceFloat poolingDeriv, int batchsize, int output_dimension, float alpha, float beta);


        /// <summary>
        /// Extract last step states in the sequential matrix
        /// matrix = lastStep (SeqMatrixs)
        /// </summary>
        /// <param name="InstanceIndex"></param>
        /// <param name="batchSize"></param>
        /// <param name="SeqMatrixs">SeqMatrixs: BatchSize * TimeSteps * Dim</param>
        /// <param name="dim"></param>
        /// <param name="matrix">BatchSize * Dim</param>
        void LastStepExtractSeqMatrix(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix);

        /// <summary>
        /// Extract last step states in the sequential matrix
        /// matrix = lastStep (SeqMatrixs)
        /// </summary>
        /// <param name="InstanceIndex"></param>
        /// <param name="SeqTransIdx"></param>
        /// <param name="batchSize"></param>
        /// <param name="SeqMatrixs"></param>
        /// <param name="dim"></param>
        /// <param name="matrix"></param>
        void LastStepExtractSeqMatrixMask(CudaPieceInt InstanceIndex, CudaPieceInt SeqTransIdx, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix,
            CudaPieceFloat matrixMask, int maskOffset);

        /// <summary>
        /// lastStep (SeqMatrixs) = alpha * lastStep (SeqMatrixs) + matrix
        /// </summary>
        /// <param name="InstanceIndex"></param>
        /// <param name="batchSize"></param>
        /// <param name="SeqMatrixs"></param>
        /// <param name="dim"></param>
        /// <param name="matrix"></param>
        /// <param name="alpha"></param>
        void DerivLastStepSeqMatrix(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix, float alpha);

        /// <summary>
        /// lastStep (SeqMatrixs) = alpha * lastStep (SeqMatrixs) + matrix
        /// </summary>
        /// <param name="InstanceIndex"></param>
        /// <param name="SeqTransIdx"></param>
        /// <param name="batchSize"></param>
        /// <param name="SeqMatrixs"></param>
        /// <param name="dim"></param>
        /// <param name="matrix"></param>
        /// <param name="alpha"></param>
        void DerivLastStepSeqMatrixMask(CudaPieceInt InstanceIndex, CudaPieceInt SeqTransIdx, int batchSize, CudaPieceFloat SeqMatrixs, int dim,
            CudaPieceFloat matrix, CudaPieceFloat matrixMask, int maskOffset, float alpha);


        /// <summary>
        /// b = softmax(a)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="labelDim"></param>
        /// <param name="batchsize"></param>
        /// <param name="gamma">warning : must be 1.</param>
        void SoftMax(CudaPieceFloat a, CudaPieceFloat b, int labelDim, int batchsize, float gamma);

        void DerivSoftmax(CudaPieceFloat softmaxData, CudaPieceFloat softmaxDeriv, CudaPieceFloat gradData, int dim, int batchsize, float alpha);

        /// <summary>
        /// outputProb = softmax(outputScore)
        /// </summary>
        /// <param name="smpIdx"></param>
        /// <param name="outputScore"></param>
        /// <param name="outputProb"></param>
        /// <param name="gamma"></param>
        /// <param name="batchSize"></param>
        void SparseSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputScore, CudaPieceFloat outputProb, float gamma, int batchSize);


        /// <summary>
        /// outputDeriv = probDeriv * softmax(outputProb)'
        /// </summary>
        /// <param name="smpIdx"></param>
        /// <param name="outputProb"></param>
        /// <param name="probDeriv"></param>
        /// <param name="outputDeriv"></param>
        /// <param name="gamma"></param>
        /// <param name="batchSize"></param>
        void DerivSparseMultiClassSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat probDeriv, CudaPieceFloat outputDeriv, float gamma, int batchSize, float alpha = 0, float beta = 1);

        /// <summary>
        /// outputDeriv = log_probDeriv * log sofmax(outputProb)'
        /// </summary>
        /// <param name="smpIdx"></param>
        /// <param name="outputProb"></param>
        /// <param name="logProbDeriv"></param>
        /// <param name="outputDeriv"></param>
        /// <param name="gamma"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        /// <param name="batchSize"></param>
        void DerivLogSparseSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat logProbDeriv, CudaPieceFloat outputDeriv, float gamma, float alpha, float beta, int batchSize);


        /// <summary>
        /// outputProb[idx] = gamma * (label[idx] - outputProb[idx]);
        /// </summary>
        /// <param name="outputProb"></param>
        /// <param name="dim"></param>
        /// <param name="batchSize"></param>
        /// <param name="smpProb">return probability per sample</param>
        /// <param name="label"></param>
        /// <param name="labelwei"></param>
        /// <param name="labelMask"></param>
        /// <param name="gamma"></param>
        /// <param name="eps"></param>
        /// <returns>Log Loss</returns>
        float LogLossDeriv(CudaPieceFloat outputProb, int dim, int batchSize, CudaPieceFloat smpProb,
            CudaPieceFloat label, CudaPieceFloat labelwei, CudaPieceInt labelMask, float gamma, float eps);

        /// <summary>
        /// deriv = (label - sigmoid(outputScore)) 
        /// </summary>
        /// <param name="outputScore"></param>
        /// <param name="outputLabel"></param>
        /// <param name="outputDeriv"></param>
        /// <param name="outputSize"></param>
        void DerivCrossEntropy(CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputDeriv, int outputSize);

        /// <summary>
        /// a = a * awei + b * bwei;
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="m"></param>
        /// <param name="awei"></param>
        /// <param name="bwei"></param>
        void Add_Vector(CudaPieceFloat a, CudaPieceFloat b, int m, float awei, float bwei);

        /// <summary>
        /// a = a * awei + b * bwei;
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="m"></param>
        /// <param name="awei"></param>
        /// <param name="bwei"></param>
        void Add_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int m, float awei, float bwei);

        /// <summary>
        /// a = a * awei + b;
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="m"></param>
        /// <param name="awei"></param>
        /// <param name="bwei"></param>
        void Init_Vector(CudaPieceFloat a, int offsetA, float b, int m, float awei);
        

        /// <summary>
        /// c = c * cwei + a * awei + b * bwei;
        /// </summary>
        /// <param name="a"></param>
        /// <param name="aMask"></param>
        /// <param name="b"></param>
        /// <param name="bMask"></param>
        /// <param name="c"></param>
        /// <param name="cMask"></param>
        /// <param name="dim"></param>
        /// <param name="batchSize"></param>
        /// <param name="awei"></param>
        /// <param name="bwei"></param>
        /// <param name="cwei"></param>
        void Matrix_AdditionMask(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAMask,
                                 CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBMask,
                                 CudaPieceFloat c, int offsetC, CudaPieceInt cMask, int offsetCMask,
                                 int dim, int batchSize, float awei, float bwei, float cwei);

        /// <summary>
        /// c = c * cwei + a * awei + b * bwei;
        /// </summary>
        /// <param name="a"></param>
        /// <param name="offsetA"></param>
        /// <param name="skipA"></param>
        /// <param name="b"></param>
        /// <param name="offsetB"></param>
        /// <param name="skipB"></param>
        /// <param name="c"></param>
        /// <param name="offsetC"></param>
        /// <param name="skipC"></param>
        /// <param name="dim"></param>
        /// <param name="batchSize"></param>
        /// <param name="awei"></param>
        /// <param name="bwei"></param>
        /// <param name="cwei"></param>
        void Matrix_AdditionEx(CudaPieceFloat a, int offsetA, int skipA,
                               CudaPieceFloat b, int offsetB, int skipB,
                               CudaPieceFloat c, int offsetC, int skipC,
                               int dim, int batchSize, float awei, float bwei, float cwei);

        /// <summary>
        /// c = c * cwei + a * awei + b * bwei;
        /// </summary>
        /// <param name="a"></param>
        /// <param name="aMask"></param>
        /// <param name="skipA"></param>
        /// <param name="b"></param>
        /// <param name="bMask"></param>
        /// <param name="skipB"></param>
        /// <param name="c"></param>
        /// <param name="cMask"></param>
        /// <param name="skipC"></param>
        /// <param name="dim"></param>
        /// <param name="batchSize"></param>
        /// <param name="awei"></param>
        /// <param name="bwei"></param>
        /// <param name="cwei"></param>
        void Matrix_AdditionExMask(CudaPieceFloat a, CudaPieceInt aMask, int skipA, 
                                   CudaPieceFloat b, CudaPieceInt bMask, int skipB, 
                                   CudaPieceFloat c, CudaPieceInt cMask, int skipC, 
                                   int dim, int batchSize, float awei, float bwei, float cwei);


        /// <summary>
        /// result = result + wei * \sum_i=0..row-1(matrix_i)
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="result">len : col</param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="wei"></param>
        void ColumnWiseSum(CudaPieceFloat matrix, CudaPieceFloat result, int row, int col, float wei);

        /// <summary>
        /// output = alpha output + beta weight * matrix
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="offsetM"></param>
        /// <param name="matrixMask"></param>
        /// <param name="offsetMM"></param>
        /// <param name="weight"></param>
        /// <param name="smpIdx"></param>
        /// <param name="batchSize"></param>
        /// <param name="output"></param>
        /// <param name="offsetO"></param>
        /// <param name="outputMask"></param>
        /// <param name="offsetOM"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        void ColumnWiseSumMask(CudaPieceFloat matrix, int offsetM, CudaPieceInt matrixMask, int offsetMM,
                CudaPieceFloat weight, CudaPieceInt smpIdx, int batchSize,
               CudaPieceFloat output, int offsetO, CudaPieceInt outputMask, int offsetOM,
               int row, int col, float alpha, float beta);

        /// <summary>
        /// output = alpha output + beta weight * matrix
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="offsetM"></param>
        /// <param name="matrixMask"></param>
        /// <param name="offsetMM"></param>
        /// <param name="weight"></param>
        /// <param name="smpIdx"></param>
        /// <param name="batchSize"></param>
        /// <param name="output"></param>
        /// <param name="offsetO"></param>
        /// <param name="outputMask"></param>
        /// <param name="offsetOM"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        void ColumnWiseSumMaskEx(CudaPieceFloat matrix, int offsetM, CudaPieceInt matrixMask, int offsetMM,
                CudaPieceFloat weight, int skipMatrix, CudaPieceInt smpIdx, int batchSize,
               CudaPieceFloat output, int offsetO, CudaPieceInt outputMask, int offsetOM,
               int skipOutput, int row, int col, float alpha, float beta);


        /// <summary>
        /// b = bwei b + awei * a;
        /// </summary>
        /// <param name="a"></param>
        /// <param name="aMask"></param>
        /// <param name="b"></param>
        /// <param name="bMask"></param>
        /// <param name="dim"></param>
        /// <param name="batchSize"></param>
        /// <param name="awei"></param>
        /// <param name="bwei"></param>
        void Scale_MatrixMask(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAM,
            CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBM,
            int dim, int batchSize, CudaPieceFloat awei, float bwei);

        /// b = bwei b + awei * a;
        void Scale_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int len, float awei, float bwei);


        /// <summary>
        /// b = b + awei * a; (in case bMask has overlap in index.)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="offsetA"></param>
        /// <param name="aMask"></param>
        /// <param name="offsetAM"></param>
        /// <param name="b"></param>
        /// <param name="offsetB"></param>
        /// <param name="bMask"></param>
        /// <param name="offsetBM"></param>
        /// <param name="dim"></param>
        /// <param name="batchSize"></param>
        /// <param name="awei"></param>
        void AccurateScale_Matrix(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAM,
            CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBM,
            int dim, int batchSize, CudaPieceFloat awei);

        /// <summary>
        /// y = alpha y + beta * matrix x;
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="x"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="y"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        void Sgemv(CudaPieceFloat matrix, CudaPieceFloat x, int row, int col, CudaPieceFloat y, bool transM, float alpha, float beta);


        /// <summary>
        /// output = tanh(output + bias)
        /// </summary>
        /// <param name="output"></param>
        /// <param name="bias"></param>
        /// <param name="batchsize"></param>
        /// <param name="outputDimension"></param>
        void Matrix_Add_Tanh(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension);

        /// <summary>
        /// output = output + bias
        /// </summary>
        /// <param name="output"></param>
        /// <param name="bias"></param>
        /// <param name="batchsize"></param>
        /// <param name="outputDimension"></param>
        void Matrix_Add_Linear(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension);

        /// <summary>
        /// output = ReLU(output + bias)
        /// </summary>
        /// <param name="output"></param>
        /// <param name="bias"></param>
        /// <param name="batchsize"></param>
        /// <param name="outputDimension"></param>
        void Matrix_Add_Rectified(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension);

        /// <summary>
        /// output = Sigmoid(output + bias)
        /// </summary>
        /// <param name="output"></param>
        /// <param name="bias"></param>
        /// <param name="batchsize"></param>
        /// <param name="outputDimension"></param>
        void Matrix_Add_Sigmoid(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Smp_Index"></param>
        /// <param name="batchsize"></param>
        /// <param name="Seg_Index"></param>
        /// <param name="Seg_Margin"></param>
        /// <param name="seg_size"></param>
        /// <param name="Fea_Index"></param>
        /// <param name="Fea_Value"></param>
        /// <param name="elementsize"></param>
        /// <param name="con_weight"></param>
        /// <param name="output"></param>
        /// <param name="Feature_dimension"></param>
        /// <param name="output_dimension"></param>
        /// <param name="win_size"></param>
        void Convolution_Sparse_Matrix_Multiply_INTEX(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, CudaPieceInt Seg_Margin, int seg_size, CudaPieceInt Fea_Index,
                                                   CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat con_weight, CudaPieceFloat output, 
                                                   int Feature_dimension, int output_dimension, int win_size);

        /// <summary>
        /// TopK Retrieve;
        /// </summary>
        /// <param name="float_array"></param>
        /// <param name="batchSize"></param>
        /// <param name="dim"></param>
        /// <param name="K"></param>
        /// <param name="mode"></param>
        /// <param name="bestValues"></param>
        /// <param name="bestIndexes"></param>
        void KLargestValueBatch(CudaPieceFloat float_array, int batchSize, int dim, int K, int mode, CudaPieceFloat bestValues, CudaPieceFloat bestIndexes);

        // Lookup Embedding Forward.
        void LookupForward(CudaPieceInt inputSmpIdx, CudaPieceInt inputItemIdx, int batchSize, CudaPieceFloat weight, CudaPieceFloat output, int inputDim, int outputDim);
        
        // Lookup Embedding Backward.
        void LookupBackward(CudaPieceInt inputSmpIdx, CudaPieceInt inputItemIdx, int batchSize, int inputItemNum, CudaPieceFloat deriv, int inputDim, int outputDim, CudaPieceFloat output, float lr);

        // sparse matrix addition.
        void SparseVectorAdd(CudaPieceInt idx, CudaPieceFloat value, CudaPieceFloat dst, float beta, int length);

        // sparse vector givens.
        //      y[xInd[i]] = c * y[xInd[i]] - s*xVal[i]
        //      x[i]               = c * xVal[i]            + s * y[xInd[i]]
        void SparseVectorGivens(CudaPieceInt idx, CudaPieceFloat value, CudaPieceFloat dst, float alpha, float beta, int length);

        // vector sum. 
        float VectorSum(CudaPieceFloat x, int index, int len, int norm);

        void NDArrayTranspose(CudaPieceFloat input, CudaPieceInt indim, CudaPieceInt transDim, CudaPieceFloat output, int dimNum, int length, float alpha, float beta);

        void Cosine_Similarity_Matching(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB,
            int aSize, int bSize, int matchSize, int dimension, CudaPieceFloat aa, CudaPieceFloat bb, float eps);

        void Inner_Product_Matching(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB,
            int aSize, int bSize, int matchSize, int dimension, float eps);

        void Inner_Product_Matching(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB,
            int aSize, int bSize, int matchSize, int dimension, float eps);

        void SoftAttention_Matching(CudaPieceFloat a, CudaPieceInt aMask, CudaPieceFloat b, CudaPieceInt bMask, int dim, int a_func, int op, CudaPieceFloat vec, 
            CudaPieceFloat c, CudaPieceInt cMask, int batchSize, float alpha, float beta);

        void DerivSoftAttention_Matching(CudaPieceFloat a, CudaPieceFloat aDeriv, CudaPieceInt aMask, CudaPieceFloat b, CudaPieceFloat bDeriv, CudaPieceInt bMask,
            int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat vecDeriv, CudaPieceFloat c, CudaPieceFloat cDeriv, CudaPieceInt cMask,
            CudaPieceInt aSmpIdx, CudaPieceInt aElementIdx, int aSize, CudaPieceInt bSmpIdx, CudaPieceInt bElementIdx, int bSize, int batchSize, float awei, float bwei, float vwei);

        void SoftAttention(CudaPieceFloat a, CudaPieceFloat b, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat c, int aBatchSize, int bBatchSize, float alpha, float beta);

        void DerivSoftAttention(CudaPieceFloat a, CudaPieceFloat aDeriv, CudaPieceFloat b, CudaPieceFloat bDeriv, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat vecDeriv,
            CudaPieceFloat c, CudaPieceFloat cDeriv, int aBatchSize, int bBatchSize, float awei, float bwei, float vwei);

        void LogBayesianRatingLoss(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize, CudaPieceFloat srcBatchLoss,
            CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputDeriv, uint outputSize, float eplison, float gamma);

        void BayesianRatingProb(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize, CudaPieceFloat srcBatchLoss,
            CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputProb, int outputSize, float eplison, float gamma);

        void MaxoutPooling(CudaPieceFloat input, int batchSize, int dim, CudaPieceInt maxpoolingIndex, int maxoutDim, int outDim, CudaPieceFloat output, int format);

        void DerivMaxoutPooling(CudaPieceFloat input_deriv, int batchSize, int dim, CudaPieceInt maxpoolingIndex, int maxoutDim, int outDim, CudaPieceFloat output_deriv);


        void CNNForwardPropagate(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth,
                            CudaPieceFloat filterMap, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride,
                            CudaPieceFloat imageOutBatch, int outHeight, int outWidth);

        void CuDNNBackwardPropagateData(CudaPieceFloat imageInBatchDeriv, int nBatch, int inFeatureMap, int inHeight, int inWidth,
                            CudaPieceFloat filterMap, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride,
                            CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth);

        void CuDNNBackwardPropagateFilter(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth,
                            CudaPieceFloat filterMapDeriv, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride,
                            CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth, float alphaValue, float betaValue);

        void CuDNNBackwardPropagateBias(CudaPieceFloat biasDeriv, CudaPieceFloat imageOutBatchDeriv, int batch, int outDepth, int outHeight, int outWidth, float alphaValue, float betaValue);


        void MatrixL1Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l1Score);
        void MatrixL2Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l2Score);

        void DerivMatrixL1Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l1Score, CudaPieceFloat l1ScoreDeriv, CudaPieceFloat xDeriv);
        void DerivMatrixL2Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l2Score, CudaPieceFloat l2ScoreDeriv, CudaPieceFloat xDeriv);

        void SpanMaxPool(CudaPieceFloat inputSent, CudaPieceInt smpIdx, CudaPieceFloat start, CudaPieceFloat end, CudaPieceInt sentMargin, int matchSize, CudaPieceFloat outputSent, CudaPieceInt outMaxIndex, int dim);

        void DerivSpanMaxPool(CudaPieceFloat outputSentDeriv, CudaPieceInt smpIdx, int batchSize, CudaPieceFloat inputSentDeriv, CudaPieceInt outMaxIndex, int dim, float beta);

        void SpanLastPool(CudaPieceFloat inputSent, CudaPieceInt smpIdx, CudaPieceFloat start, CudaPieceFloat end, CudaPieceInt sentMargin, int matchSize, CudaPieceFloat outputSent, int dim);

        void DerivSpanLastPool(
            CudaPieceFloat outputSentDeriv, CudaPieceInt outSmpIdx, CudaPieceFloat start, CudaPieceFloat end, int batchSize, CudaPieceFloat inputSentDeriv, CudaPieceInt inSmpIdx, int dim, float beta);

            //CudaPieceFloat outputSentDeriv, CudaPieceInt smpIdx, CudaPieceFloat start, CudaPieceFloat end, int batchSize, CudaPieceFloat inputSentDeriv, int dim, float beta);
        /// <summary>
        /// pDst = weiDst * pDst + weiLeft * pLeft + weiRight * pRight
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="offsetLeft"></param>
        /// <param name="pRight"></param>
        /// <param name="offsetRight"></param>
        /// <param name="pDst"></param>
        /// <param name="offsetDst"></param>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="weiLeft"></param>
        /// <param name="weiRight"></param>
        /// <param name="weiDst"></param>
        //void MatrixAddition(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst, int row, int column, float weiLeft, float weiRight, float weiDst);

        /*----------  Mainly used in forward activation, computing linear transformations  --------------*/
        void SEQ_Sparse_Matrix_Multiply_INTEX(SeqSparseBatchData data, CudaPieceFloat weight, CudaPieceFloat output, int inputDimension, int outputDimension, int winSize);
        void Convolution_Sparse_Matrix_Multiply_INTEX(SeqSparseBatchData data, CudaPieceFloat weight, CudaPieceFloat layerPoolingOutput, int inputDimension, int outputDimension, int winSize);


        void Max_Pooling(CudaPieceFloat layerPoolingOutput, SeqSparseBatchData data, CudaPieceFloat output, CudaPieceInt layerMaxPooling_Index, int outputDimension);

        void Matrix_Multipy(CudaPieceFloat input, CudaPieceFloat weight, CudaPieceFloat output, int batchsize, int inputDimension, int outputDimension, int inverse);


        /*----------  Mainly used in forward activation, computing loss function   ---------------*/
        void Cosine_Similarity(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize, int topLayerSize, float eps); //float.Epsilon);
        //void Cosine_Similarity_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize, int outputLayerSize, float eps); //float.Epsilon);
        //void Calculate_Alpha(CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA);
        //void Calculate_Alpha_MXE(CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA);
        //void Calculate_Alpha_NCE(CudaPieceFloat alpha, CudaPieceFloat dist, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA);
        //void Calculate_Alpha_NCE2(CudaPieceFloat alpha, CudaPieceFloat dist, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA);
        //void Calculate_Alpha_PAIRRANK(CudaPieceFloat alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma);
        //void FillOut_Dist_NCE(CudaPieceFloat dist, CudaPieceInt GPU_negative_index, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize);

        /*----------  Mainly used in backward propagation, computing top layer error derivative --------------*/
        //void Deriv_Cosine(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps);
        //void Derive_Cosine_Linear(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps);
        //void Derive_Cosine_Rectified(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps);
        //void Deriv_Cosine_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps);
        //void Derive_Cosine_Linear_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps);
        //void Derive_Cosine_Rectified_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps);
        //void Matrix_WeightAdd(CudaPieceFloat result, CudaPieceFloat addTerm, int batchsize, int outputLayerSize, CudaPieceFloat mweight, int start, int keep);
        //void Matrix_WeightAdd_EX(CudaPieceFloat result, CudaPieceFloat addTerm, CudaPieceInt GPU_Inver_negative_index, CudaPieceInt GPU_Inver_negative_value, int batchsize, int outputLayerSize, CudaPieceFloat mweight, int start, int keep);

        /*----------  Mainly used in backward propagation, computing layer error   --------------*/


        /*----------  Mainly used in backward propagation, computing weight derivative  --------------*/
        void SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(SeqSparseBatchData input_batch, CudaPieceFloat weightDeriv, CudaPieceFloat upperOutputErrorDeriv, int inputDimension, int outputDimension, int winSize);
        //void Convolution_Sparse_Matrix_Product_INTEX(CudaPieceFloat upperOutputErrorDeriv, CudaPieceInt layerMaxPooling_Index, SeqSparseBatchData input_batch, int winSize, int batchsize, int outputDimension, CudaPieceFloat weightDeriv, int inputDimension);
        //void Matrix_Product(CudaPieceFloat lowerOutput, CudaPieceFloat upperOutputErrorDeriv, CudaPieceFloat weightDeriv, int batchsize, int inputDimension, int outputDimension);

        /*----------  Mainly used in backward propagation, computing weight updates and updating weights --------------*/

        void Matrix_Mask(CudaPieceFloat output, CudaPieceInt dropoutMask, int batchSize, int dim);
             
        void Scale_Matrix(CudaPieceFloat matrix, int inputDimension, int outputDimnsion, float momentum);
        void Matrix_Add(CudaPieceFloat matrix, CudaPieceFloat updates, int inputDimension, int outputDimnsion, float learning_rate);

        /*----------  Mainly used in backward propagation in MultiRegression Task --------------*/
        //void Sparse2Dense_Matrix(SeqSparseBatchData data, CudaPieceFloat matrix, int batchsize, int outputDimension);

        void Zero(CudaPieceFloat matrix, int size);

        //void Matrix_Aggragate(CudaPieceFloat a, CudaPieceFloat b, int batchsize, int m);

        //void Cosine_Similarity_EX_Full(CudaPieceFloat a, CudaPieceFloat b, CudaPieceInt neg_list, CudaPieceFloat c, int nTrial, int BATCHSIZE,
        //        int batchsize, int dimension, float eps);

        //void FillOut_Dist_NCE_Full(CudaPieceFloat dist, CudaPieceInt neg_list, int nTrail, int BATCH_SIZE, int batchsize);

        //void Deriv_Cosine_EX_Full(CudaPieceFloat q, CudaPieceFloat d, CudaPieceInt neg_list, CudaPieceFloat dcq,
        //        CudaPieceFloat dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps);

        //void Deriv_Cosine_Linear_EX_Full(CudaPieceFloat q, CudaPieceFloat d, CudaPieceInt neg_list, CudaPieceFloat dcq, CudaPieceFloat dcd,
        //        int nTrail, int BATCHSIZE, int batchsize, int m, float eps);

        //void Deriv_Cosine_Rectified_EX_Full(CudaPieceFloat q, CudaPieceFloat d, CudaPieceInt neg_list, CudaPieceFloat dcq, CudaPieceFloat dcd,
        //        int nTrail, int BATCHSIZE, int batchsize, int m, float eps);

        //void Matrix_WeightAdd_Full(CudaPieceFloat gpu_floats_a, CudaPieceFloat gpu_floats_b, int nTrail, int BATCHSIZE, int batchsize, int dimension,
        //        CudaPieceFloat mweight, int start, int keep);

        //void Matrix_WeightAdd_EX_Full(CudaPieceFloat gpu_floats_a, CudaPieceFloat gpu_floats_b, CudaPieceInt inver_neg_index,
        //        CudaPieceInt inver_neg_value, int nTrial, int BATCHSIZE, int batchsize, int dimension, CudaPieceFloat mweight, int start, int keep);

        //void Cosine_Similarity_SubSpace(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int labelDim, int BATCHSIZE, int batchsize, int subspaceDim, float eps);

        //void Deriv_Cosine_Subspace(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat dcq, CudaPieceFloat dcd, CudaPieceFloat alpha, int act_type, int batchsize, int labelDim, int subspaceDim, float gamma, float eps);

        void InnerProduct_Similarity(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int batchsize, int dimension);

        //void Deriv_InnerProduct(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat dcq, CudaPieceFloat dcd, CudaPieceFloat alpha, int act_type, int batchsize, int Dim, float gamma, float eps);

        //void Matrix_Add_OFFSET(CudaPieceFloat a, int offset_a, CudaPieceFloat b, int offset_b, int len, float mweight);



        void Sparse_Matrix_Multiply_INTEX_Weight(CudaPieceInt outputSmpIndex, CudaPieceInt outputItemIndex, CudaPieceFloat outputDeriv, int batchsize,
                int outputItemNum, CudaPieceFloat weight, CudaPieceFloat hidden_deriv, int hiddenDim, float wei);

        void Square_Matrix(CudaPieceFloat a, int batchSize, int dim, CudaPieceFloat aSquare);

        //void Joint_Sparse_Matrix_Multiply_INTEX(
        //    CudaPieceFloat Src_Input, int SrcSize,
        //    CudaPieceFloat Tgt_Input, int TgtSize,
        //    CudaPieceInt Src_Match_Idx, CudaPieceInt Tgt_Match_Idx, CudaPieceFloat Match_Score, int MatchSize,
        //    CudaPieceInt ConSmp_Idx, CudaPieceInt ConFea_Idx, CudaPieceFloat ConFea_Value,
        //    int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
        //    CudaPieceFloat Weight, CudaPieceFloat Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim);

        //void Joint_Dense_Matrix_Multiply_INTEX(
        //    CudaPieceFloat Src_Input, int SrcSize,
        //    CudaPieceFloat Tgt_Input, int TgtSize,
        //    CudaPieceInt Src_Match_Idx, CudaPieceInt Tgt_Match_Idx, CudaPieceFloat Match_Score, int MatchSize,
        //    CudaPieceFloat ConFea_Value,
        //    int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
        //    CudaPieceFloat Weight, CudaPieceFloat Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim);

        void RecursiveForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize,
                                    CudaPieceFloat SeqInputMatrixs, int dim,
                                    CudaPieceFloat WMatrix, int lag, int af, CudaPieceFloat SeqOutputMatrix);

        void RecursiveBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize,
                                            CudaPieceFloat SeqOutputMatrixs, int dim,
                                            CudaPieceFloat WMatrix, int lag, int af, CudaPieceFloat SeqDerivMatrixs);

        /// <summary>
        ///  pDst = pDst * weight + pLeft * pRight'
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="pRight"></param>
        /// <param name="pDst"></param>
        /// <param name="leftRowCnt"></param>
        /// <param name="rightColumnCnt"></param>
        /// <param name="rightRowCnt"></param>
        /// <param name="weight"></param>
        void MatrixMultiplicationTranspose(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt, float weight);

        /// <summary>
        /// pDst = pDst + pLeft' * pRight * wei
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="pRight"></param>
        /// <param name="pDst"></param>
        /// <param name="leftRowCnt"></param>
        /// <param name="leftColumnCnt"></param>
        /// <param name="rightColumnCnt"></param>
        /// <param name="weiDst"></param>
        /// <param name="wei"></param>
        void MatrixMultiplicationLeftTranspose(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float wei);


        /// <summary>
        /// (pRowIndex, RowNum, pColumnIndex, pValue, elementsize) -> Sparse Matrix S
        /// pDst = pDst + weight * S' * pRight;
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="pRight"></param>
        /// <param name="pDst"></param>
        /// <param name="leftRowCnt"></param>
        /// <param name="leftColumnCnt"></param>
        /// <param name="rightColumnCnt"></param>
        /// <param name="wei"></param>
        void MatrixMultiplicationSparseLeftTranspose(CudaPieceInt pRowIndex, int RowNum, CudaPieceInt pColumnIndex,
                CudaPieceFloat pValue, int elementsize, CudaPieceFloat pRight, CudaPieceFloat pDst, int leftColumnCnt, int rightColumnCnt, float wei);


        /// <summary>
        /// grad = grad + learnRate * deriv * input ( Fea_Index Fea_Value, Seg_Index,)
        /// </summary>
        /// <param name="deriv"></param>
        /// <param name="maxpooling_index"></param>
        /// <param name="Seg_Index"></param>
        /// <param name="SegMargin_Index"></param>
        /// <param name="seg_size"></param>
        /// <param name="win_size"></param>
        /// <param name="batchsize"></param>
        /// <param name="output_dimension"></param>
        /// <param name="Fea_Index"></param>
        /// <param name="Fea_Value"></param>
        /// <param name="grad"></param>
        /// <param name="Feature_Dimension"></param>
        /// <param name="learnRate"></param>
        void Convolution_Sparse_Matrix_Product_INTEX_Weight(
            CudaPieceFloat deriv, CudaPieceInt maxpooling_index, CudaPieceInt Seg_Index, CudaPieceInt SegMargin_Index, int seg_size, int win_size,
                                        int batchsize, int output_dimension, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, CudaPieceFloat grad, int Feature_Dimension, float learnRate);

        //void Matrix_Product_Weight(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int batchsize, int m, int n, float weight);

        void SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index,
            int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize,
            CudaPieceFloat mul_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension, float weight);

        void SEQ_Sparse_Matrix_Multiply_INT(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index,
            int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize,
            CudaPieceFloat mul_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension);

        void RecursiveUpdateBatch(CudaPieceInt InstanceIndex, int batchSize, int seqSize, CudaPieceFloat SeqOutputMatrixs, int dim, CudaPieceFloat SeqDerivMatrixs, int lag, CudaPieceFloat gradient, float weight);



        void ClipVector(CudaPieceFloat gpu_floats_a, int m, float maxThreshold, float minThreshold);



        void AddAndClear(CudaPieceFloat a, CudaPieceFloat b, int m, float bwei);


        void Ada_Gradient(CudaPieceFloat ada, CudaPieceFloat grad, int m);

        //void Inner_Product_EX_Full(CudaPieceFloat a, CudaPieceFloat b, CudaPieceInt neg_list, CudaPieceFloat c, int nTrial, int BATCHSIZE,
        //    int batchsize, int dimension, float eps);

        /*---------- used for LSTM  --------------*/

        void LSTMForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c,
            CudaPieceFloat gate_o, CudaPieceFloat tanhc, int cell, CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo);
        void LSTMBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int seqSize, int cell,
            CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc,
            CudaPieceFloat deriv_o, CudaPieceFloat deriv_gate_i, CudaPieceFloat deriv_cHat, CudaPieceFloat deriv_gate_f, CudaPieceFloat deriv_c, CudaPieceFloat deriv_gate_o, CudaPieceFloat deriv_tanhc,
            CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo);
        void RNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInput, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOut);
        void DerivRNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInputDeriv, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOutDeriv);
        void UpdateRNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInput, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOutDeriv, float wei);

        void Calculate_SampleCrossEntropyDeriv(CudaPieceFloat output, CudaPieceFloat deriv, int dim, CudaPieceFloat label, int batchsize, float gamma);

        /*---------- used for GRU  --------------*/
        void GRUForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int Dim, //->x;
            CudaPieceFloat H, CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat HHat, CudaPieceFloat RestH,
            CudaPieceFloat Ur, CudaPieceFloat Uz, CudaPieceFloat Uh);

        void GRUBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int Dim,//->x
            CudaPieceFloat H, CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat HHat, CudaPieceFloat RestH, //H @ R
            CudaPieceFloat DerivH, CudaPieceFloat DerivGateR, CudaPieceFloat DerivGateZ, CudaPieceFloat DerivHHat, CudaPieceFloat DerivRestH,
            CudaPieceFloat Ur, CudaPieceFloat Uz, CudaPieceFloat Uh);

        void LSTMForwardPropagateBatchV2(CudaPieceInt InstanceIndex, int batchSize,  //->x;
            CudaPieceFloat o, //-> output,
            CudaPieceFloat gate_i, //->gate i,
            CudaPieceFloat c_hat, //->candidate memory,
            CudaPieceFloat gate_f, //->forget gate,
            CudaPieceFloat c, //->memory,
            CudaPieceFloat gate_o, //->gate o,
            CudaPieceFloat tanhc, //->tanh memory
            int cell,
            CudaPieceFloat Ui,
            CudaPieceFloat Uc,
            CudaPieceFloat Uf,
            CudaPieceFloat Uo, CudaPieceFloat Vo,
            CudaPieceInt RecursiveIdx, CudaPieceInt RecursiveMatrix, int MaxLag);

        void LSTMBackwardPropagateBatchV2(CudaPieceInt InstanceIndex, int batchSize, int seqSize, int cell,
            CudaPieceFloat o,
            CudaPieceFloat gate_i,
            CudaPieceFloat c_hat,
            CudaPieceFloat gate_f,
            CudaPieceFloat c,
            CudaPieceFloat gate_o,
            CudaPieceFloat tanhc,

            CudaPieceFloat deriv_o,
            CudaPieceFloat deriv_gate_i,
            CudaPieceFloat deriv_cHat,
            CudaPieceFloat deriv_gate_f,
            CudaPieceFloat deriv_c,
            CudaPieceFloat deriv_gate_o,
            CudaPieceFloat deriv_tanhc,

            CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo,
            CudaPieceInt RecursiveIdx, CudaPieceInt RecursiveMatrix, int MaxLag);

        void Deriv_CosineSimilarity_Matching(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat qSquare, CudaPieceFloat dSquare, int dim,
            CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, CudaPieceInt tgt2MatchIdx, CudaPieceInt tgt2MatchElement, CudaPieceInt srcIdx, CudaPieceInt tgtIdx, int srcSize, int tgtSize, int matchSize,
            CudaPieceFloat simi, CudaPieceFloat derivSimi, CudaPieceFloat dcq, CudaPieceFloat dcd, float eps);

        void Deriv_InnerProduct_Matching(CudaPieceFloat q, CudaPieceFloat d, int dim,
            CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, CudaPieceInt tgt2MatchIdx, CudaPieceInt tgt2MatchElement, CudaPieceInt srcIdx, CudaPieceInt tgtIdx, int srcSize, int tgtSize, int matchSize,
            CudaPieceFloat derivSimi, CudaPieceFloat dcq, CudaPieceFloat dcd, float eps);
        float L2Norm(CudaPieceFloat x, int size);
        void RMSPropGradient(CudaPieceFloat ada, CudaPieceFloat grad, float gamma, float epsilon, int m);
        void RMSPropV2_Gradient(CudaPieceFloat ada, CudaPieceFloat g, CudaPieceFloat grad, float gamma, float epsilon, int m);

        void VectorSquareAdd(CudaPieceFloat Z, CudaPieceFloat X, CudaPieceFloat Y, float wx, float wy, int m);
        void AdaMGradient(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat G, float alpha, float beta1, float beta2, float epsilon, int t, int size);
        void AdaDeltaGradient(CudaPieceFloat deltaX, CudaPieceFloat AccumGrad, CudaPieceFloat AccumUpdate, CudaPieceFloat Grad, float epsilon, int size);
        void AdaMax(CudaPieceFloat V, CudaPieceFloat M, CudaPieceFloat G, float alpha, float beta, float epsilon, int size);

        /// <summary>
        /// grad_t = Min(Max(-gradClip, grad_t), gradClip)        
        /// Ada_t = adaWeight * Ada_(t-1) + (1-adaWeight)*(grad_t)^2
        /// param_t = param_(t-1)+eta*grad_t/sqrt(Ada_t)
        /// param_t = Min(Max(-weightClip, param_t), weightClip)
        /// grad_t = 0
        /// </summary>
        /// <param name="ada"></param>
        /// <param name="grad"></param>
        /// <param name="param"></param>
        /// <param name="updateRate"></param>
        /// <param name="gradClip"></param>
        /// <param name="weightClip"></param>
        void ClipAdaGradUpdate(CudaPieceFloat ada, CudaPieceFloat grad, CudaPieceFloat param, float updateRate, float gradClip, float weightClip);

        void ClipAdamUpdate(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat grad, CudaPieceFloat param, float beta1, float beta2, int iter, float updateRate, float gradClip, float weightClip);

        /// <summary>
        /// matrix = alpha * matrix + beta * order(IsReverse(seqMatrixs))
        /// </summary>
        /// <param name="InstanceIndex"></param>
        /// <param name="batchSize"></param>
        /// <param name="SeqMatrixs"></param>
        /// <param name="dim"></param>
        /// <param name="isReverse"></param>
        /// <param name="order"></param>
        /// <param name="matrix"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        void GetSeqOrderMatrixs(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, CudaPieceInt MapForward, int dim, int isReverse, int order, CudaPieceFloat matrix, float alpha, float beta);

        /// <summary>
        /// matrix = concate the winsize of SeqMatrixs 
        /// </summary>
        /// <param name="InstanceMargin"></param>
        /// <param name="sentsize"></param>
        /// <param name="SeqMatrixs"></param>
        /// <param name="mapForward"></param>
        /// <param name="dim"></param>
        /// <param name="winsize"></param>
        /// <param name="matrix"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        void GetWindowMatrixs(CudaPieceInt InstanceMargin, int sentsize, CudaPieceFloat SeqMatrixs, CudaPieceInt mapForward, int dim, int winsize, CudaPieceFloat matrix, float alpha, float beta);

        /// <summary>
        /// SeqMatrixs = aggragate winsize of matrix. 
        /// </summary>
        /// <param name="InstanceMargin"></param>
        /// <param name="sentsize"></param>
        /// <param name="SeqMatrixs"></param>
        /// <param name="mapForward"></param>
        /// <param name="dim"></param>
        /// <param name="winsize"></param>
        /// <param name="matrix"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        void SetWindowMatrixs(CudaPieceInt InstanceMargin, int sentsize, CudaPieceFloat SeqMatrixs, CudaPieceInt mapForward, int dim, int winsize, CudaPieceFloat matrix, float alpha, float beta);

        /// <summary>
        /// order(IsReverse(seqMatrixs)) = alpha * order(IsReverse(seqMatrixs)) + beta * matrix;
        /// </summary>
        /// <param name="InstanceIndex"></param>
        /// <param name="batchSize"></param>
        /// <param name="SeqMatrixs"></param>
        /// <param name="dim"></param>
        /// <param name="isReverse"></param>
        /// <param name="order"></param>
        /// <param name="matrix"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        void SetSeqOrderMatrixs(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, CudaPieceInt MapForward, int dim, int isReverse, int order, CudaPieceFloat matrix, float alpha, float beta);

        void MomentumUpdate(CudaPieceFloat weight, CudaPieceFloat momentum, float momentumRate);

        void NAGUpdate(CudaPieceFloat weight, CudaPieceFloat momentum, CudaPieceFloat trueWeight, float momentumRate);

        void DerivLogProbEntropy(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat logProbDeriv, float alpha, float beta, float epislon, int batchSize);

    }

    public static class MathOperatorManager
    {
        static IMathOperationManager globalInstance = null;

        public static DeviceType MathDevice = DeviceType.GPU;

        public static DeviceType SetDevice(int deviceID)
        {
            MathDevice = Device(deviceID);
            switch (MathDevice)
            {
                case DeviceType.GPU:
                    Cudalib.CudaInit(deviceID);
                    break;
            }
            return MathDevice;
        }

        public static DeviceType Device(int deviceID)
        {
            if (deviceID >= 0) { return DeviceType.GPU; }
            else if (deviceID == -1) { return DeviceType.CPU; }
            else { return DeviceType.CPU_FAST_VECTOR; }
        }

        public static IMathOperationManager CreateInstance(DeviceType device)
        {
            switch (device)
            {
                case DeviceType.CPU:
                    return new BasicMathOperation();
                case DeviceType.GPU:
                    return new CudaMathOperation(true, true);
                case DeviceType.CPU_FAST_VECTOR:
                    return new FastVectorOperation();
            }
            return new FastVectorOperation();
        }

        /// <summary>
        /// Please try to avoid calling GlobalInstance. It is not safe for GPU. 
        /// Recommend to use ComputeLib in each runner.
        /// </summary>
        public static IMathOperationManager GlobalInstance
        {
            get
            {
                if (globalInstance == null)
                {
                    switch (MathDevice)
                    {
                        case DeviceType.GPU:
                            globalInstance = new CudaMathOperation(); // true, true);
                            break;
                        case DeviceType.CPU:
                            globalInstance = new BasicMathOperation();
                            break;
                        case DeviceType.CPU_FAST_VECTOR:
                            globalInstance = new FastVectorOperation();
                            break;
                        default:
                            throw new Exception("Error! Unknown Math_LIB " + MathDevice);
                    }
                }
                return globalInstance;
            }
        }
    }

}
