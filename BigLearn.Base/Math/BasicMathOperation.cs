﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BigLearn
{

    public class BasicMathOperation : IMathOperationManager
    {
        //public void Joint_Dense_Matrix_Multiply_INTEX(
        //    CudaPieceFloat Src_Input, int SrcSize,
        //    CudaPieceFloat Tgt_Input, int TgtSize,
        //    CudaPieceInt Src_Match_Idx, CudaPieceInt Tgt_Match_Idx, CudaPieceFloat Match_Score, int MatchSize,
        //    CudaPieceFloat ConFea_Value,
        //    int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
        //    CudaPieceFloat Weight, CudaPieceFloat Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim)
        //{
        //    BasicMathlib.Joint_Dense_Matrix_Multiply_INTEX(Src_Input.MemPtr, SrcSize, Tgt_Input.MemPtr, TgtSize, Src_Match_Idx.MemPtr,
        //            Tgt_Match_Idx.MemPtr, Match_Score.MemPtr, MatchSize, ConFea_Value.MemPtr, SrcDimension, TgtDimension,
        //            ConDimension, IsConScore, Weight.MemPtr, Output.MemPtr, OutputDimension, IsSrc, IsTgt, IsSim);
        //}

        //public void Joint_Sparse_Matrix_Multiply_INTEX(
        //    CudaPieceFloat Src_Input, int SrcSize,
        //    CudaPieceFloat Tgt_Input, int TgtSize,
        //    CudaPieceInt Src_Match_Idx, CudaPieceInt Tgt_Match_Idx, CudaPieceFloat Match_Score, int MatchSize,
        //    CudaPieceInt ConSmp_Idx, CudaPieceInt ConFea_Idx, CudaPieceFloat ConFea_Value,
        //    int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
        //    CudaPieceFloat Weight, CudaPieceFloat Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim)
        //{
        //    BasicMathlib.Joint_Sparse_Matrix_Multiply_INTEX(Src_Input.MemPtr, SrcSize, Tgt_Input.MemPtr, TgtSize,
        //        Src_Match_Idx.MemPtr, Tgt_Match_Idx.MemPtr, Match_Score.MemPtr, MatchSize, ConSmp_Idx.MemPtr,
        //        ConFea_Idx.MemPtr, ConFea_Value.MemPtr, SrcDimension, TgtDimension, ConDimension, IsConScore, Weight.MemPtr,
        //        Output.MemPtr, OutputDimension, IsSrc, IsTgt, IsSim);
        //}
        static BasicMathOperation()
        {
            int minThreads;
            int minCPThreads;
            ThreadPool.GetMinThreads(out minThreads, out minCPThreads);
            int maxThreads;
            int maxCPThreads;
            ThreadPool.GetMaxThreads(out maxThreads, out maxCPThreads);
            int availableThreads;
            int availableCPThreads;
            ThreadPool.GetAvailableThreads(out availableThreads, out availableCPThreads);

            ThreadPool.SetMaxThreads(maxThreads, maxCPThreads);
            ThreadPool.SetMinThreads(Environment.ProcessorCount, minCPThreads);
        }

        public void Square_Matrix(CudaPieceFloat a, int batchSize, int dim, CudaPieceFloat aSquare)
        {
            BasicMathlib.Square_Matrix(a.MemPtr, batchSize, dim, aSquare.MemPtr);
        }

        public void Inner_Product_Matching(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB,
            int aSize, int bSize, int matchSize, int dimension, float eps)
        {
            BasicMathlib.Inner_Product_Matching(a.MemPtr, b.MemPtr, c.MemPtr, matchIdxA.MemPtr, matchIdxB.MemPtr, aSize, bSize, matchSize, dimension, eps);
        }

        public void Cosine_Similarity_Matching(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB,
            int aSize, int bSize, int matchSize, int dimension, CudaPieceFloat aa, CudaPieceFloat bb, float eps)
        {
            BasicMathlib.Cosine_Similarity_Matching(a.MemPtr, b.MemPtr, c.MemPtr, matchIdxA.MemPtr, matchIdxB.MemPtr, aSize, bSize, matchSize, dimension, aa.MemPtr, bb.MemPtr, eps);
        }

        //public void Cosine_Similarity_EX_Full(CudaPieceFloat a, CudaPieceFloat b, CudaPieceInt neg_list, CudaPieceFloat c, int nTrial, int BATCHSIZE,
        //        int batchsize, int dimension, float eps)
        //{
        //    BasicMathlib.Cosine_Similarity_EX_Full(a.MemPtr, b.MemPtr, neg_list.MemPtr, c.MemPtr, nTrial, BATCHSIZE, batchsize, dimension, eps);
        //}
        //public void FillOut_Dist_NCE_Full(CudaPieceFloat dist, CudaPieceInt neg_list, int nTrail, int BATCH_SIZE, int batchsize)
        //{
        //    BasicMathlib.FillOut_Dist_NCE_Full(dist.MemPtr, neg_list.MemPtr, nTrail, BATCH_SIZE, batchsize);
        //}
        //public void Deriv_Cosine_EX_Full(CudaPieceFloat q, CudaPieceFloat d, CudaPieceInt neg_list, CudaPieceFloat dcq,
        //        CudaPieceFloat dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
        //{
        //    BasicMathlib.Deriv_Cosine_EX_Full(q.MemPtr, d.MemPtr, neg_list.MemPtr, dcq.MemPtr, dcd.MemPtr, nTrail, BATCHSIZE, batchsize, m, eps);
        //}
        //public void Deriv_Cosine_Linear_EX_Full(CudaPieceFloat q, CudaPieceFloat d, CudaPieceInt neg_list, CudaPieceFloat dcq, CudaPieceFloat dcd,
        //        int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
        //{
        //    BasicMathlib.Deriv_Cosine_Linear_EX_Full(q.MemPtr, d.MemPtr, neg_list.MemPtr, dcq.MemPtr, dcd.MemPtr, nTrail, BATCHSIZE, batchsize, m, eps);
        //}
        //public void Deriv_Cosine_Rectified_EX_Full(CudaPieceFloat q, CudaPieceFloat d, CudaPieceInt neg_list, CudaPieceFloat dcq, CudaPieceFloat dcd,
        //        int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
        //{
        //    BasicMathlib.Deriv_Cosine_Rectified_EX_Full(q.MemPtr, d.MemPtr, neg_list.MemPtr, dcq.MemPtr, dcd.MemPtr, nTrail, BATCHSIZE, batchsize, m, eps);
        //}
        //public void Matrix_WeightAdd_Full(CudaPieceFloat gpu_floats_a, CudaPieceFloat gpu_floats_b, int nTrail, int BATCHSIZE, int batchsize, int dimension,
        //        CudaPieceFloat mweight, int start, int keep)
        //{
        //    BasicMathlib.Matrix_WeightAdd_Full(gpu_floats_a.MemPtr, gpu_floats_b.MemPtr, nTrail, BATCHSIZE, batchsize, dimension,
        //        mweight.MemPtr, start, keep);
        //}
        //public void Matrix_WeightAdd_EX_Full(CudaPieceFloat gpu_floats_a, CudaPieceFloat gpu_floats_b, CudaPieceInt inver_neg_index,
        //        CudaPieceInt inver_neg_value, int nTrial, int BATCHSIZE, int batchsize, int dimension, CudaPieceFloat mweight,
        //        int start, int keep)
        //{
        //    BasicMathlib.Matrix_WeightAdd_EX_Full(gpu_floats_a.MemPtr, gpu_floats_b.MemPtr, inver_neg_index.MemPtr,
        //        inver_neg_value.MemPtr, nTrial, BATCHSIZE, batchsize, dimension, mweight.MemPtr, start, keep);
        //}

        public void SEQ_Sparse_Matrix_Multiply_INTEX(SeqSparseBatchData data, CudaPieceFloat weight, CudaPieceFloat output, int inputDimension, int outputDimension, int winSize)
        {
            BasicMathlib.SEQ_Sparse_Matrix_Multiply_INTEX(data.Sample_Idx_Mem, data.BatchSize, data.Seg_Idx_Mem, data.Seg_Margin_Mem, data.SentSize, data.Fea_Idx_Mem, data.Fea_Value_Mem, data.ElementSize,
                                        weight.MemPtr, output.MemPtr, inputDimension, outputDimension, winSize);
        }

        public void Convolution_Sparse_Matrix_Multiply_INTEX(SeqSparseBatchData data, CudaPieceFloat weight, CudaPieceFloat layerPoolingOutput, int inputDimension, int outputDimension, int winSize)
        {
            BasicMathlib.Convolution_Sparse_Matrix_Multiply_INTEX(data.Sample_Idx_Mem, data.BatchSize, data.Seg_Idx_Mem, data.Seg_Margin_Mem, data.SentSize, data.Fea_Idx_Mem, data.Fea_Value_Mem, data.ElementSize,
                            weight.MemPtr, layerPoolingOutput.MemPtr, inputDimension, outputDimension, winSize);

        }

        public void Max_Pooling(CudaPieceFloat layerPoolingOutput, SeqSparseBatchData data, CudaPieceFloat output, CudaPieceInt layerMaxPooling_Index, int outputDimension)
        {
            BasicMathlib.Max_Pooling(layerPoolingOutput.MemPtr, data.Sample_Idx_Mem, data.BatchSize, output.MemPtr, layerMaxPooling_Index.MemPtr, outputDimension);
        }

        public unsafe void Matrix_Multipy(CudaPieceFloat input, CudaPieceFloat weight, CudaPieceFloat output, int batchsize, int inputDimension, int outputDimension, int inverse)
        {
            BasicMathlib.Matrix_Multipy(input.CpuPtr, weight.CpuPtr, output.CpuPtr, batchsize, inputDimension, outputDimension, 0, 1, inverse);
        }

        public void Matrix_Add_Tanh(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension)
        {
            BasicMathlib.Matrix_Add_Tanh(output.MemPtr, bias.MemPtr, batchsize, outputDimension);
        }

        public void Matrix_Add_Linear(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension)
        {
            BasicMathlib.Matrix_Add_Vector(output.MemPtr, bias.MemPtr, batchsize, outputDimension);
        }

        public void Matrix_Add_Rectified(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension)
        {
            BasicMathlib.Matrix_Rectified_Vector(output.MemPtr, bias.MemPtr, batchsize, outputDimension);
        }

        public void Cosine_Similarity(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize, int topLayerSize, float eps)
        {
            BasicMathlib.Cosine_Similarity(srcTopLayerOutput.MemPtr,
                    tgtTopLayerOutput.MemPtr, alpha.MemPtr, nTrailPlus1, BATCH_SIZE, mIndex,
                    batchsize, topLayerSize, eps);
        }

        //public void Cosine_Similarity_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize, int topLayerSize, float eps)
        //{
        //    BasicMathlib.Cosine_Similarity_EX(srcTopLayerOutput.MemPtr,
        //            tgtTopLayerOutput.MemPtr, GPU_negative_index.MemPtr, alpha.MemPtr, nTrailPlus1, BATCH_SIZE, mIndex,
        //            batchsize, topLayerSize, eps);
        //}
        //public void Calculate_Alpha(CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        //{
        //    BasicMathlib.Calculate_Alpha(alpha.MemPtr, nTrailPlus1, BATCH_SIZE, batchsize, GAMMA);
        //}
        //public void Calculate_Alpha_MXE(CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        //{
        //    BasicMathlib.Calculate_Alpha_MXE(alpha.MemPtr, nTrailPlus1, BATCH_SIZE, batchsize, GAMMA);
        //}
        //public void Calculate_Alpha_NCE(CudaPieceFloat alpha, CudaPieceFloat dist, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        //{
        //    BasicMathlib.Calculate_Alpha_NCE(alpha.MemPtr, dist.MemPtr, nTrailPlus1, BATCH_SIZE, batchsize, GAMMA);
        //}
        //public void Calculate_Alpha_NCE2(CudaPieceFloat alpha, CudaPieceFloat dist, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        //{
        //    BasicMathlib.Calculate_Alpha_NCE2(alpha.MemPtr, dist.MemPtr, nTrailPlus1, BATCH_SIZE, batchsize, GAMMA);
        //}
        //public void Calculate_Alpha_PAIRRANK(CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        //{
        //    BasicMathlib.Calculate_Alpha_PAIRRANK(alpha.MemPtr, nTrailPlus1, BATCH_SIZE, batchsize, GAMMA);
        //}
        //public void FillOut_Dist_NCE(CudaPieceFloat dist, CudaPieceInt GPU_negative_index, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize)
        //{
        //    BasicMathlib.FillOut_Dist_NCE(dist.MemPtr, GPU_negative_index.MemPtr, nTrailPlus1, BATCH_SIZE, mIndex, batchsize);
        //}
        //public void Deriv_Cosine(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        //{
        //    BasicMathlib.Deriv_Cosine(srcTopLayerOutput.MemPtr, tgtTopLayerOutput.MemPtr, srcTopLayerOutputDeriv.MemPtr, tgtTopLayerOutputDeriv.MemPtr, batchsize, outputLayerSize, eps);
        //}
        //public void Derive_Cosine_Linear(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        //{
        //    BasicMathlib.Derive_Cosine_Linear(srcTopLayerOutput.MemPtr, tgtTopLayerOutput.MemPtr, srcTopLayerOutputDeriv.MemPtr, tgtTopLayerOutputDeriv.MemPtr, batchsize, outputLayerSize, eps);
        //}
        //public void Derive_Cosine_Rectified(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        //{
        //    BasicMathlib.Derive_Cosine_Rectified(srcTopLayerOutput.MemPtr, tgtTopLayerOutput.MemPtr, srcTopLayerOutputDeriv.MemPtr, tgtTopLayerOutputDeriv.MemPtr, batchsize, outputLayerSize, eps);
        //}
        //public void Deriv_Cosine_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        //{
        //    BasicMathlib.Deriv_Cosine_EX(srcTopLayerOutput.MemPtr, tgtTopLayerOutput.MemPtr, GPU_negative_index.MemPtr, srcTopLayerOutputDeriv.MemPtr, tgtTopLayerOutputDeriv.MemPtr, batchsize, outputLayerSize, eps);
        //}
        //public void Derive_Cosine_Linear_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        //{
        //    BasicMathlib.Derive_Cosine_Linear_EX(srcTopLayerOutput.MemPtr, tgtTopLayerOutput.MemPtr, GPU_negative_index.MemPtr, srcTopLayerOutputDeriv.MemPtr, tgtTopLayerOutputDeriv.MemPtr, batchsize, outputLayerSize, eps);
        //}
        //public void Derive_Cosine_Rectified_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        //{
        //    BasicMathlib.Derive_Cosine_Rectified_EX(srcTopLayerOutput.MemPtr, tgtTopLayerOutput.MemPtr, GPU_negative_index.MemPtr, srcTopLayerOutputDeriv.MemPtr, tgtTopLayerOutputDeriv.MemPtr, batchsize, outputLayerSize, eps);
        //}
        //public void Matrix_WeightAdd(CudaPieceFloat result, CudaPieceFloat addTerm, int batchsize, int outputLayerSize, CudaPieceFloat mweight, int start, int keep)
        //{
        //    BasicMathlib.Matrix_WeightAdd(result.MemPtr, addTerm.MemPtr, batchsize, outputLayerSize, mweight.MemPtr, start, keep);
        //}
        //public void Matrix_WeightAdd_EX(CudaPieceFloat result, CudaPieceFloat addTerm, CudaPieceInt GPU_Inver_negative_index, CudaPieceInt GPU_Inver_negative_value, int batchsize, int outputLayerSize, CudaPieceFloat mweight, int start, int keep)
        //{
        //    BasicMathlib.Matrix_WeightAdd_EX(result.MemPtr, addTerm.MemPtr, GPU_Inver_negative_index.MemPtr, GPU_Inver_negative_value.MemPtr, batchsize, outputLayerSize, mweight.MemPtr, start, keep);
        //}

        public void Deriv_Tanh(CudaPieceFloat errorDeriv, CudaPieceFloat output, int batchsize, int inputDimension)
        {
            BasicMathlib.Deriv_Tanh(errorDeriv.MemPtr, output.MemPtr, batchsize, inputDimension);
        }

        public void Deriv_Rectified(CudaPieceFloat errorDeriv, CudaPieceFloat output, int batchsize, int inputDimension)
        {
            BasicMathlib.Deriv_Rectified(errorDeriv.MemPtr, output.MemPtr, batchsize, inputDimension);
        }

        public void SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(SeqSparseBatchData input_batch, CudaPieceFloat weightDeriv, CudaPieceFloat upperOutputErrorDeriv, int inputDimension, int outputDimension, int winSize)
        {
            BasicMathlib.SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(input_batch.Sample_Idx_Mem, input_batch.BatchSize, input_batch.Seg_Idx_Mem, input_batch.Seg_Margin_Mem, input_batch.SentSize, input_batch.Fea_Idx_Mem, input_batch.Fea_Value_Mem, input_batch.ElementSize,
                               weightDeriv.MemPtr, upperOutputErrorDeriv.MemPtr, inputDimension, outputDimension, winSize);
        }

        //public void Convolution_Sparse_Matrix_Product_INTEX(CudaPieceFloat upperOutputErrorDeriv, CudaPieceInt layerMaxPooling_Index, SeqSparseBatchData input_batch, int winSize, int batchsize, int outputDimension, CudaPieceFloat weightDeriv, int inputDimension)
        //{
        //    BasicMathlib.Convolution_Sparse_Matrix_Product_INTEX(upperOutputErrorDeriv.MemPtr, layerMaxPooling_Index.MemPtr, input_batch.Seg_Idx_Mem, input_batch.Seg_Margin_Mem, input_batch.SentSize, winSize,
        //                             batchsize, outputDimension, input_batch.Fea_Idx_Mem, input_batch.Fea_Value_Mem, weightDeriv.MemPtr, inputDimension);
        //}
        //public void Matrix_Product(CudaPieceFloat lowerOutput, CudaPieceFloat upperOutputErrorDeriv, CudaPieceFloat weightDeriv, int batchsize, int inputDimension, int outputDimension)
        //{
        //    BasicMathlib.Matrix_Product(lowerOutput.MemPtr, upperOutputErrorDeriv.MemPtr, weightDeriv.MemPtr,
        //                batchsize, inputDimension, outputDimension);

        //}

        public void Scale_Matrix(CudaPieceFloat matrix, int inputDimension, int outputDimnsion, float momentum)
        {
            BasicMathlib.Scale_Matrix(matrix.MemPtr, inputDimension, outputDimnsion, momentum);
        }

        public void Matrix_Add(CudaPieceFloat matrix, CudaPieceFloat updates, int inputDimension, int outputDimnsion, float learning_rate)
        {
            BasicMathlib.Matrix_Add(matrix.MemPtr, updates.MemPtr, inputDimension, outputDimnsion, learning_rate);
        }

        //public void Sparse2Dense_Matrix(SeqSparseBatchData data, CudaPieceFloat matrix, int batchsize, int outputDimension)
        //{
        //    BasicMathlib.Sparse2Dense_Matrix(data.Seg_Idx_Mem, data.Fea_Idx_Mem, data.Fea_Value_Mem, matrix.MemPtr, batchsize, outputDimension);
        //}

        public void Zero(CudaPieceFloat matrix, int size)
        {
            PerfCounter.Manager.Instance["Zero"].CountOperation(() =>
                Array.Clear(matrix.MemPtr, 0, size));
        }

        //public void Matrix_Aggragate(CudaPieceFloat a, CudaPieceFloat b, int batchsize, int m)
        //{
        //    BasicMathlib.Matrix_Aggragate(a.MemPtr, b.MemPtr, batchsize, m);
        //}

        //public void Cosine_Similarity_SubSpace(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int labelDim, int BATCHSIZE, int batchsize, int subspaceDim, float eps)
        //{
        //    BasicMathlib.Cosine_Similarity_SubSpace(a.MemPtr, b.MemPtr, c.MemPtr, labelDim, BATCHSIZE, batchsize, subspaceDim, eps);
        //}

        public void SoftMax(CudaPieceFloat a, CudaPieceFloat b, int labelDim, int batchsize, float gamma)
        {
            BasicMathlib.SoftMax(a.MemPtr, b.MemPtr, labelDim, batchsize, gamma);
        }

        //public void Deriv_Cosine_Subspace(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat dcq, CudaPieceFloat dcd, CudaPieceFloat alpha, int act_type, int batchsize, int labelDim, int subspaceDim, float gamma, float eps)
        //{
        //    BasicMathlib.Deriv_Cosine_Subspace(q.MemPtr, d.MemPtr, dcq.MemPtr, dcd.MemPtr, alpha.MemPtr, act_type, batchsize, labelDim, subspaceDim, gamma, eps);
        //}

        public void InnerProduct_Similarity(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int batchsize, int dimension)
        {
            BasicMathlib.InnerProduct_Similarity(a.MemPtr, b.MemPtr, c.MemPtr, batchsize, dimension);
        }

        public void Deriv_InnerProduct(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat dcq, CudaPieceFloat dcd, CudaPieceFloat alpha, int act_type, int batchsize, int Dim, float gamma, float eps)
        {
            BasicMathlib.Deriv_InnerProduct(q.MemPtr, d.MemPtr, dcq.MemPtr, dcd.MemPtr, alpha.MemPtr, act_type, batchsize, Dim, gamma, eps);
        }

        //public void Matrix_Add_OFFSET(CudaPieceFloat a, int offset_a, CudaPieceFloat b, int offset_b, int len, float mweight)
        //{
        //    BasicMathlib.Matrix_Add_OFFSET(a.MemPtr, offset_a, b.MemPtr, offset_b, len, mweight);
        //}

        public void Matrix_Add_Sigmoid(CudaPieceFloat a, CudaPieceFloat b, int m, int n)
        {
            BasicMathlib.Matrix_Add_Sigmoid(a.MemPtr, b.MemPtr, m, n);
        }

        public void Sparse_Matrix_Multiply_INTEX_Weight(CudaPieceInt outputSmpIndex, CudaPieceInt outputItemIndex, CudaPieceFloat outputDeriv, int batchsize,
                int outputItemNum, CudaPieceFloat weight, CudaPieceFloat hidden_deriv, int hiddenDim, float wei)
        {
            BasicMathlib.Sparse_matrix_multiply_INTEX_weight(outputSmpIndex.MemPtr, outputItemIndex.MemPtr, outputDeriv.MemPtr, batchsize,
                    outputItemNum, weight.MemPtr, hidden_deriv.MemPtr, hiddenDim, wei);
        }

        //public void SEQ_Sparse_Matrix_Multiply_MASK_INTEX(SeqSparseBatchData data, CudaPieceInt mask, CudaPieceFloat weight, CudaPieceFloat output, int inputDimension, int outputDimension, int winSize)
        //{
        //    throw new NotImplementedException();
        //}
        //public void SEQ_Sparse_Matrix_Decode_INTEX(int elementsize, CudaPieceInt fea_Margin, CudaPieceInt seg_Margin, CudaPieceInt Fea_Index,
        //                                        int Feature_dimension, CudaPieceFloat mul_weight, CudaPieceFloat Fea_Value_Decode,
        //                                         CudaPieceFloat input, int input_dimension)
        //{
        //    throw new NotImplementedException();
        //}


        public void RecursiveForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqInputMatrixs, int dim, CudaPieceFloat WMatrix, int lag, int af, CudaPieceFloat SeqOutputMatrix)
        {
            PerfCounter.Manager.Instance["RecursiveForwardPropagateBatch"].CountOperation(() =>
            BasicMathlib.RecursiveForwardPropagateBatch(InstanceIndex.MemPtr, batchSize, SeqInputMatrixs.MemPtr,
                                        dim, WMatrix.MemPtr, lag, af, SeqOutputMatrix.MemPtr));
        }


        public void RecursiveBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqOutputMatrixs, int dim, CudaPieceFloat WMatrix, int lag, int af, CudaPieceFloat SeqDerivMatrixs)
        {
            PerfCounter.Manager.Instance["RecursiveBackwardPropagateBatch"].CountOperation(() =>
                BasicMathlib.RecursiveBackwardPropagateBatch(InstanceIndex.MemPtr, batchSize, SeqOutputMatrixs.MemPtr, dim, WMatrix.MemPtr, lag, af, SeqDerivMatrixs.MemPtr));
        }

        public void MatrixMultiplicationTranspose(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt, float weight)
        {
            PerfCounter.Manager.Instance["MatrixMultiplicationTranspose"].CountOperation(() =>
                BasicMathlib.MatrixMultiplicationTranspose(pLeft.MemPtr, pRight.MemPtr, pDst.MemPtr, leftRowCnt, rightColumnCnt, rightRowCnt, weight));
        }

        public unsafe void MatrixMultiplicationLeftTranspose(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float wei)
        {
            PerfCounter.Manager.Instance["MatrixMultiplicationLeftTranspose"].CountOperation(() =>
                BasicMathlib.MatrixMultiplicationLeftTranspose(pLeft.CpuPtr, pRight.CpuPtr, pDst.CpuPtr, leftRowCnt, leftColumnCnt, rightColumnCnt, 1, wei));
        }


        public void MatrixMultiplicationSparseLeftTranspose(CudaPieceInt pRowIndex, int RowNum, CudaPieceInt pColumnIndex, CudaPieceFloat pValue, int elementsize,
            CudaPieceFloat pRight, CudaPieceFloat pDst, int leftColumnCnt, int rightColumnCnt, float wei)
        {
            PerfCounter.Manager.Instance["MatrixMultiplicationSparseLeftTranspose"].CountOperation(() =>
            BasicMathlib.MatrixMultiplicationSparseLeftTranspose(pRowIndex.MemPtr, RowNum, pColumnIndex.MemPtr, pValue.MemPtr, elementsize,
                pRight.MemPtr, pDst.MemPtr, leftColumnCnt, rightColumnCnt, wei));
        }

        public void ColumnWiseSum(CudaPieceFloat matrix, CudaPieceFloat result, int row, int col, float wei)
        {
            PerfCounter.Manager.Instance["ColumnWiseSum"].CountOperation(() =>
                BasicMathlib.ColumnWiseSum(matrix.MemPtr, result.MemPtr, row, col, wei));
        }


        public void RecursiveUpdateBatch(CudaPieceInt InstanceIndex, int batchSize, int seqSize, CudaPieceFloat SeqOutputMatrixs, int dim, CudaPieceFloat SeqDerivMatrixs, int lag, CudaPieceFloat gradient, float weight)
        {
            PerfCounter.Manager.Instance["RecursiveUpdateBatch"].CountOperation(() =>
                BasicMathlib.RecursiveUpdateBatch(InstanceIndex.MemPtr, batchSize, seqSize, SeqOutputMatrixs.MemPtr, dim, SeqDerivMatrixs.MemPtr, lag, gradient.MemPtr, weight));
        }

        public void DerivCrossEntropy(CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputDeriv, int outputSize)
        {
            PerfCounter.Manager.Instance["DerivCrossEntropy"].CountOperation(() =>
                BasicMathlib.DerivCrossEntropy(outputScore.MemPtr, outputLabel.MemPtr, outputDeriv.MemPtr, outputSize));
        }


        public void ClipVector(CudaPieceFloat gpu_floats_a, int m, float maxThreshold, float minThreshold)
        {
            PerfCounter.Manager.Instance["ClipVector"].CountOperation(() =>
                BasicMathlib.ClipVector(gpu_floats_a.MemPtr, m, maxThreshold, minThreshold));
        }


        public void Add_Vector(CudaPieceFloat a, CudaPieceFloat b, int m, float awei, float bwei)
        {
            PerfCounter.Manager.Instance["Add_Vector"].CountOperation(() =>
                BasicMathlib.Add_Vector(a.MemPtr, b.MemPtr, a.MemPtr, m, awei, bwei));
        }


        public void Ada_Gradient(CudaPieceFloat ada, CudaPieceFloat grad, int m)
        {
            PerfCounter.Manager.Instance["Ada_Gradient"].CountOperation(() => BasicMathlib.Ada_Gradient(ada.MemPtr, grad.MemPtr, m));
        }


        public void Convolution_Sparse_Matrix_Product_INTEX_Weight(CudaPieceFloat deriv, CudaPieceInt maxpooling_index, CudaPieceInt Seg_Index, CudaPieceInt SegMargin_Index, int seg_size, int win_size, int batchsize, int output_dimension, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, CudaPieceFloat grad, int Feature_Dimension, float learnRate)
        {
            PerfCounter.Manager.Instance["Convolution_Sparse_Matrix_Product_INTEX_Weight"].CountOperation(() =>
            BasicMathlib.Convolution_Sparse_Matrix_Product_INTEX_Weight(deriv.MemPtr, maxpooling_index.MemPtr, Seg_Index.MemPtr, SegMargin_Index.MemPtr, seg_size, win_size, batchsize, output_dimension, Fea_Index.MemPtr, Fea_Value.MemPtr, grad.MemPtr, Feature_Dimension, learnRate));
        }


        public void SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat mul_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension, float weight)
        {
            BasicMathlib.SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(Smp_Index.MemPtr, batchsize, Seg_Index.MemPtr, seg_size, Fea_Index.MemPtr, Fea_Value.MemPtr, elementsize, mul_weight.MemPtr, output.MemPtr, Feature_dimension, output_dimension, weight);
        }


        public void SEQ_Sparse_Matrix_Multiply_INT(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat mul_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension)
        {
            BasicMathlib.SEQ_Sparse_Matrix_Multiply_INT(Smp_Index.MemPtr, batchsize, Seg_Index.MemPtr, seg_size, Fea_Index.MemPtr, Fea_Value.MemPtr, elementsize, mul_weight.MemPtr, output.MemPtr, Feature_dimension, output_dimension);
        }


        //public void Matrix_Product_Weight(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int batchsize, int m, int n, float weight)
        //{
        //    BasicMathlib.Matrix_Product_Weight(a.MemPtr, b.MemPtr, c.MemPtr, batchsize, m, n, weight);
        //}
        //public void Inner_Product_EX_Full(CudaPieceFloat a, CudaPieceFloat b, CudaPieceInt neg_list, CudaPieceFloat c, int nTrial, int BATCHSIZE, int batchsize, int dimension, float eps)
        //{
        //    BasicMathlib.Inner_Product_EX_Full(a.MemPtr, b.MemPtr, neg_list.MemPtr, c.MemPtr, nTrial, BATCHSIZE, batchsize, dimension, eps);
        //}

        public void LSTMForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c,
            CudaPieceFloat gate_o, CudaPieceFloat tanhc, int cell, CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo)
        {
            BasicMathlib.LSTMForwardPropagateBatch(InstanceIndex.MemPtr, batchSize, o.MemPtr, gate_i.MemPtr, c_hat.MemPtr, gate_f.MemPtr, c.MemPtr, gate_o.MemPtr, tanhc.MemPtr, cell, Ui.MemPtr, Uc.MemPtr, Uf.MemPtr, Uo.MemPtr, Vo.MemPtr);
        }

        public void LSTMBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int seqSize, int cell,
           CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc,
           CudaPieceFloat deriv_o, CudaPieceFloat deriv_gate_i, CudaPieceFloat deriv_cHat, CudaPieceFloat deriv_gate_f, CudaPieceFloat deriv_c, CudaPieceFloat deriv_gate_o, CudaPieceFloat deriv_tanhc,
           CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo)
        {
            BasicMathlib.LSTMBackwardPropagateBatch(InstanceIndex.MemPtr, batchSize, seqSize, cell, o.MemPtr, gate_i.MemPtr, c_hat.MemPtr, gate_f.MemPtr, c.MemPtr, gate_o.MemPtr, tanhc.MemPtr,
                deriv_o.MemPtr, deriv_gate_i.MemPtr, deriv_cHat.MemPtr, deriv_gate_f.MemPtr, deriv_c.MemPtr, deriv_gate_o.MemPtr, deriv_tanhc.MemPtr, Ui.MemPtr, Uc.MemPtr, Uf.MemPtr, Uo.MemPtr, Vo.MemPtr);
        }

        public void RNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInput, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOut)
        {
            BasicMathlib.RNNLabelOutput(smpIdx.MemPtr, batchSize, seqInput.MemPtr, seqSize, Wmatrix.MemPtr, lag, feaDim, hiddenDim, seqOut.MemPtr);
        }

        public void DerivRNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInputDeriv, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOutDeriv)
        {
            BasicMathlib.DerivRNNLabelOutput(smpIdx.MemPtr, batchSize, seqInputDeriv.MemPtr, seqSize, Wmatrix.MemPtr, lag, feaDim, hiddenDim, seqOutDeriv.MemPtr);
        }

        public void UpdateRNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInput, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOutDeriv, float wei)
        {
            BasicMathlib.UpdateRNNLabeloutput(smpIdx.MemPtr, batchSize, seqInput.MemPtr, seqSize, Wmatrix.MemPtr, lag, feaDim, hiddenDim, seqOutDeriv.MemPtr, wei);
        }

        public void LastStepExtractSeqMatrix(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix)
        {
            BasicMathlib.LastTimeStepExtractSeqMatrix(InstanceIndex.MemPtr, batchSize, SeqMatrixs.MemPtr, dim, matrix.MemPtr);
        }


        public void Calculate_SampleCrossEntropyDeriv(CudaPieceFloat output, CudaPieceFloat deriv, int dim, CudaPieceFloat label, int batchsize, float gamma)
        {
            BasicMathlib.Calculate_SampleCrossEntropyDeriv(output.MemPtr, deriv.MemPtr, dim, label.MemPtr, batchsize, gamma);
        }


        public unsafe void ElementwiseProduct(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int row, int column, float weiDst)
        {
            BasicMathlib.ElementwiseProduct(pLeft.CpuPtr, pRight.CpuPtr, pDst.CpuPtr, row * column, weiDst);
        }

        public void GRUForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int Dim, //->x;
            CudaPieceFloat H, CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat HHat, CudaPieceFloat RestH,
            CudaPieceFloat Ur, CudaPieceFloat Uz, CudaPieceFloat Uh)
        {
            BasicMathlib.GRUForwardPropagateBatch(InstanceIndex.MemPtr, batchSize, Dim, H.MemPtr, GateR.MemPtr, GateZ.MemPtr, HHat.MemPtr, RestH.MemPtr, Ur.MemPtr, Uz.MemPtr, Uh.MemPtr);
        }

        public void GRUBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int Dim,//->x
            CudaPieceFloat H, CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat HHat, CudaPieceFloat RestH, //H @ R
            CudaPieceFloat DerivH, CudaPieceFloat DerivGateR, CudaPieceFloat DerivGateZ, CudaPieceFloat DerivHHat, CudaPieceFloat DerivRestH,
            CudaPieceFloat Ur, CudaPieceFloat Uz, CudaPieceFloat Uh)
        {
            BasicMathlib.GRUBackwardPropagateBatch(InstanceIndex.MemPtr, batchSize, Dim, H.MemPtr, GateR.MemPtr, GateZ.MemPtr, HHat.MemPtr, RestH.MemPtr,
                DerivH.MemPtr, DerivGateR.MemPtr, DerivGateZ.MemPtr, DerivHHat.MemPtr, DerivRestH.MemPtr, Ur.MemPtr, Uz.MemPtr, Uh.MemPtr);
        }


        public void Deriv_CosineSimilarity_Matching(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat qSquare, CudaPieceFloat dSquare, int dim, CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, CudaPieceInt tgt2MatchIdx, CudaPieceInt tgt2MatchElement, CudaPieceInt srcIdx, CudaPieceInt tgtIdx, int srcSize, int tgtSize, int matchSize, CudaPieceFloat simi, CudaPieceFloat derivSimi, CudaPieceFloat dcq, CudaPieceFloat dcd, float eps)
        {
            BasicMathlib.Deriv_CosineSimilarity_Matching(q.MemPtr, d.MemPtr, qSquare.MemPtr, dSquare.MemPtr, dim,
                src2MatchIdx.MemPtr, src2MatchElement.MemPtr, tgt2MatchIdx.MemPtr, tgt2MatchElement.MemPtr, srcIdx.MemPtr,
                tgtIdx.MemPtr, srcSize, tgtSize, matchSize, simi.MemPtr, derivSimi.MemPtr, dcq.MemPtr, dcd.MemPtr, eps);
        }


        public void Deriv_InnerProduct_Matching(CudaPieceFloat q, CudaPieceFloat d, int dim, CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, CudaPieceInt tgt2MatchIdx, CudaPieceInt tgt2MatchElement, CudaPieceInt srcIdx, CudaPieceInt tgtIdx, int srcSize, int tgtSize, int matchSize, CudaPieceFloat derivSimi, CudaPieceFloat dcq, CudaPieceFloat dcd, float eps)
        {
            BasicMathlib.Deriv_InnerProduct_Matching(q.MemPtr, d.MemPtr, dim, src2MatchIdx.MemPtr, src2MatchElement.MemPtr, tgt2MatchIdx.MemPtr, tgt2MatchElement.MemPtr,
                    srcIdx.MemPtr, tgtIdx.MemPtr, srcSize, tgtSize, matchSize, derivSimi.MemPtr, dcq.MemPtr, dcd.MemPtr, eps);
        }

        public void LSTMForwardPropagateBatchV2(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc, int cell, CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo, CudaPieceInt RecursiveIdx, CudaPieceInt RecursiveMatrix, int MaxLag)
        {
            BasicMathlib.LSTMForwardPropagateBatch(InstanceIndex.MemPtr, batchSize, o.MemPtr, gate_i.MemPtr, c_hat.MemPtr, gate_f.MemPtr, c.MemPtr, gate_o.MemPtr, tanhc.MemPtr, cell, Ui.MemPtr, Uc.MemPtr, Uf.MemPtr, Uo.MemPtr, Vo.MemPtr);
        }

        public void LSTMBackwardPropagateBatchV2(CudaPieceInt InstanceIndex, int batchSize, int seqSize, int cell, CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc, CudaPieceFloat deriv_o, CudaPieceFloat deriv_gate_i, CudaPieceFloat deriv_cHat, CudaPieceFloat deriv_gate_f, CudaPieceFloat deriv_c, CudaPieceFloat deriv_gate_o, CudaPieceFloat deriv_tanhc, CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo, CudaPieceInt RecursiveIdx, CudaPieceInt RecursiveMatrix, int MaxLag)
        {
            BasicMathlib.LSTMBackwardPropagateBatch(InstanceIndex.MemPtr, batchSize, seqSize, cell, o.MemPtr, gate_i.MemPtr, c_hat.MemPtr, gate_f.MemPtr, c.MemPtr, gate_o.MemPtr, tanhc.MemPtr,
                deriv_o.MemPtr, deriv_gate_i.MemPtr, deriv_cHat.MemPtr, deriv_gate_f.MemPtr, deriv_c.MemPtr, deriv_gate_o.MemPtr, deriv_tanhc.MemPtr, Ui.MemPtr, Uc.MemPtr, Uf.MemPtr, Uo.MemPtr, Vo.MemPtr);
        }

        public float L2Norm(CudaPieceFloat x, int size)
        {
            return BasicMathlib.L2Norm(x.MemPtr, size);
        }

        public void RMSPropGradient(CudaPieceFloat ada, CudaPieceFloat grad, float gamma, float epsilon, int m)
        {
            BasicMathlib.RMSPropGradient(ada.MemPtr, grad.MemPtr, gamma, epsilon, m);
        }

        /// <summary>
        /// Z=wx*X + wy*Y^2
        /// </summary>
        /// <param name="Z"></param>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="wx"></param>
        /// <param name="wy"></param>
        /// <param name="m"></param>
        public void VectorSquareAdd(CudaPieceFloat Z, CudaPieceFloat X, CudaPieceFloat Y, float wx, float wy, int m)
        {
            BasicMathlib.VectorSquareAdd(Z.MemPtr, X.MemPtr, Y.MemPtr, wx, wy, m);
        }

        public void AdaMGradient(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat G, float alpha, float beta1, float beta2, float epsilon, int t, int size)
        {
            BasicMathlib.AdaMGradient(M.MemPtr, V.MemPtr, G.MemPtr, alpha, beta1, beta2, epsilon, t, size);
        }

        public void AdaDeltaGradient(CudaPieceFloat deltaX, CudaPieceFloat AccumGrad, CudaPieceFloat AccumUpdate, CudaPieceFloat Grad, float epsilon, int size)
        {
            BasicMathlib.AdaDeltaGradient(deltaX.MemPtr, AccumGrad.MemPtr, AccumUpdate.MemPtr, Grad.MemPtr, epsilon, size);
        }

        public void AdaMax(CudaPieceFloat V, CudaPieceFloat M, CudaPieceFloat G, float alpha, float beta, float epsilon, int size)
        {
            BasicMathlib.AdaMax(V.MemPtr, M.MemPtr, G.MemPtr, alpha, beta, epsilon, size);
        }


        public void Max_Pooling(CudaPieceFloat pooling_feas, CudaPieceInt Smp_Index, int batchsize, CudaPieceFloat output, CudaPieceInt maxpooling_index, int output_dimension)
        {
            BasicMathlib.Max_Pooling(pooling_feas.MemPtr, Smp_Index.MemPtr, batchsize, output.MemPtr, maxpooling_index.MemPtr, output_dimension);
        }

        public void Tanh(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize)
        {
            for (int i = 0; i < batchsize; i++)
            {
                b.MemPtr[b.Offset + offsetB + i] = (float)(Math.Tanh(a.MemPtr[a.Offset + offsetA + i]));
            }
            //FastVector.Tanh(FastVector.Create(a.MemPtr, offsetA + a.Offset, batchsize, false), FastVector.Create(b.MemPtr, offsetB + b.Offset, batchsize, false), batchsize);
        }

        public void Logistic(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize, float gamma)
        {
            for (int i = 0; i < batchsize; i++)
            {
                b.MemPtr[b.Offset + offsetB + i] = (float)(Math.Tanh(a.MemPtr[a.Offset + offsetA + i] * gamma / 2) + 1) / 2.0f;
            }
        }

        public void ReLU(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize)
        {
            for (int i = 0; i < batchsize; i++)
            {
                b.MemPtr[b.Offset + offsetB + i] = Math.Min(a.MemPtr[a.Offset + offsetA + i], 0);
            }
        }


        public void ElementwiseProduct(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst, int batchSize, int dim, float alpha)
        {
            for (int i = 0; i < batchSize * dim; i++)
            {
                pDst.MemPtr[pDst.Offset + i + offsetDst] = alpha * pDst.MemPtr[pDst.Offset + i + offsetDst] +
                    pLeft.MemPtr[pLeft.Offset + i + offsetLeft] * pRight.MemPtr[pRight.Offset + i + offsetRight];
            }
        }


        public void ElementwiseProductMask(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst,
            CudaPieceInt leftmask, int offsetlm, CudaPieceInt rightmask, int offsetrm, CudaPieceInt dstmask, int offsetdm, int row, int column, float alpha, float beta)
        {
            for (int i = 0; i < row; i++)
            {
                int leftidx = leftmask.IsEmpty ? i : leftmask.MemPtr[leftmask.Offset + offsetlm + i];
                int rightidx = rightmask.IsEmpty ? i : rightmask.MemPtr[rightmask.Offset + offsetrm + i];
                int dstidx = dstmask.IsEmpty ? i : dstmask.MemPtr[dstmask.Offset + offsetdm + i];

                for (int c = 0; c < column; c++)
                    pDst.MemPtr[pDst.Offset + offsetDst + dstidx * column + c] =
                        alpha * pDst.MemPtr[pDst.Offset + offsetDst + dstidx * column + c] +
                        beta * pLeft.MemPtr[pLeft.Offset + offsetLeft + leftidx * column + c] * pRight.MemPtr[pRight.Offset + offsetRight + rightidx * column + c];
            }
        }


        public unsafe void Sgemm(CudaPieceFloat A, int offsetA, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC, int rowA, int inDim, int outDim, float alpha, float beta, bool transA, bool transB)
        {
            if (!transA && !transB)
            {
                BasicMathlib.Matrix_Multipy(A.CpuPtr + offsetA, B.CpuPtr + offsetB, C.CpuPtr + offsetC, rowA, inDim, outDim, alpha, beta, 0);
                //FastVector.MatrixMulRowMajor(FastVector.Create(A.MemPtr, offsetA + A.Offset, rowA * inDim, false),
                //                             FastVector.Create(B.MemPtr, offsetB + B.Offset, inDim * outDim, false),
                //                             FastVector.Create(C.MemPtr, offsetC + C.Offset, rowA * outDim, false),
                //                             rowA, inDim, outDim, false, false, alpha, beta);
            }
            else if (!transA && transB)
            {
                BasicMathlib.Matrix_Multipy(A.CpuPtr + offsetA, B.CpuPtr + offsetB, C.CpuPtr + offsetC, rowA, inDim, outDim, alpha, beta, 1);
                //FastVector.MatrixMulRowMajor(FastVector.Create(A.MemPtr, offsetA + A.Offset, rowA * inDim, false),
                //                             FastVector.Create(B.MemPtr, offsetB + B.Offset, outDim * inDim, false),
                //                             FastVector.Create(C.MemPtr, offsetC + C.Offset, rowA * outDim, false),
                //                             rowA, inDim, outDim, false, true, alpha, beta);
            }
            else if (transA && !transB)
            {
                BasicMathlib.MatrixMultiplicationLeftTranspose(A.CpuPtr + offsetA, B.CpuPtr + offsetB, C.CpuPtr + offsetC, rowA, inDim, outDim, alpha, beta);
                //FastVector.MatrixMulRowMajor(FastVector.Create(A.MemPtr, offsetA + A.Offset, rowA * inDim, false),
                //                             FastVector.Create(B.MemPtr, offsetB + A.Offset, rowA * outDim, false),
                //                             FastVector.Create(C.MemPtr, offsetC + A.Offset, inDim * outDim, false), inDim, rowA, outDim, true, false, alpha, beta);
            }
        }


        public void SgemmMask(CudaPieceFloat A, int offsetA, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC, int batchsize, int m, int n, CudaPieceInt aMask, int offsetAMask, CudaPieceInt bMask, int offsetBMask, CudaPieceInt cMask, int offsetCMask, float alpha, float beta, bool transA, bool transB)
        {
            throw new NotImplementedException();
        }


        public unsafe void SparseSgemmMask(CudaPieceInt AIndex, CudaPieceInt AFeaIndex, CudaPieceFloat AFeaValue, CudaPieceFloat B, int offsetB,
            CudaPieceFloat C, int offsetC, int batchsize, int m, int n,
            CudaPieceInt aMask, int offsetAMask, 
            CudaPieceInt bMask, int offsetBMask, 
            CudaPieceInt cMask, int offsetCMask, float alpha, float beta, bool transA, bool transB)
        {
            BasicMathlib.SparseSgemmMask(AIndex.CpuPtr, AFeaIndex.CpuPtr, AFeaValue.CpuPtr, B.CpuPtr + offsetB, C.CpuPtr + offsetC, batchsize, m, n,
                aMask.CpuPtr + offsetAMask, bMask.CpuPtr + offsetBMask, cMask.CpuPtr + offsetCMask, alpha, beta, transA, transB);
        }


        public void DerivTanh(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta)
        {
            for (int i = 0; i < batchsize; i++)
            {
                c.MemPtr[c.Offset + offsetC + i] = c.MemPtr[c.Offset + offsetC + i] * alpha +
                    b.MemPtr[b.Offset + offsetB + i] * (1 + a.MemPtr[a.Offset + offsetA + i]) * (1 - a.MemPtr[a.Offset + offsetA + i]) * beta;
            }
        }

        public void DerivLogistic(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta)
        {
            for (int i = 0; i < batchsize; i++)
            {
                c.MemPtr[c.Offset + offsetC + i] = c.MemPtr[c.Offset + offsetC + i] * alpha +
                    b.MemPtr[b.Offset + offsetB + i] * (a.MemPtr[a.Offset + offsetA + i]) * (1 - a.MemPtr[a.Offset + offsetA + i]) * beta;
            }
        }

        public void DerivReLU(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta)
        {
            throw new NotImplementedException();
        }


        public void MaxPoolingMask(CudaPieceFloat poolingValues, CudaPieceInt smpIdx, CudaPieceInt smpMask, int batchsize, CudaPieceFloat output, CudaPieceInt maxpoolingIndex, int output_dimension)
        {
            if (smpMask.IsEmpty)
                Max_Pooling(poolingValues, smpIdx, batchsize, output, maxpoolingIndex, output_dimension);
            else
                BasicMathlib.Max_Pooling_Mask(poolingValues.MemPtr, smpIdx.MemPtr, batchsize, smpMask.MemPtr, output.MemPtr, maxpoolingIndex.MemPtr, output_dimension);

            //int[] seqIndex = smpMask == null ? null : smpMask.MemPtr;
            //int[] smpIndex = smpIdx.MemPtr;
            //float[] values = poolingValues.MemPtr;
            //float[] outValues = output.MemPtr;
            //int[] outIdx = maxpoolingIndex.MemPtr;
            //for (int i = 0; i < batchsize; i++)
            //{
            //    int colStart = i == 0 ? 0 : smpIndex[i-1];
            //    int colEnd = smpIndex[i];
            //    for (int j = 0; j < output_dimension; j++)
            //    {
            //        float maxValue = 0;
            //        int maxIdx = -1;
            //        if (colStart < colEnd)
            //        {
            //            maxValue = values[i * output_dimension + j];
            //            maxIdx = seqIndex == null ? colStart : seqIndex[colStart];
            //        }

            //        for (int k = colStart + 1; k < colEnd; k++)
            //        {
            //            int dim = seqIndex == null ? k : seqIndex[k];
            //            float idv = values[dim*output_dimension + j];

            //            if (idv > maxValue)
            //            {
            //                maxIdx = dim;
            //                maxValue = idv;
            //            }
            //        }

            //        outValues[i * output_dimension + j] = maxValue;
            //        outIdx[i * output_dimension + j] = maxIdx;
            //    }
            //}
        }

        public void DerivMaxPooling(CudaPieceFloat deriv, CudaPieceInt maxpoolingIndex, CudaPieceFloat poolingDeriv, int batchsize, int output_dimension, float alpha, float beta)
        {
            BasicMathlib.Deriv_Max_Pooling(deriv.MemPtr, maxpoolingIndex.MemPtr, poolingDeriv.MemPtr, batchsize, output_dimension, alpha, beta);
        }


        public void LastStepExtractSeqMatrixMask(CudaPieceInt InstanceIndex, CudaPieceInt SeqTransIdx, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix, CudaPieceFloat matrixMask, int maskOffset)
        {
            throw new NotImplementedException();
        }

        public void DerivLastStepSeqMatrix(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix, float alpha)
        {
            throw new NotImplementedException();
        }

        public void DerivLastStepSeqMatrixMask(CudaPieceInt InstanceIndex, CudaPieceInt SeqTransIdx, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix, CudaPieceFloat matrixMask, int maskOffset, float alpha)
        {
            throw new NotImplementedException();
        }


        public float LogLossDeriv(CudaPieceFloat outputProb, int dim, int batchSize, CudaPieceFloat smpProb, CudaPieceFloat label, CudaPieceFloat labelwei, CudaPieceInt labelMask, float gamma, float eps)
        {
            throw new NotImplementedException();
        }


        public void Add_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int m, float awei, float bwei)
        {
            for (int i = 0; i < m; i++)
            {
                a.MemPtr[offsetA + i] = awei * a.MemPtr[offsetA + i] + bwei * b.MemPtr[offsetB + i];
            }
        }


        public unsafe void Matrix_AdditionMask(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAMask, CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBMask, CudaPieceFloat c, int offsetC, CudaPieceInt cMask, int offsetCMask, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            BasicMathlib.Matrix_AdditionMask(a.CpuPtr + offsetA, aMask.CpuPtr + offsetAMask, b.CpuPtr + offsetB, bMask.CpuPtr + offsetBMask, c.CpuPtr + offsetC, cMask.CpuPtr + offsetCMask, dim, batchSize, awei, bwei, cwei);

            //Enumerable.Range(0, batchSize).AsParallel().ForAll(i =>
            //{
            //    int aIdx = aMask.IsEmpty ? i : aMask.MemPtr[offsetAMask + i];
            //    int bIdx = bMask.IsEmpty ? i : bMask.MemPtr[offsetBMask + i];
            //    int cIdx = cMask.IsEmpty ? i : cMask.MemPtr[offsetCMask + i];

            //    for (int d = 0; d < dim; d++)
            //    {
            //        c.CpuPtr[offsetC + cIdx * dim + d] =
            //               cwei * c.CpuPtr[offsetC + cIdx * dim + d] +
            //               awei * a.CpuPtr[offsetA + aIdx * dim + d] +
            //               bwei * b.CpuPtr[offsetB + bIdx * dim + d];
            //    }
            //});
        }


        public unsafe void SparseSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputScore, CudaPieceFloat outputProb, float gamma, int batchSize)
        {
            BasicMathlib.SparseSoftmax(smpIdx.CpuPtr, outputScore.CpuPtr, outputProb.CpuPtr, gamma, batchSize);
            //Enumerable.Range(0, batchSize).AsParallel().ForAll(idx =>
            //{
            //    int start = idx == 0 ? 0 : smpIdx.MemPtr[idx - 1];
            //    int end = smpIdx.MemPtr[idx];
            //    //get the max
            //    float max = float.MinValue;
            //    for (int i = start; i < end; i++)
            //    {
            //        max = Math.Max(max, outputScore.MemPtr[i]);
            //    }

            //    double sum = 0;
            //    for (int i = start; i < end; i++)
            //    {
            //        float exp = (float)Math.Exp(gamma * (outputScore.MemPtr[i] - max));
            //        outputProb.MemPtr[i] = exp;
            //        sum += exp;
            //    }

            //    for (int i = start; i < end; i++)
            //    {
            //        outputProb.MemPtr[i] = (float)(outputProb.MemPtr[i] / sum);
            //    }
            //});
        }


        public void DerivSparseMultiClassSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat probDeriv, CudaPieceFloat outputDeriv, float gamma, int batchSize, float alpha = 0, float beta = 1)
        {
            throw new NotImplementedException();
        }

        public unsafe void ColumnWiseSumMask(CudaPieceFloat matrix, int offsetM, CudaPieceInt matrixMask, int offsetMM, CudaPieceFloat weight, CudaPieceInt smpIdx, int batchSize, CudaPieceFloat output, int offsetO, CudaPieceInt outputMask, int offsetOM, int row, int col, float alpha, float beta)
        {
            BasicMathlib.ColumnWiseSumMask(matrix.CpuPtr + offsetM, matrixMask.CpuPtr + offsetMM, weight.CpuPtr, smpIdx.CpuPtr, batchSize, output.CpuPtr + offsetO, outputMask.CpuPtr + offsetOM, row, col, alpha, beta);
            //Enumerable.Range(0, batchSize).AsParallel().ForAll(idx =>
            //{
            //    int start = idx == 0 ? 0 : smpIdx.MemPtr[idx - 1];
            //    int end = smpIdx.MemPtr[idx];

            //    float[] sum = new float[col];
            //    for (int i = start; i < end; i++)
            //    {
            //        for (int j = 0; j < col; j++)
            //        {
            //            int ij = matrixMask.MemPtr == null ? offsetM + i * col + j : (offsetM + matrixMask.MemPtr[offsetMM + i] * col + j);
            //            sum[j] += matrix.MemPtr[ij] * (weight.MemPtr == null ? 1 : weight.MemPtr[i]);
            //        }
            //    }

            //    for (int j = 0; j < col; j++)
            //    {
            //        int mj = outputMask.MemPtr == null ? offsetO + idx * col + j : (offsetO + outputMask.MemPtr[offsetOM + idx] * col + j);
            //        output.MemPtr[mj] = alpha * output.MemPtr[mj] + beta * sum[j];
            //    }
            //});
        }

        public void Scale_MatrixMask(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAM, CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBM, int dim, int batchSize, CudaPieceFloat awei, float bwei)
        {
            throw new NotImplementedException();
        }

        public void Inner_Product_Matching(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB, int aSize, int bSize, int matchSize, int dimension, float eps)
        {
            throw new NotImplementedException();
        }

        public void Sgemv(CudaPieceFloat matrix, CudaPieceFloat x, int row, int col, CudaPieceFloat y, bool transM, float alpha, float beta)
        {
            if (!transM)
            {
                Sgemm(matrix, 0, x, 0, y, 0, row, col, 1, alpha, beta, false, false);
            }
            else
            {
                Sgemm(x, 0, matrix, 0, y, 0, 1, row, col, alpha, beta, false, false);
            }
        }

        public void ClipAdaGradUpdate(CudaPieceFloat ada, CudaPieceFloat grad, CudaPieceFloat param, float updateRate, float gradClip, float weightClip)
        {
            if (gradClip > 0)
            {
                ClipVector(grad, grad.Size, gradClip, -gradClip);
            }

            Ada_Gradient(ada, grad, grad.Size);
            Add_Vector(param, grad, grad.Size, 1, updateRate);
            if (weightClip > 0)
            {
                ClipVector(param, param.Size, weightClip, -weightClip);
            }
            Zero(grad, grad.Size);
        }

        public void ClipAdamUpdate(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat grad, CudaPieceFloat param, float beta1, float beta2, int iter, float updateRate, float gradClip, float weightClip)
        {
            float epsilon = 1.0e-8f;
            if (gradClip > 0)
            {
                ClipVector(grad, grad.Size, gradClip, -gradClip);
            }

            updateRate = (float)(updateRate * Math.Sqrt(1 - Math.Pow(beta2, iter)) / (1 - Math.Pow(beta1, iter)));

            //M = Beta1 * M + ( 1 - Beta1 ) * g
            Add_Vector(M, grad, param.Size, beta1, 1 - beta1);
            //V = beta2 * V + (1 - Beta2) * g ^ 2
            VectorSquareAdd(V, V, grad, beta2, 1 - beta2, param.Size);
            //MHat = M/(1 - Beta1 ^ Count)
            //Vhat = V/(1 - Beta2 ^ Count)
            //Gradient = Alpha * MHat/(Sqrt(VHat) + elpsion)
            AdaMGradient(M, V, grad, updateRate, beta1, beta2, epsilon, iter, param.Size);
            //Parameter += Gradient
            AddAndClear(param, grad, param.Size, 1);
            if (weightClip > 0)
            {
                ClipVector(param, param.Size, weightClip, -weightClip);
            }
        }

        public void LogBayesianRatingLoss(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize, CudaPieceFloat srcBatchLoss, CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputDeriv, uint outputSize, float eplison, float gamma)
        {
            throw new NotImplementedException();
        }


        public void AddAndClear(CudaPieceFloat a, CudaPieceFloat b, int m, float bwei)
        {
            BasicMathlib.AddAndClear(a.MemPtr, b.MemPtr, bwei);
        }

        public unsafe void GetSeqOrderMatrixs(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, CudaPieceInt MapForward, int dim, int isReverse, int order, CudaPieceFloat matrix, float alpha, float beta)
        {
            BasicMathlib.GetSeqOrderMatrixs(InstanceIndex.CpuPtr, batchSize, SeqMatrixs.CpuPtr, MapForward.CpuPtr, dim, isReverse, order, matrix.CpuPtr, alpha, beta);
            //Enumerable.Range(0, batchSize * dim).AsParallel().ForAll(id =>
            //{
            //    int idx = id / dim;
            //    int idy = id % dim;

            //    int instanceBegin = idx == 0 ? 0 : InstanceIndex.MemPtr[idx - 1];
            //    int instanceEnd = InstanceIndex.MemPtr[idx] - 1;
            //    int pos = -1;
            //    if (isReverse == 0)
            //    {
            //        pos = instanceBegin + order;
            //    }
            //    else
            //    {
            //        pos = instanceEnd - order;
            //    }

            //    if (pos >= instanceBegin && pos <= instanceEnd)
            //    {
            //        int newPos = MapForward.IsEmpty ? pos : MapForward.MemPtr[pos];
            //        matrix.MemPtr[idx * dim + idy] = alpha * matrix.MemPtr[idx * dim + idy] + beta * SeqMatrixs.MemPtr[newPos * dim + idy];
            //    }
            //    else matrix.MemPtr[idx * dim + idy] = 0;
            //});
        }

        public void SetSeqOrderMatrixs(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, CudaPieceInt MapForward, int dim, int isReverse, int order, CudaPieceFloat matrix, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void GetWindowMatrixs(CudaPieceInt InstanceMargin, int sentsize, CudaPieceFloat SeqMatrixs, CudaPieceInt mapForward, int dim, int winsize, CudaPieceFloat matrix, float alpha, float beta)
        {
            BasicMathlib.GetWindowMatrixs(InstanceMargin.MemPtr, sentsize, SeqMatrixs.MemPtr, mapForward.MemPtr, dim, winsize, matrix.MemPtr, alpha, beta);
            //for (int idx = 0; idx < sentsize; idx++)
            //{
            //    for (int idy = 0; idy < dim; idy++)
            //    {
            //        int half_win = winsize / 2;

            //        int smpidx = InstanceMargin.MemPtr[idx];
            //        for (int i = -half_win; i < winsize - half_win; i++)
            //        {
            //            int curidx = idx + i;
            //            matrix.MemPtr[idx * (winsize * dim) + (i + half_win) * dim + idy] =
            //                alpha * matrix.MemPtr[idx * (winsize * dim) + (i + half_win) * dim + idy];

            //            if (curidx >= 0 && curidx < sentsize && InstanceMargin.MemPtr[curidx] == smpidx)
            //            {
            //                int newPos = (mapForward == null || mapForward.IsEmpty) ? curidx : mapForward.MemPtr[curidx];
            //                matrix.MemPtr[idx * (winsize * dim) + (i + half_win) * dim + idy] += beta * SeqMatrixs.MemPtr[newPos * dim + idy];
            //            }
            //        }
            //    }
            //}
        }

        public void SetWindowMatrixs(CudaPieceInt InstanceMargin, int sentsize, CudaPieceFloat SeqMatrixs, CudaPieceInt mapForward, int dim, int winsize, CudaPieceFloat matrix, float alpha, float beta)
        {
            BasicMathlib.SetWindowMatrixs(InstanceMargin.MemPtr, sentsize, SeqMatrixs.MemPtr, mapForward.MemPtr, dim, winsize, matrix.MemPtr, alpha, beta);
            //throw new NotImplementedException();
        }

        public void DerivSoftmax(CudaPieceFloat softmaxData, CudaPieceFloat softmaxDeriv, CudaPieceFloat gradData, int dim, int batchsize, float alpha)
        {
            throw new NotImplementedException();
        }

        public unsafe void MaxoutPooling(CudaPieceFloat input, int batchSize, int dim, CudaPieceInt maxpoolingIndex, int maxoutDim, int outDim, CudaPieceFloat output, int format)
        {
            BasicMathlib.MaxoutPooling(input.CpuPtr, batchSize, dim, maxpoolingIndex.CpuPtr, maxoutDim, outDim, output.CpuPtr, format);
        }

        public void DerivMaxoutPooling(CudaPieceFloat input_deriv, int batchSize, int dim, CudaPieceInt maxpoolingIndex, int maxoutDim, int outDim, CudaPieceFloat output_deriv)
        {
            throw new NotImplementedException();
        }

        public void CNNForwardPropagate(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMap, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride, CudaPieceFloat imageOutBatch, int outHeight, int outWidth)
        {
            throw new NotImplementedException();
        }

        public void MomentumUpdate(CudaPieceFloat weight, CudaPieceFloat momentum, float momentumRate)
        {
            BasicMathlib.Add_Vector(weight.MemPtr, momentum.MemPtr, weight.MemPtr, weight.EffectiveSize, 1, 1);
            BasicMathlib.Scale_Matrix(momentum.MemPtr, 1, momentum.EffectiveSize, momentumRate);
        }

        public void NAGUpdate(CudaPieceFloat weight, CudaPieceFloat momentum, CudaPieceFloat trueWeight, float momentumRate)
        {
            BasicMathlib.Add_Vector(trueWeight.MemPtr, momentum.MemPtr, trueWeight.MemPtr, weight.EffectiveSize, 1, 1);
            BasicMathlib.Scale_Matrix(momentum.MemPtr, 1, momentum.EffectiveSize, momentumRate);
            BasicMathlib.Add_Vector(trueWeight.MemPtr, momentum.MemPtr, weight.MemPtr, weight.EffectiveSize, 1, 1);
        }

        public void BayesianRatingProb(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize, CudaPieceFloat srcBatchLoss, CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputProb, int outputSize, float eplison, float gamma)
        {
            throw new NotImplementedException();
        }

        public void MatrixL1Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l1Score)
        {
            throw new NotImplementedException();
        }

        public void MatrixL2Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l2Score)
        {
            throw new NotImplementedException();
        }

        public void DerivMatrixL1Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l1Score, CudaPieceFloat l1ScoreDeriv, CudaPieceFloat xDeriv)
        {
            throw new NotImplementedException();
        }

        public void DerivMatrixL2Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l2Score, CudaPieceFloat l2ScoreDeriv, CudaPieceFloat xDeriv)
        {
            throw new NotImplementedException();
        }

        public void Matrix_Mask(CudaPieceFloat output, CudaPieceInt dropoutMask, int batchSize, int dim)
        {
            BasicMathlib.Matrix_Mask(output.MemPtr, dropoutMask.MemPtr, batchSize, dim);
        }

        public void SpanMaxPool(CudaPieceFloat inputSent, CudaPieceInt smpIdx, CudaPieceFloat start, CudaPieceFloat end, CudaPieceInt sentMargin, int matchSize, CudaPieceFloat outputSent, CudaPieceInt outMaxIndex, int dim)
        {
            throw new NotImplementedException();
        }

        public void DerivSpanMaxPool(CudaPieceFloat outputSentDeriv, CudaPieceInt smpIdx, int batchSize, CudaPieceFloat inputSentDeriv, CudaPieceInt outMaxIndex, int dim, float beta)
        {
            throw new NotImplementedException();
        }

        public void SpanLastPool(CudaPieceFloat inputSent, CudaPieceInt smpIdx, CudaPieceFloat start, CudaPieceFloat end, CudaPieceInt sentMargin, int matchSize, CudaPieceFloat outputSent, int dim)
        {
            throw new NotImplementedException();
        }

        public void DerivSpanLastPool(CudaPieceFloat outputSentDeriv, CudaPieceInt outSmpIdx, CudaPieceFloat start, CudaPieceFloat end, int batchSize, CudaPieceFloat inputSentDeriv, CudaPieceInt inSmpIdx, int dim, float beta)
        {
            throw new NotImplementedException();
        }

        public void AccurateScale_Matrix(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAM, CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBM, int dim, int batchSize, CudaPieceFloat awei)
        {
            throw new NotImplementedException();
        }


        public void AccurateElementwiseProduct(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst, CudaPieceInt leftmask, int offsetlm, CudaPieceInt rightmask, int offsetrm, CudaPieceInt dstmask, int offsetdm, int row, int column, float beta)
        {
            throw new NotImplementedException();
        }

        public void Convolution_Sparse_Matrix_Multiply_INTEX(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, CudaPieceInt Seg_Margin, int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat con_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension, int win_size)
        {
            throw new NotImplementedException();
        }


        public unsafe void Matrix_AdditionEx(CudaPieceFloat a, int offsetA, int skipA, CudaPieceFloat b, int offsetB, int skipB, CudaPieceFloat c, int offsetC, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            BasicMathlib.Matrix_AdditionEx(a.CpuPtr + offsetA, skipA, b.CpuPtr + offsetB, skipB, c.CpuPtr + offsetC, skipC, dim, batchSize, awei, bwei, cwei);
            //Enumerable.Range(0, batchSize * dim).AsParallel().ForAll(id =>
            //{
            //    int x = id % dim;
            //    int y = id / dim;
            //    int aPos = y * skipA + x;
            //    int bPos = y * skipB + x;
            //    int cPos = y * skipC + x;
            //    c.MemPtr[c.Offset + offsetC + cPos] = cwei * c.MemPtr[c.Offset + offsetC + cPos] + awei * a.MemPtr[a.Offset + offsetA + aPos] + bwei * b.MemPtr[b.Offset + offsetB + bPos];
            //});
        }

        public unsafe void Matrix_AdditionExMask(CudaPieceFloat a, CudaPieceInt aMask, int skipA, CudaPieceFloat b, CudaPieceInt bMask, int skipB, CudaPieceFloat c, CudaPieceInt cMask, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            BasicMathlib.Matrix_AdditionExMask(a.CpuPtr, aMask.CpuPtr, skipA, b.CpuPtr, bMask.CpuPtr, skipB, c.CpuPtr, cMask.CpuPtr, skipC, dim, batchSize, awei, bwei, cwei);
            //Enumerable.Range(0, batchSize * dim).AsParallel().ForAll(id =>
            //{
            //    int x = id % dim;
            //    int y = id / dim;
            //    int ya = aMask.IsEmpty ? y : aMask.MemPtr[y];
            //    int yb = bMask.IsEmpty ? y : bMask.MemPtr[y];
            //    int yc = cMask.IsEmpty ? y : cMask.MemPtr[y];

            //    int aPos = ya * skipA + x;
            //    int bPos = yb * skipB + x;
            //    int cPos = yc * skipC + x;
            //    c.MemPtr[c.Offset + cPos] = cwei * c.MemPtr[c.Offset + cPos] + awei * a.MemPtr[a.Offset + aPos] + bwei * b.MemPtr[b.Offset + bPos];
            //});
        }

        public unsafe void SoftAttention_Matching(CudaPieceFloat a, CudaPieceInt aMask, CudaPieceFloat b, CudaPieceInt bMask, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat c, CudaPieceInt cMask, int batchSize, float alpha, float beta)
        {
            BasicMathlib.SoftAttention_Matching(a.CpuPtr, aMask.CpuPtr, b.CpuPtr, bMask.CpuPtr, dim, a_func, op, vec.CpuPtr, c.CpuPtr, cMask.CpuPtr, batchSize, alpha, beta);
        }

        public void DerivSoftAttention_Matching(CudaPieceFloat a, CudaPieceFloat aDeriv, CudaPieceInt aMask, CudaPieceFloat b, CudaPieceFloat bDeriv, CudaPieceInt bMask, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat vecDeriv, CudaPieceFloat c, CudaPieceFloat cDeriv, CudaPieceInt cMask, CudaPieceInt aSmpIdx, CudaPieceInt aElementIdx, int aSize, CudaPieceInt bSmpIdx, CudaPieceInt bElementIdx, int bSize, int batchSize, float awei, float bwei, float vwei)
        {
            throw new NotImplementedException();
        }

        public unsafe void SoftAttention(CudaPieceFloat a, CudaPieceFloat b, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat c, int aBatchSize, int bBatchSize, float alpha, float beta)
        {
            BasicMathlib.SoftAttention(a.CpuPtr, b.CpuPtr, dim, a_func, op, vec.CpuPtr, c.CpuPtr, aBatchSize, bBatchSize, alpha, beta);
        }

        public void DerivSoftAttention(CudaPieceFloat a, CudaPieceFloat aDeriv, CudaPieceFloat b, CudaPieceFloat bDeriv, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat vecDeriv, CudaPieceFloat c, CudaPieceFloat cDeriv, int aBatchSize, int bBatchSize, float awei, float bwei, float vwei)
        {
            throw new NotImplementedException();
        }

        public void CuDNNBackwardPropagateData(CudaPieceFloat imageInBatchDeriv, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMap, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride, CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth)
        {
            throw new NotImplementedException();
        }

        public void CuDNNBackwardPropagateFilter(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMapDeriv, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride, CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth, float alphaValue, float betaValue)
        {
            throw new NotImplementedException();
        }

        public void RMSPropV2_Gradient(CudaPieceFloat ada, CudaPieceFloat g, CudaPieceFloat grad, float gamma, float epsilon, int m)
        {
            throw new NotImplementedException();
        }

        public void CuDNNBackwardPropagateBias(CudaPieceFloat biasDeriv, CudaPieceFloat imageOutBatchDeriv, int batch, int outDepth, int outHeight, int outWidth, float alphaValue, float betaValue)
        {
            throw new NotImplementedException();
        }

        public void NDArrayTranspose(CudaPieceFloat input, CudaPieceInt indim, CudaPieceInt transDim, CudaPieceFloat output, int dimNum, int length, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void KLargestValueBatch(CudaPieceFloat float_array, int batchSize, int dim, int K, int mode, CudaPieceFloat bestValues, CudaPieceFloat bestIndexes)
        {
            throw new NotImplementedException();
        }

        public void DerivLogSparseSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat logProbDeriv, CudaPieceFloat outputDeriv, float gamma, float alpha, float beta, int batchSize)
        {
            throw new NotImplementedException();
        }

        public void ColumnWiseSumMaskEx(CudaPieceFloat matrix, int offsetM, CudaPieceInt matrixMask, int offsetMM, CudaPieceFloat weight, int skipMatrix, CudaPieceInt smpIdx, int batchSize, CudaPieceFloat output, int offsetO, CudaPieceInt outputMask, int offsetOM, int skipOutput, int row, int col, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void DerivLogProbEntropy(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat logProbDeriv, float alpha, float beta, float epislon, int batchSize)
        {
            throw new NotImplementedException();
        }

        public void LookupForward(CudaPieceInt inputSmpIdx, CudaPieceInt inputItemIdx, int batchSize, CudaPieceFloat weight, CudaPieceFloat output, int inputDim, int outputDim)
        {
            throw new NotImplementedException();
        }
        
        public void LookupBackward(CudaPieceInt inputSmpIdx, CudaPieceInt inputItemIdx, int batchSize, int inputItemNum, CudaPieceFloat deriv, int inputDim, int outputDim, CudaPieceFloat output, float lr)
        {
            throw new NotImplementedException();
        }

        public void SparseVectorAdd(CudaPieceInt idx, CudaPieceFloat value, CudaPieceFloat dst, float beta, int length)
        {
            throw new NotImplementedException();
        }

        public float VectorSum(CudaPieceFloat x, int index, int len, int norm)
        {
            throw new NotImplementedException();
        }

        public void SparseVectorGivens(CudaPieceInt idx, CudaPieceFloat value, CudaPieceFloat dst, float alpha, float beta, int length)
        {
            throw new NotImplementedException();
        }


        /// b = bwei b + awei * a;
        public void Scale_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int len, float awei, float bwei)
        {
            throw new NotImplementedException();
        }

        public void Init_Vector(CudaPieceFloat a, int offsetA, float b, int m, float awei)
        {
            throw new NotImplementedException();
        }

        public void Log(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, float alpha, float beta, int size)
        {
            throw new NotImplementedException();
        }

    }
}
