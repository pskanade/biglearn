﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MatrixStructure : Structure
    {
        public CudaPieceFloat Memory;
        public CudaPieceFloat MemoryGrad;

        public int Dim { get; set; }
        public int Size { get; set; }

        public GradientOptimizer MemoryOptimizer { get { return StructureOptimizer["GlobalMemory"].Optimizer; } set { StructureOptimizer["GlobalMemory"].Optimizer = value; } }

        public MatrixStructure(int dim, int size, DeviceType device)
        {
            Dim = dim;
            Size = size;

            DeviceType = device;

            Memory = new CudaPieceFloat(Dim * Size, device);
            Memory.Init((float)(12.0f / Math.Sqrt(Dim + Size)), (float)(-6.0f / Math.Sqrt(Dim + Size)));
            //Memory.Norm(Dim, Size, VecNormType.L2Norm);
        }

        public MatrixStructure(BinaryReader reader, DeviceType device) : base(reader, device)
        { }

        protected override void InitStructureOptimizer()
        {
            MemoryGrad = new CudaPieceFloat(Dim * Size, DeviceType);
            StructureOptimizer.Add("GlobalMemory", new ModelOptimizer() { Parameter = Memory, Gradient = MemoryGrad, Name = "Global Memory" });
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(Size);
            writer.Write(Dim);
            Memory.Serialize(writer);
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            Size = reader.ReadInt32();
            Dim = reader.ReadInt32();
            Memory = new CudaPieceFloat(reader, device);
        }

        public override DataSourceID Type { get { return DataSourceID.MatrixStructure; } }
    }
}
