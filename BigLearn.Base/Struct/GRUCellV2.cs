using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class GRUCellV2 : Structure
    {
        public int InputFeatureDim;
        public int HiddenStateDim;
        
        public override DataSourceID Type { get { return DataSourceID.GRUCellV2; } }

        public GRUCellV2(int input, int hiddenDim, DeviceType device) 
        {
            InputFeatureDim = input;
            
            HiddenStateDim = hiddenDim;

            DeviceType = device;

            Wzr = new CudaPieceFloat((InputFeatureDim + HiddenStateDim) * HiddenStateDim * 2, device);
            Bzr = new CudaPieceFloat(HiddenStateDim * 2, device);
            Wh = new CudaPieceFloat((InputFeatureDim + HiddenStateDim) * HiddenStateDim, device);
            Bh = new CudaPieceFloat(HiddenStateDim, device);

            Init();
        }

        public GRUCellV2(BinaryReader reader, DeviceType device) : base(reader, device)
        { }

        public CudaPieceFloat Wzr;
        public CudaPieceFloat Bzr;
        public CudaPieceFloat BzrGrad;
        public CudaPieceFloat WzrGrad;

        public CudaPieceFloat Wh;
        public CudaPieceFloat Bh;
        public CudaPieceFloat WhGrad;
        public CudaPieceFloat BhGrad;

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(InputFeatureDim);
            writer.Write(HiddenStateDim);

            Wzr.Serialize(writer);
            Bzr.Serialize(writer);
            Wh.Serialize(writer);
            Bh.Serialize(writer);
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            InputFeatureDim = reader.ReadInt32();
            HiddenStateDim = reader.ReadInt32();
            Wzr = new CudaPieceFloat(reader, device); 
            Bzr = new CudaPieceFloat(reader, device);
            Wh = new CudaPieceFloat(reader, device);
            Bh = new CudaPieceFloat(reader, device);
        }

        protected override void InitStructureOptimizer()
        {
            WzrGrad = new CudaPieceFloat(Wzr.Size, DeviceType);
            BzrGrad = new CudaPieceFloat(Bzr.Size, DeviceType);
            WhGrad = new CudaPieceFloat(Wh.Size, DeviceType);
            BhGrad = new CudaPieceFloat(Bh.Size, DeviceType);

            StructureOptimizer.Add("GRU_Wzr", new ModelOptimizer() { Parameter = Wzr, Gradient = WzrGrad, Name = "GRU_Wzr" });
            StructureOptimizer.Add("GRU_Bzr", new ModelOptimizer() { Parameter = Bzr, Gradient = BzrGrad, Name = "GRU_Bzr" });
            StructureOptimizer.Add("GRU_Wh", new ModelOptimizer() { Parameter = Wh, Gradient = WhGrad, Name = "GRU_Wh" });
            StructureOptimizer.Add("GRU_Bh", new ModelOptimizer() { Parameter = Bh, Gradient = BhGrad, Name = "GRU_Bh" });
        }

        #region IData Interface.
        public override void CopyFrom(IData other)
        {
            GRUCellV2 refStructure = (GRUCellV2)other;
            Wzr.CopyFrom(refStructure.Wzr);
            Bzr.CopyFrom(refStructure.Bzr);
            Wh.CopyFrom(refStructure.Wh);
            Bh.CopyFrom(refStructure.Bh);
        }
        #endregion.

        public void Init()
        {
            int input = (InputFeatureDim + HiddenStateDim);
            int output = HiddenStateDim;

            float scale = (float)Math.Sqrt(6.0 / (input + output)); 
            Wzr.Init(scale * 2, -scale); 
            Bzr.Init(0);
            Wh.Init(scale * 2, -scale);
            Bh.Init(0);
        }
    }
}