﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum RndRecurrentInit { RndNorm = 0, RndOrthogonal = 1 }
    public class GRUCell : Structure, IDisposable
    {
        RndRecurrentInit RndInit = RndRecurrentInit.RndNorm;
        public GRUCell(int input, int hiddenDim, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm) : this(input, 0, hiddenDim, device, rndInit)
        { }

        public GRUCell(int input, int attDim, int hiddenDim, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm)
        {
            InputFeatureDim = input;
            HiddenStateDim = hiddenDim;
            AttentionDim = attDim;
            RndInit = rndInit;
            DeviceType = device;
            InitMemory(device);
            Init();
        }

        public GRUCell(BinaryReader reader, DeviceType device) : base(reader, device)
        { }

        public override DataSourceID Type { get { return DataSourceID.GRUCell; } }

        public int InputFeatureDim;
        public int HiddenStateDim;
        public int AttentionDim;

        public CudaPieceFloat Wr;
        public CudaPieceFloat WrGrad;
        public CudaPieceFloat Ur;
        public CudaPieceFloat UrGrad;

        public CudaPieceFloat Br;
        public CudaPieceFloat BrGrad;
        public CudaPieceFloat Cr;
        public CudaPieceFloat CrGrad;

        public CudaPieceFloat Wz;
        public CudaPieceFloat WzGrad;
        public CudaPieceFloat Uz;
        public CudaPieceFloat UzGrad;

        public CudaPieceFloat Bz;
        public CudaPieceFloat BzGrad;
        public CudaPieceFloat Cz;
        public CudaPieceFloat CzGrad;

        public CudaPieceFloat Uh;
        public CudaPieceFloat UhGrad;

        public CudaPieceFloat Wh;
        public CudaPieceFloat WhGrad;
        public CudaPieceFloat Bh;
        public CudaPieceFloat BhGrad;
        public CudaPieceFloat Ch;
        public CudaPieceFloat ChGrad;

        public GradientOptimizer WrMatrixOptimizer { get { return StructureOptimizer["GRU_Wr"].Optimizer; } }
        public GradientOptimizer UrMatrixOptimizer { get { return StructureOptimizer["GRU_Ur"].Optimizer; } }
        public GradientOptimizer BrMatrixOptimizer { get { return StructureOptimizer["GRU_Br"].Optimizer; } }

        public GradientOptimizer WzMatrixOptimizer { get { return StructureOptimizer["GRU_Wz"].Optimizer; } }
        public GradientOptimizer UzMatrixOptimizer { get { return StructureOptimizer["GRU_Uz"].Optimizer; } }
        public GradientOptimizer BzMatrixOptimizer { get { return StructureOptimizer["GRU_Bz"].Optimizer; } }

        public GradientOptimizer WhMatrixOptimizer { get { return StructureOptimizer["GRU_Wh"].Optimizer; } }
        public GradientOptimizer UhMatrixOptimizer { get { return StructureOptimizer["GRU_Uh"].Optimizer; } }
        public GradientOptimizer BhMatrixOptimizer { get { return StructureOptimizer["GRU_Bh"].Optimizer; } }

        public GradientOptimizer CrMatrixOptimizer { get { return StructureOptimizer["GRU_Cr"].Optimizer; } }
        public GradientOptimizer CzMatrixOptimizer { get { return StructureOptimizer["GRU_Cz"].Optimizer; } }
        public GradientOptimizer ChMatrixOptimizer { get { return StructureOptimizer["GRU_Ch"].Optimizer; } }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(InputFeatureDim);
            writer.Write(HiddenStateDim);
            writer.Write(AttentionDim);

            Wr.SyncToCPU();
            Ur.SyncToCPU();
            Br.SyncToCPU();
            for (int i = 0; i < InputFeatureDim * HiddenStateDim; i++) writer.Write(Wr.MemPtr[i]);
            for (int i = 0; i < HiddenStateDim * HiddenStateDim; i++) writer.Write(Ur.MemPtr[i]);
            for (int i = 0; i < HiddenStateDim; i++) writer.Write(Br.MemPtr[i]);

            Wz.SyncToCPU();
            Uz.SyncToCPU();
            Bz.SyncToCPU();
            for (int i = 0; i < InputFeatureDim * HiddenStateDim; i++) writer.Write(Wz.MemPtr[i]);
            for (int i = 0; i < HiddenStateDim * HiddenStateDim; i++) writer.Write(Uz.MemPtr[i]);
            for (int i = 0; i < HiddenStateDim; i++) writer.Write(Bz.MemPtr[i]);

            Wh.SyncToCPU();
            Uh.SyncToCPU();
            Bh.SyncToCPU();
            for (int i = 0; i < InputFeatureDim * HiddenStateDim; i++) writer.Write(Wh.MemPtr[i]);
            for (int i = 0; i < HiddenStateDim * HiddenStateDim; i++) writer.Write(Uh.MemPtr[i]);
            for (int i = 0; i < HiddenStateDim; i++) writer.Write(Bh.MemPtr[i]);

            if(AttentionDim > 0)
            {
                Cr.SyncToCPU();
                Cz.SyncToCPU();
                Ch.SyncToCPU();
                for (int i = 0; i < AttentionDim * HiddenStateDim; i++) writer.Write(Cr.MemPtr[i]);
                for (int i = 0; i < AttentionDim * HiddenStateDim; i++) writer.Write(Cz.MemPtr[i]);
                for (int i = 0; i < AttentionDim * HiddenStateDim; i++) writer.Write(Ch.MemPtr[i]);
            }
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            InputFeatureDim = reader.ReadInt32();
            HiddenStateDim = reader.ReadInt32();
            AttentionDim = reader.ReadInt32();

            InitMemory(device);

            for (int i = 0; i < InputFeatureDim * HiddenStateDim; i++) Wr.MemPtr[i] = reader.ReadSingle();
            for (int i = 0; i < HiddenStateDim * HiddenStateDim; i++) Ur.MemPtr[i] = reader.ReadSingle();
            for (int i = 0; i < HiddenStateDim; i++) Br.MemPtr[i] = reader.ReadSingle();

            for (int i = 0; i < InputFeatureDim * HiddenStateDim; i++) Wz.MemPtr[i] = reader.ReadSingle();
            for (int i = 0; i < HiddenStateDim * HiddenStateDim; i++) Uz.MemPtr[i] = reader.ReadSingle();
            for (int i = 0; i < HiddenStateDim; i++) Bz.MemPtr[i] = reader.ReadSingle();

            for (int i = 0; i < InputFeatureDim * HiddenStateDim; i++) Wh.MemPtr[i] = reader.ReadSingle();
            for (int i = 0; i < HiddenStateDim * HiddenStateDim; i++) Uh.MemPtr[i] = reader.ReadSingle();
            for (int i = 0; i < HiddenStateDim; i++) Bh.MemPtr[i] = reader.ReadSingle();

            Wr.SyncFromCPU();
            Ur.SyncFromCPU();
            Br.SyncFromCPU();

            Wz.SyncFromCPU();
            Uz.SyncFromCPU();
            Bz.SyncFromCPU();

            Wh.SyncFromCPU();
            Uh.SyncFromCPU();
            Bh.SyncFromCPU();

            if (AttentionDim > 0)
            {
                for (int i = 0; i < AttentionDim * HiddenStateDim; i++) Cr.MemPtr[i] = reader.ReadSingle();
                for (int i = 0; i < AttentionDim * HiddenStateDim; i++) Cz.MemPtr[i] = reader.ReadSingle();
                for (int i = 0; i < AttentionDim * HiddenStateDim; i++) Ch.MemPtr[i] = reader.ReadSingle();
                Cr.SyncFromCPU();
                Cz.SyncFromCPU();
                Ch.SyncFromCPU();
            }
        }

        protected override void InitStructureOptimizer()
        {
            StructureOptimizer.Add("GRU_Wr", new ModelOptimizer() { Parameter = Wr, Gradient = WrGrad, Name = "GRU_Wr" });
            StructureOptimizer.Add("GRU_Ur", new ModelOptimizer() { Parameter = Ur, Gradient = UrGrad, Name = "GRU_Ur" });
            StructureOptimizer.Add("GRU_Br", new ModelOptimizer() { Parameter = Br, Gradient = BrGrad, Name = "GRU_Br" });
            StructureOptimizer.Add("GRU_Wz", new ModelOptimizer() { Parameter = Wz, Gradient = WzGrad, Name = "GRU_Wz" });
            StructureOptimizer.Add("GRU_Uz", new ModelOptimizer() { Parameter = Uz, Gradient = UzGrad, Name = "GRU_Uz" });
            StructureOptimizer.Add("GRU_Bz", new ModelOptimizer() { Parameter = Bz, Gradient = BzGrad, Name = "GRU_Bz" });

            StructureOptimizer.Add("GRU_Wh", new ModelOptimizer() { Parameter = Wh, Gradient = WhGrad, Name = "GRU_Wh" });
            StructureOptimizer.Add("GRU_Uh", new ModelOptimizer() { Parameter = Uh, Gradient = UhGrad, Name = "GRU_Uh" });
            StructureOptimizer.Add("GRU_Bh", new ModelOptimizer() { Parameter = Bh, Gradient = BhGrad, Name = "GRU_Bh" });
        
            if(AttentionDim > 0)
            {
                StructureOptimizer.Add("GRU_Cr" , new ModelOptimizer() { Parameter = Cr, Gradient = CrGrad, Name = "GRU_Cr" });
                StructureOptimizer.Add("GRU_Cz", new ModelOptimizer() { Parameter = Cz, Gradient = CzGrad, Name = "GRU_Cz" });
                StructureOptimizer.Add("GRU_Ch", new ModelOptimizer() { Parameter = Ch, Gradient = ChGrad, Name = "GRU_Ch" });
            }
        }

        public void InitMemory(DeviceType device)
        {
            Wr = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);
            WrGrad = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);

            Ur = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);
            UrGrad = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);

            Br = new CudaPieceFloat(HiddenStateDim, device);
            BrGrad = new CudaPieceFloat(HiddenStateDim, device);

            Wz = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);
            WzGrad = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);

            Uz = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);
            UzGrad = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);

            Bz = new CudaPieceFloat(HiddenStateDim, device);
            BzGrad = new CudaPieceFloat(HiddenStateDim, device);

            Wh = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);
            WhGrad = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);

            Uh = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);
            UhGrad = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);

            Bh = new CudaPieceFloat(HiddenStateDim, device);
            BhGrad = new CudaPieceFloat(HiddenStateDim, device);

            if (AttentionDim > 0)
            {
                Cr = new CudaPieceFloat(AttentionDim * HiddenStateDim, true, device == DeviceType.GPU);
                CrGrad = new CudaPieceFloat(AttentionDim * HiddenStateDim, true, device == DeviceType.GPU);

                Cz = new CudaPieceFloat(AttentionDim * HiddenStateDim, true, device == DeviceType.GPU);
                CzGrad = new CudaPieceFloat(AttentionDim * HiddenStateDim, true, device == DeviceType.GPU);

                Ch = new CudaPieceFloat(AttentionDim * HiddenStateDim, true, device == DeviceType.GPU);
                ChGrad = new CudaPieceFloat(AttentionDim * HiddenStateDim, true, device == DeviceType.GPU);
            }
        }

        #region IData Interface.
        public override void CopyFrom(IData other)
        {
            GRUCell refStructure = (GRUCell)other;

            Wh.CopyFrom(refStructure.Wh);
            Wz.CopyFrom(refStructure.Wz);
            Wr.CopyFrom(refStructure.Wr);

            Uh.CopyFrom(refStructure.Uh);
            Uz.CopyFrom(refStructure.Uz);
            Ur.CopyFrom(refStructure.Ur);

            Bh.CopyFrom(refStructure.Bh);
            Bz.CopyFrom(refStructure.Bz);
            Br.CopyFrom(refStructure.Br);

            if (AttentionDim > 0)
            {
                Ch.CopyFrom(refStructure.Ch);
                Cz.CopyFrom(refStructure.Cz);
                Cr.CopyFrom(refStructure.Cr);
            }
        }
        #endregion.

        public void Init()
        {
            Wr.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim))));
            //float[,] ur = RandomUtil.GetRandomOrthogonalMatrix(HiddenStateDim);
            if(RndInit == RndRecurrentInit.RndNorm) Ur.Init((float)(Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim))));
            else if(RndInit == RndRecurrentInit.RndOrthogonal) Ur.InitRandomOrthogonalMatrix(HiddenStateDim);
            Br.Init(0);

            Wz.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim))));
            if (RndInit == RndRecurrentInit.RndNorm) Uz.Init((float)(Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim))));
            else if (RndInit == RndRecurrentInit.RndOrthogonal) Uz.InitRandomOrthogonalMatrix(HiddenStateDim);
            Bz.Init(0);

            Wh.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim))));
            if (RndInit == RndRecurrentInit.RndNorm) Uh.Init((float)(Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim))));
            else if (RndInit == RndRecurrentInit.RndOrthogonal) Uh.InitRandomOrthogonalMatrix(HiddenStateDim);
            Bh.Init(0);

            if(AttentionDim > 0)
            {
                Cr.Init((float)(Math.Sqrt(6.0 / (AttentionDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + HiddenStateDim))));
                Cz.Init((float)(Math.Sqrt(6.0 / (AttentionDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + HiddenStateDim))));
                Ch.Init((float)(Math.Sqrt(6.0 / (AttentionDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + HiddenStateDim))));
            }
        }

        #region dispose function.
        private bool disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;
            if (disposing)
            {
                if (Wr != null)
                {
                    Wr.Dispose();
                }

                if (Ur != null)
                {
                    Ur.Dispose();
                }

                if (Br != null)
                {
                    Br.Dispose();
                }


                if (Wz != null)
                {
                    Wz.Dispose();
                }

                if (Uz != null)
                {
                    Uz.Dispose();
                }

                if (Bz != null)
                {
                    Bz.Dispose();
                }


                if (Wh != null)
                {
                    Wh.Dispose();
                }

                if (Uh != null)
                {
                    Uh.Dispose();
                }

                if (Bh != null)
                {
                    Bh.Dispose();
                }

            }

            Wr = null;
            Ur = null;
            Br = null;

            Wz = null;
            Uz = null;
            Bz = null;

            Wh = null;
            Uh = null;
            Bh = null;

            base.Dispose(disposing);
        }

        ~GRUCell()
        {
            this.Dispose(false);
        }
        #endregion.

    }

    public class GRUStructure : Structure, IDisposable
    {
        public List<GRUCell> GRUCells = new List<GRUCell>();

        public override DataSourceID Type { get { return DataSourceID.GRUStructure; } }

        public override List<Structure> SubStructure { get { return GRUCells.Select(i => (Structure)i).ToList(); } }

        public GRUStructure(int inputFea, int[] hiddenStates) : this(inputFea, hiddenStates, DeviceType.GPU) { }

        public GRUStructure(int inputFea, int[] hiddenStates, DeviceType device, RndRecurrentInit rInit = RndRecurrentInit.RndNorm) : this(inputFea, hiddenStates, Enumerable.Range(0, hiddenStates.Length).Select(i => 0).ToArray(), device, rInit)
        { }

        public GRUStructure(int inputFea, int[] hiddenStates, int[] attentionStates, DeviceType device, RndRecurrentInit rInit = RndRecurrentInit.RndNorm)
        {
            int input = inputFea;
            for (int i = 0; i < hiddenStates.Length; i++)
            {
                GRUCell cell = new GRUCell(input, attentionStates[i], hiddenStates[i], device, rInit);
                GRUCells.Add(cell);
                input = hiddenStates[i];
            }
        }

        public GRUStructure(BinaryReader reader, DeviceType device) : base(reader, device) { }

        public GRUStructure(List<GRUCell> cells) { GRUCells = cells; }

        public override void CopyFrom(IData other)
        {
            GRUStructure modelStructure = (GRUStructure)other;
            for (int i = 0; i < GRUCells.Count; i++)
            {
                GRUCells[i].CopyFrom(modelStructure.GRUCells[i]);
            }
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(GRUCells.Count);
            foreach (GRUCell cell in GRUCells) { cell.Serialize(writer); }
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            int layerNum = reader.ReadInt32();
            for (int i = 0; i < layerNum; i++)
            {
                GRUCell cell = new GRUCell(reader, device);
                GRUCells.Add(cell);
            }
        }

        public string Descr()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("GRU Layer Number {0}\n", GRUCells.Count));
            sb.Append(string.Join("\n", GRUCells.Select(i => string.Format("Input Dim {0}, Output Dim {1}, Attention Dim {2}", i.InputFeatureDim, i.HiddenStateDim, i.AttentionDim))));
            return sb.ToString();
        }

        #region dispose function.
        private bool disposed = false;
        ~GRUStructure()
        {
            this.Dispose(false);
        }
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;
            if (disposing)
            {
                foreach (GRUCell cell in GRUCells)
                {
                    if (cell != null) cell.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        #endregion.
    }
}