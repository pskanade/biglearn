﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    
    public class LSTMStructure : Structure, IDisposable
    {
        ///Multi Layer LSTM Model.
        public List<LSTMCell> LSTMCells = new List<LSTMCell>();

        public override List<Structure> SubStructure { get { return LSTMCells.Select(i => (Structure)i).ToList(); } }

        public LSTMStructure(int inputFea, int[] hiddenStates)
            : this(inputFea, hiddenStates, DeviceType.GPU)
        { }

        public LSTMStructure(int inputFea, int[] hiddenStates, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm)
        {
            int input = inputFea;
            for (int i = 0; i < hiddenStates.Length; i++)
            {
                LSTMCell cell = new LSTMCell(input, hiddenStates[i], device, rndInit);
                LSTMCells.Add(cell);
                input = hiddenStates[i];
            }
        }

        public LSTMStructure(int inputFea, int[] hiddenStates, int[] attentDim, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm)
        {
            int input = inputFea;
            for (int i = 0; i < hiddenStates.Length; i++)
            {
                LSTMCell cell = new LSTMCell(input, hiddenStates[i], attentDim[i], device, rndInit);
                LSTMCells.Add(cell);
                input = hiddenStates[i];
            }
        }

        public LSTMStructure(BinaryReader reader, DeviceType device) : base(reader, device) { }

        public LSTMStructure(List<LSTMCell> cells)
        {
            LSTMCells = cells;
        }

        public override DataSourceID Type { get { return DataSourceID.LSTMStructure; } }


        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(LSTMCells.Count);
            foreach (LSTMCell cell in LSTMCells) cell.Serialize(writer);
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            int layerNum = reader.ReadInt32();
            DeviceType = device;
            for (int i = 0; i < layerNum; i++)
            {
                LSTMCell cell = new LSTMCell(reader, device);
                LSTMCells.Add(cell);
            }
        }

        public override void CopyFrom(IData other)
        {
            LSTMStructure modelStructure = (LSTMStructure)other;
            for (int i = 0; i < LSTMCells.Count; i++)
            {
                LSTMCells[i].CopyFrom(modelStructure.LSTMCells[i]);
            }
        }

        public override void SyncToCPU()
        {
            for (int i = 0; i < LSTMCells.Count; i++)
            {
                LSTMCells[i].SyncToCPU();
            }
        }

        public string Descr()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("LSTM Layer Number {0}\n", LSTMCells.Count));
            sb.Append(string.Join("\n", LSTMCells.Select(i => string.Format("Input Dim {0}, Output Dim {1} ", i.InputFeatureDim, i.CellDim))));
            return sb.ToString();
        }
    }

    public class LSTMLabelStructure : Structure, IDisposable
    {
        public LSTMStructure LSTM;
        public LayerStructure Output;

        public int StackLen;
        public int OutputDim;

        public override List<Structure> SubStructure { get { return new Structure[] { LSTM, Output }.ToList(); } }

        public override void CopyFrom(IData other) 
        {
            LSTMLabelStructure modelStructure = (LSTMLabelStructure)other;
            LSTM.CopyFrom(modelStructure.LSTM);
            Output.CopyFrom(modelStructure.Output);
        }

        public LSTMLabelStructure(LSTMStructure lstm, int stackLen, int output, DeviceType device)
        {
            LSTM = lstm;
            Output = new LayerStructure(LSTM.LSTMCells.Last().CellDim * (stackLen + 1), output, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device);
            Output.Init();

            StackLen = stackLen;
            OutputDim = output;
        }

        public LSTMLabelStructure(BinaryReader reader, DeviceType device)
            : base(reader, device)
        { }


        
        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(StackLen);
            writer.Write(OutputDim);
            LSTM.Serialize(writer);
            Output.Serialize(writer);
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            StackLen = reader.ReadInt32();
            OutputDim = reader.ReadInt32();
            LSTM = new LSTMStructure(reader, device); //.Deserialize(reader, device);
            Output = new LayerStructure(reader, device); //.Deserialize(reader, device);
        }

        protected override void Dispose(bool disposing)
        {
            if (LSTM != null) LSTM.Dispose(); LSTM = null;
            if (Output != null) Output.Dispose(); Output = null;
        }
    }

    public class LSTMPredStructure : Structure, IDisposable
    {
        public LSTMStructure LSTM;

        public CudaPieceFloat Embedding;
        public CudaPieceFloat Bias;

        public CudaPieceFloat ClassEmbedding;
        public CudaPieceFloat ClassBias;

        public int VocabSize;

        public override List<Structure> SubStructure { get { return new Structure[] { LSTM }.ToList(); } }
        public GradientOptimizer EmbeddingOptimizer { get { return StructureOptimizer["Embedding"].Optimizer; } }
        public GradientOptimizer BiasOptimizer { get { return StructureOptimizer["Bias"].Optimizer; } }

        public LSTMPredStructure(LSTMStructure lstm, int vocabSize)
        {
            LSTM = lstm;
            VocabSize = vocabSize;
            Embedding = new CudaPieceFloat(vocabSize * lstm.LSTMCells.Last().CellDim, true, true);
            Embedding.Init(1.0f / lstm.LSTMCells.Last().CellDim, -0.5f / lstm.LSTMCells.Last().CellDim);

            Bias = new CudaPieceFloat(vocabSize, true, true);
            Bias.Init(0);
        }

        protected override void InitStructureOptimizer()
        {
            StructureOptimizer.Add("Embedding", new ModelOptimizer() { Parameter = Embedding });
            StructureOptimizer.Add("Bias", new ModelOptimizer() { Parameter = Bias });

            StructureOptimizer.Add("ClassEmbedding", new ModelOptimizer() { Parameter = ClassEmbedding });
            StructureOptimizer.Add("ClassBias", new ModelOptimizer() { Parameter = ClassBias });
        }

        public void InitClassEmbedding(int classNum)
        {
            ClassEmbedding = new CudaPieceFloat(classNum * LSTM.LSTMCells.Last().CellDim, true, true);
            ClassEmbedding.Init(1.0f / LSTM.LSTMCells.Last().CellDim, -0.5f / LSTM.LSTMCells.Last().CellDim);

            ClassBias = new CudaPieceFloat(classNum, true, true);
            ClassBias.Init(0);
        }
    }
}
