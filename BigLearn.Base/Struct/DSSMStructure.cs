﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public sealed class DSSMStructure : Structure, IDisposable
    {
        public override List<Structure> SubStructure
        {
            get
            {
                return IsShareModel ? new List<Structure>() { (Structure)SrcDnn } :
                    new List<Structure>() { (Structure)SrcDnn, (Structure)TgtDnn };
            }
        }

        public DNNStructure SrcDnn = null;
        public DNNStructure TgtDnn = null;
        public SimilarityType SimiType = SimilarityType.CosineSimilarity;
        public bool IsShareModel = false;

        public int OutputDim { get { return SrcDnn.OutputLayerSize; } }

        public bool IsConvolution { get { return SrcDnn.IsConvolution || TgtDnn.IsConvolution; } }
        /// <summary>
        /// Load From DSSMBuilderParameters.
        /// </summary>
        public DSSMStructure(DeviceType device, SimilarityType simType, bool isShareModel,
                        int srcDim, int[] srcLayerDim, A_Func[] srcA_Func, bool[] srcBias, N_Type[] srcArch, int[] srcWin, float[] srcDropout,
                        int tgtDim, int[] tgtLayerDim, A_Func[] tgtA_Func, bool[] tgtBias, N_Type[] tgtArch, int[] tgtWin, float[] tgtDropout)
        {
            SimiType = simType; // DSSMBuilderParameters.SimiType;
            IsShareModel = isShareModel; // DSSMBuilderParameters.IsShareModel;
            int srcFeatureDim = srcDim; // DataPanel.Train.Stat.SrcStat.MAX_FEATUREDIM;
            int tgtFeatureDim = tgtDim; // DataPanel.Train.Stat.TgtStat.MAX_FEATUREDIM;
            if (IsShareModel)
            {
                srcFeatureDim = Math.Max(srcFeatureDim, tgtFeatureDim);
                tgtFeatureDim = Math.Max(srcFeatureDim, tgtFeatureDim);
            }
            SrcDnn = new DNNStructure(srcFeatureDim, srcLayerDim, srcA_Func, srcBias, srcArch, srcWin, srcDropout, device);
            SrcDnn.Init();

            if (IsShareModel) TgtDnn = SrcDnn;
            else
            {
                TgtDnn = new DNNStructure(tgtFeatureDim, tgtLayerDim, tgtA_Func, tgtBias, tgtArch, tgtWin, tgtDropout, device);
                TgtDnn.Init();
            }
        }


        /// <summary>
        /// Load From Model Streams. (This function will be removed.)
        /// </summary>
        /// <param name="modelNamePrefix"></param>
        /// <param name="device"></param>
        public DSSMStructure(string modelNamePrefix, DNNModelVersion version, DeviceType device, SimilarityType type, bool isShareModel)
        {
            if (File.Exists(modelNamePrefix + ".Setting"))
            {
                using (BinaryReader dssmReader = new BinaryReader(new FileStream(modelNamePrefix + ".Setting", FileMode.Open, FileAccess.Read)))
                {
                    SimiType = (SimilarityType)dssmReader.ReadInt32();
                    IsShareModel = dssmReader.ReadBoolean();
                }
            }
            else
            {
                SimiType = type;
                IsShareModel = isShareModel;
            }
            SrcDnn = new DNNStructure(modelNamePrefix + ".Query", version, device);
            if (!IsShareModel) TgtDnn = new DNNStructure(modelNamePrefix + ".Doc", version, device);
            else TgtDnn = SrcDnn;
        }

        /// <summary>
        /// Load From Model Streams.
        /// </summary>
        /// <param name="modelNamePrefix"></param>
        /// <param name="device"></param>
        public DSSMStructure(Stream model, DeviceType device = DeviceType.CPU_FAST_VECTOR, bool loadTgtModel = true)
        {
            DNNModelVersion version = DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR;
            using (BinaryReader dssmReader = new BinaryReader(model, Encoding.UTF8, true))
            {
                SimiType = (SimilarityType)dssmReader.ReadInt32();
                IsShareModel = dssmReader.ReadBoolean();
                version = (DNNModelVersion)dssmReader.ReadInt32();
            }

            SrcDnn = new DNNStructure(model, version, device); 
            if (loadTgtModel)
            {
                if (!IsShareModel)
                {
                    TgtDnn = new DNNStructure(model, version, device);
                }
                else
                {
                    TgtDnn = SrcDnn;
                }
            }
        }

        /// <summary>
        /// This function will be removed.
        /// </summary>
        /// <param name="modelNamePrefix"></param>
        public override void Save(string modelNamePrefix)
        {
            using (BinaryWriter dssmWriter = new BinaryWriter(new FileStream(modelNamePrefix + ".Setting", FileMode.Create, FileAccess.Write)))
            {
                dssmWriter.Write((int)SimiType);
                dssmWriter.Write(IsShareModel);
            }
            SrcDnn.ModelSave_C_OR_DSSM_GPU_V4_2015_APR(modelNamePrefix + ".Query");
            if (!IsShareModel) TgtDnn.ModelSave_C_OR_DSSM_GPU_V4_2015_APR(modelNamePrefix + ".Doc");
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            DNNModelVersion version = DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR;
            SimiType = (SimilarityType)reader.ReadInt32();
            IsShareModel = reader.ReadBoolean();
            version = (DNNModelVersion)reader.ReadInt32();

            SrcDnn = new DNNStructure(reader.BaseStream, version, device); 
            if (!IsShareModel)
            {
                TgtDnn = new DNNStructure(reader.BaseStream, version, device);
            }
            else
            {
                TgtDnn = SrcDnn;
            }
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write((int)SimiType);
            writer.Write(IsShareModel);
            writer.Write((int)DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR);
            SrcDnn.SerializeAs_C_OR_DSSM_GPU_V4_2015_APR(writer);
            if (!IsShareModel)
            {
                TgtDnn.SerializeAs_C_OR_DSSM_GPU_V4_2015_APR(writer);
            }
        }

        //public override void InitOptimizer(StructureLearner learner)
        //{
        //    SrcDnn.InitOptimizer(learner);
        //    TgtDnn.InitOptimizer(learner);
        //}
        private bool disposed = false;
        ~DSSMStructure()
        {
            this.Dispose(false);
        }

        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;

            if (disposing)
            {
                SrcDnn.Dispose();
                TgtDnn.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
