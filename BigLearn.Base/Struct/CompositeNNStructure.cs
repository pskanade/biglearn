﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class CompositeNNStructure : Structure, IDisposable
    {
        public override List<Structure> SubStructure { get { return CompositeLinks.Select(i => (Structure)i).ToList(); } }

        public List<Structure> CompositeLinks = new List<Structure>();

        /// <summary>
        /// It only support certain type of structure.
        /// </summary>
        public CompositeNNStructure()
        { }

        public CompositeNNStructure(BinaryReader reader, DeviceType device)
            : base(reader, device)
        { }

        /// <summary>
        /// It only support certain type of structure.
        /// </summary>
        /// <param name="structure"></param>
        //public void AddLayer(Structure structure)
        //{
        //    CompositeLinks.Add(structure);
        //}

        public T AddLayer<T>(T structure) where T : Structure
        {
            CompositeLinks.Add(structure);
            return structure;
        }

        public override void Serialize(BinaryWriter mwriter)
        {
            mwriter.Write(CompositeLinks.Count);
            for (int i = 0; i < CompositeLinks.Count; i++)
            {
                mwriter.Write((int)CompositeLinks[i].Type);
                CompositeLinks[i].Serialize(mwriter);
            }
        }

        

        // please don't use this function any more . this is a very wierld function. discust. 
        public Structure DeserializeNextModel(BinaryReader mreader, DeviceType device)
        {

            int mid = mreader.ReadInt32();
            DataSourceID id = (DataSourceID)mid;
            Structure model = null;
            switch (id)
            {
                case DataSourceID.LayerStructure:
                case DataSourceID.ConvStructure:
                    model = new LayerStructure(mreader, device);
                    break;
                case DataSourceID.LSTMStructure:
                    model = new LSTMStructure(mreader, device);
                    break;
                case DataSourceID.NormLayerStructure:
                    model = new NormLayerStructure(mreader, device);
                    break;
                case DataSourceID.MemoryStructure:
                    model = new MemoryStructure(mreader, device);
                    break;
                case DataSourceID.EmbedStructure:
                    model = new EmbedStructure(mreader, device);
                    break;
                case DataSourceID.MLPAttentionStructure:
                    model = new MLPAttentionStructure(mreader, device);
                    break;
                case DataSourceID.DNNStructure:
                    model = new DNNStructure(mreader, device);
                    break;
                case DataSourceID.GRUCell:
                    model = new GRUCell(mreader, device);
                    break;
                case DataSourceID.LSTMCell:
                    model = new LSTMCell(mreader, device);
                    break;
                case DataSourceID.GRUStructure:
                    model = new GRUStructure(mreader, device);
                    break;
                case DataSourceID.MatrixStructure:
                    model = new MatrixStructure(mreader, device);
                    break;
                case DataSourceID.ImageLinkStructure:
                    model = new ImageLinkStructure(mreader, device);
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            CompositeLinks.Add(model);
            return model;
        }

        public T DeserializeModel<T>(BinaryReader mreader, DeviceType device) where T : Structure
        {
            DataSourceID id = (DataSourceID)mreader.ReadInt32();
            T model = Activator.CreateInstance(typeof(T), new object[] { mreader, device }) as T;
            CompositeLinks.Add(model);
            return model;
        }

        public static int DeserializeModelCount(BinaryReader mreader)
        {
            return mreader.ReadInt32();
        }
        public static T DeserializeNextModel<T>(BinaryReader mreader, DeviceType device) where T : Structure
        {
            DataSourceID id = (DataSourceID)mreader.ReadInt32();
            T model = Activator.CreateInstance(typeof(T), new object[] { mreader, device }) as T;
            return model;
        }

        public override void Deserialize(BinaryReader mreader, DeviceType device)
        {
            int layerNum = DeserializeModelCount(mreader); // mreader.ReadInt32();
            for (int i = 0; i < layerNum; i++)
            {
                DeserializeNextModel(mreader, device);
            }
        }

        #region Dispose Function.
        private bool disposed = false;
        ~CompositeNNStructure()
        {
            this.Dispose(false);
        }

        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;

            if (disposing)
            {
                foreach (Structure link in CompositeLinks)
                    link.Dispose();
            }

            base.Dispose(disposing);
        }
        #endregion.

    }
}
