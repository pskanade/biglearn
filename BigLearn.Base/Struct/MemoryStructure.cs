﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MemoryStructure : Structure
    {
        public CudaPieceFloat Address;
        public CudaPieceFloat Content;

        public CudaPieceFloat AddressGrad;
        public CudaPieceFloat ContentGrad;

        public int InputDim { get; set; }
        public int OutputDim { get; set; }
        public int MemoryLen { get; set; }

        public GradientOptimizer ContentOptimizer { get { return StructureOptimizer["Content"].Optimizer; } }
        public GradientOptimizer AddressOptimizer { get { return StructureOptimizer["Address"].Optimizer; } }

        public MemoryStructure(int memoryLength, int inputDim, int outputDim, DeviceType device)
        {
            InputDim = inputDim;
            OutputDim = outputDim;
            MemoryLen = memoryLength;
            DeviceType = device;

            Address = new CudaPieceFloat(MemoryLen * InputDim, true, device == DeviceType.GPU);
            Content = new CudaPieceFloat(MemoryLen * OutputDim, true, device == DeviceType.GPU);

            Address.Init((float)(12.0f / Math.Sqrt(InputDim + MemoryLen)), (float)(-6.0f / Math.Sqrt(InputDim + MemoryLen)));
            Content.Init((float)(12.0f / Math.Sqrt(OutputDim + MemoryLen)), (float)(-6.0f / Math.Sqrt(OutputDim + MemoryLen)));
        }

        public MemoryStructure(BinaryReader reader, DeviceType device) : base(reader, device)
        { }

        protected override void InitStructureOptimizer()
        {
            AddressGrad = new CudaPieceFloat(Address.Size, DeviceType);
            ContentGrad = new CudaPieceFloat(Content.Size, DeviceType);

            StructureOptimizer.Add("Address", new ModelOptimizer() { Parameter = Address, Gradient = AddressGrad });
            StructureOptimizer.Add("Content", new ModelOptimizer() { Parameter = Content, Gradient = ContentGrad });
        }

        public override DataSourceID Type { get { return DataSourceID.MemoryStructure; } }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(MemoryLen);
            writer.Write(InputDim);
            writer.Write(OutputDim);
            Address.Serialize(writer);
            Content.Serialize(writer);
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            MemoryLen = reader.ReadInt32();
            InputDim = reader.ReadInt32();
            OutputDim = reader.ReadInt32();
            Address = new CudaPieceFloat(reader, device);
            Content = new CudaPieceFloat(reader, device);
        }
    }
}
