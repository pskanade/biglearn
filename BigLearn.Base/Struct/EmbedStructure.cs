﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class EmbedStructure : Structure
    {
        public CudaPieceFloat Embedding = null;
        public CudaPieceFloat Bias = null;

        public CudaPieceFloat EmbeddingGrad;
        public CudaPieceFloat BiasGrad;

        public bool IsBias = false;
        public int VocabSize { get; private set; }
        public int Dim { get; private set; }

        public override DeviceType DeviceType { get; set; }

        public override DataSourceID Type { get { return DataSourceID.EmbedStructure; } }
        
        public GradientOptimizer EmbeddingOptimizer { get { return StructureOptimizer["Embedding"].Optimizer; }
            set
            {
                EmbeddingGrad = value.Gradient;
                StructureOptimizer.Add("Embedding", new ModelOptimizer()
                {
                    Name = "EmbedStructure_Embedding",
                    Gradient = EmbeddingGrad,
                    Parameter = Embedding,
                    Optimizer = value
                });
            }
        }

        public GradientOptimizer BiasOptimizer { get { return StructureOptimizer["Bias"].Optimizer; } set { StructureOptimizer["Bias"].Optimizer = value; } }

        public EmbedStructure(int vocabSize, int dimension, DeviceType device)
        {
            VocabSize = vocabSize;
            Dim = dimension;
            DeviceType = device;

            Embedding = new CudaPieceFloat(VocabSize * Dim, true, DeviceType == DeviceType.GPU);
            if(Dim > 0) Embedding.Init(1.0f / Dim, -0.5f / Dim); //2, - 1);//
            Bias = new CudaPieceFloat(VocabSize, true, DeviceType == DeviceType.GPU);
            Bias.Init(0);
        }

        public void Init(float min, float max)
        {
            Embedding.Init(max - min, (max - min) / 2);
        }

        protected override void InitStructureOptimizer()
        {
            if (!StructureOptimizer.ContainsKey("Embedding"))
            {
                EmbeddingGrad = new CudaPieceFloat(Embedding.Size, DeviceType);
                StructureOptimizer.Add("Embedding", new ModelOptimizer() { Name = "EmbedStructure_Embedding", Gradient = EmbeddingGrad, Parameter = Embedding });
            }

            if (IsBias && !StructureOptimizer.ContainsKey("Bias"))
            {
                BiasGrad = new CudaPieceFloat(Bias.Size, DeviceType);
                StructureOptimizer.Add("Bias", new ModelOptimizer() { Name = "EmbedStructure_Bias", Gradient = BiasGrad, Parameter = Bias });
            }
        }

        public EmbedStructure() { }
        public EmbedStructure(BinaryReader reader, DeviceType device) : base(reader, device) { }

        public override void CopyFrom(IData other) 
        {
            EmbedStructure src = (EmbedStructure)other;

            if(VocabSize != src.VocabSize || Dim != src.Dim)
            {
                throw new Exception(string.Format("vocab src {0}, tgt {1}, dim src {2}, tgt {3}", VocabSize, src.VocabSize, Dim, src.Dim));
            }
            Embedding.CopyFrom(src.Embedding);
            Bias.CopyFrom(src.Bias);


        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(VocabSize);
            writer.Write(Dim);
            writer.Write(IsBias);

            Embedding.Serialize(writer);
            Bias.Serialize(writer);
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            VocabSize = reader.ReadInt32();
            Dim = reader.ReadInt32();
            IsBias = reader.ReadBoolean();

            DeviceType = device;

            Embedding = new CudaPieceFloat(reader, device);
            Bias = new CudaPieceFloat(reader, device);
        }

    }
}
