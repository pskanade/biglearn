using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorConvUtil 
    {
        public static int FilterOutput(int inx, int filterSize, int pad, int stride)
        {
            return (inx + 2 * pad - filterSize) / stride + 1;
        }
    }

    public class TensorConvStructure : Structure, IDisposable
    {
        public int FilterWidth { get; set; }
        public int FilterHeight { get; set; }
        public int InDepth { get; set; }
        public int OutDepth { get; set; }

        public CudaPieceFloat Filter;
        public CudaPieceFloat Bias;

        public CudaPieceFloat FilterGrad;
        public CudaPieceFloat BiasGrad;

        public override DataSourceID Type  {  get  { return DataSourceID.TensorConvStructure; } }

        public int FilterSize { get { return FilterWidth * FilterHeight * InDepth * OutDepth; } }
        public TensorConvStructure(int fw, int fh, int inDepth, int outDepth, DeviceType device)
        {
            FilterWidth = fw;
            FilterHeight = fh;
            InDepth = inDepth;
            OutDepth = outDepth;
            DeviceType = device;

            Filter = new CudaPieceFloat(FilterSize, DeviceType);
            Bias = new CudaPieceFloat(OutDepth, DeviceType);

            Init();
        }

        public TensorConvStructure(BinaryReader reader, DeviceType device) : base(reader, device)
        { }

        public override void CopyFrom(IData other)
        {
            Filter.CopyFrom(((TensorConvStructure)other).Filter);
            Bias.CopyFrom(((TensorConvStructure)other).Bias);
        }
        
        protected override void InitStructureOptimizer()
        {
            FilterGrad = new CudaPieceFloat(Filter.Size, DeviceType);
            BiasGrad = new CudaPieceFloat(Bias.Size, DeviceType);

            StructureOptimizer.Add("Filter", new ModelOptimizer() { Parameter = Filter, Gradient = FilterGrad, Name = "Filter" });
            StructureOptimizer.Add("Bias", new ModelOptimizer() { Parameter = Bias, Gradient = BiasGrad, Name = "Bias" });
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(FilterWidth);
            writer.Write(FilterHeight);
            writer.Write(InDepth);
            writer.Write(OutDepth);
            Filter.Serialize(writer);
            Bias.Serialize(writer);
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            FilterWidth = reader.ReadInt32();
            FilterHeight = reader.ReadInt32();
            InDepth = reader.ReadInt32();
            OutDepth = reader.ReadInt32();

            Filter = new CudaPieceFloat(reader, device);
            Bias = new CudaPieceFloat(reader, device);
        }

        public void Init()
        {
            int inputsize = FilterSize / OutDepth;
            int outputsize = OutDepth;
            Filter.Init((float)(Math.Sqrt(1.0 /(inputsize + 1)) * 2), (float)(-Math.Sqrt(1.0 / (inputsize + 1))));
            Bias.Init(0);
        }

        #region Dispose function.
        private bool disposed = false;
        ~TensorConvStructure()
        {
            this.Dispose(false);
        }
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;
            if (disposing)
            {
                if (Filter != null) Filter.Dispose(); Filter = null;
                if (FilterGrad != null) FilterGrad.Dispose(); FilterGrad = null;
                if (Bias != null) Bias.Dispose(); Bias = null;
                if (BiasGrad != null) BiasGrad.Dispose(); BiasGrad = null;
                
            }

            base.Dispose(disposing);
        }
        #endregion.
    }
}
