﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class LSTMCell : Structure, IDisposable
    {
        public int InputFeatureDim;
        public int CellDim;
        public bool AttentionGate = false;
        public int AttentionDim = 0;

        public override DataSourceID Type { get { return DataSourceID.LSTMCell; } }

        /// <summary>
        /// Input to Hidden State Matrix.
        /// </summary>
        public CudaPieceFloat Wi;
        public CudaPieceFloat WiGrad;

        public CudaPieceFloat Ui;
        public CudaPieceFloat UiGrad;

        public CudaPieceFloat Bi;
        public CudaPieceFloat BiGrad;

        public CudaPieceFloat Wc;
        public CudaPieceFloat WcGrad;

        public CudaPieceFloat Uc;
        public CudaPieceFloat UcGrad;

        public CudaPieceFloat Bc;
        public CudaPieceFloat BcGrad;

        public CudaPieceFloat Wf;
        public CudaPieceFloat WfGrad;

        public CudaPieceFloat Uf;
        public CudaPieceFloat UfGrad;

        public CudaPieceFloat Bf;
        public CudaPieceFloat BfGrad;

        public CudaPieceFloat Wo;
        public CudaPieceFloat WoGrad;

        public CudaPieceFloat Uo;
        public CudaPieceFloat UoGrad;

        public CudaPieceFloat Vo;
        public CudaPieceFloat VoGrad;

        public CudaPieceFloat Bo;
        public CudaPieceFloat BoGrad;

        public CudaPieceFloat Zi;
        public CudaPieceFloat ZiGrad;

        public CudaPieceFloat Zf;
        public CudaPieceFloat ZfGrad;

        public CudaPieceFloat Zc;
        public CudaPieceFloat ZcGrad;

        public CudaPieceFloat Zo;
        public CudaPieceFloat ZoGrad;

        public GradientOptimizer WiMatrixOptimizer { get { return StructureOptimizer["Wi"].Optimizer; } }
        public GradientOptimizer UiMatrixOptimizer { get { return StructureOptimizer["Ui"].Optimizer; } }
        public GradientOptimizer BiMatrixOptimizer { get { return StructureOptimizer["Bi"].Optimizer; } }

        public GradientOptimizer WcMatrixOptimizer { get { return StructureOptimizer["Wc"].Optimizer; } }
        public GradientOptimizer UcMatrixOptimizer { get { return StructureOptimizer["Uc"].Optimizer; } }
        public GradientOptimizer BcMatrixOptimizer { get { return StructureOptimizer["Bc"].Optimizer; } }

        public GradientOptimizer WfMatrixOptimizer { get { return StructureOptimizer["Wf"].Optimizer; } }
        public GradientOptimizer UfMatrixOptimizer { get { return StructureOptimizer["Uf"].Optimizer; } }
        public GradientOptimizer BfMatrixOptimizer { get { return StructureOptimizer["Bf"].Optimizer; } }

        public GradientOptimizer WoMatrixOptimizer { get { return StructureOptimizer["Wo"].Optimizer; } }
        public GradientOptimizer UoMatrixOptimizer { get { return StructureOptimizer["Uo"].Optimizer; } }
        public GradientOptimizer VoMatrixOptimizer { get { return StructureOptimizer["Vo"].Optimizer; } }
        public GradientOptimizer BoMatrixOptimizer { get { return StructureOptimizer["Bo"].Optimizer; } }


        public GradientOptimizer ZiMatrixOptimizer { get { return StructureOptimizer["Zi"].Optimizer; } }
        public GradientOptimizer ZfMatrixOptimizer { get { return StructureOptimizer["Zf"].Optimizer; } }
        public GradientOptimizer ZcMatrixOptimizer { get { return StructureOptimizer["Zc"].Optimizer; } }
        public GradientOptimizer ZoMatrixOptimizer { get { return StructureOptimizer["Zo"].Optimizer; } }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(InputFeatureDim);
            writer.Write(CellDim);
            writer.Write(AttentionGate);
            writer.Write(AttentionDim);

            Wi.Serialize(writer);
            Ui.Serialize(writer);
            Bi.Serialize(writer);

            Wc.Serialize(writer);
            Uc.Serialize(writer);
            Bc.Serialize(writer);

            Wf.Serialize(writer);
            Uf.Serialize(writer);
            Bf.Serialize(writer);

            Wo.Serialize(writer);
            Uo.Serialize(writer);
            Vo.Serialize(writer);
            Bo.Serialize(writer);

            if (AttentionGate)
            {
                Zi.Serialize(writer);
                Zf.Serialize(writer);
                Zc.Serialize(writer);
                Zo.Serialize(writer);
            }
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            InputFeatureDim = reader.ReadInt32();
            CellDim = reader.ReadInt32();
            AttentionGate = reader.ReadBoolean();
            AttentionDim = reader.ReadInt32();
            DeviceType = device;

            Wi = new CudaPieceFloat(reader, device);
            Ui = new CudaPieceFloat(reader, device);
            Bi = new CudaPieceFloat(reader, device);

            Wc = new CudaPieceFloat(reader, device);
            Uc = new CudaPieceFloat(reader, device);
            Bc = new CudaPieceFloat(reader, device);

            Wf = new CudaPieceFloat(reader, device);
            Uf = new CudaPieceFloat(reader, device);
            Bf = new CudaPieceFloat(reader, device);

            Wo = new CudaPieceFloat(reader, device);
            Uo = new CudaPieceFloat(reader, device);
            Vo = new CudaPieceFloat(reader, device);
            Bo = new CudaPieceFloat(reader, device);

            if (AttentionGate)
            {
                Zi = new CudaPieceFloat(reader, device);
                Zf = new CudaPieceFloat(reader, device);
                Zc = new CudaPieceFloat(reader, device);
                Zo = new CudaPieceFloat(reader, device);
            }
        }

        protected override void InitStructureOptimizer()
        {
            WiGrad = new CudaPieceFloat(Wi.EffectiveSize, DeviceType);
            UiGrad = new CudaPieceFloat(Ui.EffectiveSize, DeviceType);
            BiGrad = new CudaPieceFloat(Bi.EffectiveSize, DeviceType);

            WcGrad = new CudaPieceFloat(Wc.EffectiveSize, DeviceType);
            UcGrad = new CudaPieceFloat(Uc.EffectiveSize, DeviceType);
            BcGrad = new CudaPieceFloat(Bc.EffectiveSize, DeviceType);

            WfGrad = new CudaPieceFloat(Wf.EffectiveSize, DeviceType);
            UfGrad = new CudaPieceFloat(Uf.EffectiveSize, DeviceType);
            BfGrad = new CudaPieceFloat(Bf.EffectiveSize, DeviceType);

            WoGrad = new CudaPieceFloat(Wo.EffectiveSize, DeviceType);
            UoGrad = new CudaPieceFloat(Uo.EffectiveSize, DeviceType);
            VoGrad = new CudaPieceFloat(Vo.EffectiveSize, DeviceType);
            BoGrad = new CudaPieceFloat(Bo.EffectiveSize, DeviceType);


            StructureOptimizer.Add("Wi", new ModelOptimizer() { Parameter = Wi, Gradient = WiGrad, Name = "LSTM_Wi" });
            StructureOptimizer.Add("Ui", new ModelOptimizer() { Parameter = Ui, Gradient = UiGrad, Name = "LSTM_Ui" });
            StructureOptimizer.Add("Bi", new ModelOptimizer() { Parameter = Bi, Gradient = BiGrad, Name = "LSTM_Bi" });

            StructureOptimizer.Add("Wc", new ModelOptimizer() { Parameter = Wc, Gradient = WcGrad, Name = "LSTM_Wc" });
            StructureOptimizer.Add("Uc", new ModelOptimizer() { Parameter = Uc, Gradient = UcGrad, Name = "LSTM_Uc" });

            StructureOptimizer.Add("Bc", new ModelOptimizer() { Parameter = Bc, Gradient = BcGrad, Name = "LSTM_Bc" });

            StructureOptimizer.Add("Wf", new ModelOptimizer() { Parameter = Wf, Gradient = WfGrad, Name = "LSTM_Wf" });

            StructureOptimizer.Add("Uf", new ModelOptimizer() { Parameter = Uf, Gradient = UfGrad, Name = "LSTM_Uf" });
            StructureOptimizer.Add("Bf", new ModelOptimizer() { Parameter = Bf, Gradient = BfGrad, Name = "LSTM_Bf" });

            StructureOptimizer.Add("Wo", new ModelOptimizer() { Parameter = Wo, Gradient = WoGrad, Name = "LSTM_Wo" });
            StructureOptimizer.Add("Uo", new ModelOptimizer() { Parameter = Uo, Gradient = UoGrad, Name = "LSTM_Uo" });
            StructureOptimizer.Add("Vo", new ModelOptimizer() { Parameter = Vo, Gradient = VoGrad, Name = "LSTM_Vo" });
            StructureOptimizer.Add("Bo", new ModelOptimizer() { Parameter = Bo, Gradient = BoGrad, Name = "LSTM_Bo" });

            if (AttentionGate)
            {
                ZiGrad = new CudaPieceFloat(Zi.EffectiveSize, DeviceType);
                ZfGrad = new CudaPieceFloat(Zf.EffectiveSize, DeviceType);
                ZcGrad = new CudaPieceFloat(Zc.EffectiveSize, DeviceType);
                ZoGrad = new CudaPieceFloat(Zo.EffectiveSize, DeviceType);
                
                StructureOptimizer.Add("Zi", new ModelOptimizer() { Parameter = Zi, Gradient = ZiGrad, Name = "LSTM_Zi" });
                StructureOptimizer.Add("Zf", new ModelOptimizer() { Parameter = Zf, Gradient = ZfGrad, Name = "LSTM_Zf" });
                StructureOptimizer.Add("Zc", new ModelOptimizer() { Parameter = Zc, Gradient = ZcGrad, Name = "LSTM_Zc" });
                StructureOptimizer.Add("Zo", new ModelOptimizer() { Parameter = Zo, Gradient = ZoGrad, Name = "LSTM_Zo" });
            }
        }

        public void InitMemory(DeviceType device)
        {
            Wi = new CudaPieceFloat(InputFeatureDim * CellDim, true, device == DeviceType.GPU);
            Ui = new CudaPieceFloat(CellDim * CellDim, true, device == DeviceType.GPU);
            Bi = new CudaPieceFloat(CellDim, true, device == DeviceType.GPU);
            Wi.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + CellDim))));
            if(RndInit == RndRecurrentInit.RndOrthogonal) Ui.InitRandomOrthogonalMatrix(CellDim);
            else if (RndInit == RndRecurrentInit.RndNorm) Ui.Init((float)(Math.Sqrt(6.0 / (CellDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (CellDim + CellDim))));
            Bi.Init(0);

            Wc = new CudaPieceFloat(InputFeatureDim * CellDim, true, device == DeviceType.GPU);
            Uc = new CudaPieceFloat(CellDim * CellDim, true, device == DeviceType.GPU);
            Bc = new CudaPieceFloat(CellDim, true, device == DeviceType.GPU);
            Wc.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + CellDim))));
            if (RndInit == RndRecurrentInit.RndOrthogonal) Uc.InitRandomOrthogonalMatrix(CellDim);
            else if (RndInit == RndRecurrentInit.RndNorm) Uc.Init((float)(Math.Sqrt(6.0 / (CellDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (CellDim + CellDim))));
            Bc.Init(0);

            Wf = new CudaPieceFloat(InputFeatureDim * CellDim, true, device == DeviceType.GPU);
            Uf = new CudaPieceFloat(CellDim * CellDim, true, device == DeviceType.GPU);
            Bf = new CudaPieceFloat(CellDim, true, device == DeviceType.GPU);
            Wf.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + CellDim))));
            if (RndInit == RndRecurrentInit.RndOrthogonal) Uf.InitRandomOrthogonalMatrix(CellDim);
            else if (RndInit == RndRecurrentInit.RndNorm) Uf.Init((float)(Math.Sqrt(6.0 / (CellDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (CellDim + CellDim))));

            // forget gate bias is very important.
            Bf.Init(1.5f);

            Wo = new CudaPieceFloat(InputFeatureDim * CellDim, true, device == DeviceType.GPU);
            Uo = new CudaPieceFloat(CellDim * CellDim, true, device == DeviceType.GPU);
            Vo = new CudaPieceFloat(CellDim * CellDim, true, device == DeviceType.GPU);
            Bo = new CudaPieceFloat(CellDim, true, device == DeviceType.GPU);
            Wo.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + CellDim))));
            if (RndInit == RndRecurrentInit.RndOrthogonal) Uo.InitRandomOrthogonalMatrix(CellDim);
            else if (RndInit == RndRecurrentInit.RndNorm) Uo.Init((float)(Math.Sqrt(6.0 / (CellDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (CellDim + CellDim))));

            if (RndInit == RndRecurrentInit.RndOrthogonal) Vo.InitRandomOrthogonalMatrix(CellDim);
            else if (RndInit == RndRecurrentInit.RndNorm) Vo.Init((float)(Math.Sqrt(6.0 / (CellDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (CellDim + CellDim))));
            Bo.Init(0);

            if (AttentionGate)
            {
                Zi = new CudaPieceFloat(AttentionDim * CellDim, true, device == DeviceType.GPU);
                Zf = new CudaPieceFloat(AttentionDim * CellDim, true, device == DeviceType.GPU);
                Zc = new CudaPieceFloat(AttentionDim * CellDim, true, device == DeviceType.GPU);
                Zo = new CudaPieceFloat(AttentionDim * CellDim, true, device == DeviceType.GPU);

                Zi.Init((float)(Math.Sqrt(6.0 / (AttentionDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + CellDim))));
                Zf.Init((float)(Math.Sqrt(6.0 / (AttentionDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + CellDim))));
                Zc.Init((float)(Math.Sqrt(6.0 / (AttentionDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + CellDim))));
                Zo.Init((float)(Math.Sqrt(6.0 / (AttentionDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + CellDim))));
            }
        }

        RndRecurrentInit RndInit = RndRecurrentInit.RndNorm;
        public LSTMCell(int input, int cell, int attentionDim, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm)
        {
            InputFeatureDim = input;
            CellDim = cell;
            RndInit = rndInit;
            AttentionDim = attentionDim;
            AttentionGate = AttentionDim == 0 ? false : true;
            DeviceType = device;
            InitMemory(device);
        }

        public LSTMCell(int input, int cell, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm) : this(input, cell, 0, device, rndInit) { }

        public LSTMCell(int input, int cell, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm) : this(input, cell, DeviceType.GPU, rndInit) { }

        public LSTMCell(BinaryReader reader, DeviceType device) : base(reader, device) { }


        public override void SyncToCPU()
        {
            Wi.SyncToCPU();
            Ui.SyncToCPU();
            Bi.SyncToCPU();

            Wc.SyncToCPU();
            Uc.SyncToCPU();
            Bc.SyncToCPU();

            Wf.SyncToCPU();
            Uf.SyncToCPU();
            Bf.SyncToCPU();

            Wo.SyncToCPU();
            Uo.SyncToCPU();
            Vo.SyncToCPU();
            Bo.SyncToCPU();

            if (AttentionGate)
            {
                Zi.SyncToCPU();
                Zf.SyncToCPU();
                Zc.SyncToCPU();
                Zo.SyncToCPU();
            }
        }

        #region IData Interface.
        public override void CopyFrom(IData other)
        {
            LSTMCell refStructure = (LSTMCell)other;

            Wi.CopyFrom(refStructure.Wi);
            Ui.CopyFrom(refStructure.Ui);
            Bi.CopyFrom(refStructure.Bi);

            Wc.CopyFrom(refStructure.Wc);
            Uc.CopyFrom(refStructure.Uc);
            Bc.CopyFrom(refStructure.Bc);

            Wf.CopyFrom(refStructure.Wf);
            Uf.CopyFrom(refStructure.Uf);
            Bf.CopyFrom(refStructure.Bf);

            Wo.CopyFrom(refStructure.Wo);
            Uo.CopyFrom(refStructure.Uo);
            Vo.CopyFrom(refStructure.Vo);
            Bo.CopyFrom(refStructure.Bo);

            if (AttentionGate)
            {
                Zi.CopyFrom(refStructure.Zi);
                Zf.CopyFrom(refStructure.Zf);
                Zc.CopyFrom(refStructure.Zc);
                Zo.CopyFrom(refStructure.Zo);
            }
        }
        #endregion.

    }
}
