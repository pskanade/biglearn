﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum SimilarityType { CosineSimilarity = 0, InnerProduct = 1 }
    public enum A_Func { Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3 };
    public enum N_Type { Fully_Connected = 0, Convolution_layer = 1, Tensor_layer = 2 };
    public enum P_Pooling { MAX_Pooling = 0 };

    public enum Recurrent_Unit { GRU = 0, LSTM = 1};
    /// <summary>
    /// NN Layer Parameters
    /// </summary>
    public sealed class LayerStructure : Structure, IDisposable
    {
        public int Neural_In;
        public int Neural_Out;

        public CudaPieceFloat weight;
        public CudaPieceFloat bias;
        public CudaPieceFloat tensorWeight;

        public IntPtr Weight { get { return weight.CudaPtr; } }
        public IntPtr Bias { get { return bias.CudaPtr; } }

        public float[] Back_Weight { get { return weight.MemPtr; } }
        public float[] Back_Bias { get { return bias.MemPtr; } }

        public A_Func Af;
        public N_Type Nt = N_Type.Fully_Connected;
        public int N_Winsize = 1;
        public P_Pooling pool_type = P_Pooling.MAX_Pooling;
        public bool IsBias = false;
        public float DropOut = 0;

        public GradientOptimizer TensorWeightOptimizer { get { return StructureOptimizer["Tensor"].Optimizer; } }
        public GradientOptimizer WeightOptimizer { get { return StructureOptimizer["Weight"].Optimizer; } set { StructureOptimizer["Weight"].Optimizer = value;  } }
        public GradientOptimizer BiasOptimizer { get { return StructureOptimizer["Bias"].Optimizer; } set { StructureOptimizer["Bias"].Optimizer = value; } }

        public CudaPieceFloat TensorWeightGrad = null;
        public CudaPieceFloat WeightGrad = null;
        public CudaPieceFloat BiasGrad = null;
        bool IsTrain = true;

        public LayerStructure(int layer_in, int layer_out, A_Func af, N_Type nt, int win_size, float dropOut, bool isbias)
            : this(layer_in, layer_out, af, nt, win_size, dropOut, isbias, DeviceType.GPU)
        { }

        public LayerStructure(int layer_in, int layer_out, A_Func af, N_Type nt, int win_size, float dropOut, bool isbias, DeviceType device, bool isTrain = true)
        {
            Neural_In = layer_in;
            Neural_Out = layer_out;
            Nt = nt;
            N_Winsize = win_size;
            Af = af;

            IsTrain = isTrain;
            IsBias = isbias;
            DropOut = dropOut;
            DeviceType = device;

            weight = new CudaPieceFloat(Neural_In * Neural_Out * N_Winsize, device);
            bias = new CudaPieceFloat(Neural_Out, device);
            if (nt == N_Type.Tensor_layer) tensorWeight = new CudaPieceFloat(Neural_In * Neural_In * Neural_Out, device);

            Init();
        }

        public LayerStructure(int layer_in, int layer_out, A_Func af, bool isbias, CudaPieceFloat pweight, CudaPieceFloat pweightGrad, 
                CudaPieceFloat pbias, CudaPieceFloat pbiasGrad, DeviceType device)
        {
            Neural_In = layer_in;
            Neural_Out = layer_out;
            Af = af;
            IsBias = isbias;
            DeviceType = device;

            weight = pweight;
            bias = pbias;

            WeightGrad = pweightGrad;
            BiasGrad = pbiasGrad;
        }

        public LayerStructure(BinaryReader reader, DeviceType device) : base(reader, device) { }


        protected override void InitStructureOptimizer()
        {
            if (IsTrain)
            {
                if(WeightGrad == null) WeightGrad = new CudaPieceFloat(Neural_In * Neural_Out * N_Winsize, DeviceType);

                StructureOptimizer.Add("Weight", new ModelOptimizer() { Parameter = weight, Gradient = WeightGrad, Name = "LayerStructure_weight" });
                if ( IsBias)
                {
                    if(BiasGrad == null) BiasGrad = new CudaPieceFloat(Neural_Out, DeviceType);
                    StructureOptimizer.Add("Bias", new ModelOptimizer() { Parameter = bias, Gradient = BiasGrad, Name = "LayerStructure_bias" });
                }
                if (Nt == N_Type.Tensor_layer)
                {
                    TensorWeightGrad = new CudaPieceFloat(Neural_In * Neural_In * Neural_Out, DeviceType);
                    StructureOptimizer.Add("Tensor", new ModelOptimizer() { Parameter = tensorWeight, Gradient = TensorWeightGrad, Name = "LayerStructure_tensor" });
                }
            }
        }

        public override void SyncToCPU()
        {
            weight.SyncToCPU();
            bias.SyncToCPU();
            if (Nt == N_Type.Tensor_layer) tensorWeight.SyncToCPU();
        }
        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(Neural_In);
            writer.Write(Neural_Out);
            writer.Write((int)Af);
            writer.Write(IsBias ? 1 : 0);
            writer.Write((int)Nt);
            writer.Write(N_Winsize);
            writer.Write((int)pool_type);
            writer.Write(DropOut);

            weight.CopyOutFromCuda();
            bias.CopyOutFromCuda();

            writer.Write(Back_Weight.Length);
            for (int m = 0; m < Back_Weight.Length; m++) writer.Write(Back_Weight[m]);

            writer.Write(Neural_Out);
            for (int m = 0; m < Neural_Out; m++) writer.Write(Back_Bias[m]);

        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            Neural_In = reader.ReadInt32();
            Neural_Out = reader.ReadInt32();
            Af = (A_Func)reader.ReadInt32();
            IsBias = reader.ReadInt32() > 0 ? true : false;
            Nt = (N_Type)reader.ReadInt32();
            N_Winsize = reader.ReadInt32();
            pool_type = (P_Pooling)reader.ReadInt32();
            DropOut = reader.ReadSingle();

            DeviceType = device;
            weight = new CudaPieceFloat(Neural_In * Neural_Out * N_Winsize, true, device == DeviceType.GPU);
            bias = new CudaPieceFloat(Neural_Out, true, device == DeviceType.GPU);

            int weight_len = reader.ReadInt32();
            if (weight_len != Back_Weight.Length)
            {
                Console.WriteLine("Loading Model Weight Error!  " + weight_len.ToString() + " " + Back_Weight.Length.ToString());
                Console.ReadLine();
            }
            for (int m = 0; m < weight_len; m++)                            // i.e., input dimension = 5; output dimension = 10; len of weights = 50;
                Back_Weight[m] = reader.ReadSingle();       // 12. Read float for each weight.  

            int bias_len = reader.ReadInt32();                             // 13. The len of bias. It should equals to output dimension of each link.
            if (bias_len != Back_Bias.Length)
            {
                Console.WriteLine("Loading Model Bias Error!  " + bias_len.ToString() + " " + Back_Bias.Length.ToString());
                Console.ReadLine();
            }
            for (int m = 0; m < bias_len; m++)
                Back_Bias[m] = reader.ReadSingle();         // 14. Read float for each bias.  

            if (device == DeviceType.GPU)
            {
                weight.CopyIntoCuda();
                bias.CopyIntoCuda();
            }

        }

        public override DataSourceID Type { get { return DataSourceID.LayerStructure; } }

        public override void CopyFrom(IData other)
        {
            LayerStructure refStructure = (LayerStructure)other;
            if (Nt == N_Type.Tensor_layer) tensorWeight.CopyFrom(refStructure.tensorWeight);
            weight.CopyFrom(refStructure.weight);
            bias.CopyFrom(refStructure.bias);
        }


        private bool disposed = false;
        ~LayerStructure()
        {
            this.Dispose(false);
        }

        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;
            if (disposing)
            {
                if (weight != null) weight.Dispose(); weight = null;
                if (bias != null) bias.Dispose(); bias = null;
                if (tensorWeight != null) tensorWeight.Dispose(); tensorWeight = null;
            }

            base.Dispose(disposing);
        }

        public void Init()
        {
            int inputsize = Neural_In * N_Winsize;
            int outputsize = Neural_Out;

            bias.Init(0);

            if (Nt == N_Type.Tensor_layer)
            {
                tensorWeight.Init((float)(Math.Sqrt(6.0 / (Neural_In * (Neural_In + 1) + outputsize)) * 2), (float)(-Math.Sqrt(6.0 / (Neural_In * (Neural_In + 1) + outputsize))));
                weight.Init((float)(Math.Sqrt(6.0 / (Neural_In * (Neural_In + 1) + outputsize)) * 2), (float)(-Math.Sqrt(6.0 / (Neural_In * (Neural_In + 1) + outputsize))));
            }
            else
            {
                weight.Init((float)(Math.Sqrt(6.0 / (inputsize + outputsize)) * 2), (float)(-Math.Sqrt(6.0 / (inputsize + outputsize))));
            }
        }
        public void Init(float wei)
        {
            if (Nt == N_Type.Tensor_layer) tensorWeight.Init(wei);
            weight.Init(wei);
            bias.Init(0);
        }

        public void Init(float wei_scale, float wei_bias)
        {
            if (Nt == N_Type.Tensor_layer) tensorWeight.Init(wei_scale, wei_bias);
            weight.Init(wei_scale, wei_bias);
            bias.Init(0);
        }

        public void Init(LayerStructure refLink)
        {
            if (Nt == N_Type.Tensor_layer) tensorWeight.Init(refLink.tensorWeight.MemPtr);
            weight.Init(refLink.Back_Weight);
            bias.Init(refLink.Back_Bias);
        }

    }
}
