﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class RNNCell : Structure
    {
        public int InputFeatureDim;
        public int HiddenStateDim;
        public int HistoryLag;
        public A_Func Af;
        public bool IsBias = true;

        /// <summary>
        /// Input to Hidden State Matrix.
        /// </summary>
        public CudaPieceFloat IMatrix;
        
        /// <summary>
        /// Hidden State History Matrix.
        /// </summary>
        public CudaPieceFloat WMatrix;

        /// <summary>
        /// Hidden State Bias.
        /// </summary>
        public CudaPieceFloat Bias;

        public GradientOptimizer IMatrixOptimizer { get { return StructureOptimizer["IMatrix"].Optimizer; } }
        public GradientOptimizer WMatrixOptimizer { get { return StructureOptimizer["WMatrix"].Optimizer; } }
        public GradientOptimizer BiasOptimizer { get { return StructureOptimizer["Bias"].Optimizer; } }
        
        public RNNCell(int input, int hidden, int lag, A_Func af, bool isBias) : this(input, hidden, lag, af, isBias, DeviceType.GPU)
        { 
        }

        public RNNCell(int input, int hidden, int lag, A_Func af, bool isBias, DeviceType device)
        {
            InputFeatureDim = input;
            HiddenStateDim = hidden;
            HistoryLag = lag;
            Af = af;
            IsBias = isBias;

            IMatrix = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, true, device == DeviceType.GPU);
            WMatrix = new CudaPieceFloat(HiddenStateDim * HistoryLag * HiddenStateDim, true, device == DeviceType.GPU);
            Bias = new CudaPieceFloat(HiddenStateDim, true, device == DeviceType.GPU);
        }


        public RNNCell(BinaryReader reader, DeviceType device) : base(reader, device)
        { 
        }

        protected override void InitStructureOptimizer()
        {
            StructureOptimizer.Add("IMatrix", new ModelOptimizer() { Parameter = IMatrix });
            StructureOptimizer.Add("WMatrix", new ModelOptimizer() { Parameter = WMatrix });
            StructureOptimizer.Add("Bias", new ModelOptimizer() { Parameter = Bias });
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(InputFeatureDim);
            writer.Write(HiddenStateDim);
            writer.Write(HistoryLag);
            writer.Write((int)Af);
            writer.Write(IsBias);

            IMatrix.SyncToCPU();
            writer.Write(IMatrix.Size);
            for (int s = 0; s < IMatrix.Size; s++) writer.Write(IMatrix.MemPtr[s]);

            WMatrix.SyncToCPU();
            writer.Write(WMatrix.Size);
            for (int s = 0; s < WMatrix.Size; s++) writer.Write(WMatrix.MemPtr[s]);

            Bias.SyncToCPU();
            writer.Write(Bias.Size);
            for (int s = 0; s < Bias.Size; s++) writer.Write(Bias.MemPtr[s]);
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            InputFeatureDim = reader.ReadInt32();
            HiddenStateDim = reader.ReadInt32();
            HistoryLag = reader.ReadInt32();
            Af = (A_Func)reader.ReadInt32();
            IsBias = reader.ReadBoolean();

            IMatrix = new CudaPieceFloat(reader.ReadInt32(), true, device== DeviceType.GPU);
            for (int s = 0; s < IMatrix.Size; s++) IMatrix.MemPtr[s] = reader.ReadSingle();

            WMatrix = new CudaPieceFloat(reader.ReadInt32(), true, device == DeviceType.GPU);
            for (int s = 0; s < WMatrix.Size; s++) WMatrix.MemPtr[s] = reader.ReadSingle();

            Bias = new CudaPieceFloat(reader.ReadInt32(), true, device == DeviceType.GPU);
            for (int s = 0; s < Bias.Size; s++) Bias.MemPtr[s] = reader.ReadSingle();

            IMatrix.SyncFromCPU();
            WMatrix.SyncFromCPU();
            Bias.SyncFromCPU();
        }

        public void Init()
        {
            IMatrix.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim))));
            WMatrix.Init((float)(Math.Sqrt(0.6 / (HiddenStateDim * HistoryLag + HiddenStateDim)) * 2), (float)(-Math.Sqrt(0.6 / (HiddenStateDim * HistoryLag + HiddenStateDim))));
            if (HistoryLag > 0)
            {
                for (int i = 0; i < HiddenStateDim; i++)
                {
                    WMatrix.MemPtr[i * HiddenStateDim + i] = 0.35f;
                }
                WMatrix.CopyIntoCuda();
            }
            Bias.Init(0);
        }

        #region Dispose Function.
        ~RNNCell()
        {
            this.Dispose(false);
        }

        private bool disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;

            if (disposing)
            {
                if (IMatrix != null)
                {
                    IMatrix.Dispose();
                }

                if (WMatrix != null)
                {
                    WMatrix.Dispose();
                }

                if (Bias != null)
                {
                    Bias.Dispose();
                }
            }

            IMatrix = null;
            WMatrix = null;
            Bias = null;

            base.Dispose(disposing);
        }
        #endregion
    }

    public class RNNStructure : Structure, IDisposable
    {
        ///Multi Layer RNN Model.
        public List<RNNCell> RNNCells = new List<RNNCell>();

        public override List<Structure> SubStructure { get { return RNNCells.Select(i => (Structure)i).ToList(); } }

        public RNNStructure(int inputFea, int[] hiddenStates, int[] lags, A_Func[] afs, bool[] isBiases) : this(inputFea, hiddenStates, lags, afs, isBiases, DeviceType.GPU)
        { }

        public RNNStructure(int inputFea, int[] hiddenStates, int[] lags, A_Func[] afs, bool[] isBiases, DeviceType device)
        {
            int input = inputFea;
            for (int i = 0; i < hiddenStates.Length; i++)
            {
                RNNCell cell = new RNNCell(input, hiddenStates[i], lags[i], afs[i], isBiases[i], device);
                cell.Init();
                RNNCells.Add(cell);
                input = hiddenStates[i];
            }
        }

        public RNNStructure(BinaryReader reader, DeviceType device) : base(reader, device)
        { }
        
        private bool disposed = false;
        ~RNNStructure()
        {
            this.Dispose(false);
        }

        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;
            if (disposing)
            {
                if (RNNCells == null)
                {
                    return;
                }

                foreach (RNNCell cell in RNNCells)
                {
                    if (cell != null)
                    {
                        cell.Dispose();
                    }
                }

                RNNCells.Clear();
                RNNCells = null;
            }

            base.Dispose(disposing);
        }

        public override void Serialize(BinaryWriter mwriter)
        {
            mwriter.Write(0);
            mwriter.Write(0);
            mwriter.Write(0);
            mwriter.Write(0);

            mwriter.Write(RNNCells.Count);
            for (int i = 0; i < RNNCells.Count; i++) RNNCells[i].Serialize(mwriter);
        }

        public override void Deserialize(BinaryReader mreader, DeviceType device)
        {
            mreader.ReadInt32();
            mreader.ReadInt32();
            mreader.ReadInt32();
            mreader.ReadInt32();

            int layer = mreader.ReadInt32();
            for (int i = 0; i < layer; i++) RNNCells.Add(new RNNCell(mreader, device));
        }
    }

    public class RNNLabelStructure : Structure, IDisposable
    {
        public RNNStructure RNN;
        public LayerStructure Output;

        public int StackLen;
        public int OutputDim;

        public override List<Structure> SubStructure { get { return new Structure[] { RNN, Output }.ToList(); } }

        public RNNLabelStructure(RNNStructure rnn, int stackLen, int output, DeviceType device)
        {
            RNN = rnn;
            Output = new LayerStructure(RNN.RNNCells.Last().HiddenStateDim * (stackLen + 1), output, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device);

            StackLen = stackLen;
            OutputDim = output;
        }

        public RNNLabelStructure(BinaryReader reader, DeviceType device) : base(reader, device)
        { }
               
        private bool disposed = false;
        ~RNNLabelStructure()
        {
            this.Dispose(false);
        }

        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;
            if (disposing)
            {
                if (RNN != null) RNN.Dispose(); RNN = null;
                if (Output != null) Output.Dispose(); Output = null;
            }

            base.Dispose(disposing);
        }

        public override void Serialize(BinaryWriter writer)
        {
            RNN.Serialize(writer);
            writer.Write(StackLen);
            writer.Write(OutputDim);
            Output.Serialize(writer);
        }

        public override void Deserialize(BinaryReader reader, DeviceType device)
        {
            RNN = new RNNStructure(reader, device);
            StackLen = reader.ReadInt32();
            OutputDim = reader.ReadInt32();
            Output = new LayerStructure(reader, device);
        }
    }
}
