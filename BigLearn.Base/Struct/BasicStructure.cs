﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{

    public class ModelOptimizer
    {
        public CudaPieceFloat Parameter;
        public CudaPieceFloat Gradient;
        public GradientOptimizer Optimizer;
		public string Name; // it is for debug purpose.
    }

    /// <summary>
    /// Basic Structure Parameters. 
    /// </summary>
    public class Structure : IData, IGlobalOptimizer, IDisposable
    {
        /// <summary>
        /// Sub Structure. 
        /// </summary>
        public virtual List<Structure> SubStructure { get { return new List<Structure>(); } }

        /// <summary>
        /// Model Parameter Optimizer.
        /// </summary>
        public Dictionary<string, ModelOptimizer> StructureOptimizer = new Dictionary<string, ModelOptimizer>();



        /// <summary>
        /// enum all the optimizers in the model structure.
        /// </summary>
        public virtual List<ModelOptimizer> ModelOptimizers
        {
            get
            {
                List<ModelOptimizer> mOptimizers = new List<ModelOptimizer>();
                mOptimizers.AddRange(StructureOptimizer.Where(i => i.Value != null && i.Value.Parameter != null).Select(i => i.Value));
                foreach (Structure sub in SubStructure) mOptimizers.AddRange(sub.ModelOptimizers);
                return mOptimizers;
            }
        }

        /// <summary>
        /// Get Model Gradient.
        /// </summary>
        /// <param name="grad"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public int GetModelGradient(CudaPieceFloat grad, int index)
        {
            int start = index;
            foreach (ModelOptimizer opt in ModelOptimizers)
            {
                grad.CopyFrom(index, opt.Optimizer.Gradient, 0, opt.Optimizer.Gradient.Size);
                index += opt.Optimizer.Gradient.Size;
            }
            return index - start;
        }

        public int GetModelParameter(CudaPieceFloat param, int index)
        {
            int start = index;
            foreach(ModelOptimizer opt in ModelOptimizers)
            {
                param.CopyFrom(index, opt.Parameter, 0, opt.Parameter.Size);
                index += opt.Parameter.Size;
            }
            return index - start;
        }

        public int SetModelParameter(CudaPieceFloat param, int index)
        {
            int start = index;
            foreach (ModelOptimizer opt in ModelOptimizers)
            {
                opt.Parameter.CopyFrom(0, param, index, opt.Parameter.Size);
                index += opt.Parameter.Size;
            }
            return index - start;
        }

        /// <summary>
        /// self = self * alpha + beta * param
        /// </summary>
        /// <param name="param"></param>
        /// <param name="index"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        /// <returns></returns>
        public int AggragateModelParameter(IMathOperationManager lib, CudaPieceFloat param, int index, float alpha, float beta)
        {
            int start = index;
            foreach (ModelOptimizer opt in ModelOptimizers)
            {
                lib.Add_Vector(opt.Parameter, 0, param, index, opt.Parameter.Size, alpha, beta); 
                index += opt.Parameter.Size;
            }
            return index - start;
        }

        public int AggragateModelParameter(IMathOperationManager lib, Structure param, float alpha, float beta)
        {
            List<ModelOptimizer> source = ModelOptimizers;
            List<ModelOptimizer> append = param.ModelOptimizers;

            if (source.Count != append.Count) { throw new Exception(string.Format("Model fusion can not be successed {0}, {1}.", source.Count, append.Count)); }

            int index = 0;
            for (int i = 0; i < source.Count; i++)
            {
                if (source[i].Parameter.Size != append[i].Parameter.Size)
                {
                    throw new Exception(string.Format("Model fusion can not be successed parameter number doesn't match {0}, {1}.", source[i].Parameter.Size, append[i].Parameter.Size));
                }
                lib.Add_Vector(source[i].Parameter, 0, append[0].Parameter, 0, source[i].Parameter.Size, alpha, beta);
                index += source[i].Parameter.Size;
            }
            return index;
        }


        public Structure() { /*InitStructureOptimizer();*/ }
        public Structure(BinaryReader reader, DeviceType device) { DeviceType = device; Deserialize(reader, device); }

        public static Structure Empty { get { return new Structure(); } }

        protected virtual void InitStructureOptimizer() 
        { }

        /// <summary>
        /// It will be staled. Please try to use InitOptimizer(StructureLearner learner, RunnerBehavior) instead...
        /// </summary>
        /// <param name="learner"></param>
        public virtual void InitOptimizer(StructureLearner learner)
        {
            throw new Exception("InitOptimizer is staled, please call InitOptimizer(StructureLearner learner, RunnerBehavior behavior)! ");
        }

        public void InitOptimizer(StructureLearner learner, RunnerBehavior behavior)
        {
            InitStructureOptimizer();
            foreach (ModelOptimizer optimizer in StructureOptimizer.Values)
            {
                if (optimizer == null) continue;
                if (optimizer.Parameter == null) continue;
                if (optimizer.Optimizer != null)
                {
                    Console.WriteLine("Structure {0} is previously set to optimizer {1}, so it will keep", optimizer.Name, optimizer.Optimizer.ToString());
                    continue;
                }
                else
                {
                    if (optimizer.Gradient == null)
                    {
                        throw new Exception(string.Format("Structure {0} havn't set the gradient to its optimizer {1}", optimizer.Name, optimizer.Optimizer.ToString()));
                    }
                    optimizer.Optimizer = GradientOptimizer.CreateLocalOptimizer(optimizer.Parameter, optimizer.Gradient, learner, behavior);
                    Console.WriteLine("Structure {0} with gradient is set to optimizer {1} done! ", optimizer.Name, optimizer.Optimizer.ToString());
                }
            }
            foreach (Structure sub in SubStructure) sub.InitOptimizer(learner, behavior);
        }

        public void TrackModelParameter()
        {
            foreach (ModelOptimizer subOptimizer in ModelOptimizers)
            {
                ///check L2 norm of model parameters.
                float l2_norm = subOptimizer.Optimizer.ComputeLib.L2Norm(subOptimizer.Optimizer.Parameter, subOptimizer.Optimizer.Parameter.Size);
                Console.WriteLine("Name {0} : {1} : {2} : {3}", subOptimizer.Name, l2_norm, subOptimizer.Optimizer.Parameter.Size, l2_norm / Math.Sqrt(subOptimizer.Optimizer.Parameter.Size));
            }
        }

        public bool TrackParameterNAN()
        {
            bool isnan = false;
            foreach (ModelOptimizer subOptimizer in ModelOptimizers)
            {
                ///check L2 norm of model parameters.
                float l2_norm = subOptimizer.Optimizer.ComputeLib.L2Norm(subOptimizer.Optimizer.Parameter, subOptimizer.Optimizer.Parameter.Size);
                if(float.IsNaN(l2_norm))
                {
                    Console.WriteLine("Name {0} : {1} : {2} : {3}", subOptimizer.Name, l2_norm, subOptimizer.Optimizer.Parameter.Size, l2_norm / Math.Sqrt(subOptimizer.Optimizer.Parameter.Size));
                    Console.WriteLine("Press any key to continue ...");
                    Console.ReadLine();
                    isnan = true;
                }
            }
            return isnan;
        }

        public double L1Norm()
        {
            double l1_norm = 0;
            foreach (ModelOptimizer subOptimizer in ModelOptimizers)
            {
                subOptimizer.Optimizer.Parameter.SyncToCPU();
                l1_norm += subOptimizer.Optimizer.Parameter.MemPtr.Sum(a => Math.Abs(a));
            }
            return l1_norm;
        }

        /// <summary>
        /// Set Model Gradient to Zero;
        /// </summary>
        /// <param name="learner"></param>
        public void ClearModelGradient()
        {
            foreach (ModelOptimizer subOptimizer in ModelOptimizers)
            {
                subOptimizer.Optimizer.ComputeLib.Zero(subOptimizer.Optimizer.Gradient, subOptimizer.Optimizer.Gradient.Size);
            }
        }        

        public virtual void Serialize(BinaryWriter writer) { }
        public virtual void Deserialize(BinaryReader reader, DeviceType device) { }

        public virtual void Save(string fileName)
        {
            using (FileStream mstream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                using (BinaryWriter mwriter = new BinaryWriter(mstream))
                {
                    Serialize(mwriter);
                }
            }
        }

        /// <summary>
        /// Model Parameter Number.
        /// </summary>
        public int ParamNumber
        {
            get
            {
                int paramNum = 0;
                foreach (ModelOptimizer opt in ModelOptimizers) { paramNum += opt.Parameter.Size; }
                return paramNum;
            }
        }

        public virtual IMetaInfo Meta { get; set; }

        public virtual IData CloneAs(DeviceType deviceType) { throw new NotImplementedException(); }

        public virtual void CopyFrom(IData other) { throw new NotImplementedException(); }

        #region IOUnit Interface.
        public virtual IOUnitHeader Head { get; set; }

        public virtual DataSourceID Type { get; set; }

        public virtual DeviceType DeviceType { get ; set; }

        public virtual void Load(BinaryReader input) { throw new NotImplementedException(); }

        public virtual void Save(BinaryWriter output) { throw new NotImplementedException(); }

        public virtual void SyncFromCPU() { throw new NotImplementedException(); }
        public virtual void SyncToCPU() { throw new NotImplementedException(); }

        #endregion

        #region Global Optimizer Toolkit.
        //public GlobalOptimizer GlobalOptimizer { get; set; }

        //public void InitGlobalOptimizer(StructureLearner learner)
        //{
        //    //1. Init Global Optimizer.
        //    GlobalOptimizer = GradientOptimizer.CreateGlobalOptimizer(ParamNumber, learner, new RunnerBehavior() { Device = learner.device });
        //    InitOptimizer(learner);
        //    //2. Obtain the model Parameter.
        //    GetModelParameter(GlobalOptimizer.Parameter, 0);
        //}
        //public void ResetGlobalOptimization()
        //{
        //    ClearModelGradient();
        //}

        
        //public void EndGlobalOptimization(double loss, object par, ObjectiveFunction func)
        //{
        //    //1. Obtain the model Gradient.
        //    GetModelGradient(GlobalOptimizer.Gradient, 0);

        //    //2. Update Global Optimization.
        //    GlobalOptimizer.UpdateGlobalOptimizer(loss, par, func);

        //    //3. Set Model Parameters.
        //    SetModelParameter(GlobalOptimizer.Parameter, 0);
        //}
        #endregion

        #region Structure Dispose Function.
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;

            if (disposing)
            {
                
                if (this.SubStructure != null && this.SubStructure.Count >0)
                {
                    foreach (var s in this.SubStructure)
                    {
                        s.Dispose();
                    }
                }

                if (this.StructureOptimizer != null)
                {
                    this.StructureOptimizer.Clear();
                }                
            }

            this.SubStructure.Clear();
            this.StructureOptimizer = null;
        }

        ~Structure()
        {
            this.Dispose(false);
        }
        #endregion
    }
}
