﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// AutoEncoder Link Model.
    /// </summary>
    public sealed class AutoEncoderLinkStructure : Structure, IDisposable
    {
        public int Neural_In;
        public int Neural_Out;

        public CudaPieceFloat weight;
        public CudaPieceFloat hidBias;
        public CudaPieceFloat inBias;

        public GradientOptimizer WeightOptimizer { get { return StructureOptimizer["Weight"].Optimizer; } }
        public GradientOptimizer HidBiasOptimizer { get { return StructureOptimizer["HidBias"].Optimizer; } }
        public GradientOptimizer InBiasOptimizer { get { return StructureOptimizer["InBias"].Optimizer; } }


        public IntPtr Weight { get { return weight.CudaPtr; } }
        public IntPtr HidBias { get { return hidBias.CudaPtr; } }
        public IntPtr InBias { get { return inBias.CudaPtr; } }

        public float[] Back_Weight { get { return weight.MemPtr; } }
        public float[] Back_HidBias { get { return hidBias.MemPtr; } }
        public float[] Back_InBias { get { return inBias.MemPtr; } }

        public A_Func Af = A_Func.Sigmoid;
        public N_Type Nt = N_Type.Fully_Connected;
        public int N_Winsize = 1;
        public P_Pooling pool_type = P_Pooling.MAX_Pooling;

        public bool IsHidBias = true;

        public float DropOut = 0;
        public bool IsDropOut { get { return DropOut > 0; } }
        CudaPieceInt DropoutMask = null;

        public AutoEncoderLinkStructure(int layer_in, int layer_out)
            : this(layer_in, layer_out, DeviceType.GPU)
        { }


        public AutoEncoderLinkStructure(int layer_in, int layer_out, DeviceType device)
        {
            Neural_In = layer_in;
            Neural_Out = layer_out;
            weight = new CudaPieceFloat(Neural_In * Neural_Out * N_Winsize, true, device == DeviceType.GPU);
            hidBias = new CudaPieceFloat(Neural_Out, true, device == DeviceType.GPU);
            inBias = new CudaPieceFloat(Neural_In, true, device == DeviceType.GPU);

            Init();
            if (IsDropOut) DropoutMask = new CudaPieceInt(Neural_Out, true, device == DeviceType.GPU);
        }

        protected override void InitStructureOptimizer()
        {
            StructureOptimizer.Add("Weight", new ModelOptimizer() { Parameter = weight });
            StructureOptimizer.Add("HidBias", new ModelOptimizer() { Parameter = hidBias });
            StructureOptimizer.Add("InBias", new ModelOptimizer() { Parameter = inBias });

        }

        private bool disposed = false;
        ~AutoEncoderLinkStructure()
        {
            this.Dispose(false);
        }

        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;
            if (disposing)
            {
                if (weight != null) weight.Dispose(); weight = null;
                if (hidBias != null) hidBias.Dispose(); hidBias = null;
                if (inBias != null) inBias.Dispose(); inBias = null;
                if (DropoutMask != null) DropoutMask.Dispose(); DropoutMask = null;
            }

            base.Dispose(disposing);
        }

        public void Init()
        {
            int inputsize = Neural_In * N_Winsize;
            int outputsize = Neural_Out;

            weight.Init((float)(Math.Sqrt(6.0 / (inputsize + outputsize)) * 2), (float)(-Math.Sqrt(6.0 / (inputsize + outputsize))));
            hidBias.Init(0);
            inBias.Init(0);
        }

        public void Init(float wei)
        {
            weight.Init(wei);
            hidBias.Init(0);
            inBias.Init(0);
        }

        public void Init(float wei_scale, float wei_bias)
        {
            weight.Init(wei_scale, wei_bias);
            hidBias.Init(wei_scale, wei_bias);
            inBias.Init(wei_scale, wei_bias);
        }

        public void Init(AutoEncoderLinkStructure refLink)
        {
            weight.Init(refLink.Back_Weight);
            hidBias.Init(refLink.Back_HidBias);
            inBias.Init(refLink.Back_InBias);
        }

        public void InitDropOut()
        {
            List<int> seqList = new List<int>();
            for (int i = 0; i < Neural_Out; i++)
            {
                seqList.Add(i);
                DropoutMask.MemPtr[i] = 1;
            }
            int expection = (int)(DropOut * Neural_Out);
            for (int h = 0; h < expection; h++)
            {
                int index = Util.URandom.Next(h, seqList.Count);
                int selectedID = seqList[index];
                seqList[index] = seqList[h];
                seqList[h] = selectedID;
                DropoutMask.MemPtr[selectedID] = 0;
            }

            if (DropoutMask.CudaPtr != IntPtr.Zero) DropoutMask.SyncFromCPU();
        }

    }
}
