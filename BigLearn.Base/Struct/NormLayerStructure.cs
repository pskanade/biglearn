﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BigLearn
{
    public class NormLayerStructure : Structure, IDisposable
    {
        public int NeuronNum;
        public CudaPieceFloat Beta;
        public CudaPieceFloat Gamma;

        public CudaPieceFloat Mu;
        public CudaPieceFloat Sigma;

        public GradientOptimizer BetaOptimizer { get { return StructureOptimizer["Beta"].Optimizer; } }
        public GradientOptimizer GammaOptimizer { get { return StructureOptimizer["Gamma"].Optimizer; } }

        public NormLayerStructure(int dim)
        {
            NeuronNum = dim;
            Beta = new CudaPieceFloat(NeuronNum, true, true);
            Gamma = new CudaPieceFloat(NeuronNum, true, true);
            Gamma.Init(1.0f);
            Mu = new CudaPieceFloat(NeuronNum, true, true);
            Sigma = new CudaPieceFloat(NeuronNum, true, true);
        }

        public NormLayerStructure(BinaryReader reader, DeviceType device) : base(reader, device)
        { }

        public override DataSourceID Type { get { return DataSourceID.NormLayerStructure; } }

        public void ReInit()
        {
            Mu.Zero();
            Sigma.Zero();
        }

        private bool disposed = false;
        ~NormLayerStructure()
        {
            this.Dispose(false);
        }

        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;
            if (disposing)
            {
                if (Beta != null) Beta.Dispose(); Beta = null;
                if (Gamma != null) Gamma.Dispose(); Gamma = null;
                if (Mu != null) Mu.Dispose(); Mu = null;
                if (Sigma != null) Sigma.Dispose(); Sigma = null;
            }

            base.Dispose(disposing);
        }

        protected override void InitStructureOptimizer()
        {
            StructureOptimizer.Add("Beta", new ModelOptimizer() { Parameter = Beta });
            StructureOptimizer.Add("Gamma", new ModelOptimizer() { Parameter = Gamma });
        }
    }
}
