﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class VectorData : BatchData
    {
        public CudaPieceFloat Output = null;
        public CudaPieceFloat Deriv = null;
        
        public override DataSourceID Type { get { return DataSourceID.VectorData; } }

        public int MaxLength { get; set; }
        public virtual int Length
        {
            get { return Output.EffectiveSize; }
            set
            {
                if (value > MaxLength) throw new Exception(string.Format("Length {0} should be smaller than Max Length {1}", value, MaxLength));
                Output.EffectiveSize = value ;
                if(Deriv != null && !Deriv.IsEmpty) Deriv.EffectiveSize = value;
            }
        }

        public VectorData(int maxLength,  DeviceType device) : 
            this(maxLength, 
                new CudaPieceFloat(maxLength, true, device == DeviceType.GPU), 
                new CudaPieceFloat(maxLength, true, device == DeviceType.GPU), 
                device) 
        { }

        public VectorData(int maxLength, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device)
        {
            Output = data;
            Deriv = deriv;
            MaxLength = maxLength;
            DeviceType = device;
        }

        public VectorData(HiddenBatchData data) : this(data.Dim * data.MAX_BATCHSIZE, data.Output.Data, data.Deriv.Data, data.DeviceType)
        { }

        public override void SyncToCPU()
        {
            Output.SyncToCPU();
            if(Deriv != null && !Deriv.IsEmpty) 
                Deriv.SyncToCPU();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
                if (Deriv != null) Deriv.Dispose(); Deriv = null;
            }
        }
    }
}
