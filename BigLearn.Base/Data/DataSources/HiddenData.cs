﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class HiddenBatchData : BatchData
    {
        public DenseBatchData Output = null;
        public DenseBatchData Deriv = null;
        public override DataSourceID Type { get { return DataSourceID.HiddenBatchData; } }
        public int BatchSize
        {
            get
            {
                if (Output != null) return Output.BatchSize;
                if (Deriv != null) return Deriv.BatchSize;
                return 0;
            }
            set
            {
                if (MAX_BATCHSIZE < value) { throw new Exception(string.Format("the batch size {0} is larger than MAX_BATCHSIZE {1}", value, MAX_BATCHSIZE)); }
                if (Output != null) Output.BatchSize = value;
                if (Deriv != null) Deriv.BatchSize = value;
                
            }
        }

        public int MAX_BATCHSIZE;
        public int Dim;

        public HiddenBatchData(int maxBatchSize, int dim, DNNRunMode mode, DeviceType device) : 
            this(maxBatchSize, dim, new CudaPieceFloat(maxBatchSize * dim, device), new CudaPieceFloat(maxBatchSize * dim, device), device) { }
        public HiddenBatchData(MatrixData data): this(data.MaxRow, data.Column, data.Output, data.Deriv, data.DeviceType) { }
        public HiddenBatchData(VectorData data) : this(data.MaxLength, 1, data.Output, data.Deriv, data.DeviceType) { }
        public HiddenBatchData(EmbedStructure data) : this(data.VocabSize, data.Dim, data.Embedding, data.EmbeddingGrad, data.DeviceType) { }
        public HiddenBatchData(int maxBatchSize, int dim, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device)
        {
            MAX_BATCHSIZE = maxBatchSize;
            Dim = dim;
            DeviceType = device;
            Output = new DenseBatchData(new DenseDataStat() { MAX_BATCHSIZE = maxBatchSize, Dim = dim }, data, device);
            if(deriv != null) Deriv = new DenseBatchData(new DenseDataStat() { MAX_BATCHSIZE = maxBatchSize, Dim = dim }, deriv, device);
        }

        public override void SyncToCPU()
        {
            Output.SyncToCPU();
            Deriv.SyncToCPU();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
                if (Deriv != null) Deriv.Dispose(); Deriv = null;
            }
        }
    }
}
