﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum FeatureDataType
    {
        SparseFeature = 0,
        DenseFeature = 1,
        NoIdeaFeature = 2,
    }

    public class GeneralBatchInputDataStat : DataStat
    {
        public int MAX_FEATUREDIM = 0;
        public int MAX_BATCHSIZE = 0;
        public int MAX_ELEMENTSIZE = 0;

        public FeatureDataType FeatureType = FeatureDataType.NoIdeaFeature;

        public override void Save(BinaryWriter writer)
        {
            if (FeatureType == FeatureDataType.NoIdeaFeature) { throw new Exception("Feature Type must be explicitly expressed as SparseFeature or DenseFeature"); }
            if (FeatureType == FeatureDataType.DenseFeature) { MAX_ELEMENTSIZE = MAX_BATCHSIZE * MAX_FEATUREDIM + 1; }

            writer.Write(MAX_FEATUREDIM);
            writer.Write(MAX_BATCHSIZE);
            writer.Write(MAX_ELEMENTSIZE);
            writer.Write(this.TotalBatchNumber);
            writer.Write(this.TotalSampleNumber);
        }

        public override void Init(BinaryReader reader)
        {
            reader.BaseStream.Seek(-5 * sizeof(Int32), SeekOrigin.End);

            MAX_FEATUREDIM = reader.ReadInt32();
            MAX_BATCHSIZE = reader.ReadInt32();
            MAX_ELEMENTSIZE = reader.ReadInt32();
            TotalBatchNumber = reader.ReadInt32();
            TotalSampleNumber = reader.ReadInt32();

            if (MAX_ELEMENTSIZE <= MAX_BATCHSIZE * MAX_FEATUREDIM) { FeatureType = FeatureDataType.SparseFeature; }
            else { FeatureType = FeatureDataType.DenseFeature; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            TextWriter tw = new StringWriter(sb);
            foreach (var kv in this.ToKeyValues())
            {
                tw.WriteLine("{0}\t{1}", kv.Key, kv.Value);
            }

            tw.Flush();
            return sb.ToString();
        }

        public override IEnumerable<KeyValuePair<string, string>> ToKeyValues()
        {
            // Keep order
            yield return new KeyValuePair<string, string>("FEATURE_DIM", this.MAX_FEATUREDIM.ToString());
            yield return new KeyValuePair<string, string>("MAX_BATCHSIZE", this.MAX_BATCHSIZE.ToString());
            yield return new KeyValuePair<string, string>("MAX_ELEMENTSIZE", this.MAX_ELEMENTSIZE.ToString());
            yield return new KeyValuePair<string, string>("TOTAL_BATCHNUM", this.TotalBatchNumber.ToString());
            yield return new KeyValuePair<string, string>("TOTAL_SAMPLENUM", this.TotalSampleNumber.ToString());
        }
    }
    

    public class GeneralBatchInputData : BatchData<GeneralBatchInputDataStat>
    {
        public override DataSourceID Type { get { return DataSourceID.GeneralBatchInputData; } }
        
        public override int BatchSize
        {
            get
            {
                switch (Stat.FeatureType)
                {
                    case FeatureDataType.SparseFeature: return BatchIdx.EffectiveSize;
                    case FeatureDataType.DenseFeature: return DenseFeatureData.EffectiveSize / Stat.MAX_FEATUREDIM;
                }
                throw new Exception("Feature Type must be explicitly expressed as SparseFeature or DenseFeature");
            }
            set
            {
                InstanceLabel.EffectiveSize = value;
                switch (Stat.FeatureType)
                {
                    case FeatureDataType.SparseFeature: BatchIdx.EffectiveSize = value; break;
                    case FeatureDataType.DenseFeature: DenseFeatureData.EffectiveSize = value * Stat.MAX_FEATUREDIM; break;
                    case FeatureDataType.NoIdeaFeature: throw new Exception("Feature Type must be explicitly expressed as SparseFeature or DenseFeature");
                }
                if (Stat.MAX_BATCHSIZE < value) Stat.MAX_BATCHSIZE = value;
            }
        }

        public int ElementSize
        {
            get
            {
                switch (Stat.FeatureType)
                {
                    case FeatureDataType.SparseFeature: return FeatureIdx.EffectiveSize;
                }
                return 0;
            }
            set
            {
                switch (Stat.FeatureType)
                {
                    case FeatureDataType.SparseFeature: FeatureIdx.EffectiveSize = value; FeatureValue.EffectiveSize = value; break;
                }
                if (Stat.MAX_ELEMENTSIZE < value) Stat.MAX_ELEMENTSIZE = value;
            }
        }

        /// <summary>
        /// sparse feature representation.
        /// </summary>
        public CudaPieceInt BatchIdx;
        public CudaPieceInt FeatureIdx;
        public CudaPieceFloat FeatureValue;
        
        /// <summary>
        /// Dense Feature Representation.
        /// </summary>
        public CudaPieceFloat DenseFeatureData;

        public CudaPieceFloat InstanceLabel;

        public GeneralBatchInputData() { }

        public GeneralBatchInputData(GeneralBatchInputDataStat stat, DeviceType device) : base(stat, device) { }

        /// <summary>
        /// if featureType = SparseFeature, feaDim allows be set to zero (it will automatically infer the feature dimension);
        /// if featureType = DenseFeature, feaDim should not be zero;
        /// </summary>
        /// <param name="feaDim"></param>
        /// <param name="maxBatchSize"></param>
        /// <param name="device"></param>
        public GeneralBatchInputData(int feaDim, int maxBatchSize, FeatureDataType featureType, DeviceType device) :
            this(new GeneralBatchInputDataStat() { MAX_FEATUREDIM = feaDim, MAX_BATCHSIZE = maxBatchSize, FeatureType = featureType }, device) { }

        public override void Init(GeneralBatchInputDataStat stat, DeviceType deviceType)
        {
            base.Init(stat, deviceType);
            switch (Stat.FeatureType)
            {
                case FeatureDataType.SparseFeature: // Sparse Feature Representation.
                    BatchIdx = new CudaPieceInt(Stat.MAX_BATCHSIZE, true, deviceType == DeviceType.GPU);
                    FeatureIdx = new CudaPieceInt(Stat.MAX_ELEMENTSIZE, true, deviceType == DeviceType.GPU);
                    FeatureValue = new CudaPieceFloat(Stat.MAX_ELEMENTSIZE, true, deviceType == DeviceType.GPU);
                    break;
                case FeatureDataType.DenseFeature: // Dense Feature Representation.
                    DenseFeatureData = new CudaPieceFloat(Stat.MAX_BATCHSIZE * Stat.MAX_FEATUREDIM, true, deviceType == DeviceType.GPU);
                    break;
            }
            InstanceLabel = new CudaPieceFloat(Stat.MAX_BATCHSIZE, true, deviceType == DeviceType.GPU);
        }
        
        public override void Load(BinaryReader reader)
        {
            BatchSize = reader.ReadInt32();
            switch (Stat.FeatureType)
            {
                case FeatureDataType.SparseFeature:
                    ElementSize = reader.ReadInt32();
                    Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * BatchSize), 0, BatchIdx.MemPtr, 0, sizeof(Int32) * BatchSize);
                    Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * ElementSize), 0, FeatureIdx.MemPtr, 0, sizeof(Int32) * ElementSize);
                    Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * ElementSize), 0, FeatureValue.MemPtr, 0, sizeof(float) * ElementSize);

                    break;
                case FeatureDataType.DenseFeature:
                    Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * BatchSize * Stat.MAX_FEATUREDIM), 0, DenseFeatureData.MemPtr, 0, sizeof(float) * BatchSize * Stat.MAX_FEATUREDIM);
                    break;
            }
            Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * BatchSize), 0, InstanceLabel.MemPtr, 0, sizeof(float) * BatchSize);
            SyncFromCPU();
        }

        public override void CopyFrom(IData other)
        {
            GeneralBatchInputData block = (GeneralBatchInputData)other;
            BatchSize = block.BatchSize;
            switch (Stat.FeatureType)
            {
                case FeatureDataType.SparseFeature:
                    ElementSize = block.ElementSize;
                    if (BatchIdx != null) BatchIdx.CopyFrom(block.BatchIdx);
                    if (FeatureIdx != null) FeatureIdx.CopyFrom(block.FeatureIdx);
                    if (FeatureValue != null) FeatureValue.CopyFrom(block.FeatureValue);
                    break;
                case FeatureDataType.DenseFeature:
                    if (DenseFeatureData != null) DenseFeatureData.CopyFrom(block.DenseFeatureData);
                    break;
            }
            if (InstanceLabel != null) InstanceLabel.CopyFrom(block.InstanceLabel);
        }


        /// <summary>
        /// Do not change the function Name.
        /// </summary>
        /// <param name="reader"></param>
        public override void SkipBlock(BinaryReader reader, DataStat stat)
        {
            int b = reader.ReadInt32();
            GeneralBatchInputDataStat MatchStat = ((GeneralBatchInputDataStat)stat);
            switch (MatchStat.FeatureType)
            {
                case FeatureDataType.SparseFeature:
                    int e = reader.ReadInt32();
                    reader.BaseStream.Seek((b + e) * sizeof(int) + e * sizeof(float), SeekOrigin.Current);
                    break;
                case FeatureDataType.DenseFeature:
                    reader.BaseStream.Seek((b * MatchStat.MAX_FEATUREDIM) * sizeof(float), SeekOrigin.Current);
                    break;
            }
            reader.BaseStream.Seek(b * sizeof(float), SeekOrigin.Current);
        }

        public override void Save(BinaryWriter writer)
        {
            if (BatchSize > 0)
            {
                SyncToCPU();

                writer.Write(BatchSize);
                switch (Stat.FeatureType)
                {
                    case FeatureDataType.SparseFeature:
                        writer.Write(ElementSize);
                        for (int i = 0; i < BatchSize; i++) writer.Write(BatchIdx.MemPtr[i]);
                        for (int i = 0; i < ElementSize; i++) writer.Write(FeatureIdx.MemPtr[i]);
                        for (int i = 0; i < ElementSize; i++) writer.Write(FeatureValue.MemPtr[i]);
                        break;
                    case FeatureDataType.DenseFeature:
                        for (int i = 0; i < BatchSize * Stat.MAX_FEATUREDIM; i++) writer.Write(DenseFeatureData.MemPtr[i]);
                        break;
                }
                for (int i = 0; i < BatchSize; i++) writer.Write(InstanceLabel.MemPtr[i]);
            }
        }

        public override void Clear()
        {
            BatchSize = 0;
            ElementSize = 0;
        }


        public void PushSample(Dictionary<int, float> fea, float label = 0)
        {
            switch (Stat.FeatureType)
            {
                case FeatureDataType.SparseFeature:
                    this.PushSparseSample(fea.Keys.ToArray(), fea.Values.ToArray(), fea.Count, label);
                    break;
                case FeatureDataType.DenseFeature:
                    this.PushDenseSample(fea.Keys.ToArray(), fea.Values.ToArray(), fea.Count, label);
                    break;
            }
        }

        public void PushSparseSample(int[] fIds, float[] values, int size, float label = 0)
        {
            int oldBatchSize = BatchSize;
            int oldElementSize = ElementSize;
             
            BatchSize = oldBatchSize + 1;
            ElementSize = oldElementSize + size;

            BatchIdx.SyncFromCPU(oldBatchSize, new int[] { ElementSize }, 0, 1);
            InstanceLabel.SyncFromCPU(oldBatchSize, new float[] { label }, 0, 1);

            FeatureIdx.SyncFromCPU(oldElementSize, fIds, 0, size);
            FeatureValue.SyncFromCPU(oldElementSize, values, 0, size);

            int maxFid = fIds.Max();
            if (maxFid >= Stat.MAX_FEATUREDIM) { Stat.MAX_FEATUREDIM = maxFid + 1; }
        }

        public void PushDenseSample(int[] fIds, float[] values, int size, float label = 0)
        {
            float[] newValues = new float[Stat.MAX_FEATUREDIM];
            for (int i = 0; i < size; i++) { newValues[fIds[i]] = values[i]; }
            PushDenseSample(newValues, Stat.MAX_FEATUREDIM, label);
        }

        public void PushDenseSample(float[] values, int size, float label = 0)
        {
            int oldBatchSize = BatchSize;
            BatchSize = oldBatchSize + 1;

            InstanceLabel.SyncFromCPU(oldBatchSize, new float[] { label }, 0, 1);
            DenseFeatureData.SyncFromCPU(oldBatchSize * Stat.MAX_FEATUREDIM, values, 0, size);
        }

        public override void SyncToCPU()
        {
            switch (Stat.FeatureType)
            {
                case FeatureDataType.SparseFeature:
                    BatchIdx.SyncToCPU(BatchSize);
                    FeatureIdx.SyncToCPU(ElementSize);
                    FeatureValue.SyncToCPU(ElementSize);
                    break;
                case FeatureDataType.DenseFeature:
                    DenseFeatureData.SyncToCPU(BatchSize * Stat.MAX_FEATUREDIM);
                    break;
            }
            InstanceLabel.SyncToCPU(BatchSize);
        }

        public override void SyncFromCPU()
        {
            switch (Stat.FeatureType)
            {
                case FeatureDataType.SparseFeature:
                    BatchIdx.SyncFromCPU(BatchSize);
                    FeatureIdx.SyncFromCPU(ElementSize);
                    FeatureValue.SyncFromCPU(ElementSize);
                    break;
                case FeatureDataType.DenseFeature:
                    DenseFeatureData.SyncFromCPU(BatchSize * Stat.MAX_FEATUREDIM);
                    break;
            }
            InstanceLabel.SyncFromCPU(BatchSize);
        }
        
        public void Concate(GeneralBatchInputData data, CudaPieceFloat output, int dim)
        {
            throw new NotImplementedException();
            //if (Stat.FeatureType == FeatureDataType.SparseFeature && data.Stat.FeatureType == FeatureDataType.SparseFeature)
            //{
            //    if (BatchIdx.Size < data.Stat.MAX_BATCHSIZE)
            //    {
            //        BatchIdx.Resize(data.Stat.MAX_BATCHSIZE);
            //        MatchSrcIdx.Resize(data.Stat.MAX_BATCHSIZE);
            //        MatchTgtIdx.Resize(data.Stat.MAX_BATCHSIZE);
            //        MatchLabelValue.Resize(data.Stat.MAX_BATCHSIZE);
            //    }
            //    if (FeatureIdx.Size < data.Stat.MAX_ELEMENTSIZE + data.Stat.MAX_BATCHSIZE * dim)
            //    {
            //        FeatureIdx.Resize(data.Stat.MAX_ELEMENTSIZE + data.Stat.MAX_BATCHSIZE * dim);
            //        FeatureValue.Resize(data.Stat.MAX_ELEMENTSIZE + data.Stat.MAX_BATCHSIZE * dim);
            //    }

            //    Array.Copy(data.MatchSrcIdx.MemPtr, MatchSrcIdx.MemPtr, data.BatchSize);
            //    Array.Copy(data.MatchTgtIdx.MemPtr, MatchTgtIdx.MemPtr, data.BatchSize);
            //    Array.Copy(data.MatchLabelValue.MemPtr, MatchLabelValue.MemPtr, data.BatchSize);

            //    int sourceIdx = 0;
            //    int targetIdx = 0;
            //    for (int i = 0; i < data.BatchSize; i++)
            //    {
            //        BatchIdx.MemPtr[i] = data.BatchIdx.MemPtr[i] + (i + 1) * dim;
            //        int feaLen = i == 0 ? data.BatchIdx.MemPtr[i] : data.BatchIdx.MemPtr[i] - data.BatchIdx.MemPtr[i - 1];
            //        Array.Copy(data.FeatureIdx.MemPtr, sourceIdx, FeatureIdx.MemPtr, targetIdx, feaLen);
            //        Array.Copy(data.FeatureValue.MemPtr, sourceIdx, FeatureValue.MemPtr, targetIdx, feaLen);
            //        sourceIdx += feaLen;
            //        targetIdx += feaLen;

            //        Array.Copy(Enumerable.Range(0, dim).Select(a => a + data.Stat.MAX_FEATUREDIM).ToArray(), 0,
            //                    FeatureIdx.MemPtr, targetIdx, dim);
            //        Array.Copy(output.MemPtr, i * dim, FeatureValue.MemPtr, targetIdx, dim);
            //        targetIdx += dim;
            //    }
            //    Stat.MAX_FEATUREDIM = Math.Max(Stat.MAX_FEATUREDIM, dim + data.Stat.MAX_FEATUREDIM);
            //    BatchSize = data.BatchSize;
            //    ElementSize = data.ElementSize + dim * BatchSize;
            //}
        }


        #region Dispose Function is not necessary.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //if (MatchSrcIdx != null) MatchSrcIdx.Dispose(); MatchSrcIdx = null;
                //if (MatchTgtIdx != null) MatchTgtIdx.Dispose(); MatchTgtIdx = null;
                //if (MatchLabelValue != null) MatchLabelValue.Dispose(); MatchLabelValue = null;
                if (BatchIdx != null) BatchIdx.Dispose(); BatchIdx = null;
                if (FeatureIdx != null) FeatureIdx.Dispose(); FeatureIdx = null;
                if (FeatureValue != null) FeatureValue.Dispose(); FeatureValue = null;
                if (DenseFeatureData != null) DenseFeatureData.Dispose(); DenseFeatureData = null;
                //if (Src2MatchIdx != null) Src2MatchIdx.Dispose(); Src2MatchIdx = null;
                //if (Src2MatchElement != null) Src2MatchElement.Dispose(); Src2MatchElement = null;
                //if (Tgt2MatchIdx != null) Tgt2MatchIdx.Dispose(); Tgt2MatchIdx = null;
                //if (Tgt2MatchElement != null) Tgt2MatchElement.Dispose(); Tgt2MatchElement = null;
            }
        }
        #endregion.

    }
}
