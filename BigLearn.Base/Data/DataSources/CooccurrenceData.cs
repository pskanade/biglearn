﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using System.Collections.Concurrent;

namespace BigLearn
{
    public enum ModelDataType
    {
        ShotOccurrence = 0,
        BowOccurrence = 1,
    }

    public class CooccurrenceUtil
    {
        public static Dictionary<int, HashSet<int>> LoadCooccurrenceData(string fileName, NameIndex Item)
        {
            Dictionary<int, HashSet<int>> data = new Dictionary<int, HashSet<int>>();
            using (StreamReader mreader = new StreamReader(fileName))
            {
                while (!mreader.EndOfStream)
                {
                    string[] items = mreader.ReadLine().Split('\t');
                    string[] list = items[1].Trim().Split(',');
                    HashSet<int> set = Item.Index(list, true);
                    data.Add(int.Parse(items[0]), set);
                }
            }
            return data;
        }

        public static Dictionary<int, HashSet<string>> LoadCooccurrenceData(string fileName)
        {
            Dictionary<int, HashSet<string>> data = new Dictionary<int, HashSet<string>>();
            using (StreamReader mreader = new StreamReader(fileName))
            {
                while (!mreader.EndOfStream)
                {
                    string[] items = mreader.ReadLine().Split('\t');
                    string[] list = items[1].Trim().Split(',');
                    HashSet<string> set = new HashSet<string>(list);
                    data.Add(int.Parse(items[0]), set);
                }
            }
            return data;
        }

        public static void SaveMissCooccurrenceData(string fileName, Dictionary<int, Tuple<HashSet<string>, HashSet<string>>> data)
        {
            using (StreamWriter mwriter = new StreamWriter(fileName))
            {
                foreach (KeyValuePair<int, Tuple<HashSet<string>, HashSet<string>>> item in data)
                {
                    mwriter.WriteLine("{0}\t{1}\t{2}", item.Key, string.Join(",", item.Value.Item1), string.Join(",", item.Value.Item2));
                }
            }
        }
    }

    /// <summary>
    /// Assign Index to any Given String.
    /// </summary>
    public class NameIndex
    {
        public Dictionary<string, int> NameDict = new Dictionary<string, int>();
        public List<string> NameList = new List<string>();
        public int NameCount { get { return NameList.Count; } }

        public void Clear()
        {
            NameDict.Clear();
            NameList.Clear();
        }

        public HashSet<int> Index(IEnumerable<string> name, bool isCreate)
        {
            HashSet<int> idSet = new HashSet<int>();
            foreach (string n in name)
            {
                int id = Index(n, isCreate);
                if (id >= 0) idSet.Add(id);
            }
            return idSet;
        }

        public int Index(string item, bool IsCreate)
        {
            if (NameDict.ContainsKey(item))
                return NameDict[item];
            else if (IsCreate)
            {
                NameDict.Add(item, NameList.Count);
                NameList.Add(item);
                return NameDict[item];
            }
            else
                return -1;
        }

        public int Index(string item, int index)
        {
            NameDict[item] = index;
            while (NameList.Count <= index) { NameList.Add(string.Empty); }
            NameList[index] = item;
            return index;
        }

        public string GetName(int index)
        {
            if (NameList.Count > index) return NameList[index];
            else return string.Empty;
        }
    }

    public abstract class CooccurrenceData
    {
        public abstract ModelDataType DataType { get; }
        public virtual int FeaNum { get; protected set; }

        public abstract int TrainNum { get; }
        public abstract int TestNum { get; }

        public virtual int TrainCooccurrenceNum(int idx) { return 0; }
        public virtual int TestCooccurrenceNum(int idx) { return 0; }

        public virtual IEnumerable<KeyValuePair<int, float>> TrainCase(int idx) { return null; }

        public virtual IEnumerable<KeyValuePair<int, float>> TestCase(int idx) { return null; }

        public virtual void LoadTrainData(string trainData)
        { }

        public virtual void LoadTestData(string testData)
        { }

        public OccurrenceGraph Graph;

        public void InitGraph()
        {
            Graph = new OccurrenceGraph(this);
        }
    }

    public class BowOccurrence : CooccurrenceData
    {
        public override ModelDataType DataType { get { return ModelDataType.BowOccurrence; } }
        public List<Dictionary<int, float>> TrainData = new List<Dictionary<int, float>>();
        public List<Dictionary<int, float>> TestData = new List<Dictionary<int, float>>();

        public override int TrainNum { get { return TrainData.Count; } }
        public override int TestNum { get { return TestData.Count; } }
        public override int TrainCooccurrenceNum(int idx) { return TrainData[idx].Count; }
        public override int TestCooccurrenceNum(int idx) { return TestData[idx].Count; }

        public override IEnumerable<KeyValuePair<int, float>> TrainCase(int idx) { return TrainData[idx]; }

        public override IEnumerable<KeyValuePair<int, float>> TestCase(int idx) { return TestData[idx]; }

        public override void LoadTrainData(string trainData)
        {
            using (StreamReader mreader = new StreamReader(trainData))
            {
                while (!mreader.EndOfStream)
                {
                    string record = mreader.ReadLine();
                    Dictionary<int, float> fea = Util.Str2Dict(record, '\t');
                    FeaNum = Math.Max(FeaNum, fea.Keys.Max() + 1);
                    TrainData.Add(fea);
                }
            }
        }
        public override void LoadTestData(string testData)
        {
            FeaNum = 0;
            using (StreamReader mreader = new StreamReader(testData))
            {
                while (!mreader.EndOfStream)
                {
                    string record = mreader.ReadLine();
                    Dictionary<int, float> fea = Util.Str2Dict(record, '\t');
                    FeaNum = Math.Max(FeaNum, fea.Keys.Max() + 1);
                    TestData.Add(fea);
                }
            }
        }
    }

    public class ShotOccurrence : CooccurrenceData
    {
        public override ModelDataType DataType { get { return ModelDataType.ShotOccurrence; } }

        public List<HashSet<int>> TrainData = new List<HashSet<int>>();
        public List<Tuple<HashSet<int>, HashSet<int>>> TestData = new List<Tuple<HashSet<int>, HashSet<int>>>();

        public NameIndex Item = new NameIndex();
        public List<string> GenerateNameList(string id2names)
        {
            if (id2names.Equals(string.Empty))
            {
                return Item.NameList;
            }

            Dictionary<string, string> dict = new Dictionary<string, string>();
            StreamReader mreader = new StreamReader(id2names);
            while (!mreader.EndOfStream)
            {
                string[] items = mreader.ReadLine().Split('\t');
                string id = items[0];
                string name = items[1];
                name = name.Replace(',', ' ');
                name = name.Replace(' ', '_');
                dict.Add(id, name);
            }
            mreader.Close();


            List<string> nameList = new List<string>();
            foreach (string id in Item.NameList)
            {
                if (dict.ContainsKey(id))
                {
                    nameList.Add(dict[id]);
                }
                else
                {
                    Console.WriteLine("No Name for .." + id.ToString());
                    nameList.Add("");
                }
            }
            return nameList;
        }


        public override int TrainNum { get { return TrainData.Count; } }
        public override int TestNum { get { return TestData.Count; } }

        public override int TrainCooccurrenceNum(int idx) { return TrainData[idx].Count; }
        public override int TestCooccurrenceNum(int idx) { return TestData[idx].Item1.Count; }

        public override IEnumerable<KeyValuePair<int, float>> TrainCase(int idx) { return TrainData[idx].ToDictionary(i => i, i => 1.0f); }

        public override IEnumerable<KeyValuePair<int, float>> TestCase(int idx) { return TestData[idx].Item1.ToDictionary(i => i, i => 1.0f); }

        public override void LoadTrainData(string trainData)
        {
            TrainData = CooccurrenceUtil.LoadCooccurrenceData(trainData, Item).Select(i => i.Value).ToList();
            FeaNum = Item.NameCount;
        }

        public override void LoadTestData(string testData)
        {
            StreamReader mreader = new StreamReader(testData);
            while (!mreader.EndOfStream)
            {
                string[] items = mreader.ReadLine().Split('\t');
                string[] trainlist = items[1].Trim().Split(',');
                string[] testlist = items[2].Trim().Split(',');

                HashSet<int> trainset = Item.Index(trainlist, false);
                HashSet<int> testset = Item.Index(testlist, false);

                if (trainset.Count > 0 && testset.Count > 0)
                    TestData.Add(new Tuple<HashSet<int>, HashSet<int>>(trainset, testset));
            }
            mreader.Close();
        }


    }

    public class OccurrenceGraph
    {
        Dictionary<int, Dictionary<int, float>> CooccurrenceGraph = new Dictionary<int, Dictionary<int, float>>();
        Dictionary<int, float> NodeFreq = new Dictionary<int, float>();

        int[] EdgeSource;
        int[] EdgeTarget;
        float[] EdgeWeight;
        public int EdgeNumber { get; set; }
        int[] Alias;
        double[] Prob;
        int[] NegTable = new int[NEG_TABLE_SIZE];
        const int NEG_TABLE_SIZE = 1000000;
        const float NEG_SAMPLING_POWER = 0.01F;
        void Push(int source, int target, float wei)
        {
            if (!CooccurrenceGraph.ContainsKey(source))
            {
                CooccurrenceGraph.Add(source, new Dictionary<int, float>());
            }
            if (!CooccurrenceGraph[source].ContainsKey(target))
            {
                CooccurrenceGraph[source].Add(target, 0);
            }
            CooccurrenceGraph[source][target] += wei;
            if (!NodeFreq.ContainsKey(source))
            {
                NodeFreq.Add(source, 0);
            }
            NodeFreq[source] += wei;
        }

        public void InitNegTable()
        {
            double sum = 0, cur_sum = 0, por = 0;
            int vid = 0;
            var keys = NodeFreq.Keys;

            var nodeIds = NodeFreq.Keys;
            sum = NodeFreq.Select(i => Math.Pow(i.Value, NEG_SAMPLING_POWER)).Sum();

            for (int k = 0; k != NEG_TABLE_SIZE; k++)
            {
                if ((double)(k + 1) / NEG_TABLE_SIZE > por)
                {
                    cur_sum += Math.Pow(NodeFreq[keys.ElementAt(vid)], NEG_SAMPLING_POWER);
                    por = cur_sum / sum;
                    vid++;
                }
                NegTable[k] = keys.ElementAt(vid - 1);
            }
        }

        public void InitAliasTable()
        {
            Alias = new int[EdgeNumber];
            Prob = new double[EdgeNumber];
            double[] norm_prob = new double[EdgeNumber];
            int[] large_block = new int[EdgeNumber];
            int[] small_block = new int[EdgeNumber];

            double sum = 0;
            int cur_small_block, cur_large_block;
            int num_small_block = 0, num_large_block = 0;

            for (int k = 0; k != EdgeNumber; k++) sum += EdgeWeight[k];
            for (int k = 0; k != EdgeNumber; k++) norm_prob[k] = EdgeWeight[k] * EdgeNumber / sum;

            for (int k = EdgeNumber - 1; k >= 0; k--)
            {
                if (norm_prob[k] < 1) small_block[num_small_block++] = k;
                else large_block[num_large_block++] = k;
            }

            while (num_small_block > 0 && num_large_block > 0)
            {
                cur_small_block = small_block[--num_small_block];
                cur_large_block = large_block[--num_large_block];
                Prob[cur_small_block] = norm_prob[cur_small_block];
                Alias[cur_small_block] = cur_large_block;
                norm_prob[cur_large_block] = norm_prob[cur_large_block] + norm_prob[cur_small_block] - 1;
                if (norm_prob[cur_large_block] < 1) small_block[num_small_block++] = cur_large_block;
                else large_block[num_large_block++] = cur_large_block;
            }

            while (num_large_block > 0) Prob[large_block[--num_large_block]] = 1;
            while (num_small_block > 0) Prob[small_block[--num_small_block]] = 1;
        }

        public void SaveGraph(string fileName)
        {
            using (StreamWriter writer = new StreamWriter(fileName))
            {
                for (int i = 0; i < EdgeNumber; i++)
                {
                    writer.WriteLine("{0} {1} {2}", EdgeSource[i], EdgeTarget[i], EdgeWeight[i]);
                }
            }
        }

        /// <summary>
        /// Build the OccurrenceGraph from ShotOccurrence Data.
        /// </summary>
        /// <param name="data"></param>
        public OccurrenceGraph(CooccurrenceData data)
        {
            for (int idx = 0; idx < data.TrainNum; idx++)
            {
                foreach (KeyValuePair<int, float> item_i in data.TrainCase(idx))
                {
                    foreach (KeyValuePair<int, float> item_j in data.TrainCase(idx))
                    {
                        if (item_i.Key == item_j.Key) continue;
                        Push(item_i.Key, item_j.Key, item_i.Value);
                    }
                }
            }

            EdgeNumber = CooccurrenceGraph.Select(i => i.Value.Count).Sum();
            EdgeSource = new int[EdgeNumber];
            EdgeTarget = new int[EdgeNumber];
            EdgeWeight = new float[EdgeNumber];

            int id = 0;
            foreach (KeyValuePair<int, Dictionary<int, float>> item_i in CooccurrenceGraph)
            {
                foreach (KeyValuePair<int, float> item_j in item_i.Value)
                {
                    EdgeSource[id] = item_i.Key;
                    EdgeTarget[id] = item_j.Key;
                    EdgeWeight[id] = item_j.Value;
                    id++;
                }
            }
            InitAliasTable();
            InitNegTable();
        }

        public Tuple<int, int> SampleEdge()
        {
            int k = (int)(EdgeNumber * Util.URandom.NextDouble());
            int pos = Util.URandom.NextDouble() < Prob[k] ? k : Alias[k];
            return new Tuple<int, int>(EdgeSource[pos], EdgeTarget[pos]);
        }

        public int SampleNode()
        {
            return NegTable[Util.URandom.Next(NEG_TABLE_SIZE)];
        }
    }

    /// <summary>
    /// HeldOneOut : iteratively select one people from receipents list for prediction.
    /// RandomHeldOneOut : randomly select one people from receipents list for prediction.
    /// HeldRandomOut : randomly select a set of recipients for prediction.
    /// RandomHeldOneSoftMax : randomly select one positive and several negative samples from receipents.
    /// NormalizeHeldOneout : random miss the number of items from all list.
    /// SkipGramHeldOut : random select one positive item and other negative items from all list. 
    /// </summary>
    public enum TrainSignalMode
    {
        HeldOneOut = 0,
        RandomHeldOneOut = 1,
        HeldRandomOut = 2,
        RandomHeldOneSoftMax = 3,
        NormalizeHeldOneOut = 4,
        SkipGramHeldOut = 5,

        /// <summary>
        ///  TO DO List.
        /// </summary>
        //NoiseReconstruction = 6,
        //RankNoiseReconstruction = 7
    }

    /// <summary>
    /// Hidden Output.
    /// </summary>
    public class CooccurrenceBatchInput : BatchData
    {
        public CudaPieceInt InputSampleIndex = null;
        public CudaPieceInt InputItemIndex = null;

        public CudaPieceInt OutputSampleIndex = null;
        public CudaPieceInt OutputItemIndex = null;
        public CudaPieceFloat OutputLabel = null;

        public CudaPieceFloat OutputScore = null;
        public CudaPieceInt OutputMapSample = null;

        public int MaxBatchSize { get; set; }
        public int MaxInputItemNum { get; set; }
        public int MaxOutputItemNum { get; set; }

        public int InputItemNum { get; set; }
        public int OutputItemNum { get; set; }
        public int ItemNum { get { return Data.FeaNum; } }
        public int BatchSize { get; set; }
        public TrainSignalMode TrainMode { get; set; }
        int TrainBatchNum { get; set; }
        int TestBatchNum { get; set; }

        int BatchIdx { get; set; }
        List<int> RandomBatchIdx = new List<int>();
        
        int MiniBatchSize { get; set; }
        int NegativeNum { get; set; }
        float HeldOutRate { get; set; }
        ShotOccurrence Data { get; set; }

        public IEnumerable<Tuple<HashSet<int>, HashSet<int>>> MiniBatchData { get; private set; }

        public CooccurrenceBatchInput() { }

        public CooccurrenceBatchInput(ShotOccurrence data, TrainSignalMode mode, int miniBatchSize, int negativeNum, float heldOutRate)
        {
            TrainMode = mode;
            MiniBatchSize = miniBatchSize;
            NegativeNum = negativeNum;
            HeldOutRate = heldOutRate;
            switch (TrainMode)
            {
                case TrainSignalMode.RandomHeldOneSoftMax:
                    TrainBatchNum = (data.TrainData.Count - 1) / MiniBatchSize + 1;
                    for (int i = 0; i < TrainBatchNum; i++)
                    {
                        var miniBatch = data.TrainData.Skip(i * MiniBatchSize).Take(MiniBatchSize);

                        int input = (from p in miniBatch select 2 * p.Count - 1).Sum();
                        int sample = (from p in miniBatch select 1).Sum();
                        int output = (from p in miniBatch select 1 + NegativeNum).Sum();

                        MaxInputItemNum = Math.Max(input, MaxInputItemNum);
                        MaxOutputItemNum = Math.Max(output, MaxOutputItemNum);
                        MaxBatchSize = Math.Max(sample, MaxBatchSize);
                    }
                    break;
                case TrainSignalMode.SkipGramHeldOut:
                    data.InitGraph();
                    //data.Graph.SaveGraph(@"graph.txt");
                    TrainBatchNum = (data.Graph.EdgeNumber - 1) / MiniBatchSize + 1;
                    MaxInputItemNum = MiniBatchSize;
                    MaxOutputItemNum = MiniBatchSize * (NegativeNum + 1);
                    MaxBatchSize = MiniBatchSize;
                    break;
                case TrainSignalMode.HeldOneOut:
                    TrainBatchNum = (data.TrainData.Count - 1) / MiniBatchSize + 1;
                    for (int i = 0; i < TrainBatchNum; i++)
                    {
                        var miniBatch = data.TrainData.Skip(i * MiniBatchSize).Take(MiniBatchSize);
                        int input = (from p in miniBatch select p.Count * p.Count).Sum();
                        int sample = (from p in miniBatch select p.Count + 1).Sum();
                        int output = (from p in miniBatch select p.Count + NegativeNum).Sum();

                        MaxInputItemNum = Math.Max(input, MaxInputItemNum);
                        MaxOutputItemNum = Math.Max(output, MaxOutputItemNum);
                        MaxBatchSize = Math.Max(sample, MaxBatchSize);
                    }
                    break;
                case TrainSignalMode.NormalizeHeldOneOut:
                    TrainBatchNum = (data.TrainData.Count - 1) / MiniBatchSize + 1;
                    for (int i = 0; i < TrainBatchNum; i++)
                    {
                        var miniBatch = data.TrainData.Skip(i * MiniBatchSize).Take(MiniBatchSize);
                        int sample = (from p in miniBatch select p.Count + 1).Sum();
                        int input = (from p in miniBatch select p.Count * p.Count).Sum();
                        int output = (from p in miniBatch select p.Count + NegativeNum).Sum();

                        MaxInputItemNum = Math.Max(input, MaxInputItemNum);
                        MaxOutputItemNum = Math.Max(output, MaxOutputItemNum);
                        MaxBatchSize = Math.Max(sample, MaxBatchSize);
                    }
                    break;
                case TrainSignalMode.RandomHeldOneOut:
                    TrainBatchNum = (data.TrainData.Count - 1) / MiniBatchSize + 1;
                    for (int i = 0; i < TrainBatchNum; i++)
                    {
                        var miniBatch = data.TrainData.Skip(i * MiniBatchSize).Take(MiniBatchSize);

                        int input = (from p in miniBatch select 2 * p.Count - 1).Sum();
                        int sample = (from p in miniBatch select 2).Sum();
                        int output = (from p in miniBatch select 1 + NegativeNum).Sum();

                        MaxInputItemNum = Math.Max(input, MaxInputItemNum);
                        MaxOutputItemNum = Math.Max(output, MaxOutputItemNum);
                        MaxBatchSize = Math.Max(sample, MaxBatchSize);
                    }
                    break;
                case TrainSignalMode.HeldRandomOut:
                    TrainBatchNum = (data.TrainData.Count - 1) / MiniBatchSize + 1;
                    for (int i = 0; i < TrainBatchNum; i++)
                    {
                        var miniBatch = data.TrainData.Skip(i * MiniBatchSize).Take(MiniBatchSize);

                        int input = (from p in miniBatch select p.Count).Sum();
                        int sample = (from p in miniBatch select 1).Sum();
                        int output = (from p in miniBatch select p.Count + NegativeNum).Sum();

                        MaxInputItemNum = Math.Max(input, MaxInputItemNum);
                        MaxOutputItemNum = Math.Max(output, MaxOutputItemNum);
                        MaxBatchSize = Math.Max(sample, MaxBatchSize);
                    }
                    break;
            }

            TestBatchNum = (data.TestData.Count - 1) / MiniBatchSize + 1;
            for (int i = 0; i < TestBatchNum; i++)
            {
                var miniBatch = data.TestData.Skip(i * MiniBatchSize).Take(MiniBatchSize);
                int input = (from p in miniBatch select p.Item1.Count).Sum();
                int sample = (from p in miniBatch select 1).Sum();
                int output = (from p in miniBatch select data.FeaNum).Sum();
                MaxInputItemNum = Math.Max(input, MaxInputItemNum);
                MaxOutputItemNum = Math.Max(output, MaxOutputItemNum);
                MaxBatchSize = Math.Max(sample, MaxBatchSize);
            }

            InputSampleIndex = new CudaPieceInt(MaxBatchSize, true, true);
            OutputSampleIndex = new CudaPieceInt(MaxBatchSize, true, true);

            InputItemIndex = new CudaPieceInt(MaxInputItemNum, true, true);
            OutputItemIndex = new CudaPieceInt(MaxOutputItemNum, true, true);
            OutputLabel = new CudaPieceFloat(MaxOutputItemNum, true, true);

            OutputScore = new CudaPieceFloat(MaxOutputItemNum, true, true);
            OutputMapSample = new CudaPieceInt(MaxOutputItemNum, true, true);
            Data = data;
        }

        public void Init()
        {
            BatchIdx = 0;
            RandomBatchIdx = Enumerable.Range(0, TrainBatchNum).ToList();
        }

        public bool TrainNext()
        {
            if (BatchIdx >= TrainBatchNum)
                return false;
            int randomPos = Util.URandom.Next(BatchIdx, TrainBatchNum);

            int id = RandomBatchIdx[randomPos];
            RandomBatchIdx[randomPos] = RandomBatchIdx[BatchIdx];
            RandomBatchIdx[BatchIdx++] = id;

            LoadTrainBatch(id);
            return true;
        }

        void LoadTrainBatch(int batchIdx)
        {
            var miniBatch = Data.TrainData.Skip(batchIdx * MiniBatchSize).Take(MiniBatchSize);

            int smpIdx = 0;
            int inputItemIdx = 0;
            int outputItemIdx = 0;
            switch (TrainMode)
            {
                case TrainSignalMode.RandomHeldOneSoftMax:
                    foreach (HashSet<int> record in miniBatch)
                    {
                        int sid = Util.URandom.Next(0, record.Count);
                        int r = record.ElementAt(sid);

                        InputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? record.Count - 1 : record.Count - 1 + InputSampleIndex.MemPtr[smpIdx - 1];
                        Array.Copy(record.Where(i => i != r).ToArray(), 0, InputItemIndex.MemPtr, inputItemIdx, record.Count - 1);

                        HashSet<int> negSample = Util.RandomNegative(NegativeNum, NegativeNum, Data.FeaNum, record);
                        OutputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? 1 + negSample.Count : 1 + negSample.Count + OutputSampleIndex.MemPtr[smpIdx - 1];
                        Array.Copy(Enumerable.Range(0, negSample.Count + 1).Select(i => smpIdx).ToArray(), 0, OutputMapSample.MemPtr, outputItemIdx, negSample.Count + 1);

                        OutputItemIndex.MemPtr[outputItemIdx] = r;
                        OutputLabel.MemPtr[outputItemIdx] = 1;
                        outputItemIdx += 1;

                        Array.Copy(negSample.ToArray(), 0, OutputItemIndex.MemPtr, outputItemIdx, negSample.Count);
                        Array.Copy(Enumerable.Range(0, negSample.Count).Select(i => -1).ToArray(), 0, OutputLabel.MemPtr, outputItemIdx, negSample.Count);

                        outputItemIdx += negSample.Count;
                        inputItemIdx += record.Count - 1;
                        smpIdx += 1;
                    }
                    break;
                case TrainSignalMode.SkipGramHeldOut:
                    for (int i = 0; i < MiniBatchSize; i++)
                    {
                        Tuple<int, int> edge = Data.Graph.SampleEdge();
                        int source = edge.Item1;
                        int target = edge.Item2;

                        InputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? 1 : 1 + InputSampleIndex.MemPtr[smpIdx - 1];
                        InputItemIndex.MemPtr[inputItemIdx] = source;

                        HashSet<int> negSet = new HashSet<int>();
                        for (int n = 0; n < NegativeNum; n++)
                        {
                            int neg = Data.Graph.SampleNode();
                            if (neg == target || neg == source)
                                continue;
                            negSet.Add(neg);
                        }

                        OutputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? 1 + negSet.Count : 1 + negSet.Count + OutputSampleIndex.MemPtr[smpIdx - 1];
                        Array.Copy(Enumerable.Range(0, negSet.Count + 1).Select(n => smpIdx).ToArray(), 0, OutputMapSample.MemPtr, outputItemIdx, negSet.Count + 1);

                        OutputItemIndex.MemPtr[outputItemIdx] = target;
                        OutputLabel.MemPtr[outputItemIdx] = 1;
                        outputItemIdx += 1;

                        Array.Copy(negSet.ToArray(), 0, OutputItemIndex.MemPtr, outputItemIdx, negSet.Count);
                        Array.Copy(Enumerable.Range(0, negSet.Count).Select(n => -1).ToArray(), 0, OutputLabel.MemPtr, outputItemIdx, negSet.Count);
                        outputItemIdx += negSet.Count;

                        inputItemIdx += 1;
                        smpIdx += 1;
                    }
                    break;
                case TrainSignalMode.HeldOneOut:
                    foreach (HashSet<int> record in miniBatch)
                    {
                        foreach (int r in record)
                        {
                            InputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? record.Count - 1 : record.Count - 1 + InputSampleIndex.MemPtr[smpIdx - 1];
                            Array.Copy(record.Where(i => i != r).ToArray(), 0, InputItemIndex.MemPtr, inputItemIdx, record.Count - 1);

                            OutputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? 1 : 1 + OutputSampleIndex.MemPtr[smpIdx - 1];
                            OutputItemIndex.MemPtr[outputItemIdx] = r;
                            OutputMapSample.MemPtr[outputItemIdx] = smpIdx;
                            OutputLabel.MemPtr[outputItemIdx] = 1;
                            inputItemIdx += record.Count - 1;
                            outputItemIdx += 1;
                            smpIdx += 1;
                        }

                        InputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? record.Count : record.Count + InputSampleIndex.MemPtr[smpIdx - 1];
                        Array.Copy(record.ToArray(), 0, InputItemIndex.MemPtr, inputItemIdx, record.Count);

                        HashSet<int> negSample = Util.RandomNegative(NegativeNum, NegativeNum, Data.FeaNum, record);
                        OutputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? negSample.Count : negSample.Count + OutputSampleIndex.MemPtr[smpIdx - 1];
                        Array.Copy(negSample.ToArray(), 0, OutputItemIndex.MemPtr, outputItemIdx, negSample.Count);
                        Array.Copy(Enumerable.Range(0, negSample.Count).Select(i => smpIdx).ToArray(), 0, OutputMapSample.MemPtr, outputItemIdx, negSample.Count);
                        Array.Copy(Enumerable.Range(0, negSample.Count).Select(i => -1).ToArray(), 0, OutputLabel.MemPtr, outputItemIdx, negSample.Count);

                        inputItemIdx += record.Count;
                        outputItemIdx += negSample.Count;
                        smpIdx += 1;
                    }
                    break;
                case TrainSignalMode.NormalizeHeldOneOut:
                    foreach (HashSet<int> record in miniBatch)
                    {
                        IEnumerable<int> list = Util.SplitCount(NegativeNum, ItemNum);
                        IEnumerable<int> pos = list.Intersect(record);
                        foreach (int r in pos)
                        {
                            InputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? record.Count - 1 : record.Count - 1 + InputSampleIndex.MemPtr[smpIdx - 1];
                            Array.Copy(record.Where(i => i != r).ToArray(), 0, InputItemIndex.MemPtr, inputItemIdx, record.Count - 1);

                            OutputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? 1 : 1 + OutputSampleIndex.MemPtr[smpIdx - 1];
                            OutputItemIndex.MemPtr[outputItemIdx] = r;
                            OutputMapSample.MemPtr[outputItemIdx] = smpIdx;
                            OutputLabel.MemPtr[outputItemIdx] = 1;
                            inputItemIdx += record.Count - 1;
                            outputItemIdx += 1;
                            smpIdx += 1;
                        }
                        IEnumerable<int> neg = list.Except(record);

                        InputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? record.Count : record.Count + InputSampleIndex.MemPtr[smpIdx - 1];
                        Array.Copy(record.ToArray(), 0, InputItemIndex.MemPtr, inputItemIdx, record.Count);

                        OutputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? neg.Count() : neg.Count() + OutputSampleIndex.MemPtr[smpIdx - 1];
                        Array.Copy(neg.ToArray(), 0, OutputItemIndex.MemPtr, outputItemIdx, neg.Count());
                        Array.Copy(Enumerable.Range(0, neg.Count()).Select(i => smpIdx).ToArray(), 0, OutputMapSample.MemPtr, outputItemIdx, neg.Count());
                        Array.Copy(Enumerable.Range(0, neg.Count()).Select(i => -1).ToArray(), 0, OutputLabel.MemPtr, outputItemIdx, neg.Count());
                        inputItemIdx += record.Count;
                        outputItemIdx += neg.Count();
                        smpIdx += 1;
                    }
                    break;
                case TrainSignalMode.RandomHeldOneOut:
                    foreach (HashSet<int> record in miniBatch)
                    {
                        int sid = Util.URandom.Next(0, record.Count);
                        int r = record.ElementAt(sid);

                        InputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? record.Count - 1 : record.Count - 1 + InputSampleIndex.MemPtr[smpIdx - 1];
                        Array.Copy(record.Where(i => i != r).ToArray(), 0, InputItemIndex.MemPtr, inputItemIdx, record.Count - 1);

                        OutputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? 1 : 1 + OutputSampleIndex.MemPtr[smpIdx - 1];
                        OutputItemIndex.MemPtr[outputItemIdx] = r;
                        OutputMapSample.MemPtr[outputItemIdx] = smpIdx;
                        OutputLabel.MemPtr[outputItemIdx] = 1;

                        inputItemIdx += record.Count - 1;
                        outputItemIdx += 1;
                        smpIdx += 1;

                        InputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? record.Count : record.Count + InputSampleIndex.MemPtr[smpIdx - 1];
                        Array.Copy(record.ToArray(), 0, InputItemIndex.MemPtr, inputItemIdx, record.Count);

                        HashSet<int> negSample = Util.RandomNegative(NegativeNum, NegativeNum, Data.FeaNum, record);
                        OutputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? negSample.Count : negSample.Count + OutputSampleIndex.MemPtr[smpIdx - 1];
                        Array.Copy(negSample.ToArray(), 0, OutputItemIndex.MemPtr, outputItemIdx, negSample.Count);
                        Array.Copy(Enumerable.Range(0, negSample.Count).Select(i => smpIdx).ToArray(), 0, OutputMapSample.MemPtr, outputItemIdx, negSample.Count);
                        Array.Copy(Enumerable.Range(0, negSample.Count).Select(i => -1).ToArray(), 0, OutputLabel.MemPtr, outputItemIdx, negSample.Count);

                        inputItemIdx += record.Count;
                        outputItemIdx += negSample.Count;
                        smpIdx += 1;
                    }
                    break;
                case TrainSignalMode.HeldRandomOut:
                    foreach (HashSet<int> record in miniBatch)
                    {
                        Tuple<HashSet<int>, HashSet<int>> dataSplit = Util.SplitRate<int>(record, HeldOutRate);

                        InputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? dataSplit.Item1.Count : dataSplit.Item1.Count + InputSampleIndex.MemPtr[smpIdx - 1];
                        Array.Copy(dataSplit.Item1.ToArray(), 0, InputItemIndex.MemPtr, inputItemIdx, dataSplit.Item1.Count);

                        HashSet<int> negSample = Util.RandomNegative(NegativeNum, NegativeNum, Data.FeaNum, record);
                        OutputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? dataSplit.Item2.Count + negSample.Count : dataSplit.Item2.Count + negSample.Count + OutputSampleIndex.MemPtr[smpIdx - 1];

                        Array.Copy(dataSplit.Item2.ToArray(), 0, OutputItemIndex.MemPtr, outputItemIdx, dataSplit.Item2.Count);
                        Array.Copy(Enumerable.Range(0, dataSplit.Item2.Count).Select(i => smpIdx).ToArray(), 0, OutputMapSample.MemPtr, outputItemIdx, dataSplit.Item2.Count);
                        Array.Copy(Enumerable.Range(0, dataSplit.Item2.Count).Select(i => 1).ToArray(), 0, OutputLabel.MemPtr, outputItemIdx, dataSplit.Item2.Count);
                        outputItemIdx += dataSplit.Item2.Count;

                        Array.Copy(negSample.ToArray(), 0, OutputItemIndex.MemPtr, outputItemIdx, negSample.Count);
                        Array.Copy(Enumerable.Range(0, negSample.Count).Select(i => smpIdx).ToArray(), 0, OutputMapSample.MemPtr, outputItemIdx, negSample.Count);
                        Array.Copy(Enumerable.Range(0, negSample.Count).Select(i => -1).ToArray(), 0, OutputLabel.MemPtr, outputItemIdx, negSample.Count);

                        outputItemIdx += negSample.Count;
                        inputItemIdx += dataSplit.Item1.Count;
                        smpIdx += 1;
                    }
                    break;
            }

            BatchSize = smpIdx;
            InputItemNum = inputItemIdx;
            OutputItemNum = outputItemIdx;

            InputSampleIndex.SyncFromCPU();
            InputItemIndex.SyncFromCPU();

            OutputSampleIndex.SyncFromCPU();
            OutputItemIndex.SyncFromCPU();
            OutputLabel.SyncFromCPU();
            OutputMapSample.SyncFromCPU();
        }

        public bool TestNext()
        {
            if (BatchIdx >= TestBatchNum)
                return false;
            LoadTestBatch(BatchIdx++);
            return true;
        }

        void LoadTestBatch(int batchIdx)
        {
            MiniBatchData = Data.TestData.Skip(batchIdx * MiniBatchSize).Take(MiniBatchSize);

            int smpIdx = 0;
            int inputItemIdx = 0;
            foreach (Tuple<HashSet<int>, HashSet<int>> record in MiniBatchData)
            {
                InputSampleIndex.MemPtr[smpIdx] = smpIdx == 0 ? record.Item1.Count : record.Item1.Count + InputSampleIndex.MemPtr[smpIdx - 1];
                Array.Copy(record.Item1.ToArray(), 0, InputItemIndex.MemPtr, inputItemIdx, record.Item1.Count);
                inputItemIdx += record.Item1.Count;
                smpIdx += 1;
            }
            BatchSize = smpIdx;
            InputItemNum = inputItemIdx;

            InputSampleIndex.SyncFromCPU(BatchSize);
            InputItemIndex.SyncFromCPU(InputItemNum);
        }
        
        ~CooccurrenceBatchInput()
        {
            this.Dispose(false);
        }
    }

    public enum BatchInputSourceMode
    {
        CooccurrenceTrain = 1,
        CooccurrenceTest = 2
    }

    //public class BatchInputLabelDataStat : DataStat
    //{
    //    public int MAX_FEATUREDIM = 0;
    //    public int MAX_BATCHSIZE = 0;
    //    public int MAX_ELEMENTSIZE = 0;

    //    public BatchInputLabelDataStat()
    //    {
    //    }
    //}

    //public class BatchInputLabelData : BatchData<BatchInputLabelDataStat>
    //{
    //    public override DataSourceID Type { get { return DataSourceID.BatchInputLabelData; } }

    //    public CudaPieceFloat LabelValue;
    //    public CudaPieceInt SampleIdx;
    //    public CudaPieceInt FeatureIdx;
    //    public CudaPieceFloat FeatureValue;
    //    public int ElementSize { get; set; }

    //    public BatchInputLabelData() { }
    //    //ShotOccurrence data, BatchInputSourceMode sourceMode, int batchSize)

    //    public override void Init(BatchInputLabelDataStat stat, DeviceType deviceType)
    //    {
    //        Stat = stat;
    //        DeviceType = deviceType;

    //        SampleIdx = new CudaPieceInt(Stat.MAX_BATCHSIZE, true, deviceType == DeviceType.GPU);
    //        FeatureIdx = new CudaPieceInt(Stat.MAX_ELEMENTSIZE, true, deviceType == DeviceType.GPU);
    //        FeatureValue = new CudaPieceFloat(Stat.MAX_ELEMENTSIZE, true, deviceType == DeviceType.GPU);
    //        LabelValue = new CudaPieceFloat(Stat.MAX_BATCHSIZE, true, deviceType == DeviceType.GPU);
    //    }

    //    public void FetchData(int batchIndex, ShotOccurrence source, BatchInputSourceMode sourceMode)
    //    {
    //        int smpIdx = 0;
    //        int elementIdx = 0;
    //        switch (sourceMode)
    //        {
    //            case BatchInputSourceMode.CooccurrenceTrain:
    //                var trainMiniBatch = source.TrainData.Skip(batchIndex * Stat.MAX_BATCHSIZE).Take(Stat.MAX_BATCHSIZE);
    //                foreach (HashSet<int> record in trainMiniBatch)
    //                {
    //                    SampleIdx.MemPtr[smpIdx] = smpIdx == 0 ? record.Count : record.Count + SampleIdx.MemPtr[smpIdx - 1];
    //                    Array.Copy(record.ToArray(), 0, FeatureIdx.MemPtr, elementIdx, record.Count);
    //                    Array.Copy(Enumerable.Range(0, record.Count).Select(i => 1).ToArray(), 0, FeatureValue.MemPtr, elementIdx, record.Count);
    //                    elementIdx += record.Count;
    //                    smpIdx += 1;
    //                }
    //                break;
    //            case BatchInputSourceMode.CooccurrenceTest:
    //                var testMiniBatch = source.TestData.Skip(batchIndex * Stat.MAX_BATCHSIZE).Take(Stat.MAX_BATCHSIZE);
    //                foreach (Tuple<HashSet<int>, HashSet<int>> record in testMiniBatch)
    //                {
    //                    SampleIdx.MemPtr[smpIdx] = smpIdx == 0 ? record.Item1.Count : record.Item1.Count + SampleIdx.MemPtr[smpIdx - 1];
    //                    Array.Copy(record.Item1.ToArray(), 0, FeatureIdx.MemPtr, elementIdx, record.Item1.Count);
    //                    Array.Copy(Enumerable.Range(0, record.Item1.Count).Select(i => 1).ToArray(), 0, FeatureValue.MemPtr, elementIdx, record.Item1.Count);
    //                    LabelValue.MemPtr[smpIdx] = record.Item2.First();
    //                    elementIdx += record.Item1.Count;
    //                    smpIdx += 1;
    //                }
    //                break;
    //        }
    //        BatchSize = smpIdx;
    //        ElementSize = elementIdx;
    //        Batch_In_GPU();
    //    }

    //    //public IEnumerable<Tuple<HashSet<int>, HashSet<int>>> TestMiniBatch { get; set; }

    //    public void Batch_In_GPU()
    //    {
    //        if (SampleIdx.CudaPtr != IntPtr.Zero) SampleIdx.SyncFromCPU();
    //        if (FeatureIdx.CudaPtr != IntPtr.Zero) FeatureIdx.SyncFromCPU();
    //        if (FeatureValue.CudaPtr != IntPtr.Zero) FeatureValue.SyncFromCPU();
    //        if (LabelValue.CudaPtr != IntPtr.Zero) LabelValue.SyncFromCPU();
    //    }


    //    //public Instance Get()
    //    //{
    //    //    Instance instance = new Instance(1 * sizeof(int));
    //    //    using (BinaryWriter writer = new BinaryWriter(new MemoryStream(instance.Binary)))
    //    //    {
    //    //        writer.Write(BatchIdx);
    //    //    }
    //    //    return instance;
    //    //}
    //    //public void Put(Instance instance)
    //    //{
    //    //    using (BinaryReader reader = new BinaryReader(new MemoryStream(instance.Binary)))
    //    //    {
    //    //        int batchIndex = reader.ReadInt32();
    //    //        Fetch(batchIndex);
    //    //    }
    //    //}
    //}

    /// <summary>
    /// Do not need to generate to different data type.
    /// </summary>
    //public class CooccurrenceDataCashier
    //{
    //    public BatchInputSourceMode SourceMode { get; set; }
    //    public ShotOccurrence Source = null;

    //    BatchInputLabelDataStat Stat = new BatchInputLabelDataStat();

    //    List<BatchInputLabelData> L2Caches = new List<BatchInputLabelData>();

    //    /// <summary>
    //    /// Shot Occurrence Data.
    //    /// </summary>
    //    /// <param name="binaryStream"></param>
    //    public CooccurrenceDataCashier(ShotOccurrence data, BatchInputSourceMode sourceMode, int batchSize)
    //    {
    //        SourceMode = sourceMode;
    //        Source = data;

    //        Stat.MAX_FEATUREDIM = data.FeaNum;
    //        Stat.MAX_BATCHSIZE = batchSize;

    //        switch (sourceMode)
    //        {
    //            case BatchInputSourceMode.CooccurrenceTrain:
    //                Stat.TotalSampleNumber = Source.TrainNum;
    //                Stat.TotalBatchNumber = Stat.TotalSampleNumber / Stat.MAX_BATCHSIZE + 1;
    //                for (int i = 0; i < Stat.TotalBatchNumber; i++)
    //                {
    //                    var miniBatch = data.TrainData.Skip(i * batchSize).Take(batchSize);
    //                    int elementSize = (from p in miniBatch select p.Count).Sum();
    //                    Stat.MAX_ELEMENTSIZE = Math.Max(elementSize, Stat.MAX_ELEMENTSIZE);
    //                }
    //                break;
    //            case BatchInputSourceMode.CooccurrenceTest:
    //                Stat.TotalSampleNumber = Source.TestNum;
    //                Stat.TotalBatchNumber = Stat.TotalSampleNumber / Stat.MAX_BATCHSIZE + 1;
    //                for (int i = 0; i < Stat.TotalBatchNumber; i++)
    //                {
    //                    var miniBatch = data.TestData.Skip(i * batchSize).Take(batchSize);
    //                    int elementSize = (from p in miniBatch select p.Item1.Count).Sum();
    //                    Stat.MAX_ELEMENTSIZE = Math.Max(elementSize, Stat.MAX_ELEMENTSIZE);
    //                }
    //                break;
    //        }
    //    }

    //    public void InitPipelineCashier(int l2Cache)
    //    {
    //        for (int i = 0; i < l2Cache; i++)
    //        {
    //            L2Caches.Add(new BatchInputLabelData());
    //            L2Caches[i].Init(Stat, DeviceType.GPU);
    //        }
    //    }

    //    public IEnumerable<BatchInputLabelData> GetInstances(bool randomProcessor)
    //    {
    //        BlockingCollection<int> gpuBuffer = new BlockingCollection<int>(L2Caches.Count - 2);

    //        var cpu2gpuParallel = Task.Run(() =>
    //        {
    //            if (randomProcessor)
    //            {
    //                int[] RandomBatchIdx = Enumerable.Range(0, Stat.TotalBatchNumber).ToArray();
    //                int batIdx = 0;
    //                while (batIdx < Stat.TotalBatchNumber)
    //                {
    //                    int randomPos = Util.URandom.Next(batIdx, Stat.TotalBatchNumber);
    //                    int id = RandomBatchIdx[randomPos];
    //                    RandomBatchIdx[randomPos] = RandomBatchIdx[batIdx];
    //                    RandomBatchIdx[batIdx] = id;

    //                    gpuBuffer.Add(batIdx % L2Caches.Count);

    //                    L2Caches[batIdx % L2Caches.Count].FetchData(id, Source, SourceMode);
    //                    batIdx += 1;
    //                }
    //            }
    //            else
    //            {
    //                int batIdx = 0;
    //                while (batIdx < Stat.TotalBatchNumber)
    //                {
    //                    gpuBuffer.Add(batIdx % L2Caches.Count);
    //                    L2Caches[batIdx % L2Caches.Count].FetchData(batIdx, Source, SourceMode);
    //                    batIdx += 1;
    //                }
    //            }
    //            // Let consumer know we are done.
    //            gpuBuffer.CompleteAdding();
    //        });

    //        while (!gpuBuffer.IsCompleted)
    //        {
    //            int gpuId = -1;
    //            try
    //            {
    //                gpuId = gpuBuffer.Take();
    //            }
    //            catch (InvalidOperationException) { }

    //            if (gpuId != -1) yield return L2Caches[gpuId];
    //        }
    //    }
    //}
}
