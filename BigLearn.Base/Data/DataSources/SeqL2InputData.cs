﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{


    public class SeqSentenceInputDataStat : DataStat
    {
        public override DataSourceID Type { get { return DataSourceID.SeqSentenceDataMeta; } }

        /// <summary>
        /// Maximum Feature Dimension.
        /// </summary>
        public int MAX_FEATUREDIM { get; set; }

        /// <summary>
        /// Maximum Batch Size.
        /// </summary>
        public int MAX_BATCHSIZE { get; set; }

        /// <summary>
        /// Maximum Segment Size.
        /// </summary>
        public int MAX_SENTENCESIZE { get; set; }
        /// <summary>
        /// Maximum Segment Size.
        /// </summary>
        public int MAX_WORDSIZE { get; set; }

        /// <summary>
        /// Maximum Element Size.
        /// </summary>
        public int MAX_ELEMENTSIZE { get; set; }

        public SeqSentenceInputDataStat()
        {
            MAX_FEATUREDIM = 0;
            MAX_BATCHSIZE = 0;
            MAX_SENTENCESIZE = 0;
            MAX_WORDSIZE = 0;
            MAX_ELEMENTSIZE = 0;
        }

        public override void Save(BinaryWriter writer)
        {
            writer.Write(MAX_FEATUREDIM);
            writer.Write(MAX_BATCHSIZE);
            writer.Write(MAX_SENTENCESIZE);
            writer.Write(MAX_WORDSIZE);
            writer.Write(MAX_ELEMENTSIZE);
            writer.Write(this.TotalBatchNumber);
            writer.Write(this.TotalSampleNumber);
        }

        public override void Init(BinaryReader reader)
        {
            reader.BaseStream.Seek(-7 * sizeof(Int32), SeekOrigin.End);
            MAX_FEATUREDIM = reader.ReadInt32();
            MAX_BATCHSIZE = reader.ReadInt32();
            MAX_SENTENCESIZE = reader.ReadInt32();
            MAX_WORDSIZE = reader.ReadInt32();
            MAX_ELEMENTSIZE = reader.ReadInt32();
            this.TotalBatchNumber = reader.ReadInt32();
            this.TotalSampleNumber = reader.ReadInt32();
        }

        public override IEnumerable<KeyValuePair<string, string>> ToKeyValues()
        {
            // Keep order
            yield return new KeyValuePair<string, string>("TYPE", this.Type.ToString());
            yield return new KeyValuePair<string, string>("FEATURE_DIM", this.MAX_FEATUREDIM.ToString());
            yield return new KeyValuePair<string, string>("MAX_BATCHSIZE", this.MAX_BATCHSIZE.ToString());
            yield return new KeyValuePair<string, string>("MAX_SENTENCESIZE", this.MAX_SENTENCESIZE.ToString());
            yield return new KeyValuePair<string, string>("MAX_WORDSIZE", this.MAX_WORDSIZE.ToString());
            yield return new KeyValuePair<string, string>("MAX_ELEMENTSIZE", this.MAX_ELEMENTSIZE.ToString());
            yield return new KeyValuePair<string, string>("TOTAL_BATCHNUM", this.TotalBatchNumber.ToString());
            yield return new KeyValuePair<string, string>("TOTAL_SAMPLENUM", this.TotalSampleNumber.ToString());
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            TextWriter tw = new StringWriter(sb);
            foreach (var kv in this.ToKeyValues())
            {
                tw.WriteLine("{0}\t{1}", kv.Key, kv.Value);
            }

            tw.Flush();
            return sb.ToString();
        }
    }

    /// <summary>
    /// sample x : <sequence1 of feature set>, <sequence2 of feature set>
    /// Sequence of Sequence of Feature Set;
    /// Consider:
    ///     A Sentence consists of sequence of letter-tri-grams
    ///     A Paragraph consists of sequence of sentences.
    /// </summary>
    public class SeqSentenceBatchInputData : BatchData<SeqSentenceInputDataStat>
    {
        public override DataSourceID Type { get { return DataSourceID.SeqSentenceBatchInputData; } }

        public override int BatchSize
        {
            get
            {
                return SampleIdx.EffectiveSize;
            }
            set
            {
                SampleIdx.EffectiveSize = value;
                if (Stat.MAX_BATCHSIZE < value) Stat.MAX_BATCHSIZE = value;
            }
        }
        public int SentSize // the total length of the full sentences.
        {
            get
            {
                return SentIdx.EffectiveSize;
            }
            set
            {
                SentIdx.EffectiveSize = value;
                SentMargin.EffectiveSize = value;
                if (Stat.MAX_SENTENCESIZE < value) Stat.MAX_SENTENCESIZE = value;
            }
        }
        public int WordSize // the total length of the full words.
        {
            get
            {
                return WordIdx.EffectiveSize;
            }
            set
            {
                WordIdx.EffectiveSize = value;
                WordMargin.EffectiveSize = value;
                if (Stat.MAX_WORDSIZE < value) Stat.MAX_WORDSIZE = value;
            }
        }
        public int ElementSize // the total length of the full feature elements.
        {
            get
            {
                return FeaIdx.EffectiveSize;
            }
            set
            {
                FeaIdx.EffectiveSize = value;
                FeaValue.EffectiveSize = value;
                if (Stat.MAX_ELEMENTSIZE < value) Stat.MAX_ELEMENTSIZE = value;
            }
        }

        public CudaPieceInt SampleIdx;

        public CudaPieceInt SentIdx;
        public CudaPieceInt SentMargin;

        public CudaPieceInt WordIdx;
        public CudaPieceInt WordMargin;

        public CudaPieceInt FeaIdx;
        public CudaPieceFloat FeaValue;
        
        public SeqSentenceBatchInputData() { }
        public SeqSentenceBatchInputData(SeqSentenceInputDataStat stat, DeviceType deviceType) : base(stat, deviceType) { }
        
        public override void Init(SeqSentenceInputDataStat stat, DeviceType deviceType)
        {
            Stat = stat;
            SampleIdx = new CudaPieceInt(Stat.MAX_BATCHSIZE, true, deviceType == DeviceType.GPU);
            SentIdx = new CudaPieceInt(Stat.MAX_SENTENCESIZE, true, deviceType == DeviceType.GPU);
            SentMargin = new CudaPieceInt(Stat.MAX_SENTENCESIZE, true, deviceType == DeviceType.GPU);
            WordIdx = new CudaPieceInt(Stat.MAX_WORDSIZE, true, deviceType == DeviceType.GPU);
            WordMargin = new CudaPieceInt(Stat.MAX_WORDSIZE, true, deviceType == DeviceType.GPU);
            FeaIdx = new CudaPieceInt(Stat.MAX_ELEMENTSIZE, true, deviceType == DeviceType.GPU);
            FeaValue = new CudaPieceFloat(Stat.MAX_ELEMENTSIZE, true, deviceType == DeviceType.GPU);
        }

        public override void Load(BinaryReader reader)
        {
            BatchSize = reader.ReadInt32();
            SentSize = reader.ReadInt32();
            WordSize = reader.ReadInt32();
            ElementSize = reader.ReadInt32();

            Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * BatchSize), 0, SampleIdx.MemPtr, 0, sizeof(Int32) * BatchSize);
            Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * SentSize), 0, SentIdx.MemPtr, 0, sizeof(Int32) * SentSize);
            Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * WordSize), 0, WordIdx.MemPtr, 0, sizeof(Int32) * WordSize);
            Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * ElementSize), 0, FeaIdx.MemPtr, 0, sizeof(Int32) * ElementSize);
            Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * ElementSize), 0, FeaValue.MemPtr, 0, sizeof(float) * ElementSize);

            for (int batch = 0; batch < BatchSize; batch++)
            {
                for (int sent = batch == 0 ? 0 : SampleIdx.MemPtr[batch - 1]; sent < SampleIdx.MemPtr[batch]; sent++)
                {
                    SentMargin.MemPtr[sent] = batch;
                    for (int fea = sent == 0 ? 0 : SentIdx.MemPtr[sent - 1]; fea < SentIdx.MemPtr[sent]; fea++)
                    {
                        WordMargin.MemPtr[fea] = sent;
                    }
                }
            }

            SyncFromCPU();
        }

        public override void SyncFromCPU()
        {
            SampleIdx.SyncFromCPU(BatchSize);
            SentIdx.SyncFromCPU(SentSize);
            WordIdx.SyncFromCPU(WordSize);
            FeaIdx.SyncFromCPU(ElementSize);
            FeaValue.SyncFromCPU(ElementSize);

            SentMargin.SyncFromCPU(SentSize);
            WordMargin.SyncFromCPU(WordSize);
        }

        public override void SyncToCPU()
        {
            SampleIdx.SyncToCPU(BatchSize);
            SentIdx.SyncToCPU(SentSize);
            WordIdx.SyncToCPU(WordSize);
            FeaIdx.SyncToCPU(ElementSize);
            FeaValue.SyncToCPU(ElementSize);

            SentMargin.SyncToCPU(SentSize);
            WordMargin.SyncToCPU(WordSize);
        }

        public override void CopyFrom(IData other)
        {
            SeqSentenceBatchInputData block = (SeqSentenceBatchInputData)other;
            BatchSize = block.BatchSize;
            SentSize = block.SentSize;
            WordSize = block.WordSize;
            ElementSize = block.ElementSize;

            SampleIdx.CopyFrom(0, block.SampleIdx, 0, BatchSize);
            SentIdx.CopyFrom(0, block.SentIdx, 0, SentSize);
            SentMargin.CopyFrom(0, block.SentMargin, 0, SentSize);
            WordIdx.CopyFrom(0, block.WordIdx, 0, WordSize);
            WordMargin.CopyFrom(0, block.WordMargin, 0, WordSize);
            FeaIdx.CopyFrom(0, block.FeaIdx, 0, ElementSize);
            FeaValue.CopyFrom(0, block.FeaValue, 0, ElementSize);
        }

        /// <summary>
        /// Do not change the function Name.
        /// </summary>
        /// <param name="reader"></param>
        public override void SkipBlock(BinaryReader reader, DataStat stat)
        {
            int b = reader.ReadInt32();
            int s = reader.ReadInt32();
            int w = reader.ReadInt32();
            int e = reader.ReadInt32();
            reader.BaseStream.Seek((b + s + w + e) * sizeof(int) + e * sizeof(float), SeekOrigin.Current);
        }

        public override void Save(BinaryWriter writer)
        {
            if (BatchSize > 0)
            {
                SyncToCPU();

                writer.Write(BatchSize);
                writer.Write(SentSize);
                writer.Write(WordSize);
                writer.Write(ElementSize);

                for (int i = 0; i < BatchSize; ++i) writer.Write(SampleIdx.MemPtr[i]);
                for (int i = 0; i < SentSize; ++i) writer.Write(SentIdx.MemPtr[i]);
                for (int i = 0; i < WordSize; ++i) writer.Write(WordIdx.MemPtr[i]);
                for (int i = 0; i < ElementSize; ++i) writer.Write(FeaIdx.MemPtr[i]);
                for (int i = 0; i < ElementSize; ++i) writer.Write(FeaValue.MemPtr[i]);
            }
        }

        public override void Clear()
        {
            BatchSize = 0;
            SentSize = 0;
            WordSize = 0;
            ElementSize = 0;
        }

        public void PushSample(List<List<Dictionary<int, float>>> wordDictList)
        {
            int batchNum = 1;
            int sentNum = wordDictList.Count;
            int wordNum = wordDictList.Select(feaDictList => feaDictList.Count).Sum();
            int elementNum = wordDictList.Select(feaDictList => feaDictList.Select(i => i.Count).Sum()).Sum();

            int oldBatchSize = BatchSize;
            int oldSentSize = SentSize;
            int oldWordSize = WordSize;
            int oldElementSize = ElementSize;

            BatchSize = oldBatchSize + batchNum;
            SentSize = oldSentSize + sentNum;
            WordSize = oldWordSize + wordNum;
            ElementSize = oldElementSize + elementNum;

            SampleIdx.MemPtr[oldBatchSize] = SentSize; //  BatchSize > 0 ? SampleIdx.MemPtr[BatchSize - 1] + wordDictList.Count : wordDictList.Count;
            int sentcursor = oldSentSize;
            int wordcursor = oldWordSize;
            int elementcursor = oldElementSize;

            foreach (List<Dictionary<int, float>> feaDictList in wordDictList)
            {
                //int sentStart = (SentSize == 0) ? 0 : SentIdx.MemPtr[SentSize - 1];
                SentIdx.MemPtr[sentcursor] = wordcursor + feaDictList.Count;
                SentMargin.MemPtr[sentcursor] = oldBatchSize;
                foreach (Dictionary<int, float> seg in feaDictList)
                {
                    //int segStart = (WordSize == 0) ? 0 : WordIdx.MemPtr[WordSize - 1];
                    WordIdx.MemPtr[wordcursor] = elementcursor + seg.Count;
                    WordMargin.MemPtr[wordcursor] = sentcursor;
                    foreach (KeyValuePair<int, float> fv in seg)
                    {
                        FeaIdx.MemPtr[elementcursor] = fv.Key;
                        FeaValue.MemPtr[elementcursor] = fv.Value;
                        elementcursor += 1;
                        if (fv.Key >= Stat.MAX_FEATUREDIM) Stat.MAX_FEATUREDIM = fv.Key + 1;
                    }
                    wordcursor += 1;
                }
                sentcursor += 1;
            }

            SampleIdx.SyncFromCPU(oldBatchSize, SampleIdx.MemPtr, oldBatchSize, BatchSize - oldBatchSize);
            SentIdx.SyncFromCPU(oldSentSize, SentIdx.MemPtr, oldSentSize, SentSize - oldSentSize);
            SentMargin.SyncFromCPU(oldSentSize, SentMargin.MemPtr, oldSentSize, SentSize - oldSentSize);
            WordIdx.SyncFromCPU(oldWordSize, WordIdx.MemPtr, oldWordSize, WordSize - oldWordSize);
            WordMargin.SyncFromCPU(oldWordSize, WordMargin.MemPtr, oldWordSize, WordSize - oldWordSize);
            FeaIdx.SyncFromCPU(oldElementSize, FeaIdx.MemPtr, oldElementSize, ElementSize - oldElementSize);
            FeaValue.SyncFromCPU(oldElementSize, FeaValue.MemPtr, oldElementSize, ElementSize - oldElementSize);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (SampleIdx != null) SampleIdx.Dispose(); SampleIdx = null;
                if (SentIdx != null) SentIdx.Dispose(); SentIdx = null;
                if (SentMargin != null) SentMargin.Dispose(); SentMargin = null;
                if (WordIdx != null) WordIdx.Dispose(); WordIdx = null;
                if (WordMargin != null) WordMargin.Dispose(); WordMargin = null;
                if (FeaIdx != null) FeaIdx.Dispose(); FeaIdx = null;
                if (FeaValue != null) FeaValue.Dispose(); FeaValue = null;
            }
        }
    }

    /// <summary>
    /// Add label for each sample in SeqSentenceBatchInputData
    /// </summary>
    //public class SeqSentenceBatchInputLabelData : SeqSentenceBatchInputData
    //{
    //    public override DataSourceID Type { get { return DataSourceID.SeqSentenceBatchInputLabelData; } }

    //    public CudaPieceFloat Label;

    //    public override int BatchSize
    //    {
    //        get
    //        {
    //            return base.BatchSize;
    //        }
    //        set
    //        {
    //            base.BatchSize = value;
    //            Label.EffectiveSize = value;
    //        }
    //    }


    //    public SeqSentenceBatchInputLabelData() { }
    //    public SeqSentenceBatchInputLabelData(SeqSentenceInputDataStat stat, DeviceType device) : base(stat, device) { }

    //    public override void Init(SeqSentenceInputDataStat stat, DeviceType deviceType)
    //    {
    //        base.Init(stat, deviceType);
    //        Label = new CudaPieceFloat(stat.MAX_BATCHSIZE, true, deviceType == DeviceType.GPU);
    //    }

    //    public override void Load(BinaryReader reader)
    //    {
    //        base.Load(reader);
    //        Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * BatchSize), 0, Label.MemPtr, 0, sizeof(float) * BatchSize);
    //        Label.SyncFromCPU(BatchSize);
    //    }
    //    public override void Save(BinaryWriter writer)
    //    {
    //        base.Save(writer);
    //        Label.SyncToCPU(BatchSize);
    //        for (int i = 0; i < BatchSize; ++i) writer.Write(Label.MemPtr[i]);
    //    }

    //    public override void CopyFrom(IData other)
    //    {
    //        base.CopyFrom(other);
    //        SeqSentenceBatchInputLabelData block = (SeqSentenceBatchInputLabelData)other;
    //        Label.CopyFrom(0, block.Label, 0, block.BatchSize);
    //    }

    //    public override void SyncFromCPU()
    //    {
    //        Label.SyncFromCPU(BatchSize);
    //    }

    //    public override void SyncToCPU()
    //    {
    //        Label.SyncToCPU(BatchSize);
    //    }

    //    public override void SkipBlock(BinaryReader reader, DataStat stat)
    //    {
    //        int b = reader.ReadInt32();
    //        int s = reader.ReadInt32();
    //        int w = reader.ReadInt32();
    //        int e = reader.ReadInt32();
    //        reader.BaseStream.Seek((b + s + w + e) * sizeof(int) + (e + b) * sizeof(float), SeekOrigin.Current);
    //    }


    //    public void PushSample(List<List<Dictionary<int, float>>> segDictList, float label)
    //    {
    //        int oldBatchSize = BatchSize;
    //        PushSample(segDictList);

    //        Label.MemPtr[oldBatchSize] = label;
    //        Label.SyncFromCPU(oldBatchSize, Label.MemPtr, oldBatchSize, 1);
    //    }

    //    protected override void Dispose(bool disposing)
    //    {
    //        base.Dispose(disposing);
    //        if (disposing)
    //        {
    //            if (Label != null) Label.Dispose(); Label = null;
    //        }
    //    }
    //}
}
