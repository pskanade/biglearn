﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ImageDataStat : DataStat
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int Depth { get; set; }   // Color or Stack Image Number. 

        public bool IsSource = false;

        public int MAX_BATCHSIZE { get; set; }

        public ImageDataStat()
        {
            MAX_BATCHSIZE = 0;
            Width = 0;
            Height = 0;
            Depth = 0;
        }

        public override void Save(BinaryWriter writer)
        {
            writer.Write(Width);
            writer.Write(Height);
            writer.Write(Depth);
            writer.Write(MAX_BATCHSIZE);
            writer.Write(this.TotalBatchNumber);
            writer.Write(this.TotalSampleNumber);
        }

        public override void Init(BinaryReader reader)
        {
            reader.BaseStream.Seek(-6 * sizeof(Int32), SeekOrigin.End);
            Width = reader.ReadInt32();
            Height = reader.ReadInt32();
            Depth = reader.ReadInt32();
            MAX_BATCHSIZE = reader.ReadInt32();
            this.TotalBatchNumber = reader.ReadInt32();
            this.TotalSampleNumber = reader.ReadInt32();
            IsSource = true;
        }
    }

    /// <summary>
    /// Basic Image Data. Format : NCHW.
    /// </summary>
    public class ImageDataSource : BatchData<ImageDataStat>
    {
        public override DataSourceID Type { get { return DataSourceID.ImageData; } }

        public CudaPieceFloat Deriv;
        public CudaPieceFloat Data;
        public int Size { get { return Data.EffectiveSize; } }
        public bool IsSource { get { return Stat.IsSource; } set { Stat.IsSource = value; } }

        public override int BatchSize
        {
            get
            {
                return Data.EffectiveSize / (Stat.Depth * Stat.Width * Stat.Height);
            }

            set
            {
                Data.EffectiveSize = value * Stat.Width * Stat.Height * Stat.Depth;
                if(Deriv != null && Deriv != CudaPieceFloat.Empty) Deriv.EffectiveSize = value * Stat.Width * Stat.Height * Stat.Depth;
                if (Stat.MAX_BATCHSIZE < value) Stat.MAX_BATCHSIZE = value;
            }
        }
        public ImageDataSource() { }
        public ImageDataSource(ImageDataStat stat, DeviceType device) : base(stat, device) { }

        public override void Init(ImageDataStat stat, DeviceType deviceType)
        {
            base.Init(stat, deviceType);
            Data = new CudaPieceFloat(Stat.MAX_BATCHSIZE * Stat.Width * Stat.Height * Stat.Depth, true, deviceType == DeviceType.GPU);
            if (!IsSource) { Deriv = new CudaPieceFloat(Stat.MAX_BATCHSIZE * Stat.Width * Stat.Height * Stat.Depth, true, deviceType == DeviceType.GPU); }
            else { Deriv = CudaPieceFloat.Empty; }
        }

        public ImageDataSource(int maxBatchSize, int width, int height, int depth, DeviceType type): 
            this(maxBatchSize, width, height, depth, false, type)
        { }

        public ImageDataSource(int maxBatchSize, int width, int height, int depth, bool isSource, DeviceType type) :
            this(new ImageDataStat() { MAX_BATCHSIZE = maxBatchSize, Width = width, Height = height, Depth = depth, IsSource = isSource }, type)
        { }
        

        public override void CopyFrom(IData other)
        {
            ImageDataSource source = (ImageDataSource)other;
            BatchSize = source.BatchSize;
            Data.CopyFrom(0, source.Data, 0, source.BatchSize * source.Stat.Width * source.Stat.Height * source.Stat.Depth);
        }

        public override void Load(BinaryReader reader)
        {
            BatchSize = reader.ReadInt32();
            Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * BatchSize * Stat.Width * Stat.Height * Stat.Depth), 0,
                             Data.MemPtr, 0, sizeof(float) * BatchSize * Stat.Width * Stat.Height * Stat.Depth);
        }

        /// <summary>
        /// Do not change the function Name.
        /// </summary>
        /// <param name="reader"></param>
        public override void SkipBlock(BinaryReader reader, DataStat stat)
        {
            int b = reader.ReadInt32();
            ImageDataStat imageStat = (ImageDataStat)stat;
            reader.BaseStream.Seek((b * imageStat.Width * imageStat.Height * imageStat.Depth) * sizeof(float), SeekOrigin.Current);
        }


        public override void Save(BinaryWriter writer)
        {
            if (BatchSize > 0)
            {
                writer.Write(BatchSize);
                for (int i = 0; i < BatchSize * Stat.Width * Stat.Height * Stat.Depth; ++i)
                    writer.Write(Data.MemPtr[i]);
            }
        }

        public void PushSample(float[] value, int batchNum)
        {
            int oldBatchSize = BatchSize;
            BatchSize = oldBatchSize + batchNum;
            Array.Copy(value, 0, Data.MemPtr, oldBatchSize * Stat.Width * Stat.Height * Stat.Depth, Stat.Width * Stat.Height * Stat.Depth * batchNum);
        }

        public override void Clear()
        {
            BatchSize = 0;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (Data != null) Data.Dispose(); Data = null;
                if (Deriv != null) Deriv.Dispose(); Deriv = null;
            }
        }
    }
    
}
