﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MemoryQAHelpData : BatchData<BiMatchBatchDataStat>
    {
        public override int BatchSize
        {
            get { return BatchMemIdx.EffectiveSize; }
            set
            {
                if (BatchMemIdx != null) BatchMemIdx.EffectiveSize = value;
                if (Stat.MAX_SRC_BATCHSIZE < value) Stat.MAX_SRC_BATCHSIZE = value;
            }
        }

        public int MemorySize
        {
            get
            {
                return MatchMem.EffectiveSize;
            }
            set
            {
                MatchSrc.EffectiveSize = value;
                MatchMem.EffectiveSize = value;
                if (Stat.MAX_MATCH_BATCHSIZE < value) Stat.MAX_MATCH_BATCHSIZE = value;
            }
        }

        public CudaPieceInt MatchSrc;
        public CudaPieceInt MatchMem;
        public CudaPieceInt MatchOut;

        public CudaPieceInt BatchMemIdx;
        public override DeviceType DeviceType { get; set; }

        public MemoryQAHelpData(int maxSrcSize, int maxMatchSize, DeviceType device)
        {
            Stat = new BiMatchBatchDataStat() { MAX_SRC_BATCHSIZE = maxSrcSize, MAX_MATCH_BATCHSIZE = maxMatchSize } ;
            DeviceType = device;
            MatchSrc = new CudaPieceInt(Stat.MAX_MATCH_BATCHSIZE, true, device == DeviceType.GPU);
            MatchMem = new CudaPieceInt(Stat.MAX_MATCH_BATCHSIZE, true, device == DeviceType.GPU);
            MatchOut = new CudaPieceInt(Stat.MAX_MATCH_BATCHSIZE, true, device == DeviceType.GPU);
            BatchMemIdx = new CudaPieceInt(Stat.MAX_SRC_BATCHSIZE, true, device == DeviceType.GPU);
        }

        public MemoryQAHelpData(BiMatchBatchData matchData, DeviceType device)
        {
            Stat = matchData.Stat;
            MatchSrc = matchData.SrcIdx;
            MatchMem = matchData.TgtIdx;
            MatchOut = matchData.SrcIdx;
            BatchMemIdx = matchData.Src2MatchIdx;
        }

        public void GenerateMemoryHelp(int[] smpIdx, int batchSize, int[] mapForward, int sentSize)
        {
            BatchSize = batchSize;
            MemorySize = sentSize;

            for (int i = 0; i < BatchSize; i++)
            {
                int memBgn = i == 0 ? 0 : smpIdx[i - 1];
                int memEnd = smpIdx[i];
                for (int m = memBgn; m < memEnd; m++)
                {
                    MatchSrc.MemPtr[m] = i;
                    int newPos = mapForward == null ? m : mapForward[m];
                    MatchMem.MemPtr[m] = newPos;
                    MatchOut.MemPtr[m] = i;
                }
                BatchMemIdx.MemPtr[i] = memEnd;
            }
            BatchMemIdx.SyncFromCPU(BatchSize);
            MatchSrc.SyncFromCPU(MemorySize);
            MatchOut.SyncFromCPU(MemorySize);
            MatchMem.SyncFromCPU(MemorySize);
        }


    }
}
