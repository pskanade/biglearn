﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BigLearn
{
    public class SeqDenseSlotBatchData : BatchData<DataStat>
    {
        public SeqDenseBatchData InputSeqData;
        public CudaPieceFloat SentGroundTruth;
        public CudaPieceInt SampleEarlyStop;

        public override DataSourceID Type { get { return DataSourceID.SeqDenseSlotBatchData; } }

        public SeqDenseSlotBatchData()
        { }

        public SeqDenseSlotBatchData(SeqDenseBatchData data, CudaPieceFloat label, CudaPieceInt earlyStop)
        {
            Stat = data.Stat;
            InputSeqData = data;
            SentGroundTruth = label;
            SampleEarlyStop = earlyStop;
        }

        public SeqDenseSlotBatchData(SeqDenseBatchData data)
        {
            Stat = data.Stat;
            InputSeqData = data;
        }
    }

    public class SeqSparseSlotBatchData : BatchData<SequenceDataStat>
    {
        public SeqSparseBatchData InputSeqData;
        public CudaPieceFloat SentGroundTruth;
        public CudaPieceInt SampleEarlyStop;

        public override DataSourceID Type { get { return DataSourceID.SeqSparseSlotBatchData; } }

        //public SeqSparseSlotBatchData()
        //{
        //    Stat = new SequenceDataStat();
        //}

        public SeqSparseSlotBatchData(SeqSparseBatchData data, CudaPieceFloat label, CudaPieceInt earlyStop)
        {
            Stat = data.Stat;
            InputSeqData = data;
            SentGroundTruth = label;
            SampleEarlyStop = earlyStop;
        }

        //public SeqSparseSlotBatchData(SeqSparseBatchData data)
        //{
        //    Stat = data.Stat;
        //    InputSeqData = data;
        //}
    }
    
    /// <summary>
    /// Sparse Feature Sequence Input Data + labels.
    /// It is used for forcasting task.
    /// </summary>
    public class SeqSparseSlotDataSource : SeqSparseDataSource
    {
        public override DataSourceID Type { get { return DataSourceID.SequenceSlotData; } }

        public CudaPieceInt SampleEarlyStop; // the element should be less than in SampleIdx.

        public override int BatchSize
        {
            get
            {
                return base.BatchSize;
            }
            set
            {
                base.BatchSize = value;
                SampleEarlyStop.EffectiveSize = value;
            }
        }

        public SeqSparseSlotDataSource() {  }
        public SeqSparseSlotDataSource(SequenceDataStat stat, DeviceType device ) { Init(stat, device); }

        public override void Init(SequenceDataStat stat, DeviceType deviceType)
        {
            base.Init(stat, deviceType);
            SampleEarlyStop = new CudaPieceInt(Stat.MAX_BATCHSIZE, true, deviceType == DeviceType.GPU);
        }

        public override void SkipBlock(BinaryReader reader, DataStat stat)
        {
            int b = reader.ReadInt32();
            int s = reader.ReadInt32();
            int e = reader.ReadInt32();
            reader.BaseStream.Seek((b + s + e) * sizeof(int) + (b + s + e) * sizeof(float), SeekOrigin.Current);
        }

        public override void Save(BinaryWriter writer)
        {
            base.Save(writer);

            SampleEarlyStop.SyncToCPU(BatchSize);
            for (int i = 0; i < BatchSize; ++i) writer.Write(SampleEarlyStop.MemPtr[i]);
        }
        public override void Load(BinaryReader reader)
        {
            base.Load(reader);
            Buffer.BlockCopy(reader.ReadBytes(sizeof(int) * BatchSize), 0, SampleEarlyStop.MemPtr, 0, sizeof(int) * BatchSize);
            SampleEarlyStop.SyncFromCPU(BatchSize);
        }
        /// <summary>
        /// keep the first K items for training input, and rest sequence for forcasting.
        /// </summary>
        /// <param name="sequenceData"></param>
        /// <param name="earlyStop"></param>
        public void PushSample(List<Tuple<Dictionary<int, float>, float>> sequenceData, int earlyStopK)
        {
            int oldBatchSize = BatchSize;
            base.PushSample(sequenceData);

            SampleEarlyStop.MemPtr[oldBatchSize] = SampleIdx.MemPtr[oldBatchSize] - earlyStopK;
            SampleEarlyStop.SyncFromCPU(oldBatchSize, SampleEarlyStop.MemPtr, oldBatchSize, 1);
        }


        /// <summary>
        /// convertor.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="seqlabel"></param>
        /// <param name="sampleEarlyStop"></param>
        public SeqSparseSlotDataSource(SeqSparseBatchData data, CudaPieceFloat seqlabel, CudaPieceInt sampleEarlyStop)
        {
            Stat = data.Stat;
            SequenceData = data;
            SequenceLabel = seqlabel;
            SampleEarlyStop = sampleEarlyStop;
        }



        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (SampleIdx != null) SampleIdx.Dispose(); SampleIdx = null;
                if (SampleEarlyStop != null) SampleEarlyStop.Dispose(); SampleEarlyStop = null;
                if (SequenceIdx != null) SequenceIdx.Dispose(); SequenceIdx = null;
                if (SequenceLabel != null) SequenceLabel.Dispose(); SequenceLabel = null;
                if (FeaIdx != null) FeaIdx.Dispose(); FeaIdx = null;
                if (FeaValue != null) FeaValue.Dispose(); FeaValue = null;
            }
        }
    }
}
