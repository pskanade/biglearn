﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class IOUnitHeader
    {
        // HeadSize = sizeof(HeadSize)+sizeof(HeadVersion)+sizeof(PayloadType)+HeadExtension.Length
        public int HeadSize { get; protected set; }

        public int HeadVersion { get; protected set; }

        public DataSourceID PayloadDataType { get; set; }

        // The binary bytes of the payload when serialized to binary
        public int PayloadSize { get; set; }

        // Rest of the binary bytes in head
        public virtual byte[] HeadExtension { get; protected set; }

        public IOUnitHeader():this(DataSourceID.BatchData)
        {
        }

        public IOUnitHeader(DataSourceID dataType)
        {
            this.HeadSize = FixedHeadSize;
            this.HeadVersion = 0;
            this.PayloadDataType = dataType;
            this.PayloadSize = 0;
            this.HeadExtension = new byte[0];
        }

        public IOUnitHeader(DataSourceID dataType, int payloadSize, byte[] extension)
        {
            this.PayloadDataType = dataType;
            this.PayloadSize = payloadSize;
            this.HeadExtension = extension ?? new byte[0];
            this.HeadSize = FixedHeadSize + this.HeadExtension.Length;
            this.HeadVersion = 0;
        }

        public void Serialize(BinaryWriter output)
        {
            //this.EncodeExtension();
            this.HeadSize = FixedHeadSize + this.HeadExtension.Length;
            output.Write(this.HeadSize);
            output.Write(this.HeadVersion);
            output.Write((int)this.PayloadDataType);
            output.Write((int)this.PayloadSize);
            output.Write(this.HeadExtension);
        }

        public void Load(BinaryReader input)
        {
            int headSize = input.ReadInt32();
            int headVersion = input.ReadInt32();
            DataSourceID payloadDataType = (DataSourceID)input.ReadInt32();
            int payloadSize = input.ReadInt32();
            byte[] headExtension = new byte[headSize - FixedHeadSize];

            if (headSize > FixedHeadSize)
            {
                input.Read(headExtension, 0, headExtension.Length);
            }

            // To keep backward compitability, we didn't throw error information on new version, just keep the extension information untouched
            this.HeadSize = headSize;
            this.HeadVersion = headVersion;
            this.HeadExtension = headExtension;
            this.PayloadSize = payloadSize;
            this.PayloadDataType = payloadDataType;
        }

        public const int FixedHeadSize = 16;
    }
}
