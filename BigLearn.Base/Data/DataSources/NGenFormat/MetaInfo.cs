﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public interface IMetaInfo : IIOUnit
    {
        IEnumerable<KeyValuePair<string, string>> ToKeyValues();

        void LoadFromKeyValues(IDictionary<string, string> kvs);

        void CopyFrom(IMetaInfo otherMeta);

        void Add(IMetaInfo other);
    }
}
