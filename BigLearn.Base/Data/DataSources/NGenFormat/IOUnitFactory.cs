﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class IOUnitFactory : IDisposable
    {
        protected IMetaInfo meta;
        private bool disposed = true;

        public IOUnitFactory(IMetaInfo meta)
        {
            this.meta = meta;
        }

        public IOUnitFactory()
            : this(null)
        {
        }

        ~IOUnitFactory()
        {
            this.Dispose(false);
        }

        public virtual IMetaInfo Meta
        {
            get
            {
                return this.meta;
            }
        }

        public virtual void SetMeta(IMetaInfo meta)
        {
            this.meta = meta;
        }

        public virtual IIOUnit GetIOUnit(DataSourceID type, DeviceType deviceType)
        {
            IOUnitHeader head = new IOUnitHeader()
            {
                PayloadDataType = type
            };

            return this.GetIOUnit(head, deviceType);
        }

        public virtual IIOUnit GetIOUnit(IOUnitHeader head, DeviceType deviceType)
        {
            IIOUnit data = null;
            var freeList = this.GetFreeList(head.PayloadDataType, deviceType);
            switch (head.PayloadDataType)
            {
                case DataSourceID.SeqSparseDataSource:
                    if (freeList == null || !freeList.TryDequeue(out data))
                    {
                        data = new SeqSparseDataSource(head, this.Meta, deviceType);
                    }
                    else
                    {
                        ((SeqSparseDataSource)data).Head = head;
                    }

                    break;
                case DataSourceID.DSSMData:
                    if (freeList == null || !freeList.TryDequeue(out data))
                    {
                        data = new DSSMData(head, this.Meta, deviceType);
                    }
                    else
                    {
                        ((DSSMData)data).Head = head;
                    }
                    break;
                case DataSourceID.SequenceDataStat:
                    data = new SequenceDataStat();
                    break;
                case DataSourceID.ModelMetaInfo:
                    data = new ModelMetaInfo();
                    break;
                case DataSourceID.DSSMDataMeta:
                    data = new DSSMDataStat();
                    break;
                //case DataSourceID.CheckPointInfo:
                //    data = new CheckPointInfo();
                //    break;
                default:
                    throw new NotSupportedException(string.Format("The new data type: {0} is not supported yet.", head.PayloadDataType));
            }

            return data;
        }

        public virtual void FreeIOUnit(IIOUnit data)
        {
            if (data == null)
            {
                return;
            }

            data.Dispose();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.meta = null;
            this.disposed = true;
        }

        protected virtual ConcurrentQueue<IIOUnit> GetFreeList(DataSourceID type, DeviceType device)
        {
            return null;
        }

        public static IOUnitFactory Default = new IOUnitFactory();
    }
}
