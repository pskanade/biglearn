﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public static class DataSourceTypeBinding
    {
        private static Dictionary<DataSourceID, Type> typeMapping;

        static DataSourceTypeBinding()
        {
            typeMapping = new Dictionary<DataSourceID, Type>()
            {
                {DataSourceID.SequenceDataStat, typeof(SequenceDataStat)},
                {DataSourceID.ModelMetaInfo, typeof(ModelMetaInfo)}
            };
        }

        public static Type GetImplType(this DataSourceID dataType)
        {
            return typeMapping[dataType];
        }

        public static bool IsAssignableFrom(this DataSourceID dataType, DataSourceID other)
        {
            return typeMapping[dataType].IsAssignableFrom(typeMapping[other]);
        }
    }
}
