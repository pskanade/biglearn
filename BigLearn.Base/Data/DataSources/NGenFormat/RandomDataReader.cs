﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class RandomDataReader : IOUnitReader
    {
        private long startPosition;

        private Dictionary<long, long> ioUnitIndecies;

        private IMetaInfo meta;

        protected Random rand;

        public RandomDataReader(Stream dataStream, IOUnitFactory factroy)
            : base(dataStream, factroy)
        {
            this.startPosition = dataStream.Position;
            this.ioUnitIndecies = null;
            if (factroy.Meta == null)
            {
                // Try to get meta information from IOUnit stream             
                this.ioUnitIndecies = this.BuildIndecies(out this.meta);

                if (this.meta == null)
                {
                    throw new ArgumentException("Can't get meta information from the provided factory.");
                }

                factroy.SetMeta(this.meta);
            }
            else
            {
                this.meta = factroy.Meta;
            }

            this.rand = new Random(0);
        }

        public virtual IMetaInfo Meta
        {
            get
            {
                return this.meta;
            }
        }

        protected virtual Dictionary<long, long> IoUnitIndecies
        {
            get
            {
                if (this.ioUnitIndecies == null)
                {
                    IMetaInfo m;
                    this.ioUnitIndecies = this.BuildIndecies(out m);
                    if (m != null)
                    {
                        this.meta = m;
                    }
                }

                return this.ioUnitIndecies;
            }
        }

        /// <summary>
        /// Number of Data Unit in the stream
        /// </summary>
        public long NumberOfData
        {
            get
            {
                return this.IoUnitIndecies == null ? 0 : this.IoUnitIndecies.Count;
            }
        }

        public virtual bool CanSeek
        {
            get { return this.BaseStream.CanSeek; }
        }

        public virtual IEnumerable<IData> SequentialDatas()
        {
            while (true)
            {
                IIOUnit data = null;
                try
                {
                    data = this.Read();
                    if (!(data is IData))
                    {
                        continue;
                    }
                }
                catch (EndOfStreamException)
                {
                    break;
                }

                yield return (IData)data;
            }
        }

        public virtual IEnumerable<IData> Shuffle()
        {
            if (!this.CanSeek)
            {
                return this.SequentialDatas();
            }
            else
            {
                return this.ShuffleInternal();
            }
        }

        protected IEnumerable<IData> ShuffleInternal()
        {
            long[] indecies = this.IoUnitIndecies.Keys.ToArray();
            Util.RandomShuffle(indecies, this.rand);

            foreach (long idx in indecies)
            {
                this.SeekIdx(idx);
                IIOUnit data = this.Read();
                if (data is IData)
                {
                    yield return (IData)data;
                }
            }
        }

        public virtual void Reset()
        {
            this.BaseStream.Position = this.startPosition;
        }

        private Dictionary<long, long> BuildIndecies(out IMetaInfo metaInfo)
        {
            metaInfo = null;
            if (!this.CanSeek)
            {
                return null;
            }

            this.BaseStream.Position = this.startPosition;
            long pos = this.startPosition;
            long idx = 0;
            IOUnitHeader head = new IOUnitHeader();
            Dictionary<long, long> indecies = new Dictionary<long, long>();
            while (this.BinaryReader.BaseStream.Position < this.BinaryReader.BaseStream.Length)
            {
                indecies[idx++] = this.BinaryReader.BaseStream.Position;

                // Skip payload
                head.Load(this.BinaryReader);
                // Meta info 
                if (head.PayloadDataType > DataSourceID.MetaInfo)
                {
                    metaInfo = (IMetaInfo)this.DataFactory.GetIOUnit(head, DeviceType.CPU);
                    metaInfo.Load(this.BinaryReader);
                }
                else
                {
                    this.BinaryReader.BaseStream.Position += head.PayloadSize;
                }
            }

            return indecies;
        }

        protected virtual void SeekIdx(long index)
        {
            if (!this.CanSeek)
            {
                throw new InvalidOperationException("The base stream is not seakable");
            }

            if (index >= this.NumberOfData)
            {
                throw new ArgumentOutOfRangeException(string.Format("The input index is out of the total IOUnits int the input stream, e.g. {0}. Input: {1}", this.NumberOfData, index));
            }

            this.BinaryReader.BaseStream.Position = this.IoUnitIndecies[index];
        }

        ~RandomDataReader()
        {
            this.Dispose(false);
        }

        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            
            this.disposed = true;

            if (this.ioUnitIndecies != null)
            {
                this.ioUnitIndecies.Clear();
                this.ioUnitIndecies = null;
            }

            this.meta = null;
            this.rand = null;

            base.Dispose(disposing);
        }
    }
}
