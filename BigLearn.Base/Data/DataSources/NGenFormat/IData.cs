﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public interface IData : IIOUnit
    {
        IMetaInfo Meta { get; }
        void CopyFrom(IData other);
        IData CloneAs(DeviceType deviceType);
        void SyncFromCPU();
        void SyncToCPU();
    }
}
