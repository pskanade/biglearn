﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{

    public class DenseDataStat : DataStat
    {
        public int MAX_BATCHSIZE { get; set; }
        public int Dim { get; set; }

        public DenseDataStat()
        {
            MAX_BATCHSIZE = 0;
            this.TotalBatchNumber = 0;
            this.TotalSampleNumber = 0;
            Dim = 0;
        }

        public override DataSourceID Type { get { return DataSourceID.DenseDataMeta; } }

        public override void Save(BinaryWriter writer)
        {
            writer.Write(Dim);
            writer.Write(MAX_BATCHSIZE);
            writer.Write(this.TotalBatchNumber);
            writer.Write(this.TotalSampleNumber);
        }

        public override void Load(BinaryReader reader)
        {
            Dim = reader.ReadInt32();
            MAX_BATCHSIZE = reader.ReadInt32();
            this.TotalBatchNumber = reader.ReadInt32();
            this.TotalSampleNumber = reader.ReadInt32();
        }

        public override void Init(BinaryReader reader)
        {
            reader.BaseStream.Seek(-4 * sizeof(Int32), SeekOrigin.End);
            this.Load(reader);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            TextWriter tw = new StringWriter(sb);
            foreach (var kv in this.ToKeyValues())
            {
                tw.WriteLine("{0}\t{1}", kv.Key, kv.Value);
            }

            tw.Flush();
            return sb.ToString();
        }

        public override IEnumerable<KeyValuePair<string, string>> ToKeyValues()
        {
            // Keep order
            yield return new KeyValuePair<string, string>("TYPE", this.Type.ToString());
            yield return new KeyValuePair<string, string>("FEATURE_DIM", this.Dim.ToString());
            yield return new KeyValuePair<string, string>("MAX_BATCHSIZE", this.MAX_BATCHSIZE.ToString());
            yield return new KeyValuePair<string, string>("TOTAL_BATCHNUM", this.TotalBatchNumber.ToString());
            yield return new KeyValuePair<string, string>("TOTAL_SAMPLENUM", this.TotalSampleNumber.ToString());
        }

        public override void LoadFromKeyValues(IDictionary<string, string> kvs)
        {
            this.Dim = int.Parse(kvs["FEATURE_DIM"]);
            this.MAX_BATCHSIZE = int.Parse(kvs["MAX_BATCHSIZE"]);
            this.TotalBatchNumber = int.Parse(kvs["TOTAL_BATCHNUM"]);
            this.TotalSampleNumber = int.Parse(kvs["TOTAL_SAMPLENUM"]);
        }

        public override void CopyFrom(IMetaInfo otherMeta)
        {
            DenseDataStat other = (DenseDataStat)otherMeta;
            this.MAX_BATCHSIZE = other.MAX_BATCHSIZE;
            this.TotalBatchNumber = other.TotalBatchNumber;
            this.TotalSampleNumber = other.TotalSampleNumber;
            this.Dim = other.Dim;
        }

        public override void Add(IMetaInfo meta)
        {
            DenseDataStat other = (DenseDataStat)meta;
            if (other.MAX_BATCHSIZE > this.MAX_BATCHSIZE)
            {
                this.MAX_BATCHSIZE = other.MAX_BATCHSIZE;
            }

            if (other.Dim > this.Dim)
            {
                this.Dim = other.Dim;
            }

            this.TotalBatchNumber += other.TotalBatchNumber;
            this.TotalSampleNumber += other.TotalSampleNumber;
        }
    }

    public class DenseBatchData : BatchData<DenseDataStat>
    {
        public override int BatchSize
        {
            get { return Data.EffectiveSize == 0 ? 0 : Data.EffectiveSize / Stat.Dim; }
            set { Data.EffectiveSize = value * Stat.Dim; if (Stat.MAX_BATCHSIZE < value) Stat.MAX_BATCHSIZE = value;  }
        }

        #region main data region.
        public CudaPieceFloat Data;
        #endregion.

        public DenseBatchData() { }
        public DenseBatchData(DenseDataStat stat, DeviceType device) : base(stat, device) { }
        public override void Init(DenseDataStat stat, DeviceType deviceType)
        {
            base.Init(stat, deviceType);
            if(Data == null) Data = new CudaPieceFloat(Stat.MAX_BATCHSIZE * Stat.Dim, deviceType);
        }

        public DenseBatchData(int maxBatchSize, int dim, DeviceType device) : this(new DenseDataStat() { Dim = dim, MAX_BATCHSIZE = maxBatchSize }, device)
        { }

        
        public DenseBatchData(IOUnitHeader head, DenseDataStat stat, DeviceType device): this(stat, device)
        {
            this.Head = head;
        }
        
        public DenseBatchData(DenseDataStat stat, CudaPieceFloat data, DeviceType device)
        {
            Data = data;
            Init(stat, device);
        }

        public override DataSourceID Type { get { return DataSourceID.DenseBatchData; } }

        public override void Load(BinaryReader reader)
        {
            int batchSize = reader.ReadInt32();
            this.BatchSize = batchSize;

            Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * batchSize * Stat.Dim), 0, Data.MemPtr, 0, sizeof(float) * batchSize * Stat.Dim);
            Data.SyncFromCPU(batchSize * Stat.Dim);
        }

        public override void Save(BinaryWriter writer)
        {
            if (BatchSize > 0)
            {
                writer.Write(BatchSize);
                Data.SyncToCPU(BatchSize * Stat.Dim);
                for (int i = 0; i < BatchSize * Stat.Dim; ++i) writer.Write(Data.MemPtr[i]);
            }
        }

        public override void SyncFromCPU()
        {
            Data.SyncFromCPU(BatchSize * Stat.Dim);
        }

        public override void SyncToCPU()
        {
            Data.SyncToCPU(BatchSize * Stat.Dim);
        }


        public override void Clear()
        {
            BatchSize = 0;
        }

        public override void CopyFrom(IData other)
        {
            if (Data != null && ((DenseBatchData)other).Data != null && ((DenseBatchData)other).BatchSize > 0)
            {
                BatchSize = ((DenseBatchData)other).BatchSize;
                Data.CopyFrom(((DenseBatchData)other).Data);
            }
        }

        public override IData CloneAs(DeviceType deviceType)
        {
            DenseBatchData ss = new DenseBatchData(this.Head, this.Stat, deviceType);
            ss.CopyFrom(this);
            return ss;
        }

        public void PushSample(float[] value, int batchNum)
        {
            int oldBatchSize = BatchSize;
            BatchSize += batchNum;
            Data.SyncFromCPU(oldBatchSize * Stat.Dim, value, 0, batchNum * Stat.Dim);
        }

        public float[] UnrollAsSampleList()
        {
            float[] values = new float[this.BatchSize];
            Array.Copy(Data.MemPtr, values, this.BatchSize);
            return values;
        }


        /// <summary>
        /// Do not change the function Name.
        /// </summary>
        /// <param name="reader"></param>
        public override void SkipBlock(BinaryReader reader, DataStat stat)
        {
            int b = reader.ReadInt32();
            reader.BaseStream.Seek((b * ((DenseDataStat)stat).Dim) * sizeof(float), SeekOrigin.Current);
        }

        #region Dispose Function is not necessary.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            this.disposed = true;

            if (disposing)
            {
                if (Data != null)
                {
                    Data.Dispose();
                }
            }

            Data = null;

            base.Dispose(disposing);
        }

        ~DenseBatchData()
        {
            this.Dispose(false);
        }
        #endregion.
    }
}
