﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class BiMatchBatchDataStat : DataStat
    {
        public int MAX_SRC_BATCHSIZE = 0;
        public int MAX_TGT_BATCHSIZE = 0;
        public int MAX_MATCH_BATCHSIZE = 0;
        public BiMatchBatchDataStat()
        { }

        public override void Save(BinaryWriter writer)
        {
            writer.Write(MAX_SRC_BATCHSIZE);
            writer.Write(MAX_TGT_BATCHSIZE);
            writer.Write(MAX_MATCH_BATCHSIZE);
            writer.Write(this.TotalBatchNumber);
            writer.Write(this.TotalSampleNumber);
        }

        public override void Init(BinaryReader reader)
        {
            reader.BaseStream.Seek(-5 * sizeof(Int32), SeekOrigin.End);
            MAX_SRC_BATCHSIZE = reader.ReadInt32();
            MAX_TGT_BATCHSIZE = reader.ReadInt32();
            MAX_MATCH_BATCHSIZE = reader.ReadInt32();
            this.TotalBatchNumber = reader.ReadInt32();
            this.TotalSampleNumber = reader.ReadInt32();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            TextWriter tw = new StringWriter(sb);
            foreach (var kv in this.ToKeyValues())
            {
                tw.WriteLine("{0}\t{1}", kv.Key, kv.Value);
            }

            tw.Flush();
            return sb.ToString();
        }

        public override IEnumerable<KeyValuePair<string, string>> ToKeyValues()
        {
            // Keep order
            yield return new KeyValuePair<string, string>("MAX_SRC_BATCHSIZE", this.MAX_SRC_BATCHSIZE.ToString());
            yield return new KeyValuePair<string, string>("MAX_TGT_BATCHSIZE", this.MAX_TGT_BATCHSIZE.ToString());
            yield return new KeyValuePair<string, string>("MAX_MATCH_BATCHSIZE", this.MAX_MATCH_BATCHSIZE.ToString());
            yield return new KeyValuePair<string, string>("TOTAL_BATCHNUM", this.TotalBatchNumber.ToString());
            yield return new KeyValuePair<string, string>("TOTAL_SAMPLENUM", this.TotalSampleNumber.ToString());
        }

    }

    public class BiMatchBatchData : BatchData<BiMatchBatchDataStat>
    {
        public int SrcSize
        {
            get { return Src2MatchIdx.EffectiveSize;}
            set
            {
                if (Src2MatchIdx != null) Src2MatchIdx.EffectiveSize = value;
                if (Src2Idx != null) Src2Idx.EffectiveSize = value;
                if (Stat.MAX_SRC_BATCHSIZE < value) Stat.MAX_SRC_BATCHSIZE = value;
            }
        }
        public int TgtSize
        {
            get { return Tgt2MatchIdx.EffectiveSize; }
            set
            {
                if (Tgt2MatchIdx != null) Tgt2MatchIdx.EffectiveSize = value;
                if (Tgt2Idx != null) Tgt2Idx.EffectiveSize = value;
                if (Stat.MAX_TGT_BATCHSIZE < value) Stat.MAX_TGT_BATCHSIZE = value;
            }
        }
        public int MatchSize { get { return BatchSize; }set { BatchSize = value; } }
        public override int BatchSize
        {
            get
            {
                return SrcIdx.EffectiveSize;
            }
            set
            {
                SrcIdx.EffectiveSize = value;
                TgtIdx.EffectiveSize = value;
                MatchInfo.EffectiveSize = value;
                if (Src2MatchElement != null) Src2MatchElement.EffectiveSize = value;
                if (Tgt2MatchElement != null) Tgt2MatchElement.EffectiveSize = value;
                if (Stat.MAX_MATCH_BATCHSIZE < value) Stat.MAX_MATCH_BATCHSIZE = value;
            }
        }

        public CudaPieceInt SrcIdx;
        public CudaPieceInt TgtIdx;
        public CudaPieceFloat MatchInfo;

        /// <summary>
        /// Src and Tgt Match Info.
        /// </summary>
        public CudaPieceInt Src2Idx = null;
        public CudaPieceInt Src2MatchIdx = null;
        public CudaPieceInt Src2MatchElement = null;

        public CudaPieceInt Tgt2Idx = null;
        public CudaPieceInt Tgt2MatchIdx = null;
        public CudaPieceInt Tgt2MatchElement = null;

        public int Dim = 1;

        public BiMatchBatchData() { }

        public BiMatchBatchData(BiMatchBatchDataStat stat, DeviceType deviceType) : base(stat, deviceType)
        { }

        public override void Init(BiMatchBatchDataStat stat, DeviceType deviceType)
        {
            base.Init(stat, deviceType);
            SrcIdx = new CudaPieceInt(Stat.MAX_MATCH_BATCHSIZE, true, deviceType == DeviceType.GPU);
            TgtIdx = new CudaPieceInt(Stat.MAX_MATCH_BATCHSIZE, true, deviceType == DeviceType.GPU);
            MatchInfo = new CudaPieceFloat(Stat.MAX_MATCH_BATCHSIZE, true, deviceType == DeviceType.GPU);

            Src2Idx = new CudaPieceInt(Stat.MAX_SRC_BATCHSIZE, true, deviceType == DeviceType.GPU);
            Src2MatchIdx = new CudaPieceInt(Stat.MAX_SRC_BATCHSIZE, true, deviceType == DeviceType.GPU);
            Src2MatchElement = new CudaPieceInt(Stat.MAX_MATCH_BATCHSIZE, true, deviceType == DeviceType.GPU);

            Tgt2Idx = new CudaPieceInt(Stat.MAX_TGT_BATCHSIZE, true, deviceType == DeviceType.GPU);
            Tgt2MatchIdx = new CudaPieceInt(Stat.MAX_TGT_BATCHSIZE, true, deviceType == DeviceType.GPU);
            Tgt2MatchElement = new CudaPieceInt(Stat.MAX_MATCH_BATCHSIZE, true, deviceType == DeviceType.GPU);
        }

        public override void Load(BinaryReader reader)
        {
            SrcSize = reader.ReadInt32();
            TgtSize = reader.ReadInt32();
            MatchSize = reader.ReadInt32();

            Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * MatchSize), 0, SrcIdx.MemPtr, 0, sizeof(Int32) * MatchSize);
            Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * MatchSize), 0, TgtIdx.MemPtr, 0, sizeof(Int32) * MatchSize);
            Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * MatchSize), 0, MatchInfo.MemPtr, 0, sizeof(float) * MatchSize);

            SrcSize = Util.InverseMatchIdx(SrcIdx.MemPtr, MatchSize, Src2Idx.MemPtr, Src2MatchIdx.MemPtr, Src2MatchElement.MemPtr);
            TgtSize = Util.InverseMatchIdx(TgtIdx.MemPtr, MatchSize, Tgt2Idx.MemPtr, Tgt2MatchIdx.MemPtr, Tgt2MatchElement.MemPtr);

            SyncFromCPU();
        }

        public override void SyncFromCPU()
        {
            SrcIdx.SyncFromCPU(MatchSize);
            TgtIdx.SyncFromCPU(MatchSize);
            MatchInfo.SyncFromCPU(MatchSize);

            Src2Idx.SyncFromCPU(SrcSize);
            Src2MatchIdx.SyncFromCPU(SrcSize);
            Src2MatchElement.SyncFromCPU(MatchSize);

            Tgt2Idx.SyncFromCPU(TgtSize);
            Tgt2MatchIdx.SyncFromCPU(TgtSize);
            Tgt2MatchElement.SyncFromCPU(MatchSize);
        }

        public override void SyncToCPU()
        {
            SrcIdx.SyncToCPU(MatchSize);
            TgtIdx.SyncToCPU(MatchSize);
            MatchInfo.SyncToCPU(MatchSize);

            Src2Idx.SyncToCPU(SrcSize);
            Src2MatchIdx.SyncToCPU(SrcSize);
            Src2MatchElement.SyncToCPU(MatchSize);

            Tgt2Idx.SyncToCPU(TgtSize);
            Tgt2MatchIdx.SyncToCPU(TgtSize);
            Tgt2MatchElement.SyncToCPU(MatchSize);
        }

        public override void CopyFrom(IData other)
        {
            BiMatchBatchData block = (BiMatchBatchData)other;
            SrcSize = block.SrcSize;
            TgtSize = block.TgtSize;
            MatchSize = block.MatchSize;

            SrcIdx.CopyFrom(block.SrcIdx);
            TgtIdx.CopyFrom(block.TgtIdx);
            MatchInfo.CopyFrom(block.MatchInfo);

            Src2Idx.CopyFrom(block.Src2Idx);
            Src2MatchIdx.CopyFrom(block.Src2MatchIdx);
            Src2MatchElement.CopyFrom(block.Src2MatchElement);

            Tgt2Idx.CopyFrom(block.Tgt2Idx);
            Tgt2MatchIdx.CopyFrom(block.Tgt2MatchIdx);
            Tgt2MatchElement.CopyFrom(block.Tgt2MatchElement);
        }

        /// <summary>
        /// Do not change the function Name.
        /// </summary>
        /// <param name="reader"></param>
        public override void SkipBlock(BinaryReader reader, DataStat stat)
        {
            int s = reader.ReadInt32();
            int t = reader.ReadInt32();
            int m = reader.ReadInt32();
            reader.BaseStream.Seek((2 * m) * sizeof(int) + (m) * sizeof(float), SeekOrigin.Current);
        }

        /// <summary>
        /// Please Call SaveAndRefresh to save the binary MiniBatch 2 Disk Stream.
        /// </summary>
        /// <param name="writer"></param>
        public override void Save(BinaryWriter writer)
        {
            if (MatchSize > 0)
            {
                writer.Write(SrcSize);
                writer.Write(TgtSize);
                writer.Write(MatchSize);

                SrcIdx.SyncToCPU(MatchSize);
                TgtIdx.SyncToCPU(MatchSize);
                MatchInfo.SyncToCPU(MatchSize);
                for (int i = 0; i < MatchSize; i++) writer.Write(SrcIdx.MemPtr[i]);
                for (int i = 0; i < MatchSize; i++) writer.Write(TgtIdx.MemPtr[i]);
                for (int i = 0; i < MatchSize; i++) writer.Write(MatchInfo.MemPtr[i]);
            }           
        }

        public override void Clear()
        {
            SrcSize = 0;
            TgtSize = 0;
            MatchSize = 0;
        }

        public void Push(int src, int tgt, float v)
        {
            int oldMatchSize = MatchSize;
            //Logger.WriteLog("add match {0}", oldMatchSize);
            MatchSize = oldMatchSize + 1;
            SrcIdx.MemPtr[oldMatchSize] = src;
            TgtIdx.MemPtr[oldMatchSize] = tgt;
            MatchInfo.MemPtr[oldMatchSize] = v;

            if (src >= SrcSize) {SrcSize = src + 1; } //Logger.WriteLog("add source {0}", SrcSize); }
            if (tgt >= TgtSize) {TgtSize = tgt + 1; } //Logger.WriteLog("add target {0}", TgtSize); }
        }

        public void PushDone()
        {
            if (MatchSize > 0)
            {
                SrcSize = InverseMatchIdx(SrcIdx.MemPtr, MatchSize, SrcSize, Src2Idx.MemPtr, Src2MatchIdx.MemPtr, Src2MatchElement.MemPtr);
                TgtSize = InverseMatchIdx(TgtIdx.MemPtr, MatchSize, TgtSize, Tgt2Idx.MemPtr, Tgt2MatchIdx.MemPtr, Tgt2MatchElement.MemPtr);
            }
            SyncFromCPU();
        }

        public void SetMatch(IEnumerable<Tuple<int, int, float>> matchData)
        {
            Clear();
            PushMatch(matchData);
        }

        public static int InverseMatchIdx(int[] matchIdx, int matchNum, int maxIdx, int[] inverseIdx, int[] inverseSmpIdx, int[] inverseElementIdx)
        {
            Array.Clear(inverseIdx, 0, maxIdx);
            for (int i = 0; i < matchNum; i++)
            {
                inverseIdx[matchIdx[i]] += 1;
            }

            int element = 0;
            for(int i = 0; i < maxIdx; i++)
            {
                int num = inverseIdx[i];
                inverseSmpIdx[i] = element;
                element = element + num;
            }

            for(int i = 0; i < matchNum; i++)
            {
                int s = inverseSmpIdx[matchIdx[i]];
                inverseElementIdx[s] = i;
                inverseSmpIdx[matchIdx[i]] += 1;
            }

            element = 0;
            int idx = 0;
            for(int i = 0; i < maxIdx; i++)
            {
                if(inverseIdx[i] > 0)
                {
                    inverseSmpIdx[idx] = element + inverseIdx[i];
                    inverseIdx[idx] = i;
                    
                    element = inverseSmpIdx[idx];
                    idx += 1;
                }
            }
            return idx;
        }



        public void PushMatch(IEnumerable<Tuple<int, int, float>> matchData)
        {
            foreach(Tuple<int,int,float> item in matchData)
            {
                int oldMatchSize = MatchSize;
                MatchSize = oldMatchSize + 1;
                SrcIdx.MemPtr[oldMatchSize] = item.Item1;
                TgtIdx.MemPtr[oldMatchSize] = item.Item2;
                MatchInfo.MemPtr[oldMatchSize] = item.Item3;

                if (item.Item1 >= SrcSize) SrcSize = item.Item1 + 1;
                if (item.Item2 >= TgtSize) TgtSize = item.Item2 + 1;
            }
            if (MatchSize > 0)
            {
                SrcSize = InverseMatchIdx(SrcIdx.MemPtr, MatchSize, SrcSize, Src2Idx.MemPtr, Src2MatchIdx.MemPtr, Src2MatchElement.MemPtr);
                TgtSize = InverseMatchIdx(TgtIdx.MemPtr, MatchSize, TgtSize, Tgt2Idx.MemPtr, Tgt2MatchIdx.MemPtr, Tgt2MatchElement.MemPtr);
            }
            SyncFromCPU();
        }

    }
}
