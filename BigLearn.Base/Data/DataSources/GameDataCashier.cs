﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Define the Data Type for Game.
    /// </summary>
    public class GameEnvironment
    {
        const int BoundaryX = 20;
        const int BoundaryY = 20;

        /// <summary>
        ///  0: empty; 1 : green thing; 2 : red thing; 3 : wall; 4 + K: other types of agent;
        /// </summary>
        public byte[] Map = new byte[BoundaryX*BoundaryY];

        public class ColorPoint
        {
            public int X;
            public int Y;
            public int Type;
            public int Life;

            public void Passing() { Life = Life - 1; }
        }
        public List<ColorPoint> RegPoints = new List<ColorPoint>();
        public int PointLife = 200;
        
        public void RegPoint(int x, int y, byte type)
        {
            RegPoints.Add(new ColorPoint() { X = x, Y = y, Type = type, Life = PointLife + EnvirRandom.Next(PointLife) });
            Map[x * BoundaryY + y] = type;
            if (type == 1) GreenNum += 1;
            if (type == 2) RedNum += 1;
        }

        public void RegAgent(int x, int y, byte type)
        {
            AgentX.Add(x);
            AgentY.Add(y);
            AgentType.Add(type);
            Map[x * BoundaryY + y] = (byte)(4 + type);
        }

        public void RemovePoint(int idx)
        {
            Map[RegPoints[idx].X * BoundaryY + RegPoints[idx].Y] = 0;
            if (RegPoints[idx].Type == 1) GreenNum -= 1;
            if (RegPoints[idx].Type == 2) RedNum -= 1;
            RegPoints.RemoveAt(idx);
        }

        public void EatPoint(int x, int y)
        {
            for (int i = RegPoints.Count - 1; i >= 0; i--)
                if (RegPoints[i].X == x && RegPoints[i].Y == y) { RemovePoint(i); break; }
        }
        
        public void PointPassing()
        {
            for (int i = 0; i < RegPoints.Count; i++) RegPoints[i].Passing();
            for (int i = RegPoints.Count - 1; i >= 0; i--)
                if (RegPoints[i].Life <= 0) RemovePoint(i);
        }

        public int MaxGreenNum = 100;
        public int MaxRedNum = 100;

        public float GreenReward = -0.6f;
        public float RedReward = 0.8f;

        public int GreenNum = 0;
        public int RedNum = 0;

        public int AgentTypeCount { get; private set; }
        public List<int> AgentX = new List<int>();
        public List<int> AgentY = new List<int>();
        public List<int> AgentType = new List<int>();

        public string MapFile = string.Empty;
        public BinaryWriter MapStream = null;

        public Random EnvirRandom = new Random(13);

        /// <summary>
        /// Build Game Enviroument.
        /// </summary>
        public GameEnvironment(string mapFile) : this(mapFile, 1)
        { }

        public GameEnvironment(string mapFile, int agentNum)
            : this(mapFile, new List<int>() { agentNum})
        { }
        
        public GameEnvironment(string mapFile, List<int> agentNumList)
        {
            for (int i = 0; i < 4; i++)
            {
                Map[4 * BoundaryY + 4 + i] = 3;
                Map[(4 + i) * BoundaryY + 4] = 3;

                Map[(12 + i) * BoundaryY + 4] = 3;
                Map[15 * BoundaryY + 4 + i] = 3;

                Map[4 * BoundaryY + 12 + i] = 3;
                Map[(4 + i) * BoundaryY + 15] = 3;

                Map[(12 + i) * BoundaryY + 15] = 3;
                Map[15 * BoundaryY + 12 + i] = 3;
            }

            /// initial Agent 
            AgentTypeCount = agentNumList.Count;
            for (int t = 0; t < AgentTypeCount; t++)
            {
                for (int i = 0; i < agentNumList[t]; i++) SampleAgentType((byte)t);
            }

            ///initial Green and Red Points;
            for (int i = 0; i < MaxGreenNum; i++) SamplePointType(1);
            for (int i = 0; i < MaxRedNum; i++) SamplePointType(2);

            MapFile = mapFile;
            MapStream = new BinaryWriter(new FileStream(MapFile, FileMode.Create, FileAccess.Write));
        }

        public void CloseReplayStream()
        {
            MapStream.Close();
        }
        void SamplePointType(byte type)
        {
            int x = EnvirRandom.Next(BoundaryX);
            int y = EnvirRandom.Next(BoundaryY);
            if (Map[x * BoundaryY + y] == 0) RegPoint(x, y, type);
        }

        void SampleAgentType(byte type)
        {
            int x = 0;
            int y = 0;
            do
            {
                x = EnvirRandom.Next(BoundaryX);
                y = EnvirRandom.Next(BoundaryY);
            } while (Map[x * BoundaryY + y] != 0);
            RegAgent(x, y, type);
            //if () RegPoint(x, y, type);
        }

        bool IsWall(int x, int y)
        {
            if (x < 0 || y < 0 || x >= BoundaryX || y >= BoundaryY || Map[x * BoundaryY + y] == 3) return true;
            else return false;
        }

        bool IsCanStepIn(int x, int y)
        {
            if (IsWall(x, y)) return false;
            else if (Map[x * BoundaryY + y] >= 4) return false;
            return true;
        }

        /// <summary>
        /// direction 0 : up; 1 : down; 2 : left; 3 : right;
        /// </summary>
        /// <param name="direction"></param>
        /// <returns> Reward; Touch Red : RedReward; Touch Green : GreenReward; else : 0 </returns>
        public float AgentAction(int direction, int agentId)
        {
            Map[AgentX[agentId] * BoundaryY + AgentY[agentId]] = 0;
            switch (direction)
            {
                case 0:
                    if (IsCanStepIn(AgentX[agentId], AgentY[agentId] - 1)) { AgentY[agentId] = AgentY[agentId] - 1; }
                    break;
                case 1:
                    if (IsCanStepIn(AgentX[agentId], AgentY[agentId] + 1)) { AgentY[agentId] = AgentY[agentId] + 1; }
                    break;
                case 2:
                    if (IsCanStepIn(AgentX[agentId] - 1, AgentY[agentId])) { AgentX[agentId] = AgentX[agentId] - 1; }
                    break;
                case 3:
                    if (IsCanStepIn(AgentX[agentId] + 1, AgentY[agentId])) { AgentX[agentId] = AgentX[agentId] + 1; }
                    break;
            }            
            float reward = 0;
            if (Map[AgentX[agentId] * BoundaryY + AgentY[agentId]] == 1) { reward = GreenReward; EatPoint(AgentX[agentId], AgentY[agentId]); }
            if (Map[AgentX[agentId] * BoundaryY + AgentY[agentId]] == 2) { reward = RedReward; EatPoint(AgentX[agentId], AgentY[agentId]); }
            Map[AgentX[agentId] * BoundaryY + AgentY[agentId]] = (byte)(4 + AgentType[agentId]);

            PointPassing();

            if (MaxGreenNum - GreenNum >= MaxRedNum - RedNum)
            {
                if (GreenNum < MaxGreenNum) SamplePointType(1);
            }
            else
                if (RedNum < MaxRedNum) SamplePointType(2);
            MapStream.Write(Map);
            return reward;
        }
        const int SenseRange = 5;
        public int MapType { get { return 4 + AgentTypeCount; } }
        public int SenseFeatureDim { get { return ((2 * SenseRange + 1) * (2 * SenseRange + 1) - 1) * MapType; } }
        public int ActionDim { get { return 4; } }
        public HashSet<int> SenseEnviroument(int agentId)
        {
            HashSet<int> status = new HashSet<int>();

            // ((2 * SenseRange + 1) * (2 * SenseRange + 1) - 1) * MapType feature set;
            int idx = 0;
            for (int i = -SenseRange; i <= SenseRange; i++)
            {
                for (int j = -SenseRange; j <= SenseRange; j++)
                {
                    if (i == 0 && j == 0) continue;
                    int senseX = AgentX[agentId] + i;
                    int senseY = AgentY[agentId] + j;
                    if (IsWall(senseX, senseY)) status.Add(idx + 3);
                    else status.Add(idx + Map[senseX * BoundaryY + senseY]);
                    idx += MapType;
                }
            }
            return status;
        }
    }

    public class ReplayBatchData : BatchData
    {
        public GeneralBatchInputData Source;
        public GeneralBatchInputData Target;
        public DenseBatchData Action;
        public DenseBatchData Reward;
        public ReplayBatchData(int feaDim, int maxBatchSize, DeviceType device)
        {
            Source = new GeneralBatchInputData(feaDim, maxBatchSize, FeatureDataType.SparseFeature, device);
            Target = new GeneralBatchInputData(feaDim, maxBatchSize, FeatureDataType.SparseFeature, device);
            Reward = new DenseBatchData(maxBatchSize, 1, device);
            Action = new DenseBatchData(maxBatchSize, 1, device);
        }

        public override void CopyFrom(IData other)
        {
            ReplayBatchData data = (ReplayBatchData)other;
            Source.CopyFrom(data.Source);
            Target.CopyFrom(data.Target);
            Reward.CopyFrom(data.Reward);
            Action.CopyFrom(data.Action);
        }

    }

    /// <summary>
    /// A Game Simulator as Data Cashier.
    /// </summary>
    public class GameDataCashier 
    {
        public GameEnvironment Envirou { get; set; }

        public int AgentId = 0;
        
        public GameDataCashier(GameEnvironment enviroument)
        {
            Envirou = enviroument;
        }

        public GameDataCashier(GameEnvironment enviroument, int agentId)
        {
            Envirou = enviroument;
            AgentId = agentId;
        }

        const int ReplayMemorySize = 1023;

        public int TemporalMemory = 3;

        public int StatusFeatureDim { get { return Envirou.SenseFeatureDim * (TemporalMemory + 1) + Envirou.ActionDim * TemporalMemory; } }

        public Queue<Tuple<HashSet<int>, int, float>> ReplayMemory = new Queue<Tuple<HashSet<int>, int, float>>(ReplayMemorySize + 1);

        public HashSet<int> SenseHash { get; set; }

        void GetMemoryFeature(HashSet<int> hash, int memoryIdx, GeneralBatchInputData data)
        {
            int fid = data.ElementSize;

            foreach (int idx in hash) data.FeatureIdx.MemPtr[data.ElementSize++] = idx;

            int featureBias = Envirou.SenseFeatureDim;
            for (int t = 0; t < TemporalMemory; t++)
            {
                if (memoryIdx - t < 0) break;
 
                foreach (int idx in ReplayMemory.ElementAt(memoryIdx - t).Item1) data.FeatureIdx.MemPtr[data.ElementSize++] = idx + featureBias;
                featureBias += Envirou.SenseFeatureDim;

                data.FeatureIdx.MemPtr[data.ElementSize++] = ReplayMemory.ElementAt(memoryIdx - t).Item2 + featureBias;
                featureBias += Envirou.ActionDim;
            }
            for (int i = fid; i < data.ElementSize; i++) data.FeatureValue.MemPtr[i] = 1;
            data.BatchIdx.MemPtr[data.BatchSize++] = data.ElementSize;
        }

        public void GetStatusFeature(GeneralBatchInputData data)
        {
            SenseHash = Envirou.SenseEnviroument(AgentId);
            data.Clear();
            GetMemoryFeature(SenseHash, ReplayMemory.Count - 1, data);
            data.SyncFromCPU();
        }

        public float SetStatusAction(int direction)
        {
            float reward = Envirou.AgentAction(direction, AgentId);
            PushReplayMemory(SenseHash, direction, reward);
            return reward;
        }

        public void PushReplayMemory(HashSet<int> status, int action, float reward)
        {
            ReplayMemory.Enqueue(new Tuple<HashSet<int>, int, float>(status, action, reward));
            if (ReplayMemory.Count >= ReplayMemorySize) ReplayMemory.Dequeue();
        }

        /// <summary>
        /// Sample MiniBatch from ReplayMemory.
        /// </summary>
        public void SampleMiniBatch(int miniBatchSize, ReplayBatchData data)
        {
            data.Source.Clear();
            data.Target.Clear();

            int availableReplaySize = ReplayMemory.Count - TemporalMemory - 1;
            int batchSize = Math.Min(miniBatchSize, availableReplaySize);
            if (batchSize <= 0) return;
            int[] RandomBatchIdx = Enumerable.Range(0, availableReplaySize).ToArray();
            int batIdx = 0;
            while (batIdx < batchSize)
            {
                int randomPos = Util.URandom.Next(batIdx, availableReplaySize);
                int id = RandomBatchIdx[randomPos];
                RandomBatchIdx[randomPos] = RandomBatchIdx[batIdx];
                RandomBatchIdx[batIdx] = id;

                GetMemoryFeature(ReplayMemory.ElementAt(id + TemporalMemory).Item1, id + TemporalMemory - 1, data.Source);
                GetMemoryFeature(ReplayMemory.ElementAt(id + TemporalMemory + 1).Item1, id + TemporalMemory, data.Target);
                data.Reward.Data.MemPtr[batIdx] = ReplayMemory.ElementAt(id + TemporalMemory).Item3;
                data.Action.Data.MemPtr[batIdx] = ReplayMemory.ElementAt(id + TemporalMemory).Item2;
                batIdx++;
            }
            data.Reward.BatchSize = batIdx;
            data.Action.BatchSize = batIdx;

            data.Reward.Data.SyncFromCPU(batIdx);
            data.Action.Data.SyncFromCPU(batIdx);
            data.Source.BatchIdx.SyncFromCPU(data.Source.BatchSize);

        }
    }


    /// <summary>
    /// Sense Enviroument.
    /// </summary>
    public class SenseRunner : StructRunner
    {
        GameDataCashier Interface { get; set; }

        public new GeneralBatchInputData Output { get { return (GeneralBatchInputData)base.Output; } protected set { base.Output = value; } }

        public SenseRunner(GameDataCashier gameInterface, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Interface = gameInterface;
            Output = new GeneralBatchInputData(Interface.StatusFeatureDim, 1, FeatureDataType.SparseFeature, Behavior.Device);
        }

        public override void Forward()
        {
            Interface.GetStatusFeature(Output);
        }
    }

    public class ActionRunner : StructRunner
    {
        GameDataCashier Interface { get;  set; }

        HiddenBatchData ActionData { get; set; }
        List<Tuple<int, float>> ScheduleEpsilon = null;

        float epsilon = 1;
        float epsilon_min = 0.1f;
        int step = 0;
        float avgReward = 0;
        int step_threshold = 100000;
        Random mRandom = new Random(13);

        int[] DirectStat = new int[4];

        public ActionRunner(GameDataCashier gameInterface, HiddenBatchData action, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Interface = gameInterface;
            ActionData = action;
        }

        public ActionRunner(GameDataCashier gameInterface, HiddenBatchData action, List<Tuple<int, float>> scheduleEpsilon, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Interface = gameInterface;
            ActionData = action;
            ScheduleEpsilon = scheduleEpsilon;
        }

        public float Epsilon(int step)
        {
            if(ScheduleEpsilon == null)
            {
                if (step < step_threshold) epsilon = epsilon - (1 - epsilon_min) / (step_threshold + 1);
                epsilon = Math.Max(epsilon, epsilon_min);
                return epsilon;
            }
            for (int i = 0; i < ScheduleEpsilon.Count; i++)
            {
                if (step < ScheduleEpsilon[i].Item1)
                {
                    float lambda = (step - ScheduleEpsilon[i - 1].Item1) * 1.0f / (ScheduleEpsilon[i].Item1 - ScheduleEpsilon[i - 1].Item1);
                    return lambda * ScheduleEpsilon[i].Item2 + (1 - lambda) * ScheduleEpsilon[i - 1].Item2;
                }
            }
            return ScheduleEpsilon.Last().Item2;
        }

        public override void Forward()
        {
            epsilon = Epsilon(step);

            int direction = -1;
            if (mRandom.NextDouble() <= epsilon)
            {
                direction = mRandom.Next(Interface.Envirou.ActionDim);
            }
            else
            {
                ActionData.Output.Data.CopyOutFromCuda();
                direction = Util.MaximumValue(ActionData.Output.Data.MemPtr);
            }
            avgReward = step * 1.0f / (step + 1) * avgReward + 1.0f / (step + 1) * Interface.SetStatusAction(direction);

            step = step + 1;

            DirectStat[direction] += 1;
            if (step % 500 == 0)
            {
                Logger.WriteLog("Agent\t{0}\t Avg Reward\t{1}, Step\t{2}", Interface.AgentId, avgReward, step);
                Logger.WriteLog("Direction Statistic {0},{1},{2},{3}", DirectStat[0], DirectStat[1], DirectStat[2], DirectStat[3]);
            }
        }
    }

    public class ReplayMemoryDataRunner : StructRunner
    {
        GameDataCashier Interface { get; set; }

        public int MiniBatchSize = 32;
        public new ReplayBatchData Output { get { return (ReplayBatchData)base.Output; } protected set { base.Output = value; } }

        public ReplayMemoryDataRunner(GameDataCashier gameInterface, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Interface = gameInterface;
            Output = new ReplayBatchData(Interface.StatusFeatureDim, MiniBatchSize, Behavior.Device);
        }

        public override void Forward()
        {
            IsContinue = true;
            Interface.SampleMiniBatch(MiniBatchSize, Output);
            if (Output.Reward.BatchSize < MiniBatchSize) IsContinue = false;
        }
    }

    public class ModelSyncRunner : StructRunner
    {
        public int SyncLoop = 512;
        public int SyncStep = 0;
        public Structure QNetwork;
        public Structure TargetNetwork;
        public ModelSyncRunner(Structure qNet, Structure tNet, int syncLoop, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            QNetwork = qNet;
            TargetNetwork = tNet;
            SyncLoop = syncLoop;
        }

        public override void Forward()
        {
            SyncStep++;
            if(SyncStep >= SyncLoop)
            {
                TargetNetwork.CopyFrom(QNetwork);
                Logger.WriteLog("Target Model Syncup at {0}", SyncStep);
                /*
                foreach (LayerStructure layer in QNetwork.NeuralLinks)
                {
                    layer.weight.CopyOutFromCuda();
                    layer.bias.CopyOutFromCuda();
                }
                TargetNetwork.Init(QNetwork);
                */
            }
            SyncStep = SyncStep % SyncLoop;
        }
    }

    public class RewardUpdateRunner : StructRunner
    {
        HiddenBatchData TargetAction { get; set; }
        DenseBatchData Reward { get; set; }
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }

        int[] Match;
        float gamma = 0.8f;
        List<Tuple<int, float>> DiscountSchedule;
        bool IsScheduled = false;

        int Step = 0;
        public RewardUpdateRunner(DenseBatchData reward, HiddenBatchData targetAction, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Reward = reward;
            TargetAction = targetAction;
            Output = new DenseBatchData(Reward.Stat.MAX_BATCHSIZE, 1, Behavior.Device);
        }

        public RewardUpdateRunner(DenseBatchData reward, int[] targetMatch, HiddenBatchData targetAction, float discount, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Reward = reward;
            TargetAction = targetAction;

            gamma = discount;
            IsScheduled = false;
            Match = targetMatch;
            
            Output = new DenseBatchData(Reward.Stat.MAX_BATCHSIZE, 1, Behavior.Device);
        }

        public RewardUpdateRunner(DenseBatchData reward, int[] targetMatch, HiddenBatchData targetAction, List<Tuple<int, float>> discountSchedule, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Reward = reward;
            TargetAction = targetAction;

            DiscountSchedule = discountSchedule;
            IsScheduled = true;

            Match = targetMatch;

            Output = new DenseBatchData(Reward.Stat.MAX_BATCHSIZE, 1, Behavior.Device);
        }


        public float Gamma(int step)
        {
            for (int i = 0; i < DiscountSchedule.Count; i++)
            {
                if (step < DiscountSchedule[i].Item1)
                {
                    float lambda = (step - DiscountSchedule[i - 1].Item1) * 1.0f / (DiscountSchedule[i].Item1 - DiscountSchedule[i - 1].Item1);
                    return lambda * DiscountSchedule[i].Item2 + (1 - lambda) * DiscountSchedule[i - 1].Item2;
                }
            }
            return DiscountSchedule.Last().Item2;
        }

        public override void Forward()
        {
            Step++;
            float discountGamma = IsScheduled ? Gamma(Step) : gamma; //* (float)Math.Tanh(Step / 50000.0f); // 1.0f / 300000;
            TargetAction.Output.Data.SyncToCPU();
            Reward.Data.SyncToCPU();
            Output.BatchSize = Reward.BatchSize;
            for (int i = 0; i < Reward.BatchSize; i++)
            {
                float maxQ = 0;
                if(Match == null)
                    maxQ = TargetAction.Output.Data.MemPtr.Skip(i * TargetAction.Dim).Take(TargetAction.Dim).Max();
                if (Match != null)
                {
                    if (Match[i] >= 0)
                        maxQ = TargetAction.Output.Data.MemPtr.Skip(Match[i] * TargetAction.Dim).Take(TargetAction.Dim).Max();
                    else
                        maxQ = 0;
                }
                Output.Data.MemPtr[i] = Reward.Data.MemPtr[i] + discountGamma * maxQ;
            }
            Output.Data.SyncFromCPU();
            
        }
    }
}
