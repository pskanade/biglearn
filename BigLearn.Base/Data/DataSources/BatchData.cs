﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BigLearn
{
    public class DataStat : IMetaInfo
    {
        public virtual int TotalBatchNumber { get; set; }
        public virtual int TotalSampleNumber { get; set; }
        #region IIOUnit
        public virtual IOUnitHeader Head { get; set; }

        public virtual DataSourceID Type { get { return DataSourceID.MetaInfo; } }

        public virtual DeviceType DeviceType { get { return DeviceType.CPU; } }

        public virtual void SyncFromCPU() { throw new NotImplementedException(); }
        public virtual void SyncToCPU() { throw new NotImplementedException(); }

        #endregion.

        #region IMetaInfo.
        public virtual IEnumerable<KeyValuePair<string, string>> ToKeyValues() { throw new NotImplementedException(); }

        public virtual void LoadFromKeyValues(IDictionary<string, string> kvs) { throw new NotImplementedException(); }

        public virtual void CopyFrom(IMetaInfo otherMeta) { throw new NotImplementedException(); }

        public virtual void Add(IMetaInfo other) { throw new NotImplementedException(); }

        public virtual void Load(BinaryReader input) { throw new NotImplementedException(); }
        #endregion.

        public virtual void Save(BinaryWriter writer) { throw new NotImplementedException(); }
        public virtual void Init(BinaryReader reader) { throw new NotImplementedException(); }

        public virtual void CountData(int batchSize)
        {
            if (batchSize > 0)
            {
                this.TotalBatchNumber++;
                this.TotalSampleNumber += batchSize;
            }
        }

        public virtual void Dispose() { }
    }

    public abstract class BatchData : IData, IDisposable
    {
        
        #region Dispose Function.
        private bool disposed = false;
        ~BatchData()
        {
            this.Dispose(false);
        }
        /// <summary>
        /// Free GPU Memory.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed) { return; }
            
            if (disposing){}

            this.disposed = true;
        }


        #endregion.

        #region IData Interface.
        public virtual IMetaInfo Meta { get; set; }
        public virtual IData CloneAs(DeviceType deviceType) { throw new NotImplementedException(); }
        public virtual void CopyFrom(IData other) { throw new NotImplementedException(); }
        #endregion.

        #region IOUnit Interface.
        public virtual IOUnitHeader Head { get; set; }
        public virtual DataSourceID Type { get { return DataSourceID.BatchData; } }
        DeviceType deviceType;
        public virtual DeviceType DeviceType { get { return deviceType; } set { deviceType = value; } }

        /// <summary>
        /// should have the same format with Save(BinaryWriter output)
        /// </summary>
        /// <param name="input"></param>
        public virtual void Load(BinaryReader input) { throw new NotImplementedException(); }

        /// <summary>
        /// should have the same format with Load(BinaryReader input)
        /// </summary>
        /// <param name="output"></param>
        public virtual void Save(BinaryWriter output) { throw new NotImplementedException(); }

        public virtual void SyncFromCPU() { throw new NotImplementedException(); }
        public virtual void SyncToCPU() { throw new NotImplementedException(); }
        #endregion



    }

    public interface IStreamRandomAccess
    {
        /// <summary>
        /// Do not change the function Name.
        /// </summary>
        /// <param name="reader"></param>
        void SkipBlock(BinaryReader reader, DataStat stat);
    }

    /// <summary>
    /// It will be served as Data Source.
    /// </summary>
    /// <typeparam name="S"></typeparam>
    public class BatchData<S> : BatchData, IStreamRandomAccess where S : DataStat, new()
    {
        public S Stat { get; set; }

        public BatchData() { Stat = new S(); }
        ~BatchData() { this.Dispose(false); }
        
        /// <summary>
        /// Recommend to create concrete data instance.
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="deviceType"></param>
        public BatchData(S stat, DeviceType deviceType) { Init(stat, deviceType); Clear(); }

        public override IMetaInfo Meta { get { return Stat; } }
        public virtual int BatchSize { get; set; }
        /// <summary>
        /// Not Recommend to call this method explicitly, please try to allocate the data memory in construction function.
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="deviceType"></param>
        public virtual void Init(S stat, DeviceType deviceType) { Stat = stat; DeviceType = deviceType; }

        /// <summary>
        /// Do not change the function Name.
        /// </summary>
        /// <param name="reader"></param>
        public virtual void SkipBlock(BinaryReader reader, DataStat stat) { throw new NotImplementedException(); }


        public virtual void Clear() { throw new NotImplementedException(); }
    }

    public static class BatchDataHelper
    {
        /// <summary>
        /// save batchdata into output, update stat and close output. 
        /// </summary>
        /// <param name="output"></param>
        public static void PopBatchCompleteStat<S>(this BatchData<S> data, BinaryWriter output, bool autoClose = true) where S : DataStat, new()
        {
            data.PopBatchToStat(output);
            data.Stat.Save(output);
            if (autoClose) output.Close();
            else output.Flush();
        }

        /// <summary>
        /// save batchdata into output, update stat, and refresh.
        /// </summary>
        /// <param name="output"></param>
        public static void PopBatchToStat<S>(this BatchData<S> data, BinaryWriter output) where S : DataStat, new()
        {
            data.Save(output);
            data.Stat.CountData(data.BatchSize);
            data.Clear();
        }

        public static void MakeOneBatchStat<S>(this BatchData<S> data) where S : DataStat, new()
        {
            // TODO: make clear of the symantic
            if (data.BatchSize > 0)
            {
                data.Stat.TotalBatchNumber = 1;
                data.Stat.TotalSampleNumber = data.BatchSize;
            }
        }
    }
}
