﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Sequence Data Status.
    /// </summary>
    public class SequenceDataStat : DataStat
    {
        public override DataSourceID Type { get { return DataSourceID.SequenceDataStat; } }

        public int FEATURE_DIM { get; set; }
        public int MAX_BATCHSIZE { get; set; }
        public int MAX_SEQUENCESIZE { get; set; }
        public int MAX_ELEMENTSIZE { get; set; }

        /// <summary>
        /// Create Dense Sequence Stat.
        /// </summary>
        /// <param name="maxBatchSize"></param>
        /// <param name="maxSequenceSize"></param>
        /// <param name="featureDim"></param>
        public SequenceDataStat(int maxBatchSize, int maxSequenceSize, int featureDim) : this(maxBatchSize, maxSequenceSize, featureDim, 0)
        { }

        public SequenceDataStat(int maxBatchSize, int maxSequenceSize, int featureDim, int maxElementSize)
        {
            MAX_BATCHSIZE = maxBatchSize;
            MAX_SEQUENCESIZE = maxSequenceSize;
            FEATURE_DIM = featureDim;
            MAX_ELEMENTSIZE = maxElementSize;
        }

        public SequenceDataStat()
        {
            FEATURE_DIM = 0;
            MAX_BATCHSIZE = 0;
            MAX_SEQUENCESIZE = 0;
            MAX_ELEMENTSIZE = 0;
        }

        public override void Save(BinaryWriter writer)
        {
            writer.Write(FEATURE_DIM);
            writer.Write(MAX_BATCHSIZE);
            writer.Write(MAX_SEQUENCESIZE);
            writer.Write(MAX_ELEMENTSIZE);
            writer.Write(this.TotalBatchNumber);
            writer.Write(this.TotalSampleNumber);
        }

        public override void Init(BinaryReader reader)
        {
            reader.BaseStream.Seek(-6 * sizeof(Int32), SeekOrigin.End);
            Load(reader);
        }

        public override void Load(BinaryReader reader)
        {
            FEATURE_DIM = reader.ReadInt32();
            MAX_BATCHSIZE = reader.ReadInt32();
            MAX_SEQUENCESIZE = reader.ReadInt32();
            MAX_ELEMENTSIZE = reader.ReadInt32();
            TotalBatchNumber = reader.ReadInt32();
            TotalSampleNumber = reader.ReadInt32();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            TextWriter tw = new StringWriter(sb);
            foreach (var kv in this.ToKeyValues())
            {
                tw.WriteLine("{0}\t{1}", kv.Key, kv.Value);
            }

            tw.Flush();
            return sb.ToString();
        }

        public override IEnumerable<KeyValuePair<string, string>> ToKeyValues()
        {
            // Keep order
            yield return new KeyValuePair<string, string>("TYPE", this.Type.ToString());
            yield return new KeyValuePair<string, string>("FEATURE_DIM", this.FEATURE_DIM.ToString());
            yield return new KeyValuePair<string, string>("MAX_BATCHSIZE", this.MAX_BATCHSIZE.ToString());
            yield return new KeyValuePair<string, string>("MAX_SEQUENCESIZE", this.MAX_SEQUENCESIZE.ToString());
            yield return new KeyValuePair<string, string>("MAX_ELEMENTSIZE", this.MAX_ELEMENTSIZE.ToString());
            yield return new KeyValuePair<string, string>("TOTAL_BATCHNUM", this.TotalBatchNumber.ToString());
            yield return new KeyValuePair<string, string>("TOTAL_SAMPLENUM", this.TotalSampleNumber.ToString());
        }

        public override void LoadFromKeyValues(IDictionary<string, string> kvs)
        {
            this.FEATURE_DIM = int.Parse(kvs["FEATURE_DIM"]);
            this.MAX_BATCHSIZE = int.Parse(kvs["MAX_BATCHSIZE"]);
            this.MAX_SEQUENCESIZE = int.Parse(kvs["MAX_SEQUENCESIZE"]);
            this.MAX_ELEMENTSIZE = int.Parse(kvs["MAX_ELEMENTSIZE"]);
            this.TotalBatchNumber = int.Parse(kvs["TOTAL_BATCHNUM"]);
            this.TotalSampleNumber = int.Parse(kvs["TOTAL_SAMPLENUM"]);
        }

        public override void CopyFrom(IMetaInfo otherMeta)
        {
            SequenceDataStat other = (SequenceDataStat)otherMeta;
            this.FEATURE_DIM = other.FEATURE_DIM;
            this.MAX_BATCHSIZE = other.MAX_BATCHSIZE;
            this.MAX_ELEMENTSIZE = other.MAX_ELEMENTSIZE;
            this.MAX_SEQUENCESIZE = other.MAX_SEQUENCESIZE;
            this.TotalBatchNumber = other.TotalBatchNumber;
            this.TotalSampleNumber = other.TotalSampleNumber;
        }

        public override void Add(IMetaInfo meta)
        {
            SequenceDataStat other = (SequenceDataStat)meta;
            this.FEATURE_DIM = Math.Max(this.FEATURE_DIM, other.FEATURE_DIM);
            this.MAX_BATCHSIZE = Math.Max(this.MAX_BATCHSIZE, other.MAX_BATCHSIZE);
            this.MAX_SEQUENCESIZE = Math.Max(this.MAX_SEQUENCESIZE, other.MAX_SEQUENCESIZE);
            this.MAX_ELEMENTSIZE = Math.Max(this.MAX_ELEMENTSIZE, other.MAX_ELEMENTSIZE);
            this.TotalBatchNumber += other.TotalBatchNumber;
            this.TotalSampleNumber += other.TotalSampleNumber;
        }

    }

    /// <summary>
    /// Sparse Feature Sequence Input Data.
    /// </summary>
    public class SeqSparseBatchData : BatchData<SequenceDataStat>
    {
        #region Data Memory Info.
        public override int BatchSize
        {
            get
            {
                return SampleIdx.EffectiveSize;
            }
            set
            {
                SampleIdx.EffectiveSize = value;
                if (Stat.MAX_BATCHSIZE < value) Stat.MAX_BATCHSIZE = value;
            }
        }

        public virtual int SentSize
        {
            get
            {
                return SequenceIdx.EffectiveSize;
            }
            set
            {
                SequenceIdx.EffectiveSize = value;
                SentMargin.EffectiveSize = value;
                if (Stat.MAX_SEQUENCESIZE < value) Stat.MAX_SEQUENCESIZE = value;
            }
        }

        public virtual int ElementSize
        {
            get
            {
                return FeaIdx.EffectiveSize;
            }
            set
            {
                FeaIdx.EffectiveSize = value;
                FeaValue.EffectiveSize = value;
                if (Stat.MAX_ELEMENTSIZE < value) Stat.MAX_ELEMENTSIZE = value;
            }
        }

        public CudaPieceInt SampleIdx;
        public CudaPieceInt SequenceIdx;
        public CudaPieceInt SentMargin;
        public CudaPieceInt FeaIdx;
        public CudaPieceFloat FeaValue;

        public int[] Fea_Idx_Mem { get { return FeaIdx.MemPtr; } }
        public float[] Fea_Value_Mem { get { return FeaValue.MemPtr; } }
        public int[] Sample_Idx_Mem { get { return SampleIdx.MemPtr; } }
        public int[] Seg_Idx_Mem { get { return SequenceIdx.MemPtr; } }
        public int[] Seg_Margin_Mem { get { return SentMargin.MemPtr; } }

        public IntPtr Fea_Idx { get { return FeaIdx.CudaPtr; } }
        public IntPtr Fea_Value { get { return FeaValue.CudaPtr; } }
        public IntPtr Sample_Idx { get { return SampleIdx.CudaPtr; } }
        public IntPtr Seg_Idx { get { return SequenceIdx.CudaPtr; } }
        public IntPtr Seg_Margin { get { return SentMargin.CudaPtr; } }
        #endregion

        //Public override IMetaInfo Meta { get { return this.Stat; } }

        public override DataSourceID Type { get { return DataSourceID.SeqSparseBatchData; } }

        public int[] GetFeatureIds()
        {
            return this.FeaIdx.MemPtr.Distinct().OrderBy(n => n).ToArray();
        }

        public override void CopyFrom(IData other)
        {
            BatchSize = ((SeqSparseBatchData)other).BatchSize;
            SentSize = ((SeqSparseBatchData)other).SentSize;
            ElementSize = ((SeqSparseBatchData)other).ElementSize;

            SampleIdx.CopyFrom(0, ((SeqSparseBatchData)other).SampleIdx, 0, BatchSize);
            SequenceIdx.CopyFrom(0, ((SeqSparseBatchData)other).SequenceIdx, 0, SentSize);
            SentMargin.CopyFrom(0, ((SeqSparseBatchData)other).SentMargin, 0, SentSize);

            FeaIdx.CopyFrom(0, ((SeqSparseBatchData)other).FeaIdx, 0, ElementSize);
            FeaValue.CopyFrom(0, ((SeqSparseBatchData)other).FeaValue, 0, ElementSize);
        }

        /// <summary>
        /// Try to avoid using the default construction function.
        /// </summary>
        public SeqSparseBatchData() { }

        public SeqSparseBatchData(SequenceDataStat stat, DeviceType device) : base(stat, device) { }

        public override void SyncFromCPU()
        {
            SampleIdx.SyncFromCPU(BatchSize);
            SequenceIdx.SyncFromCPU(SentSize);
            FeaIdx.SyncFromCPU(ElementSize);
            FeaValue.SyncFromCPU(ElementSize);
            SentMargin.SyncFromCPU(SentSize);
        }

        public override void SyncToCPU()
        {
            SampleIdx.SyncToCPU(BatchSize);
            SequenceIdx.SyncToCPU(SentSize);
            FeaIdx.SyncToCPU(ElementSize);
            FeaValue.SyncToCPU(ElementSize);
            SentMargin.SyncToCPU(SentSize);
        }

        //public SeqSparseBatchData(SequenceDataStat stat, CudaPieceInt sampleIdx, CudaPieceInt sentMargin, DeviceType deviceType)
        //{
        //    SampleIdx = sampleIdx;

        //    Stat = stat;
        //    DeviceType = deviceType;
        //    SampleIdx = sampleIdx;
        //    SentMargin = sentMargin;
        //    SequenceIdx = new CudaPieceInt(Stat.MAX_SEQUENCESIZE, true, deviceType == DeviceType.GPU);
        //    FeaIdx = new CudaPieceInt(Stat.MAX_ELEMENTSIZE, true, deviceType == DeviceType.GPU);
        //    FeaValue = new CudaPieceFloat(Stat.MAX_ELEMENTSIZE, true, deviceType == DeviceType.GPU);
        //}

        public override void Init(SequenceDataStat stat, DeviceType deviceType)
        {
            base.Init(stat, deviceType);

            SampleIdx = new CudaPieceInt(Stat.MAX_BATCHSIZE, deviceType);
            SequenceIdx = new CudaPieceInt(Stat.MAX_SEQUENCESIZE, deviceType);
            SentMargin = new CudaPieceInt(Stat.MAX_SEQUENCESIZE, deviceType);
            FeaIdx = new CudaPieceInt(Stat.MAX_ELEMENTSIZE, deviceType);
            FeaValue = new CudaPieceFloat(Stat.MAX_ELEMENTSIZE, deviceType);
        }

        /// <summary>
        /// Load Data from Stream to Device.
        /// </summary>
        /// <param name="reader"></param>
        public override void Load(BinaryReader reader)
        {
            int batchSize = reader.ReadInt32();
            int sentSize = reader.ReadInt32();
            int elemSize = reader.ReadInt32();
            this.BatchSize = batchSize;
            this.SentSize = sentSize;
            this.ElementSize = elemSize;
            //we are to change to using VINT 
            /*
            VIntHelper.ReadSortedVIntListInt(reader, SampleIdx.MemPtr);
            VIntHelper.ReadSortedVIntListInt(reader, SequenceIdx.MemPtr);
            VIntHelper.ReadVIntList(reader, FeaIdx.MemPtr);
            */
            Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * BatchSize), 0, SampleIdx.MemPtr, 0, sizeof(Int32) * BatchSize);
            Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * SentSize), 0, SequenceIdx.MemPtr, 0, sizeof(Int32) * SentSize);
            Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * ElementSize), 0, FeaIdx.MemPtr, 0, sizeof(Int32) * ElementSize);
            Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * ElementSize), 0, FeaValue.MemPtr, 0, sizeof(float) * ElementSize);

            for (int batch = 0; batch < BatchSize; batch++)
                for (int seg = batch == 0 ? 0 : SampleIdx.MemPtr[batch - 1]; seg < SampleIdx.MemPtr[batch]; seg++)
                {
                    SentMargin.MemPtr[seg] = batch;
                }

            SyncFromCPU();
        }

        /// <summary>
        /// Save Data from Device to Stream.
        /// </summary>
        /// <param name="writer"></param>
        public override void Save(BinaryWriter writer)
        {
            if (BatchSize > 0)
            {
                writer.Write(BatchSize);
                writer.Write(SentSize);
                writer.Write(ElementSize);

                SyncToCPU();

                for (int i = 0; i < BatchSize; ++i) writer.Write(SampleIdx.MemPtr[i]);
                for (int i = 0; i < SentSize; ++i) writer.Write(SequenceIdx.MemPtr[i]);
                for (int i = 0; i < ElementSize; ++i) writer.Write(FeaIdx.MemPtr[i]);
                for (int i = 0; i < ElementSize; ++i) writer.Write(FeaValue.MemPtr[i]);
            }
        }

        /// <summary>
        /// called after save.
        /// </summary>
        public override void Clear()
        {
            BatchSize = 0;
            SentSize = 0;
            ElementSize = 0;
        }

        /// <summary>
        /// Skip memory from current Stream.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="stat"></param>
        public override void SkipBlock(BinaryReader reader, DataStat stat)
        {
            int b = reader.ReadInt32();
            int s = reader.ReadInt32();
            int e = reader.ReadInt32();
            reader.BaseStream.Seek((b + s + e) * sizeof(int) + e * sizeof(float), SeekOrigin.Current);
        }

        public void PushSample(List<Dictionary<int, float>> sequenceList)
        {
            int oldBatchSize = BatchSize;
            int oldSentSize = SentSize;
            int oldElementSize = ElementSize;

            BatchSize = oldBatchSize + 1;
            SentSize = oldSentSize + sequenceList.Count;
            ElementSize = oldElementSize + sequenceList.Select(i => i.Count).Sum();

            SampleIdx.MemPtr[oldBatchSize] = SentSize;
            int sentcursor = oldSentSize;
            int elementcursor = oldElementSize;
            //
            foreach (Dictionary<int, float> seg in sequenceList)
            {
                SequenceIdx.MemPtr[sentcursor] = elementcursor + seg.Count;
                SentMargin.MemPtr[sentcursor] = oldBatchSize;
                foreach (KeyValuePair<int, float> fv in seg)
                {
                    FeaIdx.MemPtr[elementcursor] = fv.Key;
                    FeaValue.MemPtr[elementcursor] = fv.Value;
                    elementcursor += 1;
                    if (fv.Key >= Stat.FEATURE_DIM) Stat.FEATURE_DIM = fv.Key + 1;
                }
                sentcursor += 1;
            }
            SampleIdx.SyncFromCPU(oldBatchSize, SampleIdx.MemPtr, oldBatchSize, 1);

            SequenceIdx.SyncFromCPU(oldSentSize, SequenceIdx.MemPtr, oldSentSize, SentSize - oldSentSize);
            SentMargin.SyncFromCPU(oldSentSize, SentMargin.MemPtr, oldSentSize, SentSize - oldSentSize);

            FeaIdx.SyncFromCPU(oldElementSize, FeaIdx.MemPtr, oldElementSize, ElementSize - oldElementSize);
            FeaValue.SyncFromCPU(oldElementSize, FeaValue.MemPtr, oldElementSize, ElementSize - oldElementSize);
        }

        public void PushSamples(IEnumerable<List<Dictionary<int, float>>> sequenceLists)
        {
            int pBatchSize = BatchSize;
            int pSentSize = SentSize;
            int pElementSize = ElementSize;

            int bidx = 0;
            foreach (List<Dictionary<int, float>> s in sequenceLists)
            {
                int oldBatchSize = BatchSize;
                int oldSentSize = SentSize;
                int oldElementSize = ElementSize;

                BatchSize = oldBatchSize + 1;
                SentSize = oldSentSize + s.Count;
                ElementSize = oldElementSize + s.Select(i => i.Count).Sum();

                SampleIdx.MemPtr[oldBatchSize] = SentSize;
                int sentcursor = oldSentSize;
                int elementcursor = oldElementSize;
                //
                foreach (Dictionary<int, float> seg in s)
                {
                    SequenceIdx.MemPtr[sentcursor] = elementcursor + seg.Count;
                    SentMargin.MemPtr[sentcursor] = oldBatchSize;
                    foreach (KeyValuePair<int, float> fv in seg)
                    {
                        FeaIdx.MemPtr[elementcursor] = fv.Key;
                        FeaValue.MemPtr[elementcursor] = fv.Value;
                        elementcursor += 1;
                        if (fv.Key >= Stat.FEATURE_DIM) Stat.FEATURE_DIM = fv.Key + 1;
                    }
                    sentcursor += 1;
                }
                bidx += 1;
            }

            SampleIdx.SyncFromCPU(pBatchSize, SampleIdx.MemPtr, pBatchSize, bidx);

            SequenceIdx.SyncFromCPU(pSentSize, SequenceIdx.MemPtr, pSentSize, SentSize - pSentSize);
            SentMargin.SyncFromCPU(pSentSize, SentMargin.MemPtr, pSentSize, SentSize - pSentSize);

            FeaIdx.SyncFromCPU(pElementSize, FeaIdx.MemPtr, pElementSize, ElementSize - pElementSize);
            FeaValue.SyncFromCPU(pElementSize, FeaValue.MemPtr, pElementSize, ElementSize - pElementSize);
        }


        public List<List<Dictionary<int, float>>> UnrollAsSampleList()
        {
            List<List<Dictionary<int, float>>> sampleList = new List<List<Dictionary<int, float>>>();
            int startSampleIdx = 0;
            int startSeqIdx = 0;
            for (int i = 0; i < this.BatchSize; i++)
            {
                List<Dictionary<int, float>> sequenceList = new List<Dictionary<int, float>>();
                for (int j = startSampleIdx; j < this.SampleIdx.MemPtr[i]; j++)
                {
                    Dictionary<int, float> seq = new Dictionary<int, float>();
                    for (int k = startSeqIdx; k < this.SequenceIdx.MemPtr[j]; k++)
                    {
                        seq[FeaIdx.MemPtr[k]] = FeaValue.MemPtr[k];
                    }

                    startSeqIdx = this.SequenceIdx.MemPtr[j];
                    sequenceList.Add(seq);
                }
                startSampleIdx = this.SampleIdx.MemPtr[i];
                sampleList.Add(sequenceList);
            }
            return sampleList;
        }


        #region Dispose Function is not necessary.
        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            this.disposed = true;
            base.Dispose(disposing);
            if (disposing)
            {
                if (this.SampleIdx != null)
                {
                    this.SampleIdx.Dispose();
                    this.SampleIdx = null;
                }

                if (this.SequenceIdx != null)
                {
                    this.SequenceIdx.Dispose();
                    this.SequenceIdx = null;
                }

                if (this.FeaIdx != null)
                {
                    this.FeaIdx.Dispose();
                    this.FeaIdx = null;
                }

                if (this.FeaValue != null)
                {
                    this.FeaValue.Dispose();
                    this.FeaValue = null;
                }
            }

            base.Dispose(disposing);
        }


        ~SeqSparseBatchData()
        {
            this.Dispose(false);
        }
        #endregion.

    }
}
