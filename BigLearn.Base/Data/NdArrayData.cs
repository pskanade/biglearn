﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class NdArrayData : VectorData
    {
        public override DataSourceID Type { get { return DataSourceID.NdArrayData; } }

        public IntArgument[] Dimensions = null;

        public NdArrayData(IntArgument[] dimensions, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device)
            : base(dimensions.Aggregate((a, x) => new IntArgument("sum", a.Value * x.Value)).Value, data, deriv, device)
        {
            Dimensions = dimensions;
        }
        
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
                if (Deriv != null) Deriv.Dispose(); Deriv = null;
            }
        }
    }

}
