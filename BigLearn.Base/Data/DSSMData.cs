﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;
using System.Diagnostics;

namespace BigLearn
{
    public class DSSMDataStat : DataStat
    {
        public SequenceDataStat SrcStat = new SequenceDataStat();
        public SequenceDataStat TgtStat = new SequenceDataStat();
        public DenseDataStat ScoreStat = new DenseDataStat();

        //public bool IsResort = false;
        public DSSMDataStat()
        {
        }

        public override int TotalBatchNumber
        {
            get
            {
                return SrcStat.TotalBatchNumber;
            }
            set
            {
                SrcStat.TotalBatchNumber = TgtStat.TotalBatchNumber = value;
                if (ScoreStat.MAX_BATCHSIZE > 0)
                {
                    ScoreStat.TotalBatchNumber = value;
                }
            }
        }

        public override int TotalSampleNumber
        {
            get
            {
                return SrcStat.TotalSampleNumber;
            }
            set
            {
                SrcStat.TotalSampleNumber = TgtStat.TotalSampleNumber = value;
                if (ScoreStat.MAX_BATCHSIZE > 0)
                {
                    ScoreStat.TotalSampleNumber = value;
                }
            }
        }

        public override DataSourceID Type { get { return DataSourceID.DSSMDataMeta; } }


        public override void Load(BinaryReader reader)
        {
            this.SrcStat.Load(reader);
            this.TgtStat.Load(reader);
            this.ScoreStat.Load(reader);
        }

        public override void Save(BinaryWriter writer)
        {
            this.SrcStat.Save(writer);
            this.TgtStat.Save(writer);
            this.ScoreStat.Save(writer);
        }

        public override IEnumerable<KeyValuePair<string, string>> ToKeyValues()
        {
            // Keep order
            yield return new KeyValuePair<string, string>("TYPE", this.Type.ToString());
            foreach (var kv in this.SrcStat.ToKeyValues())
            {
                yield return new KeyValuePair<string, string>("SOURCE::" + kv.Key, kv.Value);
            }

            foreach (var kv in this.TgtStat.ToKeyValues())
            {
                yield return new KeyValuePair<string, string>("TARGET::" + kv.Key, kv.Value);
            }

            foreach (var kv in this.ScoreStat.ToKeyValues())
            {
                yield return new KeyValuePair<string, string>("SCORE::" + kv.Key, kv.Value);
            }
        }

        public override void LoadFromKeyValues(IDictionary<string, string> kvs)
        {
            var srcKvs = kvs.Where(kv => kv.Key.StartsWith("SOURCE::")).ToDictionary(kv => kv.Key.Substring("SOURCE::".Length), kv => kv.Value);
            this.SrcStat.LoadFromKeyValues(srcKvs);

            var dstKvs = kvs.Where(kv => kv.Key.StartsWith("TARGET::")).ToDictionary(kv => kv.Key.Substring("TARGET::".Length), kv => kv.Value);
            this.TgtStat.LoadFromKeyValues(dstKvs);

            var scoreKvs = kvs.Where(kv => kv.Key.StartsWith("SCORE::")).ToDictionary(kv => kv.Key.Substring("SCORE::".Length), kv => kv.Value);
            this.ScoreStat.LoadFromKeyValues(scoreKvs);
        }

        public override void CopyFrom(IMetaInfo otherMeta)
        {
            if (otherMeta is DSSMDataStat)
            {
                DSSMDataStat other = (DSSMDataStat)otherMeta;
                //this.IsResort = other.IsResort;
                this.SrcStat.CopyFrom(other.SrcStat);
                this.TgtStat.CopyFrom(other.TgtStat);
                this.ScoreStat.CopyFrom(other.ScoreStat);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public override void Add(IMetaInfo meta)
        {
            DSSMDataStat other = (DSSMDataStat)meta;
            this.SrcStat.Add(other.SrcStat);
            this.TgtStat.Add(other.TgtStat);
            this.ScoreStat.Add(other.ScoreStat);
        }
    }

    public class DSSMData : BatchData<DSSMDataStat>, IData
    {
        private DeviceType deviceType = DeviceType.CPU;

        public override DataSourceID Type { get { return DataSourceID.DSSMData; } }

        public SeqSparseBatchData SrcInput;
        public SeqSparseBatchData TgtInput;
        public DenseBatchData ScoreInput;
        public override int BatchSize { get { return SrcInput.BatchSize; } }

        private long? pairId = null;
        public long PairId
        {
            get
            {
                if (!this.pairId.HasValue)
                {
                    this.pairId = new Random(13).Next();
                }

                return this.pairId.Value;
            }
            set
            {
                this.pairId = value;
            }
        }

        public DSSMData()
        {
            Stat = new DSSMDataStat();
            SrcInput = new SeqSparseBatchData();
            TgtInput = new SeqSparseBatchData();
            ScoreInput = new DenseBatchData();
        }

        public DSSMData(IOUnitHeader head, IMetaInfo stat, DeviceType device):this()
        {
            this.Head = head;
            if (stat != null)
            {
                this.Stat = (DSSMDataStat)stat;
            }
            else
            {
                this.Stat = new DSSMDataStat();
            }

            this.deviceType = device;
            this.Init(this.Stat, this.DeviceType);
        }
        
        public DSSMData(SeqSparseBatchData src, SeqSparseBatchData tgt, DenseBatchData score)
        {
            Stat = new DSSMDataStat() { SrcStat = src.Stat, TgtStat = tgt.Stat, ScoreStat = score.Stat };
            SrcInput = src;
            TgtInput = tgt;
            ScoreInput = score;
        }


        public override void Init(DSSMDataStat stat, DeviceType deviceType)
        {
            // we should set Stat = stat;
            Stat = new DSSMDataStat();
            Stat.CopyFrom(stat);
            //Stat = stat;

            SrcInput.Init(Stat.SrcStat, deviceType);
            TgtInput.Init(Stat.TgtStat, deviceType);
            ScoreInput.Init(Stat.ScoreStat, deviceType);
        }

        public void Load(BinaryReader srcReader, BinaryReader tgtReader, BinaryReader scoreReader)
        {
            this.SrcInput.Load(srcReader);
            this.TgtInput.Load(tgtReader);
            if (scoreReader != null)
            {
                ScoreInput.Load(scoreReader);
            }
        }

        public override void Load(BinaryReader input)
        {
            int flag = input.ReadInt32();
            this.SrcInput.Load(input);
            this.TgtInput.Load(input);
            if ((flag & 0x4) == 0x4)
            {
                this.ScoreInput.Load(input);
            }
            if ((flag & 0x8) == 0x8)
            {
                this.pairId = input.ReadInt64();
            }
        }

        public override void Save(BinaryWriter output)
        {
            int flag = 0x3;
            if (ScoreInput.BatchSize > 0)
            {
                flag |= 0x4;
            }

            if (this.pairId.HasValue)
            {
                flag |= 0x8;
            }

            output.Write(flag);
            this.SrcInput.Save(output);
            this.TgtInput.Save(output);

            if ((flag & 0x4) == 0x4)
            {
                this.ScoreInput.Save(output);
            }

            if ((flag & 0x8) == 0x8)
            {
                output.Write(this.PairId);
            }
        }

        public void PushSample(List<Dictionary<int, float>> src, List<Dictionary<int, float>> tgt, string score)
        {
            this.SrcInput.PushSample(src);
            this.TgtInput.PushSample(tgt);
            if (!string.IsNullOrEmpty(score))
            {
                HRSItem sc;
                if (!Enum.TryParse(score, true, out sc))
                {
                    sc = HRSItem.Fair;
                }
                
                this.ScoreInput.PushSample(new float[] { (float)sc }, 1);
            }
        }

        public Tuple<List<List<Dictionary<int, float>>>, List<List<Dictionary<int, float>>>, float[]> UnrollAsSampleList()
        {
            List<List<Dictionary<int, float>>> srcList = this.SrcInput.UnrollAsSampleList();
            List<List<Dictionary<int, float>>> tgtList = this.TgtInput.UnrollAsSampleList();
            float[] scores = null;
            if (!(this.ScoreInput == null || this.ScoreInput.BatchSize == 0))
            {
                scores = this.ScoreInput.UnrollAsSampleList();
            }

            return new Tuple<List<List<Dictionary<int, float>>>, List<List<Dictionary<int, float>>>, float[]>(srcList, tgtList, scores);
        }

        public override void Clear()
        {
            this.SrcInput.Clear();
            this.TgtInput.Clear();
            this.ScoreInput.Clear();
        }

        public override void CopyFrom(IData other)
        {
            this.SrcInput.CopyFrom(((DSSMData)other).SrcInput);
            this.TgtInput.CopyFrom(((DSSMData)other).TgtInput);
            if (((DSSMData)other).ScoreInput != null && this.ScoreInput != null) this.ScoreInput.CopyFrom(((DSSMData)other).ScoreInput);
        }

        public override IData CloneAs(DeviceType deviceType)
        {
            DSSMData ss = new DSSMData(this.Head, this.Stat, deviceType);
            ss.CopyFrom(this);
            return ss;
        }

        public override void SyncFromCPU()
        {
            SrcInput.SyncFromCPU();
            TgtInput.SyncFromCPU();
            ScoreInput.SyncFromCPU();
        }

        public override void SyncToCPU()
        {
            SrcInput.SyncToCPU();
            TgtInput.SyncToCPU();
            ScoreInput.SyncToCPU();
        }

        //public override void InGPU(BatchData sblock)
        //{
        //    DSSMData block = (DSSMData)sblock;
        //    SrcInput.InGPU(block.SrcInput);
        //    TgtInput.InGPU(block.TgtInput);
        //    ScoreInput.InGPU(block.ScoreInput);
        //}

        protected  override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (SrcInput != null) SrcInput.Dispose(); SrcInput = null;
                if (TgtInput != null) TgtInput.Dispose(); TgtInput = null;
                if (ScoreInput != null) ScoreInput.Dispose(); ScoreInput = null;
            }
        }
    }


    /// <summary>
    /// To Do : Could we have only one Data Cashier?
    /// </summary>
    public class DSSMDataCashier : IDataCashier<DSSMData, DSSMDataStat>
    {
        List<DSSMData> L1Caches = new List<DSSMData>();
        List<DSSMData> L2Caches = new List<DSSMData>();

        private string srcPath;
        protected Stream SrcStream = null;
        protected BinaryReader SrcReader = null;
        protected long[] SrcSeekPosition = null;

        private string tgtPath;
        protected Stream TgtStream = null;
        protected BinaryReader TgtReader = null;
        protected long[] TgtSeekPosition = null;

        private string scorePath;
        protected Stream ScoreStream = null;
        protected BinaryReader ScoreReader = null;
        protected long[] ScoreSeekPosition = null;
        public bool IsScore { get; set; }

        //public bool IsReSort
        //{
        //    get { return this.Stat.IsResort; }
        //    set { this.Stat.IsResort = value; }
        //}
        public DSSMDataCashier(string srcStream, string tgtStream, string scoreStream, Random r = null)
        {
            this.Stat = new DSSMDataStat();
            srcPath = srcStream;
            SrcStream = FileUtil.CreateReadFS(srcStream); // StreamFactory.GetReadStream(srcStream);
            SrcReader = new BinaryReader(SrcStream);

            Stat.SrcStat.Init(SrcReader);

            tgtPath = tgtStream;
            TgtStream = FileUtil.CreateReadFS(tgtStream);
            TgtReader = new BinaryReader(TgtStream);

            Stat.TgtStat.Init(TgtReader);

            if (SrcStream.CanSeek)
            {
                SrcSeekPosition = InitBlockPosition(SrcReader, Stat.SrcStat, typeof(SeqSparseBatchData));
            }

            if (TgtStream.CanSeek)
            {
                TgtSeekPosition = InitBlockPosition(TgtReader, Stat.TgtStat, typeof(SeqSparseBatchData));
            }

            IsScore = false;
            scorePath = scoreStream;
            if (!string.IsNullOrEmpty(scoreStream))
            {
                ScoreStream = FileUtil.CreateReadFS(scoreStream);  //StreamFactory.GetReadStream(scoreStream);
                ScoreReader = new BinaryReader(ScoreStream);
                Stat.ScoreStat.Init(ScoreReader);
                if (ScoreStream.CanSeek)
                {
                    ScoreSeekPosition = InitBlockPosition(ScoreReader, Stat.ScoreStat, typeof(DenseBatchData));
                }

                IsScore = true;
            }
            this.rnd = r;
            if (this.rnd == null)
            {
                this.rnd = new Random(ParameterSetting.RANDOM_SEED);
            }
        }

        public DSSMDataStat Stat { get; private set; }

        public void InitPipelineCashier(int l1Cache, int l2Cache)
        {
            for (int i = 0; i < l1Cache; i++)
            {
                L1Caches.Add(new DSSMData());
                L1Caches[i].Init(Stat, DeviceType.CPU);
            }

            for (int i = 0; i < l2Cache; i++)
            {
                L2Caches.Add(new DSSMData());
                L2Caches[i].Init(Stat, DeviceType.GPU);
            }
        }

        public IEnumerable<DSSMData> GetInstances(bool randomProcessor)
        {
            BlockingCollection<int> dataBuffer = new BlockingCollection<int>(L1Caches.Count - 2);
            var disk2CPUParallel = Task.Run(() =>
            {
                SrcStream.Seek(0, SeekOrigin.Begin);
                TgtStream.Seek(0, SeekOrigin.Begin);
                if (ScoreStream != null) ScoreStream.Seek(0, SeekOrigin.Begin);

                if (randomProcessor && SrcStream.CanSeek && TgtStream.CanSeek && (ScoreStream == null || ScoreStream.CanSeek))
                {
                    if (SrcSeekPosition == null)
                    {
                        SrcSeekPosition = InitBlockPosition(SrcReader, Stat.SrcStat, typeof(SeqSparseBatchData));
                    }

                    if (TgtSeekPosition == null)
                    {
                        TgtSeekPosition = InitBlockPosition(TgtReader, Stat.TgtStat, typeof(SeqSparseBatchData));
                    }

                    if (ScoreSeekPosition == null && ScoreStream != null)
                    {
                        ScoreSeekPosition = InitBlockPosition(ScoreReader, Stat.ScoreStat, typeof(DenseBatchData));
                    }

                    int[] RandomBatchIdx = Enumerable.Range(0, Stat.TotalBatchNumber).ToArray();
                    Util.RandomShuffle(RandomBatchIdx, rnd);
                    int batIdx = 0;
                    int offset = (rnd.Next() & 0xFFFF);
                    while (batIdx < Stat.TotalBatchNumber)
                    {

                        int id = RandomBatchIdx[(batIdx + offset) % RandomBatchIdx.Length];

                        SrcStream.Position = SrcSeekPosition[id];
                        TgtStream.Position = TgtSeekPosition[id];
                        if (ScoreStream != null)
                        {
                            ScoreStream.Position = ScoreSeekPosition[id];
                        }


                        L1Caches[batIdx % L1Caches.Count].Load(SrcReader, TgtReader, ScoreReader);
                        dataBuffer.Add(batIdx % L1Caches.Count);

                        batIdx += 1;
                    }
                }
                else
                {
                    int batIdx = 0;
                    try
                    {
                        while (batIdx < Stat.TotalBatchNumber)
                        {
                            L1Caches[batIdx % L1Caches.Count].Load(SrcReader, TgtReader, ScoreReader);
                            dataBuffer.Add(batIdx % L1Caches.Count);
                            batIdx += 1;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Failed to load data, exp: {0}", ex);
                        dataBuffer.CompleteAdding();
                        throw;
                    }
                }
                // Let consumer know we are done.
                dataBuffer.CompleteAdding();
            });

            if (L2Caches.Count > 2)
            {
                BlockingCollection<int> gpuBuffer = new BlockingCollection<int>(L2Caches.Count - 2);
                var cpu2gpuParallel = Task.Run(() =>
                {
                    int batIdx = 0;
                    while (!dataBuffer.IsCompleted)
                    {
                        try
                        {
                            int l1Idx = dataBuffer.Take(); ///CPU buffer.
                            L2Caches[batIdx % L2Caches.Count].CopyFrom(L1Caches[l1Idx]);
                            gpuBuffer.Add(batIdx % L2Caches.Count);
                            batIdx += 1;
                        }
                        catch (InvalidOperationException) { }
                    }
                    gpuBuffer.CompleteAdding();
                });

                while (!gpuBuffer.IsCompleted)
                {
                    int gpuId = -1;
                    try
                    {
                        gpuId = gpuBuffer.Take();
                    }
                    catch (InvalidOperationException) { }

                    if (gpuId != -1)
                    {
                        yield return L2Caches[gpuId];
                    }
                }

                cpu2gpuParallel.Wait();
            }
            else
            {
                while (!dataBuffer.IsCompleted)
                {
                    int cpuId = -1;
                    try
                    {
                        cpuId = dataBuffer.Take();
                    }
                    catch (InvalidOperationException)
                    {

                    }

                    if (cpuId != -1)
                    {
                        yield return L1Caches[cpuId];
                    }
                }
            }

            SrcStream.Close();
            TgtStream.Close();
            if (ScoreStream != null)
            {
                ScoreStream.Close();
            }

            disk2CPUParallel.Wait();
        }

        private long[] InitBlockPosition(BinaryReader reader, DataStat stat, Type batchDataType)
        {
            long[] seekPosition = new long[stat.TotalBatchNumber];
            reader.BaseStream.Seek(0, SeekOrigin.Begin);

            for (int i = 0; i < stat.TotalBatchNumber; i++)
            {
                seekPosition[i] = reader.BaseStream.Position;

                ((IStreamRandomAccess)(Activator.CreateInstance(batchDataType))).SkipBlock(reader, stat);
            }
            return seekPosition;
        }

        public void Dispose()
        {
        }

        IEnumerator<DSSMData> DataFlow { get; set; }
        object DataFlowLock = new object();
        bool IsRandom { get; set; }

        Random rnd = null;
        public void InitThreadSafePipelineCashier(int l1Cache, bool isRandom)
        {
            IsRandom = isRandom;
            InitPipelineCashier(l1Cache, 0);
        }

        public void ResetDataFlow()
        {
            if (this.SrcStream != null)
            {
                this.SrcStream.Close();
                SrcStream = FileUtil.CreateReadFS(srcPath); // StreamFactory.GetReadStream(srcPath);
                SrcReader = new BinaryReader(SrcStream);
            }

            if (this.TgtStream != null)
            {
                this.TgtStream.Close();
                TgtStream = FileUtil.CreateReadFS(tgtPath); // StreamFactory.GetReadStream(tgtPath);
                TgtReader = new BinaryReader(TgtStream);
            }

            if (this.ScoreStream != null)
            {
                this.ScoreStream.Close();
                ScoreStream = FileUtil.CreateReadFS(scorePath);// StreamFactory.GetReadStream(scorePath);
                ScoreReader = new BinaryReader(ScoreStream);
            }

            DataFlow = GetInstances(IsRandom).GetEnumerator();
        }

        /// <summary>
        /// ThreadSafe for Fetching Training Data.
        /// </summary>
        /// <param name="output"> DataType must have IData Interface.</param>
        /// <returns></returns>
        public bool FetchDataThreadSafe(DSSMData output)
        {
            lock (DataFlowLock)
            {
                if (DataFlow.MoveNext())
                {
                    using (var stream = new MemoryStream())
                    {
                        output.CopyFrom(DataFlow.Current);
                    }
                    return true;
                }
                else
                {
                    output.Clear();
                    return false;
                }
            }
        }

        public void CloseDataFlow()
        {
            //throw new NotImplementedException();
        }
    }

}
