﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SparseVectorData : VectorData
    {
        public override DataSourceID Type { get { return DataSourceID.SparseVectorData; } }

        public CudaPieceInt Idx;
        public CudaPieceFloat Value { get { return Output; } }
        
        public override int Length
        {
            get
            {
                return base.Length;
            }
            set
            {
                base.Length = value;
                Idx.EffectiveSize = value;
            }
        }

        public SparseVectorData(int maxLength, DeviceType device, bool isDeriv = true) : this(maxLength,
            new CudaPieceInt(maxLength, device), 
            new CudaPieceFloat(maxLength, device), 
            isDeriv ? new CudaPieceFloat(maxLength, device) : CudaPieceFloat.Empty, device)
        {
        }

        public SparseVectorData(int maxLength, CudaPieceInt idx, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) : 
            base(maxLength, data, deriv, device)
        {
            Idx = idx;
        }
        
        public override void SyncFromCPU()
        {
            Idx.SyncFromCPU();
            Value.SyncFromCPU();
        }

        public override void SyncToCPU()
        {
            Idx.SyncToCPU();
            Value.SyncToCPU();
        }
    }
}
