﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SeqVectorData : VectorData
    {
        public CudaPieceInt SegmentIdx = null;
        public CudaPieceInt SegmentMargin = null;
        public override DataSourceID Type { get { return DataSourceID.SeqVectorData; } }

        public int MaxSegment;
        public virtual int Segment
        {
            get { return SegmentIdx.EffectiveSize; }
            set
            {
                if (value > MaxSegment) throw new Exception(string.Format("SegmentNum {0} should be smaller than Max SegmentNum {1}", value, MaxSegment));
                SegmentIdx.EffectiveSize = value; 
            }
        }

        public override int Length
        {
            get { return base.Length; }
            set
            {
                base.Length = value;
                SegmentMargin.EffectiveSize = value;
            }
        }

        public SeqVectorData(int maxLength, int maxSegment, DeviceType device) :
            this(maxLength, maxSegment, new CudaPieceFloat(maxLength, true, device == DeviceType.GPU), new CudaPieceFloat(maxLength, true, device == DeviceType.GPU),
                 new CudaPieceInt(maxSegment, true, device == DeviceType.GPU), new CudaPieceInt(maxLength, true, device == DeviceType.GPU), device)
        { }

        public SeqVectorData(int maxLength, int maxSegment, CudaPieceInt segmentIdx, CudaPieceInt segmentMargin, DeviceType device) :
            this(maxLength, maxSegment, new CudaPieceFloat(maxLength, true, device == DeviceType.GPU), new CudaPieceFloat(maxLength, true, device == DeviceType.GPU),
                 segmentIdx, segmentMargin, device)
        { }

        public SeqVectorData(int maxLength, int maxSegment, CudaPieceFloat data, CudaPieceFloat deriv, CudaPieceInt segmentIdx, CudaPieceInt segmentMargin, DeviceType device)
            :base(maxLength, data, deriv, device)
        {
            SegmentIdx = segmentIdx;
            SegmentMargin = segmentMargin;
            MaxSegment = maxSegment;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
                if (Deriv != null) Deriv.Dispose(); Deriv = null;
            }
        }
    }
}
