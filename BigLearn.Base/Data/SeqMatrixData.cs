﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SeqMatrixData : MatrixData
    {
        public CudaPieceInt SegmentIdx = null;
        public CudaPieceInt SegmentMargin = null;

        public override DataSourceID Type { get { return DataSourceID.SeqMatrixData; } }

        public int MaxSegment;
        public int Segment
        {
            get { return SegmentIdx.EffectiveSize; }
            set
            {
                if (value > MaxSegment) throw new Exception(string.Format("SegmentNum {0} should be smaller than Max SegmentNum {1}", value, MaxSegment));
                SegmentIdx.EffectiveSize = value; 
            }
        }

        public override int Row
        {
            get
            {
                return base.Row;
            }
            set
            {
                base.Row = value;
                SegmentMargin.EffectiveSize = value;
            }
        }

        //public SeqMatrixData(int column, int maxRow, int maxSegmentNum, DeviceType device) :
        //    this(column, maxRow, maxSegmentNum, 
        //        new CudaPieceFloat(column * maxRow, true, device == DeviceType.GPU),
        //        new CudaPieceFloat(column * maxRow, true, device == DeviceType.GPU), 
        //        new CudaPieceInt(maxSegmentNum, true, device == DeviceType.GPU),
        //        new CudaPieceInt(maxRow, true, device == DeviceType.GPU), 
        //        device)
        //{
        //}

        //public SeqMatrixData(int column, int maxRow, int maxSegmentNum, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) :
        //    this(column, maxRow, maxSegmentNum, 
        //        data, 
        //        deriv, 
        //        new CudaPieceInt(maxSegmentNum, true, device == DeviceType.GPU),
        //        new CudaPieceInt(maxRow, true, device == DeviceType.GPU),
        //        device)
        //{
        //}

        public SeqMatrixData(int column, int maxRow, int maxSegment, CudaPieceInt segmentIdx, CudaPieceInt segmentMargin, DeviceType device) : 
            this(column, maxRow, maxSegment, 
                new CudaPieceFloat(column * maxRow, device), 
                new CudaPieceFloat(column * maxRow, device), 
                segmentIdx, 
                segmentMargin, 
                device)
        {
        }

        public SeqMatrixData(int column, int maxRow, int maxSegment, CudaPieceFloat data, CudaPieceFloat deriv, CudaPieceInt segmentIdx, CudaPieceInt segmentMargin, DeviceType device) :
            base(column, maxRow, data, deriv, device)
        {
            MaxSegment = maxSegment;
            SegmentIdx = segmentIdx;
            SegmentMargin = segmentMargin;
        }
        public SeqMatrixData(SeqDenseBatchData data) : this(data.Dim, data.MAX_SENTSIZE, data.MAX_BATCHSIZE, data.SentOutput, data.SentDeriv, data.SampleIdx, data.SentMargin, data.DeviceType)
        { }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
                if (Deriv != null) Deriv.Dispose(); Deriv = null;
            }
        }
    }

}
