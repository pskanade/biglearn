using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class Tensor4DData : MatrixData
    {
        public override DataSourceID Type { get { return DataSourceID.Tensor4DData; } }

        public int Width { get; set; }
        public int Height { get; set; }
        public int Depth { get; set; }   // Color or Stack Image Number. 
        public int MaxBatchSize;
        
        public virtual int BatchSize
        {
            get { return Length / (Width * Height * Depth); }
            set
            {
                if (value > MaxBatchSize) throw new Exception(string.Format("BatchSize {0} should be smaller than Max BatchSize {1}", value, MaxBatchSize));
                Length = (Width * Height * Depth) * value;
            }
        }

        public Tensor4DData(int width, int height, int depth, int maxBatchSize, DeviceType device) 
                    : base(width * height * depth, maxBatchSize, device)
        {
            MaxBatchSize = maxBatchSize;
            Width = width;
            Height = height;
            Depth = depth;
        }


        public Tensor4DData(int width, int height, int depth, int maxBatchSize, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) 
                    : base(width * height * depth, maxBatchSize, data, deriv, device)
        {
            MaxBatchSize = maxBatchSize;
            Width = width;
            Height = height;
            Depth = depth;
        }
    }

}
