﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MatrixData : VectorData
    {
        public override DataSourceID Type { get { return DataSourceID.MatrixData; } }

        public int Column { get; set; }

        public int MaxRow;
        
        public virtual int Row
        {
            get { return Length / Column; }
            set
            {
                if (value > MaxRow) throw new Exception(string.Format("Row {0} should be smaller than Max Row {1}", value, MaxRow));
                Length = Column * value;
            }
        }

        public MatrixData(int column, int maxRow, DeviceType device) : base(column * maxRow, device)
        {
            Column = column;
            MaxRow = maxRow;
        }


        public MatrixData(int column, int maxRow, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) : base(column * maxRow, data, deriv, device)
        {
            Column = column;
            MaxRow = maxRow;
        }

        public MatrixData(LayerStructure layer) : this(layer.Neural_Out, layer.Neural_In, layer.weight, layer.WeightGrad, layer.DeviceType)
        { }

        public MatrixData(ImageDataSource imageSet) : this(imageSet.Stat.Width * imageSet.Stat.Height * imageSet.Stat.Depth, imageSet.Stat.MAX_BATCHSIZE, imageSet.Data, imageSet.Deriv, imageSet.DeviceType)
        { }

        public MatrixData(MatrixStructure matrix) : this(matrix.Dim, matrix.Size, matrix.Memory, matrix.MemoryGrad, matrix.DeviceType)
        { }

        public MatrixData(HiddenBatchData data) : this(data.Dim, data.MAX_BATCHSIZE, data.Output.Data, data.Deriv == null ? null : data.Deriv.Data, data.DeviceType)
        { }

        public MatrixData(SeqDenseBatchData data) : this(data.Dim, data.MAX_SENTSIZE, data.SentOutput, data.SentDeriv, data.DeviceType)
        { }

        public MatrixData(EmbedStructure data) : this(data.Dim, data.VocabSize, data.Embedding, data.EmbeddingGrad, data.DeviceType )
        { }

        /// <summary>
        /// column 1 matrix.
        /// </summary>
        /// <param name="data"></param>
        public MatrixData(VectorData data) : this(1, data.MaxLength, data.Output, data.Deriv, data.DeviceType)
        { }

        // public MatrixData(MatrixData data, bool isRef) : this(data.Column, data.MaxRow, 
        //         isRef ? data.Output : new CudaPieceFloat(data.MaxLength, data.DeviceType),
        //         isRef ? data.Deriv : new CudaPieceFloat(data.MaxLength, data.DeviceType), 
        //         data.DeviceType)
        // { }

    }

}
