﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Concurrent;

namespace BigLearn
{
    /// <summary>
    /// Data Type mush Support IData.
    /// </summary>
    /// <typeparam name="DataType"></typeparam>
    /// <typeparam name="MetaType"></typeparam>
    public class DataCashier<DataType, MetaType> : IDataCashier<DataType, MetaType>
        where DataType : BatchData<MetaType>, new()
        where MetaType : DataStat, new()
    {
        private MetaType stat = new MetaType();
        List<DataType> L1Caches = new List<DataType>();
        List<DataType> L2Caches = new List<DataType>();
        protected Stream IStream = null;
        protected BinaryReader Reader = null;
        protected long[] SeekPosition = null;

        protected string Name { get; set; }
        /// <summary>
        /// Single File Stream.
        /// </summary>
        /// <param name="binaryStream"></param>
        public DataCashier(string binaryStream)
            : this(binaryStream, string.Empty)
        { }

        /// <summary>
        /// Single File Stream with cursorStream
        /// </summary>
        /// <param name="binaryStream"></param>
        public DataCashier(string binaryStream, string cursorStream)
            : this(new BinaryReader(new FileStream(binaryStream, FileMode.Open, FileAccess.Read)), cursorStream)
        { }

        public DataCashier(BinaryReader reader, string cursorStream = "", string name = "")
        {
            Reader = reader;
            Stat.Init(Reader);
            IStream = Reader.BaseStream;
            Name = name;
            InitBlockPosition(cursorStream);
        }

        public DataCashier(MetaType m_stat, string name = "")
        {
            stat = m_stat;
            Name = name;
        }

        public void InitReader(BinaryReader reader, string cursorStream = "")
        {
            Reader = reader;
            IStream = Reader.BaseStream;
            Stat.Init(Reader);
            InitBlockPosition(cursorStream);
        }

        public MetaType Stat { get { return stat; } }

        #region PreL2 will be removed. It is not useful.
        bool IsPreL2 = false;
        
        public void PreL2()
        {
            PreL2(DeviceType.GPU);
        }

        /// <summary>
        /// Load all data into memory.
        /// </summary>
        /// <param name="device"></param>
        public void PreL2(DeviceType device)
        {
            int batIdx = 0;
            IStream.Seek(0, SeekOrigin.Begin);
            while (batIdx < Stat.TotalBatchNumber)
            {
                DataType d = new DataType();
                d.Init(Stat, device);
                d.Load(Reader);
                if (device == DeviceType.GPU) d.SyncFromCPU(); // (d);
                L2Caches.Add(d);
                batIdx += 1;
            }
            IsPreL2 = true;
        }
        #endregion.

        public IEnumerable<DataType> GetInstances(bool randomProcessor)
        {
            return GetInstances(randomProcessor, Util.URandom);
        }

        public IEnumerable<DataType> GetInstances(bool randomProcessor, Random randomizer)
        {
            //Random localRandom = new Random((int)DateTime.Now.TimeOfDay.TotalSeconds);
            if (IsPreL2)
            {
                if (randomProcessor)
                {
                    int[] RandomBatchIdx = Enumerable.Range(0, Stat.TotalBatchNumber).ToArray();
                    int batIdx = 0;
                    while (batIdx < TrainBatchNum && batIdx < Stat.TotalBatchNumber)
                    {
                        int randomPos = randomizer.Next(batIdx, Stat.TotalBatchNumber);
                        int id = RandomBatchIdx[randomPos];
                        RandomBatchIdx[randomPos] = RandomBatchIdx[batIdx];
                        RandomBatchIdx[batIdx] = id;
                        yield return L2Caches[id];
                        batIdx += 1;
                    }
                }
                else
                {
                    int batIdx = 0;
                    while (batIdx < TrainBatchNum && batIdx < Stat.TotalBatchNumber)
                    {
                        yield return L2Caches[batIdx];
                        batIdx += 1;
                    }
                }
            }
            else
            {
                BlockingCollection<int> dataBuffer = new BlockingCollection<int>(L1Caches.Count - 2);

                var disk2CPUParallel = Task.Run(() =>
                {
                    IStream.Seek(0, SeekOrigin.Begin);
                    if (randomProcessor)
                    {
                        int[] RandomBatchIdx = Enumerable.Range(0, Stat.TotalBatchNumber).ToArray();
                        int batIdx = 0;
                        while (batIdx < TrainBatchNum && batIdx < Stat.TotalBatchNumber)
                        {
                            int randomPos = randomizer.Next(batIdx, Stat.TotalBatchNumber);
                            int id = RandomBatchIdx[randomPos];
                            RandomBatchIdx[randomPos] = RandomBatchIdx[batIdx];
                            RandomBatchIdx[batIdx] = id;

                            IStream.Position = SeekPosition[id];
                            L1Caches[batIdx % L1Caches.Count].Load(Reader);
                            dataBuffer.Add(batIdx % L1Caches.Count);
                            batIdx += 1;
                        }
                    }
                    else
                    {
                        int batIdx = 0;
                        while (batIdx < TrainBatchNum && batIdx < Stat.TotalBatchNumber)
                        {
                            L1Caches[batIdx % L1Caches.Count].Load(Reader);
                            dataBuffer.Add(batIdx % L1Caches.Count);
                            batIdx += 1;
                        }
                    }
                    // Let consumer know we are done.
                    dataBuffer.CompleteAdding();
                });

                if (L2Caches.Count > 2)
                {
                    BlockingCollection<int> gpuBuffer = new BlockingCollection<int>(L2Caches.Count - 2);
                    var cpu2gpuParallel = Task.Run(() =>
                    {
                        Cudalib.AttachToInitialContext();
                        int batIdx = 0;
                        while (!dataBuffer.IsCompleted)
                        {
                            try
                            {
                                int l1Idx = dataBuffer.Take(); ///CPU buffer.
                                L2Caches[batIdx % L2Caches.Count].CopyFrom(L1Caches[l1Idx]);
                                gpuBuffer.Add(batIdx % L2Caches.Count);
                                batIdx += 1;
                            }
                            catch (InvalidOperationException)
                            {
                            }
                        }
                        gpuBuffer.CompleteAdding();
                    });

                    while (!gpuBuffer.IsCompleted)
                    {
                        int gpuId = -1;
                        try
                        {
                            gpuId = gpuBuffer.Take();
                        }
                        catch (InvalidOperationException) { }

                        if (gpuId != -1) yield return L2Caches[gpuId];
                    }
                }
                else
                {
                    while (!dataBuffer.IsCompleted)
                    {
                        int cpuId = -1;
                        try
                        {
                            cpuId = dataBuffer.Take();
                        }
                        catch (InvalidOperationException)
                        {

                        }

                        if (cpuId != -1)
                        {
                            yield return L1Caches[cpuId];
                        }
                    }
                }
            }
        }


        public void InitPipelineCashier(int l1Cache, int l2Cache)
        {
            for (int i = 0; i < l1Cache; i++)
            {
                L1Caches.Add(new DataType());
                L1Caches[i].Init(Stat, DeviceType.CPU);
            }

            for (int i = 0; i < l2Cache; i++)
            {
                L2Caches.Add(new DataType());
                L2Caches[i].Init(Stat, DeviceType.GPU);
            }
        }

        void InitBlockPosition(string cursorFile)
        {
            SeekPosition = new long[Stat.TotalBatchNumber];
            IStream.Seek(0, SeekOrigin.Begin);

            if (!cursorFile.Equals(string.Empty) && File.Exists(cursorFile))
            {
                using (StreamReader cursorReader = new StreamReader(cursorFile))
                {
                    for (int i = 0; i < Stat.TotalBatchNumber; i++)
                    {
                        //if (i % 1000 == 0) Console.Write(".");

                        SeekPosition[i] = long.Parse(cursorReader.ReadLine());
                    }
                }
            }
            else
            {
                for (int i = 0; i < Stat.TotalBatchNumber; i++)
                {
                    SeekPosition[i] = IStream.Position;
                    new DataType().SkipBlock(Reader, Stat);
                    //if (i % 1000 == 0) Console.Write(".");
                }

                if (!cursorFile.Equals(string.Empty))
                {
                    using (StreamWriter cursorWriter = new StreamWriter(cursorFile))
                    {
                        for (int i = 0; i < Stat.TotalBatchNumber; i++) cursorWriter.WriteLine(SeekPosition[i]);
                    }
                }
            }
        }

        bool DebugMode = false;
        int DebugBatchNum = 0;
        public int TrainBatchNum { get { return DebugMode ? DebugBatchNum : Stat.TotalBatchNumber; } set { if (value > 0) { DebugMode = true; DebugBatchNum = value; } } }


        IEnumerator<DataType> DataFlow { get; set; }
        object DataFlowLock = new object();
        bool IsRandom { get; set; }
        Random dataRandom;

        public void InitThreadSafePipelineCashier(int l1Cache, bool isRandom)
        {
            IsRandom = isRandom;
            InitPipelineCashier(l1Cache, 0);
            dataRandom = new Random(ParameterSetting.RANDOM_SEED);//Util.URandom;
        }

        public void InitThreadSafePipelineCashier(int l1Cache, int groupRandomSeed)
        {
            IsRandom = true;
            dataRandom = new Random(groupRandomSeed);
            InitPipelineCashier(l1Cache, 0);
        }

        bool IsReseted = false;

        /// <summary>
        /// Only need to start once. 
        /// </summary>
        public void ResetDataFlow()
        {
            if (!IsReseted)
            {
                IsReseted = true;
                DataFlow = GetInstances(IsRandom, dataRandom).GetEnumerator();
            }
        }

        /// <summary>
        /// ThreadSafe for Fetching Training Data.
        /// </summary>
        /// <param name="output"> DataType must have IData Interface.</param>
        /// <returns></returns>
        public bool FetchDataThreadSafe(DataType output)
        {
            lock (DataFlowLock)
            {
                if (DataFlow.MoveNext())
                {
                    using (var stream = new MemoryStream())
                    {
                        /*  This is very Slow!! Please Use CopyFrom Instead.*/
                        //var sw = new BinaryWriter(stream);
                        //DataFlow.Current.Save(sw);
                        //sw.Flush();
                        //stream.Position = 0;
                        //var sr = new BinaryReader(stream);
                        //output.Load(sr);
                        output.CopyFrom(DataFlow.Current);
                    }
                    return true;
                }
                else
                {
                    output.Clear();
                    IsReseted = false;
                    return false;
                }
            }
        }

        public void Dispose()
        {
            for (int i = 0; i < L1Caches.Count; i++)
            {
                L1Caches[i].Dispose();
            }

            for (int i = 0; i < L2Caches.Count; i++)
            {
                L2Caches[i].Dispose();
            }
            Reader.Close();
            IStream.Close();
        }

        public void CloseDataFlow()
        {
            Reader.Close();
            IStream.Close();
        }
    }

    /// <summary>
    /// Data Runner Support ThreadSafe.
    /// </summary>
    /// <typeparam name="DataType"></typeparam>
    /// <typeparam name="MetaType"></typeparam>
    public class DataRunner<DataType, MetaType> : StructRunner
        where DataType : BatchData<MetaType>, new()
        where MetaType : DataStat, new()
    {
        public new DataType Output { get { return (DataType)base.Output; } protected set { base.Output = value; } }

        IDataCashier<DataType, MetaType> DataInterface { get; set; }

        public DataRunner(IDataCashier<DataType, MetaType> dataCashier, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            /// Suppose we have a data factory.
            DataInterface = dataCashier;
            Output = new DataType(); // DataInterface.Stat, Behavior.Device);
            Output.Init(DataInterface.Stat, Behavior.Device);
        }

        public DataRunner(IDataCashier<DataType, MetaType> dataCashier, DataType cache, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            DataInterface = dataCashier;
            Output = cache;
        }

        public override void Init()
        {
            IsTerminate = false;
            IsContinue = true;
            DataInterface.ResetDataFlow();
        }

        public override void Forward()
        {
            // Perf is not threadSafe.
            //var dataCounter = PerfCounter.Manager.Instance["Data"].Begin(); 
            Output.Clear();
            if (!DataInterface.FetchDataThreadSafe(Output)) IsTerminate = true;
            //PerfCounter.Manager.Instance["Data"].TakeCount(dataCounter);
        }

        public override void Complete()
        {
            
        }
    }

    /// <summary>
    /// Data Runner Support ThreadSafe.
    /// </summary>
    /// <typeparam name="DataType"></typeparam>
    /// <typeparam name="MetaType"></typeparam>
    //public class EnumlateDataRunner<DataType, MetaType> : StructRunner
    //    where DataType : BatchData<MetaType>, new()
    //    where MetaType : DataStat, new()
    //{
    //    public new DataType Output { get { return (DataType)base.Output; } protected set { base.Output = value; } }

    //    IEnumerator<DataType> DataFlow;
    //    public EnumlateDataRunner(IEnumerable<DataType> dataFlow, MetaType stat, RunnerBehavior behavior)
    //        : base(Structure.Empty, behavior)
    //    {
    //        DataFlow = dataFlow.GetEnumerator();
    //        Output = new DataType(); Output.Init(stat == null ? new MetaType() : stat, Behavior.Device);
    //    }

    //    public override void Init()
    //    {
    //        IsTerminate = false;
    //        IsContinue = true;
    //    }

    //    public override void Forward()
    //    {
    //        if (DataFlow.MoveNext())
    //        {
    //            Output.CopyFrom(DataFlow.Current);
    //        }
    //        else
    //        {
    //            Output.Clear();
    //            IsTerminate = true;
    //            IsContinue = false;
    //        }
    //    }

    //}
}
