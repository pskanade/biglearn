﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// To Pengcheng : Why we need Local Data Cashier?
    /// Answer : it is used for cosmos stream and http stream.
    /// </summary>
    /// <typeparam name="DataType"></typeparam>
    /// <typeparam name="MetaType"></typeparam>
    public class LocalDataCashier<DataType, MetaType> : IDataCashier<DataType, MetaType>
        where DataType : IData, new()
        where MetaType : IMetaInfo, new()
    {
        public MetaType Stat { get; private set; }
        DataType[] L1Caches = null;
        DataType[] L2Caches = null;
        BlockingCollection<int> L1DataBuffer = null;
        BlockingCollection<int> L2DataBuffer = null;

        RandomDataReader reader;
        IOUnitFactory factory = null;

        public LocalDataCashier(string path)
        {
            this.factory = new CachableIOUnitFactory();
            this.reader = new RandomDataReader(new FileStream(path, FileMode.Open, FileAccess.Read), this.factory);
            this.Stat = (MetaType)reader.Meta;
        }

        public LocalDataCashier(RandomDataReader dataReader, MetaType meta)
        {
            this.factory = dataReader.DataFactory;
            this.reader = dataReader;
            this.Stat = meta;
        }

        ~LocalDataCashier()
        {
            this.Dispose(false);
        }

        public IEnumerable<DataType> GetInstances(bool randomProcessor)
        {
            return this.L2Instances(randomProcessor);
        }

        protected IEnumerable<DataType> L2Instances(bool randomProcessor)
        {
            if (this.L2Caches == null || this.L2Caches.Length <= 2)
            {
                foreach (var d in this.L1Instances(randomProcessor))
                {
                    yield return d;
                }
            }
            else
            {
                Task feedingTask = this.FeedingL2Cache(randomProcessor);
                while (!L2DataBuffer.IsCompleted)
                {
                    int cpuId = -1;
                    try
                    {
                        cpuId = L2DataBuffer.Take();
                    }
                    catch (InvalidOperationException)
                    {

                    }

                    if (cpuId != -1)
                    {
                        yield return L2Caches[cpuId];
                    }
                }

                feedingTask.Wait();
                feedingTask.Dispose();                
            }
        }

        protected IEnumerable<DataType> L1Instances(bool randomProcessor)
        {
            Task feedingTask = FeedingL1Cache(randomProcessor);

            while (!L1DataBuffer.IsCompleted)
            {
                int cpuId = -1;
                try
                {
                    cpuId = L1DataBuffer.Take();
                }
                catch (InvalidOperationException)
                {

                }

                if (cpuId != -1)
                {
                    yield return L1Caches[cpuId];
                }
            }

            feedingTask.Wait();
            feedingTask.Dispose();
        }

        private Task FeedingL2Cache(bool randomProcessor)
        {
            this.L2DataBuffer = new BlockingCollection<int>(this.L2Caches.Length - 2);
            var disk2CPUParallel = Task.Run(() =>
            {
                try
                {
                    Cudalib.AttachToInitialContext();

                    int batIdx = 0;
                    foreach (var d in this.L1Instances(randomProcessor))
                    {
                        if (L2Caches[batIdx % L2Caches.Length] == null)
                        {
                            L2Caches[batIdx % L2Caches.Length] = (DataType)d.CloneAs(DeviceType.GPU);
                        }
                        else
                        {
                            L2Caches[batIdx % L2Caches.Length].CopyFrom(d);
                        }

                        L2DataBuffer.Add(batIdx % L2Caches.Length);
                        batIdx++;
                    }
                }
                finally
                {
                    L2DataBuffer.CompleteAdding();
                }
            });

            return disk2CPUParallel;
        }

        private Task FeedingL1Cache(bool randomProcessor)
        {
            this.L1DataBuffer = new BlockingCollection<int>(L1Caches.Length - 2);
            var disk2CPUParallel = Task.Run(() =>
            {
                try
                {
                    reader.Reset();
                    IEnumerable<IData> dataEnumerator = null;
                    if (randomProcessor)
                    {
                        dataEnumerator = reader.Shuffle();
                    }
                    else
                    {
                        dataEnumerator = reader.SequentialDatas();
                    }

                    int batIdx = 0;
                    foreach (var d in dataEnumerator)
                    {
                        // Check/skip the datatype cast
                        if (!(d is DataType))
                        {
                            continue;
                        }
                        int idx = batIdx % L1Caches.Length;
                        DataType pred = L1Caches[idx];
                        if (pred != null)
                        {
                            this.factory.FreeIOUnit(pred);
                        }

                        L1Caches[idx] = (DataType)d;
                        L1DataBuffer.Add(idx);
                        batIdx++;
                    }
                }
                finally
                {
                    // Let consumer know we are done.
                    L1DataBuffer.CompleteAdding();
                }
            });

            return disk2CPUParallel;
        }

        public void InitPipelineCashier(int l1Cache, int l2Cache)
        {
            this.L1Caches = new DataType[l1Cache];
            if (l2Cache > 2)
            {
                this.L2Caches = new DataType[l2Cache];
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;
            if (disposing)
            {
                if (this.L2Caches != null)
                {
                    for (int i = 0; i < this.L2Caches.Length; i++)
                    {
                        if (this.L2Caches[i] != null)
                        {
                            this.L2Caches[i].Dispose();
                        }

                        this.L2Caches[i] = default(DataType);
                    }

                    this.L2Caches = null;
                }

                if (this.L1Caches != null)
                {
                    for (int i = 0; i < this.L1Caches.Length; i++)
                    {
                        if (this.L1Caches[i] != null)
                        {
                            this.L1Caches[i].Dispose();
                        }
                        
                        this.L1Caches[i] = default(DataType);
                    }

                    this.L1Caches = null;
                }
                                
                if (this.L1DataBuffer!=null)
                {
                    this.L1DataBuffer.Dispose();
                }
                
                if (this.L2DataBuffer != null)
                {                    
                    this.L2DataBuffer.Dispose();
                }

                this.reader.Dispose();
            }

            this.L2Caches = null;
            this.L1Caches = null;
            this.L1DataBuffer = null;
            this.L2DataBuffer = null;
            this.reader = null;
            this.factory = null;
        }


        IEnumerator<DataType> DataFlow { get; set; }
        object DataFlowLock = new object();
        bool IsRandom { get; set; }
        public void InitThreadSafePipelineCashier(int l1Cache, bool isRandom)
        {
            IsRandom = isRandom;
            InitPipelineCashier(l1Cache, 0);
        }

        public void ResetDataFlow()
        {
            DataFlow = GetInstances(IsRandom).GetEnumerator();
        }

        /// <summary>
        /// ThreadSafe for Fetching Training Data.
        /// </summary>
        /// <param name="output"> DataType must have IData Interface.</param>
        /// <returns></returns>
        public bool FetchDataThreadSafe(DataType output)
        {
            lock (DataFlowLock)
            {
                if (DataFlow.MoveNext())
                {
                    using (var stream = new MemoryStream())
                    {
                        var sw = new BinaryWriter(stream);
                        DataFlow.Current.Save(sw);
                        sw.Flush();
                        stream.Position = 0;
                        var sr = new BinaryReader(stream);
                        output.Load(sr);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void CloseDataFlow()
        {
            //throw new NotImplementedException();
        }
    }
}
