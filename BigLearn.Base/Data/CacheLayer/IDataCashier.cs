﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public interface IDataCashier<DataType, MetaType> : IDisposable 
    {
        /// <summary>
        /// Meta Data for Instances.
        /// </summary>
        MetaType Stat { get; } 

        /// <summary>
        /// Get Instances from Stream.
        /// </summary>
        /// <param name="randomProcessor"></param>
        /// <returns></returns>
        IEnumerable<DataType> GetInstances(bool randomProcessor);

        /// <summary>
        /// Initialize Data Cache.
        /// </summary>
        /// <param name="l1"></param>
        /// <param name="l2"></param>
        void InitPipelineCashier(int l1, int l2);

        /// <summary>
        /// Only L1 Cache Works.
        /// </summary>
        /// <param name="l1Cache"></param>
        /// <param name="isRandom"></param>
        void InitThreadSafePipelineCashier(int l1Cache, bool isRandom);

        /// <summary>
        /// Fetch Data From DataCashier.
        /// It could support multiple consumers.
        /// </summary>
        /// <param name="output"></param>
        /// <returns></returns>
        bool FetchDataThreadSafe(DataType output);

        void ResetDataFlow();

        void CloseDataFlow();
    }
}
