﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SparseMatrixData : VectorData
    {
        public override DataSourceID Type { get { return DataSourceID.SparseMatrixData; } }

        public CudaPieceInt SampleIdx;
        public CudaPieceInt FeatureIdx;
        public CudaPieceFloat FeatureValue { get { return Output; } }
        public int Column { get; set; }

        public int MaxRow;
        public virtual int Row
        {
            get { return SampleIdx.EffectiveSize; }
            set
            {
                if (value > MaxRow) throw new Exception(string.Format("Row {0} should be smaller than Max Row {1}", value, MaxRow));
                SampleIdx.EffectiveSize = value;
            }
        }
        
        public override int Length
        {
            get
            {
                return base.Length;
            }
            set
            {
                base.Length = value;
                FeatureIdx.EffectiveSize = value;
            }
        }

        public SparseMatrixData(int column, int maxRow, int maxLength, DeviceType device, bool isDeriv = true) : this(column, maxRow, maxLength,
            new CudaPieceInt(maxRow, device), 
            new CudaPieceInt(maxLength, device), 
            new CudaPieceFloat(maxLength, device), 
            isDeriv ? new CudaPieceFloat(maxLength, device) : CudaPieceFloat.Empty, device)
        {
        }

        public SparseMatrixData(int column, int maxRow, int maxLength, CudaPieceInt smpIdx, CudaPieceInt feaIdx, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) : base(maxLength, data, deriv, device)
        {
            SampleIdx = smpIdx;
            FeatureIdx = feaIdx;
            Column = column;
            MaxRow = maxRow;
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
                if (Deriv != null) Deriv.Dispose(); Deriv = null;
            }
        }
        public override void SyncFromCPU()
        {
            SampleIdx.SyncFromCPU();
            FeatureIdx.SyncFromCPU();
            FeatureValue.SyncFromCPU();
        }

        public override void SyncToCPU()
        {
            SampleIdx.SyncToCPU();
            FeatureIdx.SyncToCPU();
            FeatureValue.SyncToCPU();
        }
    }
}
