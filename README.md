BigLearn : Deep Learning with Big Data (The BigLearn Platform can only be used for research purpose). 


Enviroument Setup (Linux):
Step 1 : Install Anaconda2; 
	wget https://repo.continuum.io/archive/Anaconda2-5.2.0-Linux-x86.sh 
	sh Anaconda2-5.2.0-Linux-x86_64.sh

Step 2 : Install Mono
    conda install -c conda-forge mono 

Step 3 : Install Cuda 8.0
	suppose you have installed.
	

Task 1: Embedding Knowledge Graph for Knowledge Base Completion

Data:
	Download FB15k and WN18 from the https://github.com/thunlp/KB2E 

Train:

        BigLearn.DeepNet.exe Config/ekgn_embedding.config PATH ./FB15k(the data folder) MODEL-PATH ./FB15kModel


Test:

        BigLearn.DeepNet.exe Config/ekgn_embedding.config RUN-MODE 1 PATH ./FB15k(the data folder) MODEL-PATH ./FB15kModel 


Task 2: ReasoNet for MRC

Prediction:

    Test dataset : dev-v1.1.json 
		(https://worksheets.codalab.org/bundles/0x8f29fe78ffe545128caccab74eb06c57/)
		
	Model Path: model.v62_13.12 
		(https://worksheets.codalab.org/bundles/0x083d4dfa9e9d4eccae1a70fb2a15d848/)


    Step 1. Data Preprocess.
		sh data-script/preprocessV21.sh dev-v1.1.json
	
	Step 2. Predict.
		BigLearn.DeepNet.exe model.v62_13.12/ReasoNetReader.nltk.V62_13.config

