## Single Model ReasoNet Predict Pipeline

#python3 "$(dirname "$0")/"preproV2.py -i $1 -o tmp.nltk -p 0  #-is_train True

#wc -l $2.ids
#wc -l $2.ques

#python3 "$(dirname "$0")/"vocab.py tmp.nltk.ques tmp.nltk.para tmp.nltk.vocab 

#wc -l tmp.nltk.vocab 

#mono "$(dirname "$0")/"RunScript-2-20-2017/BigLearn.DeepNet.exe "$(dirname "$0")/"model1/ReasoNetReader.nltk.V17_1.config 

#python3 "$(dirname "$0")/"ensemble_restore.py --span_path cook/tmp.nltk.spans --raw_path cook/tmp.nltk.doc --ids_path cook/tmp.nltk.ids --fout $1 --seqlen 0 --inpaths score.v17_17_1.17.dump,score.v51_18_5.10.dump,score.v51_18_5.11.dump,score.v51_18_5.12.dump,score.v51_18_5.13.dump,score.v51_18_6.18.dump,score.v51_18_6.18.dump,score.v51_18_6.19.dump,score.v51_18_6.20.dump,score.v52_18_6.20.dump,score.v52_18_6.21.dump,score.v52_18_6.22.dump,score.v52_18_6.23.dump,score.v51_18_9.17.dump,score.v51_18_9.18.dump,score.v51_18_9.19.dump,score.v53_18_9.17.dump,score.v53_18_9.18.dump,score.v53_18_9.19.dump,score.v55_18_9.20.dump,score.v55_18_9.21.dump,score.v55_18_9.22.dump,score.v59_18_9.17.dump,score.v59_18_9.18.dump,score.v59_18_9.19.dump,score.v58_18_9.15.dump,score.v58_18_9.16.dump,score.v58_18_9.17.dump,score.v57_18_9.18.dump,score.v57_18_9.19.dump,score.v57_18_9.20.dump,score.v56_18_9.11.dump,score.v56_18_9.12.dump,score.v56_18_9.13.dump,score.v54_18_9.19.dump,score.v54_18_9.20.dump,score.v54_18_9.21.dump
# {"f1": 80.86358279409546, "exact_match": 72.2705771050142}
# {"f1": 80.94385189355248, "exact_match": 72.32734153263955}
# {"f1": 80.96689086929787, "exact_match": 72.35572374645223}
# {"f1": 81.03577232355411, "exact_match": 72.49763481551561}



# score.v17_17_1.17.dump	{"f1": 78.76680722303045, "exact_match": 69.22421948912014}	top1

# score.v51_18_9.18.dump	{"f1": 78.11396962621174, "exact_match": 68.22138126773888}
# score.v53_18_9.18.dump	{"f1": 78.2367164495038, "exact_match": 68.61873226111636}	top8
# score.v54_18_9.20.dump	{"f1": 78.06664382547743, "exact_match": 68.30652790917692}
# score.v55_18_9.20.dump	{"f1": 78.63904903862586, "exact_match": 69.03500473036897}	top2
# score.v56_18_9.12.dump	{"f1": 78.13545191143369, "exact_match": 68.47682119205298}
# score.v57_18_9.18.dump	{"f1": 78.2932563958431, "exact_match": 68.61873226111636}	top7	score.v57_18_9.18.dump,
# score.v58_18_9.16.dump	{"f1": 77.95994280522207, "exact_match": 67.97540208136235}
# score.v59_18_9.18.dump	{"f1": 78.39342030481222, "exact_match": 68.39167455061495}

# score.v57_18_10.16.dump	{"f1": 78.20526064636843, "exact_match": 68.6565752128666}
# score.v59_18_10.15.dump	{"f1": 78.4512131237556, "exact_match": 68.7511825922422}	top3
# score.v53_18_10.16.dump	{"f1": 78.22502133701623, "exact_match": 68.74172185430463}	top9
# score.v54_18_10.16.dump	{"f1": 78.42236158037218, "exact_match": 68.6565752128666}	top5
# score.v55_18_10.16.dump	{"f1": 78.03870229386884, "exact_match": 68.7511825922422}	
# score.v56_18_10.17.dump	{"f1": 78.51528121656663, "exact_match": 68.52412488174078}	top6	
# score.v51_18_10.20.dump	{"f1": 78.40189930678677, "exact_match": 68.64711447492904}	top10
# score.v52_18_10.18.dump	{"f1": 78.31915851770535, "exact_match": 68.7511825922422}	top4

# score.v52_18_11.15.dump	{"f1": 78.27050312943017, "exact_match": 68.67549668874172}
# score.v51_18_11.12.dump	{"f1": 78.28111400064365, "exact_match": 68.45789971617786}

# score.v51_18_5.10.dump	{"f1": 77.77120103387627, "exact_match": 67.73888363292336}
 
# score.v51_18_6.18.dump	{"f1": 77.9842424479081, "exact_match": 67.2942289498581}
# score.v52_18_6.20.dump	{"f1": 78.14763128987221, "exact_match": 68.22138126773888}

# score.v61_1.17.dump	{"f1": 78.49066389139406, "exact_match": 68.52412488174078}	top11	
# score.v62_1.16.dump	{"f1": 78.42861843367541, "exact_match": 68.76064333017976}	top12
# score.v61_2.20.dump	{"f1": 78.41430813534572, "exact_match": 68.68495742667928}	top13
# score.v65_1.16.dump	{"f1": 78.47381055431615, "exact_match": 68.77010406811732}	top14
# score.v68_1.18.dump	{"f1": 78.68185082670732, "exact_match": 69.3755912961211}	top15
# score.v69_1.17.dump	{"f1": 78.55326308467065, "exact_match": 68.90255439924314}	top16
# score.v61_3.20.dump	{"f1": 78.66732913599839, "exact_match": 69.16745506149479}	top17
# score.v67_1.15.dump	{"f1": 78.74134778372361, "exact_match": 69.29990539262063}	top18


#python3 "$(dirname "$0")/"ensemble_restore.py --span_path cook/tmp.nltk.spans --raw_path cook/tmp.nltk.doc --ids_path cook/tmp.nltk.ids --fout $1 --seqlen 0 --inpaths score.v17_17_1.17.dump,score.v55_18_9.20.dump,score.v59_18_10.15.dump,score.v52_18_10.18.dump,score.v54_18_10.16.dump,score.v53_18_9.18.dump,score.v53_18_10.16.dump,score.v51_18_10.20.dump,score.v62_1.16.dump,score.v61_2.20.dump,score.v65_1.16.dump,score.v68_1.18.dump,score.v69_1.17.dump,score.v56_18_10.17.dump,score.v61_1.17.dump,score.v61_3.20.dump,score.v67_1.15.dump

# score.v62_13.12.dump	{"f1": 79.74327374535383, "exact_match": 70.69063386944181}
# score.v61_14.11.dump	{"f1": 79.49306156474731, "exact_match": 70.69063386944181}
# score.v61_16.15.dump	{"f1": 79.63878153209598, "exact_match": 70.61494796594134}
# score.v61_17.14.dump	{"f1": 79.35059211297778, "exact_match": 70.55818353831599}
# score.v62_14.16.dump	{"f1": 79.6130260777732, "exact_match": 70.50141911069063}

# score.v61_12.12.dump	{"f1": 79.5056133709497, "exact_match": 70.44465468306528}	New2
# score.v62_15.11.dump	{"f1": 79.1174445009809, "exact_match": 70.35004730368969}
# score.v62_12.11.dump	{"f1": 79.55637124697893, "exact_match": 70.3027436140019}
# score.v72_1.17.dump	{"f1": 79.22993209427177, "exact_match": 70.28382213812678}	New
# score.v62_10.20.dump	{"f1": 79.07937018193566, "exact_match": 70.22705771050141}	New2
# score.v62_11.9.dump	{"f1": 79.30248137058854, "exact_match": 70.21759697256385}
# score.v61_13.14.dump	{"f1": 79.24186754211722, "exact_match": 70.21759697256385}
# score.v61_11.15.dump	{"f1": 79.18878231915025, "exact_match": 70.18921475875118}	New
# score.v72_2.12.dump	{"f1": 79.45995367065959, "exact_match": 70.18921475875118}
# score.v61_15.14.dump	{"f1": 78.82473372509044, "exact_match": 70.15137180700094}
# score.v62_9.17.dump	{"f1": 79.21131374502187, "exact_match": 70.10406811731315}	New
# score.v67_1.17.dump	{"f1": 78.91314091902589, "exact_match": 69.47965941343425}	top1	selected
# score.v68_1.18.dump	{"f1": 78.68185082670732, "exact_match": 69.3755912961211}	top2
# score.v61_5.21.dump	{"f1": 78.6719578203713, "exact_match": 69.36613055818354}	
# score.v68_3.20.dump	{"f1": 78.73104048797514, "exact_match": 69.27152317880795}
# score.v64_5.16.dump	{"f1": 78.6968067361122, "exact_match": 69.22421948912014}	New
# score.v17_17_1.17.dump	{"f1": 78.76680722303045, "exact_match": 69.22421948912014}	top3	selected
# score.v61_3.20.dump	{"f1": 78.66732913599839, "exact_match": 69.16745506149479}	top4
# score.v62_6.19.dump	{"f1": 78.63380550172268, "exact_match": 69.120151371807}
# score.v69_3.24.dump	{"f1": 78.42483291916827, "exact_match": 69.11069063386944}
# score.v68_5.18.dump	{"f1": 78.67110982378256, "exact_match": 69.11069063386944}	New
# score.v55_18_9.20.dump	{"f1": 78.63904903862586, "exact_match": 69.03500473036897}	top5	selected
# score.v65_2.22.dump	{"f1": 78.64670033214247, "exact_match": 69.03500473036897}	top5-6
# score.v69_1.17.dump	{"f1": 78.55326308467065, "exact_match": 68.90255439924314}	top6	selected	{"f1": 81.173917593579, "exact_match": 72.8949858088931}
#									{"f1": 81.19455903235774, "exact_match": 72.96121097445601}
# score.v62_4.20.dump	{"f1": 78.51547085408677, "exact_match": 68.94039735099338}	top15
# score.v61_4.19.dump	{"f1": 78.56286706641067, "exact_match": 68.89309366130558}	top16
# score.v64_3.21.dump	{"f1": 78.61268832834982, "exact_match": 68.85525070955535}	top18
# score.v63_3.18.dump	{"f1": 78.44351246946552, "exact_match": 68.81740775780511}	top17
# 
# score.v63_2.16.dump	{"f1": 78.33085924645067, "exact_match": 68.79848628193}	top7		{"f1": 81.13271201360634, "exact_match": 72.86660359508042}
# score.v62_5.21.dump	{"f1": 78.64969772948793, "exact_match": 68.77956480605488}	
# score.v64_2.19.dump	{"f1": 78.87371045124094, "exact_match": 68.77010406811732}	top14
# score.v65_1.16.dump	{"f1": 78.47381055431615, "exact_match": 68.77010406811732}	top7	selected
# score.v62_1.16.dump	{"f1": 78.42861843367541, "exact_match": 68.76064333017976}	top7	selected	{"f1": 81.31454273194068, "exact_match": 72.98959318826869}
# score.v67_5.15.dump	{"f1": 78.58902364765181, "exact_match": 68.76064333017976}	New
# score.v59_18_10.15.dump	{"f1": 78.4512131237556, "exact_match": 68.7511825922422}	top8		{"f1": 81.17277853147935, "exact_match": 72.87606433301798}
# score.v52_18_10.18.dump	{"f1": 78.31915851770535, "exact_match": 68.7511825922422}	top8	selected	{"f1": 81.2587028822099, "exact_match": 72.94228949858089}
# score.v55_18_10.16.dump	{"f1": 78.03870229386884, "exact_match": 68.7511825922422}	top9		{"f1": 81.18125599314342, "exact_match": 73.01797540208136}
# score.v53_18_10.16.dump	{"f1": 78.22502133701623, "exact_match": 68.74172185430463}	top10	selected
# score.v71_1.18.dump	{"f1": 78.39787932276215, "exact_match": 68.73226111636707}
# score.v69_2.13.dump	{"f1": 78.49769823593606, "exact_match": 68.69441816461683}	top11
# score.v65_3.19.dump	{"f1": 78.41063642276517, "exact_match": 68.69441816461683}
# score.v61_2.20.dump	{"f1": 78.41430813534572, "exact_match": 68.68495742667928}	top12
# score.v52_18_11.15.dump	{"f1": 78.27050312943017, "exact_match": 68.67549668874172}	top12	selected
# score.v54_18_10.16.dump	{"f1": 78.42236158037218, "exact_match": 68.6565752128666}	top12
# score.v57_18_10.16.dump	{"f1": 78.20526064636843, "exact_match": 68.6565752128666}	top12
# score.v51_18_10.20.dump	{"f1": 78.40189930678677, "exact_match": 68.64711447492904}	top12	selected

# score.v57_18_9.18.dump	{"f1": 78.2932563958431, "exact_match": 68.61873226111636}	top13
# score.v53_18_9.18.dump	{"f1": 78.2367164495038, "exact_match": 68.61873226111636}	top13
# score.v66_3.20.dump	{"f1": 78.32274071269589, "exact_match": 68.59981078524125}	
# score.v67_2.19.dump	{"f1": 78.09830042701236, "exact_match": 68.55250709555345}
# score.v56_18_10.17.dump	{"f1": 78.51528121656663, "exact_match": 68.52412488174078}	top13
# score.v61_1.17.dump	{"f1": 78.49066389139406, "exact_match": 68.52412488174078}	top13

# score.v68_2.13.dump	{"f1": 78.3877868809936, "exact_match": 68.51466414380322}	topN
# score.v56_18_9.12.dump	{"f1": 78.13545191143369, "exact_match": 68.47682119205298}	topN	nouse
# score.v51_18_11.12.dump	{"f1": 78.28111400064365, "exact_match": 68.45789971617786}	topN	nouse


list="score.v67_1.17.dump score.v68_1.18.dump score.v17_17_1.17.dump score.v61_3.20.dump score.v55_18_9.20.dump score.v65_2.22.dump score.v69_1.17.dump score.v63_2.16.dump score.v64_2.19.dump score.v65_1.16.dump score.v62_1.16.dump score.v59_18_10.15.dump score.v52_18_10.18.dump score.v55_18_10.16.dump score.v53_18_10.16.dump score.v69_2.13.dump score.v61_2.20.dump score.v52_18_11.15.dump score.v54_18_10.16.dump score.v57_18_10.16.dump score.v51_18_10.20.dump score.v57_18_9.18.dump score.v53_18_9.18.dump score.v56_18_10.17.dump score.v61_1.17.dump score.v68_2.13.dump score.v62_4.20.dump score.v61_4.19.dump score.v64_3.21.dump score.v63_3.18.dump score.v65_3.19.dump score.v66_3.20.dump score.v61_5.21.dump score.v62_5.21.dump score.v67_2.19.dump score.v68_3.20.dump score.v69_3.24.dump score.v62_6.19.dump score.v64_5.16.dump score.v72_1.17.dump score.v61_11.15.dump score.v62_9.17.dump score.v68_5.18.dump score.v67_5.15.dump score.v61_12.12.dump score.v62_10.20.dump score.v72_2.12.dump score.v62_11.9.dump score.v62_10.20.dump score.v61_14.11.dump score.v62_12.11.dump score.v62_13.12.dump score.v61_15.14.dump score.v61_16.15.dump score.v62_14.16.dump score.v61_17.14.dump score.v62_15.11.dump"
#score.v69_1.17.dump score.v65_1.16.dump score.v62_1.16.dump score.v59_18_10.15.dump score.v52_18_10.18.dump score.v55_18_10.16.dump score.v53_18_10.16.dump score.v69_2.13.dump score.v61_2.20.dump score.v52_18_11.15.dump score.v54_18_10.16.dump score.v57_18_10.16.dump score.v51_18_10.20.dump score.v57_18_9.18.dump score.v53_18_9.18.dump score.v56_18_10.17.dump score.v61_1.17.dump score.v68_2.13.dump score.v56_18_9.12.dump score.v51_18_11.12.dump" 

#score.v51_18_10.20.dump 
#list=""
fileList=""

acceptList="score.v62_13.12.dump"
#"score.v17_17_1.17.dump,score.v65_3.19.dump,score.v59_18_10.15.dump,score.v57_18_10.16.dump,score.v53_18_10.16.dump,score.v55_18_9.20.dump,score.v56_18_10.17.dump,score.v51_18_10.20.dump,score.v62_4.20.dump,score.v54_18_10.16.dump,score.v64_2.19.dump,score.v64_3.21.dump,score.v52_18_10.18.dump,score.v63_3.18.dump,score.v61_1.17.dump,score.v65_1.16.dump,score.v67_1.17.dump"

#score.v64_2.19.dump,score.v62_10.20.dump,score.v68_5.18.dump,score.v68_1.18.dump,score.v62_12.11.dump,score.v65_2.22.dump,score.v61_12.12.dump,score.v62_6.19.dump,score.v61_15.14.dump,score.v61_14.11.dump,score.v64_3.21.dump,score.v62_9.17.dump,score.v17_17_1.17.dump,score.v69_3.24.dump,score.v61_16.15.dump,score.v62_14.16.dump,score.v61_5.21.dump,score.v62_13.12.dump
#{"f1": 82.98140830384375, "exact_match": 75.41154210028382}

#score.v62_10.20.dump,score.v51_18_10.20.dump,score.v68_5.18.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v61_11.15.dump,score..9.dump,score.v69_2.13.dump,score.v65_2.22.dump,score.v61_14.11.dump,score.v62_12.11.dump,score.v64_3.21.dump,score.v61_3.20.dump,score5.14.dump,score.v52_18_10.18.dump,score.v61_12.12.dump,score.v72_2.12.dump,score.v69_3.24.dump,score.v62_9.17.dump,score.v61_5.21.dump,v62_13.12.dump
#{"f1": 82.8544265096938, "exact_match": 75.42100283822138}

#score.v51_18_10.20.dump,score.v68_5.18.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v61_11.15.dump,score.v62_11.9.dump,score.v3.dump,score.v65_2.22.dump,score.v61_14.11.dump,score.v62_12.11.dump,score.v64_3.21.dump,score.v61_3.20.dump,score.v61_15.14.dump,score8_10.18.dump,score.v61_12.12.dump,score.v72_2.12.dump,score.v69_3.24.dump,score.v62_9.17.dump,score.v61_5.21.dump,score.v62_13.12.dump
#{"f1": 82.88972675627386, "exact_match": 75.42100283822138}

#score.v68_5.18.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v61_11.15.dump,score.v62_11.9.dump,score.v69_2.13.dump,score.v65_2mp,score.v61_14.11.dump,score.v62_12.11.dump,score.v64_3.21.dump,score.v61_3.20.dump,score.v61_15.14.dump,score.v52_18_10.18.dump,score2.12.dump,score.v72_2.12.dump,score.v69_3.24.dump,score.v62_9.17.dump,score.v61_5.21.dump,score.v62_13.12.dump
#{"f1": 82.85469689420879, "exact_match": 75.40208136234627}

#score.v72_1.17.dump,score.v17_17_1.17.dump,score.v61_11.15.dump,score.v62_11.9.dump,score.v69_2.13.dump,score.v65_2.22.dump,score.v61_14.11.dump,score.v62_12.11.dump,score.v64_3.21.dump,score.v61_3.20.dump,score.v61_15.14.dump,score.v52_18_10.18.dump,score.v61_12.12.dump,score.v72_2.12.dump,score.v69_3.24.dump,score.v62_9.17.dump,score.v61_5.21.dump,score.v62_13.12.dump
#{"f1": 82.8978929743941, "exact_match": 75.38315988647115}

#score.v62_11.9.dump,score.v69_2.13.dump,score.v65_2.22.dump,score.v61_14.11.dump,score.v62_12.11.dump,score.v64_3.21.dump,score.v61_3.2,score.v61_15.14.dump,score.v52_18_10.18.dump,score.v61_12.12.dump,score.v72_2.12.dump,score.v69_3.24.dump,score.v62_9.17.dump,score.v6.dump,score.v62_13.12.dump
#{"f1": 82.76365899163893, "exact_match": 75.23178807947019}

#score.v64_5.16.dump,score.v69_2.13.dump,score.v62_12.11.dump,score.v66_3.20.dump,score.v61_11.15.dump,score.v64_3.21.dump,score.v62_9.17.dump,score.v62_6.19.dump,score.v72_1.17.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump,score.v69_3.24.dump,score.v61_14.11.dump
#{"f1": 82.72767862972287, "exact_match": 75.28855250709556}

#score.v62_11.9.dump,score.v64_5.16.dump,score.v69_2.13.dump,score.v62_12.11.dump,score.v66_3.20.dump,score.v61_11.15.dump,score.v64_3.21.dump,score.v62_9.17.dump,score.v62_6.19.dump,score.v72_1.17.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump,score.v69_3.24.dump,score.v61_14.11.dump
#{"f1": 82.70530841731276, "exact_match": 75.279091769158}

#score.v61_3.20.dump,score.v62_11.9.dump,score.v64_5.16.dump,score.v69_2.13.dump,score.v62_12.11.dump,score.v66_3.20.dump,score.v61_11.15.dump,score.v64_3.21.dump,score.v62_9.17.dump,score.v62_6.19.dump,score.v72_1.17.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump,score.v69_3.24.dump,score.v61_14.11.dump
#{"f1": 82.64509858040864, "exact_match": 75.20340586565752}

#score.v61_5.21.dump,score.v62_10.20.dump,score.v61_3.20.dump,score.v62_11.9.dump,score.v64_5.16.dump,score.v69_2.13.dump,score.v62_12.11.dump,score.v66_3.20.dump,score.v61_11.15.dump,score.v64_3.21.dump,score.v62_9.17.dump,score.v62_6.19.dump,score.v72_1.17.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump,score.v69_3.24.dump,score.v61_14.11.dump
#{"f1": 82.65513585604727, "exact_match": 75.21286660359507}

#score.v51_18_10.20.dump,score.v61_5.21.dump,score.v62_10.20.dump,score.v61_3.20.dump,score.v62_11.9.dump,score.v64_5.16.dump,score.v69_2.13.dump,score.v62_12.11.dump,score.v66_3.20.dump,score.v61_11.15.dump,score.v64_3.21.dump,score.v62_9.17.dump,score.v62_6.19.dump,score.v72_1.17.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump,score.v69_3.24.dump,score.v61_14.11.dump
#{"f1": 82.66772790528131, "exact_match": 75.20340586565752}

#score.v62_1.16.dump,score.v62_9.17.dump,score.v69_3.24.dump,score.v62_6.19.dump,score.v61_11.15.dump,score.v65_2.22.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v62_11.9.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.54163045240041, "exact_match": 74.98580889309366}

#score.v64_5.16.dump,score.v62_1.16.dump,score.v62_9.17.dump,score.v69_3.24.dump,score.v62_6.19.dump,score.v61_11.15.dump,score.v65_2.22.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v62_11.9.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.5592065737167, "exact_match": 74.91012298959319}

#score.v64_3.21.dump,score.v64_5.16.dump,score.v62_1.16.dump,score.v62_9.17.dump,score.v69_3.24.dump,score.v62_6.19.dump,score.v61_11.15.dump,score.v65_2.22.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v62_11.9.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.55057636063697, "exact_match": 74.89120151371807}

#score.v64_2.19.dump,score.v64_3.21.dump,score.v68_1.18.dump,score.v62_10.20.dump,score.v64_5.16.dump,score.v62_1.16.dump,score.v62_9.17.dump,score.v69_3.24.dump,score.v62_6.19.dump,score.v61_11.15.dump,score.v65_2.22.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v62_11.9.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.62909224405176, "exact_match": 74.93850520340587}

#score.v62_6.19.dump,score.v61_11.15.dump,score.v65_2.22.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v62_11.9.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.53731336795857, "exact_match": 74.87228003784296}

#score.v64_3.21.dump,score.v62_6.19.dump,score.v61_11.15.dump,score.v65_2.22.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v62_11.9.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.57205486002236, "exact_match": 74.83443708609272}

#score.v69_3.24.dump,score.v62_6.19.dump,score.v61_11.15.dump,score.v65_2.22.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v62_11.9.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.49232102687479, "exact_match": 74.8628192999054}

#score.v62_5.21.dump,score.v62_9.17.dump,score.v62_10.20.dump,score.v17_17_1.17.dump,score.v67_1.17.dump,score.v66_3.20.dump,score.v72_1.17.dump,score.v69_3.24.dump,score.v61_11.15.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.46235878600969, "exact_match": 74.79659413434248}

#score.v62_10.20.dump,score.v17_17_1.17.dump,score.v67_1.17.dump,score.v66_3.20.dump,score.v72_1.17.dump,score.v69_3.24.dump,score.v61_11.15.dump,score.v72_2.12.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.43815165670746, "exact_match": 74.73036896877956}

#score.v62_6.19.dump,score.v62_9.17.dump,score.v64_5.16.dump,score.v64_2.19.dump,score.v64_3.21.dump,score.v69_3.24.dump,score.v68_1.18.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v62_10.20.dump,score.v65_2.22.dump,score.v61_11.15.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.41708536012247, "exact_match": 74.73982970671712}

#score.v64_3.21.dump,score.v69_3.24.dump,score.v68_1.18.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v62_10.20.dump,score.v65_2.22.dump,score.v61_11.15.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.25296177140409, "exact_match": 74.69252601702932}

#score.v69_3.24.dump,score.v68_1.18.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v62_10.20.dump,score.v65_2.22.dump,score.v61_11.15.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.24474920519084, "exact_match": 74.61684011352885}

#score.v72_1.17.dump,score.v17_17_1.17.dump,score.v62_10.20.dump,score.v65_2.22.dump,score.v61_11.15.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.12401542613686, "exact_match": 74.47492904446547}

#score.v68_1.18.dump,score.v72_1.17.dump,score.v17_17_1.17.dump,score.v62_10.20.dump,score.v65_2.22.dump,score.v61_11.15.dump,score.v69_1.17.dump,score.v61_12.12.dump
#{"f1": 82.23334331773107, "exact_match": 74.59791863765373}

#score.v17_17_1.17.dump,score.v65_2.22.dump,score.v69_2.13.dump,score.v61_11.15.dump,score.v51_18_10.20.dump,score.v62_9.17.dump,score.v68_1.18.dump,score.v72_1.17.dump
#{"f1": 82.08130416896094, "exact_match": 74.21948912015137}

#score.v62_6.19.dump,score.v65_2.22.dump,score.v69_2.13.dump,score.v61_11.15.dump,score.v51_18_10.20.dump,score.v62_9.17.dump,score.v68_1.18.dump,score.v72_1.17.dump
#{"f1": 82.06432587662736, "exact_match": 74.28571428571429}

#score.v64_3.21.dump,score.v62_6.19.dump,score.v65_2.22.dump,score.v69_2.13.dump,score.v61_11.15.dump,score.v51_18_10.20.dump,score.v62_9.17.dump,score.v68_1.18.dump,score.v72_1.17.dump
#{"f1": 82.16283147014758, "exact_match": 74.26679280983917}

#score.v69_1.17.dump,score.v68_3.20.dump,score.v62_6.19.dump,score.v65_2.22.dump,score.v69_2.13.dump,score.v61_11.15.dump,score.v51_18_10.20.dump,score.v62_9.17.dump,score.v68_1.18.dump,score.v72_1.17.dump
#{"f1": 82.1741631457996, "exact_match": 74.27625354777673}

#score.v52_18_10.18.dump,score.v61_5.21.dump,score.v17_17_1.17.dump,score.v62_1.16.dump,score.v69_1.17.dump,score.v68_3.20.dump,score.v62_6.19.dump,score.v65_2.22.dump,score.v69_2.13.dump,score.v61_11.15.dump,score.v51_18_10.20.dump,score.v62_9.17.dump,score.v68_1.18.dump,score.v72_1.17.dump
#{"f1": 82.13733871646753, "exact_match": 74.29517502365185}

#score.v55_18_9.20.dump,score.v52_18_10.18.dump,score.v61_5.21.dump,score.v17_17_1.17.dump,score.v62_1.16.dump,score.v69_1.17.dump,score.v68_3.20.dump,score.v62_6.19.dump,score.v65_2.22.dump,score.v69_2.13.dump,score.v61_11.15.dump,score.v51_18_10.20.dump,score.v62_9.17.dump,score.v68_1.18.dump,score.v72_1.17.dump
#{"f1": 82.09244321735255, "exact_match": 74.31409649952697}

#"score.v67_1.17.dump"
#score.v53_18_9.18.dump,score.v55_18_9.20.dump,score.v56_18_10.17.dump,score.v51_18_10.20.dump,score.v62_4.20.dump,score.v54_18_10.16.dumdump,score.v63_3.18.dump,score.v61_1.17.dump,score.v65_1.16.dump,score.v67_1.17.dump
#{"f1": 81.7444281304708, "exact_match": 73.50047303689688}

#score.v61_3.20.dump,score.v53_18_10.16.dump,score.v55_18_9.20.dump,score.v56_18_10.17.dump,score.v51_18_10.20.dump,score.v62_4.20.dump,sp,score.v52_18_10.18.dump,score.v63_3.18.dump,score.v61_1.17.dump,score.v65_1.16.dump,score.v67_1.17.dump
#{"f1": 81.78276116105124, "exact_match": 73.50993377483444}

#score.v69_3.24.dump,score.v57_18_10.16.dump,score.v53_18_10.16.dump,score.v55_18_9.20.dump,score.v56_18_10.17.dump,score.v51_18_10.20.du.dump,score.v64_3.21.dump,score.v52_18_10.18.dump,score.v63_3.18.dump,score.v61_1.17.dump,score.v65_1.16.dump,score.v67_1.17.dump
#{"f1": 81.84916768806674, "exact_match": 73.49101229895932}

#score.v59_18_10.15.dump,score.v57_18_10.16.dump,score.v53_18_10.16.dump,score.v55_18_9.20.dump,score.v56_18_10.17.dump,score.v51_18_10.20.dump,score.v62_4.20.dump,score.v54_18_10.16.dump,score.v64_2.19.dump,score.v64_3.21.dump,score.v52_18_10.18.dump,score.v63_3.18.dump,score.v61_1.17.dump,score.v65_1.16.dump,score.v67_1.17.dump
#{"f1": 81.85356167036925, "exact_match": 73.49101229895932}

#"score.v17_17_1.17.dump,score.v65_3.19.dump,score.v59_18_10.15.dump,score.v57_18_10.16.dump,score.v53_18_10.16.dump,score.v55_18_9.20.dump,score.v56_18_10.17.dump,score.v51_18_10.20.dump,score.v62_  4.20.dump,score.v54_18_10.16.dump,score.v64_2.19.dump,score.v64_3.21.dump,score.v52_18_10.18.dump,score.v63_3.18.dump,score.v61_1.17.dump,score.v65_1.16.dump,score.v67_1.17.dump"
#{"f1": 81.78668443372864, "exact_match": 73.57615894039735}

#"score.v69_2.13.dump,score.v68_1.18.dump,score.v66_3.20.dump,score.v64_3.21.dump,score.v65_3.19.dump,score.v51_18_10.20.dump,score.v55_18_9.20.dump,score.v17_17_1.17.dump,score.v69_1.17.dump,score.v67_1.17.dump,score.v65_1.16.dump"
#{"f1": 81.63799711277187, "exact_match": 73.4720908230842}

#"score.v53_18_9.18.dump,score.v69_2.13.dump,score.v68_1.18.dump,score.v66_3.20.dump,score.v64_3.21.dump,score.v65_3.19.dump,score.v51_18_10.20.dump,score.v55_18_9.20.dump,score.v17_17_1.17.dump,score.v69_1.17.dump,score.v67_1.17.dump,score.v65_1.16.dump"
#{"f1": 81.65014416535631, "exact_match": 73.53831598864711}

#"score.v63_3.18.dump,score.v53_18_9.18.dump,score.v69_2.13.dump,score.v68_1.18.dump,score.v66_3.20.dump,score.v64_3.21.dump,score.v65_3.19.dump,score.v51_18_10.20.dump,score.v55_18_9.20.dump,score.v17_17_1.17.dump,score.v69_1.17.dump,score.v67_1.17.dump,score.v65_1.16.dump"
#{"f1": 81.63826412257563, "exact_match": 73.4720908230842}


#"score.v67_1.17.dump,score.v68_1.18.dump,score.v17_17_1.17.dump,score.v61_3.20.dump,score.v55_18_9.20.dump,score.v69_1.17.dump,score.v62_1.16.dump,score.v52_18_10.18.dump,score.v55_18_10.16.dump,score.v53_18_10.16.dump,score.v69_2.13.dump,score.v51_18_10.20.dump,score.v61_1.17.dump"
#{"f1": 81.51255740813619, "exact_match": 73.33017975402082}

#"score.v52_18_11.15.dump,score.v53_18_10.16.dump,score.v62_1.16.dump,score.v52_18_10.18.dump,score.v51_18_10.20.dump,score.v55_18_9.20.dump,score.v17_17_1.17.dump,score.v69_1.17.dump,score.v65_1.16.dump,score.v67_1.17.dump"
#{"f1": 81.47285893585828, "exact_match": 73.38694418164617}

#score.v64_2.19.dump,score.v68_1.18.dump,score.v61_3.20.dump,score.v59_18_10.15.dump,score.v65_2.22dump,score.v69_2.13.dump,score.v55_18_10.16.dump,score.v53_18_9.18.dump,score.v52_18_11.15.dump,sc,score.v62_1.16.dump,score.v52_18_10.18.dump,score.v51_18_10.20.dump,score.v55_18_9.20.dump,score.e.v69_1.17.dump,score.v65_1.16.dump,score.v67_1.17.dump
#{"f1": 81.64636003489376, "exact_match": 73.43424787133397}

#score.v63_2.16.dump,score.v69_2.13.dump,score.v55_18_10.16.dump,score.v53_18_9.18.dump,score.v52_18_11.15.dump,score.v53_18_10.16.dump,score.v62_1.16.dump,score.v52_18_10.18.dump,score.v51_18_10.20.dump,score.v55_18_9.20.dump,score.v17_17_1.17.dump,score.v69_1.17.dump,score.v65_1.16.dump,score.v67_1.17.dump
#{"f1": 81.47452608040888, "exact_match": 73.43424787133397}

#python3 "$(dirname "$0")/"test_restore.py --span_path cook/tmp.nltk.spans --raw_path cook/tmp.nltk.doc --ids_path cook/tmp.nltk.ids --fout $1.label --seqlen 0 --inpaths dev.nltk.pos
#python "$(dirname "$0")/"evaluate-v1.1.py dev-v1.1.json $1 

#python3 "$(dirname "$0")/"example.py --span_path cook/tmp.nltk.spans --raw_path cook/tmp.nltk.doc --ids_path cook/tmp.nltk.ids --fout $1.result --seqlen 15 --inpaths $acceptList


python3 "$(dirname "$0")/"ensemble_restore.py --span_path cook/tmp.nltk.spans --raw_path cook/tmp.nltk.doc --ids_path cook/tmp.nltk.ids --fout $1 --seqlen 0 --inpaths $acceptList
python "$(dirname "$0")/"evaluate-v1.1.py dev-v1.1.json $1 

bestS=0
setList=$acceptList

for it in $(seq 1 20); do    
    for i in $list; do
        fileList=$i",$acceptList"
        echo $fileList
        python3 "$(dirname "$0")/"ensemble_restore.py --span_path cook/tmp.nltk.spans --raw_path cook/tmp.nltk.doc --ids_path cook/tmp.nltk.ids --fout $1 --seqlen 0 --inpaths $fileList
        python "$(dirname "$0")/"evaluate-v1.1.py dev-v1.1.json $1
        score=`python "$(dirname "$0")/"evaluate-v1.1.py dev-v1.1.json $1 | awk -F"," '{split($2, em, ":"); split(em[2], v, "}"); print v[1];}' `
        #score=`python "$(dirname "$0")/"evaluate-v1.1.py dev-v1.1.json $1 | awk -F"," '{split($1, em, ":"); split(em[2], v, ","); print v[1];}' `
        echo $score
        if [ 1 -eq "$(echo "${score} > ${bestS} " | bc)" ]
        then  
            bestS=${score}
            setList=$fileList
            echo "best score"
            echo $bestS
        fi
    done
    acceptList=$setList
    bestS=0
done
echo "best list"
echo $acceptList
#python3 "$(dirname "$0")/"ensemble_restore.py --span_path cook/tmp.nltk.spans --raw_path cook/tmp.nltk.doc --ids_path cook/tmp.nltk.ids --fout $1 --seqlen 0 --inpaths $acceptList 

python3 "$(dirname "$0")/"ensemble_restore.py --span_path cook/tmp.nltk.spans --raw_path cook/tmp.nltk.doc --ids_path cook/tmp.nltk.ids --fout $1 --seqlen 0 --inpaths $acceptList

#score.v17_17_1.17.dump,score.v55_18_9.20.dump,score.v68_1.18.dump,score.v61_3.20.dump,score.v67_1.15.dump,score.v69_1.17.dump,score.v62_1.16.dump,score.v52_18_10.18.dump,score.v53_18_10.16.dump,score.v51_18_10.20.dump,score.v61_1.17.dump,score.v55_18_10.16.dump,score.v68_2.13.dump,score.v69_2.13.dump

#score.v57_18_10.16.dump,score.v57_18_9.18.dump,score.v53_18_9.18.dump


#score.v55_18_9.20.dump,score.v17_17_1.17.dump
#
#,score.v57_18_10.16.dump,score.v59_18_10.15.dump,score.v53_18_10.16.dump,score.v54_18_10.16.dump,score.v55_18_10.16.dump,score.v56_18_10.17.dump,score.v52_18_11.15.dump,score.v51_18_11.12.dump,score.v51_18_10.20.dump,score.v52_18_10.18.dump,,score.v55_18_9.20.dump,score.v59_18_9.17.dump,score.v58_18_9.15.dump,score.v57_18_9.18.dump,score.v56_18_9.11.dump,score.v54_18_9.19.dump
#score.v58_18_10.17.dump,
#



#score.v51_18_5.10.dump,score.v51_18_6.18.dump,score.v52_18_6.20.dump,
python "$(dirname "$0")/"evaluate-v1.1.py dev-v1.1.json $1 




