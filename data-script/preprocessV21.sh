## Single Model ReasoNet Predict Pipeline

python3 "$(dirname "$0")/"preproV21.py -i $1 -o tmp.v2.nltk -p 0  #-is_train True
python3 "$(dirname "$0")/"preproV2.py -i $1 -o tmp.v1.nltk -p 0  #-is_train True

#wc -l $2.ids
#wc -l $2.ques

python3 "$(dirname "$0")/"vocabV21.py tmp.v2.nltk.ques tmp.v2.nltk.para tmp.v2.nltk.vocab 
python3 "$(dirname "$0")/"vocab.py tmp.v1.nltk.ques tmp.v1.nltk.para tmp.v1.nltk.vocab 

wc -l tmp.v2.nltk.vocab 
wc -l tmp.v1.nltk.vocab 

#mono "$(dirname "$0")/"RunScript-2-20-2017/BigLearn.DeepNet.exe "$(dirname "$0")/"model1/ReasoNetReader.nltk.V17_1.config 

#python3 "$(dirname "$0")/"ensemble_restore.py --span_path tmp.nltk.spans --raw_path tmp.nltk.doc --ids_path tmp.nltk.ids --fout $2 --seqlen 15 --inpaths tmp.score.0.dump

#python "$(dirname "$0")/"evaluate-v1.1.py $1 $2

#python "$(dirname "$0")/"evaluate/restore.py --span_path tmp.nltk.spans --raw_path tmp.nltk.doc --ids_path 