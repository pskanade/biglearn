import argparse
import json
import os
import codecs

# data: q, cq, (dq), (pq), y, *x, *cx
# shared: x, cx, (dx), (px), word_counter, char_counter, word2vec
# no metadata
from collections import Counter

from utils import get_word_span, get_word_idx, process_tokens, filter_tokens, get_2d_spans

def main():
    args = get_args()
    prepro(args)


def get_args():
    parser = argparse.ArgumentParser()
    #home = os.path.expanduser("~")
    #source_dir = "./" #os.path.join(home, "data", "squad")
    #target_dir = "./"
    #glove_dir = "./" #os.path.join(home, "data", "glove")
    parser.add_argument('-i', "--input", default="test.json")
    parser.add_argument('-o', "--output", default="test.nltk")
    parser.add_argument('-p', "--pos", default="0")
    #parser.add_argument('-t', "--target_dir", default=target_dir)
    #parser.add_argument('-d', "--debug", action='store_true')
    #parser.add_argument("--train_ratio", default=0.9, type=int)
    #parser.add_argument("--glove_corpus", default="6B")
    #parser.add_argument("--glove_dir", default=glove_dir)
    #parser.add_argument("--glove_vec_size", default=100, type=int)
    #parser.add_argument("--mode", default="full", type=str)
    #parser.add_argument("--single_path", default="", type=str)
    #parser.add_argument("--tokenizer", default="PTB", type=str)
    #parser.add_argument("--url", default="vision-server2.corp.ai2", type=str)
    #parser.add_argument("--port", default=8000, type=int)
    #parser.add_argument("--split", action='store_true')
    # TODO : put more args here
    return parser.parse_args()


#def create_all(args):
#    out_path = os.path.join(args.source_dir, "all-v1.1.json")
#    if os.path.exists(out_path):
#        return
#    train_path = os.path.join(args.source_dir, "train-v1.1.json")
#    train_data = json.load(open(train_path, 'r'))
#    dev_path = os.path.join(args.source_dir, "dev-v1.1.json")
#    dev_data = json.load(open(dev_path, 'r'))
#    train_data['data'].extend(dev_data['data'])
#    print("dumping all data ...")
#    json.dump(train_data, open(out_path, 'w'))

def prepro(args):
    #if not os.path.exists(args.target_dir):
    #    os.makedirs(args.target_dir)

    #if args.mode == 'full':
    #prepro_each(args, 'train', out_name='train')
    prepro_each(args.input, args.output, args.pos == '1')
    #    prepro_each(args, 'dev', out_name='test')
    #elif args.mode == 'all':
    #    create_all(args)
    #    prepro_each(args, 'dev', 0.0, 0.0, out_name='dev')
    #    prepro_each(args, 'dev', 0.0, 0.0, out_name='test')
    #    prepro_each(args, 'all', out_name='train')
    #elif args.mode == 'single':
    #    assert len(args.single_path) > 0
    #    prepro_each(args, "NULL", out_name="single", in_path=args.single_path)
    #else:
    #    prepro_each(args, 'train', 0.0, args.train_ratio, out_name='train')
    #    prepro_each(args, 'train', args.train_ratio, 1.0, out_name='dev')
    #    prepro_each(args, 'dev', out_name='test')

def save(args, data, shared, data_type):
    data_path = os.path.join(args.target_dir, "data_{}.json".format(data_type))
    shared_path = os.path.join(args.target_dir, "shared_{}.json".format(data_type))
    json.dump(data, open(data_path, 'w'))
    json.dump(shared, open(shared_path, 'w'))


def get_word2vec(args, word_counter):
    glove_path = os.path.join(args.glove_dir, "glove.{}.{}d.txt".format(args.glove_corpus, args.glove_vec_size))
    sizes = {'6B': int(4e5), '42B': int(1.9e6), '840B': int(2.2e6), '2B': int(1.2e6)}
    total = sizes[args.glove_corpus]
    word2vec_dict = {}
    with open(glove_path, 'r', encoding='utf-8') as fh:
        for line in tqdm(fh, total=total):
            array = line.lstrip().rstrip().split(" ")
            word = array[0]
            vector = list(map(float, array[1:]))
            if word in word_counter:
                word2vec_dict[word] = vector
            elif word.capitalize() in word_counter:
                word2vec_dict[word.capitalize()] = vector
            elif word.lower() in word_counter:
                word2vec_dict[word.lower()] = vector
            elif word.upper() in word_counter:
                word2vec_dict[word.upper()] = vector

    print("{}/{} of word vocab have corresponding vectors in {}".format(len(word2vec_dict), len(word_counter), glove_path))
    return word2vec_dict


def prepro_each(in_path, out_path, is_train):
    #if args.tokenizer == "PTB":
    import nltk
    sent_tokenize = nltk.sent_tokenize
    def word_tokenize(tokens):
        return [token.replace("''", '"').replace("``", '"') for token in nltk.word_tokenize(tokens)]
    #elif args.tokenizer == 'Stanford':
    #    from my.corenlp_interface import CoreNLPInterface
    #    interface = CoreNLPInterface(args.url, args.port)
    #    sent_tokenize = interface.split_doc
    #    word_tokenize = interface.split_sent
    #else:
    #    raise Exception()

    #if not args.split:
    #    sent_tokenize = lambda para: [para]
    sent_tokenize = lambda para: [para]
    source_path = in_path 
    #or os.path.join(args.source_dir, "{}-v1.1.json".format(data_type))
    source_data = json.load(open(source_path, 'r'))

    ids_path =  "{}.ids".format(out_path)
    ids_writer = codecs.open(ids_path, 'w', 'utf-8')
    
    ques_path = "{}.ques".format(out_path)
    ques_writer = codecs.open(ques_path, 'w', 'utf-8')
    
    para_path = "{}.para".format(out_path)
    para_writer = codecs.open(para_path, 'w', 'utf-8')
    
    spans_path = "{}.spans".format(out_path)
    spans_writer = codecs.open(spans_path, 'w', 'utf-8')
    
    #if(is_train):
    pos_path =  "{}.pos".format(out_path)
    pos_writer = codecs.open(pos_path, 'w', 'utf-8')
    
    
    q, cq, y, rx, rcx, ids, idxs = [], [], [], [], [], [], []
    cy = []
    x, cx = [], []
    answerss = []
    p = []
    word_counter, char_counter, lower_word_counter = Counter(), Counter(), Counter()
    start_ai = 0 	#int(round(len(source_data['data']) * start_ratio))
    stop_ai = len(source_data['data'])
    raw_doc_list = []
    for ai, article in enumerate(source_data['data'][start_ai:stop_ai]):
        xp, cxp = [], []
        pp = []
        x.append(xp)
        cx.append(cxp)
        p.append(pp)
        print('loading paragraph ..')
        
        qi = word_tokenize(article['query'])
        qi = filter_tokens(qi)
        cqi = [list(qij) for qij in qi]
        qids = article['query_id']
        for pi, para in enumerate(article['passages']):
            # wordss
            raw_doc = para['passage_text']
            raw_doc = raw_doc.replace('\n',' ')
            context = raw_doc
            context = context.replace("''", '" ')
            context = context.replace("``", '" ')
            xi = list(map(word_tokenize, sent_tokenize(context)))
            xi = [process_tokens(tokens) for tokens in xi]  # process tokens
            xi = [filter_tokens(tokens) for tokens in xi]   # filter empty token
            # given xi, add chars
            cxi = [[list(xijk) for xijk in xij] for xij in xi]
            xp.append(xi)
            cxp.append(cxi)
            pp.append(context)

            spanss = get_2d_spans(context, xi)
            spanList = []
            for span in spanss[0]:
                spanList.append(str(span[0])+':'+str(span[1]))
                
            assert len(x) - 1 == ai
            assert len(x[ai]) - 1 == pi
            
                
            posStr = []

            posStr.append('(0,0),(0,0)')
            ids_writer.write(str(qids)+'-'+str(pi)+'\n')
            ques_writer.write(u' '.join(qi)+'\n')
            para_writer.write(u' '.join(xi[0])+'\n')
            pos_writer.write('#'.join(posStr)+'\n')
            spans_writer.write(' '.join(spanList)+'\n')
            assert len(context) == len(para['passage_text'])
            raw_doc_list.append(context)

    ids_writer.close()
    ques_writer.close()
    para_writer.close()
    spans_writer.close()
    #if(is_train):
    pos_writer.close()
    doc_path = "{}.doc".format(out_path)
    with codecs.open(doc_path, 'w', 'utf8') as writer:
        writer.write("\n".join(raw_doc_list)+'\n')
    #word2vec_dict = get_word2vec(args, word_counter)
    #lower_word2vec_dict = get_word2vec(args, lower_word_counter)

    # add context here
    #data = {'q': q, 'cq': cq, 'y': y, '*x': rx, '*cx': rcx, 'cy': cy,
    #        'idxs': idxs, 'ids': ids, 'answerss': answerss, '*p': rx}
    #shared = {'x': x, 'cx': cx, 'p': p,
    #          'word_counter': word_counter, 'char_counter': char_counter, 'lower_word_counter': lower_word_counter }
    #, 'word2vec': word2vec_dict, 'lower_word2vec': lower_word2vec_dict}

    #print("saving ...")
    #save(args, data, shared, out_name)

if __name__ == "__main__":
    main()