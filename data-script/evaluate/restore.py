"""
Restore answer spans into the submit format
"""
import argparse
import codecs
import simplejson as json

def ReadFile(path, encoding='utf-8'):
    with codecs.open(path, 'r', encoding=encoding) as reader:
        return reader.readlines()

def WriteFile(path, text, encoding='utf-8'):
    with codecs.open(path, 'w', encoding=encoding) as writer:
        writer.write(text)

def Parse2DSpans(text):
    span_list = []
    for line in text:
        line = line.strip()
        sents = line.split('#')
        sub_span_list = []
        for sent in sents:
            _s = []
            tokens = sent.split()
            for token in tokens:
                items = map(int, token.split(':'))
                _s.append((items[0], items[1]))
            sub_span_list.append(_s)
        span_list.append(sub_span_list)
    return span_list


def ParsePos(text):
    _list = []
    for line in text:
        pos = [int(t) for t in line.split('\t')[:2]]
        _list.append(pos)
    return _list

def SearchPos(span_list, start, stop):
    spans = []
    cnt = 0
    for sent in span_list:
        for token in sent:
            if start == cnt:
                spans.append(token)
            if stop == cnt:
                spans.append(token)
            cnt += 1
    #print len(spans),start,stop;
    assert len(spans) == 2

    return (spans[0][0], spans[1][1])

def Restore(span_path, raw_path, id_path, fin_path, fout_path):
    spans = Parse2DSpans(ReadFile(span_path))
    raw = ReadFile(raw_path)
    ids_list = ReadFile(id_path)
    pos_data = ParsePos(ReadFile(fin_path))
    assert len(spans) == len(raw) and len(raw) == len(ids_list)
    assert len(ids_list) == len(pos_data)
    results = {}
    for idx, pos in enumerate(pos_data):
        ids = ids_list[idx].strip()
        cont = raw[idx]
        span = spans[idx]
        start, stop = SearchPos(span, pos[0], pos[1])
        assert stop >= start
        ans = cont[start:stop]
        results[ids] = ans

    with codecs.open(fout_path, 'w', 'utf-8') as writer:
        json.dump(results, writer)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Reform to submit format')
    parser.add_argument('--span_path', required=True, help='The path of context spans')
    parser.add_argument('--raw_path', required=True, help='The path of raw context')
    parser.add_argument('--ids_path', required=True, help='The path of ids')
    parser.add_argument('--fin', required=True, help='Path of ans span')
    parser.add_argument('--fout', required=True, help='output file path')
    args = parser.parse_args()
    Restore(args.span_path, args.raw_path, args.ids_path, args.fin, args.fout)
