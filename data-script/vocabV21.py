#vocab.py
import codecs
import argparse
import sys

class Vocab:
    """
    Vocabulary diction
    """
    def __init__(self):
        self.word2idx = {}
        self.idx2word = {}

    def add(self, word):
        idx = len(self.word2idx)
        self.word2idx[word] = idx
        self.idx2word[idx] = word

    def get_idx(self, word):
        id = self.word2idx.get(word, -1)
        assert id >= 0
        return id

    def get_word(self, idx):
        return self.idx2word.get(idx, 'UNK')

    def extract_ids(self, words):
        ids = [self.get_idx(w) for w in words]
        return ids

    def save(self, path, with_ids=True):
        with codecs.open(path, 'w', 'utf-8') as writer:
            for idx in range(len(self.idx2word)):
                writer.write('{}\t{}\n'.format(self.get_word(idx), idx))

    def load(self, path, with_ids=True):
        with codecs.open(path, 'r', 'utf-8') as reader:
            if with_ids:
                for line in reader:
                    line = line.strip()
                    items = line.split('\t')
                    self.word2idx[items[0]] = int(items[1])
                    self.idx2word[int(items[1])] = items[0]
            else:
                self.reset()
                for line in reader:
                    line = line.strip()
                    idx = len(self.word2idx)
                    self.word2idx[line] = idx
                    self.idx2word[idx] = line

    def reset(self):
        self.word2idx.clear()
        self.idx2word.clear()

def construct_vocab(word_dict, mfilepath):
    with codecs.open(mfilepath, 'r', 'utf8') as reader:
        for line in reader:
            line = line.strip('\n')
            #line = line.lower()
            toks = line.split(' ')
            for t in toks:
                word_dict[t] = word_dict.get(t, 0) + 1

def map_vocab(vocab, inFile, outFile):
    writer = codecs.open(outFile, 'w', 'utf-8')
    with codecs.open(inFile, 'r', 'utf8') as reader:
        for line in reader:
            line = line.strip('\n')
            #line = line.lower()
            toks = line.split(' ')
            tokIds = vocab.extract_ids(toks)
            writer.write(' '.join(str(x) for x in tokIds)+'\n')
            #'{}\n'.format(self.get_word(idx), idx))
            #' '.join(tokIds)+'\n')
    writer.close()
word_vocab = {}

construct_vocab(word_vocab, sys.argv[1])
construct_vocab(word_vocab, sys.argv[2])

vocab_list = sorted(word_vocab, key=word_vocab.get, reverse=True)
#vocab_list = vocab_filter(vocab_list, embed_path)
vocab = Vocab()
for v in vocab_list:
    vocab.add(v)
vocab.save(sys.argv[3])

map_vocab(vocab, sys.argv[1],  sys.argv[1]+'.idx')
map_vocab(vocab, sys.argv[2], sys.argv[2]+'.idx')
