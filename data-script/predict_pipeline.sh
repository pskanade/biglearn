## Single Model ReasoNet Predict Pipeline

python3 "$(dirname "$0")/"preproV2.py -i $1 -o tmp.nltk -p 0  #-is_train True

wc -l $2.ids
wc -l $2.ques

python3 "$(dirname "$0")/"vocab.py tmp.nltk.ques tmp.nltk.para tmp.nltk.vocab 

wc -l tmp.nltk.vocab 

mono "$(dirname "$0")/"RunScript-2-20-2017/BigLearn.DeepNet.exe "$(dirname "$0")/"model1/ReasoNetReader.nltk.V17_1.config 

python3 "$(dirname "$0")/"ensemble_restore.py --span_path tmp.nltk.spans --raw_path tmp.nltk.doc --ids_path tmp.nltk.ids --fout $2 --seqlen 15 --inpaths tmp.score.0.dump

#python "$(dirname "$0")/"evaluate-v1.1.py $1 $2

#python "$(dirname "$0")/"evaluate/restore.py --span_path tmp.nltk.spans --raw_path tmp.nltk.doc --ids_path 