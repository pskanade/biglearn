﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BigLearn;
namespace BigLearn.RLGym
{

    public class ReplayBatchData : BatchData
    {
        public MatrixData Source;
        public MatrixData Target;

        public MatrixData Action;
        public DenseBatchData Reward;

        public int[] TargetMapping;
        public ReplayBatchData(int maxBatchSize, int inputDim, int outputDim, DeviceType device)
        {
            Source = new MatrixData(inputDim, maxBatchSize, device);
            Target = new MatrixData(inputDim, maxBatchSize, device);

            TargetMapping = new int[maxBatchSize];
            Action = new MatrixData(outputDim, maxBatchSize, device);// int[maxBatchSize]; // CudaPieceInt(maxBatchSize, true, false);
            Reward = new DenseBatchData(maxBatchSize, 1, device);
        }
    }

    public class ReplayMemoryCache
    {
        float[] Observation;
        float[] Reward;
        bool[] IsTerminal;
        float[] Action;
        int[] History;

        int ObDim { get; set; }
        int ActDim { get; set; }
        int ReplayMemorySize { get; set; }

        public int FirstReplayIndex { get; set; }
        public int LastReplayIndex { get; set; }
        int TotalFrameNum { get; set; }

        int Episodic { get; set; }

        public ReplayMemoryCache(int obDim, int actDim, int replayMemorySize)
        {
            ObDim = obDim;
            ActDim = actDim;
            ReplayMemorySize = replayMemorySize;

            Observation = new float[ObDim * ReplayMemorySize];
            Action = new float[ActDim * ReplayMemorySize];
            Reward = new float[ReplayMemorySize];
            IsTerminal = new bool[ReplayMemorySize];
            History = new int[ReplayMemorySize];

            FirstReplayIndex = 0;
            LastReplayIndex = 0;
            TotalFrameNum = 0;

            Episodic = 0;
        }

        public void PushScreen(float[] ob, float[] action, float reward, bool isTerminal)
        {
            int tmpLast = LastReplayIndex % ReplayMemorySize;
            Array.Copy(ob, 0, Observation, tmpLast * ObDim, ObDim);
            Array.Copy(action, 0, Action, tmpLast * ActDim, ActDim);
            Reward[tmpLast] = reward;
            IsTerminal[tmpLast] = isTerminal;
            History[tmpLast] = Episodic;

            LastReplayIndex += 1;
            if (LastReplayIndex - FirstReplayIndex >= ReplayMemorySize * 0.1f)
            {
                FirstReplayIndex += 1;
            }
            Episodic += 1;

            if (isTerminal) { Episodic = 0; }
            TotalFrameNum += 1;
        }

        public int GetPreviousKOb(int replayIdx, int k, float[] tgtbuffer, int offset)
        {
            if (replayIdx >= LastReplayIndex || replayIdx < FirstReplayIndex) return 0;
            Array.Copy(Observation, (replayIdx % ReplayMemorySize) * ObDim, tgtbuffer, offset, ObDim);
            for (int p = 1; p < k; p++)
            {
                int nIdx = (replayIdx - p) % ReplayMemorySize;
                if (replayIdx - p < FirstReplayIndex || IsTerminal[nIdx]) return p;
                Array.Copy(Observation, nIdx * ObDim, tgtbuffer, offset + p * ObDim, ObDim);
            }
            return k;
        }

        public int GetPreviousKAct(int replayIdx, int k, float[] tgtbuffer, int offset)
        {
            if (replayIdx >= LastReplayIndex || replayIdx < FirstReplayIndex) return 0;
            Array.Copy(Action, (replayIdx % ReplayMemorySize) * ActDim, tgtbuffer, offset, ActDim);
            for (int p = 1; p < k; p++)
            {
                int nIdx = (replayIdx - p) % ReplayMemorySize;
                if (replayIdx - p < FirstReplayIndex || IsTerminal[nIdx]) return p;
                Array.Copy(Action, nIdx * ActDim, tgtbuffer, offset + p * ActDim, ActDim);
            }
            return k;
        }

        public bool IsTerminate(int replayIdx) { return IsTerminal[replayIdx % ReplayMemorySize]; }

        public float RewardValue(int replayIdx) { return Reward[replayIdx % ReplayMemorySize]; }
    }

    public class OneGradRunner : ObjectiveRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        public new DenseBatchData Input { get { return (DenseBatchData)base.Input; } set { base.Input = value; } }


        public OneGradRunner(DenseBatchData value, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Input = value;
            Output = value;
        }

        public override void Forward()
        {
            Input.Data.Init(1.0f / (Input.Stat.Dim * Input.BatchSize));
            ObjectiveScore = 1;
        }
    }

    /// <summary>
    /// export to python interface.
    /// </summary>
    public class EnvDDPGAgent
    {
        static CudaPieceFloat ATest;
        static CudaPieceFloat BTest;

        static int ObDim = 24;
        static int ActDim = 4;

        static int History = 5;
        static int XDim { get { return ObDim * (History + 1); } }

        static CudaPieceFloat Observation = null;
        static CudaPieceFloat Action = null;

        static DeviceBehavior Lib;
        static DNNStructure QNet;
        static DNNStructure _QNet;
        static int[] A_HiddenLayers { get { return new int[] { 100, 100, ActDim }; } }
        static A_Func[] A_HiddenFunc { get { return new A_Func[] { A_Func.Rectified, A_Func.Rectified, A_Func.Tanh }; } }
        static bool[] A_HiddenBias { get { return new bool[] { true, true, true }; } }
        static float[] A_HiddenDropout { get { return new float[] { 0, 0, 0 }; } }

        static int MiniBatchSize = 64;
        static int ReplayColdStart = 1024;
        static int ReplayMemorySize = 1000000;
        static float RewardDiscount = 0.99f;

        static float lambda = 0.001f;
        static DNNStructure CriticNet;
        static DNNStructure _CriticNet;

        static CompositeNNStructure DeepNet;

        static int[] C_HiddenLayers { get { return new int[] { 400, 300, 1 }; } }
        static A_Func[] C_HiddenFunc { get { return new A_Func[] { A_Func.Rectified, A_Func.Rectified, A_Func.Linear }; } }
        static bool[] C_HiddenBias { get { return new bool[] { true, true, true }; } }
        static float[] C_HiddenDropout { get { return new float[] { 0, 0, 0 }; } }

        static ReplayMemoryCache ReplayMemory;
        static RandomUtil random;

        static ReplayBatchData ReplayBatch;


        static ComputationGraph ActionCG = null;
        static ComputationGraph CriticCG = null;

        static float[] NoiseAct = null;
        ///// <summary>
        ///// version 1: ob1,ob2,ob3... 
        ///// </summary>
        ///// <param name="observation"></param>
        ///// <param name="replayIdx"></param>
        ///// <param name="statusX"></param>
        //static void Put_V1_ActX(float[] observation, int replayIdx, CudaPieceFloat statusX, int offset)
        //{
        //    Buffer.BlockCopy(observation, 0, statusX.MemPtr, offset, ObDim);
        //    ReplayMemory.GetPreviousKOb(replayIdx, History, statusX.MemPtr, offset + ObDim);
        //}
        //static int V1_ActXDim { get { return ObDim * (History + 1); } }

        ///// <summary>
        ///// version 1: ob1,ob2,ob3...obk,act2,act3,...,actk 
        ///// </summary>
        ///// <param name="observation"></param>
        ///// <param name="replayIdx"></param>
        ///// <param name="statusX"></param>
        //static void Put_V2_ActX(float[] observation, int replayIdx, CudaPieceFloat statusX, int offset)
        //{
        //    Buffer.BlockCopy(observation, 0, statusX.MemPtr, offset, ObDim);
        //    ReplayMemory.GetPreviousKOb(replayIdx, History , statusX.MemPtr, offset + ObDim);
        //    ReplayMemory.GetPreviousKAct(replayIdx, History, statusX.MemPtr, offset + ObDim + History * ObDim);
        //}
        //static int V2_ActXDim { get { return ObDim + ObDim * History + ActDim * History; } }

        public static void Init(int deviceID)
        {
            Lib = new DeviceBehavior(deviceID);

            QNet = new DNNStructure(XDim, A_HiddenLayers, A_HiddenFunc, A_HiddenBias,
                Enumerable.Range(0, A_HiddenLayers.Length).Select(i => N_Type.Fully_Connected).ToArray(),
                Enumerable.Range(0, A_HiddenLayers.Length).Select(i => 1).ToArray(),
                A_HiddenDropout);
            _QNet = new DNNStructure(XDim, A_HiddenLayers, A_HiddenFunc, A_HiddenBias,
                Enumerable.Range(0, A_HiddenLayers.Length).Select(i => N_Type.Fully_Connected).ToArray(),
                Enumerable.Range(0, A_HiddenLayers.Length).Select(i => 1).ToArray(),
                A_HiddenDropout);
            _QNet.Init(QNet);


            StructureLearner Qlearner = new StructureLearner()
            {
                Optimizer = DNNOptimizerType.Adam,
                LearnRate = 0.0001f,
                Adam_Beta1 = 0.9f,
                Adam_Beta2 = 0.999f,
                Adam_Epsilon = 1.0e-8f,
                //ClipDelta = 0.001f,
                //ClipWeight = 10,
            };
            QNet.InitOptimizer(Qlearner, Lib.TrainMode);
            _QNet.InitOptimizer(Qlearner, Lib.TrainMode);

            ActionCG = BuildActionQNetwork(Lib.PredictMode);

            CriticNet = new DNNStructure(XDim + ActDim, C_HiddenLayers, C_HiddenFunc, C_HiddenBias,
                Enumerable.Range(0, C_HiddenLayers.Length).Select(i => N_Type.Fully_Connected).ToArray(),
                Enumerable.Range(0, C_HiddenLayers.Length).Select(i => 1).ToArray(),
                C_HiddenDropout);
            _CriticNet = new DNNStructure(XDim + ActDim, C_HiddenLayers, C_HiddenFunc, C_HiddenBias,
                Enumerable.Range(0, C_HiddenLayers.Length).Select(i => N_Type.Fully_Connected).ToArray(),
                Enumerable.Range(0, C_HiddenLayers.Length).Select(i => 1).ToArray(),
                C_HiddenDropout);
            _CriticNet.Init(CriticNet);
            

            StructureLearner Criticlearner = new StructureLearner()
            {
                Optimizer = DNNOptimizerType.Adam,
                LearnRate = 0.001f,
                Adam_Beta1 = 0.9f,
                Adam_Beta2 = 0.999f,
                Adam_Epsilon = 1.0e-8f,
                //ClipDelta = 0.001f,
                //ClipWeight = 10,
            };
            CriticNet.InitOptimizer(Criticlearner, Lib.TrainMode);
            _CriticNet.InitOptimizer(Criticlearner, Lib.TrainMode);

            DeepNet = new CompositeNNStructure();
            DeepNet.AddLayer(QNet);
            DeepNet.AddLayer(CriticNet);
            CriticCG = BuildCriticNetwork(Lib.TrainMode);
            CriticCG.SetDelegateModel(DeepNet);

            ReplayMemory = new ReplayMemoryCache(ObDim, ActDim, ReplayMemorySize);
            random = new RandomUtil(ParameterSetting.RANDOM_SEED);
            NoiseAct = new float[ActDim];

            ActionCG.Init();
            CriticCG.Init();

            //ATest = new CudaPieceFloat(100, Lib.Device);
            //BTest = new CudaPieceFloat(100, Lib.Device);
            Console.WriteLine("Init Configure file.");
        }

        /// <summary>
        /// then let's add different component into it.
        /// </summary>
        /// <param name="observation"></param>
        /// <param name="inDim"></param>
        /// <param name="outDim"></param>
        /// <returns></returns>
        //public static float[] TestAdd(float[] a, float[] b)
        //{
        //    Array.Copy(a, ATest.MemPtr, 100);
        //    ATest.SyncFromCPU();

        //    Array.Copy(b, BTest.MemPtr, 100);
        //    BTest.SyncFromCPU();

        //    Lib.Computelib.Add_Vector(ATest, BTest, 100, 1, 1);

        //    float[] r = new float[100];
        //    ATest.SyncToCPU();
        //    Array.Copy(ATest.MemPtr, r, 100);
        //    return r;
        //}

        static ComputationGraph BuildCriticNetwork(RunnerBehavior behavior)
        {
            ReplayBatch = new ReplayBatchData(MiniBatchSize, XDim, ActDim, Lib.Device);
            ComputationGraph cg = new ComputationGraph();

            HiddenBatchData inputSource = new HiddenBatchData(ReplayBatch.Source);
            HiddenBatchData inputTarget = new HiddenBatchData(ReplayBatch.Target);
            HiddenBatchData inputAction = new HiddenBatchData(ReplayBatch.Action);

            HiddenBatchData action = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(_QNet, inputTarget, behavior) { IsBackProp = false, IsUpdate = false });
            HiddenBatchData sa = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { inputTarget, action }, behavior) { IsBackProp = false, IsUpdate = false });
            HiddenBatchData score = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(_CriticNet, sa, behavior) { IsBackProp = false, IsUpdate = false });

            DenseBatchData updateReward = (DenseBatchData)cg.AddRunner(new RewardUpdateRunner(ReplayBatch.Reward, ReplayBatch.TargetMapping, score, RewardDiscount, behavior) { IsBackProp = false });

            //update critic network.
            HiddenBatchData source_sa = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { inputSource, inputAction }, behavior) { IsBackProp = false, IsUpdate = false });
            HiddenBatchData source_score = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(CriticNet, source_sa, behavior));
            cg.AddObjective(new MSERunner(source_score.Output, updateReward, CudaPieceFloat.Empty, source_score.Deriv, behavior));

            //update actor network.
            HiddenBatchData sourceAction = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(QNet, inputSource, behavior));
            HiddenBatchData actor_sa = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { inputSource, sourceAction }, behavior));
            HiddenBatchData actor_score = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(CriticNet, actor_sa, behavior) { IsUpdate = false });
            cg.AddObjective(new OneGradRunner(actor_score.Deriv, behavior));

            //target network update.
            return cg;
        }
        

        static ComputationGraph BuildActionQNetwork(RunnerBehavior behavior)
        {
            Observation = new CudaPieceFloat(XDim, Lib.Device);
            ComputationGraph cg = new ComputationGraph();
            VectorData ob = new VectorData(XDim, Observation, CudaPieceFloat.Empty, Lib.Device);
            HiddenBatchData input = new HiddenBatchData(new MatrixData(ob));
            HiddenBatchData actionScore = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(QNet, input, behavior) { IsBackProp = false, IsUpdate = false });
            Action = actionScore.Output.Data;
            cg.SetDelegateModel(QNet);
            return cg;
        }


        public static float[] TakeQAction(float[] observation)
        {
            // receive one observation, and take one action.
            Array.Clear(Observation.MemPtr, 0, Observation.Size);

            // copy the current observation into x.
            Buffer.BlockCopy(observation, 0, Observation.MemPtr, 0, ObDim);
            ReplayMemory.GetPreviousKOb(ReplayMemory.LastReplayIndex - 1, History, Observation.MemPtr, ObDim);
            Observation.SyncFromCPU();

            ActionCG.Step();
            Action.SyncToCPU();

            for(int i=0;i<ActDim;i++)
            {
                NoiseAct[i] = (float)random.NextDiscreteOU(NoiseAct[i], 0.15, 0.2, 0);
                Action.MemPtr[i] += NoiseAct[i];

                if (Action.MemPtr[i] >= 1) Action.MemPtr[i] = 1;
                if (Action.MemPtr[i] <= -1) Action.MemPtr[i] = -1;
            }
            return Action.MemPtr;
        }

        public static void StoreTransition(float[] observation, float[] action, float reward, int isTerminate)
        {
            ReplayMemory.PushScreen(observation, action, reward, isTerminate > 0);
            if(isTerminate > 0)
            {
                Array.Clear(NoiseAct, 0, ActDim);
            }
        }

        public static void Train()
        {
            // start to train the network.
            if (ReplayMemory.LastReplayIndex - ReplayMemory.FirstReplayIndex > ReplayColdStart)
            {
                int[] randomBatchIdx = Enumerable.Range(ReplayMemory.FirstReplayIndex, ReplayMemory.LastReplayIndex - ReplayMemory.FirstReplayIndex - 1).ToArray();
                random.RandomShuffle(randomBatchIdx, MiniBatchSize);

                Array.Clear(ReplayBatch.Source.Output.MemPtr, 0, MiniBatchSize * XDim);
                Array.Clear(ReplayBatch.Target.Output.MemPtr, 0, MiniBatchSize * XDim);
                Array.Clear(ReplayBatch.Action.Output.MemPtr, 0, MiniBatchSize * ActDim);
                Array.Clear(ReplayBatch.Reward.Data.MemPtr, 0, MiniBatchSize);

                int srcIdx = 0;
                int tgtIdx = 0;
                foreach (int replayIdx in randomBatchIdx)
                {
                    ReplayMemory.GetPreviousKOb(replayIdx, History + 1, ReplayBatch.Source.Output.MemPtr, srcIdx * XDim);
                    ReplayMemory.GetPreviousKAct(replayIdx, 1, ReplayBatch.Action.Output.MemPtr, srcIdx * ActDim);
                    ReplayBatch.Reward.Data.MemPtr[srcIdx] = ReplayMemory.RewardValue(replayIdx);

                    if (ReplayMemory.IsTerminate(replayIdx))
                    {
                        ReplayBatch.TargetMapping[srcIdx] = -1;
                    }
                    else
                    {
                        ReplayBatch.TargetMapping[srcIdx] = tgtIdx;
                        ReplayMemory.GetPreviousKOb(replayIdx + 1, History + 1, ReplayBatch.Target.Output.MemPtr, tgtIdx * XDim);
                        tgtIdx += 1;
                    }
                    srcIdx += 1;

                    if (srcIdx >= MiniBatchSize) break;
                }

                ReplayBatch.Source.Row = srcIdx;
                ReplayBatch.Target.Row = tgtIdx;
                ReplayBatch.Action.Row = srcIdx;
                ReplayBatch.Reward.BatchSize = srcIdx;

                ReplayBatch.Source.Output.SyncFromCPU(srcIdx * XDim);
                ReplayBatch.Target.Output.SyncFromCPU(tgtIdx * XDim);
                ReplayBatch.Action.Output.SyncFromCPU(srcIdx * ActDim);
                ReplayBatch.Reward.Data.SyncFromCPU(srcIdx);

                CriticCG.Step();

                _QNet.AggragateModelParameter(Lib.Computelib, QNet, 1 - lambda, lambda);
                _CriticNet.AggragateModelParameter(Lib.Computelib, CriticNet, 1 - lambda, lambda);

            }
        }
    }


}
