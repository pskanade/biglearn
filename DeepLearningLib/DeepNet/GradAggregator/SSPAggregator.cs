﻿//using CommLib;
//using Microsoft.MachineLearning.Internal.Sse;
//using ScopeML;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BigLearn;
namespace BigLearn.DeepNet
{
    // unsafe class SSPAggregator : IGradientAggregator
    // {
    //     bool cancelled = false;
    //     BlockingCollection<BufState> SendBuf;
    //     BlockingCollection<BufState> outFreeBuf;
    //     BlockingCollection<BufState> RecvBuf;
    //     BlockingCollection<BufState> inFreeBuf;
    //     ConcurrentQueue<IntPtr> RecvQueue;

    //     ~SSPAggregator()
    //     {
    //         this.cancelled = true;
    //         this.SendBuf.CompleteAdding();
    //         this.RecvBuf.CompleteAdding();
    //         this.inFreeBuf.CompleteAdding();
    //         this.outFreeBuf.CompleteAdding();
    //     }

    //     public SSPAggregator(int dimension)
    //     {
    //         SendBuf = new BlockingCollection<BufState>(2);
    //         RecvBuf = new BlockingCollection<BufState>(2);
    //         outFreeBuf = new BlockingCollection<BufState>(2);
    //         inFreeBuf = new BlockingCollection<BufState>(2);
    //         RecvQueue = new ConcurrentQueue<IntPtr>();

    //         BufState stat0 = new BufState() { Buffer = new float[dimension]};
    //         BufState stat1 = new BufState() { Buffer = new float[dimension]};
    //         outFreeBuf.Add(stat0);
    //         outFreeBuf.Add(stat1);

    //         BufState stat3 = new BufState() { Buffer = new float[dimension]};
    //         BufState stat4 = new BufState() { Buffer = new float[dimension]};
    //         inFreeBuf.Add(stat3);
    //         inFreeBuf.Add(stat4);

    //         this.Id = AsyncCommunication.RegisterAyncBroadcastSession(OnBroadcastRecv, null);
    //         ThreadPool.QueueUserWorkItem(SendProc);
    //         ThreadPool.QueueUserWorkItem(RecvProc);
    //     }

    //     public UInt32 Id { get; private set; }

    //     public override void Aggregate(params CudaPieceFloat[] grads)
    //     {
    //         foreach (var g in grads)
    //         {
    //             AsyncPush(g);
    //             AsyncPull(g);
    //         }
    //     }

    //     protected void OnBroadcastRecv(void* pBuffer, ulong size, void* args)
    //     {
    //         RecvQueue.Enqueue(new IntPtr(pBuffer));
    //     }

    //     protected void AsyncPull(CudaPieceFloat grads)
    //     {
    //         BufState accBuf = null;
    //         if (RecvBuf.TryTake(out accBuf))
    //         {
    //             fixed (float* pDst = grads.MemPtr)
    //             {
    //                 fixed (float* pSrc = accBuf.Buffer)
    //                 {
    //                     SseUtils.Add((float*)pSrc, pDst, pDst, grads.Size);
    //                 }
    //             }

    //             Array.Clear(accBuf.Buffer, 0, accBuf.Buffer.Length);
    //             inFreeBuf.Add(accBuf);
    //         }
    //     }

    //     protected void AsyncPush(CudaPieceFloat grads)
    //     {
    //         // TODO: Need to further optimize the gap between send and fill
    //         BufState accBuf = null;
    //         if (!SendBuf.TryTake(out accBuf))
    //         {
    //             accBuf = outFreeBuf.Take();
    //         }

    //         fixed (float* pDst = accBuf.Buffer)
    //         {
    //             fixed (float* pSrc = grads.MemPtr)
    //             {
    //                 SseUtils.Add((float*)pSrc, pDst, pDst, grads.Size);
    //             }
    //         }
            
    //         SendBuf.Add(accBuf);
    //     }

    //     protected void RecvProc(object state)
    //     {
    //         int recvCnt = 0;
    //         try
    //         {
    //             while (!cancelled)
    //             {
    //                 IntPtr recvPtr = IntPtr.Zero;
    //                 if (!RecvQueue.TryDequeue(out recvPtr))
    //                 {
    //                     continue;
    //                 }

    //                 Console.WriteLine("[{0}]Received {1}", this.Id, recvCnt++);

    //                 BufState accBuf = null;
    //                 if (!RecvBuf.TryTake(out accBuf))
    //                 {
    //                     accBuf = inFreeBuf.Take();
    //                 }

    //                 if (accBuf != null)
    //                 {
    //                     fixed (float* pDst = accBuf.Buffer)
    //                     {
    //                         SseUtils.Add((float*)recvPtr.ToPointer(), pDst, pDst, accBuf.Buffer.Length);
    //                     }

    //                     RecvBuf.Add(accBuf);
    //                 }

    //                 AsyncCommunication.FreeSessionBuffer(recvPtr.ToPointer());
    //             }
    //         }
    //         catch (Exception ex)
    //         {
    //             Console.WriteLine("Exception happenned in recv proc: {0}", ex);
    //         }
    //     }

    //     protected void SendProc(object state)
    //     {
    //         try
    //         {
    //             while (!cancelled)
    //             {
    //                 BufState outBuf = SendBuf.Take();
    //                 if (outBuf == null)
    //                 {
    //                     continue;
    //                 }

    //                 fixed (float* pSend = outBuf.Buffer)
    //                 {
    //                     AsyncCommunication.BroadcastSession(this.Id, pSend, (uint)outBuf.Buffer.Length*sizeof(float));
    //                 }

    //                 Array.Clear(outBuf.Buffer, 0, outBuf.Buffer.Length);
    //                 outFreeBuf.Add(outBuf);
    //             }
    //         }
    //         catch (Exception ex)
    //         {
    //             Console.WriteLine("Exception happenned in send proc: {0}", ex);
    //         }
    //     }
    // }

    // class BufState
    // {
    //     public float[] Buffer;
    //     public uint RefCount;
    // }
}
