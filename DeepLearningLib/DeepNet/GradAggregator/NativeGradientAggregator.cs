﻿//using CommLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;
namespace BigLearn.DeepNet
{
    unsafe class MultiGPUAggregator: IGradientAggregator
    {
        public override void Aggregate(params CudaPieceFloat[] inputs)
        {
            foreach (var input in inputs)
            {
                Cudalib.MPI_Aggregation(input.CudaPtr, (uint)input.Size);
            }
        }
    }

    // unsafe class NativeGradientAggregator : IGradientAggregator
    // {
    //     static int[] delta = null;

    //     public override void Aggregate(params CudaPieceFloat[] inputs)
    //     {
    //         var aggCnt = PerfCounter.Manager.Instance["GradAgg"].Begin();
    //         foreach (var input in inputs)
    //         {
    //             input.SyncToCPU();

    //             AggregateNative(input);
    //             input.SyncFromCPU();
    //         }
    //         PerfCounter.Manager.Instance["GradAgg"].TakeCount(aggCnt);
    //     }

    //     public static void AggregateNative(CudaPieceFloat input)
    //     {
    //         /*
    //         if (!File.Exists("AllReduce.dump.bin"))
    //         {
    //             FileStream fs = new FileStream("AllReduce.dump.bin", FileMode.Create, FileAccess.Write);
    //             BinaryWriter bw = new BinaryWriter(fs);
              
    //             byte[] buffer = new byte[input.Size*sizeof(float)];
    //             Buffer.BlockCopy(input.MemPtr,0, buffer, 0, buffer.Length);
    //             bw.Write(buffer.Length);
    //             bw.Write(buffer, 0, buffer.Length);
    //             bw.Flush();
    //             fs.Close();
    //         }
    //         */
    //             var cnt = PerfCounter.Manager.Instance["AllReduce"].Begin();
    //             DistComputationUtils.Sum(input.MemPtr, input.Size, 120*1000);
    //             PerfCounter.Manager.Instance["AllReduce"].TakeCount(cnt);
    //     }

    //     public static void Aggregate1Bit(CudaPieceFloat input)
    //     {
    //         int alignedSize = (input.Size + 7) / 8;
    //         if (delta == null || delta.Length < alignedSize)
    //         {
    //             delta = new int[alignedSize];
    //         }

    //         float[] data = input.MemPtr;
    //         float avg = 0;
    //         int n = 0;
    //         PerfCounter.Manager.Instance["GradAvg"].CountOperation(() =>
    //         {
    //             for (int i = 0; i < data.Length; i++)
    //             {
    //                 if (data[i] != 0)
    //                 {
    //                     avg += data[i] > 0 ? data[i] : -data[i];
    //                     n++;
    //                     if (n > 10)
    //                     {
    //                         break;
    //                     }
    //                 }
    //             }

    //             for (int i = 0; i < delta.Length; i++)
    //             {
    //                 delta[i] = 0;
    //             }
    //         });

    //         avg /= n;

    //         fixed (void* ptr = &delta[0])
    //         {
    //             byte* bitS = (byte*)ptr;
    //             for (int i = 0; i < data.Length; i++)
    //             {
    //                 byte bit = 0;
    //                 if (data[i] != 0)
    //                 {
    //                     if (data[i] >= avg)
    //                     {
    //                         bit = 4;
    //                         data[i] -= avg;
    //                     }
    //                     else if (data[i] <= -avg)
    //                     {
    //                         bit = 1;
    //                         data[i] += avg;
    //                     }

    //                     bitS[i >> 1] |= ((i & 1) == 1 ? bit : (byte)(bit << 4));
    //                 }
    //             }

    //             var cnt = PerfCounter.Manager.Instance["AllReduce"].Begin();
    //             AsyncCommunication.AllReduce(ptr, (ulong)alignedSize, sizeof(int), AllReduceDataType.TpInt32, AllReduceOpType.OpSum);
    //             PerfCounter.Manager.Instance["AllReduce"].TakeCount(cnt);

    //             for (int i = 0; i < data.Length; i++)
    //             {
    //                 int bitValue = bitS[i >> 1];
    //                 if (bitValue != 0)
    //                 {
    //                     int bit = (((i & 1) == 1 ? bitValue : (byte)(bitValue >> 4)) & 0xf);
    //                     int t = (bit >> 2) - (bit & 0x3);
    //                     if (t != 0)
    //                     {
    //                         data[i] += t * avg;
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }
}
