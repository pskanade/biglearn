﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
    public class AppReinforceWalkLinkPredictionV4_10_NEWBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.
            public static string InputDir { get { return Argument["INPUT-DIR"].Value; } }
            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }
            public static int MCTSNum { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

            public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            public static int RNN_Dim { get { return int.Parse(Argument["RNN-DIM"].Value); } }
            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] S_NET { get { return Argument["S-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] S_AF { get { return Argument["S-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }
            
            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            public static int MIN_HOP { get { return int.Parse(Argument["MIN-HOP"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }
            //public static float REWARD_MCTS_DISCOUNT { get { return float.Parse(Argument["REWARD-MCTS-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static float UCB1_C { get { return float.Parse(Argument["UCB1-C"].Value); } }
            

            public static float PUCB_C { get { return float.Parse(Argument["PUCB-C"].Value); } }
            public static float PUCB_B { get { return float.Parse(Argument["PUCB-B"].Value); } }
            public static float PUCB_M { get { return float.Parse(Argument["PUCB-M"].Value); } }
            public static int MAX_ACTION_NUM { get { return int.Parse(Argument["MAX-ACTION-NUM"].Value); } }

            public static int TEST_SAMPLES { get { return int.Parse(Argument["TEST-SAMPLES"].Value); } }

            public static int DEV_AS_TRAIN { get { return int.Parse(Argument["DEV-TRAIN"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }
            public static int ActorMiniBatch { get { return int.Parse(Argument["ACTOR-MINI-BATCH"].Value); } }
            public static int ActorPolicy { get { return int.Parse(Argument["ACTOR-POLICY"].Value); } } 
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }

            public static int LearnerMiniBatch { get { return int.Parse(Argument["LEARNER-MINI-BATCH"].Value); } }
            public static int LearnerEpoch { get { return int.Parse(Argument["LEARNER-EPOCH"].Value); } }
            public static int ModelSyncUp { get { return int.Parse(Argument["MODEL-SYNCUP"].Value); } }
            public static int ReplayBufferSize { get { return int.Parse(Argument["REPLAY-BUFF-SIZE"].Value); } } 
            public static int TReplaySize { get { return int.Parse(Argument["T-REPLAY-SIZE"].Value); } } 
            public static float ReplayDecay { get { return float.Parse(Argument["REPLAY-DECAY"].Value); } }
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                #region Input Data Argument.
                Argument.Add("INPUT-DIR", new ParameterArgument(string.Empty, "dir input."));
                #endregion.

                Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
                Argument.Add("RNN-DIM", new ParameterArgument("100", "RNN hidden state dim."));

                Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));
                //Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                //Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));
                Argument.Add("S-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("S-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));


                Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));
                Argument.Add("MIN-HOP", new ParameterArgument("2", "minimum number of hops"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

                Argument.Add("SCORE-TYPE", new ParameterArgument("0", "0:probability; 1:pesudo reward;"));
                Argument.Add("ROLL-NUM", new ParameterArgument("1", "roll number"));

                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));
                Argument.Add("MCTS-NUM", new ParameterArgument("0", "Monto Carlo Tree Search Number"));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.99", "Reward discount"));
                Argument.Add("REWARD-MCTS-DISCOUNT", new ParameterArgument("0.8", "mcts reward discount."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument("model/", "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("./", "Output Score File."));

                Argument.Add("TEST-MCTS-NUM", new ParameterArgument("64", "Pre Monto Carlo Tree Search Number"));
                
                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));
                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("SCORE-LAMBDA", new ParameterArgument("1.0", "Score lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0", "Prob lambda in scoring function."));

                Argument.Add("MCTS-EXP", new ParameterArgument("8,16,24,48,64", "Exploration strategy."));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1", "mcts reward discount."));
                Argument.Add("MAX-ACTION-NUM", new ParameterArgument("0", "Default max action number."));

                Argument.Add("EVALUATION-TOOL", new ParameterArgument(string.Empty, "Evaluation path tool."));

                Argument.Add("AGG-SCORE", new ParameterArgument("0", "0:enumate; 1:max pool; 2:sum pool;"));
                Argument.Add("MAP-EVAL", new ParameterArgument("1", "0:no map eval; 1:mapeval;"));
                Argument.Add("BETA", new ParameterArgument("1", "moving average baseline estimation."));
                Argument.Add("ALPHA-1", new ParameterArgument("0","max entropy for termination gate."));
                Argument.Add("ALPHA-2", new ParameterArgument("0", "max entropy for action gate."));

                Argument.Add("TRAIN-STR", new ParameterArgument("0","0:standard train strategy; 1:mcts train."));
                Argument.Add("TRAIN-L", new ParameterArgument("1", "train random lambda"));
                Argument.Add("TRAIN-C", new ParameterArgument("1", "train self lambda."));
                Argument.Add("TEST-L", new ParameterArgument("1", "test random lambda."));
                Argument.Add("TEST-C", new ParameterArgument("1", "test self lambda."));

                Argument.Add("TRAIN-SAMPLES", new ParameterArgument("0", "training samples per epoch."));
                Argument.Add("TEST-SAMPLES", new ParameterArgument("0", "testing samples per epoch."));

                Argument.Add("STEP-FILTER", new ParameterArgument("0", "step filter"));
                Argument.Add("DEV-TRAIN", new ParameterArgument("0", "use dev set as training set."));


                Argument.Add("ACTOR-MINI-BATCH", new ParameterArgument("128", "actor mini batch size"));
                Argument.Add("ACTOR-POLICY", new ParameterArgument("4", "0:UnifiedSampling; 1:ThompasSampling; 2:UCB; 3:AlphaGoZero1; 4:AlphaGoZero2; 5:PUCB"));

                Argument.Add("LEARNER-MINI-BATCH", new ParameterArgument("32", "learner mini batch size"));
                Argument.Add("LEARNER-EPOCH", new ParameterArgument("5", "learner training epoch size"));
                Argument.Add("MODEL-SYNCUP", new ParameterArgument("3000", "model syncup iteration."));   
                Argument.Add("REPLAY-BUFF-SIZE", new ParameterArgument("3000000", "buffer size for the replay memory."));   
                Argument.Add("T-REPLAY-SIZE", new ParameterArgument("5000", "threshold of replay buffer size."));   
                Argument.Add("REPLAY-DECAY", new ParameterArgument("0.99995", "Replay Decay Value."));   
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_REINFORCE_WALK_LINK_PREDICTION_V4_10_NEW; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        /// <summary>
        /// episodic memory structure. it is a global structure.
        /// </summary>
        public class EpisodicMemory 
        {
            //decaying memory for visiting and success rate.
            Dictionary<string, Tuple<float[], float[]>> Mem = new Dictionary<string, Tuple<float[], float[]>>();
            Dictionary<string, int> MemTimer = new Dictionary<string, int>();

            static int ReplayBufferSize = BuilderParameters.ReplayBufferSize; //  1000000;
            static float ReplayDecay = BuilderParameters.ReplayDecay; // 0.9999f;
            static int MiniReplaySize = BuilderParameters.TReplaySize;

            int ReplayCursor = 0;
            int ReplayLength = 0;
            int Timer { get; set; }
            
            float[] ReplayRewards = null;
            int[] ReplayIdx = null;
            int[] ReplayTgt = null;
            int[] ReplaySrc = null;
            int[] ReplayRel = null;
            Tuple<int, int, int, int>[][] ReplayPath = null;

            public bool IsReplayBufferReady { get { if(ReplayLength < MiniReplaySize) return false; return true; } }
            
            public Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 998);

            public EpisodicMemory(bool isReplay)
            {
                if(isReplay)
                {
                    ReplayRewards = new float[ReplayBufferSize];
                    ReplayIdx = new int[ReplayBufferSize];
                    ReplayTgt = new int[ReplayBufferSize];
                    ReplaySrc = new int[ReplayBufferSize];
                    ReplayRel = new int[ReplayBufferSize];
                    ReplayPath = new Tuple<int, int, int, int>[ReplayBufferSize][];
                }
                Timer = 0;
                ReplayCursor = 0;
                ReplayLength = 0;
            }

            public void Clear()
            {
                Mem.Clear();
                MemTimer.Clear();
            }

            /// <summary>
            /// memory index.
            /// </summary>
            /// <param name="key"></param>
            /// <param name="actionCount"></param>
            /// <param name="topK"></param>
            /// <returns></returns>
            public Tuple<float[], float[], float> Search(string key)
            {
                if (Mem.ContainsKey(key))
                {
                    float decay = (float)Math.Pow(ReplayDecay, Timer - MemTimer[key]);
                    return new Tuple<float[], float[], float>(Mem[key].Item1, Mem[key].Item2, decay);
                }
                else
                {
                    return null;
                }
            }

            public void AddReplayBuff(int dIdx, int srcId, int relId, int tgtId, Tuple<int, int, int, int>[] path, float reward)
            {
                ReplaySrc[ReplayCursor%ReplayBufferSize] = srcId;
                ReplayRel[ReplayCursor%ReplayBufferSize] = relId;
                ReplayPath[ReplayCursor%ReplayBufferSize] = path;

                ReplayIdx[ReplayCursor%ReplayBufferSize] = dIdx;
                ReplayRewards[ReplayCursor%ReplayBufferSize] = reward;
                ReplayTgt[ReplayCursor%ReplayBufferSize] = tgtId;
                ReplayCursor += 1;
                ReplayLength += 1;
            }

            public Tuple<int, int, int, int, Tuple<int, int, int, int>[], float> SampleReplayData()
            {
                int idx = random.Next(ReplayLength < ReplayBufferSize ? ReplayLength : ReplayBufferSize);
                return new Tuple<int, int, int, int, Tuple<int, int, int, int>[], float>(
                        ReplayIdx[idx], 
                        ReplaySrc[idx], 
                        ReplayRel[idx], 
                        ReplayTgt[idx],
                        ReplayPath[idx],
                        ReplayRewards[idx]
                    );
            }

            public void UpdateMem(int rawIdx, int srcId, int relId, Tuple<int, int, int, int>[] path, float reward)
            {
                string mkey = string.Format("Idx:{0}--N:{1}?R:{2}", rawIdx, srcId, relId);

                for(int i = 0; i < path.Length; i++)
                {
                    //if(mkey.Equals("N:1098?R:53"))
                    //{
                    //    Logger.WriteLog("action {0} and action dim {1}", path[i].Item1, path[i].Item2);
                    //}
                    Update(mkey, path[i].Item1, path[i].Item2, reward);
                    mkey = string.Format("{0}-R:{1}->N:{2}", mkey, path[i].Item4, path[i].Item3);
                }
            }


            public unsafe void Update(string key, int actid, int totalAct, float reward, float v = 1.0f)
            {
                float new_r = 0;
                float new_v = 0;
                // update memory.
                if (Mem.ContainsKey(key))
                {
                    int memTime = MemTimer[key];
                    float decay = (float)Math.Pow(ReplayDecay, Timer - memTime);

                    fixed(float * pr = &Mem[key].Item1[0])
                    fixed(float * pv = &Mem[key].Item2[0])
                    {
                        SSElib.SSE_Scale(decay, pr, totalAct);
                        SSElib.SSE_Scale(decay, pv, totalAct);
                    }
                    float or = Mem[key].Item1[actid];
                    float ov = Mem[key].Item2[actid];

                    new_r = or + reward; // c / (c + v) * u + v / (c + v) * reward;
                    new_v = ov + v;
                }
                else
                {
                    Mem.Add(key, new Tuple<float[], float[]>(new float[totalAct], new float[totalAct]));
                    MemTimer.Add(key, Timer);
                    new_r = reward;
                    new_v = v;

                    if(Mem.Count % 1000000 == 0)
                    {
                        Logger.WriteLog("MCTS Memory Size over {0}", Mem.Count);
                    }
                }
                Mem[key].Item1[actid] = new_r;
                Mem[key].Item2[actid] = new_v;
                MemTimer[key] = Timer;
            }

            public void UpdateTiming()
            {
                Timer += 1;
            }
        }

        public class BanditAlg 
        {
            /// <summary>
            /// Exp 0.
            /// </summary>
            /// <param name="prior"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int UniformRandomStrategy0(int dim, Random random)
            {
                return random.Next(dim);
            }

            /// <summary>
            /// Exp 1.
            /// </summary>
            /// <param name="actdim"></param>
            /// <param name="rand_term"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int CascadeRandomStrategy1(int actdim, float rand_term, Random random)
            {
                if (random.NextDouble() > rand_term)
                {
                    return actdim;
                }
                return random.Next(actdim);
            }

            /// <summary>
            /// Exp 2.
            /// </summary>
            /// <param name="prior"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int ThompasSampling(float[] prior, Random random)
            {
                int idx = Util.Sample(prior.ToArray(), random);
                return idx;
            }


            /// <summary>
            /// Exp 3. Standard UCB Bandit.
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int UCB1Bandit(Tuple<float[], float[], float> arms, int dim, float c, Random random)
            {
                if (arms == null)
                {
                    return UniformRandomStrategy0(dim, random);
                }

                float log_total = (float)Math.Log(arms.Item2.Sum() * arms.Item3 + 0.1f);
                float[] v = new float[dim];
                //foreach (Tuple<float, float> arm in arms)
                for(int d=0; d<dim; d++)
                {
                    v[d] = arms.Item1[d] * arms.Item3 / (arms.Item2[d] * arms.Item3 + 0.1f) + 
                           c * (float)Math.Sqrt(log_total / (arms.Item2[d] * arms.Item3 + 0.1f)) + 
                           (float)random.NextDouble() * 0.0001f;
                }
                int idx = Util.MaximumValue(v);
                return idx;
            }

            public static int PUCB(Tuple<float[], float[], float> arms, float[] prior, int dim, float mb, float c, float m, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < dim; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float sum = prior.Sum() + mb * dim;
                    float t = (float)arms.Item2.Sum() * arms.Item3;

                    for (int i = 0; i < dim; i++)
                    {
                        float si = arms.Item1[i] * arms.Item3;
                        float xi = (arms.Item2[i] == 0 ? 1 : arms.Item1[i] * 1.0f / (arms.Item2[i]));
                        float ci = si == 0 ? 0 : c * (float)Math.Sqrt(Math.Log(t) / si);
                        float mi = (t == 0 ? 1 : (float)Math.Sqrt(Math.Log(t) / t)) * m * sum / (prior[i] + mb);
                        float s = xi + ci - mi;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }


            /// <summary>
            /// Exp 4. PUCT.
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int AlphaGoZeroBandit(Tuple<float[], float[], float> arms, float[] prior, int dim, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < dim; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Item2.Sum() * arms.Item3 + 1.0f);

                    for (int i = 0; i < dim; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], BuilderParameters.PUCT_D) * (float)Math.Sqrt(log_total) / (arms.Item2[i] * arms.Item3 + 1.0f) +
                                                        (arms.Item2[i] == 0 ? 0 : arms.Item1[i] * 1.0f / (arms.Item2[i]));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }


            public static int AlphaGoZeroBanditV2(Tuple<float[], float[], float> arms, float[] prior, int dim, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;
                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < dim; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float total = (float)arms.Item2.Sum() * arms.Item3;

                    for (int i = 0; i < dim; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], BuilderParameters.PUCT_D) * (float)Math.Sqrt(total) / (arms.Item2[i] * arms.Item3 + 1.0f) +
                                                       (arms.Item2[i] == 0 ? 0 : arms.Item1[i] * 1.0f / (arms.Item2[i]));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }

            public static int AlphaGoZeroBanditV3(Tuple<float[], float[], float> arms, float[] prior, int dim, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < dim; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Item2.Sum() * arms.Item3 + 1.0f);

                    for (int i = 0; i < dim; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], BuilderParameters.PUCT_D) * (float)Math.Sqrt(log_total / (arms.Item2[i] * arms.Item3 + 1.0f)) +
                                                        (arms.Item2[i] == 0 ? 0 : arms.Item1[i] * 1.0f / (arms.Item2[i]));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }

            /// <summary>
            /// s = 
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int AlphaGoZeroBandit_Stochastic(Tuple<float[], float[], float> arms, float[] prior_q, int dim, float d, float lambda, Random random)
            {
                float[] prob = null; 
                {
                    float[] k = new float[dim]; 
                    for (int idx = 0; idx < dim; idx++)
                    {
                        k[idx] = (prior_q[idx] * d + (arms == null ? 0 : arms.Item1[idx] * arms.Item3) + lambda * random.Next()) / (d + (arms == null ? 0 : arms.Item2[idx] * arms.Item3) + lambda);
                    }
                    prob = Util.Softmax(k, 1);
                }
                return Util.Sample(prob, random);
            }


            /// <summary>
            /// mb = 0.00001;
            /// c = exploration.
            /// m = 
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="mb"></param>
            /// <param name="c"></param>
            /// <param name="m"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int PUCB2(Tuple<float[], float[], float> arms, float[] prior, int dim, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < dim; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Item2.Sum() * arms.Item3);
                    float[] v = new float[dim];
                    //foreach (Tuple<float, float> arm in arms)
                    for(int d = 0 ;d < dim; d++)
                    {
                        v[d] = arms.Item1[d] * arms.Item3 / (arms.Item2[d] * arms.Item3 + 1.0f) + 
                                c  * (float)Math.Sqrt(log_total / (arms.Item2[d] * arms.Item3)) + (float)(random.NextDouble() * 0.0001f);
                    }
                    int idx = Util.MaximumValue(v.ToArray());
                }
                return maxI;
            }
        }

        public class GraphQueryData : BatchData
        {
            public List<int> RawSource = new List<int>();
            public List<int> RawRel = new List<int>();
            public List<int> RawTarget = new List<int>();
            public List<int> RawIndex = new List<int>();
            public List<int> RawStart = new List<int>();
            // the edge already exists in KB.
            public List<List<int>> BlackTargets = new List<List<int>>();
            public List<Tuple<int, int, int, int>[]> GuidPath = null;
            public List<float> GuidReward = null;

            public List<StatusData> StatusPath = new List<StatusData>();

            public int BatchSize { get { return RawSource.Count; } }
            public int MaxBatchSize { get; set; }

            /// <summary>
            /// max group number, and group size;
            /// </summary>
            /// <param name="maxGroupNum"></param>
            /// <param name="groupSize"></param>
            /// <param name="device"></param>
            public GraphQueryData(int maxBatchSize, DeviceType device)
            {
                MaxBatchSize = maxBatchSize;
            }

            public List<int> GetBatchIdxs(int b, int step)
            {
                List<int> r = new List<int>();
                for (int i = 0; i < StatusPath[step].BatchSize; i++)
                {
                    if (StatusPath[step].GetOriginalStatsIndex(i) == b)
                        r.Add(i);
                }
                return r;
            }
        }

        public class StatusData : BatchData
        {
            /// <summary>
            /// Raw Query.
            /// </summary>
            public GraphQueryData GraphQuery { get; set; }

            /// <summary>
            /// Node ID.
            /// </summary>
            public List<int> NodeID { get; set; }

            /// <summary>
            /// LogProbability to this node.
            /// </summary>
            List<float> LogProb { get; set; }

            /// <summary>
            /// MatchCandidate and MatchCandidateProb.
            /// </summary>
            public Tuple<List<int>, List<int>> MatchCandidate = null;
            public SeqVectorData MatchCandidateQ = null;
            public SeqVectorData MatchCandidateProb = null;

            public List<int> PreSelActIndex = null;
            public List<int> PreStatusIndex = null;

            public int GetPreStatusIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else return PreStatusIndex[batchIdx];
            }

            public int GetPreActionIndex(int batchIdx)
            {
                if(Step == 0) { throw new NotImplementedException("Current Status is zero."); }
                else return PreSelActIndex[batchIdx];
            }

            public int GetActionDim(int b)
            {
                return GetActionEndIndex(b) - GetActionStartIndex(b);
            }

            public int GetActionStartIndex(int b)
            {
                return b == 0 ? 0 : MatchCandidateProb.SegmentIdx.MemPtr[b - 1];
            }
            public int GetActionEndIndex(int b)
            {
                return MatchCandidateProb.SegmentIdx.MemPtr[b];
            }

            public Tuple<int, int, int, int> GetPreActionInfo(int batchIdx)
            {
                int pb = GetPreStatusIndex(batchIdx);
                int gactIdx = GetPreActionIndex(batchIdx);

                int actIdx = gactIdx - GraphQuery.StatusPath[Step - 1].GetActionStartIndex(pb);

                int actDim = GraphQuery.StatusPath[Step - 1].GetActionDim(pb);
                
                int nodId = GraphQuery.StatusPath[Step - 1].MatchCandidate.Item1[gactIdx];
                int relId = GraphQuery.StatusPath[Step - 1].MatchCandidate.Item2[gactIdx];

                return new Tuple<int, int, int, int>(actIdx, actDim, nodId, relId);
            }

            public float GetPreQValue(int batchIdx)
            {
                int aidx = GetPreActionIndex(batchIdx);
                StatusData pst = GraphQuery.StatusPath[Step - 1];
                return pst.MatchCandidateQ.Output.MemPtr[aidx];
            }

            public int GetOriginalStatsIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else
                {
                    int b = GetPreStatusIndex(batchIdx);
                    return GraphQuery.StatusPath[Step - 1].GetOriginalStatsIndex(b);
                }
            }

            

            public string GetStatusKey(int b)
            {
                Tuple<int, int, int, int>[] path = GetPath(b);
                
                int origialB = GetOriginalStatsIndex(b); 
                string mkey = string.Format("Idx:{0}--N:{1}?R:{2}", GraphQuery.RawIndex[origialB], 
                    GraphQuery.RawSource[origialB], GraphQuery.RawRel[origialB]);
                foreach(Tuple<int, int, int, int> n in path) 
                {
                    mkey = string.Format("{0}-R:{1}->N:{2}", mkey, n.Item4, n.Item3);
                }
                return mkey;
            }

            public Tuple<int, int, int, int>[] GetPath(int b)
            {
                Tuple<int, int, int, int>[] mpath = new Tuple<int, int, int, int>[Step];
                StatusData pointer = this;
                int mb = b;
                for(int i = 1; i <= Step; i++)  
                {
                    Tuple<int,int,int,int> n = pointer.GetPreActionInfo(mb);
                    mpath[Step-i] = n;

                    mb = pointer.GetPreStatusIndex(mb);
                    pointer = GraphQuery.StatusPath[Step - i];
                }
                return mpath;
            }

            public int MaxBatchSize { get { return StateEmbed.MAX_BATCHSIZE; } }
            public int BatchSize { get { return StateEmbed.BatchSize; } }

            /// <summary>
            /// logprobability of the node.
            /// </summary>
            /// <param name="batchIdx"></param>
            /// <returns></returns>
            public float GetLogProb(int batchIdx)
            {
                if (Step == 0) { return 0; }
                else { return LogProb[batchIdx]; }
            }

            /// <summary>
            /// Embedding of the State.
            /// </summary>
            public HiddenBatchData StateEmbed { get; set; }

            public HiddenBatchData StateV { get; set; }

            public int Step;

            public StatusData(GraphQueryData interData, List<int> nodeIndex, HiddenBatchData stateEmbed, DeviceType device) :
                this(interData, nodeIndex, null, null, null, stateEmbed, device)
            { }

            public StatusData(GraphQueryData interData,
                              List<int> nodeIndex, List<float> logProb, List<int> selectedAction, List<int> preStatusIndex,
                              HiddenBatchData stateEmbed, DeviceType device)
            {
                GraphQuery = interData;
                GraphQuery.StatusPath.Add(this);

                NodeID = nodeIndex;
                StateEmbed = stateEmbed;

                LogProb = logProb;
                PreSelActIndex = selectedAction;
                PreStatusIndex = preStatusIndex; 

                Step = GraphQuery.StatusPath.Count - 1;
            }
        }

        /// <summary>
        /// Sample Graph. 
        /// </summary>
        class SampleRunner : StructRunner
        {
            DataEnviroument Data { get; set; }
            public new GraphQueryData Output { get; set; }
            int MaxBatchSize { get; set; }
            int GroupSize { get; set; }
            //int Roll { get; set; }
            DataRandomShuffling Shuffle { get; set; }
            Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1);
            int MiniBatchCounter = 0;
            public SampleRunner(DataEnviroument data, int groupSize, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                GroupSize = groupSize;
                //Roll = roll;
                MaxBatchSize = GroupSize; // * Roll;
                Shuffle = new DataRandomShuffling(Data.Triple.Count, random);
                Output = new GraphQueryData(MaxBatchSize, behavior.Device);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
                MiniBatchCounter = 0;
            }

            public override void Forward()
            {
                Output.RawRel.Clear();
                Output.RawSource.Clear();
                Output.RawTarget.Clear();
                Output.BlackTargets.Clear();
                Output.RawIndex.Clear();
                Output.RawStart.Clear();

                MiniBatchCounter += 1;

                if (Behavior.RunMode == DNNRunMode.Predict && BuilderParameters.TEST_SAMPLES > 0 && MiniBatchCounter > BuilderParameters.TEST_SAMPLES)
                {
                    IsTerminate = true; return;
                }

                int groupIdx = 0;

                while (groupIdx < GroupSize)
                {
                    int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext(); 
                    if (idx <= -1) { break; }

                    int srcId = Data.Triple[idx].Item1;
                    int linkId = Data.Triple[idx].Item2;
                    int tgtId = Data.Triple[idx].Item3;
                    List<int> truthIds = Data.TruthDict[new Tuple<int, int>(srcId, linkId)];

                        Output.RawRel.Add(linkId);
                        Output.RawSource.Add(srcId);
                        Output.RawIndex.Add(idx);
                        Output.RawTarget.Add(tgtId);
                        Output.BlackTargets.Add(truthIds);
                        Output.RawStart.Add(DataPanel.GraphEnv.StartIdx);

                    groupIdx += 1;
                }
                //Logger.WriteLog("Sampled Group Size {0}", groupIdx);
                if (groupIdx == 0) { IsTerminate = true; return; }
            }
        }


        /// <summary>
        /// Sample Graph in Memory. 
        /// </summary>
        class ReplayMemRunner : StructRunner
        {
            EpisodicMemory Data { get; set; }
            public new GraphQueryData Output { get; set; }
            int MaxBatchSize { get; set; }
            int GroupSize { get; set; }
            Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1);
            int MiniBatchCounter = 0;
            
            public ReplayMemRunner(EpisodicMemory data, int groupSize, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                GroupSize = groupSize;
                MaxBatchSize = GroupSize;
                Output = new GraphQueryData(MaxBatchSize, behavior.Device);
                Output.GuidPath = new List<Tuple<int, int, int, int>[]>();
                Output.GuidReward = new List<float>();
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                MiniBatchCounter = 0;
            }

            public override void Complete()
            {
                Output.GuidPath.Clear();
            }

            public override void Forward()
            {
                Output.RawRel.Clear();
                Output.RawSource.Clear();
                Output.RawTarget.Clear();
                Output.BlackTargets.Clear();
                Output.RawIndex.Clear();
                Output.GuidPath.Clear();
                Output.GuidReward.Clear();
                Output.RawStart.Clear();

                MiniBatchCounter += 1;

                if (MiniBatchCounter > BuilderParameters.LearnerEpoch)
                {
                    IsTerminate = true; 
                    return;
                }

                int groupIdx = 0;
                while (groupIdx < GroupSize)
                {
                    Tuple<int, int, int, int, Tuple<int, int, int, int>[], float> smp = 
                        Data.SampleReplayData();

                    int idx = smp.Item1;    
                    int srcId = smp.Item2; 
                    int linkId = smp.Item3;
                    int tgtId = smp.Item4;
                    float reward = smp.Item6;

                    Tuple<int, int, int, int>[] path = smp.Item5;
                    {
                        Output.RawRel.Add(linkId);
                        Output.RawSource.Add(srcId);
                        Output.RawIndex.Add(idx);
                        Output.RawTarget.Add(tgtId);
                        Output.BlackTargets.Add(null);
                        Output.GuidPath.Add(path);
                        Output.GuidReward.Add(reward);
                        Output.RawStart.Add(DataPanel.GraphEnv.StartIdx);
                    }
                    groupIdx += 1;
                }
                if (groupIdx == 0) { IsTerminate = true; return; }
            }
        }        

        class CandidateActionRunner : CompositeNetRunner
        {
            public new StatusData Input;
            public new Tuple<List<int>, List<int>> Output = null;

            GraphEnviroument Graph;
            public BiMatchBatchData Match;
            
            /// <summary>
            /// Input : State.
            /// node and rel embed.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="nodeEmbed"></param>
            /// <param name="relEmbed"></param>
            /// <param name="behavior"></param>
            public CandidateActionRunner(StatusData input,  GraphEnviroument graph, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;
                Graph = graph;

                int maxMatchSize = Input.MaxBatchSize * Graph.MaxNeighborNum;
                Match = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_MATCH_BATCHSIZE = maxMatchSize,
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = maxMatchSize
                }, behavior.Device);

                Output = new Tuple<List<int>, List<int>>(new List<int>(maxMatchSize), new List<int>(maxMatchSize));
            }

            public override void Forward()
            {
                Output.Item1.Clear();
                Output.Item2.Clear();
                Match.Clear();

                int cursor = 0;
                for (int b = 0; b < Input.BatchSize; b++)
                {
                    // current node.
                    int seedNode = Input.NodeID[b];

                    // query idx. 
                    int qid = Input.GetOriginalStatsIndex(b);

                    // raw node.
                    int rawN = Input.GraphQuery.RawSource[qid];

                    // raw relation.
                    int rawR = Input.GraphQuery.RawRel[qid];

                    // raw answer.
                    int answerN = Input.GraphQuery.RawTarget[qid];

                    int candidateNum = 0;

                    //&& !IsLast
                    if (Graph.Graph.ContainsKey(seedNode) )
                    {
                        for (int nei = 0; nei < Graph.Graph[seedNode].Count; nei++)
                        {
                            int lid = Graph.Graph[seedNode][nei].Item1;
                            int tgtNode = Graph.Graph[seedNode][nei].Item2;

                            if (seedNode == rawN && lid == rawR && tgtNode == answerN) { continue; }
                            if (seedNode == answerN && lid == DataPanel.ReverseRelation(rawR) && tgtNode == rawN) continue;

                            Output.Item1.Add(tgtNode);
                            Output.Item2.Add(lid); 
                        
                            Match.Push(b, cursor, 1);
                            cursor += 1;
                            candidateNum += 1;
                        }
                    }
                    // add stop action for each node.
                    {
                        Output.Item1.Add(seedNode);
                        Output.Item2.Add(Graph.StopIdx); 

                        Match.Push(b, cursor, 1);
                        cursor += 1;
                        candidateNum += 1;
                    }
                }
                Match.PushDone();
            }
        }

        static Random SampleRandom = new Random(DeepNet.BuilderParameters.RandomSeed);

        public class BasicPolicyRunner : CompositeNetRunner
        {
            public new StatusData Input { get; set; }

            /// <summary>
            /// MatchData.
            /// </summary>
            public BiMatchBatchData MatchPath = null;

            /// <summary>
            /// new Node Index.
            /// </summary>
            public List<int> NodeIndex { get; set; }

            /// <summary>
            /// next step log probability.
            /// </summary>
            public List<float> LogProb { get; set; }

            /// <summary>
            /// pre sel action and pre status. 
            /// </summary>
            public List<int> PreSelActIndex { get; set; }
            public List<int> PreStatusIndex { get; set; }

            public BasicPolicyRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;

                NodeIndex = new List<int>();
                LogProb = new List<float>();
                PreSelActIndex = new List<int>();
                PreStatusIndex = new List<int>();
            }
        }

        class ReplayPolicyRunner : BasicPolicyRunner
        {
            public ReplayPolicyRunner(StatusData input, int maxNeighbor, RunnerBehavior behavior) : base(input, behavior)
            {
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                }, behavior.Device);
            }
            
            // sample actions according to the replay memory.
            public override void Forward()
            {
                NodeIndex.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                LogProb.Clear();
                MatchPath.Clear();

                Input.MatchCandidateProb.Output.SyncToCPU();
                Input.MatchCandidateQ.Output.SyncToCPU();

                int currentStep = Input.Step;
                int batchSize = Input.MatchCandidateProb.Segment;
                for (int i = 0; i < batchSize; i++)
                {
                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];
                    
                    int actionDim = e - s;
                    int step = Input.Step;

                    int origialB = Input.GetOriginalStatsIndex(i);
                    
                    Tuple<int, int, int, int>[] path = Input.GraphQuery.GuidPath[origialB];
                    int selectIdx = s + path[step].Item1;

                    //selectIdx = s + idx;
                    float prob = Input.GetLogProb(i) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                    {
                        PreSelActIndex.Add(selectIdx);
                        PreStatusIndex.Add(i);
                        NodeIndex.Add(Input.MatchCandidate.Item1[selectIdx]);
                        LogProb.Add(prob);
                        MatchPath.Push(i, selectIdx, 1);
                    }
                }
                MatchPath.PushDone();
            }
        }

        class BeamSearchActionRunner : BasicPolicyRunner
        {
            int BeamSize = 1;

            /// <summary>
            /// group aware beam search.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public BeamSearchActionRunner(StatusData input, int maxNeighbor, int beamSize, RunnerBehavior behavior) : base(input, behavior)
            {
                BeamSize = beamSize;

                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
                }, behavior.Device);
            }

            public override void Forward()
            {
                if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }

                Input.MatchCandidateProb.Output.SyncToCPU();
                Input.MatchCandidateQ.Output.SyncToCPU();

                NodeIndex.Clear();
                LogProb.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                MatchPath.Clear();

                int cursor = 0;

                for (int i = 0; i < Input.GraphQuery.BatchSize; i++)
                {
                    List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);
                    MinMaxHeap<Tuple<int, int>> topKheap = new MinMaxHeap<Tuple<int, int>>(BeamSize, 1);
                    foreach (int b in idxs)
                    {
                        int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[b];
                        int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];
                        {
                            for (int t = s; t < e; t++)
                            {
                                float tmp_prob = Input.GetLogProb(b) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t]);
                                topKheap.push_pair(new Tuple<int, int>(b, t), tmp_prob);
                            }
                        }
                    }

                    while (!topKheap.IsEmpty)
                    {
                        KeyValuePair<Tuple<int, int>, float> p = topKheap.PopTop();


                        NodeIndex.Add(Input.MatchCandidate.Item1[p.Key.Item2]);
                        LogProb.Add(p.Value);

                        PreStatusIndex.Add(p.Key.Item1);
                        PreSelActIndex.Add(p.Key.Item2);
                        MatchPath.Push(p.Key.Item1, p.Key.Item2, p.Value);
                        
                        cursor += 1;
                    }
                }
                MatchPath.PushDone();
            }
        }

        class MCTSActionSamplingRunner : BasicPolicyRunner
        {
            EpisodicMemory Memory { get; set; }
            public int MCTSIdx { get; set; }
            /// <summary>
            /// match candidate, and match probability. 
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public MCTSActionSamplingRunner(StatusData input, int maxNeighbor, int mctsIdx, EpisodicMemory memory, RunnerBehavior behavior) : base(input, behavior)
            {
                Memory = memory;
                MCTSIdx = mctsIdx;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                }, behavior.Device);
            }

            public override void Forward()
            {
                NodeIndex.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                LogProb.Clear();
                MatchPath.Clear();

                Input.MatchCandidateProb.Output.SyncToCPU();
                Input.MatchCandidateQ.Output.SyncToCPU();

                int currentStep = Input.Step;
                int batchSize = Input.MatchCandidateProb.Segment;
                for (int i = 0; i < batchSize; i++)
                {
                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

                    string Qkey = Input.GetStatusKey(i);
                    Tuple<float[], float[], float> m = Memory.Search(Qkey);

                    int actionDim = e - s;
                    
                    if(actionDim == 0)
                    {
                        Logger.WriteLog("actionDim is zero, it is not acceptable.");
                        Console.ReadLine();
                    }

                    if( m != null && (actionDim != m.Item1.Length || actionDim != m.Item2.Length) ) 
                    {
                        Logger.WriteLog("mcts actionDim doesn't match {0}, {1}, {2}, Qkey {3}, Step {4}", 
                            actionDim, m.Item1.Length, m.Item2.Length, Qkey, currentStep);
                        Console.ReadLine();
                    } 

                    float[] prior_r = new float[actionDim];
                    Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);

                    int idx = 0;
                    {
                        int strategy = MCTSIdx; 
                        switch (strategy)
                        {
                            case 0: idx = BanditAlg.UniformRandomStrategy0(prior_r.Length, SampleRandom); break;
                            case 1: idx = BanditAlg.ThompasSampling(prior_r, SampleRandom); break;
                            case 2: idx = BanditAlg.UCB1Bandit(m, actionDim, BuilderParameters.UCB1_C, SampleRandom); break;
                            case 3: idx = BanditAlg.AlphaGoZeroBandit(m, prior_r, actionDim, BuilderParameters.PUCT_C, SampleRandom); break;
                            case 4: idx = BanditAlg.AlphaGoZeroBanditV2(m, prior_r, actionDim, BuilderParameters.PUCT_C, SampleRandom); break;
                            case 5: idx = BanditAlg.PUCB(m, prior_r, actionDim, BuilderParameters.PUCB_B, BuilderParameters.PUCB_C, BuilderParameters.PUCB_M, SampleRandom); break;
                            //case 6: idx = BanditAlg.AlphaGoZeroBandit_Stochastic(m, prior_q.ToList(), Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.TRAIN_C : BuilderParameters.TEST_C,
                            //                                                                          Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.TRAIN_L : BuilderParameters.TEST_L, SampleRandom); break;
                        }
                    }

                    int selectIdx = s + idx;
                    float prob = Input.GetLogProb(i) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                    {
                        PreSelActIndex.Add(selectIdx);
                        PreStatusIndex.Add(i);
                        NodeIndex.Add(Input.MatchCandidate.Item1[selectIdx]);
                        LogProb.Add(prob);
                        MatchPath.Push(i, selectIdx, 1);
                    }
                }
                MatchPath.PushDone();
            }
        }

        class PiRunner : ObjectiveRunner
        {
            new GraphQueryData Input { get; set; } 
            EpisodicMemory Mem { get; set; }
            int SmpNum = 0;
            float[] Pi_Value = null;
            int Epoch = 0;

            public PiRunner(GraphQueryData input, EpisodicMemory mem, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Mem = mem;
                SmpNum = 0;
                Pi_Value = new float[BuilderParameters.MAX_HOP]; 
                Epoch = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
                if(Epoch % 200 == 0)
                {
                    Logger.WriteLog("Pi Learner Epoch : {0}", Epoch);
                    for(int i = 0; i < Pi_Value.Length; i++)
                    {
                        Logger.WriteLog("Avg Q Step : {0}; Value {1};", i, Pi_Value[i] * 1.0f / SmpNum );
                    }
                    SmpNum = 0;
                    Array.Clear(Pi_Value, 0, Pi_Value.Length);
                }
            }

            public override void Forward()
            {
                float total_reward = 0;

                for (int i = 0; i < Input.StatusPath.Count ; i++)
                {
                    StatusData st = Input.StatusPath[i];
                    if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncToCPU();
                }
                
                float beta = 0.1f;
                // mcts sampling.
                {

                    for (int i = 0; i < Input.StatusPath.Last().BatchSize; i++)
                    {
                        int t = Input.StatusPath.Last().Step;
                        int b = i;
                        int predId = Input.StatusPath[t].NodeID[b];
                        
                        int origialB = Input.StatusPath[t].GetOriginalStatsIndex(b);

                        int targetId = Input.RawTarget[origialB];
                        int rawIdx = Input.RawIndex[origialB];
                        float reward = Input.GuidReward[origialB];

                        total_reward += reward;
                        {
                            float y = reward;
                            StatusData st = Input.StatusPath[t];
                            for (int pt = t - 1; pt >= 0; pt--)
                            {
                                int pb = st.GetPreStatusIndex(b);
                                int sidx = st.GetPreActionIndex(b); 

                                StatusData pst = Input.StatusPath[pt];

                                Tuple<float[], float[], float> m = Mem.Search(pst.GetStatusKey(pb));
                                int e = pst.MatchCandidateProb.SegmentIdx.MemPtr[pb];
                                int s = pb == 0 ? 0 : pst.MatchCandidateProb.SegmentIdx.MemPtr[pb - 1];
                                
                                float sum = m.Item2.Sum() * m.Item3 + (e - s) * beta;

                                for(int x = s; x < e; x++)
                                {
                                    pst.MatchCandidateProb.Deriv.MemPtr[x] += (m.Item2[x - s] * m.Item3 + beta ) / sum;
                                }
                                // calculate the average Q value for each step.
                                float pi = pst.MatchCandidateProb.Output.MemPtr[sidx];
                                Pi_Value[pt] = Pi_Value[pt] + pi;
                                
                                st = pst;
                                b = pb;
                            }
                        }
                    }
                    SmpNum += Input.StatusPath.Last().BatchSize;
                }
                ObjectiveScore = total_reward / (Input.StatusPath.Last().BatchSize + float.Epsilon);
                {
                    for (int i = 0; i < Input.StatusPath.Count ; i++)
                    {
                        StatusData st = Input.StatusPath[i];
                        if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
                    }
                }

            }
        }

        // Q Learning.
        class QRunner : ObjectiveRunner
        {
            new GraphQueryData Input { get; set; }
            int Epoch = 0;

            float[] Q_Value = null;
            float[] Q_MAE = null;
            int SmpNum = 0;

            public QRunner(GraphQueryData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Q_Value = new float[BuilderParameters.MAX_HOP]; 
                Q_MAE = new float[BuilderParameters.MAX_HOP];
                SmpNum = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
                if(Epoch % 200 == 0)
                {
                    Logger.WriteLog("Q Learner Epoch : {0}", Epoch);
                    for(int i = 0; i < Q_Value.Length; i++)
                    {
                        Logger.WriteLog("Avg Q Step : {0}; Value {1}; MAE {2}", i, Q_Value[i] * 1.0f / SmpNum, Q_MAE[i] * 1.0f / SmpNum );
                    }
                    SmpNum = 0;
                    Array.Clear(Q_Value, 0, Q_Value.Length);
                    Array.Clear(Q_MAE, 0, Q_MAE.Length);
                }
            }

            public override void Forward()
            {
                float total_reward = 0;
                int sampleNum = 0; 
                {
                    for (int i = 0; i < Input.StatusPath.Count ; i++)
                    {
                        StatusData st = Input.StatusPath[i];
                        if (st.MatchCandidateQ != null) st.MatchCandidateQ.Deriv.SyncToCPU();
                    }
                }
                // mcts sampling.
                {
                    for (int i = 0; i < Input.StatusPath.Last().BatchSize; i++)
                    {
                        int t = Input.StatusPath.Last().Step;
                        int b = i;
                        int predId = Input.StatusPath[t].NodeID[b];
                        int origialB = Input.StatusPath[t].GetOriginalStatsIndex(b);
                        int targetId = Input.RawTarget[origialB];
                        int rawIdx = Input.RawIndex[origialB];
                        float reward = Input.GuidReward[origialB];

                        total_reward += reward;
                        sampleNum += 1;
                        {
                            float y = reward;
                            StatusData st = Input.StatusPath[t];
                            for (int pt = t - 1; pt >= 0; pt--)
                            {
                                int pb = st.GetPreStatusIndex(b);
                                int sidx = st.GetPreActionIndex(b); 

                                StatusData pst = Input.StatusPath[pt];

                                // calculate the average Q value for each step.
                                float q = pst.MatchCandidateQ.Output.MemPtr[sidx];
                                Q_Value[pt] = Q_Value[pt] + q;
                                Q_MAE[pt] = Q_MAE[pt] + Math.Abs(y - q);
                                
                                pst.MatchCandidateQ.Deriv.MemPtr[sidx] += (y - q);// * 1.0f / Input.Count;
                                
                                int e = pst.MatchCandidateQ.SegmentIdx.MemPtr[pb];
                                int s = pb == 0 ? 0 : pst.MatchCandidateQ.SegmentIdx.MemPtr[pb - 1];
                                int maxQidx = Util.MaximumValue(pst.MatchCandidateQ.Output.MemPtr, s, e);
                                y = BuilderParameters.REWARD_DISCOUNT * pst.MatchCandidateQ.Output.MemPtr[maxQidx];

                                st = pst;
                                b = pb;
                            }
                        }
                    }
                    SmpNum += Input.StatusPath.Last().BatchSize;
                }
                
                ObjectiveScore = total_reward / (sampleNum + float.Epsilon);

                {
                    for (int i = 0; i < Input.StatusPath.Count ; i++)
                    {
                        StatusData st = Input.StatusPath[i];

                        if (st.MatchCandidateQ != null) st.MatchCandidateQ.Deriv.SyncFromCPU();
                    }
                }
            }
        }

        public class RollBackRunner : StructRunner
        {
            new GraphQueryData Input { get; set; }
            EpisodicMemory Memory { get; set; }

            int PosNum = 0;
            int NegNum = 0;
            int BlackNum = 0;
            int SmpNum = 0;
            int Epoch = 0;
            public int ReportPerEpoch = 1;
            public override void Init()
            {
                SmpNum = 0;
                PosNum = 0;
                NegNum = 0;
                BlackNum = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
                if(Behavior.RunMode == DNNRunMode.Train)
                { 
                    Memory.UpdateTiming();
                }

                if(Epoch % ReportPerEpoch == 0)
                {
                    ReportNum();
                }
            }

            public void ReportNum()
            {
                Logger.WriteLog("Sample Num {0}", SmpNum);
                Logger.WriteLog("Pos Num {0}, Pos Mean {1}", PosNum, PosNum / (SmpNum + 0.000001f));
                Logger.WriteLog("Neg Num {0}, Neg Mean {1}", NegNum, NegNum / (SmpNum + 0.000001f));
                Logger.WriteLog("Black Num {0}, Black Mean {1}", BlackNum, BlackNum / (SmpNum + 0.000001f));
            }

            public RollBackRunner(GraphQueryData input, EpisodicMemory memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Memory = memory;
            }

            public override void Forward()
            {
                StatusData st = Input.StatusPath.Last();
                int t = st.Step; 

                for (int i = 0; i < st.BatchSize; i++)
                {
                    int b = i; 
                    int origialB = st.GetOriginalStatsIndex(b);

                    int predId = st.NodeID[b];

                    int targetId = Input.RawTarget[origialB];
                    int raw_b = Input.RawIndex[origialB];
                    int raw_src = Input.RawSource[origialB];
                    int raw_rel = Input.RawRel[origialB];

                    bool isBlack = true;
                    float normReward = 0;
                    float buffReward = 0;

                    if (Input.BlackTargets[origialB].Contains(predId) && predId != targetId)
                    {
                        buffReward = BuilderParameters.BLACK_R;
                        normReward = 0;
                        BlackNum += 1;
                        isBlack = true;
                    }
                    else if (targetId == predId)
                    {
                        buffReward = BuilderParameters.POS_R;
                        normReward = 1;
                        PosNum += 1;
                        isBlack = false;
                    }
                    else
                    {
                        buffReward = BuilderParameters.NEG_R;
                        normReward = 0;
                        NegNum += 1;
                        isBlack = false;
                    }

                    if(Behavior.RunMode == DNNRunMode.Predict) 
                    {
                        if(!isBlack) normReward = st.GetPreQValue(b) * 1.0f / BuilderParameters.POS_R;
                        isBlack = true;
                    }

                    Tuple<int, int, int, int>[] visitPath = st.GetPath(b);
                    Memory.UpdateMem(raw_b, raw_src, raw_rel, visitPath, normReward);
                    if(!isBlack)
                    {
                        Memory.AddReplayBuff(raw_b, raw_src, raw_rel, targetId, visitPath, buffReward); 
                    }
                    SmpNum += 1;
                }
            }
        }

        class AggregrateRunner : StructRunner
        {
            GraphQueryData Query { get; set; }
            Dictionary<int, List<Tuple<int, float>>> AggDict { get; set; }
            Dictionary<int, int> TgtDict { get; set; }

            int ScoreType { get; set; }
            public AggregrateRunner(GraphQueryData query, Dictionary<int, List<Tuple<int, float>>> aggDict, int scoreType, Dictionary<int, int> tgtDict, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Query = query;
                AggDict = aggDict;
                TgtDict = tgtDict;
                ScoreType = scoreType;
            }

            public override void Forward()
            {
                DataUtil.AggregrateScore(Query, AggDict, ScoreType, TgtDict);
            }
        }

        public class NeuralWalkerModel : CompositeNNStructure
        {
            public EmbedStructure NodeEmbed { get; set; }
            public EmbedStructure RelEmbed { get; set; }

            public DNNStructure SrcDNN { get; set; }
            public GRUCell GruCell { get; set; }

            public NeuralWalkerModel(int entityNum, int relationNum, int nodeDim, int relDim, int rnnDim, DeviceType device)
            {
                NodeEmbed = AddLayer(new EmbedStructure(entityNum, nodeDim, device)); // DeviceType.CPU_FAST_VECTOR));
                RelEmbed = AddLayer(new EmbedStructure(relationNum, relDim, device)); // DeviceType.CPU_FAST_VECTOR));
                int embedDim = nodeDim + relDim;
                GruCell = AddLayer(new GRUCell(embedDim, rnnDim, device));
                
                int inputDim = rnnDim + embedDim;
                List<int> mapLayers = new List<int>(BuilderParameters.DNN_DIMS);
                mapLayers.Add(embedDim);
                SrcDNN = AddLayer(new DNNStructure(inputDim, mapLayers.ToArray(),
                                           mapLayers.Select(i => A_Func.Tanh).ToArray(),
                                           mapLayers.Select(i => true).ToArray(),
                                           device));
            }
            
            public override void CopyFrom(IData other) 
            {
                NeuralWalkerModel src = (NeuralWalkerModel)other;
                NodeEmbed.CopyFrom(src.NodeEmbed);
                RelEmbed.CopyFrom(src.RelEmbed);
                GruCell.CopyFrom(src.GruCell);
                SrcDNN.CopyFrom(src.SrcDNN);   
            }


            public NeuralWalkerModel(BinaryReader reader, DeviceType device)
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

                NodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                RelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                GruCell = (GRUCell)DeserializeNextModel(reader, device);
                SrcDNN = (DNNStructure)DeserializeNextModel(reader, device);
            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        public static void ReinforceWalkModel(ComputationGraph cg, GraphQueryData graphQuery, GraphEnviroument graph, 
                                  int policyId, int batchSize, int beamSize, int maxHop, EpisodicMemory mem, NeuralWalkerModel model, RunnerBehavior Behavior)
        {
            HiddenBatchData srcEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawSource, graphQuery.MaxBatchSize, model.NodeEmbed, Behavior));
            HiddenBatchData relEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawRel, graphQuery.MaxBatchSize, model.RelEmbed, Behavior));
            HiddenBatchData startEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawStart, graphQuery.MaxBatchSize, model.RelEmbed, Behavior));

            IntArgument batchSizeArg = new IntArgument("sample-batch-size"); 
            cg.AddRunner(new HiddenDataBatchSizeRunner(relEmbed, batchSizeArg, Behavior));

            HiddenBatchData statusEmbed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { srcEmbed, startEmbed }, Behavior));
            HiddenBatchData queryEmbed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { srcEmbed, relEmbed }, Behavior));

            HiddenBatchData initZeroH0 = (HiddenBatchData)cg.AddRunner(new ZeroMatrixRunner(statusEmbed.MAX_BATCHSIZE, model.GruCell.HiddenStateDim, batchSizeArg, Behavior));

            GRUStateRunner initStateRunner = new GRUStateRunner(model.GruCell, initZeroH0, statusEmbed, Behavior);
            cg.AddRunner(initStateRunner);

            HiddenBatchData H1 = initStateRunner.Output;
            HiddenBatchData Rq = queryEmbed; 
            
            StatusData status = new StatusData(graphQuery, graphQuery.RawSource, H1, Behavior.Device);

            #region multi-hop expansion
            // travel four steps in the knowledge graph.
            for (int i = 0; i < maxHop; i++)
            {
                // given status, obtain the match. miniBatch * maxNeighbor number.
                CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, graph, Behavior);
                cg.AddRunner(candidateActionRunner);

                // miniMatch * maxNeighborNumber.
                HiddenBatchData candNodeEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(candidateActionRunner.Output.Item1, 
                                                                        candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, 
                                                                        model.NodeEmbed, Behavior));

                HiddenBatchData candRelEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(candidateActionRunner.Output.Item2, 
                                                                        candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, 
                                                                        model.RelEmbed, Behavior));
                
                HiddenBatchData A_data = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>()
                                                                        { candNodeEmbed, candRelEmbed }, Behavior));


                HiddenBatchData I_data = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>()
                                            { status.StateEmbed, Rq }, Behavior));

                DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, I_data, Behavior);
                cg.AddRunner(srcHiddenRunner);
                MatrixInnerProductRunner innerRunner = new MatrixInnerProductRunner(new MatrixData(srcHiddenRunner.Output),
                                                                                    new MatrixData(A_data), candidateActionRunner.Match, Behavior);
                cg.AddRunner(innerRunner);

                status.MatchCandidate = candidateActionRunner.Output;
                status.MatchCandidateQ = new SeqVectorData(innerRunner.Output.MaxLength,
                                                           status.MaxBatchSize, innerRunner.Output.Output, innerRunner.Output.Deriv,
                                                           candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx, Behavior.Device);

                SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(status.MatchCandidateQ, 1, Behavior, true);
                

                cg.AddRunner(normAttRunner);

                status.MatchCandidateProb = normAttRunner.Output;

                BasicPolicyRunner policyRunner = null;
                // mcts policy.
                if(policyId == 0)
                {
                    policyRunner = new MCTSActionSamplingRunner(status, graph.MaxNeighborNum, BuilderParameters.ActorPolicy, mem, Behavior);
                }
                // replay policy.
                else if(policyId == 1)
                {
                    policyRunner = new ReplayPolicyRunner(status, graph.MaxNeighborNum, Behavior);
                }
                // beam search policy.
                else if(policyId == 2)
                {
                    //Logger.WriteLog("Set up beam search runner");
                    policyRunner = new BeamSearchActionRunner(status, graph.MaxNeighborNum, beamSize, Behavior);
                }

                cg.AddRunner(policyRunner);

                //MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(A_data, policyRunner.MatchPath, 2, Behavior);
                //cg.AddRunner(tgtExpRunner);

                MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
                cg.AddRunner(srcExpRunner);

                MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(A_data, policyRunner.MatchPath, 2, Behavior);
                cg.AddRunner(tgtExpRunner);

                MatrixExpansionRunner rQExpRunner = new MatrixExpansionRunner(Rq, policyRunner.MatchPath, 1, Behavior);
                cg.AddRunner(rQExpRunner);
                Rq = rQExpRunner.Output;

                GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
                cg.AddRunner(stateRunner);

                StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.LogProb,
                    policyRunner.PreSelActIndex, policyRunner.PreStatusIndex, stateRunner.Output, Behavior.Device);

                status = nextStatus;
            }
            #endregion.
        }

        // actor model.
        public static void ActorModel(ComputationGraph cg, DataEnviroument data, GraphEnviroument graph, int batchSize, int maxHop,
                                                EpisodicMemory mem, NeuralWalkerModel model, RunnerBehavior Behavior)
        {
            SampleRunner SmpRunner = new SampleRunner(data, batchSize, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;
            
            ReinforceWalkModel(cg, interface_data, graph, 0, batchSize, 1, maxHop, mem, model, Behavior);

            RollBackRunner rollbackRunner = new RollBackRunner(interface_data, mem, Behavior);
            cg.AddRunner(rollbackRunner);
        }

        // learner model.
        public static void LearnerModel(ComputationGraph cg, GraphEnviroument graph, int batchSize, int maxHop,
                                                EpisodicMemory mem, NeuralWalkerModel model, RunnerBehavior Behavior)
        {
            ReplayMemRunner replayRunner = new ReplayMemRunner(mem, batchSize, Behavior);
            cg.AddDataRunner(replayRunner);
            GraphQueryData interface_data = replayRunner.Output;
            
            ReinforceWalkModel(cg, interface_data, graph, 1, batchSize, 1, maxHop, mem, model, Behavior);

            PiRunner piRunner = new PiRunner(interface_data, mem, Behavior);
            cg.AddObjective(piRunner);

            cg.SetDelegateModel(model);
        }

        public static void TestBSModel(ComputationGraph cg, DataEnviroument data, GraphEnviroument graph, int batchSize, int beamSize,
            int maxHop, EpisodicMemory mem, Dictionary<int, List<Tuple<int, float>>> aggScore, Dictionary<int, int> aggTgt, NeuralWalkerModel model, RunnerBehavior Behavior) 
        {
            SampleRunner SmpRunner = new SampleRunner(data, batchSize, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;

            ReinforceWalkModel(cg, interface_data, graph, 2, batchSize, beamSize, maxHop, mem, model, Behavior);
            
            // aggregate path probability;
            AggregrateRunner aggRunner = new AggregrateRunner(interface_data, aggScore, 1, aggTgt, Behavior);
            cg.AddRunner(aggRunner);
        }

        public static void TestMCTSModel(ComputationGraph cg, DataEnviroument data, GraphEnviroument graph, int batchSize,
            int maxHop, EpisodicMemory mem, Dictionary<int, List<Tuple<int, float>>> aggScore, Dictionary<int, int> aggTgt, NeuralWalkerModel model, RunnerBehavior Behavior) 
        {
            SampleRunner smpRunner = new SampleRunner(data, batchSize, Behavior);
            cg.AddDataRunner(smpRunner);
            GraphQueryData interface_data = smpRunner.Output;

            ReinforceWalkModel(cg, interface_data, graph, 0, batchSize, 1, maxHop, mem, model, Behavior);
            
            RollBackRunner rollbackRunner = new RollBackRunner(interface_data, mem, Behavior);
            rollbackRunner.ReportPerEpoch = 500;
            cg.AddRunner(rollbackRunner);

            // aggregate last step q value;
            AggregrateRunner aggRunner = new AggregrateRunner(interface_data, aggScore, 0, aggTgt, Behavior);
            cg.AddRunner(aggRunner);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            Dictionary<int, int> step2 = DataPanel.GraphEnv.TestDataGraph(DataPanel.TestData);
            Dictionary<int, int> step3 = DataPanel.GraphEnv.TestDataGraph(DataPanel.DevData);
            
            foreach (KeyValuePair<int, int> i in step2)
            {
                Logger.WriteLog("Test Step {0}, Stat {1}", i.Key, i.Value);
            }

            foreach (KeyValuePair<int, int> i in step3)
            {
                Logger.WriteLog("Dev Step {0}, Stat {1}", i.Key, i.Value);
            }
            
            NeuralWalkerModel model =
                BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
                new NeuralWalkerModel(DataPanel.EntityDict.Count, DataPanel.RelationDict.Count, 
                                      BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, BuilderParameters.RNN_Dim, device) :
                new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
            model.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

            Logger.WriteLog("Model Parameter Number {0}", model.ParamNumber);

            EpisodicMemory Memory = new EpisodicMemory(true);
            EpisodicMemory TestMemory = new EpisodicMemory(false);

            ComputationGraph testCG = new ComputationGraph();
            ComputationGraph validCG = new ComputationGraph();
            ComputationGraph actorCG = new ComputationGraph();
            ComputationGraph learnerCG = new ComputationGraph();

            Dictionary<int, List<Tuple<int, float>>> AggScore = new Dictionary<int, List<Tuple<int, float>>>();
            Dictionary<int, int> AggTgt = new Dictionary<int, int>();

            if(BuilderParameters.BeamSize > 0 && BuilderParameters.MCTSNum == 0)
            {
                TestBSModel(testCG, DataPanel.TestData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize, BuilderParameters.BeamSize,
                    BuilderParameters.MAX_HOP, Memory, AggScore, AggTgt, model, 
                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                TestBSModel(validCG, DataPanel.DevData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize, BuilderParameters.BeamSize,
                    BuilderParameters.MAX_HOP, Memory, AggScore, AggTgt, model, 
                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
            }
            else if(BuilderParameters.BeamSize == 0 && BuilderParameters.MCTSNum > 0)
            {
                TestMCTSModel(testCG, DataPanel.TestData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize, 
                    BuilderParameters.MAX_HOP, TestMemory, AggScore, AggTgt, model, 
                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
             
                TestMCTSModel(validCG, DataPanel.DevData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize, 
                    BuilderParameters.MAX_HOP, TestMemory, AggScore, AggTgt, model, 
                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });   
            }
            else
            {
                throw new Exception("only one of BeamSize and MCTSNum should be non zero.");
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    NeuralWalkerModel backupModel = new NeuralWalkerModel(DataPanel.EntityDict.Count, DataPanel.RelationDict.Count, 
                                      BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, BuilderParameters.RNN_Dim, device);
                    backupModel.CopyFrom(model);
                    Logger.WriteLog("Generate backup Model.");

                    ActorModel(actorCG, BuilderParameters.DEV_AS_TRAIN > 0 ? DataPanel.DevData : DataPanel.TrainData, 
                            DataPanel.GraphEnv, BuilderParameters.ActorMiniBatch, BuilderParameters.MAX_HOP, Memory, backupModel,
                            new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
            
                    LearnerModel(learnerCG, DataPanel.GraphEnv, BuilderParameters.LearnerMiniBatch, BuilderParameters.MAX_HOP, Memory, model, 
                            new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    actorCG.Init();
                    learnerCG.IsReportObjectives = false;
                    
                    int actorEpoch = 0;

                    for(int iter = 1; iter <= OptimizerParameters.Iteration; iter++)
                    {
                        // actor model collect the data into replay memory.
                        if(!actorCG.Forward())
                        {
                            actorCG.Complete();
                            actorEpoch += 1;
                            actorCG.Init();
                            Logger.WriteLog("actor CG restart iteration : {0}; epoch {1}", iter, actorEpoch);
                            continue;
                        }

                        if(Memory.IsReplayBufferReady)
                        {
                            learnerCG.Execute();
                        }

                        if( iter % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            Logger.WriteLog("Evaluation at Iteration {0}", iter);

                            AggScore.Clear();
                            AggTgt.Clear();
                            TestMemory.Clear();
                            for(int i=0; i <= BuilderParameters.MCTSNum; i++)
                            {
                                testCG.Execute();
                                if( i % 500 == 0)
                                {
                                    Logger.WriteLog("Test evaluation at mcts {0} .....", i );
                                    Dictionary<string, float> result = DataUtil.HitK(AggScore, AggTgt);
                                    foreach(KeyValuePair<string, float> p in result)
                                    {
                                        Logger.WriteLog("{0} : {1}", p.Key, p.Value * 1.0f / AggTgt.Count);
                                    }
                                }
                            }

                            AggScore.Clear();
                            AggTgt.Clear();
                            TestMemory.Clear();
                            for(int i=0; i <= BuilderParameters.MCTSNum; i++)
                            {
                                validCG.Execute();
                                if( i % 500 == 0)
                                {
                                    Logger.WriteLog("Valid evaluation at mcts {0} .....", i );
                                    Dictionary<string, float> result = DataUtil.HitK(AggScore, AggTgt);
                                    foreach(KeyValuePair<string, float> p in result)
                                    {
                                        Logger.WriteLog("{0} : {1}", p.Key, p.Value * 1.0f / AggTgt.Count);
                                    }
                                }
                            }

                        }
                        if(iter % BuilderParameters.ModelSyncUp == 0)
                        {
                            Logger.WriteLog("Model backup at Iteration {0}", iter);
                            backupModel.CopyFrom(model);
                        }
                
                    }
                    break;
                case DNNRunMode.Predict:
                    AggScore.Clear();
                    AggTgt.Clear();
                    TestMemory.Clear();
                    for(int i=0; i <= BuilderParameters.MCTSNum; i++)
                    {
                        testCG.Execute();
                        if( i % 500 == 0)
                        {
                            Logger.WriteLog("Test evaluation at mcts {0} .....", i );
                            Dictionary<string, float> result = DataUtil.HitK(AggScore, AggTgt);
                            foreach(KeyValuePair<string, float> p in result)
                            {
                                Logger.WriteLog("{0} : {1}", p.Key, p.Value * 1.0f / AggTgt.Count);
                            }
                        }
                    }

                    AggScore.Clear();
                    AggTgt.Clear();
                    TestMemory.Clear();
                    for(int i=0; i <= BuilderParameters.MCTSNum; i++)
                    {
                        validCG.Execute();
                        if( i % 500 == 0)
                        {
                            Logger.WriteLog("Valid evaluation at mcts {0} .....", i );
                            Dictionary<string, float> result = DataUtil.HitK(AggScore, AggTgt);
                            foreach(KeyValuePair<string, float> p in result)
                            {
                                Logger.WriteLog("{0} : {1}", p.Key, p.Value * 1.0f / AggTgt.Count);
                            }
                        }
                    }
                    break;
            }    
            Logger.CloseLog();
        }
        
        class DataUtil 
        {
            /// scoreType 0: Q value; 1 : Path probability;
            public static void AggregrateScore(GraphQueryData query, Dictionary<int, List<Tuple<int, float>>> result, int scoreType, Dictionary<int, int> tgt)
            {
                StatusData st = query.StatusPath.Last();
                {
                    int t = st.Step;
                    for (int i = 0; i < st.BatchSize; i++) //.Results.Count; i++)
                    {
                        int b = i;
                        int predId = st.NodeID[b];
                        int origialB = st.GetOriginalStatsIndex(b);
                        int rawIdx = query.RawIndex[origialB];
                        float score = scoreType == 0 ? st.GetPreQValue(b) : st.GetLogProb(b);
                        int tgtIdx =  query.RawTarget[origialB];
                        if(!result.ContainsKey(rawIdx)) result.Add(rawIdx, new List<Tuple<int, float>>());
                        if(!tgt.ContainsKey(rawIdx)) tgt.Add(rawIdx, tgtIdx);
                        if(query.BlackTargets[origialB].Contains(predId) && query.RawTarget[origialB] != predId)
                        {
                            continue;
                        }
                        result[rawIdx].Add(new Tuple<int, float>(predId, score));
                    }
                }
            }

            public static Dictionary<string, float> HitK(Dictionary<int, List<Tuple<int, float> > > aggScore, Dictionary<int, int> tgt)
            {
                Dictionary<string, float> report = new Dictionary<string, float>();

                float AP = 0;
                float Hit1 = 0;
                float Hit3 = 0;
                float Hit5 = 0;
                float Hit10 = 0;
                float Hit20 = 0;
                float Hited = 0;
                foreach(KeyValuePair<int, int> pair in tgt)
                {
                    List<Tuple<int, float>> score = aggScore[pair.Key];
                    score.Sort((x, y) => y.Item2.CompareTo(x.Item2));

                    int rawTarget = pair.Value;
                    int pos = 1;
                    bool isFound = false;
                    HashSet<int> visitIdx = new HashSet<int>();
                    foreach (Tuple<int, float> item in score)
                    {
                        if (visitIdx.Contains(item.Item1)) continue;
                        visitIdx.Add(item.Item1);
                        if(item.Item1 == rawTarget)
                        {
                            isFound = true;
                            break;
                        }
                        pos += 1;
                    }
                    if (isFound)
                    {
                        AP += 1.0f / pos;
                        Hited += 1;
                        if(pos <= 1) { Hit1 += 1; }
                        if (pos <= 3) { Hit3 += 1; }
                        if (pos <= 5) { Hit5 += 1; }
                        if (pos <= 10) { Hit10 += 1; }
                        if (pos <= 20) { Hit20 += 1; }
                    }
                }
                report["AP"] = AP;
                report["Hit1"] = Hit1;
                report["Hit3"] = Hit3;
                report["Hit5"] = Hit5;
                report["Hit10"] = Hit10;
                report["Hit20"] = Hit20;
                report["Hited"] = Hited;
                return report;   
            }
        }

        public class DataPanel 
        {
            public static GraphEnviroument GraphEnv { get; set; }

            public static DataEnviroument TrainData { get; set; }
            public static DataEnviroument TestData { get; set; }
            public static DataEnviroument DevData { get; set; }

            public static Dictionary<string, int> EntityDict { get; set; }
            public static Dictionary<string, int> RelationDict { get; set; }

            public static Dictionary<int, string> EntityLookup { get; set; }
            public static Dictionary<int, string> RelationLookup { get; set; }

            public static int ReverseRelation(int relation)
            {
                string relStr = RelationLookup[relation];
                if(relStr.StartsWith("_"))
                {
                    string newRel = relStr.Substring(1);
                    if(RelationDict.ContainsKey(newRel))
                    {
                        return RelationDict[newRel];
                    }
                }

                string newRel2 = "_" + relStr;
                if (RelationDict.ContainsKey(newRel2))
                {
                    return RelationDict[newRel2];
                }


                if(relStr.EndsWith("_inv"))
                {
                    string newRel3 = relStr.Substring(0, relStr.Length - 4);
                    if(RelationDict.ContainsKey(newRel3))
                    {
                        return RelationDict[newRel3];
                    }
                }

                string newRel4 = relStr + "_inv";
                if (RelationDict.ContainsKey(newRel4))
                {
                    return RelationDict[newRel4];
                }

                return -1;
                //throw new NotImplementedException("Cannot find the reverse Relation " + relStr);
            }

            public static string START_SYMBOL = "#START#";
            public static string END_SYMBOL = "#END#";

            public static void Init()
            {
                EntityDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(BuilderParameters.InputDir + "/vocab/entity_vocab.json"));
                RelationDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(BuilderParameters.InputDir + "/vocab/relation_vocab.json"));
                RelationDict.Add(END_SYMBOL, RelationDict.Count);
                RelationDict.Add(START_SYMBOL, RelationDict.Count);

                EntityLookup = new Dictionary<int, string>();
                foreach (KeyValuePair<string, int> item in EntityDict)
                {
                    EntityLookup.Add(item.Value, item.Key);
                }

                RelationLookup = new Dictionary<int, string>();
                foreach (KeyValuePair<string, int> item in RelationDict)
                {
                    RelationLookup.Add(item.Value, item.Key);
                }

                GraphEnv = new GraphEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + "/graph.txt");

                TrainData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + "/train.txt",
                                            new string[] { BuilderParameters.InputDir + "/train.txt" });

                TestData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + "/test.txt",
                                            new string[] { BuilderParameters.InputDir + "/test.txt",
                                                           BuilderParameters.InputDir + "/train.txt",
                                                           BuilderParameters.InputDir + "/dev.txt",
                                                           BuilderParameters.InputDir + "/graph.txt" });

                DevData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + "/dev.txt",
                                            new string[] { BuilderParameters.InputDir + "/test.txt",
                                                           BuilderParameters.InputDir + "/train.txt",
                                                           BuilderParameters.InputDir + "/dev.txt",
                                                           BuilderParameters.InputDir + "/graph.txt" });
            }

        }

        public class DataEnviroument 
        {
            public List<Tuple<int, int, int>> Triple { get; set; }
            public List<int> ShortestReachStep { get; set; }
            public Dictionary<Tuple<int, int>, List<int>> TruthDict { get; set; }

            public DataEnviroument(Dictionary<string, int> entityDict, Dictionary<string, int> relationDict, string tripleFile, string[] graphFiles)
            {
                Triple = new List<Tuple<int, int, int>>();
                TruthDict = new Dictionary<Tuple<int, int>, List<int>>();
                ShortestReachStep = new List<int>();
                int missNum = 0;
                using (StreamReader tripleReader = new StreamReader(tripleFile))
                {
                    while (!tripleReader.EndOfStream)
                    {
                        string[] tokens = tripleReader.ReadLine().Split('\t');

                        if (!entityDict.ContainsKey(tokens[0]))
                        {
                            Console.WriteLine("Graph doesn't exist node {0}", tokens[0]);
                            missNum += 1;
                            continue;
                        }

                        if (!relationDict.ContainsKey(tokens[1]))
                        {
                            Console.WriteLine("Graph doesn't exist relation {0}", tokens[1]);
                            missNum += 1;
                            continue;
                        }

                        if (!entityDict.ContainsKey(tokens[2]))
                        {
                            Console.WriteLine("Graph doesn't exist node {0}", tokens[2]);
                            missNum += 1;
                            continue;
                        }

                        int e1 = entityDict[tokens[0]];
                        int r = relationDict[tokens[1]];
                        int e2 = entityDict[tokens[2]];

                        Triple.Add(new Tuple<int, int, int>(e1, r, e2));
                    }

                    Logger.WriteLog("{0} Graph miss connections {1}", tripleFile, missNum);
                }

                foreach (string gStr in graphFiles)
                {
                    using (StreamReader gReader = new StreamReader(gStr))
                    {
                        while (!gReader.EndOfStream)
                        {
                            string[] tokens = gReader.ReadLine().Split('\t');


                            if (!entityDict.ContainsKey(tokens[0]))
                            {
                                Console.WriteLine("Graph doesn't exist node {0}", tokens[0]);
                                continue;
                            }

                            if (!relationDict.ContainsKey(tokens[1]))
                            {
                                Console.WriteLine("Graph doesn't exist relation {0}", tokens[1]);
                                continue;
                            }

                            if (!entityDict.ContainsKey(tokens[2]))
                            {
                                Console.WriteLine("Graph doesn't exist node {0}", tokens[2]);
                                continue;
                            }

                            int e1 = entityDict[tokens[0]];
                            int r = relationDict[tokens[1]];
                            int e2 = entityDict[tokens[2]];

                            Tuple<int, int> gKey = new Tuple<int, int>(e1, r);

                            if (!TruthDict.ContainsKey(gKey))
                            {
                                TruthDict.Add(gKey, new List<int>());
                            }
                            TruthDict[gKey].Add(e2);
                        }
                    }
                }
            }
        }

        public class GraphEnviroument 
        {
            public Dictionary<int, List<Tuple<int, int>>> Graph { get; set; }

            public int MaxNeighborNum { get; set; }
            public int StopIdx { get; set; }
            public int StartIdx { get; set; }
            public GraphEnviroument(Dictionary<string, int> entityDict, Dictionary<string, int> relationDict, string graphFile)
            {
                Graph = new Dictionary<int, List<Tuple<int, int>>>();
                StopIdx = relationDict[DataPanel.END_SYMBOL];
                StartIdx = relationDict[DataPanel.START_SYMBOL];
                int missNum = 0;
                using (StreamReader graphReader = new StreamReader(graphFile))
                {
                    while (!graphReader.EndOfStream)
                    {
                        string[] tokens = graphReader.ReadLine().Split('\t');

                        if (!entityDict.ContainsKey(tokens[0]))
                        {
                            Console.WriteLine("Graph doesn't exist node {0}", tokens[0]);
                            missNum += 1;
                            continue;
                        }

                        if (!relationDict.ContainsKey(tokens[1]))
                        {
                            Console.WriteLine("Graph doesn't exist relation {0}", tokens[1]);
                            missNum += 1;
                            continue;
                        }

                        if(!entityDict.ContainsKey(tokens[2]))
                        {
                            Console.WriteLine("Graph doesn't exist node {0}", tokens[2]);
                            missNum += 1;
                            continue;
                        }

                        int e1 = entityDict[tokens[0]];
                        int r = relationDict[tokens[1]];
                        int e2 = entityDict[tokens[2]];

                        if (!Graph.ContainsKey(e1)) { Graph.Add(e1, new List<Tuple<int, int>>()); }

                        // set the maximum action number.
                        if (BuilderParameters.MAX_ACTION_NUM > 0 && Graph[e1].Count >= BuilderParameters.MAX_ACTION_NUM)
                            continue;
                        Graph[e1].Add(new Tuple<int, int>(r, e2));
                    }

                    Logger.WriteLog("{0} Graph miss connections {1}", graphFile, missNum);
                }

                MaxNeighborNum = 0;
                foreach (int entity in Graph.Keys)
                {
                    //Graph[entity].Add(new Tuple<int, int>(StopIdx, entity));
                    if (Graph[entity].Count + 1 >= MaxNeighborNum ) { MaxNeighborNum = Graph[entity].Count + 1; }
                }

                Logger.WriteLog("Max Neighbor Number {0}", MaxNeighborNum);
            }

            public Dictionary<int, int> TestDataGraph(DataEnviroument data)
            {
                Dictionary<int, int> stepStat = new Dictionary<int, int>();
                data.ShortestReachStep.Clear();
                foreach(Tuple<int, int, int> item in data.Triple)
                {
                    int step = ShortestPath(item.Item1, item.Item3, item.Item2, BuilderParameters.MAX_HOP);
                    if(!stepStat.ContainsKey(step))
                    {
                        stepStat.Add(step, 1);
                    }
                    else
                    {
                        stepStat[step] += 1;
                    }
                    data.ShortestReachStep.Add(step);
                }
                return stepStat;
            }
            
            public HashSet<int> NeighborSet(int e, int maxHop)
            {
                List<KeyValuePair<int, int>> searchNodes = new List<KeyValuePair<int, int>>();
                HashSet<int> history = new HashSet<int>();

                searchNodes.Add(new KeyValuePair<int, int>(e, 0));
                history.Add(e);

                int begin = 0;
                int end = searchNodes.Count;
                int iter = 0;
                for (iter = 0; iter <= maxHop; iter++)
                {
                    for (int i = begin; i < end; i++)
                    {
                        KeyValuePair<int, int> xnode = searchNodes[i];
                        // no neighbor continue;
                        if (!Graph.ContainsKey(xnode.Key)) continue;
                        foreach (Tuple<int, int> transitem in Graph[xnode.Key])
                        {
                            if (history.Contains(transitem.Item2)) continue;
                            int step = xnode.Value + 1;
                            history.Add(transitem.Item2);
                            searchNodes.Add(new KeyValuePair<int, int>(transitem.Item2, step));
                        }
                    }
                    begin = end;
                    end = searchNodes.Count;
                }
                return history;
            }

            /// <summary>
            /// calculate the shortest path from entity e1 and entity e2.
            /// </summary>
            /// <param name="e1"></param>
            /// <param name="e2"></param>
            /// <param name="blackrid"></param>
            /// <param name="maxHop"></param>
            /// <returns></returns>
            public int ShortestPath(int e1, int e2, int blackrid, int maxHop)
            {
                List<KeyValuePair<int, int>> searchNodes = new List<KeyValuePair<int, int>>();
                HashSet<int> history = new HashSet<int>();

                searchNodes.Add(new KeyValuePair<int, int>(e1, 0));
                history.Add(e1);

                int begin = 0;
                int end = searchNodes.Count;
                int iter = 0;
                for (iter = 0; iter <= maxHop; iter++)
                {
                    for (int i = begin; i < end; i++)
                    {
                        KeyValuePair<int, int> xnode = searchNodes[i];

                        // no neighbor continue;
                        if (!Graph.ContainsKey(xnode.Key)) continue;

                        foreach (Tuple<int, int> transitem in Graph[xnode.Key])
                        {
                            // histroy connected node : continue;
                            if (history.Contains(transitem.Item2)) continue;

                            // black link : continue;
                            if (xnode.Key == e1 && transitem.Item1 == blackrid && transitem.Item2 == e2) continue;
                            if (xnode.Key == e2 && transitem.Item1 == DataPanel.ReverseRelation(blackrid) && transitem.Item2 == e1) continue;

                            int step = xnode.Value + 1;
                            history.Add(transitem.Item2);
                            searchNodes.Add(new KeyValuePair<int, int>(transitem.Item2, step));

                            if (transitem.Item2 == e2) return step;
                        }
                    }
                    begin = end;
                    end = searchNodes.Count;
                }

                return -1;
            }

        }
    }
}
