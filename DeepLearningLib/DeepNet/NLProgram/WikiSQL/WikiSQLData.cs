using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
using BigLearn.DeepNet;
namespace BigLearn.DeepNet.NL2SQL
{
	//{"phase": 1, "table_id": "1-10015132-16", 
	//"question": "how many schools or teams had jalen rose", 
	//"sql": {"sel": 5, "conds": [[0, 0, "Jalen Rose"]], "agg": 3}}
        public class Sql
            {
                public int sel { get; set; }
                public List<List<object>> conds { get; set; }
                public int agg { get; set; }
            }
        public class WikiSQLData
        {
            public int phase { get; set; }
            public string table_id { get; set; }
            public string question { get; set; }
            public Sql sql { get; set; }
        }

}
