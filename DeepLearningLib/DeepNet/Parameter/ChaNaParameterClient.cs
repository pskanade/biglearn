﻿//using CommLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BigLearn;

namespace BigLearn.DeepNet
{
    // public unsafe class ChaNaParameterClient
    // {
    //     AutoResetEvent pushEvent;
    //     ParameterServerBase.PsPushCallback pushCallback;
    //     GCHandle cbHandle;

    //     private static ChaNaParameterClient instance = null;
    //     public static ChaNaParameterClient Instance
    //     {
    //         get
    //         {
    //             if (instance == null)
    //             {
    //                 instance = new ChaNaParameterClient();
    //             }

    //             return instance;
    //         }
    //     }

    //     protected ChaNaParameterClient()
    //     {
    //         pushEvent = new AutoResetEvent(false);
    //         pushCallback = new ParameterServerBase.PsPushCallback(PushCallback);
    //         cbHandle = GCHandle.Alloc(pushCallback);

    //     }

    //     public void Push(int cnt, ulong* key, void** val, ulong* length, ParameterServerBase.PsPushCallback cb)
    //     {
    //         ParameterServerBase.ChaNaPSPush((ulong)cnt, key, val, length, cb, null);
    //     }

    //     public void PushSync(int cnt, ulong* key, void** val, ulong* length)
    //     {
    //         ulong left = (ulong)cnt;
    //         pushCnt = 0;
    //         int req = 0;
    //         while (left > 0)
    //         {
    //             ulong toSent = Math.Min(left, 1024);
    //             left -= toSent;
    //             ParameterServerBase.ChaNaPSPush(toSent, key, val, length, pushCallback, null);
    //             req++;
    //             key += toSent;
    //             val += toSent;
    //             length += toSent;
    //             pushEvent.WaitOne();
    //         }

    //         //while (pushCnt < req)
    //         //{
    //         //    Thread.Sleep(1);
    //         //}
    //     }

    //     public void PullSync(int cnt, ulong* key, void** val, ulong* length)
    //     {
    //         pullCnt = 0;
    //         int req = 0;
    //         ulong left = (ulong)cnt;
    //         while (left > 0)
    //         {
    //             ulong toSent = Math.Min(left, 128);
    //             left -= toSent;
    //             ParameterServerBase.ChaNaPSPull(toSent, key, val, length, null);
    //             req++;
    //             key += toSent;
    //             val += toSent;
    //             length += toSent;
    //         }

    //         while (pullCnt < req)
    //         {
    //             Thread.Sleep(1);
    //         }
    //     }

    //     private int pushCnt = 0;
    //     public void PushCallback(ulong cnt, ulong* keys, void** vals, ulong* lengths, void* args)
    //     {
    //         Interlocked.Increment(ref pushCnt);
    //         pushEvent.Set();
    //     }

    //     private int pullCnt = 0;
    //     public void OnPullAck(void* args)
    //     {
    //         Interlocked.Increment(ref pullCnt);
    //     }
    // }
}
