﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using CommLib;
using System.Runtime.InteropServices;
using System.Threading;
using BigLearn;

namespace BigLearn.DeepNet
{
    // public unsafe class ChaNaParameterServer : ParameterServerBase
    // {
    //     private int[] bufSizes;
    //     private float** BufPtr;
    //     private bool[] sparseFlags;
    //     public AutoResetEvent PullDone;


    //     protected ChaNaParameterServer()
    //     {
    //         PullDone = new AutoResetEvent(false);
    //     }

    //     public void Init(int[] bufSizes, float** bufs, bool[] sparseFlags)
    //     {
    //         this.bufSizes = bufSizes;
    //         this.BufPtr = bufs;
    //         this.sparseFlags = sparseFlags;
    //     }

    //     public override unsafe void OnControl(int cmd_id, void* data, ulong len)
    //     {

    //     }

    //     public override unsafe void OnPullAck(void* args)
    //     {
    //         ChaNaParameterClient.Instance.OnPullAck(args);
    //     }

    //     public override unsafe void OnPullProc(ulong* keys, ulong key_count, void** vals, ulong* valsizes, DeAlocator dealloc, void** args, bool* fixed_val_size)
    //     {
    //         *fixed_val_size = true;
    //         for (ulong i = 0; i < key_count; i++)
    //         {
    //             ulong pk = (keys[i] & 0xFFFFFFFF)*300;
    //             ulong bk = (keys[i] >> 32);
    //             if (sparseFlags[bk])
    //             {
    //                 vals[i] = &BufPtr[bk][pk];
    //                 valsizes[i] = 1200;
    //             }
    //             else
    //             {
    //                 vals[i] = BufPtr[bk];
    //                 fixed_val_size[i] = false;
    //                 valsizes[i] = (ulong)this.bufSizes[bk];
    //             }
    //         }
    //     }

    //     public override unsafe void OnPushProc(ulong key_count, ulong* keys, void** vals, ulong* valsizes)
    //     {
    //         Parallel.For(0, (int)key_count, i =>
    //         {
    //             ulong pk = (keys[i] & 0xFFFFFFFF) * 300;
    //             ulong bk = (keys[i] >> 32);
    //             if (sparseFlags[bk])
    //             {
    //                 for (int j = 0; j < 300; j++)
    //                 {
    //                     this.BufPtr[bk][(int)pk + j] += ((float**)vals)[i][j];
    //                 }
    //             }
    //             else
    //             {
    //                 int elements = (int)valsizes[i] >> 2;

    //                 Parallel.For(0, elements, j =>
    //                 {
    //                     if (((float**)vals)[i][j] != 0)
    //                     {
    //                         this.BufPtr[bk][j] += ((float**)vals)[i][j];
    //                     }
    //                 });
    //             }
    //         });
    //     }

    //     private static ChaNaParameterServer instance = null;
    //     public static ChaNaParameterServer Instance
    //     {
    //         get
    //         {
    //             if (instance == null)
    //             {
    //                 instance = new ChaNaParameterServer();
    //             }
    //             return instance;
    //         }
    //     }
    // }
}
