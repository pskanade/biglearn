﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web;

using BigLearn;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
    public class AppReinforceWalkBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.
            public static string InputDir { get { return Argument["INPUT-DIR"].Value; } }
            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

            public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] S_NET { get { return Argument["S-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] S_AF { get { return Argument["S-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }
            //public static float TNEG_R { get { return float.Parse(Argument["TNEG-R"].Value); } }

            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            public static int SCORE_TYPE { get { return int.Parse(Argument["SCORE-TYPE"].Value); } }
            public static int ROLL_NUM { get { return int.Parse(Argument["ROLL-NUM"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }
            public static float REWARD_MCTS_DISCOUNT { get { return float.Parse(Argument["REWARD-MCTS-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (ConfigPath + Argument["MODEL-PATH"].Value); } }
            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            //public static float T_MAX_CLIP { get { return float.Parse(Argument["T-MAX-CLIP"].Value); } }
            //public static float T_MIN_CLIP { get { return float.Parse(Argument["T-MIN-CLIP"].Value); } }

            //public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            //public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            //public static float GUID_RATE { get { return float.Parse(Argument["GUID-RATE"].Value); } }
            //public static float RAND_RATE { get { return float.Parse(Argument["RAND-RATE"].Value); } }


            //public static int TRAIN_DEBUG { get { return int.Parse(Argument["TRAIN-DEBUG"].Value); } }

            

            public static int TEST_MCTS_NUM { get { return int.Parse(Argument["TEST-MCTS-NUM"].Value); } }

            public static int MCTS_NUM { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

            //public static int SEED_VERSION { get { return int.Parse(Argument["SEED-VERSION"].Value); } }

            public static float MSE_LAMBDA { get { return float.Parse(Argument["MSE-LAMBDA"].Value); } }

            //public static float RAND_TERM { get { return float.Parse(Argument["RAND-TERM"].Value); } }

            public static float UCB1_C { get { return float.Parse(Argument["UCB1-C"].Value); } }
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }

            public static List<Tuple<int, float>> Reward_Feedback
            {
                get
                {
                    return Argument["R-FB"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static float Epsilon(int step)
            {
                for (int i = 0; i < Reward_Feedback.Count; i++)
                {
                    if (step < Reward_Feedback[i].Item1)
                    {
                        float lambda = (step - Reward_Feedback[i - 1].Item1) * 1.0f / (Reward_Feedback[i].Item1 - Reward_Feedback[i - 1].Item1);
                        return lambda * Reward_Feedback[i].Item2 + (1 - lambda) * Reward_Feedback[i - 1].Item2;
                    }
                }
                return Reward_Feedback.Last().Item2;
            }

            public static int EStrategy(int mcts_idx)
            {
                string[] idxs = Argument["MCTS-EXP"].Value.Split(',').ToArray();
                for (int i = 0; i < idxs.Length; i++)
                {
                    if (mcts_idx < int.Parse(idxs[i]))
                    {
                        return i;
                    }
                }
                return idxs.Length - 1;
            }

            public static float UPDATE_R_DISCOUNT { get { return float.Parse(Argument["UPDATE-R-DISCOUNT"].Value); } }

            public static float BASE_LAMBDA { get { return float.Parse(Argument["BASE-LAMBDA"].Value); } }
            public static float PROB_LAMBDA { get { return float.Parse(Argument["PROB-LAMBDA"].Value); } }
            public static float SCORE_LAMBDA { get { return float.Parse(Argument["SCORE-LAMBDA"].Value); } }

            public static float PUCB_C { get { return float.Parse(Argument["PUCB-C"].Value); } }
            public static float PUCB_B { get { return float.Parse(Argument["PUCB-B"].Value); } }
            public static float PUCB_M { get { return float.Parse(Argument["PUCB-M"].Value); } }

            public static int CRITIC_TYPE { get { return int.Parse(Argument["CRITIC-TYPE"].Value); } }

            public static int MAX_ACTION_NUM { get { return int.Parse(Argument["MAX-ACTION-NUM"].Value); } }

            public static string EVALUATION_TOOL { get { return Argument["EVALUATION-TOOL"].Value; } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                #region Input Data Argument.
                Argument.Add("INPUT-DIR", new ParameterArgument(string.Empty, "dir input."));
                #endregion.

                Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
                Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));
                //Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                //Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));
                Argument.Add("S-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("S-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));

                Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

                Argument.Add("SCORE-TYPE", new ParameterArgument("0", "0:probability; 1:pesudo reward;"));
                Argument.Add("ROLL-NUM", new ParameterArgument("1", "roll number"));

                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.99", "Reward discount"));
                Argument.Add("REWARD-MCTS-DISCOUNT", new ParameterArgument("0.8", "mcts reward discount."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument("model/", "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("./", "Output Score File."));
                
                Argument.Add("TEST-MCTS-NUM", new ParameterArgument("64", "Pre Monto Carlo Tree Search Number"));
                Argument.Add("MCTS-NUM", new ParameterArgument("64", "Monto Carlo Tree Search Number"));

                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));
                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("SCORE-LAMBDA", new ParameterArgument("1.0", "Score lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0", "Prob lambda in scoring function."));

                Argument.Add("GAMMA", new ParameterArgument("1.0", "gamma value."));


                Argument.Add("MCTS-EXP", new ParameterArgument("8,16,24,48,64", "Exploration strategy."));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1", "mcts reward discount."));
                Argument.Add("MAX-ACTION-NUM", new ParameterArgument("0","Default max action number."));

                Argument.Add("EVALUATION-TOOL", new ParameterArgument(string.Empty, "Evaluation path tool."));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_REINFORCE_WALK; } }

        public static Random DataRandom = new Random(DeepNet.BuilderParameters.RandomSeed);

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        /// <summary>
        /// episodic memory structure.
        /// </summary>
        public class EpisodicMemory
        {
            Dictionary<string, Tuple<List<Tuple<float, float>>, int>> Mem = new Dictionary<string, Tuple<List<Tuple<float, float>>, int>>();
            public bool IsGlobal = true;
            int Time { get; set; }
            public EpisodicMemory(bool isGlobal)
            {
                IsGlobal = isGlobal;
                Time = 0;
            }
            public void Clear()
            {
                Mem.Clear();
                Time = 0;
            }
            /// <summary>
            /// memory index.
            /// </summary>
            /// <param name="key"></param>
            /// <param name="actionCount"></param>
            /// <param name="topK"></param>
            /// <returns></returns>
            public List<Tuple<float, float>> Search(string key)
            {
                if (Mem.ContainsKey(key))
                {
                    return Mem[key].Item1;
                }
                else
                {
                    return null;
                }
            }

            public void Update(string key, int actid, int totalAct, float reward, float v = 1.0f)
            {
                float new_u = 0;
                float new_c = 0;

                // update memory.
                if (Mem.ContainsKey(key))
                {
                    float u = Mem[key].Item1[actid].Item1;
                    float c = Mem[key].Item1[actid].Item2;
                    new_u = u + reward; // c / (c + v) * u + v / (c + v) * reward;
                    new_c = c + v;
                }
                else
                {
                    Mem.Add(key, new Tuple<List<Tuple<float, float>>, int>(new List<Tuple<float, float>>(), Time));
                    for (int a = 0; a < totalAct; a++)
                    {
                        Mem[key].Item1.Add(new Tuple<float, float>(0, 0));
                    }
                    new_u = reward;
                    new_c = v;
                }
                Mem[key].Item1[actid] = new Tuple<float, float>(new_u, new_c);
            }

            public void UpdateTiming()
            {
                Time += 1;
            }
        }

        public class BanditAlg
        {
            /// <summary>
            /// Exp 0.
            /// </summary>
            /// <param name="prior"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int UniformRandomStrategy0(int dim, Random random)
            {
                return random.Next(dim);
            }

            /// <summary>
            /// Exp 1.
            /// </summary>
            /// <param name="actdim"></param>
            /// <param name="rand_term"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int CascadeRandomStrategy1(int actdim, float rand_term, Random random)
            {
                if (random.NextDouble() > rand_term)
                {
                    return actdim;
                }
                return random.Next(actdim);
            }

            /// <summary>
            /// Exp 2.
            /// </summary>
            /// <param name="prior"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int ThompasSampling(float[] prior, Random random)
            {
                int idx = Util.Sample(prior.ToArray(), random);
                return idx;
            }


            /// <summary>
            /// Exp 3. Standard UCB Bandit.
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int UCB1Bandit(List<Tuple<float, float>> arms, int dim, float c, Random random)
            {
                if (arms == null)
                {
                    return UniformRandomStrategy0(dim, random);
                }

                float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 0.1f);
                List<float> v = new List<float>();
                foreach (Tuple<float, float> arm in arms)
                {
                    v.Add(arm.Item1 / (arm.Item2 + 0.1f) + c * (float)Math.Sqrt(log_total / (arm.Item2 + 0.1f)) + (float)random.NextDouble() * 0.0001f);
                }
                int idx = Util.MaximumValue(v.ToArray());
                return idx;
            }

            public static int PUCB(List<Tuple<float, float>> arms, List<float> prior, float mb, float c, float m, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float sum = prior.Sum() + mb * prior.Count;

                    float t = (float)arms.Select(i => i.Item2).Sum();

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float si = arms[i].Item1;
                        float xi = (arms[i].Item2 == 0 ? 1 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        float ci = si == 0 ? 0 : c * (float)Math.Sqrt(Math.Log(t) / si);
                        float mi = (t == 0 ? 1 : (float)Math.Sqrt(Math.Log(t) / t)) * m * sum / (prior[i] + mb);
                        float s = xi + ci - mi;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }


            /// <summary>
            /// Exp 4. PUCT.
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int AlphaGoZeroBandit(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], BuilderParameters.PUCT_D) * (float)Math.Sqrt(log_total) / (arms[i].Item2 + 1.0f) +
                                                        (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }


            public static int AlphaGoZeroBanditV2(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.01f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float total = (float)arms.Select(i => i.Item2).Sum() + 1;

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], BuilderParameters.PUCT_D) * (float)Math.Sqrt(total) / (arms[i].Item2 + 1.0f) +
                                                       (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }

            public static int AlphaGoZeroBanditV3(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], BuilderParameters.PUCT_D) * (float)Math.Sqrt(log_total / (arms[i].Item2 + 1.0f)) +
                                                        (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }

            /// <summary>
            /// mb = 0.00001;
            /// c = exploration.
            /// m = 
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="mb"></param>
            /// <param name="c"></param>
            /// <param name="m"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int PUCB2(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum());
                    List<float> v = new List<float>();
                    foreach (Tuple<float, float> arm in arms)
                    {
                        v.Add(arm.Item1 / (arm.Item2 + 1.0f) + c * (float)Math.Sqrt(log_total / (arm.Item2)) + (float)random.NextDouble() * 0.0001f);
                    }
                    int idx = Util.MaximumValue(v.ToArray());
                }
                return maxI;
            }
        }


        /// <summary>
        /// current status Data.
        /// </summary>
        public class StatusData : BatchData
        {
            /// <summary>
            /// Raw Query.
            /// </summary>
            public QueryBatchData Query { get; set; }

            /// <summary>
            /// Node ID.
            /// </summary>
            public List<int> NodeID { get; set; }

            /// <summary>
            /// Embedding of the State.
            /// </summary>
            public HiddenBatchData StateEmbed { get; set; }  // Tuple<HiddenBatchData, HiddenBatchData>

            public int MaxBatchSize { get { return StateEmbed.MAX_BATCHSIZE; } }
            public int BatchSize { get { return StateEmbed.BatchSize; } }

            public int Step = 0;

            /// <summary>
            /// MatchCandidate and MatchCandidateProb.
            /// </summary>
            public List<Tuple<int, int>> MatchCandidate = null;
            public SeqVectorData MatchCandidateProb = null;
            public List<int> ResultIndex = null;
            public HiddenBatchData Score = null;


            List<int> mQueryIdx { get; set; }
            public int QueryIdx(int b)
            {
                if (Step == 0) return b;
                else return mQueryIdx[b];
            }
            public List<int> GetQueryIdxs(int b)
            {
                List<int> r = new List<int>();
                for (int i = 0; i < BatchSize; i++)
                {
                    if (QueryIdx(i) == b)
                        r.Add(i);
                }
                return r;
            }

            List<string> mStatusKey { get; set; }
            public string StatusKey(int b)
            {
                if (Step == 0) return string.Format("R:{1} ? N:{0} : ", Query.RawQuery[b].Item1, Query.RawQuery[b].Item2);
                else return mStatusKey[b];
            }

            /// <summary>
            /// LogProbability to this node.
            /// </summary>
            List<float> mLogProb { get; set; }
            public float GetLogProb(int b)
            {
                if (Step == 0) { return 0; }
                else { return mLogProb[b]; }
            }


            public List<int> PreSelActIndex { get; set; }
            public List<int> PreStatusIndex { get; set; }
            public int GetPreStatusIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else return PreStatusIndex[batchIdx];
            }
            public int GetActionDim(int b)
            {
                return GetActionEndIndex(b) - GetActionStartIndex(b);
            }
            public int GetActionStartIndex(int b)
            {
                return b == 0 ? 0 : MatchCandidateProb.SegmentIdx.MemPtr[b - 1];
            }
            public int GetActionEndIndex(int b)
            {
                return MatchCandidateProb.SegmentIdx.MemPtr[b];
            }

            //Tuple<HiddenBatchData, HiddenBatchData>
            public StatusData(QueryBatchData query, List<int> nodeIndex, HiddenBatchData stateEmbed, DeviceType device) 
            {
                Query = query;
                NodeID = nodeIndex;
                StateEmbed = stateEmbed;
                Step = 0;
            }

            public StatusData(QueryBatchData interData, List<int> nodeIndex, HiddenBatchData stateEmbed, 
                              List<int> queryIndex, List<string> statusKey, int step, List<float> logProb, List<int> preSelActIndex, List<int> preStatusIndex, DeviceType device)
            {
                Query = interData;
                NodeID = nodeIndex;
                StateEmbed = stateEmbed;
                Step = step;

                mQueryIdx = queryIndex;
                mStatusKey = statusKey;
                mLogProb = logProb;

                PreSelActIndex = preSelActIndex;
                PreStatusIndex = preStatusIndex;
            }
        }

        /// <summary>
        /// Sample Graph. 
        /// </summary>
        class StatusEmbedRunner : CompositeNetRunner
        {
            public new List<Tuple<int, int>> Input { get; set; }

            public new HiddenBatchData Output { get; set; }

            EmbedStructure InNodeEmbed { get; set; }
            EmbedStructure LinkEmbed { get; set; }

            public StatusEmbedRunner(List<Tuple<int, int>> input, int maxBatchSize, EmbedStructure inNodeEmbed, EmbedStructure linkEmbed, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;
                InNodeEmbed = inNodeEmbed;
                LinkEmbed = linkEmbed;
                // concate of node embedding and relation embedding.
                Output = new HiddenBatchData(maxBatchSize, inNodeEmbed.Dim + linkEmbed.Dim, DNNRunMode.Train, behavior.Device);
            }

            public override void Forward()
            {
                InNodeEmbed.Embedding.SyncToCPU();
                LinkEmbed.Embedding.SyncToCPU();

                //BatchLinks.Clear();
                int batchSize = 0;
                while (batchSize < Input.Count)
                {
                    int srcId = Input[batchSize].Item1;
                    int linkId = Input[batchSize].Item2;

                    int bidx = batchSize;

                    if (BuilderParameters.N_EmbedDim > 0)
                    {
                        FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim,
                                                InNodeEmbed.Embedding.MemPtr, srcId * InNodeEmbed.Dim, InNodeEmbed.Dim, 0, 1);
                    }
                    FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim + InNodeEmbed.Dim,
                                            LinkEmbed.Embedding.MemPtr, linkId * LinkEmbed.Dim, LinkEmbed.Dim, 0, 1);
                    batchSize += 1;
                }
                Output.BatchSize = batchSize;
                Output.Output.Data.SyncFromCPU(Output.BatchSize * Output.Dim);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                Output.Deriv.Data.SyncToCPU(Output.Dim * Output.BatchSize);

                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();
                LinkEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();

                for (int b = 0; b < Output.BatchSize; b++)
                {
                    int srcIdx = Input[b].Item1;
                    int linkIdx = Input[b].Item2;

                    if (BuilderParameters.N_EmbedDim > 0)
                    {
                        FastVector.Add_Vector(InNodeEmbed.EmbeddingOptimizer.Gradient.MemPtr, srcIdx * InNodeEmbed.Dim,
                                        Output.Deriv.Data.MemPtr, b * Output.Dim, InNodeEmbed.Dim, 1, 1);
                    }
                    FastVector.Add_Vector(LinkEmbed.EmbeddingOptimizer.Gradient.MemPtr, linkIdx * LinkEmbed.Dim,
                                    Output.Deriv.Data.MemPtr, b * Output.Dim + InNodeEmbed.Dim, LinkEmbed.Dim, 1, 1);
                }
                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
                LinkEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();

            }
        }

        class CandidateActionRunner : CompositeNetRunner
        {
            public new StatusData Input { get; set; }
            public GraphEnviroument Graph { get; set; }

            bool IsLastStep { get; set; }

            public new List<Tuple<int, int>> Output = new List<Tuple<int, int>>();
            public BiMatchBatchData Match;

            /// <summary>
            /// Input : State.
            /// node and rel embed.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="nodeEmbed"></param>
            /// <param name="relEmbed"></param>
            /// <param name="behavior"></param>
            public CandidateActionRunner(StatusData input, bool isLastStep, GraphEnviroument graph, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;
                IsLastStep = isLastStep;
                Graph = graph;
                Match = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize * Graph.MaxNeighborNum,
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * Graph.MaxNeighborNum
                }, behavior.Device);
            }

            //Random random = new Random();
            public override void Forward()
            {
                Output.Clear();
                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
                int cursor = 0;
                for (int b = 0; b < Input.BatchSize; b++)
                {
                    // current node.
                    int seedNode = Input.NodeID[b];

                    // query idx. 
                    int qid = Input.QueryIdx(b);

                    // raw node.
                    int rawN = Input.Query.RawQuery[qid].Item1;

                    // raw relation.
                    int rawR = Input.Query.RawQuery[qid].Item2;

                    // raw answer.
                    int answerN = Input.Query.RawTarget[qid];

                    int candidateNum = 0;

                    if (IsLastStep || !Graph.Graph.ContainsKey(seedNode))
                    {
                        Output.Add(new Tuple<int, int>(seedNode, Graph.StopIdx));
                        match.Add(new Tuple<int, int, float>(b, cursor, 1));
                        cursor += 1;
                        candidateNum += 1;
                    }
                    else
                    {
                        for (int nei = 0; nei < Graph.Graph[seedNode].Count; nei++)
                        {
                            int lid = Graph.Graph[seedNode][nei].Item1;
                            int tgtNode = Graph.Graph[seedNode][nei].Item2;
                            if (seedNode == rawN && lid == rawR && tgtNode == answerN) { continue; }

                            Output.Add(new Tuple<int, int>(tgtNode, lid));
                            match.Add(new Tuple<int, int, float>(b, cursor, 1));
                            cursor += 1;
                            candidateNum += 1;
                        }
                    }

                    if (candidateNum == 0)
                    {
                        throw new NotImplementedException("empty entity is found in the dataset");
                    }
                }
                Match.SetMatch(match);
            }
        }

        public class BasicPolicyRunner : CompositeNetRunner
        {
            public new StatusData Input { get; set; }

            /// <summary>
            /// MatchData.
            /// </summary>
            public BiMatchBatchData MatchPath = null;

            List<Tuple<int, int, float>> Match { get; set; }

            public List<int> QueryIndex { get; set; }

            /// <summary>
            /// new Node Index.
            /// </summary>
            public List<int> NodeIndex { get; set; }

            /// <summary>
            /// next step log probability.
            /// </summary>
            public List<float> LogProb { get; set; }

            /// <summary>
            /// pre sel action and pre status. 
            /// </summary>
            public List<int> PreSelActIndex { get; set; }
            public List<int> PreStatusIndex { get; set; }

            /// <summary>
            /// the key of the status.
            /// </summary>
            public List<string> StatusKey { get; set; }

            public List<int> ResultIndex { get; set; }

            public BasicPolicyRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;

                // new node index.
                NodeIndex = new List<int>();
                LogProb = new List<float>();
                PreSelActIndex = new List<int>();
                PreStatusIndex = new List<int>();
                StatusKey = new List<string>();
                ResultIndex = new List<int>();
                QueryIndex = new List<int>();
                Match = new List<Tuple<int, int, float>>();
            }

            public void Clear()
            {
                NodeIndex.Clear();
                LogProb.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                StatusKey.Clear();
                ResultIndex.Clear();
                QueryIndex.Clear();
                Match.Clear();
            }

            public void MatchDone()
            {
                MatchPath.SetMatch(Match);
            }

            public void AddResult(int b)
            {
                ResultIndex.Add(b);
            }

            public void AddNextStatus(int b, int selectIdx)
            {
                PreStatusIndex.Add(b);
                PreSelActIndex.Add(selectIdx);
                
                string statusQ = string.Format("{0}---R:{2}-->N:{1}", Input.StatusKey(b), Input.MatchCandidate[selectIdx].Item1, Input.MatchCandidate[selectIdx].Item2);
                NodeIndex.Add(Input.MatchCandidate[selectIdx].Item1);

                StatusKey.Add(statusQ);

                float prob = Input.GetLogProb(b) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                LogProb.Add(prob);

                QueryIndex.Add(Input.QueryIdx(b));
                Match.Add(new Tuple<int, int, float>(b, selectIdx, 1));
            }
        }

        /// <summary>
        /// take action.
        /// </summary>
        class ActionSamplingRunner : BasicPolicyRunner
        {
            /// <summary>
            /// match candidate, and match probability.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public ActionSamplingRunner(StatusData input, int maxNeighbor, RunnerBehavior behavior) : base(input, behavior)
            {
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                }, behavior.Device);
            }

            public override void Forward()
            {
                Clear();

                Input.MatchCandidateProb.Output.SyncToCPU();
                
                int currentStep = Input.Step;
                
                for (int i = 0; i < Input.MatchCandidateProb.Segment; i++)
                {
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];
                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                    
                    float[] policy_pi = null;

                    int actionDim = e - s;

                    policy_pi = new float[actionDim];
                    Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, policy_pi, 0, actionDim);
                    
                    //if (Input.Term != null)
                    //{
                    //    float t = Util.Logistic(Input.Term.Output.Data.MemPtr[i]);
                    //    if (t >= BuilderParameters.T_MAX_CLIP) { t = BuilderParameters.T_MAX_CLIP; }
                    //    if (t <= BuilderParameters.T_MIN_CLIP) { t = BuilderParameters.T_MIN_CLIP; }
                    //    policy_pi[actionDim] = t;

                    //    for (int a = 0; a < actionDim; a++) { policy_pi[a] = policy_pi[a] * (1 - t); }
                    //}
                    //else
                    //{
                    //    policy_pi = new float[actionDim];
                    //    Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, policy_pi, 0, actionDim);
                    //}

                    //int idx = 0;
                    //float r = (float)DataRandom.NextDouble();
                    // draw sample actions.
                    //if (r > BuilderParameters.RAND_RATE) {
                    int idx = Util.Sample(policy_pi, DataRandom);
                    //}
                    // draw random actions.
                    //else { idx = SampleRandom.Next(0, policy_pi.Length); }

                    int selectIdx = s + idx;

                    if (idx == actionDim - 1)
                    {
                        AddResult(i); // .ResultIndex.Add(i); // .Query.Results.Add(new Tuple<int, int>(Input.Step, i));
                    }
                    else
                    {
                        AddNextStatus(i, selectIdx);
                    }
                }
                MatchDone();
            }
        }

        class BeamSearchActionRunner : BasicPolicyRunner
        {
            int BeamSize = 1;

            /// <summary>
            /// group aware beam search.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public BeamSearchActionRunner(StatusData input, int beamSize, int maxNeighbor, RunnerBehavior behavior) : base(input, behavior)
            {
                BeamSize = beamSize;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.Query.MaxBatchSize * BeamSize
                }, behavior.Device);
            }

            public override void Forward()
            {
                if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }

                Input.MatchCandidateProb.Output.SyncToCPU();

                Clear();

                // for each beam.
                for (int i = 0; i < Input.Query.BatchSize; i++)
                {
                    List<int> idxs = Input.GetQueryIdxs(i); // .Query.GetBatchIdxs(i, Input.Step);
                    MinMaxHeap<Tuple<int, int, bool>> topKheap = new MinMaxHeap<Tuple<int, int, bool>>(BeamSize, 1);

                    foreach (int b in idxs)
                    {
                        int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[b];
                        int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

                        //if (Input.Step > 0)
                        //float nonLogTerm = Input.LogPTerm(b, false);
                        //{
                            //nonLogTerm =
                        //    Input.Query.Results.Add(new Tuple<int, int>(Input.Step, b));
                        //}

                        for (int t = s; t < e; t++)
                        {
                            float prob = Input.GetLogProb(b) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t]);
                            topKheap.push_pair(new Tuple<int, int, bool>(b, t, t == e - 1), prob);
                        }
                    }

                    while (!topKheap.IsEmpty)
                    {
                        KeyValuePair<Tuple<int, int, bool>, float> p = topKheap.PopTop();

                        if(p.Key.Item3)
                        {
                            AddResult(p.Key.Item1);
                        }
                        else
                        {
                            AddNextStatus(p.Key.Item1, p.Key.Item2);
                        }
                    }
                }
                MatchDone();
            }
        }

        class MCTSActionSamplingRunner : BasicPolicyRunner
        {
            int lineIdx = 0;
            EpisodicMemory Memory { get; set; }
            //List<EpisodicMemory> Memory { get; set; }
            //Random random = new Random(21);
            public int MCTSIdx { get; set; }
            public override void Init()
            {
                lineIdx = 0;
            }

            // List<EpisodicMemory> memory,
            /// <summary>
            /// match candidate, and match probability.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public MCTSActionSamplingRunner(StatusData input, int mctsIdx, int maxNeighbor, EpisodicMemory memory, RunnerBehavior behavior) : base(input, behavior)
            {
                // Memory = memory;
                MCTSIdx = mctsIdx;
                Memory = memory;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                }, behavior.Device);
            }

            public override void Forward()
            {
                Clear();
                Input.MatchCandidateProb.Output.SyncToCPU();

                int currentStep = Input.Step;
                for (int i = 0; i < Input.MatchCandidateProb.Segment; i++)
                {
                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

                    int selectIdx = s;

                    List<Tuple<float, float>> m = Memory.Search(Input.StatusKey(i));
                    float[] prior_r = null;

                    int actionDim = e - s;
                    //if (Input.Term != null)
                    {
                        prior_r = new float[actionDim];
                        Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);
                    }
                    //else
                    //{
                    //    prior_r = new float[actionDim];
                    //    Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);
                    //}

                    //if (m != null && actionDim != m.Count) { throw new Exception(string.Format("the dimension doesn't match {0}, {1}", actionDim, m.Count)); }
                    //int idx = BanditAlg.UCBBandit_Sample(m, prior_r, 2, random);
                    //int idx = BanditAlg.AlphaGoZeroBandit(m, prior_r.ToList(), 2, random);

                    int idx = actionDim - 1 ;
                    {
                        int strategy = BuilderParameters.EStrategy(MCTSIdx);
                        //Console.WriteLine("MCTS Index {0}, strategy {1}", MCTSIdx, strategy);
                        switch (strategy)
                        {
                            case 0: idx = BanditAlg.UniformRandomStrategy0(prior_r.Length, DataRandom); break;
                            //case 1: idx = BanditAlg.CascadeRandomStrategy1(actionDim, BuilderParameters.RAND_TERM, DataRandom); break;
                            case 1: idx = BanditAlg.ThompasSampling(prior_r, DataRandom); break;
                            case 2: idx = BanditAlg.UCB1Bandit(m, prior_r.Length, BuilderParameters.UCB1_C, DataRandom); break;
                            case 3: idx = BanditAlg.AlphaGoZeroBandit(m, prior_r.ToList(), BuilderParameters.PUCT_C, DataRandom); break;
                            case 4: idx = BanditAlg.AlphaGoZeroBanditV2(m, prior_r.ToList(), BuilderParameters.PUCT_C, DataRandom); break;
                            case 5: idx = BanditAlg.PUCB(m, prior_r.ToList(), BuilderParameters.PUCB_B, BuilderParameters.PUCB_C, BuilderParameters.PUCB_M, DataRandom); break;
                        }
                    }

                    if (idx == actionDim - 1)
                    {
                        AddResult(i);
                    }
                    else
                    {
                        AddNextStatus(i, idx + selectIdx);
                    }
                }
                MatchDone();
                lineIdx += Input.MatchCandidateProb.Segment;
            }
        }

        ///// <summary>
        ///// it is a little difficult.
        ///// </summary>
        //class RewardRunner : ObjectiveRunner
        //{
        //    //Random random = new Random();
        //    new List<List<StatusData>> Input { get; set; }
        //    //EpisodicMemory Memory { get; set; }

        //    //Dictionary<int, float> Success = new Dictionary<int, float>();
        //    //Dictionary<int, float> StepSuccess = new Dictionary<int, float>();

        //    float[] MCTSReward = null;
        //    float[] MCTSNumber = null;
        //    int Epoch = 0;
        //    public override void Init()
        //    {
        //        Array.Clear(MCTSReward, 0, MCTSReward.Length);
        //        Array.Clear(MCTSNumber, 0, MCTSNumber.Length);
        //    }

        //    public override void Complete()
        //    {
        //        Epoch += 1;
        //        foreach (KeyValuePair<int, float> i in StepSuccess)
        //        {
        //            Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
        //        }
        //        int sc = Success.Where(i => i.Value > 0).Count();
        //        int t = Success.Count;
        //        Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
        //    }

        //    public RewardRunner(List<List<StatusData>> mctsStatusPath, int mctsNumber, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        //    {
        //        Input = mctsStatusPath;
        //        MCTSReward = new float[mctsNumber];
        //        MCTSNumber = new float[mctsNumber];
        //        //Memory = memory;
        //    }

        //    public override void Forward()
        //    {
        //        float trueReward = 0;
        //        int sampleNum = 0;
        //        float pos_mseloss = 0;
        //        float neg_mseloss = 0;
        //        int pos_smp = 0;
        //        int neg_smp = 0;
        //        ObjectiveDict.Clear();

        //        Dictionary<int, float> rewards_history = new Dictionary<int, float>();
        //        Dictionary<int, float> baseline_history = new Dictionary<int, float>();

        //        for (int m = Input.Count - 1; m >= 0; m--)
        //        {
        //            for (int step = Input[m].Count-1; step >= 0; step--)
        //            {
        //                StatusData st = Input[m][step];
        //                //Array.Clear(st.Score.Deriv.Data.MemPtr, 0, st.Score.Deriv.Data.EffectiveSize);
        //                //Array.Clear()
        //                for (int b = 0; b < st.ResultIndex.Count; b++)
        //                {
        //                    int bidx = st.ResultIndex[b];
        //                    int origialB = st.QueryIdx(bidx);
        //                    int targetId = st.Query.RawTarget[origialB]; //.StatusPath[t].NodeID[b];
        //                    int predId = st.NodeID[bidx];

        //                    float reward = BuilderParameters.NEG_R;
        //                    if (predId == targetId) { reward = BuilderParameters.POS_R; }

        //                    int stopActIdx = st.GetActionEndIndex(bidx) - 1;
                            
        //                    if (!st.Query.RawTruths[origialB].Contains(predId))
        //                    {
        //                        // mse error for critic.
        //                        st.Score.Deriv.Data.MemPtr[bidx] = BuilderParameters.MSE_LAMBDA * (reward - st.Score.Output.Data.MemPtr[bidx]);

        //                        rewards_history[origialB] = reward + 
        //                                                (rewards_history.ContainsKey(origialB) ? rewards_history[origialB] * BuilderParameters.REWARD_MCTS_DISCOUNT : 0);
        //                        baseline_history[origialB] = st.Score.Output.Data.MemPtr[bidx] +
        //                                                (baseline_history.ContainsKey(origialB) ? baseline_history[origialB] * BuilderParameters.REWARD_MCTS_DISCOUNT : 0);

        //                        // policy gradient for actor.
        //                        st.MatchCandidateProb.Deriv.MemPtr[stopActIdx] = rewards_history[origialB] - baseline_history[origialB];

        //                        for (int pt = step - 1; pt >= 0; pt--)
        //                        {
        //                            StatusData pst = Input[m][pt]; 
        //                            int pb = st.GetPreStatusIndex(b);
        //                            int sidx = st.PreSelActIndex[b];

        //                            float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt);

        //                            if (BuilderParameters.CRITIC_TYPE == 0)
        //                            {
        //                                if (pst.Term != null)
        //                                {
        //                                    pst.Term.Deriv.Data.MemPtr[pb] = discount * update_v * (-pst.Term.Output.Data.MemPtr[pb]);
        //                                }
        //                                pst.MatchCandidateProb.Deriv.MemPtr[sidx] = discount * update_v;
        //                            }
        //                            else if (BuilderParameters.CRITIC_TYPE == 1)
        //                            {
        //                                float p_estimate_v = Util.Logistic(pst.Score.Output.Data.MemPtr[pb]);
        //                                if (pst.Term != null)
        //                                {
        //                                    pst.Term.Deriv.Data.MemPtr[pb] = (discount * update_v - p_estimate_v) * (-pst.Term.Output.Data.MemPtr[pb]);
        //                                }
        //                                pst.MatchCandidateProb.Deriv.MemPtr[sidx] = (discount * update_v - p_estimate_v);
        //                                pst.Score.Deriv.Data.MemPtr[pb] = BuilderParameters.MSE_LAMBDA * (discount * update_v - p_estimate_v);
        //                            }
        //                            st = pst;
        //                            b = pb;
        //                        }


        //                        rewards_history[origialB] = 
        //                    }

        //                    MCTSReward[m] += reward;
        //                    MCTSNumber[m] += 1;
        //                }
        //            }
        //        }

        //        // mcts sampling.
        //        for (int m = 0; m < Input.Count; m++)
        //        {
        //            for (int i = 0; i < Input[m].Results.Count; i++)
        //            {
        //                int t = Input[m].Results[i].Item1;
        //                int b = Input[m].Results[i].Item2;
        //                int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);
        //                int predId = Input[m].StatusPath[t].NodeID[b];

        //                Dictionary<int, float> targetId = Input[m].RawTarget[origialB];
        //                int rawIdx = Input[m].RawIndex[origialB];

        //                //float estimate_v = Math.Max(0, Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);
        //                //if (Input[m].BlackTargets[origialB].Contains(predId)) { estimate_v = 0; }

        //                string mkey = string.Format("Term {0}", t);
        //                if (!ObjectiveDict.ContainsKey(mkey)) { ObjectiveDict[mkey] = 0; }
        //                ObjectiveDict[mkey] += 1;

        //                float update_v = 0;

        //                float true_v = 0;
        //                if (targetId.ContainsKey(predId) && targetId[predId] > 0)
        //                {
        //                    true_v = BuilderParameters.POS_R;
        //                    update_v = true_v;
        //                    //if (BuilderParameters.NORM_REWARD > 0) update_v = true_v * 1.0f / (ReNorm[origialB][0] + 0.0000001f);
        //                    //posReward += 1;
        //                }
        //                else if (targetId.ContainsKey(predId) && targetId[predId] < 0)
        //                {
        //                    true_v = BuilderParameters.TNEG_R;
        //                    update_v = true_v;
        //                    //if (BuilderParameters.NORM_REWARD > 0) update_v = true_v * 1.0f / (ReNorm[origialB][0] + 0.0000001f);
        //                    //posReward += 1;
        //                }
        //                else if (Input[m].BlackTargets[origialB].Contains(predId))
        //                {
        //                    true_v = BuilderParameters.BLACK_R;
        //                    update_v = true_v;
        //                    //if (BuilderParameters.NORM_REWARD > 0) update_v = true_v * 1.0f / (ReNorm[origialB][1] + 0.0000001f);
        //                }
        //                else
        //                {
        //                    true_v = BuilderParameters.NEG_R;
        //                    update_v = true_v;
        //                    //if (BuilderParameters.NORM_REWARD > 0) update_v = true_v * 1.0f / (ReNorm[origialB][2] + 0.0000001f);
        //                    //negReward += 1;
        //                }

        //                if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
        //                Success[rawIdx] += true_v;

        //                if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = 0;
        //                StepSuccess[t] += true_v;

        //                float estimate_v = Util.Logistic(Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);
        //                //Math.Max(0, );
        //                //if (estimate_v > 1) estimate_v = 1;

        //                //if (targetId == predId || !Input[m].BlackTargets[origialB].Contains(predId))
        //                {
        //                    trueReward += true_v;
        //                    sampleNum += 1;


        //                    if (true_v > 0.5)
        //                    {
        //                        pos_mseloss += Math.Abs(true_v - estimate_v);
        //                        pos_smp += 1;
        //                    }
        //                    else
        //                    {
        //                        neg_mseloss += Math.Abs(true_v - estimate_v);
        //                        neg_smp += 1;
        //                    }

        //                    // Policy gradient for path finding.
        //                    StatusData st = Input[m].StatusPath[t];

        //                    float adv = update_v;
        //                    if (BuilderParameters.CRITIC_TYPE == 1)
        //                    {
        //                        adv = update_v - estimate_v;
        //                    }

        //                    //if (t != BuilderParameters.MAX_HOP)
        //                    st.Term.Deriv.Data.MemPtr[b] = adv * (1 - st.Term.Output.Data.MemPtr[b]);
        //                    // MSE error for reward estimation.
        //                    st.Score.Deriv.Data.MemPtr[b] = BuilderParameters.MSE_LAMBDA * (update_v - estimate_v);

        //                    for (int pt = t - 1; pt >= 0; pt--)
        //                    {
        //                        StatusData pst = Input[m].StatusPath[pt];
        //                        int pb = st.GetPreStatusIndex(b);
        //                        int sidx = st.PreSelActIndex[b];

        //                        float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt);

        //                        if (BuilderParameters.CRITIC_TYPE == 0)
        //                        {
        //                            if (pst.Term != null)
        //                            {
        //                                pst.Term.Deriv.Data.MemPtr[pb] = discount * update_v * (-pst.Term.Output.Data.MemPtr[pb]);
        //                            }
        //                            pst.MatchCandidateProb.Deriv.MemPtr[sidx] = discount * update_v;
        //                        }
        //                        else if (BuilderParameters.CRITIC_TYPE == 1)
        //                        {
        //                            float p_estimate_v = Util.Logistic(pst.Score.Output.Data.MemPtr[pb]);
        //                            if (pst.Term != null)
        //                            {
        //                                pst.Term.Deriv.Data.MemPtr[pb] = (discount * update_v - p_estimate_v) * (-pst.Term.Output.Data.MemPtr[pb]);
        //                            }
        //                            pst.MatchCandidateProb.Deriv.MemPtr[sidx] = (discount * update_v - p_estimate_v);
        //                            pst.Score.Deriv.Data.MemPtr[pb] = BuilderParameters.MSE_LAMBDA * (discount * update_v - p_estimate_v);
        //                        }
        //                        st = pst;
        //                        b = pb;
        //                    }
        //                }
        //            }
        //        }

        //        //ObjectiveDict["AVG-TRUE-PATH"] = trueReward / (sampleNum + float.Epsilon);
        //        ObjectiveDict["TERM-POS-MSE-LOSS"] = pos_mseloss / (pos_smp + 1);
        //        ObjectiveDict["TERM-NEG-MSE-LOSS"] = neg_mseloss / (neg_smp + 1);

        //        ObjectiveDict["TERM-POS-NUM"] = pos_smp;
        //        ObjectiveDict["TERM-NEG-NUM"] = neg_smp;


        //        ObjectiveScore = trueReward / (sampleNum + float.Epsilon);

        //        //
        //        //average ground truth results.
        //        for (int p = 0; p < Input.Count; p++)
        //        {
        //            for (int i = Input[p].StatusPath.Count - 1; i >= 0; i--)
        //            {
        //                StatusData st = Input[p].StatusPath[i];
        //                if (st.Term != null) st.Term.Deriv.Data.SyncFromCPU();
        //                if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
        //                if (st.Score != null) st.Score.Deriv.SyncFromCPU();
        //            }
        //            Input[p].Results.Clear();
        //        }

        //    }
        //}

        class PolicyGradientRunner : ObjectiveRunner
        {
            //Random random = new Random();
            new List<List<StatusData>> Input { get; set; }
            //EpisodicMemory Memory { get; set; }

            //Dictionary<int, float> Success = new Dictionary<int, float>();
            //Dictionary<int, float> StepSuccess = new Dictionary<int, float>();

            float[] MCTSReward = null;
            float[] MCTSNumber = null;
            int Epoch = 0;
            public override void Init()
            {
                Array.Clear(MCTSReward, 0, MCTSReward.Length);
                Array.Clear(MCTSNumber, 0, MCTSNumber.Length);
            }

            public override void Complete()
            {
                Epoch += 1;
                for (int i = 0; i < MCTSNumber.Length; i++)
                {
                    Logger.WriteLog("MCTS Index : {0}, Number : {1},  Reward : {2}", i, MCTSNumber[i], MCTSReward[i]); 
                }
            }

            public PolicyGradientRunner(List<List<StatusData>> mctsStatusPath, int mctsNumber, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = mctsStatusPath;
                MCTSReward = new float[mctsNumber];
                MCTSNumber = new float[mctsNumber];
                //Memory = memory;
            }

            public override void Forward()
            {
                float totalReward = 0;
                int totalSampleNum = 0;
                
                ObjectiveDict.Clear();

                Dictionary<int, float> rewards_history = new Dictionary<int, float>();
                Dictionary<int, float> baseline_history = new Dictionary<int, float>();

                for (int m = Input.Count - 1; m >= 0; m--)
                {
                    for (int step = Input[m].Count - 1; step >= 0; step--)
                    {
                        StatusData mainST = Input[m][step];
                        //Array.Clear(st.Score.Deriv.Data.MemPtr, 0, st.Score.Deriv.Data.EffectiveSize);
                        //Array.Clear()
                        for (int b = 0; b < mainST.ResultIndex.Count; b++)
                        {
                            int bidx = mainST.ResultIndex[b];
                            int origialB = mainST.QueryIdx(bidx);
                            int targetId = mainST.Query.RawTarget[origialB]; //.StatusPath[t].NodeID[b];
                            int predId = mainST.NodeID[bidx];

                            float reward = BuilderParameters.NEG_R;
                            if (predId == targetId) {
                                reward = BuilderParameters.POS_R;
                            }

                            float deriv_wei = 1;

                            //if (!mainST.Query.RawTruths[origialB].Contains(predId) || predId == targetId)
                            {
                                deriv_wei = 1;
                                totalReward += reward;
                                totalSampleNum += 1;
                                MCTSReward[m] += reward;
                                MCTSNumber[m] += 1;
                            }
                            // mse error for critic.
                            mainST.Score.Deriv.Data.MemPtr[bidx] = deriv_wei * BuilderParameters.MSE_LAMBDA * (reward - Util.Logistic(mainST.Score.Output.Data.MemPtr[bidx]));

                            rewards_history[origialB] = reward +
                                                    (rewards_history.ContainsKey(origialB) ? rewards_history[origialB] * BuilderParameters.REWARD_MCTS_DISCOUNT : 0);
                            baseline_history[origialB] = 1 +
                                                    (baseline_history.ContainsKey(origialB) ? baseline_history[origialB] * BuilderParameters.REWARD_MCTS_DISCOUNT : 0);

                            int stopActIdx = mainST.GetActionEndIndex(bidx) - 1;

                            float reward_step = rewards_history[origialB] * 1.0f / baseline_history[origialB];

                            if(reward_step > 0) {
                                reward_step = reward_step + 0.0000001f; }
                            // policy gradient for actor.
                            mainST.MatchCandidateProb.Deriv.MemPtr[stopActIdx] = deriv_wei * reward_step;

                            StatusData st = mainST;
                            for (int pt = step - 1; pt >= 0; pt--)
                            {
                                StatusData pst = Input[m][pt];
                                int pb = st.GetPreStatusIndex(bidx);
                                int sidx = st.PreSelActIndex[bidx];

                                //int sidx = st.GetPreStatusIndex(b);

                                float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, step - pt);
                                pst.MatchCandidateProb.Deriv.MemPtr[sidx] = deriv_wei * discount * reward_step;

                                st = pst;
                                bidx = pb;
                            }
                        }
                    }
                }

                //average ground truth results.
                for (int m = Input.Count - 1; m >= 0; m--)
                {
                    for (int step = Input[m].Count - 1; step >= 0; step--)
                    {
                        StatusData st = Input[m][step];
                        st.MatchCandidateProb.Deriv.SyncFromCPU();
                        st.Score.Deriv.SyncFromCPU();
                    }
                }
                ObjectiveScore = totalReward / (totalSampleNum + float.Epsilon);
            }
        }

        // update reward feedback 
        class RewardFeedbackRunner : StructRunner
        {
            new List<StatusData> Input { get; set; }
            EpisodicMemory Memory { get; set; }

            float PosMean = 0;
            float NegMean = 0;
            int PosNum = 0;
            int NegNum = 0;

            int Epoch = 0;
            public override void Init()
            {
                Epoch = 0;
                PosMean = 0;
                PosNum = 0;

                NegMean = 0;
                NegNum = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
                Logger.WriteLog("Pos Num {0}, Pos Mean {1}", PosNum, PosMean / (PosNum + 0.000001f));
                Logger.WriteLog("Neg Num {0}, Neg Mean {1}", NegNum, NegMean / (NegNum + 0.000001f));
            }

            public RewardFeedbackRunner(List<StatusData> input, EpisodicMemory memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Memory = memory;
            }

            public override void Forward()
            {
                for (int step = Input.Count - 1; step >= 0; step--)
                {
                    Input[step].Score.Output.Data.SyncToCPU();

                    for (int i = 0; i < Input[step].ResultIndex.Count; i++)
                    {
                        int b = Input[step].ResultIndex[i];  // Input[step].QueryIdx(Input[step].ResultIndex[i]); // .Results[i].Item2;
                        int origialB = Input[step].QueryIdx(b); // Input.StatusPath[t].GetOriginalStatsIndex(b);
                        int predId = Input[step].NodeID[b];
                        int targetId = Input[step].Query.RawTarget[origialB];

                        float feedback_v = 0;

                        float estimate_v = Util.Logistic(Input[step].Score.Output.Data.MemPtr[b]); // Util.Logistic(Input.StatusPath[t].Score.Output.Data.MemPtr[b]);// Math.Max(0, );
                        float true_v = BuilderParameters.NEG_R;
                        if (Input[step].Query.RawTruths[origialB].Contains(predId) && predId != targetId)
                        {
                            true_v = BuilderParameters.BLACK_R;
                            estimate_v = BuilderParameters.BLACK_R;
                        }
                        else if (targetId == predId)
                        {
                            true_v = BuilderParameters.POS_R;
                            PosMean += estimate_v;
                            PosNum += 1;
                        }
                        else
                        {
                            true_v = BuilderParameters.NEG_R;
                            NegMean += estimate_v;
                            NegNum += 1;
                        }
                        // use true feed back or pesudo reward.
                        if (Behavior.RunMode == DNNRunMode.Train || DataRandom.NextDouble() < BuilderParameters.Epsilon(Epoch)) { feedback_v = true_v; }
                        else { feedback_v = estimate_v; }

                        feedback_v = feedback_v * BuilderParameters.UPDATE_R_DISCOUNT;
                        float v = BuilderParameters.UPDATE_R_DISCOUNT;

                        StatusData st = Input[step];
                        string key = st.StatusKey(b);

                        Memory.Update(key, st.GetActionDim(b) - 1, st.GetActionDim(b), feedback_v, v);
                        for (int pt = step - 1; pt >= 0; pt--)
                        {
                            StatusData pst = Input[pt];
                            int pb = st.GetPreStatusIndex(b);
                            string pkey = pst.StatusKey(pb);

                            int sidx = st.PreSelActIndex[b] - pst.GetActionStartIndex(pb);
                            float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, step - pt);
                            Memory.Update(pkey, sidx, pst.GetActionDim(pb), feedback_v * discount, v * discount);
                            st = pst;
                            b = pb;
                        }
                    }
                }
                Memory.UpdateTiming();
            }
        }

        /// <summary>
        /// REINFORCE-WALK prediction Runner.
        /// </summary>
        class MAPPredictionRunner : StructRunner
        {
            //float MAP = 0;

            //int SmpIdx = 0;
            QueryBatchData Query { get; set; }
            List<List<StatusData>> Result { get; set; }
            int Iteration = 0;
            //Dictionary<int, float> Success = new Dictionary<int, float>();
            //Dictionary<int, float> StepSuccess = new Dictionary<int, float>();

            string TmpInFile = @"c:\cygwin64\bin\TMP.in";
            string TmpOutFile = @"c:\cygwin64\bin\TMP.out";
            StreamWriter InWriter = null;
            public MAPPredictionRunner(QueryBatchData query, List<List<StatusData>> result, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Query = query;
                Result = result;
                Iteration = 0;
            }

            public override void Init()
            {
                InWriter = new StreamWriter(TmpInFile);
            }


            public override void Complete()
            {
                InWriter.Close();

                File.Copy(BuilderParameters.InputDir + "sort_test.pairs", @"c:\cygwin64\bin\sort_test.pairs", true);

                using (Process callProcess = new Process()
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = @"c:\cygwin64\bin\mintty.exe",
                        Arguments = "/bin/bash -l -e \"/cygdrive/c/cygwin64/bin/eval.sh\"",
                        //\"{0}\" \"{1}\" \"{2}\" \"{3}\"", BuilderParameters.EVALUATION_TOOL, TmpInFile, BuilderParameters.InputDir+ "sort_test.pairs", TmpOutFile),
                        CreateNoWindow = true,
                        UseShellExecute = true,
                        RedirectStandardOutput = false,
                    }
                })
                {
                    callProcess.Start();
                    callProcess.WaitForExit();
                }


                string line = File.ReadAllText(TmpOutFile);

                Logger.WriteLog("MAP {0}", line);

                Iteration += 1;
            }
            public override void Forward()
            {
                List<Tuple<int, float>>[] scoreDict = DataUtil.AggregrateScore(Result, Query, BuilderParameters.PROB_LAMBDA, BuilderParameters.SCORE_LAMBDA);

                for (int originB = 0; originB < Query.BatchSize; originB++)
                {
                    int rawSource = Query.RawSource[originB];
                    foreach (Tuple<int, float> item in scoreDict[originB])
                    {
                        InWriter.WriteLine("{0}\t{1}\t{2}", DataPanel.EntityLookup[rawSource], DataPanel.EntityLookup[item.Item1], item.Item2);
                    }
                }
            }
        }

        //class MAPPredictionV2Runner : StructRunner
        //{
        //    float MAP = 0;
        //    int Iteration = 0;
        //    int SmpIdx = 0;
        //    int nonHit = 0;
        //    int effectNonHit = 0;
        //    QueryBatchData Query { get; set; }

        //    Dictionary<int, float> Success = new Dictionary<int, float>();
        //    Dictionary<int, float> StepSuccess = new Dictionary<int, float>();

        //    public MAPPredictionV2Runner(QueryBatchData query, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        //    {
        //        Query = query;
        //        Iteration = 0;
        //    }

        //    public override void Init()
        //    {
        //        Success.Clear();
        //        StepSuccess.Clear();

        //        SmpIdx = 0;
        //        MAP = 0;

        //        nonHit = 0;
        //        effectNonHit = 0;
        //    }

        //    public void Report()
        //    {
        //        Logger.WriteLog("Sample Idx {0}", SmpIdx);
        //        Logger.WriteLog(string.Format("MAP {0}", MAP / SmpIdx));
        //        Logger.WriteLog("nonhit {0}", nonHit);
        //        Logger.WriteLog("effectNonhit {0}", effectNonHit);
        //    }

        //    public override void Complete()
        //    {
        //        Logger.WriteLog("Final Report!");
        //        {
        //            Report();
        //        }

        //        foreach (KeyValuePair<int, float> i in StepSuccess)
        //        {
        //            Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
        //        }
        //        int sc = Success.Where(i => i.Value > 0).Count();
        //        int t = Success.Count;
        //        Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);

        //        Iteration += 1;
        //    }
        //    public override void Forward()
        //    {
        //        Dictionary<int, float>[] scoreDict = new Dictionary<int, float>[Query.BatchSize];
        //        for (int i = 0; i < Query.BatchSize; i++)
        //        {
        //            scoreDict[i] = new Dictionary<int, float>();
        //        }

        //        // add last step nodes.
        //        //if (Query.StatusPath.Count == BuilderParameters.MAX_HOP + 1)
        //        //{
        //        //    for (int b = 0; b < Query.StatusPath[BuilderParameters.MAX_HOP].BatchSize; b++)
        //        //    {
        //        //        Query.Results.Add(new Tuple<int, int>(BuilderParameters.MAX_HOP, b));
        //        //    }
        //        //}

        //        for (int t = Query.StatusPath.Count - 1; t >= 0; t--)
        //        {
        //            if (Query.StatusPath[t].Term != null) { Query.StatusPath[t].Term.Output.Data.SyncToCPU(); }
        //            if (Query.StatusPath[t].Score != null) { Query.StatusPath[t].Score.Output.Data.SyncToCPU(); }
        //        }

        //        // add results.
        //        for (int i = 0; i < Query.Results.Count; i++)
        //        {
        //            int t = Query.Results[i].Item1;
        //            int b = Query.Results[i].Item2;
        //            int origialB = Query.StatusPath[t].GetOriginalStatsIndex(b);
        //            int predId = Query.StatusPath[t].NodeID[b];

        //            Dictionary<int, float> targetId = Query.RawTarget[origialB];

        //            float v = BuilderParameters.PROB_LAMBDA * (Query.StatusPath[t].GetLogProb(b) + (float)Query.StatusPath[t].LogPTerm(b, true));

        //            float score = Util.Logistic(Query.StatusPath[t].Score.Output.Data.MemPtr[b]);
        //            //if (score >= 1) score = 1;
        //            //else if (score <= 0) score = 0;

        //            float s = (float)Math.Log(score + BuilderParameters.BASE_LAMBDA);
        //            v = v + s * BuilderParameters.SCORE_LAMBDA;

        //            if (!scoreDict[origialB].ContainsKey(predId))
        //            {
        //                scoreDict[origialB][predId] = v;
        //            }
        //            else
        //            {
        //                scoreDict[origialB][predId] = Util.LogAdd(scoreDict[origialB][predId], v);
        //            }


        //            float true_v = 0;
        //            if (targetId.ContainsKey(predId) && targetId[predId] > 0) true_v = 1;

        //            int rawIdx = Query.RawIndex[origialB];
        //            if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
        //            Success[rawIdx] += true_v;

        //            if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = 0;
        //            StepSuccess[t] += true_v;

        //        }


        //        for (int g = 0; g < Query.BatchSize; g++)
        //        {
        //            int relation = Query.RawQuery[g].Item2;
        //            Dictionary<int, float> target = Query.RawTarget[g];
        //            HashSet<int> blackTargets = Query.BlackTargets[g];

        //            Dictionary<int, float> score = scoreDict[g];

        //            int posNum = target.Where(m => m.Value > 0).Count();
        //            bool isGood = posNum == target.Count || posNum == 0;


        //            Dictionary<int, Tuple<float, float>> combine = new Dictionary<int, Tuple<float, float>>();

        //            foreach (int mk in target.Keys)
        //            {
        //                float s = float.MinValue;
        //                if (score.ContainsKey(mk)) s = score[mk];
        //                combine.Add(mk, new Tuple<float, float>(target[mk], s));

        //                if (target[mk] > 0 && !score.ContainsKey(mk))
        //                {
        //                    nonHit += 1;
        //                }
        //                if (target[mk] > 0 && !score.ContainsKey(mk) && !isGood)
        //                {
        //                    effectNonHit += 1;
        //                }

        //            }
        //            var sortD = combine.OrderByDescending(pair => pair.Value.Item2);

        //            float ap = 0;

        //            int idx = 0;
        //            int hit = 0;
        //            foreach (KeyValuePair<int, Tuple<float, float>> mk in sortD)
        //            {
        //                idx += 1;
        //                if (mk.Value.Item1 > 0)
        //                {
        //                    hit += 1;
        //                    ap = ap + hit * 1.0f / idx;
        //                }
        //            }
        //            ap = isGood ? 1 : ap / (hit + 0.00001f);
        //            MAP += ap;
        //        }

        //        SmpIdx += Query.BatchSize;
        //        Query.Results.Clear();
        //        //Report();
        //    }
        //}

        //public class MCTSResult
        //{
        //    public List<int> OriginalBatchIndex = new List<int>();
        //    public List<int> PredIndex = new List<int>();
        //    public List<Dictionary<int, float>> RawTarget = new List<Dictionary<int, float>>();
        //    public List<float> Score = new List<float>();
        //    public List<int> RawIndex = new List<int>();
        //    public List<int> TermStep = new List<int>();
        //    public void Clear()
        //    {
        //        OriginalBatchIndex.Clear();
        //        PredIndex.Clear();
        //        RawTarget.Clear();
        //        Score.Clear();
        //        RawIndex.Clear();
        //        TermStep.Clear();
        //    }

        //    public MCTSResult(QueryBatchData data)
        //    {
        //        for (int i = 0; i < data.Results.Count; i++)
        //        {
        //            int t = data.Results[i].Item1;
        //            int b = data.Results[i].Item2;
        //            int origialB = data.StatusPath[t].GetOriginalStatsIndex(b);
        //            int predId = data.StatusPath[t].NodeID[b];

        //            Dictionary<int, float> targetId = data.RawTarget[origialB];

        //            float v = BuilderParameters.PROB_LAMBDA * (data.StatusPath[t].GetLogProb(b) + (float)Math.Log(data.StatusPath[t].Term.Output.Data.MemPtr[b]));

        //            float ms = Util.Logistic(data.StatusPath[t].Score.Output.Data.MemPtr[b]);

        //            float s = (float)Math.Log(ms + BuilderParameters.BASE_LAMBDA);
        //            v = v + s * BuilderParameters.SCORE_LAMBDA;

        //            string mkey = string.Format("Term {0}", t);
        //            int rawIdx = data.RawIndex[origialB];

        //            OriginalBatchIndex.Add(origialB);
        //            PredIndex.Add(predId);
        //            RawTarget.Add(targetId);
        //            RawIndex.Add(rawIdx);

        //            Score.Add(v);
        //            TermStep.Add(t);
        //        }
        //        data.Results.Clear();
        //    }
        //}

        //class MAPPredictionV4Runner : StructRunner
        //{
        //    float MAP = 0;
        //    int Iteration = 0;
        //    int SmpIdx = 0;
        //    List<MCTSResult> Results { get; set; }
        //    QueryBatchData Query { get; set; }
        //    EpisodicMemory Memory { get; set; }
        //    int nonHit = 0;
        //    int effectNonHit = 0;
        //    public MAPPredictionV4Runner(List<MCTSResult> results, QueryBatchData query, EpisodicMemory memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        //    {
        //        Query = query;
        //        Results = results;
        //        Memory = memory;

        //        Iteration = 0;
        //    }

        //    Dictionary<int, float> Success = new Dictionary<int, float>();
        //    Dictionary<int, float> StepSuccess = new Dictionary<int, float>();

        //    public override void Init()
        //    {
        //        Success.Clear();
        //        StepSuccess.Clear();

        //        SmpIdx = 0;
        //        MAP = 0;
        //        nonHit = 0;
        //        effectNonHit = 0;
        //    }
        //    public void Report()
        //    {
        //        Logger.WriteLog("Sample Idx {0}", SmpIdx);
        //        Logger.WriteLog(string.Format("MAP {0}", MAP / SmpIdx));
        //        Logger.WriteLog("nonhit {0}", nonHit);
        //        Logger.WriteLog("effectNonhit {0}", effectNonHit);

        //    }

        //    public override void Complete()
        //    {
        //        Logger.WriteLog("Final Report!");
        //        {
        //            Report();
        //        }
        //        foreach (KeyValuePair<int, float> i in StepSuccess)
        //        {
        //            Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
        //        }
        //        int sc = Success.Where(i => i.Value > 0).Count();
        //        int t = Success.Count;
        //        Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
        //        Iteration += 1;
        //    }
        //    public override void Forward()
        //    {
        //        Dictionary<int, float>[] scoreDict = new Dictionary<int, float>[Query.BatchSize];
        //        for (int i = 0; i < Query.BatchSize; i++)
        //        {
        //            scoreDict[i] = new Dictionary<int, float>();
        //        }

        //        // add results.
        //        for (int m = 0; m < Results.Count; m++)
        //        {
        //            for (int i = 0; i < Results[m].OriginalBatchIndex.Count; i++)
        //            {
        //                //int t = Results[m].Results[i].Item1;
        //                //int b = Results[m].Results[i].Item2;
        //                int origialB = Results[m].OriginalBatchIndex[i]; // StatusPath[t].GetOriginalStatsIndex(b);
        //                int predId = Results[m].PredIndex[i];

        //                Dictionary<int, float> targetId = Results[m].RawTarget[i];

        //                //float v = Results[m].StatusPath[t].GetLogProb(b);
        //                //if (t != BuilderParameters.MAX_HOP)
        //                //{ v = v + (float)Math.Log(Query[m].StatusPath[t].Term.Output.Data.MemPtr[b]); }// LogPTerm(b, true); }
        //                //float s = (float)Math.Log(Query[m].StatusPath[t].Score.Output.Data.MemPtr[b] + BuilderParameters.BASE_LAMBDA);
        //                //v = v + s * BuilderParameters.MSE_LAMBDA;

        //                float v = Results[m].Score[i];

        //                if (!scoreDict[origialB].ContainsKey(predId))
        //                {
        //                    scoreDict[origialB][predId] = v;
        //                }
        //                else
        //                {
        //                    scoreDict[origialB][predId] = Util.LogAdd(scoreDict[origialB][predId], v);
        //                }

        //                float true_v = 0;
        //                if (targetId.ContainsKey(predId) && targetId[predId] > 0) true_v = 1;

        //                int rawIdx = Results[m].RawIndex[i];
        //                if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
        //                Success[rawIdx] += true_v;

        //                int t = Results[m].TermStep[i];

        //                if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = 0;
        //                StepSuccess[t] += true_v;
        //            }
        //        }


        //        for (int g = 0; g < Query.BatchSize; g++)
        //        {
        //            int relation = Query.RawQuery[g].Item2;
        //            Dictionary<int, float> target = Query.RawTarget[g];
        //            HashSet<int> blackTargets = Query.BlackTargets[g];

        //            Dictionary<int, float> score = scoreDict[g];
        //            Dictionary<int, Tuple<float, float>> combine = new Dictionary<int, Tuple<float, float>>();

        //            int posNum = target.Where(m => m.Value > 0).Count();
        //            bool isGood = posNum == target.Count || posNum == 0;

        //            foreach (int mk in target.Keys)
        //            {
        //                float s = float.MinValue;
        //                if (score.ContainsKey(mk)) s = score[mk];
        //                combine.Add(mk, new Tuple<float, float>(target[mk], s));

        //                if (target[mk] > 0 && !score.ContainsKey(mk))
        //                {
        //                    nonHit += 1;
        //                }
        //                if (target[mk] > 0 && !score.ContainsKey(mk) && !isGood)
        //                {
        //                    effectNonHit += 1;
        //                }
        //            }
        //            var sortD = combine.OrderByDescending(pair => pair.Value.Item2);

        //            float ap = 0;

        //            int idx = 0;
        //            int hit = 0;
        //            foreach (KeyValuePair<int, Tuple<float, float>> mk in sortD)
        //            {
        //                idx += 1;
        //                if (mk.Value.Item1 > 0)
        //                {
        //                    hit += 1;
        //                    ap = ap + hit * 1.0f / idx;
        //                }
        //            }
        //            ap = isGood ? 1 : ap / (hit + 0.000000001f);
        //            MAP += ap;
        //        }

        //        SmpIdx += Query.BatchSize;

        //        Memory.Clear();
        //    }
        //}

        public class NeuralWalkerModel : CompositeNNStructure
        {
            public EmbedStructure InNodeEmbed { get; set; }
            public EmbedStructure InRelEmbed { get; set; }

            public EmbedStructure CNodeEmbed { get; set; }
            public EmbedStructure CRelEmbed { get; set; }

            public DNNStructure SrcDNN { get; set; }
            public DNNStructure TgtDNN { get; set; }

            public LayerStructure AttEmbed { get; set; }
            //public DNNStructure TermDNN { get; set; }
            public DNNStructure ScoreDNN { get; set; }
            //public DNNStructure Query_C_DNN { get; set; }
            //public DNNStructure Query_O_DNN { get; set; }

            public GRUCell RnnCell { get; set; }

            public NeuralWalkerModel(int nodeDim, int relDim, DeviceType device)
            {
                InNodeEmbed = AddLayer(new EmbedStructure(DataPanel.EntityDict.Count, nodeDim, device)); // DeviceType.CPU_FAST_VECTOR));
                CNodeEmbed = InNodeEmbed; //AddLayer(new EmbedStructure(DataPanel.EntityNum, nodeDim, device));// DeviceType.CPU_FAST_VECTOR));
                //share node embedding.

                InRelEmbed = AddLayer(new EmbedStructure(DataPanel.RelationDict.Count, relDim, device)); // DeviceType.CPU_FAST_VECTOR));
                CRelEmbed = AddLayer(new EmbedStructure(DataPanel.RelationDict.Count, relDim, device)); // DeviceType.CPU_FAST_VECTOR));

                int StateDim = nodeDim + relDim;

                SrcDNN = AddLayer(new DNNStructure(StateDim, BuilderParameters.DNN_DIMS,
                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
                                           device));

                TgtDNN = AddLayer(new DNNStructure(StateDim, BuilderParameters.DNN_DIMS,
                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
                                           device));

                AttEmbed = AddLayer(new LayerStructure(BuilderParameters.DNN_DIMS.Last(), 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device));

                //TermDNN = AddLayer(new DNNStructure(StateDim + BuilderParameters.DNN_DIMS.Last(), BuilderParameters.T_NET, BuilderParameters.T_AF,
                //                         BuilderParameters.T_NET.Select(i => true).ToArray(), device));

                ScoreDNN = AddLayer(new DNNStructure(StateDim, BuilderParameters.S_NET, BuilderParameters.S_AF,
                                         BuilderParameters.S_NET.Select(i => true).ToArray(), device));

                //Query_C_DNN = AddLayer(new DNNStructure(StateDim, new int[] { StateDim }, new A_Func[] { A_Func.Tanh }, new bool[] { false }, device));
                //Query_O_DNN = AddLayer(new DNNStructure(StateDim, new int[] { StateDim }, new A_Func[] { A_Func.Tanh }, new bool[] { false }, device));

                RnnCell = AddLayer(new GRUCell(StateDim, StateDim, device));
            }
            public NeuralWalkerModel(BinaryReader reader, DeviceType device)
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

                InNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                CNodeEmbed = InNodeEmbed; // (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);

                InRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                CRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);

                SrcDNN = (DNNStructure)DeserializeNextModel(reader, device);
                TgtDNN = (DNNStructure)DeserializeNextModel(reader, device);

                AttEmbed = (LayerStructure)DeserializeNextModel(reader, device);
                //TermDNN = (DNNStructure)DeserializeNextModel(reader, device);

                //if (BuilderParameters.SEED_VERSION == 1)
                //{
                ScoreDNN = (DNNStructure)DeserializeNextModel(reader, device);

                //Query_C_DNN = (DNNStructure)DeserializeNextModel(reader, device);
                //Query_O_DNN = (DNNStructure)DeserializeNextModel(reader, device);

                //}
                //else
                //{
                //    ScoreDNN = AddLayer(new DNNStructure(BuilderParameters.N_EmbedDim + BuilderParameters.R_EmbedDim, BuilderParameters.S_NET, BuilderParameters.S_AF,
                //                         BuilderParameters.S_NET.Select(i => true).ToArray(), device));
                //}
                RnnCell = (GRUCell)DeserializeNextModel(reader, device);

            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                //InNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //InRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                //CNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //CRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        class MemoryScheduleRunner : StructRunner
        {
            //EpisodicMemory GMemory { get; set; }
            EpisodicMemory LMemory { get; set; }
            //public new List<EpisodicMemory> Output { get; set; }
            //EpisodicMemory globalMemory,
            public MemoryScheduleRunner( EpisodicMemory localMemory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                //GMemory = globalMemory;
                LMemory = localMemory;
                //Output = new List<EpisodicMemory>();
            }
            int Epoch = 0;
            public override void Complete()
            {
                Epoch += 1;
                LMemory.Clear();
                //if (BuilderParameters.MEM_CLEAN > 0 && Epoch % BuilderParameters.MEM_CLEAN == 0) GMemory.Clear();
            }
            public override void Forward()
            {
                LMemory.Clear();
                //Output.Clear();
                //if (Behavior.RunMode == DNNRunMode.Train && SampleRandom.NextDouble() < BuilderParameters.Epsilon(Epoch)) { Output.Add(GMemory); }
                //else { LMemory.Clear(); Output.Add(LMemory); }
                //Epoch += 1;
            }
        }

        public static ComputationGraph BuildComputationGraph(DataEnviroument data, GraphEnviroument graph, int batchSize, int roll, int mctsNum, int beamSize, int maxHop, 
            EpisodicMemory mem, NeuralWalkerModel model, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            DataSampleRunner dataRunner = new DataSampleRunner(data, batchSize, roll, Behavior);
            cg.AddDataRunner(dataRunner);

            // sample a list of tuple from dataset.
            QueryBatchData interface_data = dataRunner.Output;

            // get the embedding from the query data.
            StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, model.InNodeEmbed, model.InRelEmbed, Behavior);
            cg.AddRunner(statusEmbedRunner);

            cg.AddRunner(new MemoryScheduleRunner(mem, Behavior));

            //HiddenBatchData stateEmbed_C = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(model.Query_C_DNN, statusEmbedRunner.Output, Behavior));
            //HiddenBatchData stateEmbed_O = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(model.Query_O_DNN, statusEmbedRunner.Output, Behavior));
            //Tuple<HiddenBatchData, HiddenBatchData> stateEmbed = new Tuple<HiddenBatchData, HiddenBatchData>(stateEmbed_C, stateEmbed_O);

            HiddenBatchData stateEmbed = statusEmbedRunner.Output;

            List<List<StatusData>> mctsStatusPath = new List<List<StatusData>>(); 
            //List<QueryBatchData> Queries = new List<QueryBatchData>();
            // MCTS path number.
            for (int j = 0; j < mctsNum; j++)
            {
                List<StatusData> statusPath = new List<StatusData>();

                //QueryBatchData newQuery = new QueryBatchData(interface_data);
                StatusData status = new StatusData(interface_data, interface_data.RawSource, stateEmbed, Behavior.Device);
                statusPath.Add(status);

                #region multi-hop expan
                // travel four steps in the knowledge graph.
                for (int i = 0; i < maxHop; i++)
                {
                    // given status, obtain the match. miniBatch * maxNeighbor number.
                    CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, i == maxHop - 1, graph, Behavior);
                    cg.AddRunner(candidateActionRunner);

                    // miniMatch * maxNeighborNumber.
                    StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE,
                                                                              model.CNodeEmbed, model.CRelEmbed, Behavior);
                    cg.AddRunner(candEmbedRunner);

                    DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
                    cg.AddRunner(srcHiddenRunner);

                    DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
                    cg.AddRunner(candHiddenRunner);

                    //SeqDenseBatchData neiSeqData = new SeqDenseBatchData(new SequenceDataStat()
                    //{
                    //    FEATURE_DIM = candHiddenRunner.Output.Dim,
                    //    MAX_BATCHSIZE = status.MaxBatchSize,
                    //    MAX_SEQUENCESIZE = candHiddenRunner.Output.MAX_BATCHSIZE
                    //},
                    //                                                         candidateActionRunner.Match.Src2MatchIdx,
                    //                                                         candidateActionRunner.Match.SrcIdx,
                    //                                                         candHiddenRunner.Output.Output.Data,
                    //                                                         candHiddenRunner.Output.Deriv.Data,
                    //                                                         Behavior.Device);
                    //// neighbor dataset.
                    //HiddenBatchData neiData = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(neiSeqData, Behavior));

                    // alignment miniBatch * miniBatch * neighbor.
                    VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
                                                                                candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
                    cg.AddRunner(attentionRunner);

                    // softmax action selection.
                    SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
                                                                                status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
                                                                                candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
                                                                                Behavior.Device), 1, Behavior, true);
                    cg.AddRunner(normAttRunner);

                    status.MatchCandidate = candidateActionRunner.Output;
                    status.MatchCandidateProb = normAttRunner.Output;

                    //MatrixData newStateData = (MatrixData)cg.AddRunner(new MatrixConcateRunner(new List<MatrixData>() { new MatrixData(status.StateEmbed), new MatrixData(neiData) }, Behavior));

                    // estimate the value of returning the node.
                    DNNRunner<HiddenBatchData> scoreRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, status.StateEmbed, Behavior) { name = "V_SCORE" };
                    cg.AddRunner(scoreRunner);
                    status.Score = scoreRunner.Output;

                    BasicPolicyRunner policyRunner = null;
                    if (Behavior.RunMode == DNNRunMode.Train)
                    {
                        policyRunner = new MCTSActionSamplingRunner(status, j, graph.MaxNeighborNum, mem, Behavior);
                    }
                    else
                    {
                        policyRunner = new BeamSearchActionRunner(status, beamSize, graph.MaxNeighborNum, Behavior);
                    }
                    cg.AddRunner(policyRunner);
                    status.ResultIndex = policyRunner.ResultIndex;

                    if (i < maxHop - 1)
                    {
                        #region take action.
                        MatrixExpansionRunner srcOExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
                        cg.AddRunner(srcOExpRunner);

                        //MatrixExpansionRunner srcCExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
                        //cg.AddRunner(srcCExpRunner);

                        MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, policyRunner.MatchPath, 2, Behavior);
                        cg.AddRunner(tgtExpRunner);

                        //MatrixExpansionRunner neiExpRunner = new MatrixExpansionRunner(neiData, policyRunner.MatchPath, 1, Behavior);
                        //cg.AddRunner(neiExpRunner);

                        //HiddenBatchData concateData = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { neiExpRunner.Output, tgtExpRunner.Output }, Behavior));

                        //srcCExpRunner.Output,
                        GRUStateRunner stateRunner = new GRUStateRunner(model.RnnCell, srcOExpRunner.Output, tgtExpRunner.Output, Behavior);
                        cg.AddRunner(stateRunner);
                        #endregion.

                        StatusData nextStatus = new StatusData(interface_data, policyRunner.NodeIndex, stateRunner.Output, // new Tuple<HiddenBatchData, HiddenBatchData>(stateRunner.C, stateRunner.O),
                            policyRunner.QueryIndex, policyRunner.StatusKey, i + 1, policyRunner.LogProb, policyRunner.PreSelActIndex, policyRunner.PreStatusIndex, Behavior.Device);
                        status = nextStatus;
                        statusPath.Add(status);
                    }

                    
                }
                #endregion.
                //if (Behavior.RunMode == DNNRunMode.Train)
                //{
                RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(statusPath, mem, Behavior);
                cg.AddRunner(rewardFeedRunner);
                //}
                //Queries.Add(newQuery);
                mctsStatusPath.Add(statusPath);
            }

            // this is objective function.
            if (Behavior.RunMode == DNNRunMode.Train)
            {
                // policy gradient optimization.
                PolicyGradientRunner pgRunner = new PolicyGradientRunner(mctsStatusPath, mctsNum, Behavior); //aMem,
                cg.AddObjective(pgRunner);
            }
            else if(Behavior.RunMode == DNNRunMode.Predict)
            {
                MAPPredictionRunner predRunner = new MAPPredictionRunner(interface_data, mctsStatusPath, Behavior);
                cg.AddRunner(predRunner);
            }
            cg.SetDelegateModel(model);
            return cg;
        }

        //public class MCTSPredRunner : CompositeNetRunner
        //{
        //    int MCTS { get; set; }
        //    QueryBatchData RawInput { get; set; }
        //    HiddenBatchData EmbedC { get; set; }
        //    HiddenBatchData EmbedO { get; set; }
        //    EpisodicMemory Memory { get; set; }
        //    new NeuralWalkerModel Model { get; set; }
        //    List<MCTSActionSamplingRunner> PolicyRunner = new List<MCTSActionSamplingRunner>();
        //    public List<MCTSResult> Results = new List<MCTSResult>();
        //    public MCTSPredRunner(QueryBatchData interface_data, HiddenBatchData embedC, HiddenBatchData embedO, EpisodicMemory memory, NeuralWalkerModel model, RunnerBehavior behavior) : base(behavior)
        //    {
        //        MCTS = BuilderParameters.MCTS_NUM;

        //        List<EpisodicMemory> Mems = new List<EpisodicMemory>() { memory };
        //        RawInput = interface_data;
        //        EmbedC = embedC;
        //        EmbedO = embedO;

        //        Memory = memory;

        //        Model = model;

        //        //GraphQueryData newQuery = new GraphQueryData(interface_data);
        //        StatusData status = new StatusData(RawInput, RawInput.RawSource, embedC, embedO, Behavior.Device);

        //        #region multi-hop expan
        //        // travel four steps in the knowledge graph.
        //        for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
        //        {
        //            // given status, obtain the match. miniBatch * maxNeighbor number.
        //            CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
        //            LinkRunners.Add(candidateActionRunner);

        //            // miniMatch * maxNeighborNumber.
        //            StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE,
        //                model.CNodeEmbed, model.CRelEmbed, Behavior);
        //            LinkRunners.Add(candEmbedRunner);

        //            DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbedO, Behavior);
        //            LinkRunners.Add(srcHiddenRunner);

        //            DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
        //            LinkRunners.Add(candHiddenRunner);


        //            SeqDenseBatchData neiSeqData = new SeqDenseBatchData(new SequenceDataStat() { FEATURE_DIM = candHiddenRunner.Output.Dim, MAX_BATCHSIZE = status.MaxBatchSize, MAX_SEQUENCESIZE = candHiddenRunner.Output.MAX_BATCHSIZE },
        //                                                                     candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx, candHiddenRunner.Output.Output.Data, candHiddenRunner.Output.Deriv.Data, Behavior.Device);

        //            // neighbor dataset.
        //            MaxPoolingRunner<SeqDenseBatchData> neiPoolRunner = new MaxPoolingRunner<SeqDenseBatchData>(neiSeqData, Behavior);
        //            LinkRunners.Add(neiPoolRunner);
        //            HiddenBatchData neiData = neiPoolRunner.Output;


        //            // alignment miniBatch * miniBatch * neighbor.
        //            VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
        //                                                                        candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
        //            LinkRunners.Add(attentionRunner);

        //            SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
        //                                                                        status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
        //                                                                        candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
        //                                                                        Behavior.Device), 1, Behavior, true);
        //            LinkRunners.Add(normAttRunner);

        //            status.MatchCandidate = candidateActionRunner.Output;
        //            status.MatchCandidateProb = normAttRunner.Output;
        //            // it will cause un-necessary unstable.


        //            MatrixConcateRunner state_neiConcateRunner = new MatrixConcateRunner(new List<MatrixData>() { new MatrixData(status.StateEmbedO), new MatrixData(neiData) }, Behavior);
        //            LinkRunners.Add(state_neiConcateRunner);
        //            MatrixData newStateData = state_neiConcateRunner.Output;

        //            DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, new HiddenBatchData(newStateData), Behavior);
        //            LinkRunners.Add(termRunner);
        //            status.Term = termRunner.Output;

        //            DNNRunner<HiddenBatchData> scoreRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, new HiddenBatchData(newStateData), Behavior);
        //            LinkRunners.Add(scoreRunner);
        //            status.Score = scoreRunner.Output;

        //            //BasicPolicyRunner PolicyRunner = null;

        //            // select the most promising state.
        //            MCTSActionSamplingRunner tmpPolicyRunner = new MCTSActionSamplingRunner(status, -1, Mems, Behavior, i == BuilderParameters.MAX_HOP - 1);
        //            LinkRunners.Add(tmpPolicyRunner);

        //            PolicyRunner.Add(tmpPolicyRunner);

        //            if (i < BuilderParameters.MAX_HOP - 1)
        //            {
        //                #region version 2 of action selection.
        //                MatrixExpansionRunner srcCExpRunner = new MatrixExpansionRunner(status.StateEmbedC, tmpPolicyRunner.MatchPath, 1, Behavior);
        //                LinkRunners.Add(srcCExpRunner);

        //                MatrixExpansionRunner srcOExpRunner = new MatrixExpansionRunner(status.StateEmbedO, tmpPolicyRunner.MatchPath, 1, Behavior);
        //                LinkRunners.Add(srcOExpRunner);


        //                MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, tmpPolicyRunner.MatchPath, 2, Behavior);
        //                LinkRunners.Add(tgtExpRunner);

        //                MatrixExpansionRunner neiExpRunner = new MatrixExpansionRunner(neiData, tmpPolicyRunner.MatchPath, 1, Behavior);
        //                LinkRunners.Add(neiExpRunner);

        //                EnsembleMatrixRunner enseRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { neiExpRunner.Output, tgtExpRunner.Output }, Behavior);
        //                LinkRunners.Add(enseRunner);
        //                HiddenBatchData concateData = enseRunner.Output;


        //                LSTMStateRunner stateRunner = new LSTMStateRunner(model.RnnCell, srcOExpRunner.Output, srcCExpRunner.Output, concateData, Behavior);
        //                LinkRunners.Add(stateRunner);
        //                #endregion.

        //                StatusData nextStatus = new StatusData(status.Query, tmpPolicyRunner.NodeIndex, tmpPolicyRunner.LogProb,
        //                    tmpPolicyRunner.PreSelActIndex, tmpPolicyRunner.PreStatusIndex, tmpPolicyRunner.StatusKey,stateRunner.C, stateRunner.O, Behavior.Device);

        //                //DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, nextStatus.StateEmbed, Behavior);
        //                //LinkRunners.Add(termRunner);
        //                //nextStatus.Term = termRunner.Output;

        //                //DNNRunner<HiddenBatchData> scoreRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, nextStatus.StateEmbed, Behavior);
        //                //LinkRunners.Add(scoreRunner);
        //                //nextStatus.Score = scoreRunner.Output;

        //                status = nextStatus;
        //            }
        //        }
        //        RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(RawInput, Mems, Behavior);
        //        LinkRunners.Add(rewardFeedRunner);
        //        #endregion.   
        //    }

        //    // only works for prediction. in the prediction, things are changed.
        //    public override void Forward()
        //    {
        //        Results.Clear();
        //        for (int i = 0; i < MCTS; i++)
        //        {
        //            foreach (MCTSActionSamplingRunner runner in PolicyRunner)
        //                runner.MCTSIdx = i;
        //            base.Forward();
        //            Results.Add(new MCTSResult(RawInput));
        //        }
        //    }
        //}

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            NeuralWalkerModel model =
                BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
                new NeuralWalkerModel(BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, device) :
                new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);

            EpisodicMemory Memory = new EpisodicMemory(true);

            model.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

            //ComputationGraph validCG = null;
            ComputationGraph testCG = null;
            // prediction. 0:beam search; 1:mcts search;

            if (BuilderParameters.SCORE_TYPE == 0)
            {
                //validCG = BuildComputationGraph(DataPanel.DevData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                //                                 1, 1, BuilderParameters.BeamSize, BuilderParameters.MAX_HOP, Memory, model, 
                //                                 new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                testCG = BuildComputationGraph(DataPanel.TestData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                                                 1, 1, BuilderParameters.BeamSize, BuilderParameters.MAX_HOP, Memory, model,
                                                 new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
            }
            else if (BuilderParameters.SCORE_TYPE == 1)
            {
                //validCG = BuildComputationGraph(DataPanel.DevData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize, 
                //                                 1, BuilderParameters.TEST_MCTS_NUM, 1, BuilderParameters.MAX_HOP, Memory, model,
                //                                 new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                testCG = BuildComputationGraph(DataPanel.TestData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                                                 1, BuilderParameters.TEST_MCTS_NUM, 1, BuilderParameters.MAX_HOP, Memory, model,
                                                 new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
            }
            
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainData, DataPanel.GraphEnv, BuilderParameters.MiniBatchSize,
                                                                     BuilderParameters.ROLL_NUM, BuilderParameters.MCTS_NUM, 1, BuilderParameters.MAX_HOP, Memory, model,  
                                                                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    //testCG.Execute();
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        //ActionSampler = new Random(10);
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        if ((iter + 1) % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            Logger.WriteLog("Evaluation at Iteration {0}", iter);
                            //validCG.Execute();
                            testCG.Execute();
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "model." + iter.ToString(), FileMode.Create, FileAccess.Write)))
                            {
                                model.Serialize(writer);
                            }
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    //validCG.Execute();
                    testCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// status data.
        /// </summary>
        //public class MCTSRunner : CompositeNetRunner
        //{
        //    int MCTS { get; set; }

        //    QueryBatchData Query { get; set; }
        //    HiddenBatchData InitEmbed { get; set; }
        //    EpisodicMemory Memory { get; set; }
        //    new NeuralWalkerModel Model { get; set; }

        //    StatePolicyRunner PolicyRunner { get; set; }
        //    StateUpdateRunner UpdateRunner { get; set; }
        //    public GraphPathData Path { get; set; }

        //    /// <summary>
        //    /// forward execution only.
        //    /// execution mcts in parallel for different samples.
        //    /// </summary>
        //    /// <param name="query"></param>
        //    /// <param name="embed"></param>
        //    /// <param name="memory"></param>
        //    /// <param name="model"></param>
        //    /// <param name="behavior"></param>
        //    public MCTSRunner(QueryBatchData query, Tuple<HiddenBatchData, HiddenBatchData> initEmbed, EpisodicMemory memory, NeuralWalkerModel model, RunnerBehavior behavior) : base(behavior)
        //    {
        //        MCTS = BuilderParameters.MCTS_NUM;

        //        Query = query;
        //        InitEmbed = initEmbed;
        //        Memory = memory;
        //        Model = model;
        //        PolicyRunner = new StatePolicyRunner(query, new List<Tuple<int, int>>(), new HiddenBatchData(1, InitEmbed.Dim, DNNRunMode.Train, Behavior.Device), 1, model, Behavior);

        //        UpdateRunner = new StateUpdateRunner(new HiddenBatchData(1, InitEmbed.Dim, DNNRunMode.Train, Behavior.Device),
        //                                             new HiddenBatchData(1, PolicyRunner.NeiPool.Dim, DNNRunMode.Train, Behavior.Device),
        //                                             new HiddenBatchData(DataPanel.MaxNeighborCount, PolicyRunner.NeiEmds.Dim, DNNRunMode.Train, Behavior.Device),
        //                                             new BiMatchBatchData(new BiMatchBatchDataStat() { MAX_SRC_BATCHSIZE = 1, MAX_TGT_BATCHSIZE = DataPanel.MaxNeighborCount, MAX_MATCH_BATCHSIZE = 1 },
        //                                             Behavior.Device), model, Behavior);

        //        Path = new GraphPathData(Query.MaxBatchSize);
        //    }

        //    // only works for prediction. in the prediction, things are changed.
        //    public override void Forward()
        //    {
        //        Path.Clear();
        //        InitEmbed.Output.Data.SyncToCPU();
        //        for (int b = 0; b < Query.BatchSize; b++)
        //        {
        //            Memory.Clear();

        //            string statusKey = string.Format("N:{0}-R:{1}", Query.RawQuery[b].Item1, Query.RawQuery[b].Item2);
        //            float[] embed = new float[InitEmbed.Dim];
        //            Array.Copy(InitEmbed.Output.Data.MemPtr, b * InitEmbed.Dim, embed, 0, InitEmbed.Dim);
        //            int node = Query.RawQuery[b].Item1;

        //            Memory.Add(statusKey, embed);

        //            List<Tuple<int, string, float[]>> Paths = new List<Tuple<int, string, float[]>>();

        //            List<Tuple<int, int, int>> PathActions = new List<Tuple<int, int, int>>();
        //            List<float[]> PathDiverseActions = new List<float[]>();

        //            Paths.Add(new Tuple<int, string, float[]>(node, statusKey, embed));

        //            for (int step = 0; step < BuilderParameters.MAX_HOP; step++)
        //            {
        //                Tuple<int, string, float[]> root = Paths[step];

        //                for (int i = 0; i < MCTS; i++)
        //                {
        //                    string nodeKey = root.Item2;
        //                    int nodeIdx = root.Item1;
        //                    float[] nodeEmd = root.Item3;
        //                    Dictionary<string, int> actionDict = new Dictionary<string, int>();

        //                    for (int h = step + 1; h < BuilderParameters.MAX_HOP; h++)
        //                    {
        //                        StateRecord arms = Memory.Search(nodeKey);

        //                        if (arms.IsLeaf)
        //                        {
        //                            #region expand leaf node.
        //                            PolicyRunner.InputNodes.Clear();
        //                            PolicyRunner.InputNodes.Add(new Tuple<int, int>(nodeIdx, b));
        //                            PolicyRunner.StateEmbed.BatchSize = 1;
        //                            PolicyRunner.StateEmbed.Output.Data.SyncFromCPU(0, nodeEmd, 0, InitEmbed.Dim);

        //                            PolicyRunner.Forward();

        //                            PolicyRunner.NeighProb.Output.SyncToCPU();
        //                            PolicyRunner.T.Output.Data.SyncToCPU();
        //                            PolicyRunner.V.Output.Data.SyncToCPU();
        //                            PolicyRunner.R.Output.Data.SyncToCPU();
        //                            PolicyRunner.NeiPool.Output.Data.SyncToCPU();
        //                            PolicyRunner.NeiEmds.Output.Data.SyncToCPU();

        //                            int neighNum = PolicyRunner.Neighbors.Count;
        //                            int armNum = neighNum + 1;

        //                            float t = Util.Logistic(PolicyRunner.T.Output.Data.MemPtr[0]);
        //                            if (t >= BuilderParameters.T_MAX_CLIP) { t = BuilderParameters.T_MAX_CLIP; }
        //                            if (t <= BuilderParameters.T_MIN_CLIP) { t = BuilderParameters.T_MIN_CLIP; }
        //                            float[] p = new float[armNum];

        //                            Array.Copy(PolicyRunner.NeighProb.Output.MemPtr, p, neighNum);
        //                            for (int a = 0; a < neighNum; a++) { p[a] = p[a] * (1 - t); }
        //                            p[neighNum] = t;

        //                            float v = PolicyRunner.V.Output.Data.MemPtr[0]; // (float)Math.Tanh(PolicyRunner.V.Output.Data.MemPtr[0]);
        //                            float r = PolicyRunner.R.Output.Data.MemPtr[0]; // (float)Math.Tanh(PolicyRunner.R.Output.Data.MemPtr[0]);

        //                            // expand one leaf per time.
        //                            Memory.ExpandLeaf(nodeKey, armNum, p, v, r,
        //                                                   PolicyRunner.NeiPool.Dim, PolicyRunner.NeiPool.Output.Data.MemPtr,
        //                                                   PolicyRunner.NeiEmds.Dim, PolicyRunner.NeiEmds.Output.Data.MemPtr,
        //                                                   PolicyRunner.Neighbors.Select(tmp => tmp.Item1).ToArray(),
        //                                                   PolicyRunner.Neighbors.Select(tmp => tmp.Item2).ToArray());
        //                            #endregion.

        //                            //backup.
        //                            foreach (KeyValuePair<string, int> item in actionDict)
        //                            {
        //                                Memory.Update(item.Key, item.Value, v);
        //                            }

        //                            break;
        //                        }

        //                        int action = h == (BuilderParameters.MAX_HOP - 1) ? arms.ArmNum - 1 :
        //                                arms.AlphaGoZero_Bandit(BuilderParameters.PUCT_C, BuilderParameters.PUCT_D);

        //                        actionDict.Add(nodeKey, action);

        //                        // termination state.
        //                        if (action == arms.ArmNum - 1)
        //                        {
        //                            foreach (KeyValuePair<string, int> item in actionDict)
        //                            {
        //                                Memory.Update(item.Key, item.Value, arms.R);
        //                            }
        //                            break;
        //                        }
        //                        else
        //                        {
        //                            nodeIdx = arms.NextN[action];
        //                            int relIdx = arms.NextR[action];

        //                            // statusKey changed.
        //                            nodeKey = string.Format("{0}-N:{1}-R:{2}", nodeKey, nodeIdx, relIdx);

        //                            StateRecord tmpArm = Memory.Search(nodeKey);
        //                            if (tmpArm != null)
        //                            {
        //                                Array.Copy(tmpArm.Emd, nodeEmd, tmpArm.Emd.Length);
        //                            }
        //                            else
        //                            {
        //                                #region state update runner.
        //                                // regiester new key, embed.
        //                                UpdateRunner.StateEmbed.BatchSize = 1;
        //                                UpdateRunner.StateEmbed.Output.Data.SyncFromCPU(0, nodeEmd, 0, InitEmbed.Dim);


        //                                UpdateRunner.StatePoolEmbed.BatchSize = 1;
        //                                UpdateRunner.StatePoolEmbed.Output.Data.SyncFromCPU(0, arms.NeighPoolEmd, 0, arms.NeiPoolEmdDim);

        //                                UpdateRunner.NeighEmbed.BatchSize = arms.ArmNum - 1;
        //                                UpdateRunner.NeighEmbed.Output.Data.SyncFromCPU(0, arms.NeighEmds, 0, arms.NeighEmdDim * (arms.ArmNum - 1));

        //                                UpdateRunner.Match.SetMatch(new List<Tuple<int, int, float>>() { new Tuple<int, int, float>(0, action, 1) });
        //                                UpdateRunner.Forward();

        //                                UpdateRunner.NewStateEmbed.Output.Data.SyncToCPU();
        //                                Array.Copy(UpdateRunner.NewStateEmbed.Output.Data.MemPtr, nodeEmd, InitEmbed.Dim);
        //                                Memory.Add(nodeKey, nodeEmd);
        //                                #endregion.
        //                            }
        //                        }
        //                    }
        //                }

        //                // select action based on visitCount.
        //                StateRecord rootState = Memory.Mem[root.Item2];
        //                float[] actProb = rootState.ActionProb(0.5f);

        //                int selectA = Util.Sample(actProb, SampleRandom);
        //                PathActions.Add(new Tuple<int, int, int>(root.Item1, selectA, rootState.ArmNum));
        //                PathDiverseActions.Add(actProb);

        //                if (selectA == rootState.ArmNum - 1) { break; }

        //                int newRootIdx = rootState.NextN[selectA];
        //                string newRootKey = string.Format("{0}-N:{1}-R:{2}", root.Item2, newRootIdx, rootState.NextR[selectA]);
        //                float[] newRootEmd = new float[InitEmbed.Dim];
        //                #region root node update.
        //                StateRecord newRootArm = Memory.Search(newRootKey);
        //                if (newRootArm != null)
        //                {
        //                    Array.Copy(newRootArm.Emd, newRootEmd, InitEmbed.Dim);
        //                }
        //                else
        //                {
        //                    // regiester new key, embed.
        //                    UpdateRunner.StateEmbed.BatchSize = 1;
        //                    UpdateRunner.StateEmbed.Output.Data.SyncFromCPU(0, root.Item3, 0, InitEmbed.Dim);

        //                    UpdateRunner.StatePoolEmbed.BatchSize = 1;
        //                    UpdateRunner.StatePoolEmbed.Output.Data.SyncFromCPU(0, rootState.NeighPoolEmd, 0, rootState.NeiPoolEmdDim);

        //                    UpdateRunner.NeighEmbed.BatchSize = rootState.ArmNum - 1;
        //                    UpdateRunner.NeighEmbed.Output.Data.SyncFromCPU(0, rootState.NeighEmds, 0, rootState.NeighEmdDim * (rootState.ArmNum - 1));

        //                    UpdateRunner.Match.SetMatch(new List<Tuple<int, int, float>>() { new Tuple<int, int, float>(0, selectA, 1) });
        //                    UpdateRunner.Forward();

        //                    UpdateRunner.NewStateEmbed.Output.Data.SyncToCPU();
        //                    Array.Copy(UpdateRunner.NewStateEmbed.Output.Data.MemPtr, newRootEmd, InitEmbed.Dim);
        //                    Memory.Add(newRootKey, newRootEmd);
        //                }
        //                #endregion.

        //                Paths.Add(new Tuple<int, string, float[]>(newRootIdx, newRootKey, newRootEmd));
        //            }

        //            Path.AddPath(PathActions, PathDiverseActions);

        //        }

        //    }
        //}


        public class QueryBatchData : BatchData
        {
            public List<int> RawIndex = new List<int>();
            public List<int> RawSource = new List<int>();
            public List<Tuple<int, int>> RawQuery = new List<Tuple<int, int>>();
            public List<int> RawTarget = new List<int>();
            public List<List<int>> RawTruths = new List<List<int>>();

            public int BatchSize { get { return RawQuery.Count; } }
            public int MaxBatchSize { get; set; }

            public void Clear()
            {
                RawIndex.Clear();
                RawSource.Clear();
                RawQuery.Clear();
                RawTarget.Clear();
                RawTruths.Clear();
            }

            public void Push(int rawIdx, int srcIdx, int relIdx, int tgtIdx, List<int> truthIdxs)
            {
                RawIndex.Add(rawIdx);
                RawSource.Add(srcIdx);
                RawQuery.Add(new Tuple<int, int>(srcIdx, relIdx));
                RawTarget.Add(tgtIdx);
                RawTruths.Add(truthIdxs);
            }

            /// <summary>
            /// max group number, and group size;
            /// </summary>
            /// <param name="maxGroupNum"></param>
            /// <param name="groupSize"></param>
            /// <param name="device"></param>
            public QueryBatchData(int maxBatchSize, DeviceType device)
            {
                MaxBatchSize = maxBatchSize;
            }
        }

        class DataSampleRunner : StructRunner
        {
            DataEnviroument Data { get; set; }
            public new QueryBatchData Output { get; set; }
            int MaxBatchSize { get; set; }
            int GroupSize { get; set; }
            int Roll { get; set; }
            DataRandomShuffling Shuffle { get; set; }
            public DataSampleRunner(DataEnviroument data, int groupSize, int roll, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                Roll = roll;
                GroupSize = groupSize;
                MaxBatchSize = groupSize * Roll;
                Shuffle = new DataRandomShuffling(Data.Triple.Count, DataRandom);
                Output = new QueryBatchData(MaxBatchSize, behavior.Device);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
            }

            public override void Forward()
            {
                Output.Clear();
                int groupIdx = 0;
                while (groupIdx < GroupSize)
                {
                    int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext();
                    if (idx <= -1) { break; }

                    int srcId = Data.Triple[idx].Item1;
                    int linkId = Data.Triple[idx].Item2;
                    int tgtId = Data.Triple[idx].Item3;
                    List<int> truthIds = Data.TruthDict[new Tuple<int, int>(srcId, linkId)];

                    for (int roll = 0; roll < Roll; roll++)
                    {
                        Output.Push(idx, srcId, linkId, tgtId, truthIds);
                    }
                    groupIdx += 1;
                }
                if (groupIdx == 0) { IsTerminate = true; return; }
            }
        }

        class DataUtil
        {
            public static List<Tuple<int, float>>[] AggregrateScore(List<List<StatusData>> data, QueryBatchData query, float prob_wei, float score_wei)
            {
                List<Tuple<int, float>>[] result = new List<Tuple<int, float>>[query.BatchSize];
                for (int b = 0; b < query.BatchSize; b++)
                {
                    result[b] = new List<Tuple<int, float>>();
                }

                for (int m = data.Count - 1; m >= 0; m--)
                {
                    for (int step = data[m].Count - 1; step >= 0; step--)
                    {
                        StatusData st = data[m][step];
                        for (int i = 0; i < st.ResultIndex.Count; i++)
                        {
                            int b = st.ResultIndex[i];
                            int origialB = data[m][step].QueryIdx(b); 
                            int predId = data[m][step].NodeID[b];
                            int targetId = query.RawTarget[origialB];

                            //if (query.RawTruths[origialB].Contains(predId) && predId != targetId) continue;

                            float score = prob_wei * data[m][step].GetLogProb(b) + score_wei * Util.LogLogistial(data[m][step].Score.Output.Data.MemPtr[b]);

                            result[origialB].Add(new Tuple<int, float>(predId, score));
                            //if (!result[origialB].ContainsKey(predId))
                            //{
                                
                            //}
                            //else
                            //{
                            //    result[origialB][predId] = Math.Max(result[origialB][predId], score); // Util.LogAdd(result[origialB][predId], score);
                            //}
                        }
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// load data.
        /// </summary>
        public class DataPanel
        {
            public static GraphEnviroument GraphEnv { get; set; }

            public static DataEnviroument TrainData { get; set; }
            public static DataEnviroument TestData { get; set; }
            public static DataEnviroument DevData { get; set; }


            public static Dictionary<string, int> EntityDict { get; set; }
            public static Dictionary<string, int> RelationDict { get; set; }

            public static Dictionary<int, string> EntityLookup { get; set; }
            public static Dictionary<int, string> RelationLookup { get; set; }

            public static string START_SYMBOL = "#START#";
            public static string END_SYMBOL = "#END#";

            public static void Init()
            {
                EntityDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(BuilderParameters.InputDir + @"\vocab\entity_vocab.json"));
                RelationDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(BuilderParameters.InputDir + @"\vocab\relation_vocab.json"));
                RelationDict.Add(END_SYMBOL, RelationDict.Count);
                RelationDict.Add(START_SYMBOL, RelationDict.Count);

                EntityLookup = new Dictionary<int, string>();
                foreach (KeyValuePair<string, int> item in EntityDict)
                {
                    EntityLookup.Add(item.Value, item.Key);
                }

                RelationLookup = new Dictionary<int, string>();
                foreach (KeyValuePair<string, int> item in RelationDict)
                {
                    RelationLookup.Add(item.Value, item.Key);
                }

                GraphEnv = new GraphEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + @"\graph.txt");

                TrainData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + @"\train.txt",
                                            new string[] { BuilderParameters.InputDir + @"\train.txt" });

                TestData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + @"\test.txt",
                                            new string[] { BuilderParameters.InputDir + @"\test.txt",
                                                           BuilderParameters.InputDir + @"\train.txt",
                                                           BuilderParameters.InputDir + @"\dev.txt",
                                                           BuilderParameters.InputDir + @"\graph.txt" });

                DevData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + @"\dev.txt",
                                            new string[] { BuilderParameters.InputDir + @"\test.txt",
                                                           BuilderParameters.InputDir + @"\train.txt",
                                                           BuilderParameters.InputDir + @"\dev.txt",
                                                           BuilderParameters.InputDir + @"\graph.txt" });
            }

           
        }

        public class DataEnviroument
        {
            public List<Tuple<int, int, int>> Triple { get; set; }

            public Dictionary<Tuple<int, int>, List<int>> TruthDict { get; set; }

            public DataEnviroument(Dictionary<string, int> entityDict, Dictionary<string, int> relationDict, string tripleFile, string[] graphFiles)
            {
                Triple = new List<Tuple<int, int, int>>();
                TruthDict = new Dictionary<Tuple<int, int>, List<int>>();
                using (StreamReader tripleReader = new StreamReader(tripleFile))
                {
                    while (!tripleReader.EndOfStream)
                    {
                        string[] tokens = tripleReader.ReadLine().Split('\t');
                        
                        if (!entityDict.ContainsKey(tokens[0]) || !relationDict.ContainsKey(tokens[1]) || !entityDict.ContainsKey(tokens[2]))
                        {
                            Console.WriteLine("Graph doesn't exist {0}, {1}, {2}", tokens[0], tokens[1], tokens[2]);
                            continue;
                        }

                        int e1 = entityDict[tokens[0]];
                        int r = relationDict[tokens[1]];
                        int e2 = entityDict[tokens[2]];

                        Triple.Add(new Tuple<int, int, int>(e1, r, e2));
                    }
                }

                foreach (string gStr in graphFiles)
                {
                    using (StreamReader gReader = new StreamReader(gStr))
                    {
                        while (!gReader.EndOfStream)
                        {
                            string[] tokens = gReader.ReadLine().Split('\t');

                            if (!entityDict.ContainsKey(tokens[0]) || !relationDict.ContainsKey(tokens[1]) || !entityDict.ContainsKey(tokens[2]))
                            {
                                Console.WriteLine("Graph doesn't exist {0}, {1}, {2}", tokens[0], tokens[1], tokens[2]);
                                continue;
                            }

                            int e1 = entityDict[tokens[0]];
                            int r = relationDict[tokens[1]];
                            int e2 = entityDict[tokens[2]];

                            Tuple<int, int> gKey = new Tuple<int, int>(e1, r);

                            if (!TruthDict.ContainsKey(gKey))
                            {
                                TruthDict.Add(gKey, new List<int>());
                            }
                            TruthDict[gKey].Add(e2);
                        }
                    }
                }
            }
        }

        public class GraphEnviroument
        { 
            public Dictionary<int, List<Tuple<int, int>>> Graph { get; set; }

            public int MaxNeighborNum { get; set; }
            public int StopIdx { get; set; }
            public int StartIdx { get; set; }
            public GraphEnviroument(Dictionary<string, int> entityDict, Dictionary<string, int> relationDict, string graphFile)
            {
                Graph = new Dictionary<int, List<Tuple<int, int>>>();
                StopIdx = relationDict[DataPanel.END_SYMBOL];
                StartIdx = relationDict[DataPanel.START_SYMBOL];
                using (StreamReader graphReader = new StreamReader(graphFile))
                {
                    while(!graphReader.EndOfStream)
                    {
                        string[] tokens = graphReader.ReadLine().Split('\t');

                        if (!entityDict.ContainsKey(tokens[0]) || !relationDict.ContainsKey(tokens[1]) || !entityDict.ContainsKey(tokens[2]))
                        {
                            Console.WriteLine("Graph doesn't exist {0}, {1}, {2}", tokens[0], tokens[1], tokens[2]);
                            continue;
                        }
                        int e1 = entityDict[tokens[0]];
                        int r = relationDict[tokens[1]];
                        int e2 = entityDict[tokens[2]];

                        if (!Graph.ContainsKey(e1)) { Graph.Add(e1, new List<Tuple<int, int>>()); }

                        // set the maximum action number.
                        if (BuilderParameters.MAX_ACTION_NUM > 0 && Graph[e1].Count >= BuilderParameters.MAX_ACTION_NUM)
                            continue;
                        Graph[e1].Add(new Tuple<int, int>(r, e2));
                    }
                }

                MaxNeighborNum = 0;
                foreach (int entity in Graph.Keys)
                {
                    Graph[entity].Add(new Tuple<int, int>(StopIdx, entity));
                    if(Graph[entity].Count > MaxNeighborNum) { MaxNeighborNum = Graph[entity].Count; }
                }

            }
            
        }
    }
}
