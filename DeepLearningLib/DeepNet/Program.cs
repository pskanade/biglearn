﻿using BigLearn;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.ExceptionServices;
using System.Security;

using BigLearn.Common;
namespace BigLearn.DeepNet
{
    public class Program
    {
        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public unsafe static void Main(string[] args)
        {
            Console.WriteLine("App command line:");
            for (int i = 0; i < args.Length; i++)
            {
                Console.Write(args[i] + " ");
            }

            Console.WriteLine();

            Builder builder = null;
            if (args.Length <= 0)
            {
                Console.WriteLine("Please specify config file:");
                Environment.Exit(-1);
            }
            else if (args.Length == 1)
            {
                builder = Builder.Init(args[0]);
            }
            else if(args.Length <= 1000)
            {
                string config = args[0];
                string[] add_args = new string[args.Length - 1];
                for (int t = 1; t < args.Length; t++) add_args[t - 1] = args[t];
                builder = Builder.Init(config, add_args);
            }
            else
            {
                DNNAppArgs appArgs = ArgumentsFactory<DNNAppArgs>.GetInstance(args);
                if (appArgs.NeedHelp)
                {
                    Console.WriteLine("Usage:\r\n");
                    appArgs.Usage();

                    Console.WriteLine("Or you can use in legacy way which will be deprecated soon:\r\n");
                    Console.WriteLine("{0} {{Path of the configure file}}", Assembly.GetExecutingAssembly().GetName().Name);

                    Environment.Exit(0);
                }
                PerfCounter.Manager.Instance.Verbose = appArgs.Verbose;
                builder = Builder.Init(appArgs);
            }
            DateTime now = DateTime.Now;
            //int returnCode = 0;
#if DEBUG
            Console.WriteLine("Program start at: {0}", DateTime.Now);
            Console.WriteLine("Requirment! ");
            builder.Rock();
            builder = null;
            //Environment.Exit(returnCode);
#endif

            try
            {
                if (builder != null)
                {
                    Console.WriteLine("Program start at: {0}", DateTime.Now);
                    Console.WriteLine("Requirment! ");
                    builder.Rock();
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Failed with unknown error: {0}", ex);
                if (!Directory.Exists("MiniDumps"))
                {
                    Directory.CreateDirectory("MiniDumps");
                }

                MiniDump.WriteMiniDump(@"MiniDumps");
                //returnCode = -1;
            }
            finally
            {
                Console.WriteLine("Program End at: {0}. During: {1} s", DateTime.Now, (DateTime.Now - now).TotalSeconds);
                PerfCounter.Manager.Instance.DumpStatistics();
            }
            Console.WriteLine("Builder Rock Done!");
            //Environment.Exit(returnCode);
        }
    }
}
