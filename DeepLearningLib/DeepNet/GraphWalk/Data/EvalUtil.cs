using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
	public class EvalUtil 
       {
            /// scoreType 0: Q value; 1 : Path probability;
            public static void AggregrateProb(GraphQueryData query, Dictionary<int, Tuple<List<int>, List<float>, int > > result)
            {
                StatusData st = query.StatusPath.Last();
                {
                    int t = st.Step;
                    for (int i = 0; i < st.BatchSize; i++) //.Results.Count; i++)
                    {
                        int b = i;
                        int predId = st.NodeID[b];
                        int origialB = st.GetOriginalStatsIndex(b);
                        int rawIdx = query.RawIndex[origialB];
                        float score = st.GetLogProb(b);
                        int tgtIdx =  query.RawTarget[origialB];

                        if(!result.ContainsKey(rawIdx)) 
                        	result.Add(rawIdx, new Tuple<List<int>, List<float>, int>(new List<int>(), new List<float>(), tgtIdx));

                        if(query.BlackTargets[origialB].Contains(predId) && query.RawTarget[origialB] != predId)
                        {
                            continue;
                        }
                        result[rawIdx].Item1.Add(predId);
                        result[rawIdx].Item2.Add(score);
                    }
                }
            }

            /// scoreType 0: Q value; 1 : Path probability;
            public static void AggregrateScore(GraphQueryData query, Dictionary<int, Tuple<List<int>, List<float>, int > > result)
            {
                StatusData st = query.StatusPath.Last();
                {
                    int t = st.Step;
                    if(st.Score.Length != st.BatchSize)
                    {
                        throw new Exception(string.Format("score dimension is not match with batch size {0}, {1}",  st.Score.Length, st.BatchSize));
                    }
                    st.Score.Output.SyncToCPU();

                    for (int i = 0; i < st.BatchSize; i++) //.Results.Count; i++)
                    {
                        int b = i;
                        int predId = st.NodeID[b];
                        int origialB = st.GetOriginalStatsIndex(b);
                        int rawIdx = query.RawIndex[origialB];
                        float score = (float)Math.Tanh(st.Score.Output.MemPtr[b]); // Util.Logistic(st.Score.Output.MemPtr[b]); //st.GetLogProb(b);
                        int tgtIdx =  query.RawTarget[origialB];

                        if(!result.ContainsKey(rawIdx)) 
                            result.Add(rawIdx, new Tuple<List<int>, List<float>, int>(new List<int>(), new List<float>(), tgtIdx));

                        if(query.BlackTargets[origialB].Contains(predId) && tgtIdx != predId)
                        {
                            continue;
                        }
                        
                        result[rawIdx].Item1.Add(predId);
                        result[rawIdx].Item2.Add(score);
                    }
                }
            }

            public static void HitK(HiddenBatchData score, List<List<int>> black, List<int> label, bool isAscending, Dictionary<string, float> result)
            {
                float AP = 0;
                float Hit1 = 0;
                float Hit3 = 0;
                float Hit5 = 0;
                float Hit10 = 0;
                float Hit20 = 0;

                if(!result.ContainsKey("AP")) result["AP"] = 0;
                if(!result.ContainsKey("Hit1")) result["Hit1"] = 0;
                if(!result.ContainsKey("Hit3")) result["Hit3"] = 0;
                if(!result.ContainsKey("Hit5")) result["Hit5"] = 0;
                if(!result.ContainsKey("Hit10")) result["Hit10"] = 0;
                if(!result.ContainsKey("Hit20")) result["Hit20"] = 0;
                if(!result.ContainsKey("SMP")) result["SMP"] = 0;
                
                score.Output.Data.SyncToCPU(score.BatchSize * score.Dim);

                int THREAD_NUM = ParameterSetting.BasicMathLibThreadNum;
                int total = score.BatchSize;
                int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;

                float[] mAP = new float[THREAD_NUM];
                float[] mHit1 = new float[THREAD_NUM];
                float[] mHit3 = new float[THREAD_NUM];
                float[] mHit5 = new float[THREAD_NUM];
                float[] mHit10 = new float[THREAD_NUM];
                float[] mHit20 = new float[THREAD_NUM];
                
                Parallel.For(0, THREAD_NUM, thread_idx =>
                {
                    for (int t = 0; t < process_len; t++)
                    {
                        int idx = thread_idx * process_len + t;
                        if (idx < total)
                        {
                            int pos = 0;

                            IEnumerable<int> orders = isAscending ?
                                                            score.Output.Data.MemPtr.Skip(idx * score.Dim).Take(score.Dim).Select((item, index) => new { item, index }).OrderBy(a => a.item).Select(a => a.index)
                                                            : score.Output.Data.MemPtr.Skip(idx * score.Dim).Take(score.Dim).Select((item, index) => new { item, index }).OrderBy(a => a.item).Select(a => a.index).Reverse();
                            foreach(int predId in orders)
                            {
                                if(black[idx].Contains(predId) && label[idx] != predId)
                                {
                                    continue;
                                }
                                pos += 1;
                                if (label[idx] == predId)
                                {
                                    mAP[thread_idx] += 1.0f / pos;
                                    if(pos <= 1) { mHit1[thread_idx] += 1; }
                                    if (pos <= 3) { mHit3[thread_idx] += 1; }
                                    if (pos <= 5) { mHit5[thread_idx] += 1; }
                                    if (pos <= 10) { mHit10[thread_idx] += 1; }
                                    if (pos <= 20) { mHit20[thread_idx] += 1; }
                                    break;
                                }
                            }
                        }
                        else
                            break;
                    }
                });
                // for(int idx = 0; idx < score.BatchSize; idx++)
                // {
                //     int pos = 0;
                //     IEnumerable<int> orders = isAscending ?
                //                                     score.Output.Data.MemPtr.Skip(idx * score.Dim).Take(score.Dim).Select((item, index) => new { item, index }).OrderBy(a => a.item).Select(a => a.index)
                //                                     : score.Output.Data.MemPtr.Skip(idx * score.Dim).Take(score.Dim).Select((item, index) => new { item, index }).OrderBy(a => a.item).Select(a => a.index).Reverse();
                //     foreach(int predId in orders)
                //     {
                //         if(black[idx].Contains(predId) && label[idx] != predId)
                //         {
                //             continue;
                //         }
                //         pos += 1;
                //         if (label[idx] == predId)
                //         {
                //             AP += 1.0f / pos;
                //             if(pos <= 1) { Hit1 += 1; }
                //             if (pos <= 3) { Hit3 += 1; }
                //             if (pos <= 5) { Hit5 += 1; }
                //             if (pos <= 10) { Hit10 += 1; }
                //             if (pos <= 20) { Hit20 += 1; }
                //             break;
                //         }
                //     }
                // }
                
                result["AP"] += mAP.Sum();
                result["Hit1"] += mHit1.Sum();
                result["Hit3"] += mHit3.Sum();
                result["Hit5"] += mHit5.Sum();
                result["Hit10"] += mHit10.Sum();
                result["Hit20"] += mHit20.Sum();
                result["SMP"] += score.BatchSize;
            }


            /// HitK Sparse. 
            public static Dictionary<string, float> HitK(Dictionary<int, Tuple<List<int>, List<float>, int> > result, bool ascending)
            {
                Dictionary<string, float> report = new Dictionary<string, float>();

                float AP = 0;
                float Hit1 = 0;
                float Hit3 = 0;
                float Hit5 = 0;
                float Hit10 = 0;
                float Hit20 = 0;
                float Hited = 0;
                int LeafNode = 0;
                foreach(KeyValuePair<int, Tuple<List<int>, List<float>, int> > pair in result)
                {
                    int leafNode = pair.Value.Item1.Distinct().ToArray().Length;
                    LeafNode += leafNode;

                	IEnumerable<int> orders = ascending ?
                		pair.Value.Item2.Select((item, index) => new { item, index }).OrderBy(a => a.item).Select(a => a.index) :
                		pair.Value.Item2.Select((item, index) => new { item, index }).OrderBy(a => a.item).Select(a => a.index).Reverse();
                	
                	HashSet<int> predHash = new HashSet<int>();

                	foreach(int idx in orders)
                    {
                    	int predId = pair.Value.Item1[idx];
                        if(predHash.Contains(predId)) continue;
                        predHash.Add(predId);

                        int pos = predHash.Count;
                        
                        if (pair.Value.Item3 == predId)
                        {
                            AP += 1.0f / pos;
                            if(pos <= 1) { Hit1 += 1; }
                            if (pos <= 3) { Hit3 += 1; }
                            if (pos <= 5) { Hit5 += 1; }
                            if (pos <= 10) { Hit10 += 1; }
                            if (pos <= 20) { Hit20 += 1; }
                            Hited += 1;
                            break;
                        }
                    }
                }
                report["AP"] = AP;
                report["Hit1"] = Hit1;
                report["Hit3"] = Hit3;
                report["Hit5"] = Hit5;
                report["Hit10"] = Hit10;
                report["Hit20"] = Hit20;
                report["Hited"] = Hited;
                report["LEAF-NODE"] = LeafNode;
                return report;   
            }
        }
}