using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
        public class StatusData : BatchData
        {
            /// <summary>
            /// Raw Query.
            /// </summary>
            public GraphQueryData GraphQuery { get; set; }

            /// <summary>
            /// Node ID.
            /// </summary>
            public List<int> NodeID { get; set; }

            /// <summary>
            /// LogProbability to this node.
            /// </summary>
            List<float> LogProb { get; set; }

            /// <summary>
            /// MatchCandidate and MatchCandidateProb.
            /// </summary>
            public Tuple<List<int>, List<int>> MatchCandidate = null;

            public SeqVectorData MatchCandidateQ = null;
            
            public SeqVectorData MatchCandidateProb = null;

            public List<int> PreSelActIndex = null;
            public List<int> PreStatusIndex = null;


            public int MaxBatchSize { get { return StateEmbed.MAX_BATCHSIZE; } }
            public int BatchSize { get { return StateEmbed.BatchSize; } }

            /// <summary>
            /// Embedding of the State.
            /// </summary>
            public HiddenBatchData StateEmbed { get; set; }
            public HiddenBatchData InputEmbed { get; set; }
            public VectorData Score { get; set; }

            public int Step;
            
            
            public int GetPreStatusIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else return PreStatusIndex[batchIdx];
            }

            public int GetPreActionIndex(int batchIdx)
            {
                if(Step == 0) { throw new NotImplementedException("Current Status is zero."); }
                else return PreSelActIndex[batchIdx];
            }

            public int GetActionDim(int b)
            {
                return GetActionEndIndex(b) - GetActionStartIndex(b);
            }

            public int GetActionStartIndex(int b)
            {
                return b == 0 ? 0 : MatchCandidateProb.SegmentIdx.MemPtr[b - 1];
            }

            public int GetActionEndIndex(int b)
            {
                return MatchCandidateProb.SegmentIdx.MemPtr[b];
            }

            public Tuple<int, int, int, int> GetPreActionInfo(int batchIdx)
            {
                int pb = GetPreStatusIndex(batchIdx);
                int gactIdx = GetPreActionIndex(batchIdx);

                int actIdx = gactIdx - GraphQuery.StatusPath[Step - 1].GetActionStartIndex(pb);

                int actDim = GraphQuery.StatusPath[Step - 1].GetActionDim(pb);
                
                int nodId = GraphQuery.StatusPath[Step - 1].MatchCandidate.Item1[gactIdx];
                int relId = GraphQuery.StatusPath[Step - 1].MatchCandidate.Item2[gactIdx];

                return new Tuple<int, int, int, int>(actIdx, actDim, nodId, relId);
            }

            public float GetPreQValue(int batchIdx)
            {
                int aidx = GetPreActionIndex(batchIdx);
                StatusData pst = GraphQuery.StatusPath[Step - 1];
                return pst.MatchCandidateQ.Output.MemPtr[aidx];
            }

            public int GetOriginalStatsIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else
                {
                    int b = GetPreStatusIndex(batchIdx);
                    return GraphQuery.StatusPath[Step - 1].GetOriginalStatsIndex(b);
                }
            }

            public string GetStatusKey(int b)
            {
                Tuple<int, int, int, int>[] path = GetPath(b);
                
                int origialB = GetOriginalStatsIndex(b); 
                string mkey = string.Format("Idx:{0}--N:{1}?R:{2}", GraphQuery.RawIndex[origialB], 
                    GraphQuery.RawSource[origialB], GraphQuery.RawRel[origialB]);
                foreach(Tuple<int, int, int, int> n in path) 
                {
                    mkey = string.Format("{0}-R:{1}->N:{2}", mkey, n.Item4, n.Item3);
                }
                return mkey;
            }

            public Tuple<int, int, int, int>[] GetPath(int b)
            {
                Tuple<int, int, int, int>[] mpath = new Tuple<int, int, int, int>[Step];
                StatusData pointer = this;
                int mb = b;
                for(int i = 1; i <= Step; i++)  
                {
                    Tuple<int,int,int,int> n = pointer.GetPreActionInfo(mb);
                    mpath[Step-i] = n;

                    mb = pointer.GetPreStatusIndex(mb);
                    pointer = GraphQuery.StatusPath[Step - i];
                }
                return mpath;
            }

            /// <summary>
            /// logprobability of the node.
            /// </summary>
            /// <param name="batchIdx"></param>
            /// <returns></returns>
            public float GetLogProb(int batchIdx)
            {
                if (Step == 0) { return 0; }
                else { return LogProb[batchIdx]; }
            }

            public StatusData(GraphQueryData interData, 
                              List<int> nodeIndex, 
                              HiddenBatchData stateEmbed, 
                              DeviceType device) 
            : this(interData, nodeIndex, null, null, null, stateEmbed, device)
            { }

            public StatusData(GraphQueryData interData, 
                              List<int> nodeIndex, 
                              HiddenBatchData stateEmbed, 
                              HiddenBatchData inputEmbed,
                              DeviceType device) 
            : this(interData, nodeIndex, null, null, null, stateEmbed, inputEmbed, device)
            { }

            public StatusData(GraphQueryData interData,
                              List<int> nodeIndex, 
                              List<float> logProb, 
                              List<int> selectedAction, 
                              List<int> preStatusIndex,
                              HiddenBatchData stateEmbed, 
                              DeviceType device)
            : this(interData, nodeIndex, logProb, selectedAction, preStatusIndex, stateEmbed, null, device)
            { }

            public StatusData(GraphQueryData interData,
                              List<int> nodeIndex, 
                              List<float> logProb, 
                              List<int> selectedAction, 
                              List<int> preStatusIndex,
                              HiddenBatchData stateEmbed, 
                              HiddenBatchData inputEmbed,
                              DeviceType device)
            {
                GraphQuery = interData;
                GraphQuery.StatusPath.Add(this);

                NodeID = nodeIndex;
                StateEmbed = stateEmbed;
                InputEmbed = inputEmbed;

                LogProb = logProb;
                PreSelActIndex = selectedAction;
                PreStatusIndex = preStatusIndex; 

                Step = GraphQuery.StatusPath.Count - 1;
            }

        }
}
