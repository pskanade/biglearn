using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
        public class GraphQueryData : BatchData
        {
            public List<int> RawSource = new List<int>();
            public List<int> RawRel = new List<int>();
            public List<int> RawTarget = new List<int>();
            public List<int> RawIndex = new List<int>();
            public List<int> RawStart = new List<int>();

             // the edge already exists in KB.
            public List<List<int>> BlackTargets = new List<List<int>>();
            
            /// suppose this is a good implementation.
            /// public SparseVectorData BlackLabels = null;

            public List<Tuple<int, int, int, int>[]> GuidPath = null;
            public List<float> GuidReward = null;

            public List<StatusData> StatusPath = new List<StatusData>();

            public int BatchSize { get { return RawSource.Count; } }
            public int MaxBatchSize { get; set; }

            /// <summary>
            /// max group number, and group size;
            /// </summary>
            /// <param name="maxGroupNum"></param>
            /// <param name="groupSize"></param>
            /// <param name="device"></param>
            public GraphQueryData(int maxBatchSize, DeviceType device)
            {
                MaxBatchSize = maxBatchSize;
            }

            public List<int> GetBatchIdxs(int b, int step)
            {
                List<int> r = new List<int>();
                for (int i = 0; i < StatusPath[step].BatchSize; i++)
                {
                    if (StatusPath[step].GetOriginalStatsIndex(i) == b)
                        r.Add(i);
                }
                return r;
            }

            public void Clear()
            {
                RawSource.Clear();
                RawRel.Clear();
                RawTarget.Clear();
                RawIndex.Clear();
                RawStart.Clear();
                BlackTargets.Clear();

                if(GuidPath != null) GuidPath.Clear();
                if(GuidReward != null) GuidReward.Clear();
            }

            public void Push(int idx, int srcId, int linkId, int tgtId, List<int> blacks, int start)
            {
                RawIndex.Add(idx);
                
                RawSource.Add(srcId);
                
                RawRel.Add(linkId);
                
                RawTarget.Add(tgtId);
                BlackTargets.Add(blacks);
                RawStart.Add(start);
             }
        }
}
