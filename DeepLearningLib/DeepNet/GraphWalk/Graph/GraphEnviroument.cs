using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
        public class GraphEnviroument 
        {
            public Dictionary<int, List<Tuple<int, int>>> Graph { get; set; }
            public int MaxNeighborNum { get; set; }
            public GraphVocab GraphVoc { get; set; }
            
            public GraphEnviroument(GraphVocab graphVoc,
                                    int maxNeighborNum,
                                    string graphFile)
            {
                GraphVoc = graphVoc;
                Graph = new Dictionary<int, List<Tuple<int, int>>>();
                
                int missNum = 0;
                using (StreamReader graphReader = new StreamReader(graphFile))
                {
                    while (!graphReader.EndOfStream)
                    {
                        string[] tokens = graphReader.ReadLine().Split('\t');

                        if (!GraphVoc.EntityDict.ContainsKey(tokens[0]))
                        {
                            missNum += 1;
                            continue;
                        }

                        if (!GraphVoc.RelationDict.ContainsKey(tokens[1]))
                        {
                            missNum += 1;
                            continue;
                        }

                        if(!GraphVoc.EntityDict.ContainsKey(tokens[2]))
                        {
                            missNum += 1;
                            continue;
                        }

                        int e1 = GraphVoc.EntityDict[tokens[0]];
                        int r = GraphVoc.RelationDict[tokens[1]];
                        int e2 = GraphVoc.EntityDict[tokens[2]];

                        if (!Graph.ContainsKey(e1)) { Graph.Add(e1, new List<Tuple<int, int>>()); }

                        if (maxNeighborNum > 0 && Graph[e1].Count >= maxNeighborNum) continue;

                        Graph[e1].Add(new Tuple<int, int>(r, e2));
                    }
                    Logger.WriteLog("{0} Graph miss connections {1}", graphFile, missNum);
                }

                MaxNeighborNum = 0;
                foreach (int entity in Graph.Keys)
                {
                    if (Graph[entity].Count + 1 >= MaxNeighborNum ) { MaxNeighborNum = Graph[entity].Count + 1; }
                }
                Logger.WriteLog("Max Neighbor Number {0}", MaxNeighborNum);
            }
        }
}
