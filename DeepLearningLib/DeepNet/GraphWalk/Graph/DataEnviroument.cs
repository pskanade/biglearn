using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
        public class DataEnviroument 
        {
            public List<Tuple<int, int, int>> Triple { get; set; }
            public Dictionary<int, List<int>> TruthDict { get; set; }

            // group of source and relation : <src, rel>  
            public List<Tuple<int, int>> Grouple { get; set; }

            public GraphVocab GraphVoc { get; set; }

            public DataEnviroument(GraphVocab graphVoc, string tripleFile, string[] graphFiles)
            {
                GraphVoc = graphVoc;
                Triple = new List<Tuple<int, int, int>>();
                Grouple = new List<Tuple<int, int>>();
                TruthDict = new Dictionary<int, List<int>>();
                
                int missNum = 0;
                using (StreamReader tripleReader = new StreamReader(tripleFile))
                {
                    while (!tripleReader.EndOfStream)
                    {
                        string[] tokens = tripleReader.ReadLine().Split('\t');

                        if (!GraphVoc.EntityDict.ContainsKey(tokens[0]))
                        {
                            missNum += 1;
                            continue;
                        }

                        if (!GraphVoc.RelationDict.ContainsKey(tokens[1]))
                        {
                            missNum += 1;
                            continue;
                        }

                        if (!GraphVoc.EntityDict.ContainsKey(tokens[2]))
                        {
                            missNum += 1;
                            continue;
                        }

                        int e1 = GraphVoc.EntityDict[tokens[0]];
                        int r = GraphVoc.RelationDict[tokens[1]];
                        int e2 = GraphVoc.EntityDict[tokens[2]];

                        Triple.Add(new Tuple<int, int, int>(e1, r, e2));
                    }
                    Logger.WriteLog("{0} Graph miss connections {1}", tripleFile, missNum);
                }

                foreach (string gStr in graphFiles)
                {
                    using (StreamReader gReader = new StreamReader(gStr))
                    {
                        while (!gReader.EndOfStream)
                        {
                            string[] tokens = gReader.ReadLine().Split('\t');

                            if (!GraphVoc.EntityDict.ContainsKey(tokens[0]))
                            {
                                //Console.WriteLine("Graph doesn't exist node {0}", tokens[0]);
                                continue;
                            }

                            if (!GraphVoc.RelationDict.ContainsKey(tokens[1]))
                            {
                                //Console.WriteLine("Graph doesn't exist relation {0}", tokens[1]);
                                continue;
                            }

                            if (!GraphVoc.EntityDict.ContainsKey(tokens[2]))
                            {
                                //Console.WriteLine("Graph doesn't exist node {0}", tokens[2]);
                                continue;
                            }

                            int e1 = GraphVoc.EntityDict[tokens[0]];
                            int r = GraphVoc.RelationDict[tokens[1]];
                            int e2 = GraphVoc.EntityDict[tokens[2]];

                            int gKey = GraphVoc.Key(e1, r);

                            if (!TruthDict.ContainsKey(gKey))
                            {
                                TruthDict.Add(gKey, new List<int>());
                                Grouple.Add(new Tuple<int, int>(e1, r));
                            }
                            TruthDict[gKey].Add(e2);
                        }
                    }
                }
            }
        }
    
}
