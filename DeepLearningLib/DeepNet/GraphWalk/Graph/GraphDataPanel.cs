using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
        public class GraphDataPanel 
        {
            public static GraphEnviroument GraphEnv { get; set; }

            public static DataEnviroument TrainData { get; set; }
            public static DataEnviroument TestData { get; set; }
            public static DataEnviroument DevData { get; set; }

            public static GraphVocab GraphVoc { get; set; }

            public static void Init(string dataDir, int initMaxNeighbor)
            {
                GraphVoc = new GraphVocab(dataDir + @"/vocab/entity_vocab.json", dataDir + @"/vocab/relation_vocab.json");

                GraphEnv = new GraphEnviroument(GraphVoc, initMaxNeighbor, dataDir + @"/graph.txt");

                TrainData = new DataEnviroument(GraphVoc, dataDir + @"/train.txt",
                                            new string[] { dataDir + @"/train.txt" });

                TestData = new DataEnviroument(GraphVoc, dataDir + @"/test.txt",
                                            new string[] { dataDir + @"/test.txt",
                                                           dataDir + @"/train.txt",
                                                           dataDir + @"/dev.txt",
                                                           dataDir + @"/graph.txt" });

                DevData = new DataEnviroument(GraphVoc, dataDir + @"/dev.txt",
                                            new string[] { dataDir + @"/test.txt",
                                                           dataDir + @"/train.txt",
                                                           dataDir + @"/dev.txt",
                                                           dataDir + @"/graph.txt" });
            }

        }

        
}
