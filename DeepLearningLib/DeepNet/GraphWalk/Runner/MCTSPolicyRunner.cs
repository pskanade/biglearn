

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
        public class MCTSPolicyRunner : BasicPolicyRunner
        {
            EpisodicMemory Memory { get; set; }

            float Alpha_C { get; set; }
            float Alpha_D { get; set; }

            /// <summary>
            /// match candidate, and match probability. 
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public MCTSPolicyRunner(StatusData input, int maxNeighbor, EpisodicMemory memory, 
            						float alpha_c, 
            						float alpha_d, 
            						RunnerBehavior behavior) : base(input, behavior)
            {
                Memory = memory;

                Alpha_C = alpha_c;
                Alpha_D = alpha_d;

                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                }, behavior.Device);
            }

            public override void Forward()
            {
            	if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }

                Input.MatchCandidateProb.Output.SyncToCPU();

                NodeIndex.Clear();
                ActIndex.Clear();
                StatusIndex.Clear();
                LogProb.Clear();
                MatchPath.Clear();

                int currentStep = Input.Step;
                int batchSize = Input.MatchCandidateProb.Segment;
                
                for (int i = 0; i < batchSize; i++)
                {
                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

                    string Qkey = Input.GetStatusKey(i);
                    Tuple<float[], float[], float> arms = Memory.Search(Qkey);

                    int actionDim = e - s;
                    if(actionDim == 0)
                    	throw new Exception(string.Format(" MctsPolicyRunner: actionDim is zero, it is not acceptable. s : {0}, e: {1}", s, e));

                    if( arms != null && (actionDim != arms.Item1.Length || actionDim != arms.Item2.Length) ) 
                    {
                    	throw new Exception(string.Format("MctsPolicyRunner: actionDim doesn't match graphActDim: {0}, MemActDim1: {1}, MemActDim2: {2}, Qkey: {3}",
                                    actionDim, arms.Item1.Length, arms.Item2.Length, Qkey));
                    }

                    float[] prior_r = new float[actionDim];
                    Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);

                    int idx = BanditAlg.AlphaGoZeroBandit(arms, prior_r, actionDim, Alpha_C, Alpha_D); 
                    int selectIdx = s + idx;
                    float prob = Input.GetLogProb(i) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                    {
                        ActIndex.Add(selectIdx);
                        StatusIndex.Add(i);
                        NodeIndex.Add(Input.MatchCandidate.Item1[selectIdx]);
                        LogProb.Add(prob);
                        MatchPath.Push(i, selectIdx, 1);
                    }
                }
                MatchPath.PushDone();
            }
        }
}