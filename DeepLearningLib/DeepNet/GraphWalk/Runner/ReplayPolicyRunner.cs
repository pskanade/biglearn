using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
	public class ReplayPolicyRunner : BasicPolicyRunner
        {
            public ReplayPolicyRunner(StatusData input, int maxNeighbor, RunnerBehavior behavior) : base(input, behavior)
            {
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                }, behavior.Device);
            }
            
            // sample actions according to the replay memory.
            public override void Forward()
            {
            	if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }

                Input.MatchCandidateProb.Output.SyncToCPU();

                NodeIndex.Clear();
                ActIndex.Clear();
                StatusIndex.Clear();
                LogProb.Clear();
                MatchPath.Clear();

                int currentStep = Input.Step;
                int batchSize = Input.MatchCandidateProb.Segment;
                for (int i = 0; i < batchSize; i++)
                {
                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];
                    
                    //int actionDim = e - s;
                    int step = Input.Step;

                    int origialB = Input.GetOriginalStatsIndex(i);
                    
                    Tuple<int, int, int, int>[] path = Input.GraphQuery.GuidPath[origialB];
                    int selectIdx = s + path[step].Item1;

                    //selectIdx = s + idx;
                    float prob = Input.GetLogProb(i) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                    {
                    	NodeIndex.Add(Input.MatchCandidate.Item1[selectIdx]);
                        ActIndex.Add(selectIdx);
                        StatusIndex.Add(i);
                        LogProb.Add(prob);
                        MatchPath.Push(i, selectIdx, 1);
                    }
                }
                MatchPath.PushDone();
            }
        }
}