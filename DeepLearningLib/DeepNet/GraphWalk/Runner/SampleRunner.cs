using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
using BigLearn.DeepNet;

namespace BigLearn.DeepNet
{
		/// <summary>
        /// Sample Graph. 
        /// </summary>
        class SampleRunner : StructRunner
        {
            DataEnviroument Data { get; set; }
            public new GraphQueryData Output { get; set; }
            int MaxBatchSize { get; set; }
            int GroupSize { get; set; }
            int Roll { get; set; }
            DataRandomShuffling Shuffle { get; set; }
            Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1);
            int MiniBatchCounter = 0;
            int DebugSamples = 0;
            bool IsGrouple = false;

            public SampleRunner(DataEnviroument data, int groupSize, int roll, int debugSamples, RunnerBehavior behavior) 
                : this(data, groupSize, roll, debugSamples, false, behavior)
            { }

            public SampleRunner(DataEnviroument data, int groupSize, int roll, int debugSamples, bool isGrouple, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                GroupSize = groupSize;
                Roll = roll;
                MaxBatchSize = GroupSize * Roll;
                DebugSamples = debugSamples;
                IsGrouple = isGrouple;
                Shuffle = new DataRandomShuffling(IsGrouple ? Data.Grouple.Count : Data.Triple.Count, random);
                Output = new GraphQueryData(MaxBatchSize, behavior.Device);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
                MiniBatchCounter = 0;
            }

            public override void Forward()
            {
                Output.Clear();

                MiniBatchCounter += 1;
                if (DebugSamples > 0 && MiniBatchCounter > DebugSamples) { IsTerminate = true; return; }

                int groupIdx = 0;
                while (groupIdx < GroupSize)
                {
                    int idx = Shuffle.RandomNext(); 
                    if (idx <= -1) { break; }
                    
                    int srcId = IsGrouple ? Data.Grouple[idx].Item1 : Data.Triple[idx].Item1;
                    int linkId = IsGrouple ? Data.Grouple[idx].Item2 : Data.Triple[idx].Item2;
                    int tgtId = IsGrouple ? -1 : Data.Triple[idx].Item3;
                    List<int> truthIds = Data.TruthDict[Data.GraphVoc.Key(srcId, linkId)];
                    for (int roll = 0; roll < Roll; roll++)
                    {
                    	Output.Push(idx, srcId, linkId, tgtId, truthIds, Data.GraphVoc.START_IDX);
                    }
                    groupIdx += 1;
                }
                if (groupIdx == 0) { IsTerminate = true; return; }
            }
        }
}