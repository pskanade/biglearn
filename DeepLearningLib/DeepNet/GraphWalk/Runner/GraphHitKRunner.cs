
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
	public class GraphHitKRunner : StructRunner
    {
            GraphQueryData Query { get; set; }
            
            public static int Epoch = 0;
            
            Dictionary<int, Tuple<List<int>, List<float>, int> > Result = null;
            bool IsExternalR = false;

            // 0 : probabolity; 1 : score;
            int ScoreType { get; set; }
            
            public GraphHitKRunner(GraphQueryData query, Dictionary<int, Tuple<List<int>, List<float>, int> > r, int scoreType, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Query = query;
                Epoch = 0;
                ScoreType = scoreType;
                if(r != null) { Result = r; IsExternalR = true; }
                else { Result = new Dictionary<int, Tuple<List<int>, List<float>, int> >(); IsExternalR = false; }
            }

            public override void Init()
            {
                if(!IsExternalR) Result.Clear();
            }

            public override void Complete()
            {
            	if(Epoch % ReportPerEpoch == 0)
                {
                    Logger.WriteLog("Report in Epoch {0}", Epoch);
                    Report(Result);
                }	    
                Epoch += 1;
            }

            public static void Report(Dictionary<int, Tuple<List<int>, List<float>, int> > result)
            {
                Dictionary<string, float> r = EvalUtil.HitK(result, false);
            	foreach(KeyValuePair<string, float> p in r)
                {
                	Logger.WriteLog("{0} : {1}", p.Key, p.Value * 1.0f / result.Count);
                }
            }	

            public override void Forward()
            {
                if(ScoreType == 0)
                {
                    EvalUtil.AggregrateProb(Query, Result);
                }
                else if(ScoreType == 1)
                {
                    EvalUtil.AggregrateScore(Query, Result);
                }
            }
        }
 }