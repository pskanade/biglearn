using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
    public class AppQWalk_GRUBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.
            public static string InputDir { get { return Argument["INPUT-DIR"].Value; } }
            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }
            public static int MCTSNum { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

            public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            public static int RNN_Dim { get { return int.Parse(Argument["RNN-DIM"].Value); } }
            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] S_NET { get { return Argument["S-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] S_AF { get { return Argument["S-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }
            
            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            public static int MIN_HOP { get { return int.Parse(Argument["MIN-HOP"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static float UCB1_C { get { return float.Parse(Argument["UCB1-C"].Value); } }
            

            public static float PUCB_C { get { return float.Parse(Argument["PUCB-C"].Value); } }
            public static float PUCB_B { get { return float.Parse(Argument["PUCB-B"].Value); } }
            public static float PUCB_M { get { return float.Parse(Argument["PUCB-M"].Value); } }
            public static int MAX_ACTION_NUM { get { return int.Parse(Argument["MAX-ACTION-NUM"].Value); } }

            public static int TEST_SAMPLES { get { return int.Parse(Argument["TEST-SAMPLES"].Value); } }

            public static int DEV_AS_TRAIN { get { return int.Parse(Argument["DEV-TRAIN"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }
            public static int ActorMiniBatch { get { return int.Parse(Argument["ACTOR-MINI-BATCH"].Value); } }
            public static int ActorPolicy { get { return int.Parse(Argument["ACTOR-POLICY"].Value); } } 
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }

            public static int LearnerMiniBatch { get { return int.Parse(Argument["LEARNER-MINI-BATCH"].Value); } }
            public static int LearnerEpoch { get { return int.Parse(Argument["LEARNER-EPOCH"].Value); } }
            public static int ModelSyncUp { get { return int.Parse(Argument["MODEL-SYNCUP"].Value); } }
            public static int ReplayBufferSize { get { return int.Parse(Argument["REPLAY-BUFF-SIZE"].Value); } } 
            public static int TReplaySize { get { return int.Parse(Argument["T-REPLAY-SIZE"].Value); } } 
            public static float ReplayDecay { get { return float.Parse(Argument["REPLAY-DECAY"].Value); } }

            public static float Lambda { get { return float.Parse(Argument["LAMBDA"].Value); } }
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                #region Input Data Argument.
                Argument.Add("INPUT-DIR", new ParameterArgument(string.Empty, "dir input."));
                #endregion.

                Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
                Argument.Add("RNN-DIM", new ParameterArgument("100", "RNN hidden state dim."));

                Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));
                //Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                //Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));
                Argument.Add("S-NET", new ParameterArgument("1", "Score Network."));
                Argument.Add("S-AF", new ParameterArgument(((int)A_Func.Rectified).ToString(), "Scoring Network AF."));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));


                Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));
                Argument.Add("MIN-HOP", new ParameterArgument("2", "minimum number of hops"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

                Argument.Add("SCORE-TYPE", new ParameterArgument("0", "0:probability; 1:pesudo reward;"));
                Argument.Add("ROLL-NUM", new ParameterArgument("1", "roll number"));

                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));
                Argument.Add("MCTS-NUM", new ParameterArgument("0", "Monto Carlo Tree Search Number"));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.99", "Reward discount"));
                Argument.Add("REWARD-MCTS-DISCOUNT", new ParameterArgument("0.8", "mcts reward discount."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument("model/", "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("./", "Output Score File."));

                Argument.Add("TEST-MCTS-NUM", new ParameterArgument("64", "Pre Monto Carlo Tree Search Number"));
                

                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));
                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("SCORE-LAMBDA", new ParameterArgument("1.0", "Score lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0", "Prob lambda in scoring function."));

                Argument.Add("MCTS-EXP", new ParameterArgument("8,16,24,48,64", "Exploration strategy."));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1", "mcts reward discount."));
                Argument.Add("MAX-ACTION-NUM", new ParameterArgument("0", "Default max action number."));

                Argument.Add("EVALUATION-TOOL", new ParameterArgument(string.Empty, "Evaluation path tool."));

                Argument.Add("AGG-SCORE", new ParameterArgument("0", "0:enumate; 1:max pool; 2:sum pool;"));
                Argument.Add("MAP-EVAL", new ParameterArgument("1", "0:no map eval; 1:mapeval;"));
                Argument.Add("BETA", new ParameterArgument("1", "moving average baseline estimation."));
                Argument.Add("ALPHA-1", new ParameterArgument("0","max entropy for termination gate."));
                Argument.Add("ALPHA-2", new ParameterArgument("0", "max entropy for action gate."));

                Argument.Add("TRAIN-STR", new ParameterArgument("0","0:standard train strategy; 1:mcts train."));
                Argument.Add("TRAIN-L", new ParameterArgument("1", "train random lambda"));
                Argument.Add("TRAIN-C", new ParameterArgument("1", "train self lambda."));
                Argument.Add("TEST-L", new ParameterArgument("1", "test random lambda."));
                Argument.Add("TEST-C", new ParameterArgument("1", "test self lambda."));

                Argument.Add("TRAIN-SAMPLES", new ParameterArgument("0", "training samples per epoch."));
                Argument.Add("TEST-SAMPLES", new ParameterArgument("0", "testing samples per epoch."));

                Argument.Add("STEP-FILTER", new ParameterArgument("0", "step filter"));
                Argument.Add("DEV-TRAIN", new ParameterArgument("0", "use dev set as training set."));


                Argument.Add("ACTOR-MINI-BATCH", new ParameterArgument("128", "actor mini batch size"));
                Argument.Add("ACTOR-POLICY", new ParameterArgument("4", "0:UnifiedSampling; 1:ThompasSampling; 2:UCB; 3:AlphaGoZero1; 4:AlphaGoZero2; 5:PUCB"));

                Argument.Add("LEARNER-MINI-BATCH", new ParameterArgument("32", "learner mini batch size"));
                Argument.Add("LEARNER-EPOCH", new ParameterArgument("5", "learner training epoch size"));
                Argument.Add("MODEL-SYNCUP", new ParameterArgument("3000", "model syncup iteration."));   
                Argument.Add("REPLAY-BUFF-SIZE", new ParameterArgument("3000000", "buffer size for the replay memory."));   
                Argument.Add("T-REPLAY-SIZE", new ParameterArgument("5000", "threshold of replay buffer size."));   
                Argument.Add("REPLAY-DECAY", new ParameterArgument("0.99995", "Replay Decay Value."));   
                Argument.Add("LAMBDA", new ParameterArgument("0.01", "lambda for Q learning."));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_QWALK_GRU; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }        

        static Random SampleRandom = new Random(DeepNet.BuilderParameters.RandomSeed);

        public class NeuralWalkerModel : CompositeNNStructure
        {
            public EmbedStructure srcNodeEmbed { get; set; }
            public EmbedStructure srcRelEmbed { get; set; }
            
            public EmbedStructure tgtNodeEmbed { get; set; }
            public EmbedStructure tgtRelEmbed { get; set; }
            
            public DNNStructure SrcDNN { get; set; }
            public GRUCell GruCell { get; set; }
            //public DNNStructure ScoreDNN { get; set; }

            public NeuralWalkerModel(int entityNum, int relationNum, int nodeDim, int relDim, int rnnDim, DeviceType device)
            {
                srcNodeEmbed = AddLayer(new EmbedStructure(entityNum, nodeDim, device)); 
                srcRelEmbed = AddLayer(new EmbedStructure(relationNum, relDim, device)); 

                tgtNodeEmbed = AddLayer(new EmbedStructure(entityNum, nodeDim, device)); 
                tgtRelEmbed = AddLayer(new EmbedStructure(relationNum, relDim, device)); 

                int embedDim = nodeDim ; //+ relDim;
                GruCell = AddLayer(new GRUCell(embedDim, rnnDim, device));

                int inputDim = rnnDim + embedDim;

                List<int> srcLayers = new List<int>(BuilderParameters.DNN_DIMS);
                srcLayers.Add(embedDim);

                SrcDNN = AddLayer(new DNNStructure(inputDim, srcLayers.ToArray(),
                                           srcLayers.Select(i => A_Func.Tanh).ToArray(),
                                           srcLayers.Select(i => true).ToArray(),
                                           device));
                
                //List<int> scoreLayers = new List<int>(BuilderParameters.S_NET);
                //srcLayers.Add(1);
                //List<A_Func> scoreAfs = new List<A_Func>(BuilderParameters.S_AF);
                //scoreAfs.Add(A_Func.Linear);
                //ScoreDNN = AddLayer(new DNNStructure(inputDim, scoreLayers.ToArray(),
                //                    scoreAfs.ToArray(), scoreLayers.Select( i => true).ToArray(), device));
            }
            
            public override void CopyFrom(IData other) 
            {
                NeuralWalkerModel src = (NeuralWalkerModel)other;

                srcNodeEmbed.CopyFrom(src.srcNodeEmbed);
                srcRelEmbed.CopyFrom(src.srcRelEmbed);
                
                tgtNodeEmbed.CopyFrom(src.tgtNodeEmbed);
                tgtRelEmbed.CopyFrom(src.tgtRelEmbed);
                
                SrcDNN.CopyFrom(src.SrcDNN);   
                GruCell.CopyFrom(src.GruCell);
                //ScoreDNN.CopyFrom(src.ScoreDNN);
            }


            public NeuralWalkerModel(BinaryReader reader, DeviceType device)
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

                srcNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                srcRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                
                tgtNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                tgtRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                
                GruCell = (GRUCell)DeserializeNextModel(reader, device);
                
                SrcDNN = (DNNStructure)DeserializeNextModel(reader, device);
                
                //ScoreDNN = (DNNStructure)DeserializeNextModel(reader, device);
            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        // we need to keep the 
        public static void ReinforceWalkModel(ComputationGraph cg, 
                                  GraphQueryData graphQuery, 
                                  GraphEnviroument graph, 
                                  int policyId, 
                                  int maxHop, 
                                  //EpisodicMemory mem, 
                                  NeuralWalkerModel model, 
                                  RunnerBehavior Behavior)
        {
            HiddenBatchData srcEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawSource, graphQuery.MaxBatchSize, 
                                                                                             model.srcNodeEmbed, Behavior));
            
            HiddenBatchData relEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawRel, graphQuery.MaxBatchSize, 
                                                                                             model.srcRelEmbed, Behavior));
            
            //if(Behavior.RunMode == DNNRunMode.Train)
            //    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(srcEmbed, 0.2f, Behavior));
            //if(Behavior.RunMode == DNNRunMode.Train)
            //    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(relEmbed, 0.2f, Behavior));

            // concate the [srcEmbed, queryEmbed] as the initial state.
            //HiddenBatchData queryEmbed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { srcEmbed, relEmbed }, Behavior));
            HiddenBatchData queryEmbed = (HiddenBatchData)cg.AddRunner(new AdditionRunner(srcEmbed, relEmbed, Behavior));

            IntArgument batchSizeArg = new IntArgument("sample-batch-size"); 
            cg.AddRunner(new HiddenDataBatchSizeRunner(queryEmbed, batchSizeArg, Behavior));

            HiddenBatchData initZeroH0 = (HiddenBatchData)cg.AddRunner(new ZeroMatrixRunner(queryEmbed.MAX_BATCHSIZE, model.GruCell.HiddenStateDim, batchSizeArg, Behavior));
            GRUStateRunner initStateRunner = new GRUStateRunner(model.GruCell, initZeroH0, queryEmbed, Behavior);
            cg.AddRunner(initStateRunner);

            HiddenBatchData H1 = initStateRunner.Output;
            StatusData status = new StatusData(graphQuery, graphQuery.RawSource, H1, Behavior.Device);

            HiddenBatchData tgtEmbed = srcEmbed;

            #region multi-hop expansion
            // travel four steps in the knowledge graph.
            for (int i = 0; i < maxHop; i++)
            {
                CandidateRunner candidateActionRunner = new CandidateRunner(status, graph, Behavior);
                cg.AddRunner(candidateActionRunner);
                status.MatchCandidate = candidateActionRunner.Output;

                HiddenBatchData candNodeEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(candidateActionRunner.Output.Item1, 
                                                                        candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, 
                                                                        model.tgtNodeEmbed, Behavior));

                HiddenBatchData candRelEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(candidateActionRunner.Output.Item2, 
                                                                        candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, 
                                                                        model.tgtRelEmbed, Behavior));

                //if(Behavior.RunMode == DNNRunMode.Train)
                //    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(candNodeEmbed, 0.2f, Behavior));
                //if(Behavior.RunMode == DNNRunMode.Train)
                //    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(candRelEmbed, 0.2f, Behavior));

                HiddenBatchData candEmbed = (HiddenBatchData)cg.AddRunner(new AdditionRunner(candNodeEmbed, candRelEmbed, Behavior));

                //HiddenBatchData candEmbed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>()
                //                                                        { candNodeEmbed, candRelEmbed }, Behavior));
                HiddenBatchData I_data = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>()
                                            { status.StateEmbed, queryEmbed }, Behavior));

                DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, I_data, Behavior);
                cg.AddRunner(srcHiddenRunner);

                MatrixInnerProductRunner innerRunner = new MatrixInnerProductRunner(new MatrixData(srcHiddenRunner.Output),
                                                                                    new MatrixData(candEmbed), candidateActionRunner.Match, Behavior);
                cg.AddRunner(innerRunner);

                //ActivationRunner normQRunner = new ActivationRunner(new HiddenBatchData(innerRunner.Output), A_Func.Tanh, Behavior);
                //cg.AddRunner(normQRunner);

                status.MatchCandidateQ = new SeqVectorData(innerRunner.Output.MaxLength,
                                                           status.MaxBatchSize, normQRunner.Output.Output.Data, normQRunner.Output.Deriv.Data,
                                                           candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx, Behavior.Device);

                SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(status.MatchCandidateQ, 1, Behavior, true)
                    { 
                        IsBackProp = false,
                        IsUpdate = false
                    };
                cg.AddRunner(normAttRunner);
                status.MatchCandidateProb = normAttRunner.Output;
                
                BasicPolicyRunner policyRunner = null;
                // mcts policy.
                if(policyId == 0)
                {
                    policyRunner = new MCTSPolicyRunner(status, graph.MaxNeighborNum, Behavior);
                }
                // replay policy.
                else if(policyId == 1)
                {
                    //policyRunner = new ReplayPolicyRunner(status, graph.MaxNeighborNum, Behavior);
                }
                // beam search policy.
                //else if(policyId == 2)
                //{
                //    policyRunner = new BeamSearchActionRunner(status, graph.MaxNeighborNum, beamSize, Behavior);
                //}
                cg.AddRunner(policyRunner);

                #region take action.

                        MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
                        cg.AddRunner(srcExpRunner);

                        MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbed, policyRunner.MatchPath, 2, Behavior);
                        cg.AddRunner(tgtExpRunner);

                        MatrixExpansionRunner rQExpRunner = new MatrixExpansionRunner(Rq, policyRunner.MatchPath, 1, Behavior);
                        cg.AddRunner(rQExpRunner);
                        Rq = rQExpRunner.Output;

                        GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
                        cg.AddRunner(stateRunner);


                //MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbed, policyRunner.MatchPath, 2, Behavior);
                HiddenBatchData selCandEmbed = (HiddenBatchData)cg.AddRunner(new MatrixExpansionRunner(candEmbed, policyRunner.MatchPath, 2, Behavior));

                tgtEmbed = (HiddenBatchData)cg.AddRunner(new MatrixExpansionRunner(candNodeEmbed, policyRunner.MatchPath, 2, Behavior));
                
                //MatrixExpansionRunner rQExpRunner = new MatrixExpansionRunner(Rq, policyRunner.MatchPath, 1, Behavior);
                //cg.AddRunner(rQExpRunner);
                //Rq = rQExpRunner.Output;
                GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, status.StateEmbed, selCandEmbed, Behavior);
                cg.AddRunner(stateRunner);
                #endregion.

                StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.LogProb,
                    policyRunner.ActIndex, policyRunner.StatusIndex, stateRunner.Output, Behavior.Device);

                status = nextStatus;
            }

            MatrixInnerProductRunner scoreRunner = new MatrixInnerProductRunner(new MatrixData(queryEmbed), new MatrixData(tgtEmbed), null, Behavior);
            cg.AddRunner(scoreRunner);

            ActivationRunner normScoreRunner = new ActivationRunner(new HiddenBatchData(scoreRunner.Output), A_Func.Tanh, Behavior);
            cg.AddRunner(normScoreRunner);
            status.Score = new VectorData(normScoreRunner.Output);
            #endregion.
        }

        // actor model.
        public static void ActorModel(ComputationGraph cg, DataEnviroument data, GraphEnviroument graph, int maxHop,
                                                EpisodicMemory mem, NeuralWalkerModel model, RunnerBehavior Behavior)
        {
            // sample a minibatch from data.
            SampleRunner SmpRunner = new SampleRunner(data, BuilderParameters.ActorMiniBatch, 1, 0, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;

            // mcts policy.
            ReinforceWalkModel(cg, interface_data, graph, 0, maxHop, mem, model, Behavior);

            RollBackRunner rollbackRunner = new RollBackRunner(interface_data, mem, Behavior);
            cg.AddRunner(rollbackRunner);

            cg.SetDelegateModel(model);
        }

        // learner model.
        public static void LearnerModel(ComputationGraph cg, DataEnviroument data,  GraphEnviroument graph, int maxHop,
                                                EpisodicMemory mem, NeuralWalkerModel model, RunnerBehavior Behavior)
        {
            ReplayMemRunner replayRunner = new ReplayMemRunner(mem, data, BuilderParameters.LearnerMiniBatch, BuilderParameters.LearnerEpoch, Behavior);
            cg.AddDataRunner(replayRunner);
            GraphQueryData interface_data = replayRunner.Output;
            
            ReinforceWalkModel(cg, interface_data, graph, 1, maxHop, mem, model, Behavior);

            HiddenBatchData vData = new HiddenBatchData(interface_data.StatusPath.Last().Score); 
            MSERunner mseRunner = new MSERunner(interface_data.GuidReward, vData, 0, false, Behavior);
            cg.AddObjective(mseRunner);

            //CrossEntropyRunner ceRunner = new CrossEntropyRunner(interface_data.GuidReward, vData, Behavior); 
            //cg.AddObjective(ceRunner);

            QLearnRunner qRunner = new QLearnRunner(interface_data, BuilderParameters.REWARD_DISCOUNT, BuilderParameters.Lambda, Behavior);
            cg.AddObjective(qRunner);

            cg.SetDelegateModel(model);
        }

        public static void TestBeamModel(ComputationGraph cg, DataEnviroument data, GraphEnviroument graph, 
            int maxHop, EpisodicMemory mem, Dictionary<int, Tuple<List<int>, List<float>, int> > results, NeuralWalkerModel model, RunnerBehavior Behavior) 
        {
            SampleRunner smpRunner = new SampleRunner(data, BuilderParameters.TestMiniBatchSize, 1, BuilderParameters.TEST_SAMPLES, Behavior);
            cg.AddDataRunner(smpRunner);
            GraphQueryData interface_data = smpRunner.Output;

            ReinforceWalkModel(cg, interface_data, graph, 0, maxHop, mem, model, Behavior);
            
            RollBackRunner rollbackRunner = new RollBackRunner(interface_data, mem, Behavior);
            rollbackRunner.ReportPerEpoch = 200;
            cg.AddRunner(rollbackRunner);

            // aggregate last step q value;
            GraphHitKRunner hitkRunner = new GraphHitKRunner(interface_data, results, 1, Behavior);
            hitkRunner.ReportPerEpoch = 200;
            cg.AddRunner(hitkRunner);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation/Test Data.");
            GraphDataPanel.Init(BuilderParameters.InputDir, BuilderParameters.MAX_ACTION_NUM);
            Logger.WriteLog("Load Data Finished.");

            NeuralWalkerModel model =
                BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
                new NeuralWalkerModel(GraphDataPanel.GraphVoc.EntityNum, GraphDataPanel.GraphVoc.RelationNum, 
                                      BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, BuilderParameters.RNN_Dim, device) :
                new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
            model.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

            Logger.WriteLog("Model Parameter Number {0}", model.ParamNumber);

            EpisodicMemory Memory = new EpisodicMemory(true, 
                                                        BuilderParameters.ReplayBufferSize,
                                                        BuilderParameters.ReplayDecay,
                                                        BuilderParameters.REWARD_DISCOUNT,
                                                        BuilderParameters.TReplaySize);
            
            EpisodicMemory TestMemory = new EpisodicMemory(false,
                                                        BuilderParameters.ReplayBufferSize,
                                                        BuilderParameters.ReplayDecay,
                                                        BuilderParameters.REWARD_DISCOUNT,
                                                        BuilderParameters.TReplaySize);


            ComputationGraph testCG = new ComputationGraph();
            ComputationGraph validCG = new ComputationGraph();
            ComputationGraph actorCG = new ComputationGraph();
            ComputationGraph learnerCG = new ComputationGraph();

            Dictionary<int, Tuple<List<int>, List<float>, int> > results = new Dictionary<int, Tuple<List<int>, List<float>, int> >();
            //Dictionary<int, int> AggTgt = new Dictionary<int, int>();
            // if(BuilderParameters.BeamSize > 0 && BuilderParameters.MCTSNum == 0)
            // {
            //     TestBSModel(testCG, DataPanel.TestData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize, BuilderParameters.BeamSize,
            //         BuilderParameters.MAX_HOP, Memory, AggScore, AggTgt, model, 
            //         new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
            //     TestBSModel(validCG, DataPanel.DevData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize, BuilderParameters.BeamSize,
            //         BuilderParameters.MAX_HOP, Memory, AggScore, AggTgt, model, 
            //         new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
            // }
            // else if(BuilderParameters.BeamSize == 0 && BuilderParameters.MCTSNum > 0)
            {
                TestMCTSModel(testCG, GraphDataPanel.TestData, GraphDataPanel.GraphEnv,
                    BuilderParameters.MAX_HOP, TestMemory, results, model, 
                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
             
                TestMCTSModel(validCG, GraphDataPanel.DevData, GraphDataPanel.GraphEnv, 
                    BuilderParameters.MAX_HOP, TestMemory, results, model, 
                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });   
            }
            // else
            // {
            //     throw new Exception("only one of BeamSize and MCTSNum should be non zero.");
            // }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    NeuralWalkerModel backupModel = new NeuralWalkerModel(GraphDataPanel.GraphVoc.EntityNum, GraphDataPanel.GraphVoc.RelationNum, 
                                      BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, BuilderParameters.RNN_Dim, device);
                    backupModel.CopyFrom(model);
                    Logger.WriteLog("Generate backup Model.");

                    DataEnviroument trainData = BuilderParameters.DEV_AS_TRAIN > 0 ? GraphDataPanel.DevData : GraphDataPanel.TrainData;

                    ActorModel(actorCG, trainData,
                            GraphDataPanel.GraphEnv, BuilderParameters.MAX_HOP, Memory, backupModel,
                            new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
            
                    LearnerModel(learnerCG, trainData,  
                            GraphDataPanel.GraphEnv, BuilderParameters.MAX_HOP, Memory, model, 
                            new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    actorCG.Init();
                    learnerCG.IsReportObjectives = false;
                    
                    int actorEpoch = 0;

                    for(int iter = 1; iter <= OptimizerParameters.Iteration; iter++)
                    {
                        // actor model collect the data into replay memory.
                        if(!actorCG.Forward())
                        {
                            actorCG.Complete();
                            actorEpoch += 1;
                            actorCG.Init();
                            Logger.WriteLog("actor CG restart iteration : {0}; epoch {1}", iter, actorEpoch);
                            continue;
                        }

                        if(Memory.IsReplayBufferReady)
                        {
                            learnerCG.Execute();
                        }

                        if( iter % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            Logger.WriteLog("Evaluation at Iteration {0}", iter);

                            results.Clear();
                            TestMemory.Clear();
                            GraphHitKRunner.Epoch = 0;
                            for(int i = 0; i <= BuilderParameters.MCTSNum; i++)
                            {
                                if( i % 200 == 0) Logger.WriteLog("Test evaluation at mcts {0} .....", i );
                                testCG.Execute();
                            }

                            results.Clear();
                            TestMemory.Clear();
                            GraphHitKRunner.Epoch = 0;
                            for(int i=0; i <= BuilderParameters.MCTSNum; i++)
                            {
                                if( i % 200 == 0) Logger.WriteLog("Valid evaluation at mcts {0} .....", i );
                                validCG.Execute();
                            }
                        }
                        if(iter % BuilderParameters.ModelSyncUp == 0)
                        {
                            Logger.WriteLog("Model backup at Iteration {0}", iter);
                            backupModel.CopyFrom(model);
                        }
                
                    }
                    break;
                // case DNNRunMode.Predict:
                //     AggScore.Clear();
                //     AggTgt.Clear();
                //     TestMemory.Clear();
                //     for(int i=0; i <= BuilderParameters.MCTSNum; i++)
                //     {
                //         testCG.Execute();
                //         if( i % 500 == 0)
                //         {
                //             Logger.WriteLog("Test evaluation at mcts {0} .....", i );
                //             Dictionary<string, float> result = DataUtil.HitK(AggScore, AggTgt);
                //             foreach(KeyValuePair<string, float> p in result)
                //             {
                //                 Logger.WriteLog("{0} : {1}", p.Key, p.Value * 1.0f / AggTgt.Count);
                //             }
                //         }
                //     }
                //     AggScore.Clear();
                //     AggTgt.Clear();
                //     TestMemory.Clear();
                //     for(int i=0; i <= BuilderParameters.MCTSNum; i++)
                //     {
                //         validCG.Execute();
                //         if( i % 500 == 0)
                //         {
                //             Logger.WriteLog("Valid evaluation at mcts {0} .....", i );
                //             Dictionary<string, float> result = DataUtil.HitK(AggScore, AggTgt);
                //             foreach(KeyValuePair<string, float> p in result)
                //             {
                //                 Logger.WriteLog("{0} : {1}", p.Key, p.Value * 1.0f / AggTgt.Count);
                //             }
                //         }
                //     }
                //     break;
            }    
            Logger.CloseLog();
        }

    }
}
