using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
namespace BigLearn.DeepNet
{
	public class QLearnRunner : ObjectiveRunner
        {
            new GraphQueryData Input { get; set; } 
            
            float[] Q_MAE = null;
            float[] Q_Value = null;
            int Epoch = 0;
            
            float RewardDiscount = 0.95f;

            float TrueReward = 0;
            float EstReward = 0;
            int SmpNum = 0;
            float Wei = 0;
            public QLearnRunner(GraphQueryData input, float rewardDiscount, float wei, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                name = "QLearnRunner";
                Input = input;
                RewardDiscount = rewardDiscount;

                SmpNum = 0;
                TrueReward = 0;
                EstReward = 0;
                Q_MAE = new float[Input.StatusPath.Count]; 
                Q_Value = new float[Input.StatusPath.Count];
                Epoch = 0;
                Wei = wei;
            }

            public override void Complete()
            {
                Epoch += 1;
                if(Epoch % 200 == 0)
                {
                    Logger.WriteLog("{0} Epoch : {1}; Avg True Reward {2}; Avg Est Reward {3}; ", name, Epoch, 
                                                        TrueReward * 1.0f / SmpNum, EstReward * 1.0f / SmpNum);

                    for(int i = 0; i < Q_MAE.Length; i++)
                    {
                        Logger.WriteLog("Avg Q Step : {0}; Value {1}; MAE {2};", i, Q_Value[i] * 1.0f / SmpNum, Q_MAE[i] * 1.0f / SmpNum);
                    }
                    SmpNum = 0;
                    TrueReward = 0;
                    EstReward = 0;
                    Array.Clear(Q_Value, 0, Q_Value.Length);
                    Array.Clear(Q_MAE, 0, Q_MAE.Length);
                }
            }

            public override void Forward()
            {
                float total_reward = 0;
                {
                    for (int i = 0; i < Input.StatusPath.Count ; i++)
                    {
                        StatusData cst = Input.StatusPath[i];
                        if (cst.MatchCandidateQ != null) 
                        {
                            cst.MatchCandidateQ.Deriv.SyncToCPU();
                            cst.MatchCandidateQ.Output.SyncToCPU();
                        }

                        if(cst.Score != null) cst.Score.Output.SyncToCPU();
                    }
                }

                StatusData st = Input.StatusPath.Last();
                {
                    for (int i = 0; i < st.BatchSize; i++)
                    {
                        int t = st.Step;
                        int b = i;
                        int predId = st.NodeID[b];
                        int origialB = st.GetOriginalStatsIndex(b);
                        int targetId = Input.RawTarget[origialB];
                        int rawIdx = Input.RawIndex[origialB];
                        float reward = Input.GuidReward[origialB];

                        total_reward += reward;

                        //SmpNum += 1;
                        {
                            float est_r = st.Score.Output.MemPtr[b];
                            EstReward += est_r;

                            // estimated v;  
                            float y = RewardDiscount * est_r;

                            StatusData cst = st;
                            for (int pt = t - 1; pt >= 0; pt--)
                            {
                                int pb = cst.GetPreStatusIndex(b);
                                int sidx = cst.GetPreActionIndex(b); 

                                StatusData pst = Input.StatusPath[pt];

                                // calculate the average Q value for each step.
                                float q = pst.MatchCandidateQ.Output.MemPtr[sidx];
                                Q_Value[pt] = Q_Value[pt] + q;
                                Q_MAE[pt] = Q_MAE[pt] + Math.Abs(y - q);
                                
                                pst.MatchCandidateQ.Deriv.MemPtr[sidx] += (y - q) * Wei;// * 1.0f / Input.Count;
                                
                                int e = pst.MatchCandidateQ.SegmentIdx.MemPtr[pb];
                                int s = pb == 0 ? 0 : pst.MatchCandidateQ.SegmentIdx.MemPtr[pb - 1];
                                int maxQidx = Util.MaximumValue(pst.MatchCandidateQ.Output.MemPtr, s, e);
                                y = RewardDiscount * pst.MatchCandidateQ.Output.MemPtr[maxQidx];

                                cst = pst;
                                b = pb;
                            }
                        }
                    }
                    SmpNum += st.BatchSize;
                    TrueReward += total_reward;
                }
                
                ObjectiveScore = total_reward / (st.BatchSize + float.Epsilon);
                {
                    for (int i = 0; i < Input.StatusPath.Count ; i++)
                    {
                        StatusData cst = Input.StatusPath[i];
                        if (cst.MatchCandidateQ != null) cst.MatchCandidateQ.Deriv.SyncFromCPU();
                    }
                }

            }
        }
}