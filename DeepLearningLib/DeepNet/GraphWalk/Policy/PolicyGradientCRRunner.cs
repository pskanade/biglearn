using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
namespace BigLearn.DeepNet
{
		public class PolicyGradientCRRunner : ObjectiveRunner
        {
            //Random random = new Random();
            new GraphQueryData Input { get; set; }
            int Epoch = 0;
            int totalBatchNum = 0;
            
            float Entropy = 0; 
            float Discount = 0;

            CudaPieceFloat LogProb { get; set; }
            int Roll { get; set; }
            float Wei { get; set; }
            public override void Init()
            {
            }

            public override void Complete()
            {
                Epoch += 1;
                float alpha_entropy = Entropy * (float)Math.Pow(0.995f, totalBatchNum / 200);
                Logger.WriteLog("Entropy Term {0}", alpha_entropy);
            }

            public PolicyGradientCRRunner(GraphQueryData input, float entropy, float discount, float wei, CudaPieceFloat logProb, int roll, RunnerBehavior behavior) 
                : base(Structure.Empty, behavior)
            {
                Input = input;
                Entropy = entropy;
                Discount = discount;
                LogProb = logProb;
                Roll = roll;
                Wei = wei;
            }

            public override void Forward()
            {
                 ObjectiveDict.Clear();
                
                 float avg_act_prob = 0;
                for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
                {
                    if (Input.StatusPath[t].MatchCandidateProb != null)
                        Input.StatusPath[t].MatchCandidateProb.Deriv.SyncToCPU();
                }
                LogProb.SyncToCPU();
                
                {
                	StatusData st = Input.StatusPath.Last();
                    int t = st.Step;

                    for (int g = 0; g < st.BatchSize / Roll; g++)
                    {
                        float logSum = Util.LogSum(LogProb.MemPtr.Skip(g * Roll), Roll);
                        for(int i = 0; i < Roll; i++)
                        {
                            int b = g * Roll + i;
                            int origialB = st.GetOriginalStatsIndex(b);
                            int predId = st.NodeID[b];
                            float neg_likihood = st.GetLogProb(b);
                            
                            avg_act_prob += neg_likihood;

                            int rawIdx = Input.RawIndex[origialB];

                            float reward = (float)(Math.Exp(LogProb.MemPtr[b] - logSum) - 1.0f / Roll) * Wei;

                            StatusData cst = st;
                            for (int pt = t - 1; pt >= 0; pt--)
                            {
                                StatusData pst = Input.StatusPath[pt];
                                int pb = cst.GetPreStatusIndex(b);
                                int sidx = cst.PreSelActIndex[b];

                                float discount = (float)Math.Pow(Discount, t - pt);
                                pst.MatchCandidateProb.Deriv.MemPtr[sidx] += discount * reward;
                                
                                cst = pst;
                                b = pb;
                            }
                        }
                    }
                    ObjectiveScore = avg_act_prob * 1.0f / st.BatchSize;
                }
                

                totalBatchNum += 1;
                float alpha_entropy = Entropy * (float)Math.Pow(0.995f, totalBatchNum / 200);

                {
                    for (int i = Input.StatusPath.Count - 1; i >= 0; i--)
                    {
                        StatusData st = Input.StatusPath[i];
                        if (st.MatchCandidateProb != null)
                        {
                            st.MatchCandidateProb.Deriv.SyncFromCPU();
                            ComputeLib.DerivLogProbEntropy(st.MatchCandidateProb.SegmentIdx, st.MatchCandidateProb.Output,
                                st.MatchCandidateProb.Deriv, 1, alpha_entropy, Util.LargeEpsilon, st.MatchCandidateProb.Segment);
                        }
                        //if (st.Score != null) st.Score.Deriv.SyncFromCPU();
                    }
                    //Input[p].Results.Clear();
                }
            }
        }
}
