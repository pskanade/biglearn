using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
        public class BanditAlg 
        {
        	public static int AlphaGoZeroBandit(Tuple<float[], float[], float> arms, float[] prior, int dim, float alpha_c, float alpha_d)
            {
            	if(arms == null) return Util.MaximumValue(prior);

                float totalVisit = (float)arms.Item2.Sum() * arms.Item3;
                float totalReward = (float)arms.Item1.Sum() * arms.Item3;
                float avgQ = totalReward * 1.0f / totalVisit;
                float maxV = -100000000;
                int maxI = -1;

                for (int i = 0; i < dim; i++)
                {
                    double q = arms.Item2[i] == 0 ? 0 : arms.Item1[i] * 1.0f / arms.Item2[i];
                    double u = alpha_c * Math.Pow(prior[i], alpha_d) * Math.Sqrt(totalVisit) / (arms.Item2[i] * arms.Item3 + 1.0f);
                    float s = (float)(q + u);
                    if (s > maxV)
                    {
                        maxV = s;
                        maxI = i;
                    }
                }
                return maxI;
            }

        }
}
