using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
namespace BigLearn.DeepNet
{
	public class PiLearnRunner : ObjectiveRunner
        {
            new GraphQueryData Input { get; set; } 
            EpisodicMemory Mem { get; set; }
            int SmpNum = 0;
            float[] Pi_Value = null;
            int Epoch = 0;
            float Wei { get; set; }

            public PiLearnRunner(GraphQueryData input, EpisodicMemory mem, float wei, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                name = "PiLearnRunner";
                Input = input;
                Mem = mem;
                SmpNum = 0;
                Pi_Value = new float[Input.StatusPath.Count]; 
                Epoch = 0;
                Wei = wei;
            }

            public override void Complete()
            {
                Epoch += 1;
                if(Epoch % 200 == 0)
                {
                    Logger.WriteLog("{0} Epoch : {0}", name, Epoch);
                    for(int i = 0; i < Pi_Value.Length; i++)
                    {
                        Logger.WriteLog("Avg Pi Step : {0}; Value {1};", i, Pi_Value[i] * 1.0f / SmpNum );
                    }
                    SmpNum = 0;
                    Array.Clear(Pi_Value, 0, Pi_Value.Length);
                }
            }

            public override void Forward()
            {
                float total_reward = 0;

                for (int i = 0; i < Input.StatusPath.Count ; i++)
                {
                    StatusData st = Input.StatusPath[i];
                    if (st.MatchCandidateProb != null)
                    { 
                        st.MatchCandidateProb.Output.SyncToCPU();
                        st.MatchCandidateProb.Deriv.SyncToCPU();
                    }
                }
                
                float beta = 0.2f;
                // mcts sampling.
                {
                	StatusData st = Input.StatusPath.Last();
                	int t = st.Step;

                    for (int i = 0; i < st.BatchSize; i++)
                    {
                        int b = i;
                        int predId = st.NodeID[b];
                        
                        int origialB = st.GetOriginalStatsIndex(b);

                        int targetId = Input.RawTarget[origialB];
                        int rawIdx = Input.RawIndex[origialB];
                        float reward = Input.GuidReward[origialB];

                        total_reward += reward;
                        {
                            float y = reward;
                            StatusData cst = st;
                            for (int pt = t - 1; pt >= 0; pt--)
                            {
                                int pb = cst.GetPreStatusIndex(b);
                                int sidx = cst.GetPreActionIndex(b); 

                                StatusData pst = Input.StatusPath[pt];
                                Tuple<float[], float[], float> arms = Mem.Search(pst.GetStatusKey(pb));

                                int e = pst.MatchCandidateProb.SegmentIdx.MemPtr[pb];
                                int s = pb == 0 ? 0 : pst.MatchCandidateProb.SegmentIdx.MemPtr[pb - 1];
                                
                                float sum = arms.Item2.Sum() * arms.Item3 + (e - s) * beta;

                                for(int x = s; x < e; x++)
                                {
                                    pst.MatchCandidateProb.Deriv.MemPtr[x] += (arms.Item2[x - s] * arms.Item3 + beta ) * Wei;
                                }
                                // calculate the average Q value for each step.
                                float pi = pst.MatchCandidateProb.Output.MemPtr[sidx];
                                Pi_Value[pt] = Pi_Value[pt] + pi;
                                
                                cst = pst;
                                b = pb;
                            }
                        }
                    }
                    SmpNum += st.BatchSize;
                    ObjectiveScore = total_reward / (st.BatchSize + float.Epsilon);
                }
                {
                    for (int i = 0; i < Input.StatusPath.Count ; i++)
                    {
                        StatusData st = Input.StatusPath[i];
                        if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
                    }
                }

            }
        }
}