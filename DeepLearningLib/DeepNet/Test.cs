﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using BigLearn;
using System.Diagnostics;


namespace BigLearn.DeepNet
{
    class Test
    {
        //static IMathOperationManager lib;
        
        //static void InitLib()
        //{
        //    DeviceType device = MathOperatorManager.SetDevice(0);
        //    lib = MathOperatorManager.CreateInstance(device);
        //}

        static void TestCudaVectorScale()
        {
            IntPtr GHandle = Cudalib.CudaCreateCuBlas();
            int batch = 9;
            CudaPieceFloat inputX = new CudaPieceFloat(batch, true, true);
            inputX.Init(1);

            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();

            for (int i = 0; i < 1; i++)
            {
                //Cudalib.Matrix_Product_Weight(inputX.CudaPtr, outputY.CudaPtr, weight.CudaPtr,
                //        batch, input, output, 1);
                Cudalib.CuBLAS_Scal(GHandle, inputX.CudaPtr, 10, batch);
            }

            CudaPieceFloatDebug.Print("VectorScale ", inputX, 300, false);

            //inputX.SyncToCPU();
            //stopwatch.Stop();
            
            //for(int i=0;i < batch;i ++)
            //{
            //    Console.WriteLine("num " + inputX.MemPtr[i].ToString());
            //}
            //TestCudaElementwiseProduct();

            Cudalib.CudaDestroyCuBlas(GHandle);
            inputX.Dispose();
        }

        static void TestCudaElementwiseProduct()
        {
            //Cudalib.CudaSetDevice(4);
            IntPtr cuDnnHandle = Cudalib.CudaCreateCuDnn();

            int row = 1024;
            int column = 512;
            CudaPieceFloat pLeft = new CudaPieceFloat(row * column, true, true);
            CudaPieceFloat pRight = new CudaPieceFloat(row * column, true, true);
            CudaPieceFloat pDst = new CudaPieceFloat(row * column, true, true);
            
            pLeft.Init(0.25f);
            pRight.Init(0.5f);
            pDst.Init(98f);

            //for(int i = 0; i < row * column; i++)
            //{
            //    pLeft.MemPtr[i] = 0.25f;
            //    pRight.MemPtr[i] = 0.5f;
            //    pDst.MemPtr[i] = 98f;
            //}
            //pLeft.SyncFromCPU(row * column);
            //pRight.SyncFromCPU(row * column);
            //pDst.SyncFromCPU(row * column);

            Cudalib.Tanh(pDst.CudaPtr, pDst.CudaPtr, row * column);
            CudaPieceFloatDebug.Print("Tanh ", pDst, 10, false);
            
            Stopwatch stopwatch = new Stopwatch();
            

            /******* method 1 : customerzied call ******/
            stopwatch.Restart();
            for(int m = 0; m<1000; m++)
            {
                Cudalib.ElementwiseProduct(pLeft.CudaPtr, pRight.CudaPtr, pDst.CudaPtr, row, column, 1);
            }
            CudaPieceFloatDebug.Print("Method 1: Element Wise Product ", pDst, 20, false);
            stopwatch.Stop();
            // Write result.
            Console.WriteLine("Method 1 Time elapsed: {0}", stopwatch.Elapsed);


            /******* method 2 : cuDNN call ******/
            stopwatch.Restart();
            for(int m=0;m<1000; m++)
            {
                Cudalib.CuDNNVectorMul(cuDnnHandle, pLeft.CudaPtr, pRight.CudaPtr, pDst.CudaPtr, 1, 1, row * column);
            }
            CudaPieceFloatDebug.Print("Method 2: Element Wise Product ", pDst, 20, false);
            stopwatch.Stop();
            // Write result.
            Console.WriteLine("Method 2 Time elapsed: {0}", stopwatch.Elapsed);


            Cudalib.CudaDestroyCuDnn(cuDnnHandle);

        }

        public static void TestAdd_Vector()
        {
            //float[] a, int a_idx, float[] b, int b_idx, 
             //   int size, float a_wei, float b_wei
            int size = 100;
            float[] a = new float[size];
            float[] b = new float[size];
            for(int i = 0; i < size; i++)
            {
                a[i] = i ;
                b[i] = i + 1;
            }
            FastVector.Add_Vector(a, 10, b, 10, size - 10, 1, 1);

            for(int i=0;i < size;i ++)
            {
                Console.WriteLine("num " + a[i].ToString() + " " + b[i].ToString());
            }
        }

        public static void TestDictionary()
        {
            Dictionary<string, Tuple<float[], float[]>> Mem = new Dictionary<string, Tuple<float[], float[]>>();

            for(int i=0;i<100000000;i++)
            {
                string mkey = "string" + i.ToString();
                Mem.Add(mkey, new Tuple<float[], float[]>(new float[100], new float[200]));

                if(i % 1000000 == 0)
                {
                    Console.WriteLine("push dictionary {0}", i);
                }
            }

            Random random = new Random();
            for(int i=0;i<100;i++)
            {
                int t = random.Next(100000000);
                Console.WriteLine(Mem["string"+t.ToString()].Item1.Length.ToString());
            }
        }

        public static void TestArray()
        {
            float[] m = new float[100];
            for(int i = 0; i< 100; i++)
            {
                m[i] = i;
            }

            IEnumerable<float> x = m.Take(10);
            foreach(float k in x)
            {
                Console.WriteLine(k.ToString());
            }
        }

        public static void TestSortArray()
        {
            Random random = new Random();
            float[] m = new float[5];
            for(int i = 0; i < 5; i++)
            {
                m[i] = random.Next(10000);
                Console.WriteLine(m[i]);
            }
            foreach(int k in m.Select((item, index) => new { item, index }).OrderBy(a => a.item).Select(a => a.index).Reverse())
            {
                Console.WriteLine(k);
            }
        }


        public static void TestCuSparse()
        {
            IntPtr cuSparseHandle = Cudalib.CudaCreateCuSparse();
            int batch = 100;
            CudaPieceFloat inputX = new CudaPieceFloat(batch, true, true);
            inputX.Init(100);

            SparseVectorData data = new SparseVectorData(5, DeviceType.GPU);

            data.Length = 5;
            data.Idx.MemPtr[0] = 1;
            data.Idx.MemPtr[1] = 3;
            data.Idx.MemPtr[2] = 5;
            data.Idx.MemPtr[3] = 7;
            data.Idx.MemPtr[4] = 9;

            data.Value.MemPtr[0] = 1;
            data.Value.MemPtr[1] = 2;
            data.Value.MemPtr[2] = 3;
            data.Value.MemPtr[3] = 4;
            data.Value.MemPtr[4] = 5;

            data.SyncFromCPU();

            Cudalib.SparseVectorAdd(cuSparseHandle, data.Idx.CudaPtr, data.Value.CudaPtr, inputX.CudaPtr, 1, 5);


            CudaPieceFloatDebug.Print("data", inputX, 100, false);

            Cudalib.CudaDestroyCuSparse(cuSparseHandle);
        }


        public static void TestCuSum()
        {
            IntPtr cuBlasHandle = Cudalib.CudaCreateCuBlas();
            int batch = 10;
            CudaPieceFloat inputX = new CudaPieceFloat(batch, true, true);
            for(int i=0; i < batch;i++)
            {
                inputX.MemPtr[i] = -i ;
            }
            inputX.SyncFromCPU();
            float sum = Cudalib.CUBLAS_Sasum(cuBlasHandle, inputX.CudaPtr, batch, 1);
            Console.WriteLine(sum);
            Cudalib.CudaDestroyCuBlas(cuBlasHandle);
        }

        public static void Main(string[] args)
        {
            TestCudaElementwiseProduct();
            return;

            TestCuSum();
            return;

            TestCuSparse();
            return;

            TestSortArray();
            return;

            TestArray();

            //TestDictionary();
            
            Cudalib.CudaSetDevice(4);

            Console.WriteLine("hello world.");
            //int m = Sselib.AddTest(100, 100);
            //int n = Sselib.MultiTest(200, 200);

            //TestAdd_Vector();
            TestCudaVectorScale();
            TestCudaElementwiseProduct();
            

            float[] x = new float[10];
            for (int i = 0; i < 10;i++)
            {
                x[i] = i;
            }

            unsafe
            {

                fixed (float* pArray = &x[0])
                {
                    //IntPtr intPtr = new IntPtr((void*)pArray);
                    float m = SSElib.SSE_SumAbs(pArray, 5);
                    float n = SSElib.SSE_SumSq(pArray, 5);

                    Console.WriteLine(m.ToString());
                    Console.WriteLine(n.ToString());
                }
            }


        }
    }
}
