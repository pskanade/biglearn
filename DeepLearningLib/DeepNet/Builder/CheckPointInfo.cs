﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;

namespace BigLearn.DeepNet
{
    public class CheckPointInfo : IIOUnit
    {
        public IOUnitHeader Head { get; set; }
        public DataSourceID Type { get { return DataSourceID.CheckPointInfo; } }

        public DeviceType DeviceType { get { return DeviceType.CPU; } }

        public ModelMetaInfo ModelInfo { get; set; }

        public int CurrentTrainIter { get; set; }

        public Structure Model { get; set; }

        public OptimizerFlow Optimizer { protected get; set; }

        public BuilderType ModelType { get; set; }

        public DeviceType ModelDevice { get; set; }

        protected MemoryStream OptimizerCheckBuffer;

        public void Load(BinaryReader input)
        {
            this.CurrentTrainIter = input.ReadInt32();
            this.ModelType = (BuilderType)input.ReadInt32();
            this.ModelDevice = (DeviceType)input.ReadInt32();

            IOUnitReader rd = new IOUnitReader(input.BaseStream);
            this.ModelInfo = (ModelMetaInfo)rd.Read();

            switch (this.ModelType)
            {
                case BuilderType.DSSM:
                case BuilderType.DSSM_GRAPH:
                    this.Model = new DSSMStructure(input.BaseStream, this.ModelDevice);
                    break;
                default:
                    throw new NotSupportedException(string.Format("Model of type {0} is not supported yet.", this.ModelType));
            }

            long optLen = input.ReadInt64();
            if (optLen != 0)
            {
                byte[] optBuffer = input.ReadBytes((int)optLen);
                OptimizerCheckBuffer = new MemoryStream(optBuffer);
            }
        }

        public void Save(BinaryWriter output)
        {
            output.Write(this.CurrentTrainIter);
            output.Write((Int32)this.ModelType);
            output.Write((Int32)this.ModelDevice);
            IOUnitWriter wr = new IOUnitWriter(output.BaseStream);
            wr.Write(this.ModelInfo);

            Model.Serialize(output);

            if (this.Optimizer != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    this.Optimizer.Save(new BinaryWriter(ms, Encoding.UTF8, true));
                    output.Write(ms.Length);
                    output.Write(ms.ToArray(), 0, (int)ms.Length);
                }
            }
            else
            {
                output.Write((long)0);
            }
        }

        public void LoadOptimizer(OptimizerFlow opt)
        {
            if (this.OptimizerCheckBuffer != null && this.OptimizerCheckBuffer.Length > 0)
            {
                this.OptimizerCheckBuffer.Position = 0;
                BinaryReader br = new BinaryReader(this.OptimizerCheckBuffer);
                opt.Load(br);
            }
        }

        public void SyncFromCPU() { throw new NotImplementedException(); }
        public void SyncToCPU() { throw new NotImplementedException(); }

        public void Dispose()
        {
            if (this.OptimizerCheckBuffer != null)
            {
                this.OptimizerCheckBuffer.Close();
                this.OptimizerCheckBuffer = null;
            }
        }
    }
}
