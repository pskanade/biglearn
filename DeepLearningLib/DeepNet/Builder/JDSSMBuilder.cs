﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    


    //public class JDSSMData : BatchData, ISourcable
    //{
    //    public DataSourceID DataTypeID { get { return DataSourceID.JDSSMData; } }
    //    public SeqBatchInputData SrcInput = null;
    //    public SeqBatchInputData TgtInput = null;
    //    public MatchBatchInputData MatchInput = null;
    //    public override int BatchSize { get { return MatchInput.BatchSize; } }

    //    public JDSSMData(SeqBatchInputData srcInput, SeqBatchInputData tgtInput, MatchBatchInputData matchInput)
    //    {
    //        SrcInput = srcInput;
    //        TgtInput = tgtInput;
    //        MatchInput = matchInput;
    //    }

    //    public void Init()
    //    {
    //        SrcInput.Init();
    //        TgtInput.Init();
    //        MatchInput.Init();
    //    }

    //    public void Load()
    //    {
    //        SrcInput.Load();
    //        TgtInput.Load();
    //        MatchInput.Load();
    //    }

    //    public Instance Get()
    //    {
    //        Instance instance = new Instance(1);
    //        instance.subInstance.Add(SrcInput.Get());
    //        instance.subInstance.Add(TgtInput.Get());
    //        instance.subInstance.Add(MatchInput.Get());
    //        return instance;
    //    }

    //    public void Put(Instance instance)
    //    {
    //        SrcInput.Put(instance.subInstance[0]);
    //        TgtInput.Put(instance.subInstance[1]);
    //        MatchInput.Put(instance.subInstance[2]);
    //    }

    //    public void SkipLoad()
    //    {
    //        SrcInput.SkipLoad();
    //        TgtInput.SkipLoad();
    //        MatchInput.SkipLoad(); 
    //    }

    //    public int BatchNum()
    //    {
    //        return MatchInput.TOTAL_BATCHNUM;
    //    }

    //    public void SeekTo(int batchId)
    //    {
    //        SrcInput.SeekTo(batchId);
    //        TgtInput.SeekTo(batchId);
    //        MatchInput.SeekTo(batchId);
    //    }

    //    public override void Dispose()
    //    {
    //        if (SrcInput != null) SrcInput.Dispose(); SrcInput = null;
    //        if (TgtInput != null) TgtInput.Dispose(); TgtInput = null;
    //        if (MatchInput != null) MatchInput.Dispose(); MatchInput = null;
    //    }
    //}

    //public class JointLayerConfig
    //{
    //    public bool IsJointSimFea = true;
    //    public SimilarityType SimiType = SimilarityType.CosineSimilarity;
    //    public bool IsJointSrcFea = false;
    //    public bool IsJointTgtFea = false;
    //    public bool IsJointMatchFea = true;
    //    public bool IsShareModel = false;

    //    public bool IsOnlySim { get { return (IsJointSimFea && !IsJointSrcFea && !IsJointTgtFea && !IsJointMatchFea); } }

    //    public void Save(BinaryWriter writer)
    //    {
    //        writer.Write(IsJointSimFea);
    //        writer.Write((int)SimiType);
    //        writer.Write(IsJointSrcFea);
    //        writer.Write(IsJointTgtFea);
    //        writer.Write(IsJointMatchFea);
    //        writer.Write(IsShareModel);
    //    }

    //    public void Load(BinaryReader reader)
    //    {
    //        IsJointSimFea = reader.ReadBoolean();
    //        SimiType = (SimilarityType)reader.ReadInt32();
    //        IsJointSrcFea = reader.ReadBoolean();
    //        IsJointTgtFea = reader.ReadBoolean();
    //        IsJointMatchFea = reader.ReadBoolean();
    //        IsShareModel = reader.ReadBoolean();
    //    }
    //}

    //public class JointLayerRunner<IN> : StructRunner<LayerStructure, IN> where IN : JointLayerData
    //{
    //    public HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
    //    public HiddenBatchData SimData;

    //    public CudaPieceFloat SrcInputSquare;
    //    public CudaPieceFloat TgtInputSquare;

    //    public JointLayerConfig JointConfig;

    //    //BatchData data,  data, 
    //    public JointLayerRunner(Structure model, RunnerBehavior behavior)
    //        : base(model, behavior)
    //    {
    //        JointConfig = (JointLayerConfig)behavior.BehaviorParameter;
    //        Output = new HiddenBatchData(); //
    //    }

    //    public override void InitMemory()
    //    {
    //        if (Output.MAX_BATCHSIZE < Input.MatchBatchData.Stat.MAX_BATCHSIZE)
    //        {
    //            AllocateMemory();
    //        }
    //    }

    //    public void AllocateMemory()
    //    {
    //        Output.Init(Input.MatchBatchData.Stat.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
    //        SimData = new HiddenBatchData(Input.MatchBatchData.Stat.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
    //        if (JointConfig.IsJointSimFea)
    //        {
    //            SrcInputSquare = new CudaPieceFloat(Input.SrcBatchData.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
    //            TgtInputSquare = new CudaPieceFloat(Input.TgtBatchData.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
    //        }
    //    }

    //    //    Output = new HiddenBatchData(Input.MatchBatchData.MAX_BATCHSIZE, Model.Neural_Out, behavior.RunMode, behavior.Device);
    //    //    SimData = new HiddenBatchData(Input.MatchBatchData.MAX_BATCHSIZE, 1, behavior.RunMode, behavior.Device);
    //    //    //if (JointConfig.IsJointSimFea)
    //    //    //{
    //    //    SrcInputSquare = new CudaPieceFloat(Input.SrcBatchData.MAX_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
    //    //    TgtInputSquare = new CudaPieceFloat(Input.TgtBatchData.MAX_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
    //    //    //}
    //    //}

    //    ~JointLayerRunner()
    //    {
    //        this.Dispose(false);
    //    }

    //    private bool disposed = false;
    //    protected override void Dispose(bool disposing)
    //    {
    //        if (disposed)
    //        {
    //            return;
    //        }

    //        disposed = true;
    //        if (disposing)
    //        {
    //            if (SrcInputSquare != null) SrcInputSquare.Dispose(); SrcInputSquare = null;
    //            if (TgtInputSquare != null) TgtInputSquare.Dispose(); TgtInputSquare = null;
    //            if (Output != null) Output.Dispose(); Output = null;
    //            if (SimData != null) SimData.Dispose(); SimData = null;
    //        }

    //        base.Dispose(disposing);
    //    }

    //    /// <summary>
    //    /// Forward Propagation.
    //    /// </summary>
    //    public override void Forward()
    //    {
    //        InitMemory();
    //        MathOperatorManager.MathDevice = Behavior.Device;

    //        if (JointConfig.IsJointSimFea)
    //        {
    //            switch (JointConfig.SimiType)
    //            {
    //                case SimilarityType.CosineSimilarity:
    //                    MathOperatorManager.GlobalInstance.Square_Matrix(Input.SrcBatchData.Output.Data, Input.SrcBatchData.BatchSize, Input.SrcBatchData.Output.Stat.Dim, SrcInputSquare);
    //                    MathOperatorManager.GlobalInstance.Square_Matrix(Input.TgtBatchData.Output.Data, Input.TgtBatchData.BatchSize, Input.TgtBatchData.Output.Stat.Dim, TgtInputSquare);

    //                    MathOperatorManager.GlobalInstance.Cosine_Similarity_Matching(Input.SrcBatchData.Output.Data,
    //                                                      Input.TgtBatchData.Output.Data,
    //                                                      SimData.Output.Data,
    //                                                      Input.MatchBatchData.MatchSrcIdx,
    //                                                      Input.MatchBatchData.MatchTgtIdx,
    //                                                      Input.SrcBatchData.BatchSize,
    //                                                      Input.TgtBatchData.BatchSize,
    //                                                      Input.MatchBatchData.BatchSize,
    //                                                      Input.SrcBatchData.Output.Stat.Dim,
    //                                                      SrcInputSquare,
    //                                                      TgtInputSquare,
    //                                                      Util.GPUEpsilon);

    //                    break;
    //                case SimilarityType.InnerProduct:
    //                    MathOperatorManager.GlobalInstance.Inner_Product_Matching(Input.SrcBatchData.Output.Data,
    //                                                      Input.TgtBatchData.Output.Data,
    //                                                      SimData.Output.Data,
    //                                                      Input.MatchBatchData.MatchSrcIdx,
    //                                                      Input.MatchBatchData.MatchTgtIdx,
    //                                                      Input.SrcBatchData.BatchSize,
    //                                                      Input.TgtBatchData.BatchSize,
    //                                                      Input.MatchBatchData.BatchSize,
    //                                                      Input.SrcBatchData.Output.Stat.Dim,
    //                                                      Util.GPUEpsilon);
    //                    break;
    //            }
    //            SimData.BatchSize = Input.MatchBatchData.BatchSize;
    //        }

    //        switch (Input.MatchBatchData.Stat.FeatureType)
    //        {
    //            case FeatureDataType.SparseFeature:
    //                MathOperatorManager.GlobalInstance.Joint_Sparse_Matrix_Multiply_INTEX(Input.SrcBatchData.Output.Data, Input.SrcBatchData.Output.BatchSize,
    //                                                            Input.TgtBatchData.Output.Data, Input.TgtBatchData.Output.BatchSize,
    //                                                            Input.MatchBatchData.MatchSrcIdx, Input.MatchBatchData.MatchTgtIdx,
    //                                                            SimData.Output.Data, Input.MatchBatchData.BatchSize,

    //                                                            Input.MatchBatchData.BatchIdx,
    //                                                            Input.MatchBatchData.FeatureIdx, Input.MatchBatchData.FeatureValue,

    //                                                            Input.SrcBatchData.Output.Stat.Dim, Input.TgtBatchData.Output.Stat.Dim, Input.MatchBatchData.Stat.MAX_FEATUREDIM,
    //                                                            JointConfig.IsJointMatchFea ? 1 : 0, Model.weight, Output.Output.Data,
    //                                                            Output.Output.Stat.Dim, JointConfig.IsJointSrcFea ? 1 : 0, JointConfig.IsJointTgtFea ? 1 : 0,
    //                                                            JointConfig.IsJointSimFea ? 1 : 0);
    //                break;
    //            case FeatureDataType.DenseFeature:
    //                MathOperatorManager.GlobalInstance.Joint_Dense_Matrix_Multiply_INTEX(Input.SrcBatchData.Output.Data, Input.MatchBatchData.MaxSrcIdx,
    //                                                            Input.TgtBatchData.Output.Data, Input.MatchBatchData.MaxTgtIdx,
    //                                                            Input.MatchBatchData.MatchSrcIdx, Input.MatchBatchData.MatchTgtIdx,
    //                                                            SimData.Output.Data, Input.MatchBatchData.BatchSize,

    //                                                            Input.MatchBatchData.DenseFeatureData,

    //                                                            Input.SrcBatchData.Output.Stat == null ? 0 : Input.SrcBatchData.Output.Stat.Dim,
    //                                                            Input.TgtBatchData.Output.Stat == null ? 0 : Input.TgtBatchData.Output.Stat.Dim,
    //                                                            Input.MatchBatchData.Stat.MAX_FEATUREDIM,
    //                                                            JointConfig.IsJointMatchFea ? 1 : 0, Model.weight, Output.Output.Data,
    //                                                            Output.Output.Stat.Dim, JointConfig.IsJointSrcFea ? 1 : 0, JointConfig.IsJointTgtFea ? 1 : 0,
    //                                                            JointConfig.IsJointSimFea ? 1 : 0);
    //                break;
    //        }
    //        Output.BatchSize = Input.MatchBatchData.BatchSize;
    //        Model.ActiveOutput(Output.Output, Behavior.RunMode);
    //    }

    //    /// <summary>
    //    /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
    //    /// </summary>
    //    /// <param name="cleanDeriv"></param>
    //    public override void Backward(bool cleanDeriv)
    //    {
    //        if (JDSSMBuilderParameters.IS_SRC_SEED_UPDATE || JDSSMBuilderParameters.IS_TGT_SEED_UPDATE)
    //        {
    //            Model.DeactiveOutput(Output.Output, Output.Deriv, Behavior.RunMode);

    //            if (JointConfig.IsJointSimFea)
    //            {
    //                Cudalib.Joint_Backward_Sim(Output.Deriv.Data.CudaPtr, Output.Deriv.BatchSize, Output.Deriv.Stat.Dim,
    //                        Model.Weight, SimData.Deriv.Data.CudaPtr);
    //            }

    //            //if (JointConfig.IsJointSrcFea || JointConfig.IsJointSimFea)
    //            //    Input.MatchBatchData.InverseSrcMatch(Input.SrcBatchData.BatchSize);

    //            //if (JointConfig.IsJointTgtFea || JointConfig.IsJointSimFea)
    //            //    Input.MatchBatchData.InverseTgtMatch(Input.TgtBatchData.BatchSize);

    //            if (JointConfig.IsJointSrcFea || JointConfig.IsJointTgtFea || JointConfig.IsJointSimFea)
    //            {
    //                Input.SrcBatchData.Deriv.Data.Zero();
    //                Input.TgtBatchData.Deriv.Data.Zero();

    //                // Backpropagate Deriv.
    //                Cudalib.Joint_Backward_Src_Tgt(Output.Deriv.Data.CudaPtr, Output.Deriv.BatchSize, Output.Deriv.Stat.Dim,
    //                                               Model.Weight, SimData.Deriv.Data.CudaPtr,
    //                                               Input.MatchBatchData.MatchSrcIdx.CudaPtr, Input.MatchBatchData.MatchTgtIdx.CudaPtr,
    //                                               Input.SrcBatchData.Output.Data.CudaPtr, Input.SrcBatchData.BatchSize, Input.SrcBatchData.Dim,
    //                                               Input.TgtBatchData.Output.Data.CudaPtr, Input.TgtBatchData.BatchSize, Input.TgtBatchData.Dim,
    //                                               SimData.Output.Data.CudaPtr,

    //                                               SrcInputSquare == null ? IntPtr.Zero : SrcInputSquare.CudaPtr,
    //                                               TgtInputSquare == null ? IntPtr.Zero : TgtInputSquare.CudaPtr,

    //                                               JointConfig.IsJointSrcFea ? 1 : 0,
    //                                               JointConfig.IsJointTgtFea ? 1 : 0, JointConfig.IsJointSimFea ? 1 : 0,
    //                                               Input.MatchBatchData.Src2MatchIdx.CudaPtr, Input.MatchBatchData.Src2MatchElement.CudaPtr,
    //                                               Input.MatchBatchData.Tgt2MatchIdx.CudaPtr, Input.MatchBatchData.Tgt2MatchElement.CudaPtr,
    //                                               Input.SrcBatchData.Deriv.Data.CudaPtr, Input.TgtBatchData.Deriv.Data.CudaPtr, Util.GPUEpsilon, (int)JointConfig.SimiType);
    //            }
    //        }
    //    }

    //    /// <summary>
    //    /// Backpropagate Parameter Update.
    //    /// </summary>
    //    public override void Update()
    //    {
    //        Model.WeightOptimizer.BeforeGradient();

    //        switch (Input.MatchBatchData.Stat.FeatureType)
    //        {
    //            case FeatureDataType.SparseFeature:
    //                Cudalib.Joint_Sparse_Matrix_Product_INTEX(Input.SrcBatchData.Output.Data.CudaPtr, Input.MatchBatchData.MaxSrcIdx,  //Input.SrcBatchData.Output.BatchSize,
    //                                                    Input.TgtBatchData.Output.Data.CudaPtr, Input.MatchBatchData.MaxTgtIdx,  //Input.TgtBatchData.Output.BatchSize,
    //                                                    Input.MatchBatchData.MatchSrcIdx.CudaPtr, Input.MatchBatchData.MatchTgtIdx.CudaPtr,
    //                                                    SimData.Output.Data.CudaPtr, Input.MatchBatchData.BatchSize,

    //                                                    Input.MatchBatchData.BatchIdx.CudaPtr,
    //                                                    Input.MatchBatchData.FeatureIdx.CudaPtr, Input.MatchBatchData.FeatureValue.CudaPtr,

    //                                                    Input.SrcBatchData.Output.Stat.Dim, Input.TgtBatchData.Output.Stat.Dim, Input.MatchBatchData.Stat.MAX_FEATUREDIM,
    //                                                    JointConfig.IsJointMatchFea ? 1 : 0, Model.WeightOptimizer.Gradient.CudaPtr, Output.Deriv.Data.CudaPtr,
    //                                                    Output.Output.Stat.Dim, JointConfig.IsJointSrcFea ? 1 : 0, JointConfig.IsJointTgtFea ? 1 : 0,
    //                                                    JointConfig.IsJointSimFea ? 1 : 0, Model.WeightOptimizer.GradientStep);
    //                break;
    //            case FeatureDataType.DenseFeature:
    //                Cudalib.Joint_Dense_Matrix_Product_INTEX(
    //                    Input.SrcBatchData.Output.Data == null ? IntPtr.Zero : Input.SrcBatchData.Output.Data.CudaPtr, Input.MatchBatchData.MaxSrcIdx,  //Input.SrcBatchData.Output.BatchSize,
    //                    Input.TgtBatchData.Output.Data == null ? IntPtr.Zero : Input.TgtBatchData.Output.Data.CudaPtr, Input.MatchBatchData.MaxTgtIdx,  //Input.TgtBatchData.Output.BatchSize,
    //                                                    Input.MatchBatchData.MatchSrcIdx.CudaPtr, Input.MatchBatchData.MatchTgtIdx.CudaPtr,
    //                                                    SimData.Output.Data.CudaPtr, Input.MatchBatchData.BatchSize,

    //                                                    Input.MatchBatchData.DenseFeatureData.CudaPtr,

    //                                                    Input.SrcBatchData.Output.Stat == null ? 0 : Input.SrcBatchData.Output.Stat.Dim,
    //                                                    Input.TgtBatchData.Output.Stat == null ? 0 : Input.TgtBatchData.Output.Stat.Dim,

    //                                                    //Input.SrcBatchData.Output.Stat.Dim, 
    //                    //Input.TgtBatchData.Output.Stat.Dim, 

    //                                                    Input.MatchBatchData.Stat.MAX_FEATUREDIM,
    //                                                    JointConfig.IsJointMatchFea ? 1 : 0, Model.WeightOptimizer.Gradient.CudaPtr, Output.Deriv.Data.CudaPtr,
    //                                                    Output.Output.Stat.Dim, JointConfig.IsJointSrcFea ? 1 : 0, JointConfig.IsJointTgtFea ? 1 : 0,
    //                                                    JointConfig.IsJointSimFea ? 1 : 0, Model.WeightOptimizer.GradientStep);
    //                break;
    //        }
    //        Model.WeightOptimizer.AfterGradient();

    //        if (Model.IsBias)
    //        {
    //            Model.BiasOptimizer.BeforeGradient();
    //            Cudalib.Matrix_Aggragate_Weight(Output.Deriv.Data.CudaPtr, Model.BiasOptimizer.Gradient.CudaPtr, Output.Deriv.BatchSize, Model.Neural_Out, Model.BiasOptimizer.GradientStep);
    //            Model.BiasOptimizer.AfterGradient();
    //        }
    //    }
    //}

    public class JDSSMBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static string QueryTrainData { get { return Argument["QUERY"].Value; } }
            public static string DocTrainData { get { return Argument["DOC"].Value; } }
            public static string MatchTrainData { get { return Argument["MATCH"].Value; } }

            public static string QueryTrainCursorData { get { return Argument["QUERY-CURSOR"].Value; } }
            public static string DocTrainCursorData { get { return Argument["DOC-CURSOR"].Value; } }
            public static string MatchTrainCursorData { get { return Argument["MATCH-CURSOR"].Value; } }

            public static string QueryValidData { get { return Argument["QUERY-VALID"].Value; } }
            public static string DocValidData { get { return Argument["DOC-VALID"].Value; } }
            public static string MatchValidData { get { return Argument["MATCH-VALID"].Value; } }

            public static string QueryTestData { get { return Argument["QUERY-TEST"].Value; } }
            public static string DocTestData { get { return Argument["DOC-TEST"].Value; } }
            public static string MatchTestData { get { return Argument["MATCH-TEST"].Value; } }

            public static bool IsValidFile { get { return !(QueryValidData.Equals(string.Empty) || DocValidData.Equals(string.Empty) || MatchValidData.Equals(string.Empty)); } }
            public static bool IsTestFile { get { return !(QueryTestData.Equals(string.Empty) || DocTestData.Equals(string.Empty) || MatchTestData.Equals(string.Empty)); } }

            public static int[] SRC_LAYER_DIM { get { return Argument["SRC-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] SRC_ACTIVATION { get { return Argument["SRC-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] SRC_LAYER_BIAS { get { return Argument["SRC-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static N_Type[] SRC_ARCH { get { return Argument["SRC-ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
            public static int[] SRC_WINDOW { get { return Argument["SRC-WINDOW"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static string SRC_SEED_MODEL { get { return Argument["SRC-SEED-MODEL"].Value; } }
            public static bool IS_SRC_SEED_UPDATE { get { return int.Parse(Argument["SRC-SEED-UPDATE"].Value) > 0; } }
            public static float[] SRC_LAYER_DROPOUT { get { return Argument["SRC-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static int[] TGT_LAYER_DIM { get { return Argument["TGT-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] TGT_ACTIVATION { get { return Argument["TGT-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] TGT_LAYER_BIAS { get { return Argument["TGT-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static N_Type[] TGT_ARCH { get { return Argument["TGT-ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
            public static int[] TGT_WINDOW { get { return Argument["TGT-WINDOW"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static string TGT_SEED_MODEL { get { return Argument["TGT-SEED-MODEL"].Value; } }
            public static bool IS_TGT_SEED_UPDATE { get { return int.Parse(Argument["TGT-SEED-UPDATE"].Value) > 0; } }
            public static float[] TGT_LAYER_DROPOUT { get { return Argument["TGT-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static bool IsShareModel { get { return int.Parse(Argument["SHARE-MODEL"].Value) > 0; } }
            public static SimilarityType SimiType { get { return (SimilarityType)(int.Parse(Argument["SIM-TYPE"].Value)); } }

            public static bool IsJointSimFea { get { return (int.Parse(Argument["JOINT-SIM"].Value) > 0 && SRC_LAYER_DIM.Last() == TGT_LAYER_DIM.Last()); } }
            public static bool IsJointSrcFea { get { return int.Parse(Argument["JOINT-SRC"].Value) > 0; } }
            public static bool IsJointTgtFea { get { return int.Parse(Argument["JOINT-TGT"].Value) > 0; } }
            //public static bool IsJointOperator { get { return int.Parse(Argument["JOINT-OPERATOR"].Value) > 0; } }  //
            public static bool IsJointMatchFea { get { return int.Parse(Argument["JOINT-MATCH"].Value) > 0; } }

            public static int[] JOINT_LAYER_DIM { get { return Argument["JOINT-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] JOINT_ACTIVATION { get { return Argument["JOINT-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] JOINT_LAYER_BIAS { get { return Argument["JOINT-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static float[] JOINT_LAYER_DROPOUT { get { return Argument["JOINT-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }

            public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
            public static float JointLearnRate { get { return float.Parse(Argument["JOINT-LEARN-RATE"].Value); } }
            public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
            public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static bool IsOnlySim { get { return (IsJointSimFea && !IsJointSrcFea && !IsJointTgtFea && !IsJointMatchFea); } }

            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static DeviceType Device { get { return GPUID >= 0 ? DeviceType.GPU : DeviceType.CPU; } }
            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
            public static DNNModelVersion DSSM_MODEL_VERSION { get { return (DNNModelVersion)int.Parse(Argument["DSSM-MODEL-VERSION"].Value); } }

            public static int DebugBatchNum { get { return int.Parse(Argument["DEBUG-BATCH"].Value); } }

            public static float WeightInitBias { get { return float.Parse(Argument["INIT-WEIGHT-BIAS"].Value); } }

            public static float WeightInitScale { get { return float.Parse(Argument["INIT-WEIGHT-SCALE"].Value); } }

            public static float SimWeightInitBias { get { return float.Parse(Argument["INIT-SIM-WEIGHT-BIAS"].Value); } }
            /// <summary>
            /// Only need to Specify Data and Archecture of Deep Nets.
            /// </summary>
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("QUERY", new ParameterArgument(string.Empty, "QUERY Train Data."));
                Argument.Add("DOC", new ParameterArgument(string.Empty, "DOC Train Data."));
                Argument.Add("MATCH", new ParameterArgument(string.Empty, "MATCH Train Data."));

                Argument.Add("QUERY-CURSOR", new ParameterArgument(string.Empty, "QUERY Train CURSOR Data."));
                Argument.Add("DOC-CURSOR", new ParameterArgument(string.Empty, "DOC Train CURSOR Data."));
                Argument.Add("MATCH-CURSOR", new ParameterArgument(string.Empty, "MATCH Train CURSOR Data."));

                Argument.Add("QUERY-VALID", new ParameterArgument(string.Empty, "QUERY Valid Data."));
                Argument.Add("DOC-VALID", new ParameterArgument(string.Empty, "DOC Valid Data."));
                Argument.Add("MATCH-VALID", new ParameterArgument(string.Empty, "MATCH Valid Data."));

                Argument.Add("QUERY-TEST", new ParameterArgument(string.Empty, "QUERY Test Data."));
                Argument.Add("DOC-TEST", new ParameterArgument(string.Empty, "DOC Test Data."));
                Argument.Add("MATCH-TEST", new ParameterArgument(string.Empty, "MATCH Test Data."));

                Argument.Add("SRC-LAYER-DIM", new ParameterArgument("256", "Source Layer Dim"));
                Argument.Add("SRC-ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("SRC-LAYER-BIAS", new ParameterArgument("0", "Source Layer Bias"));
                Argument.Add("SRC-ARCH", new ParameterArgument(((int)N_Type.Fully_Connected).ToString(), ParameterUtil.EnumValues(typeof(N_Type))));
                Argument.Add("SRC-WINDOW", new ParameterArgument("1", "Source DNN Window Size"));
                Argument.Add("SRC-SEED-MODEL", new ParameterArgument(string.Empty, "Source Seed Model."));
                Argument.Add("SRC-SEED-UPDATE", new ParameterArgument("1", "Update Source Seed Model. 1: true; 0 : false;"));
                Argument.Add("SRC-LAYER-DROPOUT", new ParameterArgument("0", "Source DNN Layer Dropout"));

                Argument.Add("TGT-LAYER-DIM", new ParameterArgument("256", "Target Layer Dim"));
                Argument.Add("TGT-ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("TGT-LAYER-BIAS", new ParameterArgument("0", "Target Layer Bias"));
                Argument.Add("TGT-ARCH", new ParameterArgument(((int)N_Type.Fully_Connected).ToString(), ParameterUtil.EnumValues(typeof(N_Type))));
                Argument.Add("TGT-WINDOW", new ParameterArgument("1", "Target DNN Window Size"));
                Argument.Add("TGT-SEED-MODEL", new ParameterArgument(string.Empty, "Target Seed Model."));
                Argument.Add("TGT-SEED-UPDATE", new ParameterArgument("1", "Update Target Seed Model. 1: true; 0 : false;"));
                Argument.Add("TGT-LAYER-DROPOUT", new ParameterArgument("0", "Target DNN Layer Dropout"));

                Argument.Add("MODEL-PATH", new ParameterArgument(@"\\DEEP-06\Test-JDSSM\Model_Path\", "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
                Argument.Add("SHARE-MODEL", new ParameterArgument("1", "Share Source and Target Model; 0 : no share; 1 : share;"));
                Argument.Add("SIM-TYPE", new ParameterArgument(((int)SimilarityType.CosineSimilarity).ToString(), ParameterUtil.EnumValues(typeof(SimilarityType))));

                Argument.Add("JOINT-SIM", new ParameterArgument("1", "Joint Src and Target Similarity into JDSSM;"));
                Argument.Add("JOINT-SRC", new ParameterArgument("1", "Joint Src Feature into JDSSM;"));
                Argument.Add("JOINT-TGT", new ParameterArgument("1", "Joint Target Feature into JDSSM;"));
                Argument.Add("JOINT-MATCH", new ParameterArgument("1", "Joint Match Feature into JDSSM;"));

                Argument.Add("JOINT-LAYER-DIM", new ParameterArgument("1", "JOINT DNN Layer Dim"));
                Argument.Add("JOINT-ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("JOINT-LAYER-BIAS", new ParameterArgument("1", "JOINT DNN Layer Bias"));
                Argument.Add("JOINT-LAYER-DROPOUT", new ParameterArgument("0", "JOINT DNN Layer Dropout"));

                Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.BinaryClassification).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("GAMMA", new ParameterArgument("10", "List-wise Ranking Smooth Parameter."));

                Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "JDSSM Learn Rate."));
                Argument.Add("JOINT-LEARN-RATE", new ParameterArgument("0.0000001", "JDSSM Learn Rate."));
                

                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));

                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.NDCG).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));
                //@"EvaluationToolkit\Evaluation_AUC\AUC.exe", "AUC Tool Path."));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));
                Argument.Add("LABEL-MIN", new ParameterArgument(((int)HRSItem.Bad).ToString(), "Label Start with 1 by default;"));
                Argument.Add("LABEL-MAX", new ParameterArgument(((int)HRSItem.Perfect).ToString(), "Label End with 5 by default;"));
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("DSSM-MODEL-VERSION", new ParameterArgument(((int)DNNModelVersion.C_OR_DSSM_GPU_V2_2014_MAY).ToString(), ParameterUtil.EnumValues(typeof(DNNModelVersion))));

                Argument.Add("DEBUG-BATCH", new ParameterArgument("0", "Evaluation on Every Debug Batch Number"));
                Argument.Add("INIT-WEIGHT-BIAS", new ParameterArgument("0", "Init Model Weight Bias"));
                Argument.Add("INIT-WEIGHT-SCALE", new ParameterArgument("-1", "Init Model Weight Scale"));
                Argument.Add("INIT-SIM-WEIGHT-BIAS", new ParameterArgument("1", "Init Model Weight Scale"));

            }
        }

        public override BuilderType Type { get { return BuilderType.JDSSM; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> query,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> doc,
                                                             IDataCashier<BiMatchBatchData, BiMatchBatchDataStat> match,
                                                             IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> matchFea,

                                                             // text embedding cnn.
                                                             DNNStructure queryDNN,
                                                             DNNStructure docDNN,
                                                             DNNStructure feaDNN,
                                                             DNNStructure jointDNN,
                                                             CompositeNNStructure model, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));
            SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(doc, Behavior));
            BiMatchBatchData matchData = (BiMatchBatchData)cg.AddDataRunner(new DataRunner<BiMatchBatchData, BiMatchBatchDataStat>(match, Behavior));
            GeneralBatchInputData matchFeaData = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(matchFea, Behavior));

            HiddenBatchData queryOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(queryDNN, QueryData, Behavior));

            HiddenBatchData docOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(docDNN, ContextData, Behavior));

            HiddenBatchData SimOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(queryOutput, docOutput, matchData, BuilderParameters.SimiType, Behavior));

            HiddenBatchData feaOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(feaDNN, matchFeaData, Behavior));

            HiddenBatchData tgtFea = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SimOutput, docOutput, feaOutput }, Behavior));

            SeqDenseBatchData tgtLevelFea = new SeqDenseBatchData(new SequenceDataStat(matchData.Stat.MAX_SRC_BATCHSIZE, matchData.Stat.MAX_MATCH_BATCHSIZE, tgtFea.Dim ), 
                matchData.Src2MatchIdx, matchData.SrcIdx, tgtFea.Output.Data, tgtFea.Deriv.Data, tgtFea.DeviceType );

            HiddenBatchData combineFea = new HiddenBatchData((SeqMatrixData)cg.AddRunner(new SeqMatrixConcateRunner(new MatrixData(queryOutput), new SeqMatrixData(tgtLevelFea), Behavior)));

            HiddenBatchData output = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(jointDNN, combineFea, Behavior));

            cg.AddObjective(new CrossEntropyRunner(matchFeaData.InstanceLabel, output.Output, output.Deriv, BuilderParameters.Gamma, Behavior));
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");

            CompositeNNStructure modelStructure = new CompositeNNStructure();

            DNNStructure SrcDnn = null;
            DNNStructure TgtDnn = null;
            DNNStructure FeatureDnn = null;
            DNNStructure JointDnn = null;

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                SrcDnn = new DNNStructure(
                            DataPanel.TrainQueryInput.Stat.FEATURE_DIM,
                            BuilderParameters.SRC_LAYER_DIM,
                            BuilderParameters.SRC_ACTIVATION,
                            BuilderParameters.SRC_LAYER_BIAS,
                            BuilderParameters.SRC_ARCH,
                            BuilderParameters.SRC_WINDOW,
                            BuilderParameters.SRC_LAYER_DROPOUT.Length == BuilderParameters.SRC_LAYER_DIM.Length ?
                                BuilderParameters.SRC_LAYER_DROPOUT : new float[BuilderParameters.SRC_LAYER_DIM.Length]
                            );
                modelStructure.AddLayer(SrcDnn);

                if (BuilderParameters.IsShareModel)
                {
                    TgtDnn = SrcDnn;
                }
                else
                {
                    TgtDnn = new DNNStructure(
                            DataPanel.TrainDocInput.Stat.FEATURE_DIM,
                            BuilderParameters.TGT_LAYER_DIM,
                            BuilderParameters.TGT_ACTIVATION,
                            BuilderParameters.TGT_LAYER_BIAS,
                            BuilderParameters.TGT_ARCH,
                            BuilderParameters.TGT_WINDOW,
                            BuilderParameters.TGT_LAYER_DROPOUT.Length == BuilderParameters.SRC_LAYER_DIM.Length ?
                                BuilderParameters.TGT_LAYER_DROPOUT : new float[BuilderParameters.SRC_LAYER_DIM.Length]
                            );
                    modelStructure.AddLayer(TgtDnn);
                }
                FeatureDnn = new DNNStructure(DataPanel.TrainMatchFea.Stat.MAX_FEATUREDIM,
                            BuilderParameters.JOINT_LAYER_DIM,
                            BuilderParameters.JOINT_ACTIVATION,
                            BuilderParameters.JOINT_LAYER_DROPOUT,
                            BuilderParameters.JOINT_LAYER_BIAS
                            );
                modelStructure.AddLayer(FeatureDnn);

                JointDnn = new DNNStructure(SrcDnn.NeuralLayers.Last() + TgtDnn.NeuralLayers.Last() + 1 + FeatureDnn.NeuralLayers.Last(),
                            new int[] { 100, 1 }, new A_Func[] { A_Func.Tanh, A_Func.Linear }, new bool[] { true, false });
                modelStructure.AddLayer(JointDnn);

            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, device);
                    int link = 0;
                    SrcDnn = (DNNStructure)modelStructure.CompositeLinks[link++];
                    if (BuilderParameters.IsShareModel)
                    {
                        TgtDnn = (DNNStructure)modelStructure.CompositeLinks[link++];
                    }
                    FeatureDnn = (DNNStructure)modelStructure.CompositeLinks[link++];
                    JointDnn = (DNNStructure)modelStructure.CompositeLinks[link++];
                }
            }

            ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainQueryInput, DataPanel.TrainDocInput, DataPanel.TrainMatchBin, DataPanel.TrainMatchFea,
                  SrcDnn, TgtDnn, FeatureDnn, JointDnn, modelStructure,  new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
            trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

            //DataPanel.Train.TrainBatchNum = BuilderParameters.DebugBatchNum;
            for (int i = 0; i < OptimizerParameters.Iteration; i++)
            {
                double loss = trainCG.Execute();// trainRunner.Train(DataPanel.Train.GetInstances(true)); // .TrainEpoch();
                Logger.WriteLog("Iteration {0} \t Train Loss {1}", i, loss);
                Logger.WriteLog("Test Set Evaluation!");
            }
            
            Logger.CloseLog();
        }

        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQueryInput = null;
            public static DataCashier<BiMatchBatchData, BiMatchBatchDataStat> TrainMatchBin = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainDocInput = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TrainMatchFea = null;


            public static void Init()
            {
                TrainQueryInput = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.QueryTrainData);
                TrainDocInput = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.DocTrainData);
                TrainMatchBin = new DataCashier<BiMatchBatchData, BiMatchBatchDataStat>(BuilderParameters.MatchTrainData);
                TrainMatchFea = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.MatchTrainData);

                TrainQueryInput.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                TrainDocInput.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                TrainMatchBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                TrainMatchFea.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
            }
        }

    }
}
