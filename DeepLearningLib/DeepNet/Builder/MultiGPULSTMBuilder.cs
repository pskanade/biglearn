﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class MultiGPULSTMBuilder : Builder
    {
        public override BuilderType Type { get { return BuilderType.MULTI_GPU_LSTM; } }

        public override void InitStartup(string fileName)
        {
            LSTMSeqLabelBuilderParameters.Parse(fileName);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation Data.");
            LSTMSeqLabelBuilder.DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            if (LSTMSeqLabelBuilderParameters.Device == DeviceType.GPU) Cudalib.CudaInit(LSTMSeqLabelBuilderParameters.GPUID);

            Logger.OpenLog(BuilderParameters.LogFile);
            MathOperatorManager.SetDevice(LSTMSeqLabelBuilderParameters.GPUID);
            Logger.WriteLog("Loading Training/Validation Data.");
            LSTMSeqLabelBuilder.DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            switch (LSTMSeqLabelBuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    LSTMStructure lstmModel = new LSTMStructure(LSTMSeqLabelBuilder.DataPanel.Train.Stat.FEATURE_DIM, LSTMSeqLabelBuilderParameters.LAYER_DIM,
                        LSTMSeqLabelBuilderParameters.Device);
                    LSTMLabelStructure lstmLabelModel = new LSTMLabelStructure(lstmModel, LSTMSeqLabelBuilderParameters.STACK, 1, LSTMSeqLabelBuilderParameters.Device);
                    ComputationGraph predCG = LSTMSeqLabelBuilder.BuildComputationGraph(LSTMSeqLabelBuilder.DataPanel.Valid, DNNRunMode.Predict, lstmLabelModel);

                    /// Multiple Computation Graphs for Data Partitions on Multiple GPUs.
                    /// Suppose the Computation Graph can be run on only one device.
                    
                    /// Global Model Parameter in CPU.
                    CudaPieceFloat modelParameter = new CudaPieceFloat(lstmLabelModel.ParamNumber, true, false);
                    lstmLabelModel.GetModelParameter(modelParameter, 0);
                    ParameterServer parameterServer = new ParameterServer(modelParameter, 5, 5);

                    /// Multiple Computation Graph for Data Partitions.
                    List<ComputationGraph> trainCGs = new List<ComputationGraph>();
                    List<Structure> trainCGStructure = new List<Structure>();

                    List<int> deviceList = new List<int>() {1, 3};// 1, 2, 3 };
                    for (int i = 0; i < deviceList.Count; i++)
                    {
                        //Cudalib.CudaSetDevice(deviceList[i]);

                        DeviceType threadDevice = MathOperatorManager.SetDevice(deviceList[i]);
                        IMathOperationManager threadComputelib = MathOperatorManager.CreateInstance(threadDevice);


                        LSTMStructure lstmModelDevice = new LSTMStructure(LSTMSeqLabelBuilder.DataPanel.Train.Stat.FEATURE_DIM, LSTMSeqLabelBuilderParameters.LAYER_DIM, threadDevice);
                        LSTMLabelStructure lstmLabelModelDevice = new LSTMLabelStructure(lstmModelDevice, LSTMSeqLabelBuilderParameters.STACK, 1, threadDevice);

                        lstmLabelModelDevice.InitOptimizer(new StructureLearner() { Optimizer = DNNOptimizerType.SGD, LearnRate = DSSMGraphParameters.LearnRate, device = threadDevice },
                                      new RunnerBehavior() { Device = threadDevice, Computelib = threadComputelib });

                        //AsyncSGDOptimizer optimizer = new AsyncSGDOptimizer(lstmLabelModelDevice, LSTMSeqLabelBuilderParameters.LearnRate, 0, new RunnerBehavior() { Device = LSTMSeqLabelBuilderParameters.Device } );

                        trainCGStructure.Add(lstmLabelModelDevice);

                        ///Now the computation graph will be in device (i);
                        ComputationGraph trainCG = LSTMSeqLabelBuilder.BuildComputationGraph(LSTMSeqLabelBuilder.DataPanel.Train, DNNRunMode.Train, lstmLabelModelDevice);
                        trainCGs.Add(trainCG);
                    }

                    if (!Directory.Exists(LSTMSeqLabelBuilderParameters.ModelOutputPath)) Directory.CreateDirectory(LSTMSeqLabelBuilderParameters.ModelOutputPath);

                    for (int iter = 0; iter < LSTMSeqLabelBuilderParameters.Iteration; iter++)
                    {
                        //double loss = trainCG.Execute();
                        parameterServer.ExecuteTrainMultipleComputationGraph(deviceList, trainCGs, trainCGStructure);
                        //Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        Cudalib.CudaSetDevice(LSTMSeqLabelBuilderParameters.GPUID);
                        lstmLabelModel.SetModelParameter(parameterServer.ModelParameter, 0);

                        if (LSTMSeqLabelBuilderParameters.IsValidFile)
                        {
                            predCG.Execute();
                        }
                        lstmLabelModel.Save(string.Format(@"{0}\\LSTM.{1}.model", LSTMSeqLabelBuilderParameters.ModelOutputPath, iter.ToString()));
                    }
                    ////DataPanel.Train,
                    break;
                case DNNRunMode.Predict:
                    LSTMLabelStructure validlstmLabelModel = new LSTMLabelStructure(
                        new BinaryReader(new FileStream(LSTMSeqLabelBuilderParameters.ModelOutputPath, FileMode.Open, FileAccess.Read)), LSTMSeqLabelBuilderParameters.Device);

                    if (LSTMSeqLabelBuilderParameters.IsValidFile)
                    {
                        ComputationGraph validCG = LSTMSeqLabelBuilder.BuildComputationGraph(LSTMSeqLabelBuilder.DataPanel.Valid, DNNRunMode.Predict, validlstmLabelModel);
                        validCG.Execute();
                        Logger.WriteLog(string.Join("\n", AUCEvaluationSet.EvaluateAUC(LSTMSeqLabelBuilderParameters.ScoreOutputPath, LSTMSeqLabelBuilderParameters.MetricOutputPath).Item1));
                    }
                    break;
            }

        }
    }
}
