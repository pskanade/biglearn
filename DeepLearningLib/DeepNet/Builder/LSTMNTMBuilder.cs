﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    /// <summary>
    /// LSTM Neural Turing Machine.
    /// Add Attention. 
    /// </summary>
    public class LSTMNTMBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "LSTM Layer Dim"));

                Argument.Add("NN-ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("NN-LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
                Argument.Add("NN-LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("NN-LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));

                Argument.Add("MEMORYKEY-LAYER-DIM", new ParameterArgument("1", "Memory Key Layer Dim."));

                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("DELTA-CLIP", new ParameterArgument("0", "Model Update Clip."));
                Argument.Add("WEIGHT-CLIP", new ParameterArgument("0", "Model Weight Clip"));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
                Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
                Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
                Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));

                Argument.Add("CHECK-GRAD", new ParameterArgument("0", "Check Gradient!"));
                Argument.Add("DEBUG-TRAIN", new ParameterArgument("0", "Debugset for Training"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }

            public static int Debug_Train { get { return int.Parse(Argument["DEBUG-TRAIN"].Value); } }


            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }


            public static int[] NN_LAYER_DIM { get { return Argument["NN-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] NN_ACTIVATION { get { return Argument["NN-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] NN_LAYER_BIAS { get { return Argument["NN-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static float[] NN_LAYER_DROPOUT { get { return Argument["NN-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static int[] MEMORYKEY_LAYER_DIM { get { return Argument["MEMORYKEY-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static float DELTACLIP { get { return float.Parse(Argument["DELTA-CLIP"].Value); } }
            public static float WEIGHTCLIP { get { return float.Parse(Argument["WEIGHT-CLIP"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

            public static string ScoreOutputPath
            {
                get
                {
                    if (RunMode == DNNRunMode.Predict) return Argument["SCORE-PATH"].Value;
                    else return LogFile + ".tmp.score";
                }
            }

            public static string MetricOutputPath
            {
                get
                {
                    if (RunMode == DNNRunMode.Predict) return Argument["METRIC-PATH"].Value;
                    else return LogFile + ".tmp.metric";
                }
            }

            public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
            public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
            public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
            public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
            public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }

            public static bool CheckGrad { get { return int.Parse(Argument["CHECK-GRAD"].Value) > 0; } }
        }

        public override BuilderType Type { get { return BuilderType.LSTMNTM; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
            Cudalib.CudaInit(BuilderParameters.GPUID);
        }

        public ComputationGraph BuildComputationGraph(DataCashier<SeqSparseDataSource, SequenceDataStat> data, DNNRunMode mode,
            LSTMStructure lstmModel, List<LayerStructure> memoryLinks, List<LayerStructure> embedLinks, List<LayerStructure> keyLinks)
        {
            ComputationGraph cg = new ComputationGraph();

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            CudaMathOperation lib = new CudaMathOperation(true, true);
            RunnerBehavior Behavior = new RunnerBehavior() { RunMode = mode, Device = device, Computelib = lib };

            SeqSparseDataSource dataSource = (SeqSparseDataSource)cg.AddRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(data, Behavior));

            //SeqHelpData SrcHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(dataSource.SequenceData, Behavior));

            SeqDenseRecursiveData lstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(lstmModel.LSTMCells[0], dataSource.SequenceData, false, Behavior));
            for (int i = 1; i < lstmModel.LSTMCells.Count; i++)
                lstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], lstmOutput, Behavior));

            HiddenBatchData embedOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(lstmOutput, Behavior));
            
            for (int i = 0; i < embedLinks.Count; i++)
            {
                ///Generate Key for embedOutput.
                HiddenBatchData memoryKey = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(keyLinks[i], embedOutput, Behavior));

                ///From key to memory output.
                HiddenBatchData memoryOutput = (HiddenBatchData)cg.AddRunner(new MemoryControlRunner<BatchData>(lstmOutput, memoryKey, 5, Behavior));

                /// joint connect poolingOutput and memoryOutput for next layer.
                HiddenBatchData embed1 = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(embedLinks[i], embedOutput, 
                    new RunnerBehavior() { RunMode = mode, Device = DeviceType.GPU }));
                
                HiddenBatchData embed2 = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(memoryLinks[i], memoryOutput, 
                    new RunnerBehavior() { RunMode = mode, Device = DeviceType.GPU }));

                if (i == embedLinks.Count - 1)
                {
                    embedOutput = (HiddenBatchData)cg.AddRunner(new AdditionRunner(embed1, embed2,
                        new RunnerBehavior() { RunMode = mode, Device = DeviceType.GPU }));
                }
                else
                {
                    HiddenBatchData embedJoint = (HiddenBatchData)cg.AddRunner(new AdditionRunner(embed1, embed2,
                        new RunnerBehavior() { RunMode = mode, Device = DeviceType.GPU }));

                    embedOutput = (HiddenBatchData)cg.AddRunner(new ActivationRunner(embedJoint, A_Func.Tanh,
                        new RunnerBehavior() { RunMode = mode, Device = DeviceType.GPU }));
                }
            }
            
            switch (mode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new CrossEntropyRunner(dataSource.InstanceLabel, embedOutput.Output, embedOutput.Deriv, BuilderParameters.Gamma, new RunnerBehavior() { RunMode = mode, Device = DeviceType.GPU }));
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new AUCDiskDumpRunner(dataSource.InstanceLabel, embedOutput.Output, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    
                    /// the example of building the computational graph. 

                    /// construct model parameters.
                    CompositeNNStructure deepModel = new CompositeNNStructure();
                    LSTMStructure lstmModel = new LSTMStructure(DataPanel.Train.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM);
                    deepModel.AddLayer(lstmModel);

                    List<LayerStructure> keyLinks = new List<LayerStructure>();
                    List<LayerStructure> embedLinks = new List<LayerStructure>();
                    List<LayerStructure> memoryLinks = new List<LayerStructure>();

                    int inputEmbed = BuilderParameters.LAYER_DIM.Last();
                    for (int i = 0; i < BuilderParameters.NN_LAYER_DIM.Length; i++ )
                    {
                        keyLinks.Add(new LayerStructure(inputEmbed, BuilderParameters.MEMORYKEY_LAYER_DIM[i], 
                            A_Func.Sigmoid, N_Type.Fully_Connected, 1, 0, true));

                        embedLinks.Add(new LayerStructure(inputEmbed, BuilderParameters.NN_LAYER_DIM[i],
                            A_Func.Linear, N_Type.Fully_Connected, 1, 0, true));

                        memoryLinks.Add(new LayerStructure(BuilderParameters.LAYER_DIM.Last(), BuilderParameters.NN_LAYER_DIM[i],
                           A_Func.Linear, N_Type.Fully_Connected, 1, 0, true));

                        inputEmbed = BuilderParameters.NN_LAYER_DIM[i];

                        keyLinks[i].Init();
                        embedLinks[i].Init();
                        memoryLinks[i].Init();
                        deepModel.AddLayer(keyLinks[i]);
                        deepModel.AddLayer(embedLinks[i]);
                        deepModel.AddLayer(memoryLinks[i]);
                    }

                    deepModel.InitOptimizer(new StructureLearner()
                    {
                        Optimizer = BuilderParameters.Optimizer,
                        LearnRate = BuilderParameters.LearnRate,
                        AdaBoost = BuilderParameters.AdaBoost,
                        ClipDelta = BuilderParameters.DELTACLIP,
                        ClipWeight = BuilderParameters.WEIGHTCLIP
                    });

                    /// construct single source computational graph.
                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train, DNNRunMode.Train, lstmModel, memoryLinks, embedLinks, keyLinks);
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.Valid, DNNRunMode.Predict, lstmModel, memoryLinks, embedLinks, keyLinks);
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);


                    for (int iter = 0; iter < BuilderParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss / (DataPanel.Train.Stat.TotalBatchNumber + float.Epsilon));

                        if (BuilderParameters.IsValidFile) predCG.Execute();
                        /// Model Serialization.
                        //lstmLabelModel.Save(string.Format(@"{0}\\LSTM.{1}.model", LSTMSeqLabelBuilderParameters.ModelOutputPath, iter.ToString()));
                    }
                    break;
                case DNNRunMode.Predict:
                    //...
                    break;
            }
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Train = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Valid = null;

            public static void Init()
            {
                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainData);
                    Train.InitThreadSafePipelineCashier(100, true);
                }
                if (BuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidData);
                    Valid.InitThreadSafePipelineCashier(100, false);
                }
            }

            internal static void DeInit()
            {
                if (null != Train) Train.Dispose(); Train = null;
                if (null != Valid) Valid.Dispose(); Valid = null;
            }
        }
    }
}
