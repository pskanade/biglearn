﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class KnowledgeGraphRecurrentAttentionBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static string Entity2Id { get { return Argument["ENTITY2ID"].Value; } }
            public static string Relation2Id { get { return Argument["RELATION2ID"].Value; } }

            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static string TestData { get { return Argument["TEST"].Value; } }

            public static bool IsValidFile { get { return !(ValidData.Equals(string.Empty)); } }
            public static bool IsTestFile { get { return !(TestData.Equals(string.Empty)); } }

            public static int EmbedDim { get { return int.Parse(Argument["EMBED-DIM"].Value); } }

            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static int NTrail { get { return int.Parse(Argument["NTRAIL"].Value); } }
            public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }
            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }
            public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
            public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
            public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
            public static float DELTACLIP { get { return float.Parse(Argument["DELTA-CLIP"].Value); } }
            public static float WEIGHTCLIP { get { return float.Parse(Argument["WEIGHT-CLIP"].Value); } }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
            public static int BatchIteration { get { return int.Parse(Argument["BATCH-ITERATION"].Value); } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static string ScoreOutputPath
            {
                get
                {
                    return Argument["SCORE-PATH"].Value.Equals(string.Empty) ? LogFile + ".tmp.score" : Argument["SCORE-PATH"].Value;
                }
            }
            public static string MetricOutputPath
            {
                get
                {
                    return Argument["METRIC-PATH"].Value.Equals(string.Empty) ? LogFile + ".tmp.metric" : Argument["METRIC-PATH"].Value;
                }
            }
            public static DeviceType Device
            {
                get
                {
                    if (GPUID >= 0)
                    {
                        return DeviceType.GPU;
                    }
                    else if (GPUID == -1)
                    {
                        return DeviceType.CPU;
                    }
                    else
                    {
                        return DeviceType.CPU_FAST_VECTOR;
                    }
                }
            }

            static BuilderParameters()
            {
                Argument.Add("ENTITY2ID", new ParameterArgument(string.Empty, "Entity 2 ID."));
                Argument.Add("RELATION2ID", new ParameterArgument(string.Empty, "Relation 2 ID."));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));
                Argument.Add("EMBED-DIM", new ParameterArgument("100", "Embedding Dim"));


                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-VERSION", new ParameterArgument(((int)DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR).ToString(), ParameterUtil.EnumValues(typeof(DNNModelVersion))));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SHARE-MODEL", new ParameterArgument("0", "Share Source and Target Model; 0 : no share; 1 : share;"));
                Argument.Add("SIM-TYPE", new ParameterArgument(((int)SimilarityType.CosineSimilarity).ToString(), ParameterUtil.EnumValues(typeof(SimilarityType))));

                Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.SampleSoftmax).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));
                Argument.Add("NTRAIL", new ParameterArgument("20", "Negative Sample Number."));

                Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "DSSM Learn Rate."));
                Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.AdaGrad).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));
                Argument.Add("ADABOOST", new ParameterArgument("0.01", "Ada Gradient Adjustment."));
                Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
                Argument.Add("DELTA-CLIP", new ParameterArgument("0", "Model Update Clip."));
                Argument.Add("WEIGHT-CLIP", new ParameterArgument("0", "Model Weight Clip"));

                Argument.Add("GPUID", new ParameterArgument("1", "GPU Device ID"));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
                Argument.Add("BATCH-ITERATION", new ParameterArgument("20", "Train Batch Iteration."));
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.NDCG).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));
                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument(string.Empty, "Output Metric Path"));

                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));
            }
        }
        public override BuilderType Type { get { return BuilderType.RECURRENT_ATTENTION_GRAPH_EMBED; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static List<Tuple<int, float>> ScheduleEpsilon(string schedule)
        {
            return schedule.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
        }


        public float EpsSchedule(string schedule, int step)
        {
            List<Tuple<int, float>> scheduleList = ScheduleEpsilon(schedule);
            for (int i = 0; i < scheduleList.Count; i++)
            {
                if (step < scheduleList[i].Item1)
                {
                    float lambda = (step - scheduleList[i - 1].Item1) * 1.0f / (scheduleList[i].Item1 - scheduleList[i - 1].Item1);
                    return lambda * scheduleList[i].Item2 + (1 - lambda) * scheduleList[i - 1].Item2;
                }
            }
            return scheduleList.Last().Item2;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            MathOperatorManager.MathDevice = DeviceType.CPU_FAST_VECTOR;
            RunnerBehavior behavior = new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR, RunMode = DNNRunMode.Train };

            DeepGraphEmbedingStruct DeepEmbed = new DeepGraphEmbedingStruct(DataPanel.knowledgeGraph, 100, 100, 0);
            
            DeepEmbed.MaxNeighbor = 10;
            string schedule = "0:0.99,100000:0.8,1000000:0.4,10000000:0.1";
            //foreach (ModelOptimizer optimizer in DeepEmbed.GlobalOptimizers)
            //    optimizer.Optimizer = new SparseGradientOptimizer(optimizer.Parameter, 0.01f, DeviceType.CPU_FAST_VECTOR);

            DeepEmbed.EntityOptimizer = new SparseGradientOptimizer(DeepEmbed.EntityVec, true, 0.005f, behavior);
            DeepEmbed.RelationOptimizer = new SparseGradientOptimizer(DeepEmbed.RelationVec, true, 0.005f, behavior);

            DeepEmbed.QueryEmbedOptimizer = new ClipSGDOptimizer(DeepEmbed.QueryAttentionEmbed, 0.005f, 0.1f, 5, behavior);
            DeepEmbed.RelationEmbedOptimizer = new ClipSGDOptimizer(DeepEmbed.RelationAttentionEmbed, 0.005f, 0.1f, 5, behavior);
            DeepEmbed.OutputEmbedOptimizer = new ClipSGDOptimizer(DeepEmbed.OutputAttentionVec, 0.005f, 0.1f, 5, behavior);

            bool TestFirst = false;
            bool IsL1Flag = true;
            int NegVersion = 0;
            int learnSampleCount = 0;


            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);


            PerfCounter.Manager instance = new PerfCounter.Manager();

            for (int iter = 0; iter < 1000; iter++)
            {
                int rowCount = 0;
                double loss = 0;

                if (!TestFirst)
                {
                    DataRandomShuffling Shuffle = new DataRandomShuffling(DataPanel.Train.Count);

                    while (true)
                    {
                        int id = Shuffle.RandomNext();
                        if (id == -1) break;

                        learnSampleCount += 1;
                        DeepEmbed.Eps = EpsSchedule(schedule, learnSampleCount);

                        int leftEntityId = DataPanel.Train[id].Item1;
                        int rightEntityId = DataPanel.Train[id].Item2;
                        int relationId = DataPanel.Train[id].Item3;

                        int head;
                        int tail;
                        int connection;
                        int negtail;

                        var trainSampleTimer = instance["Train"].Begin();


                        double pr = NegVersion == 0 ? 0.5 : DataPanel.TailProb[relationId] / (DataPanel.TailProb[relationId] + DataPanel.HeadProb[relationId]);
                        if (ParameterSetting.Random.NextDouble() < pr)
                        {
                            head = leftEntityId;
                            connection = 1;
                            tail = rightEntityId;
                            do
                            {
                                negtail = ParameterSetting.Random.Next(DataPanel.EntityNum);
                            } while (DataPanel.IsInGraph(head, negtail, relationId));
                        }
                        else
                        {
                            head = rightEntityId;
                            connection = -1;
                            tail = leftEntityId;
                            do
                            {
                                negtail = ParameterSetting.Random.Next(DataPanel.EntityNum);
                            } while (DataPanel.IsInGraph(negtail, head, relationId));
                        }


                        DeepEmbed.SetBlackConnection(leftEntityId, relationId, rightEntityId);

                        QAData qaData = new QAData();


                        qaData.Query = new HiddenBatchData(1, DeepEmbed.Dim, DNNRunMode.Train, DeviceType.CPU_FAST_VECTOR);
                        FastVector.Add_Vector(qaData.Query.Output.Data.MemPtr, 0, DeepEmbed.RelationVec.MemPtr, relationId * DeepEmbed.Dim,
                            DeepEmbed.Dim, 0, connection);

                        var nodeRunnerTimer = instance["NodeRunner"].Begin();
                        GraphNodeRunner nodeRunner = new GraphNodeRunner(DeepEmbed, head, qaData, behavior);
                        nodeRunner.Forward();
                        instance["NodeRunner"].TakeCount(nodeRunnerTimer);

                        double PosSum = 0;
                        float[] wPos = new float[DeepEmbed.Dim];
                        for (int ii = 0; ii < DeepEmbed.Dim; ii++)
                        {
                            wPos[ii] = DeepEmbed.EntityVec.MemPtr[tail * DeepEmbed.Dim + ii] - qaData.Answer.Output.Data.MemPtr[ii];
                            if (IsL1Flag) PosSum += Math.Abs(wPos[ii]);
                            else PosSum += wPos[ii] * wPos[ii];
                        }

                        double NegSum = 0;
                        float[] wNeg = new float[DeepEmbed.Dim];
                        for (int ii = 0; ii < DeepEmbed.Dim; ii++)
                        {
                            wNeg[ii] = DeepEmbed.EntityVec.MemPtr[negtail * DeepEmbed.Dim + ii] - qaData.Answer.Output.Data.MemPtr[ii];
                            if (IsL1Flag) NegSum += Math.Abs(wNeg[ii]);
                            else NegSum += wNeg[ii] * wNeg[ii];
                        }

                        if (PosSum + 1.0f > NegSum)
                        {
                            foreach (ModelOptimizer optimizer in DeepEmbed.ModelOptimizers)
                                optimizer.Optimizer.BeforeGradient();

                            //PosDeriv = -1;
                            //NegDeriv = 1;
                            loss += 1.0f + PosSum - NegSum;
                            nodeRunner.CleanDeriv();

                            for (int ii = 0; ii < DeepEmbed.Dim; ii++)
                            {
                                float pDeriv = 0;
                                float nDeriv = 0;
                                if (IsL1Flag)
                                {
                                    if (wPos[ii] > 0) pDeriv = 1;
                                    else if (wPos[ii] < 0) pDeriv = -1;

                                    if (wNeg[ii] > 0) nDeriv = -1;
                                    else if (wNeg[ii] < 0) nDeriv = 1;
                                }
                                else
                                {
                                    pDeriv = 2 * wPos[ii];
                                    nDeriv = -2 * wNeg[ii];
                                }
                                qaData.Answer.Deriv.Data.MemPtr[ii] = pDeriv + nDeriv;
                                wPos[ii] = pDeriv;
                                wNeg[ii] = nDeriv;
                            }

                            nodeRunner.Backward(false);

                            ((SparseGradientOptimizer)DeepEmbed.RelationOptimizer).PushGradientIndex(relationId * DeepEmbed.Dim, DeepEmbed.Dim);
                            FastVector.Add_Vector(DeepEmbed.RelationOptimizer.Gradient.MemPtr, relationId * DeepEmbed.Dim,
                                qaData.Query.Deriv.Data.MemPtr, 0, DeepEmbed.Dim, 1, connection * DeepEmbed.EntityOptimizer.GradientStep);

                            ((SparseGradientOptimizer)DeepEmbed.EntityOptimizer).PushGradientIndex(tail * DeepEmbed.Dim, DeepEmbed.Dim);
                            FastVector.Add_Vector(DeepEmbed.EntityOptimizer.Gradient.MemPtr, tail * DeepEmbed.Dim,
                                wPos, 0, DeepEmbed.Dim, 1, -1 * DeepEmbed.EntityOptimizer.GradientStep);

                            ((SparseGradientOptimizer)DeepEmbed.EntityOptimizer).PushGradientIndex(negtail * DeepEmbed.Dim, DeepEmbed.Dim);
                            FastVector.Add_Vector(DeepEmbed.EntityOptimizer.Gradient.MemPtr, negtail * DeepEmbed.Dim,
                                wNeg, 0, DeepEmbed.Dim, 1, -1 * DeepEmbed.EntityOptimizer.GradientStep);

                            nodeRunner.Update();

                            foreach (ModelOptimizer optimizer in DeepEmbed.ModelOptimizers)
                                optimizer.Optimizer.AfterGradient();
                        }

                        DeepEmbed.ClearBlackConnection();
                        instance["Train"].TakeCount(trainSampleTimer);

                        if (++rowCount % 1000 == 0)
                        {
                            Logger.WriteLog("Avg Loss {0}, Count {1}, Iteration {2}", loss * 1.0f / rowCount, rowCount, iter);
                            Logger.WriteLog("Perf " + instance["Train"].ToString() + "\n");
                            Logger.WriteLog("Perf " + instance["NodeRunner"].ToString() + "\n");
                        }
                    }
                }
                string modelName = string.Format(@"{0}\\KnowledgeGraphEmbedding.{1}.model", BuilderParameters.ModelOutputPath, iter);
                DeepEmbed.Save(modelName);

                TestFirst = false;
                if ((iter + 1) % 50 == 0)
                {
                    int lsum = 0, lsum_filter = 0;
                    int rsum = 0, rsum_filter = 0;
                    int lp_n = 0, lp_n_filter = 0;
                    int rp_n = 0, rp_n_filter = 0;
                    int HitK = 10;

                    DeepEmbed.Eps = 0;
                    // Test.
                    //Parallel.For(0, DataPanel.knowledgeGraph.Test.Count, itemIdx => // 
                    for (int itemIdx = 0; itemIdx < DataPanel.knowledgeGraph.Test.Count; itemIdx++)
                    {
                        int head = DataPanel.knowledgeGraph.Test[itemIdx].Item1;
                        int tail = DataPanel.knowledgeGraph.Test[itemIdx].Item2;
                        int r = DataPanel.knowledgeGraph.Test[itemIdx].Item3;

                        int connection = 1;
                        QAData qaData = new QAData();
                        qaData.Query = new HiddenBatchData(1, DeepEmbed.Dim, DNNRunMode.Predict, DeviceType.CPU_FAST_VECTOR);
                        FastVector.Add_Vector(qaData.Query.Output.Data.MemPtr, 0, DeepEmbed.RelationVec.MemPtr, r * DeepEmbed.Dim,
                            DeepEmbed.Dim, 0, connection);
                        GraphNodeRunner nodeRunner = new GraphNodeRunner(DeepEmbed, head, qaData, behavior);
                        nodeRunner.Forward();

                        float[] tscores = new float[DeepEmbed.EntityNum];
                        //for (int e = 0; e < DeepEmbed.EntityNum; e++)
                        //{
                        Parallel.For(0, DeepEmbed.EntityNum, new ParallelOptions() { MaxDegreeOfParallelism = 32 } ,e =>
                        {
                            float dvalue = 0;
                            for (int ii = 0; ii < DeepEmbed.Dim; ii++)
                            {
                                float tmp = DeepEmbed.EntityVec.MemPtr[e * DeepEmbed.Dim + ii] - qaData.Answer.Output.Data.MemPtr[ii];
                                if (IsL1Flag) dvalue += Math.Abs(tmp);
                                else dvalue += tmp * tmp;
                            }
                            tscores[e] = dvalue;
                        });

                        int[] lindicies = Enumerable.Range(0, DeepEmbed.EntityNum).ToArray();
                        Array.Sort(tscores, lindicies);

                        int filter = 0;
                        for (int c = DeepEmbed.EntityNum - 1; c >= 0; c--)
                        {
                            int ctail = lindicies[DeepEmbed.EntityNum - 1 - c];
                            if (!DataPanel.IsInGraph(head, ctail, r)) filter++;
                            if (ctail == tail)
                            {
                                Interlocked.Add(ref lsum, DeepEmbed.EntityNum - c);
                                Interlocked.Add(ref lsum_filter, filter + 1);
                                if (DeepEmbed.EntityNum - c <= HitK) Interlocked.Increment(ref lp_n); // lp_n += 1;
                                if (filter < HitK) Interlocked.Increment(ref lp_n_filter); // += 1;
                                break;
                            }
                        }

                        connection = -1;
                        QAData rqaData = new QAData();
                        rqaData.Query = new HiddenBatchData(1, DeepEmbed.Dim, DNNRunMode.Predict, DeviceType.CPU_FAST_VECTOR);
                        FastVector.Add_Vector(rqaData.Query.Output.Data.MemPtr, 0, DeepEmbed.RelationVec.MemPtr, r * DeepEmbed.Dim,
                            DeepEmbed.Dim, 0, connection);
                        GraphNodeRunner rnodeRunner = new GraphNodeRunner(DeepEmbed, tail, rqaData, behavior);
                        rnodeRunner.Forward();

                        float[] hscores = new float[DeepEmbed.EntityNum];
                        //for (int e = 0; e < DeepEmbed.EntityNum; e++)
                        //{
                        Parallel.For(0, DeepEmbed.EntityNum, new ParallelOptions() { MaxDegreeOfParallelism = 32 }, e =>
                        {
                            float dvalue = 0;
                            for (int ii = 0; ii < DeepEmbed.Dim; ii++)
                            {
                                float tmp = DeepEmbed.EntityVec.MemPtr[e * DeepEmbed.Dim + ii] - rqaData.Answer.Output.Data.MemPtr[ii];
                                if (IsL1Flag) dvalue += Math.Abs(tmp);
                                else dvalue += tmp * tmp;
                            }
                            hscores[e] = dvalue;
                        });

                        int[] hindicies = Enumerable.Range(0, DeepEmbed.EntityNum).ToArray();
                        Array.Sort(hscores, hindicies);

                        filter = 0;
                        for (int c = DeepEmbed.EntityNum - 1; c >= 0; c--)
                        {
                            int chead = hindicies[DeepEmbed.EntityNum - 1 - c];
                            if (!DataPanel.IsInGraph(chead, tail, r)) filter++;
                            if (chead == head)
                            {
                                Interlocked.Add(ref rsum, DeepEmbed.EntityNum - c);
                                Interlocked.Add(ref rsum_filter, filter + 1);
                                if (DeepEmbed.EntityNum - c <= HitK) Interlocked.Increment(ref rp_n); // lp_n += 1;
                                if (filter < HitK) Interlocked.Increment(ref rp_n_filter); // += 1;
                                break;
                            }
                        }
                        if((itemIdx + 1) % 1000 == 0)
                        {
                            Logger.WriteLog(string.Format("Test steps {0}", itemIdx));
                            Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 1.0 / itemIdx));
                            Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 1.0 / itemIdx));
                            Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 1.0 / itemIdx));
                            Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 1.0 / itemIdx));

                            Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 1.0 / itemIdx));
                            Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 1.0 / itemIdx));
                            Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 1.0 / itemIdx));
                            Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 1.0 / itemIdx));

                            Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 0.5 / itemIdx));
                            Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 0.5 / itemIdx));
                            Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 0.5 / itemIdx));
                            Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 0.5 / itemIdx));
                        }
                    }//);
                    //});

                    int totalSampleNum = DataPanel.Test.Count;
                    Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 1.0 / totalSampleNum));

                    Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 1.0 / totalSampleNum));

                    Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 0.5 / totalSampleNum));
                    Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 0.5 / totalSampleNum));
                    Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 0.5 / totalSampleNum));
                    Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 0.5 / totalSampleNum));
                }
            }
        }

        public class DeepGraphEmbedingStruct : Structure
        {
            public CudaPieceFloat EntityVec;
            public CudaPieceFloat RelationVec;

            public CudaPieceFloat QueryAttentionEmbed;
            public CudaPieceFloat RelationAttentionEmbed;
            public CudaPieceFloat OutputAttentionVec;


            public GradientOptimizer EntityOptimizer { get { return StructureOptimizer["EntityVec"].Optimizer; } set { StructureOptimizer["EntityVec"].Optimizer = value; } }
            public GradientOptimizer RelationOptimizer { get { return StructureOptimizer["RelationVec"].Optimizer; } set { StructureOptimizer["RelationVec"].Optimizer = value; } }

            public GradientOptimizer RelationEmbedOptimizer { get { return StructureOptimizer["RelationAttentionEmbed"].Optimizer; } set { StructureOptimizer["RelationAttentionEmbed"].Optimizer = value; } }
            public GradientOptimizer QueryEmbedOptimizer { get { return StructureOptimizer["QueryAttentionEmbed"].Optimizer; } set { StructureOptimizer["QueryAttentionEmbed"].Optimizer = value; } }

            public GradientOptimizer OutputEmbedOptimizer { get { return StructureOptimizer["OutputAttentionVec"].Optimizer; } set { StructureOptimizer["OutputAttentionVec"].Optimizer = value; } }



            public int EntityNum;
            public int RelationNum;
            public int Dim;
            public int AttentionHiddenDim;

            public int MaxHop = 1;
            public float ExploreDiscount = 0.8f;
            public float Eps = 0.9f;
            public int MaxNeighbor = 5;
            public RelationGraphData GraphData;


            int blackeid1 { get; set; }
            int blackrid { get; set; }
            int blackeid2 { get; set; }
            public bool IsBlackConnection(int eid1, int rid, int eid2)
            {
                if (blackeid1 == eid1 && blackeid2 == eid2 && blackrid == rid)
                    return true;
                return false;
            }

            public void SetBlackConnection(int eid1, int rid, int eid2)
            {
                blackeid1 = eid1;
                blackeid2 = eid2;
                blackrid = rid;
            }
            public void ClearBlackConnection()
            {
                blackeid1 = -1;
                blackrid = -1;
                blackeid2 = -1;
            }

            public void NormEntity()
            {
                Parallel.For(0, EntityNum, i =>
                {
                    double x = Math.Sqrt(EntityVec.MemPtr.Skip(i * Dim).Take(Dim).Select(v => v * v).Sum());
                    if (x > 1)
                        for (int ii = 0; ii < Dim; ii++)
                            EntityVec.MemPtr[i * Dim + ii] = (float)(EntityVec.MemPtr[i * Dim + ii] / x);
                });
            }

            public override void Serialize(BinaryWriter writer)
            {
                writer.Write(EntityNum);
                writer.Write(RelationNum);
                writer.Write(Dim);
                writer.Write(AttentionHiddenDim);

                EntityVec.Serialize(writer);
                RelationVec.Serialize(writer);

                QueryAttentionEmbed.Serialize(writer);
                RelationAttentionEmbed.Serialize(writer);
                OutputAttentionVec.Serialize(writer);

            }

            public override void Deserialize(BinaryReader reader, DeviceType device)
            {
                EntityNum = reader.ReadInt32();
                RelationNum = reader.ReadInt32();
                Dim = reader.ReadInt32();
                AttentionHiddenDim = reader.ReadInt32();

                EntityVec = new CudaPieceFloat(reader, device);
                RelationVec = new CudaPieceFloat(reader, device);

                QueryAttentionEmbed = new CudaPieceFloat(reader, device);
                RelationAttentionEmbed = new CudaPieceFloat(reader, device);
                OutputAttentionVec = new CudaPieceFloat(reader, device);
            }

            public DeepGraphEmbedingStruct(RelationGraphData graphData, int dim, int attentionEmbed, int maxHop)
            {
                GraphData = graphData;

                Dim = dim;
                AttentionHiddenDim = attentionEmbed;
                EntityNum = GraphData.EntityNum;
                RelationNum = GraphData.RelationNum;

                MaxHop = maxHop;

                EntityVec = new CudaPieceFloat(EntityNum * Dim, true, false);
                EntityVec.Init((float)(12.0f / Math.Sqrt(Dim)), (float)(-6.0f / Math.Sqrt(Dim)));
                NormEntity();

                RelationVec = new CudaPieceFloat(RelationNum * Dim, true, false);
                RelationVec.Init((float)(12.0f / Math.Sqrt(Dim)), (float)(-6.0f / Math.Sqrt(Dim)));

                QueryAttentionEmbed = new CudaPieceFloat(Dim * AttentionHiddenDim, true, false);
                QueryAttentionEmbed.Init((float)(12.0f / Math.Sqrt(Dim + AttentionHiddenDim)), 
                                          (float)(-6.0f / Math.Sqrt(Dim + AttentionHiddenDim)));

                RelationAttentionEmbed = new CudaPieceFloat(Dim * AttentionHiddenDim, true, false);
                RelationAttentionEmbed.Init((float)(12.0f / Math.Sqrt(Dim + AttentionHiddenDim)),
                                          (float)(-6.0f / Math.Sqrt(Dim + AttentionHiddenDim)));

                OutputAttentionVec = new CudaPieceFloat(AttentionHiddenDim, true, false);
                OutputAttentionVec.Init((float)(12.0f / Math.Sqrt(AttentionHiddenDim)), 
                                        (float)(-6.0f / Math.Sqrt(AttentionHiddenDim)));

                DeviceType = DeviceType.CPU;
            }

            public DeepGraphEmbedingStruct(BinaryReader reader, DeviceType device) : base(reader, device) { }

            protected override void InitStructureOptimizer()
            {
                StructureOptimizer.Add("EntityVec", new ModelOptimizer() { Parameter = EntityVec });
                StructureOptimizer.Add("RelationVec", new ModelOptimizer() { Parameter = RelationVec });
                StructureOptimizer.Add("RelationAttentionEmbed", new ModelOptimizer() { Parameter = RelationAttentionEmbed });
                StructureOptimizer.Add("QueryAttentionEmbed", new ModelOptimizer() { Parameter = QueryAttentionEmbed });
                StructureOptimizer.Add("OutputAttentionVec", new ModelOptimizer() { Parameter = OutputAttentionVec });

            }
        }


        public class QAData
        {
            public string Tag;
            public int Step;
            public HiddenBatchData Query;
            public HiddenBatchData Answer;
            public bool IsExpandGate;

            public HiddenBatchData GreenAnswer;
            public HiddenBatchData QueryHidden;

            public List<Tuple<GraphNodeRunner, QAData>> NeiQAData = new List<Tuple<GraphNodeRunner, QAData>>();

            public HiddenBatchData AttentionWeight;
            public int[] NeiSelectAttention = null;
            public ComputationGraph CG = new ComputationGraph();
        }

        public class SparseGradientOptimizer : GradientOptimizer
        {
            Dictionary<int, int> GradientIndex = new Dictionary<int, int>();
            bool IsNorm = true;
            public SparseGradientOptimizer(CudaPieceFloat weight, bool isNorm, float learnRate, RunnerBehavior behavior) : base(behavior)
            {
                UpdateRate = learnRate;
                GradientStep = 1.0f;
                Parameter = weight;
                IsNorm = isNorm;
                Gradient = new CudaPieceFloat(weight.Size, true, behavior.Device == DeviceType.GPU);
            }

            public virtual void PushGradientIndex(int idx, int range)
            {
                if (GradientIndex.ContainsKey(idx)) GradientIndex[idx] = Math.Max(GradientIndex[idx], range);
                else GradientIndex[idx] = range;
            }

            public override void BeforeGradient()
            {
                MathOperatorManager.GlobalInstance.Zero(Gradient, Gradient.Size);
                GradientIndex.Clear();
            }

            public override void AfterGradient()
            {
                Gradient.SyncToCPU();
                Parallel.ForEach(GradientIndex, gIdx =>
                {
                    for (int s = 0; s < gIdx.Value; s++)
                    {
                        Parameter.MemPtr[gIdx.Key + s] += UpdateRate * Gradient.MemPtr[gIdx.Key + s];
                    }

                    if (IsNorm)
                    {
                        double x = 0;
                        for (int s = 0; s < gIdx.Value; s++)
                        {
                            x = x + Parameter.MemPtr[gIdx.Key + s] * Parameter.MemPtr[gIdx.Key + s];
                        }
                        x = Math.Sqrt(x);
                        if (x > 1)
                            for (int ii = 0; ii < gIdx.Value; ii++)
                                Parameter.MemPtr[gIdx.Key + ii] = (float)(Parameter.MemPtr[gIdx.Key + ii] / x);
                    }

                });

                Parameter.SyncFromCPU();
            }
        }

        public class SimpleAnswerRunner : StructRunner
        {
            public DeepGraphEmbedingStruct GraphEmbed;
            public int NodeID { get; protected set; }

            public HiddenBatchData Query { get; set; }
            public HiddenBatchData Answer { get; set; }
            public SimpleAnswerRunner(DeepGraphEmbedingStruct graphEmbed, int nodeID, HiddenBatchData query, HiddenBatchData answer, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                GraphEmbed = graphEmbed;
                NodeID = nodeID;
                Query = query;
                Answer = answer;
            }

            public override void Forward()
            {
                Answer.BatchSize = 1;
                FastVector.Add_Vector(Query.Output.Data.MemPtr, 0,
                               GraphEmbed.EntityVec.MemPtr, NodeID * GraphEmbed.Dim, 
                               Answer.Output.Data.MemPtr, 0, GraphEmbed.Dim, 1, 1);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.Add_Vector(Query.Deriv.Data, Answer.Deriv.Data, GraphEmbed.Dim, 1, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Answer.Deriv.Data, Answer.Deriv.Data.Size);
            }

            public override void Update()
            {
                ((SparseGradientOptimizer)GraphEmbed.EntityOptimizer).PushGradientIndex(NodeID * GraphEmbed.Dim, GraphEmbed.Dim);

                FastVector.Add_Vector(GraphEmbed.EntityOptimizer.Gradient.MemPtr, NodeID * GraphEmbed.Dim,
                                      Answer.Deriv.Data.MemPtr, 0, GraphEmbed.Dim, 1, GraphEmbed.EntityOptimizer.GradientStep);
            }
        }

        public class SubQueryRunner : StructRunner
        {
            public DeepGraphEmbedingStruct GraphEmbed;
            public int Connection { get; set; }
            public int RelationID { get; protected set; }

            public HiddenBatchData Query { get; set; }
            public HiddenBatchData SubQuery { get; set; }
            public SubQueryRunner(DeepGraphEmbedingStruct graphEmbed, int connection, int relationId, HiddenBatchData query, HiddenBatchData subQuery, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                GraphEmbed = graphEmbed;
                Connection = connection;    // +1 or -1;
                RelationID = relationId;
                Query = query;
                SubQuery = subQuery;
            }

            public override void Forward()
            {
                SubQuery.BatchSize = 1;
                for (int ii = 0; ii < GraphEmbed.Dim; ii++)
                    SubQuery.Output.Data.MemPtr[ii] = Query.Output.Data.MemPtr[ii] - Connection * GraphEmbed.RelationVec.MemPtr[RelationID * GraphEmbed.Dim + ii];
            }

            public override void Backward(bool cleanDeriv)
            {
                FastVector.Add_Vector(SubQuery.Deriv.Data.MemPtr, 0, Query.Deriv.Data.MemPtr, 0, 
                            Query.Deriv.Data.MemPtr, 0, GraphEmbed.Dim, 1, 1);
            }

            public override void CleanDeriv()
            {
                FastVector.ZeroItems(SubQuery.Deriv.Data.MemPtr, GraphEmbed.Dim);
            }

            public override void Update()
            {
                ((SparseGradientOptimizer)GraphEmbed.RelationOptimizer).PushGradientIndex(RelationID * GraphEmbed.Dim, GraphEmbed.Dim);
                FastVector.Add_Vector(GraphEmbed.RelationOptimizer.Gradient.MemPtr, RelationID * GraphEmbed.Dim,
                        SubQuery.Deriv.Data.MemPtr, 0, GraphEmbed.Dim, 1, -Connection * GraphEmbed.RelationOptimizer.GradientStep);
            }
        }

        public class QueryProjectionRunner : StructRunner
        {
            public DeepGraphEmbedingStruct GraphEmbed;
            public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            public QueryProjectionRunner(DeepGraphEmbedingStruct graphEmbed, HiddenBatchData input, HiddenBatchData output, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                GraphEmbed = graphEmbed;
                Input = input;
                Output = output;
            }

            public override void Forward()
            {
                Output.BatchSize = 1;
                ComputeLib.Sgemm(Input.Output.Data, 0, GraphEmbed.QueryAttentionEmbed, 0, Output.Output.Data, 0, 1, Input.Dim, Output.Dim, 0, 1, false, false);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.Sgemm(Output.Deriv.Data, 0, GraphEmbed.QueryAttentionEmbed, 0, Input.Deriv.Data, 0, 1, Output.Dim, Input.Dim, 1, 1, false, true);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Deriv.Data.Size);
            }

            public override void Update()
            {
                ComputeLib.Sgemm(Input.Output.Data, 0, 
                                Output.Deriv.Data, 0, 
                                GraphEmbed.QueryEmbedOptimizer.Gradient, 0, 1, Input.Dim, Output.Dim, 
                                1, GraphEmbed.QueryEmbedOptimizer.GradientStep, true, false);
            }
        }

        public class RelationProjectionRunner : StructRunner
        {
            public DeepGraphEmbedingStruct GraphEmbed { get; set; }
            public int RelationID { get; set; }
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
            public RelationProjectionRunner(DeepGraphEmbedingStruct graphEmbed, int relationID, HiddenBatchData output, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Output = output;
                GraphEmbed = graphEmbed;
                RelationID = relationID;
            }

            public override void Forward()
            {
                Output.BatchSize = 1;
                ComputeLib.Sgemm(GraphEmbed.RelationVec, RelationID * GraphEmbed.Dim,
                                 GraphEmbed.RelationAttentionEmbed, 0,
                                 Output.Output.Data, 0,
                                 1, GraphEmbed.Dim, GraphEmbed.AttentionHiddenDim, 0, 1, false, false);
            }


            public override void CleanDeriv()
            {
                FastVector.ZeroItems(Output.Deriv.Data.MemPtr, Output.Deriv.Data.Size);
            }

            public override void Update()
            {
                ((SparseGradientOptimizer)GraphEmbed.RelationOptimizer).PushGradientIndex(RelationID * GraphEmbed.Dim, GraphEmbed.Dim);
                ComputeLib.Sgemm(Output.Deriv.Data, 0, 
                                 GraphEmbed.RelationAttentionEmbed, 0, 
                                 GraphEmbed.RelationOptimizer.Gradient, RelationID * GraphEmbed.Dim, 
                                 1, GraphEmbed.AttentionHiddenDim, GraphEmbed.Dim, 
                                 1, GraphEmbed.RelationOptimizer.GradientStep, false, true);

                ComputeLib.Sgemm(GraphEmbed.RelationVec, RelationID * GraphEmbed.Dim,
                                Output.Deriv.Data, 0,
                                GraphEmbed.RelationEmbedOptimizer.Gradient, 0, 
                                1, GraphEmbed.Dim, GraphEmbed.AttentionHiddenDim,
                                1, GraphEmbed.RelationEmbedOptimizer.GradientStep, true, false);
            }
        }

        public class AdditionRunner: StructRunner
        {
            HiddenBatchData Dst { get; set; }
            HiddenBatchData Src { get; set; }
            float Alpha { get; set; }
            float Beta { get; set; }
            public AdditionRunner(HiddenBatchData dst, HiddenBatchData src, float alpha, float beta, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Dst = dst;
                Src = src;
                Alpha = alpha;
                Beta = beta;
            }

            HiddenBatchData BetaWeight = null;
            int BetaIdx;

            public AdditionRunner(HiddenBatchData dst, HiddenBatchData src, float alpha, HiddenBatchData beta, int betaIdx, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Dst = dst;
                Src = src;
                Alpha = alpha;
                BetaWeight = beta;
                BetaIdx = betaIdx;
            }

            public override void Forward()
            {
                Dst.BatchSize = 1;
                if(BetaWeight != null) Beta = BetaWeight.Output.Data.MemPtr[BetaIdx];

                ComputeLib.Add_Vector(Dst.Output.Data, Src.Output.Data, Dst.Dim, Alpha, Beta);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Dst.Deriv.Data, Dst.Deriv.Data.Size);
            }

            /// <summary>
            /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                if(BetaWeight != null)
                {
                    BetaWeight.Deriv.Data.MemPtr[BetaIdx] += 
                        FastVector.DotProduct(Src.Output.Data.MemPtr, Dst.Deriv.Data.MemPtr, Src.Output.Data.Size);

                        //FastVector.Create(Dst.Deriv.Data.MemPtr).DotProduct(Src.Output.Data.MemPtr);
                }
                FastVector.AddScale(Beta, Src.Deriv.Data.MemPtr, Dst.Deriv.Data.MemPtr, Src.Deriv.Data.Size); 

                //FastVector.Create(Src.Deriv.Data.MemPtr).AddScale(Beta, Dst.Deriv.Data.MemPtr);
            }

        }

        public class DotProductRunner : StructRunner
        {
            public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }
            CudaPieceFloat Embed { get; set; }
            GradientOptimizer EmbedOptimizer { get; set; }
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
            int OutputIdx { get; set; }
            public DotProductRunner(HiddenBatchData input, CudaPieceFloat embed, GradientOptimizer embedOptimizer, HiddenBatchData output, int outputIdx, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Embed = embed;
                EmbedOptimizer = embedOptimizer;
                Output = output;
                OutputIdx = outputIdx;
            }

            public override void Forward()
            {
                Output.Output.Data.MemPtr[OutputIdx] = 
                    FastVector.DotProduct(Input.Output.Data.MemPtr, Embed.MemPtr, Input.Output.Data.Size);
                    //FastVector.Create(Input.Output.Data.MemPtr).DotProduct(0, Embed.MemPtr);
            }

            public override void CleanDeriv()
            {
                Output.Deriv.Data.MemPtr[OutputIdx] = 0;
            }

            /// <summary>
            /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                FastVector.AddScale(Output.Deriv.Data.MemPtr[OutputIdx], Embed.MemPtr, Input.Deriv.Data.MemPtr, Input.Deriv.Data.Size);
                //FastVector.Create(Input.Deriv.Data.MemPtr).AddScale(Output.Deriv.Data.MemPtr[OutputIdx], Embed.MemPtr);
            }

            public override void Update()
            {
                FastVector.AddScale(Output.Deriv.Data.MemPtr[OutputIdx]* EmbedOptimizer.GradientStep, Input.Output.Data.MemPtr, EmbedOptimizer.Gradient.MemPtr, Input.Deriv.Data.Size);
                //FastVector.Create(EmbedOptimizer.Gradient.MemPtr).AddScale(
                //    Output.Deriv.Data.MemPtr[OutputIdx] * EmbedOptimizer.GradientStep,
                //    Input.Output.Data.MemPtr);
            }
        }

        public class SoftmaxRunner : StructRunner
        {
            public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
            int[] InputSelect { get; set; }

            public SoftmaxRunner(HiddenBatchData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Output = input;
            }

            public SoftmaxRunner(HiddenBatchData input, int[] inputSelect, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Output = input;
                InputSelect = inputSelect;
            }


            public override void Forward()
            {
                if (InputSelect == null)
                {
                    Util.Softmax(Input.Output.Data.MemPtr, Output.Output.Data.MemPtr, 1);
                }
                else
                {
                    Util.Softmax(Input.Output.Data.MemPtr, InputSelect, Output.Output.Data.MemPtr, 1);
                }
            }

            /// <summary>
            /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                if (InputSelect == null)
                {
                    float tmpSum1 = FastVector.DotProduct(Input.Output.Data.MemPtr, Output.Deriv.Data.MemPtr, Input.Output.Data.Size);
                        //FastVector.Create(Input.Output.Data.MemPtr).DotProduct(0, Output.Deriv.Data.MemPtr);
                    for (int i = 0; i < Output.Deriv.Data.Size; i++)
                        Input.Deriv.Data.MemPtr[i] = Input.Output.Data.MemPtr[i] * (Output.Deriv.Data.MemPtr[i] - tmpSum1);
                }
                else
                {
                    float tmpSum1 = 0;

                    for (int i = 0; i < InputSelect.Length; i++)
                    {
                        int idx = InputSelect[i];
                        tmpSum1 += Input.Output.Data.MemPtr[idx] * Output.Deriv.Data.MemPtr[idx];
                    }
                    for (int i = 0; i < InputSelect.Length; i++)
                    {
                        int idx = InputSelect[i];
                        Input.Deriv.Data.MemPtr[idx] = Input.Output.Data.MemPtr[idx] * (Output.Deriv.Data.MemPtr[idx] - tmpSum1);
                    }
                }
            }
        }

        public class GraphNodeRunner : StructRunner
        {
            DeepGraphEmbedingStruct GraphEmbed;
            public int NodeID { get; protected set; }
            public QAData Data { get; set; }
            public GraphNodeRunner(DeepGraphEmbedingStruct graphEmbed, int nodeID, QAData qaData, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                GraphEmbed = graphEmbed;
                NodeID = nodeID;
                Data = qaData;
            }

            public override void Forward()
            {
                bool isTerminal = false;
                #region Terminal Conditions.
                if (Data.Step >= GraphEmbed.MaxHop) isTerminal = true;
                //if (Math.Pow(GraphEmbed.ExploreDiscount, Data.Step) < ParameterSetting.Random.NextDouble()) isTerminal = true;
                if (ComputeLib.L2Norm(Data.Query.Output.Data, Data.Query.Dim) < 0.01) isTerminal = true;
                #endregion.

                Data.Answer = new HiddenBatchData(1, GraphEmbed.Dim, DNNRunMode.Train, DeviceType.CPU_FAST_VECTOR);
                if (isTerminal)
                {
                    SimpleAnswerRunner simpleAnswerRunner = new SimpleAnswerRunner(GraphEmbed, NodeID, Data.Query, Data.Answer, Behavior);
                    simpleAnswerRunner.Forward();
                    Data.CG.AddRunner(simpleAnswerRunner);

                    Data.IsExpandGate = false;
                    return ;
                }
                else
                {
                    Data.GreenAnswer = new HiddenBatchData(1, GraphEmbed.Dim, DNNRunMode.Train, DeviceType.CPU);

                    SimpleAnswerRunner simpleAnswerRunner = new SimpleAnswerRunner(GraphEmbed, NodeID, Data.Query, Data.GreenAnswer, Behavior);
                    simpleAnswerRunner.Forward();
                    Data.CG.AddRunner(simpleAnswerRunner);

                    Data.IsExpandGate = true;
                }

                Data.QueryHidden = new HiddenBatchData(1, GraphEmbed.AttentionHiddenDim, DNNRunMode.Train, DeviceType.CPU_FAST_VECTOR);

                QueryProjectionRunner queryHiddenRunner = new QueryProjectionRunner(GraphEmbed, Data.Query, Data.QueryHidden, Behavior);
                queryHiddenRunner.Forward();
                Data.CG.AddRunner(queryHiddenRunner);

                List<Tuple<HiddenBatchData, RelationProjectionRunner, AdditionRunner, ActivationRunner, int >>
                    RelationExpendInfo = new List<Tuple<HiddenBatchData, RelationProjectionRunner, AdditionRunner, ActivationRunner, int>>();

                int relationIdx = 0;
                /// random select topK Links.
                foreach (Tuple<int, int, int> connection in GraphEmbed.GraphData.TrainNeighborLink[NodeID])
                {
                    relationIdx += 1;

                    if ((connection.Item1 > 0 && GraphEmbed.IsBlackConnection(NodeID, connection.Item2, connection.Item3)) ||
                        (connection.Item1 < 0 && GraphEmbed.IsBlackConnection(connection.Item3, connection.Item2, NodeID)))
                        continue;

                    HiddenBatchData relationHidden = new HiddenBatchData(1, GraphEmbed.AttentionHiddenDim, DNNRunMode.Train, DeviceType.CPU_FAST_VECTOR);
                    RelationProjectionRunner relationHiddenRunner = new RelationProjectionRunner(GraphEmbed, connection.Item2, relationHidden, Behavior);
                    AdditionRunner additionRunner = new AdditionRunner(relationHidden, Data.QueryHidden, connection.Item1, 1, Behavior);
                    ActivationRunner activationRunner = new ActivationRunner(relationHidden, A_Func.Tanh, Behavior);

                    RelationExpendInfo.Add(new Tuple<HiddenBatchData, RelationProjectionRunner, AdditionRunner, ActivationRunner, int>
                        (relationHidden, relationHiddenRunner, additionRunner, activationRunner, relationIdx - 1));
                }

                List<Tuple<HiddenBatchData, RelationProjectionRunner, AdditionRunner, ActivationRunner, int>> 
                    neighborHiddenInfo = new List<Tuple<HiddenBatchData, RelationProjectionRunner, AdditionRunner, ActivationRunner, int>>();

                if (ParameterSetting.Random.NextDouble() >= GraphEmbed.Eps)
                {
                    //HiddenBatchData candidate = new HiddenBatchData(1, RelationExpendInfo.Count, DNNRunMode.Train, DeviceType.CPU_FAST_VECTOR);
                    Data.AttentionWeight = new HiddenBatchData(1, RelationExpendInfo.Count + 1, DNNRunMode.Train, DeviceType.CPU_FAST_VECTOR);

                    List<DotProductRunner> relationAttentionRunners = new List<DotProductRunner>();
                    MinMaxHeap<int> minmaxHeap = new MinMaxHeap<int>(GraphEmbed.MaxNeighbor, 1);

                    //select the best candidate.
                    for (int mn = 0; mn < RelationExpendInfo.Count; mn++)
                    {
                        RelationExpendInfo[mn].Item2.Forward();
                        RelationExpendInfo[mn].Item3.Forward();
                        RelationExpendInfo[mn].Item4.Forward();

                        DotProductRunner relationAttentionRunner = new DotProductRunner(RelationExpendInfo[mn].Item1,
                                GraphEmbed.OutputAttentionVec, GraphEmbed.OutputEmbedOptimizer, Data.AttentionWeight, mn + 1, Behavior);
                        relationAttentionRunner.Forward();

                        relationAttentionRunners.Add(relationAttentionRunner);
                        minmaxHeap.push_pair(mn, Data.AttentionWeight.Output.Data.MemPtr[mn + 1]);
                    }

                    Data.NeiSelectAttention = new int[minmaxHeap.topK.Count + 1];
                    Data.NeiSelectAttention[0] = 0;
                    for (int mn = 0; mn < minmaxHeap.topK.Count; mn++)
                    {
                        Data.NeiSelectAttention[mn + 1] = minmaxHeap.topK[mn].Key + 1;

                        Data.CG.AddRunner(RelationExpendInfo[minmaxHeap.topK[mn].Key].Item2);
                        Data.CG.AddRunner(RelationExpendInfo[minmaxHeap.topK[mn].Key].Item3);
                        Data.CG.AddRunner(RelationExpendInfo[minmaxHeap.topK[mn].Key].Item4);
                        Data.CG.AddRunner(relationAttentionRunners[minmaxHeap.topK[mn].Key]);

                        neighborHiddenInfo.Add(new Tuple<HiddenBatchData, RelationProjectionRunner, AdditionRunner, ActivationRunner, int>(
                            RelationExpendInfo[minmaxHeap.topK[mn].Key].Item1, RelationExpendInfo[minmaxHeap.topK[mn].Key].Item2,
                            RelationExpendInfo[minmaxHeap.topK[mn].Key].Item3, RelationExpendInfo[minmaxHeap.topK[mn].Key].Item4, minmaxHeap.topK[mn].Key));
                    }
                }
                else
                {
                    //select random candidate.
                    DataRandomShuffling randomSelect = new DataRandomShuffling(RelationExpendInfo.Count);
                    for (int mn = 0; mn < GraphEmbed.MaxNeighbor; mn++)
                    {
                        int neighborId = randomSelect.RandomNext();
                        if (neighborId == -1) break;
                        RelationExpendInfo[neighborId].Item2.Forward();
                        RelationExpendInfo[neighborId].Item3.Forward();
                        RelationExpendInfo[neighborId].Item4.Forward();

                        Data.CG.AddRunner(RelationExpendInfo[neighborId].Item2);
                        Data.CG.AddRunner(RelationExpendInfo[neighborId].Item3);
                        Data.CG.AddRunner(RelationExpendInfo[neighborId].Item4);

                        neighborHiddenInfo.Add(RelationExpendInfo[neighborId]);
                    }

                    Data.AttentionWeight = new HiddenBatchData(1, neighborHiddenInfo.Count + 1, DNNRunMode.Train, DeviceType.CPU_FAST_VECTOR);

                    for (int i = 0; i < neighborHiddenInfo.Count; i++)
                    {
                        DotProductRunner relationAttentionRunner = new DotProductRunner(neighborHiddenInfo[i].Item1,
                            GraphEmbed.OutputAttentionVec, GraphEmbed.OutputEmbedOptimizer, Data.AttentionWeight, i + 1, Behavior);
                        relationAttentionRunner.Forward();
                        Data.CG.AddRunner(relationAttentionRunner);
                    }
                }

                //-->
                ActivationRunner queryActivationRunner = new ActivationRunner(Data.QueryHidden, A_Func.Tanh, Behavior);
                queryActivationRunner.Forward();
                DotProductRunner queryAttentionRunner = new DotProductRunner(Data.QueryHidden, GraphEmbed.OutputAttentionVec, GraphEmbed.OutputEmbedOptimizer, Data.AttentionWeight, 0, Behavior);
                queryAttentionRunner.Forward();
                Data.CG.AddRunner(queryActivationRunner);
                Data.CG.AddRunner(queryAttentionRunner);

                // do something here.
                SoftmaxRunner softmaxRunner = new SoftmaxRunner(Data.AttentionWeight, Data.NeiSelectAttention, Behavior);
                softmaxRunner.Forward();
                Data.CG.AddRunner(softmaxRunner);

                for(int idx = 0; idx < neighborHiddenInfo.Count;idx++)
                {
                    int linkid = neighborHiddenInfo[idx].Item5;
                    Tuple<int, int, int> connection = GraphEmbed.GraphData.TrainNeighborLink[NodeID][linkid];

                    QAData subQA = new QAData();

                    subQA.Step = Data.Step + 1;
                    subQA.Query = new HiddenBatchData(1, GraphEmbed.Dim, DNNRunMode.Train, DeviceType.CPU_FAST_VECTOR);
                    subQA.Query.BatchSize = 1;

                    SubQueryRunner subQueryRunner = new SubQueryRunner(GraphEmbed, connection.Item1, connection.Item2, Data.Query, subQA.Query, Behavior);
                    subQueryRunner.Forward();
                    Data.CG.AddRunner(subQueryRunner);

                    GraphNodeRunner subRunner = new GraphNodeRunner(GraphEmbed, connection.Item3, subQA, Behavior);
                    subRunner.Forward();
                    Data.CG.AddRunner(subRunner);
                    
                    Data.NeiQAData.Add(new Tuple<GraphNodeRunner, QAData>(subRunner, subQA));
                }

                AdditionRunner queryAnswerRunner = new AdditionRunner(Data.Answer, Data.GreenAnswer, 0, Data.AttentionWeight, 0, Behavior);
                queryAnswerRunner.Forward();
                Data.CG.AddRunner(queryAnswerRunner);

                for (int i = 0; i < Data.NeiQAData.Count; i++)
                {
                    int idx = Data.NeiSelectAttention == null ? i + 1 : Data.NeiSelectAttention[i + 1]; 
                    AdditionRunner relationAnswerRunner = new AdditionRunner(Data.Answer, Data.NeiQAData[i].Item2.Answer, 1, Data.AttentionWeight, idx, Behavior);
                    relationAnswerRunner.Forward();
                    Data.CG.AddRunner(relationAnswerRunner);
                }
                return;
            }

            public override void Backward(bool cleanDeriv)
            {
                Data.CG.Backward();
            }

            public override void CleanDeriv()
            {
                Data.CG.CleanDeriv();
            }

            public override void Update()
            {
                Data.CG.Update();
            }
        }

        public class DataPanel
        {
            public static RelationGraphData knowledgeGraph;

            public static int EntityNum { get { return knowledgeGraph.EntityId.Count; } }
            public static int RelationNum { get { return knowledgeGraph.RelationId.Count; } }

            public static List<Tuple<int, int, int>> Train
            {
                get { return knowledgeGraph.Train; }
            }

            public static List<Tuple<int, int, int>> Test
            {
                get { return knowledgeGraph.Test; }
            }

            public static List<Tuple<int, int, int>> Valid
            {
                get { return knowledgeGraph.Valid; }
            }


            public static double[] HeadProb { get { return knowledgeGraph.HeadProb; } }

            public static double[] TailProb { get { return knowledgeGraph.TailProb; } }

            public static bool IsInGraph(int eid1, int eid2, int rid)
            {
                if (knowledgeGraph.ValidGraphHash.Contains(string.Format("{0}#{1}#{2}", eid1, eid2, rid))) return true;
                else return false;
            }

            public static void Init()
            {
                knowledgeGraph = new RelationGraphData();

                knowledgeGraph.EntityId = RelationGraphData.LoadMapId(BuilderParameters.Entity2Id);
                knowledgeGraph.RelationId = RelationGraphData.LoadMapId(BuilderParameters.Relation2Id);

                knowledgeGraph.Train = knowledgeGraph.LoadGraph(BuilderParameters.TrainData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, true);
                knowledgeGraph.Valid = knowledgeGraph.LoadGraph(BuilderParameters.ValidData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);
                knowledgeGraph.Test = knowledgeGraph.LoadGraph(BuilderParameters.TestData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);
            }
        }

    }
}
