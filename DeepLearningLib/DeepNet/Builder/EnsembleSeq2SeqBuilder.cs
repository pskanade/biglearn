﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    public class EnsembleSeq2SeqBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "SRC Valid Data."));
                Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "TGT Valid Data."));

                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                ///Language Word Error Rate.
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.ACCURACY).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("LAYER-ATTENTION", new ParameterArgument("1", "Layer Attention"));
                Argument.Add("ATT-HIDDEN", new ParameterArgument("64", "Attention Layer Hidden Dim"));

                ///Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));
                Argument.Add("TGT-MAX-LEN", new ParameterArgument("30", " Tgt Seq Max Length."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return DNNRunMode.Predict; } }

            public static string ValidSrcData { get { return Argument["VALID-SRC"].Value; } }
            public static string ValidTgtData { get { return Argument["VALID-TGT"].Value; } }
            public static bool IsValidFile { get { return (!ValidSrcData.Equals(string.Empty)) && (!ValidTgtData.Equals(string.Empty)); } }

            public static string Vocab { get { return Argument["VOCAB"].Value; } }
            public static Vocab2Freq mVocabDict = null;
            public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Vocab); return mVocabDict; } }
            public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
            public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }
            /// <summary>
            /// Decode LSTM Dimension.
            /// </summary>
            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_ATTENTION { get { return Argument["LAYER-ATTENTION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static int Attention_Hidden { get { return int.Parse(Argument["ATT-HIDDEN"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }
            public static int TgtSeqMaxLen { get { return int.Parse(Argument["TGT-MAX-LEN"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

        }

        public override BuilderType Type { get { return BuilderType.ENSEMBLE_SEQ2SEQ; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }
        
        public static List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> AddLSTMEncoder(ComputationGraph cg, LSTMStructure lstmModel, SeqSparseBatchData input, bool isReverse, //SeqHelpData inputHelp,
            RunnerBehavior behavior)
        {
            List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> memory = new List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>>();
            /**************** Source LSTM Encoder *********/
            FastLSTMSparseRunner<SeqSparseBatchData> encoderRunner = new FastLSTMSparseRunner<SeqSparseBatchData>(lstmModel.LSTMCells[0], input, isReverse, behavior);
            SeqDenseRecursiveData SrclstmOutput = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);
            memory.Add(new Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>(encoderRunner.Output, encoderRunner.C));
            /***************** MultiLayer LSTM Encoder ******************************/
            for (int i = 1; i < lstmModel.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> hEncoderRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], SrclstmOutput, behavior);
                SrclstmOutput = (SeqDenseRecursiveData)cg.AddRunner(hEncoderRunner);
                memory.Add(new Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>(hEncoderRunner.Output, hEncoderRunner.C));
            }
            return memory;
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseDataSource, SequenceDataStat> srcData,
                                                             IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
            LSTMStructure[] d1Encoder, LSTMStructure[] d2Encoder, LSTMStructure[] decoder, EmbedStructure[] embed,
            List<MLPAttentionStructure>[] attentions, CompositeNNStructure[] Model, int EnsembleNum, RunnerBehavior Behavior)
            //List<ConvStructure>[] srcLayers,
            //                                                 LSTMStructure[] decoder,
            //                                                 MLPAttentionStructure[] attentions,
            //                                                 EmbedStructure[] decodeEmbedTgt,
            //                                                 DNNRunMode mode, CompositeNNStructure[] Model, int EnsembleNum, int deviceID, EvaluationType evalType)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

            /**************** Get Source and Target Data from DataCashier *********/
            SeqSparseDataSource SrcData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(srcData, Behavior));
            
            SeqSparseDataSource TgtData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(tgtData, Behavior));

            EnsembleBeamSearchRunner ensembleBeamSearchRunner = new EnsembleBeamSearchRunner(BuilderParameters.BeginWordIndex, BuilderParameters.TerminalWordIndex,
                BuilderParameters.BeamSearchCandidate, BuilderParameters.BeamSearchDepth, BuilderParameters.VocabDict.Vocab.Count, TgtData.Stat.MAX_BATCHSIZE, Behavior);
            
            for (int ensemble = 0; ensemble < EnsembleNum; ensemble++)
            {
                List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD1Memory = AddLSTMEncoder(cg, d1Encoder[ensemble], SrcData.SequenceData, false, Behavior);
                List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD2Memory = AddLSTMEncoder(cg, d2Encoder[ensemble], SrcData.SequenceData, true, Behavior);

                /**************** Bidirection Source LSTM Encoder Ensemble *********/
                List<SeqDenseBatchData> SrcEnsembleMemory = new List<SeqDenseBatchData>();
                List<Tuple<HiddenBatchData, HiddenBatchData>> SrcEnsembleStatus = new List<Tuple<HiddenBatchData, HiddenBatchData>>();
                for (int i = 0; i < SrcD1Memory.Count; i++)
                {
                    HiddenBatchData SrcD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1Memory[i].Item1, true, 0, SrcD1Memory[i].Item1.MapForward, Behavior));
                    HiddenBatchData SrcD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i].Item1, false, 0, SrcD2Memory[i].Item1.MapForward, Behavior));

                    HiddenBatchData SrcD1C = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1Memory[i].Item2, true, 0, SrcD1Memory[i].Item2.MapForward, Behavior));
                    HiddenBatchData SrcD2C = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i].Item2, false, 0, SrcD2Memory[i].Item2.MapForward, Behavior));

                    HiddenBatchData SrcO = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1O, SrcD2O }, Behavior));
                    HiddenBatchData SrcC = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1C, SrcD2C }, Behavior));
                    SrcEnsembleStatus.Add(new Tuple<HiddenBatchData, HiddenBatchData>(SrcO, SrcC));

                    SeqDenseBatchData SrcSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i].Item1, Behavior));
                    SeqDenseBatchData SrcSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i].Item1, Behavior));

                    //SeqDenseBatchData SrcSeqD1C = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i].Item2, Behavior));
                    //SeqDenseBatchData SrcSeqD2C = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i].Item2, Behavior));

                    SeqDenseBatchData SrcSeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1O, SrcSeqD2O }, Behavior));
                    //SeqDenseBatchData SrcSeqC = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1C, SrcSeqD2C }, Behavior));

                    SrcEnsembleMemory.Add(SrcSeqO);
                }

                List<MLPAttentionV2Runner> decodeAttentionRunners = new List<MLPAttentionV2Runner>();
                for (int i = 0; i < SrcEnsembleMemory.Count; i++)
                    decodeAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
                        new MLPAttentionV2Runner(attentions[ensemble][i], SrcEnsembleMemory[i],  TgtData.Stat.MAX_BATCHSIZE, BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior) : null);

                // the probability of exactly generate the true tgt.
                LSTMDecodingRunner decodeRunner = new LSTMDecodingRunner(decoder[ensemble], embed[ensemble], SrcEnsembleStatus,
                    decodeAttentionRunners.Select(i => (BasicMLPAttentionRunner)i).ToList(), Behavior, true);
                ensembleBeamSearchRunner.AddBeamSearch(decodeRunner);
            }
            cg.AddRunner(ensembleBeamSearchRunner);

            switch (BuilderParameters.Evaluation)
            {
                case EvaluationType.ACCURACY:
                    cg.AddRunner(new BeamAccuracyRunner(ensembleBeamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel));
                    break;
                case EvaluationType.BSBLEU:
                    cg.AddRunner(new BLEUDiskDumpRunner(ensembleBeamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                    break;
                case EvaluationType.ROUGE:
                    cg.AddRunner(new ROUGEDiskDumpRunner(ensembleBeamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath));
                    break;
                case EvaluationType.TOPKBEAM:
                    cg.AddRunner(new TopKBeamRunner(ensembleBeamSearchRunner.BatchResult, BuilderParameters.BeamSearchCandidate, BuilderParameters.ScoreOutputPath));
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            List<CompositeNNStructure> modelStructures = new List<CompositeNNStructure>();
            string[] models = BuilderParameters.ModelOutputPath.Split(';').ToArray();

            foreach (string model in models)
            {
                modelStructures.Add(new CompositeNNStructure(new BinaryReader(new FileStream(model, FileMode.Open, FileAccess.Read)), DeviceType.GPU));
            }

            //decodeLSTMStructure = (LSTMStructure)modelStructure.CompositeLinks[0];
            //embedDecodeTgt = (EmbedStructure)modelStructure.CompositeLinks[1];
            //attentionStructures = (AttentionStructure)modelStructure.CompositeLinks[2];
            //for (int i = 3; i < modelStructure.CompositeLinks.Count; i++) srcLayers.Add((ConvStructure)modelStructure.CompositeLinks[i]);

            LSTMStructure[] encodeLSTMStructure = new LSTMStructure[models.Length];
            LSTMStructure[] reverseEncodeLSTMStructure = new LSTMStructure[models.Length];
            LSTMStructure[] decodeLSTMStructure = new LSTMStructure[models.Length];
            EmbedStructure[] embedStructure = new EmbedStructure[models.Length];
            List<MLPAttentionStructure>[] attentionStructures = new List<MLPAttentionStructure>[models.Length];

            for (int t = 0; t < modelStructures.Count; t++)
            {
                attentionStructures[t] = new List<MLPAttentionStructure>();
                for (int i = 4; i < modelStructures[t].CompositeLinks.Count; i++)
                    attentionStructures[t].Add((MLPAttentionStructure)modelStructures[t].CompositeLinks[i]);
            }

            ComputationGraph cg = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin,
                modelStructures.Select(m => (LSTMStructure)m.CompositeLinks[0]).ToArray(),
                modelStructures.Select(m => (LSTMStructure)m.CompositeLinks[1]).ToArray(),
                modelStructures.Select(m => (LSTMStructure)m.CompositeLinks[2]).ToArray(),
                modelStructures.Select(m => (EmbedStructure)m.CompositeLinks[3]).ToArray(),
                attentionStructures,
                modelStructures.ToArray(), models.Length, new RunnerBehavior() { Computelib = computeLib, Device = device, RunMode = DNNRunMode.Train });
            double predScore = cg.Execute();
            Logger.WriteLog("Prediction Score {0}", predScore);
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;
            
            public static void Init()
            {
                if (BuilderParameters.IsValidFile)
                {
                    ValidSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidSrcData);
                    ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidTgtData);
                    
                    ValidSrcBin.InitThreadSafePipelineCashier(100, false);
                    ValidTgtBin.InitThreadSafePipelineCashier(100, false);
                }
            }
        }
    }
}