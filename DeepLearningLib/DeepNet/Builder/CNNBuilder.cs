﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class CNNBuilderParameters : BaseModelArgument<CNNBuilderParameters>
    {
        public static string TrainData { get { return Argument["TRAIN"].Value; } }
        public static bool IsTrainFile { get { return !TrainData.Equals(string.Empty); } }
        public static string ValidData { get { return Argument["VALID"].Value; } }
        public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }
        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

        public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
        public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
        public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

        public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static int[] WINDOW_SIZE { get { return Argument["WINDOW-SIZE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static N_Type[] ARCH { get { return Argument["ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
        public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
        public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
        public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }
        
        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

        public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }

        public static string ScoreOutputPath
        {
            get { if (Argument["SCORE-PATH"].Value.Equals(string.Empty)) { return LogFile + ".tmp.score"; } else return Argument["SCORE-PATH"].Value; ; }
        }

        public static string MetricOutputPath
        {
            get { if (Argument["METRIC-PATH"].Value.Equals(string.Empty)) { return LogFile + ".tmp.metric"; } else return Argument["METRIC-PATH"].Value; ; }
        }

        /// <summary>
        /// Only need to Specify Data and Archecture of Deep Nets.
        /// </summary>
        static CNNBuilderParameters()
        {
            Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
            Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

            Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
            Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.BinaryClassification).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
            Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

            Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
            Argument.Add("WINDOW-SIZE", new ParameterArgument("1", "CNN Window Size"));
            Argument.Add("ARCH", new ParameterArgument(((int)N_Type.Fully_Connected).ToString(), ParameterUtil.EnumValues(typeof(N_Type))));
            Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
            Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
            Argument.Add("LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));
            
            Argument.Add("MODEL-PATH", new ParameterArgument(@"\\DEEP-06\Test-JDSSM\Model_Path\", "Model Path"));

            Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score Path"));
            Argument.Add("METRIC-PATH", new ParameterArgument(string.Empty, "Output Metric Path"));
            Argument.Add("LABEL-BIAS", new ParameterArgument("1", "Label Start with 1 by default;"));
        }
    }

    public class CNNBuilder : Builder
    {
        public override BuilderType Type { get { return BuilderType.CNN; } }

        public override void InitStartup(string fileName)
        {
            CNNBuilderParameters.Parse(fileName);
            OptimizerParameters.Parse(fileName);
        }

        public ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseDataSource, SequenceDataStat> data, DNNRunMode mode,
            DNNStructure dnnModel, DeviceType device)
        {
            ComputationGraph cg = new ComputationGraph();

            /// Data Source Runner. 
            SeqSparseDataSource dataSource = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(data,
                        new RunnerBehavior() { RunMode = mode, Device = device }));

            HiddenBatchData output = (HiddenBatchData)cg.AddRunner( new DNNRunner<SeqSparseBatchData>(dnnModel, dataSource.SequenceData,
                       new RunnerBehavior() { RunMode = mode, Device = device }));

            switch (mode)
            {
                case DNNRunMode.Train:
                    switch(CNNBuilderParameters.LossFunction)
                    {
                        case LossFunctionType.BinaryClassification:
                            cg.AddObjective(new CrossEntropyRunner(dataSource.InstanceLabel, output.Output, output.Deriv, CNNBuilderParameters.Gamma,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                            break;
                        case LossFunctionType.MultiClassification:
                            cg.AddObjective(new MultiClassSoftmaxRunner(dataSource.InstanceLabel, output.Output, output.Deriv, CNNBuilderParameters.Gamma,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                            break;
                    }
                    break;
                case DNNRunMode.Predict:
                    switch(CNNBuilderParameters.Evaluation)
                    {
                        case EvaluationType.AUC:
                            cg.AddRunner(new AUCDiskDumpRunner(dataSource.InstanceLabel, output.Output, CNNBuilderParameters.ScoreOutputPath, CNNBuilderParameters.MetricOutputPath));
                            break;
                        case EvaluationType.ACCURACY:
                            cg.AddRunner(new AccuracyDiskDumpRunner(dataSource.InstanceLabel, output.Output, CNNBuilderParameters.Gamma , CNNBuilderParameters.ScoreOutputPath));
                            break;
                    }
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(CNNBuilderParameters.LogFile);
            MathOperatorManager.SetDevice(CNNBuilderParameters.GPUID);
            
            switch (CNNBuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    Logger.WriteLog("Loading Training/Validation Data at:{0}", DateTime.Now);
                    DataPanel.Init();
                    Logger.WriteLog("Load Data Finished at:{0}", DateTime.Now);

                    ///int[] layerDim, A_Func[] activation, bool[] isbias, N_Type[] arch, int[] wind)
                    Logger.WriteLog("Loading CNN Structure.");
                    DNNStructure dnnModel = new DNNStructure(
                                        DataPanel.Train.Stat.FEATURE_DIM,
                                        CNNBuilderParameters.LAYER_DIM,
                                        CNNBuilderParameters.ACTIVATION,
                                        CNNBuilderParameters.LAYER_BIAS,
                                        CNNBuilderParameters.ARCH,
                                        CNNBuilderParameters.WINDOW_SIZE,
                                        CNNBuilderParameters.LAYER_DROPOUT);

                    if (!Directory.Exists(CNNBuilderParameters.ModelOutputPath)) Directory.CreateDirectory(CNNBuilderParameters.ModelOutputPath);


                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train, DNNRunMode.Train, dnnModel, MathOperatorManager.MathDevice);

                    ComputationGraph validCG = CNNBuilderParameters.IsValidFile ? 
                        BuildComputationGraph(DataPanel.Valid, DNNRunMode.Predict, dnnModel, MathOperatorManager.MathDevice) : null;

                    /// GradientCheck
                    //if (OptimizerParameters.Optimizer == DNNOptimizer.GlobalGradientCheck)
                    //{
                    //    GradientCheckFlow gradCheck = new GradientCheckFlow(dnnModel, MathOperatorManager.MathDevice, trainCG);
                    //    gradCheck.Run();
                    //}

                    dnnModel.InitOptimizer(OptimizerParameters.StructureOptimizer);
                    //new StructureLearner()
                    //{
                    //    LearnRate = OptimizerParameters.LearnRate,
                    //    Optimizer = OptimizerParameters.Optimizer,
                    //    ShrinkLR = OptimizerParameters.ShrinkLR,
                    //    BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
                    //    EpochNum = OptimizerParameters.Iteration
                    //});

                    double validScore = 0;
                    double bestValidScore = double.MinValue;
                    double bestValidIter = -1;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1} at:{2}", iter, loss, DateTime.Now);
                        dnnModel.Serialize(FileUtil.CreateBinaryWrite(string.Format(@"{0}\\DNN.{1}.model", CNNBuilderParameters.ModelOutputPath, iter.ToString())));

                        if (CNNBuilderParameters.IsValidFile)
                        {
                            validScore = validCG.Execute();
                            if (validScore >= bestValidScore)
                            {
                                bestValidScore = validScore;
                                bestValidIter = iter;
                                File.Copy(string.Format(@"{0}\\DNN.{1}.model", CNNBuilderParameters.ModelOutputPath, bestValidIter.ToString()),
                                          string.Format(@"{0}\\DNN.{1}.model", CNNBuilderParameters.ModelOutputPath, "done"), true);
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                        }
                    }
                    if (CNNBuilderParameters.IsValidFile)
                        Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                    break;
                case DNNRunMode.Predict:
                    Logger.WriteLog("Loading Validation Data at:{0}", DateTime.Now);
                    DataPanel.Init();
                    Logger.WriteLog("Load Data Finished at:{0}", DateTime.Now);
                    Logger.WriteLog("Loading CNN Structure.");
                    DNNStructure pDnnModel = new DNNStructure(CNNBuilderParameters.ModelOutputPath, DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR,
                        MathOperatorManager.MathDevice);
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.Valid, DNNRunMode.Predict, pDnnModel, MathOperatorManager.MathDevice);
                    predCG.Execute();
                    break;
            }
            Logger.CloseLog();
            Console.WriteLine("All Done at:{0}", DateTime.Now);
        }


        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Train = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Valid = null;

            public static void Init()
            {
                if (CNNBuilderParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DataCashier<SeqSparseDataSource, SequenceDataStat>(CNNBuilderParameters.TrainData);
                    Train.InitThreadSafePipelineCashier(100, true);
                }
                if (CNNBuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<SeqSparseDataSource, SequenceDataStat>(CNNBuilderParameters.ValidData);
                    Valid.InitThreadSafePipelineCashier(100, false);
                }
            }
        }
    }
}
