﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;
namespace BigLearn.DeepNet
{
    public class EnsembleEmbedSeq2SeqBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "SRC Valid Data."));
                Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "TGT Valid Data."));

                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                ///Language Word Error Rate.
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.ACCURACY).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("LAYER-ATTENTION", new ParameterArgument("1", "Layer Attention"));
                Argument.Add("ATT-HIDDEN", new ParameterArgument("64", "Attention Layer Hidden Dim"));

                ///Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));
                Argument.Add("TGT-MAX-LEN", new ParameterArgument("30", " Tgt Seq Max Length."));


                Argument.Add("SRC-VOCAB-DIM", new ParameterArgument(string.Empty, "Source Vocab Dimension."));
                Argument.Add("TGT-VOCAB-DIM", new ParameterArgument(string.Empty, "Target Vocab Dimension."));

                Argument.Add("DEV-MINI-BATCH", new ParameterArgument("32", "Mini Batch for Dev."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return DNNRunMode.Predict; } }

            public static string ValidSrcData { get { return Argument["VALID-SRC"].Value; } }
            public static string ValidTgtData { get { return Argument["VALID-TGT"].Value; } }
            public static bool IsValidFile { get { return (!ValidSrcData.Equals(string.Empty)) && (!ValidTgtData.Equals(string.Empty)); } }


            public static int Src_Vocab_Dim { get { return int.Parse(Argument["SRC-VOCAB-DIM"].Value); } }
            public static int Tgt_Vocab_Dim { get { return int.Parse(Argument["TGT-VOCAB-DIM"].Value); } }

            public static int Dev_Mini_Batch { get { return int.Parse(Argument["DEV-MINI-BATCH"].Value); } }

            //public static string Vocab { get { return Argument["VOCAB"].Value; } }
            //public static Vocab2Freq mVocabDict = null;
            //public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Vocab); return mVocabDict; } }
            public static int BeginWordIndex = 0;
            public static int TerminalWordIndex = 0;

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }
            /// <summary>
            /// Decode LSTM Dimension.
            /// </summary>
            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_ATTENTION { get { return Argument["LAYER-ATTENTION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static int Attention_Hidden { get { return int.Parse(Argument["ATT-HIDDEN"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }
            public static int TgtSeqMaxLen { get { return int.Parse(Argument["TGT-MAX-LEN"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

        }

        public override BuilderType Type { get { return BuilderType.ENSEMBLE_EMBED_SEQ2SEQ; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> AddLSTMEncoder(ComputationGraph cg, LSTMStructure lstmModel,
            SeqDenseRecursiveData input, RunnerBehavior behavior)
        {
            List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> memory = new List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>>();
            SeqDenseRecursiveData SrclstmOutput = input;
            for (int i = 0; i < lstmModel.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> encoderRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], SrclstmOutput, behavior);
                SrclstmOutput = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);
                memory.Add(new Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>(encoderRunner.Output, encoderRunner.C));
            }
            return memory;
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> srcData,
                                                             IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
                                                             EmbedStructure[] embedSrc, EmbedStructure[] embedTgt,
                                                             LayerStructure[] transO, LayerStructure[] transC,
                                                             LSTMStructure[] encoder, LSTMStructure[] reverseEncoder, LSTMStructure[] decoder,
                                                             List<MLPAttentionStructure>[] attentions, EmbedStructure[] decodeEmbedTgt,
                                                             RunnerBehavior Behavior, int EnsembleNum)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

            /**************** Get Source and Target Data from DataCashier *********/
            SeqSparseBatchData SrcData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(srcData, Behavior));
            SeqSparseDataSource TgtData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(tgtData, Behavior));
            
            EnsembleBeamSearchRunner ensembleBeamSearchRunner = new EnsembleBeamSearchRunner(BuilderParameters.BeginWordIndex, BuilderParameters.TerminalWordIndex,
                 BuilderParameters.BeamSearchCandidate, BuilderParameters.BeamSearchDepth, decodeEmbedTgt.Last().VocabSize, TgtData.Stat.MAX_BATCHSIZE, Behavior);

            for (int ensemble = 0; ensemble < EnsembleNum; ensemble++)
            {
                SeqDenseRecursiveData SrcD1Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc[ensemble], SrcData, false, Behavior));
                SeqDenseRecursiveData SrcD2Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc[ensemble], SrcData, true, Behavior));

                List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD1Memory = AddLSTMEncoder(cg, encoder[ensemble], SrcD1Embed, Behavior);
                List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD2Memory = AddLSTMEncoder(cg, reverseEncoder[ensemble], SrcD2Embed, Behavior);

                /**************** Bidirection Source LSTM Encoder Ensemble *********/
                List<SeqDenseBatchData> SrcEnsembleMemory = new List<SeqDenseBatchData>();
                List<Tuple<HiddenBatchData, HiddenBatchData>> SrcEnsembleStatus = new List<Tuple<HiddenBatchData, HiddenBatchData>>();
                for (int i = 0; i < SrcD1Memory.Count; i++)
                {
                    HiddenBatchData SrcD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1Memory[i].Item1, true, 0, SrcD1Memory[i].Item1.MapForward, Behavior));
                    HiddenBatchData SrcD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i].Item1, false, 0, SrcD2Memory[i].Item1.MapForward, Behavior));

                    HiddenBatchData SrcD1C = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1Memory[i].Item2, true, 0, SrcD1Memory[i].Item2.MapForward, Behavior));
                    HiddenBatchData SrcD2C = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i].Item2, false, 0, SrcD2Memory[i].Item2.MapForward, Behavior));

                    HiddenBatchData SrcO = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1O, SrcD2O }, Behavior));
                    HiddenBatchData SrcC = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1C, SrcD2C }, Behavior));

                    HiddenBatchData newSrcO = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(transO[ensemble], SrcO, Behavior));
                    HiddenBatchData newSrcC = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(transC[ensemble], SrcC, Behavior));
                    SrcEnsembleStatus.Add(new Tuple<HiddenBatchData, HiddenBatchData>(newSrcO, newSrcC));
                    //SrcEnsembleStatus.Add(new Tuple<BigLearn.HiddenBatchData, BigLearn.HiddenBatchData>(SrcO, SrcC));

                    SeqDenseBatchData SrcSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i].Item1, Behavior));
                    SeqDenseBatchData SrcSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i].Item1, Behavior));

                    //SeqDenseBatchData SrcSeqD1C = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i].Item2, Behavior));
                    //SeqDenseBatchData SrcSeqD2C = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i].Item2, Behavior));

                    SeqDenseBatchData SrcSeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1O, SrcSeqD2O }, Behavior));
                    //SeqDenseBatchData SrcSeqC = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1C, SrcSeqD2C }, Behavior));

                    SrcEnsembleMemory.Add(SrcSeqO);
                }

                List<MLPAttentionV2Runner> decodeAttentionRunners = new List<MLPAttentionV2Runner>();
                for (int i = 0; i < SrcEnsembleMemory.Count; i++)
                {
                    decodeAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
                       new MLPAttentionV2Runner(attentions[ensemble][i], SrcEnsembleMemory[i],  TgtData.Stat.MAX_BATCHSIZE, 
                       BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior) : null);

                    Logger.WriteLog("Attention Layer {0} {1}", i, BuilderParameters.LAYER_ATTENTION[i]);

                }

                // the probability of exactly generate the true tgt.
                LSTMEmbedDecodingRunner decodeRunner = new LSTMEmbedDecodingRunner(decoder[ensemble], embedTgt[ensemble], decodeEmbedTgt[ensemble], SrcEnsembleStatus,
                    decodeAttentionRunners.Select(i => (BasicMLPAttentionRunner)i).ToList(), Behavior, true);

                ensembleBeamSearchRunner.AddBeamSearch(decodeRunner);
            }
            cg.AddRunner(ensembleBeamSearchRunner);

            switch (BuilderParameters.Evaluation)
            {
                case EvaluationType.ACCURACY:
                    cg.AddRunner(new BeamAccuracyRunner(ensembleBeamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel));
                    break;
                case EvaluationType.BSBLEU:
                    cg.AddRunner(new BLEUDiskDumpRunner(ensembleBeamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                    break;
                case EvaluationType.ROUGE:
                    cg.AddRunner(new ROUGEDiskDumpRunner(ensembleBeamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath));
                    break;
                case EvaluationType.TOPKBEAM:
                    cg.AddRunner(new TopKBeamRunner(ensembleBeamSearchRunner.BatchResult, BuilderParameters.BeamSearchCandidate, BuilderParameters.ScoreOutputPath));
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            List<CompositeNNStructure> modelStructures = new List<CompositeNNStructure>();
            string[] models = BuilderParameters.SeedModel.Split(';').ToArray();

            foreach (string model in models)
            {
                modelStructures.Add(new CompositeNNStructure(new BinaryReader(new FileStream(model, FileMode.Open, FileAccess.Read)), DeviceType.GPU));
            }

            List<MLPAttentionStructure>[] attentionStructures = new List<MLPAttentionStructure>[models.Length];
            for (int t = 0; t < modelStructures.Count; t++)
            {
                attentionStructures[t] = new List<MLPAttentionStructure>();
                for (int i = 8; i < modelStructures[t].CompositeLinks.Count; i++)
                    attentionStructures[t].Add((MLPAttentionStructure)modelStructures[t].CompositeLinks[i]);
            }

            ComputationGraph cg = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin,
                modelStructures.Select(m => (EmbedStructure)m.CompositeLinks[5]).Reverse().ToArray(),
                modelStructures.Select(m => (EmbedStructure)m.CompositeLinks[6]).Reverse().ToArray(),

                modelStructures.Select(m => (LayerStructure)m.CompositeLinks[3]).Reverse().ToArray(),
                modelStructures.Select(m => (LayerStructure)m.CompositeLinks[4]).Reverse().ToArray(),

                modelStructures.Select(m => (LSTMStructure)m.CompositeLinks[0]).Reverse().ToArray(),
                modelStructures.Select(m => (LSTMStructure)m.CompositeLinks[1]).Reverse().ToArray(),
                modelStructures.Select(m => (LSTMStructure)m.CompositeLinks[2]).Reverse().ToArray(),
                attentionStructures.Reverse().ToArray(),
                modelStructures.Select(m => (EmbedStructure)m.CompositeLinks[7]).Reverse().ToArray(),
                 new RunnerBehavior() { Computelib = computeLib, Device = device, RunMode = DNNRunMode.Train }, models.Length);

            double predScore = cg.Execute();
            Logger.WriteLog("Prediction Score {0}", predScore);
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;

            public static void Init()
            {
                if (BuilderParameters.IsValidFile)
                {
                    List<Tuple<List<int>, List<int>>> data = CommonExtractor.LoadPairSequence(
                                new StreamReader(BuilderParameters.ValidSrcData),
                                new StreamReader(BuilderParameters.ValidTgtData));

                    IEnumerable<List<Dictionary<int, float>>> srcSeqData = data.Select(i => Util.Enumable2SeqDict<int>(i.Item1));
                    using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ValidSrcData + ".bin", FileMode.Create, FileAccess.Write)))
                    {
                        CommonExtractor.ExtractSeqSparseDataBinary(srcSeqData, BuilderParameters.Src_Vocab_Dim, BuilderParameters.Dev_Mini_Batch, writer);
                    }

                    IEnumerable<List<int>> tgtSeqData = data.Select(i => i.Item2);
                    using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ValidTgtData + ".bin", FileMode.Create, FileAccess.Write)))
                    {
                        CommonExtractor.ExtractSeqSparseDataBinary(tgtSeqData, BuilderParameters.Tgt_Vocab_Dim, BuilderParameters.Dev_Mini_Batch, writer);
                    }


                    ValidSrcBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidSrcData + ".bin");
                    ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidTgtData + ".bin");

                    ValidSrcBin.InitThreadSafePipelineCashier(100, false);
                    ValidTgtBin.InitThreadSafePipelineCashier(100, false);
                }
            }
        }
    }
}