﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{

    public class ParameterArgument
    {
        public string Value = string.Empty;
        public string Descr = string.Empty;
        public ParameterArgument(string value, string descr)
        {
            Value = value;
            Descr = descr;
        }
    }

    public class ParameterUtil
    {
        public static string EnumValues(Type enumType)
        {
            StringBuilder sb = new StringBuilder();
            var list = Enum.GetValues(enumType);
            foreach (var item in list)
            {
                sb.Append(((int)item) + " : " + item.ToString() + " ; ");
            }
            return sb.ToString();
        }

        public static Dictionary<string, string> Parse(StreamReader argReader)
        {
            Dictionary<string, string> args = new Dictionary<string, string>();
            while (!argReader.EndOfStream)
            {
                string[] item = argReader.ReadLine().Split('\t', ' ');
                if (item.Length >= 2)
                {
                    args[item[0].ToUpper()] = item[1];
                }
            }

            return args;
        }

        public static string[] ParseCmd(StreamReader argReader)
        {
            Dictionary<string, string> args = new Dictionary<string, string>();
            while (!argReader.EndOfStream)
            {
                string[] item = argReader.ReadLine().Split('\t', ' ');
                if (item.Length >= 2)
                {
                    args[item[0].ToUpper()] = item[1];
                }
            }

            string[] result = new string[args.Count * 2];
            int idx = 0;
            foreach(KeyValuePair<string, string> item in args)
            {
                result[idx * 2] = "-" + item.Key;
                result[idx * 2 + 1] = item.Value;
                idx += 1;
            }
            return result;
        }

        public static string[] ParseCmd(string filename)
        {
            StreamReader sr = new StreamReader(filename);
            string[] result = ParseCmd(sr);
            sr.Close();
            return result;
        }
    }

    public class BaseModelArgument<T> where T : BaseModelArgument<T>
    {
        protected static Dictionary<string, ParameterArgument> Argument = new Dictionary<string, ParameterArgument>();

        public static string ConfigPath = string.Empty;
        public static string[] Args = null; 
        static BaseModelArgument()
        {
            System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(T).TypeHandle);
        }

        public static string LogFile
        {
            get
            {
                string dateTime = DateTime.Now.ToString("yy-MM-dd.HH.mm.ss");
                string mlog = "BigLearn.DeepNet_" + dateTime ;
                if (Args == null)
                {
                    if (! string.IsNullOrEmpty(ConfigPath))
                    {
                        Console.WriteLine("Log File {0}", ConfigPath + ".log");
                        mlog = ConfigPath;
                    }
                }
                else
                {
                    
                    if (string.IsNullOrEmpty(ConfigPath))
                    {
                        return string.Join("_", Args.Select(i => i.Replace("/", "@")));
                    }
                    else
                    {
                        Console.WriteLine("Log File {0}", ConfigPath + ".log");
                        mlog = ConfigPath+"."+ string.Join("_", Args.Select(i => i.Replace("/", "@"))) ;
                    }
                }
                return mlog.Substring(0, Math.Min(mlog.Length, 128)) + ".log";
            }
        }

        public static void PrintHelp()
        {
            Console.WriteLine("Model Argument Help :");
            foreach (KeyValuePair<string, ParameterArgument> arg in Argument)
            {
                Console.WriteLine("{0} [{1}] \t Default: {2}", arg.Key, arg.Value.Descr, arg.Value.Value);
            }
        }

        public static void Parse(string fileName)
        {
            PrintHelp();
            ConfigPath = fileName;
            StreamReader argReader = new StreamReader(fileName);
            Dictionary<string, string> args = ParameterUtil.Parse(argReader);
            foreach (var arg in args)
            {
                if (Argument.ContainsKey(arg.Key))
                {
                    Argument[arg.Key].Value = arg.Value;
                }
            }

            argReader.Close();
        }

        public static void Parse(string[] args)
        {
            PrintHelp();
            Args = args;
            int argCursor = 0;
            while (argCursor < args.Length - 1)
            {
                if (Argument.ContainsKey(args[argCursor]))
                {
                    Argument[args[argCursor]].Value = args[++argCursor];
                }
                argCursor++;
            }
        }

        public static void LoadFromKeyValuePairs(IDictionary<string, string> parameterPairs)
        {
            foreach (var param in parameterPairs)
            {
                if (!Argument.ContainsKey(param.Key))
                {
                    Argument[param.Key] = new ParameterArgument(param.Value, string.Empty);
                }
                else
                {
                    Argument[param.Key].Value = param.Value;
                }
            }
        }

        public static bool ContainsParam(string key)
        {
            return Argument.ContainsKey(key);
        }

        public static Dictionary<string, string> ToKeyValues()
        {
            return Argument.Where(kv => kv.Value != null && !string.IsNullOrEmpty(kv.Value.Value)).ToDictionary(kv => kv.Key, kv => kv.Value.Value);
        }
    }
}
