﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Collections.Concurrent;
//using ScopeML;
using BigLearn;namespace BigLearn.DeepNet
{
    public enum BuilderType
    {
        JDSSM = 0,
        STACKDNN = 1,
        DNN = 2,
        CNN = 3,
        DSSM = 4,
        MSCNN = 5,
        IMAGECNN = 6,
        RNNSEQPRED = 7,
        BPLDADNN = 8,
        RNNSEQSLOT = 9,
        RNNSEQLABEL = 10,
        LSTMSEQLABEL = 11,
        LMLSTM = 12,
        COMPISITENN = 13,
        FASTRNNSEQPRED = 14,
        SEQ2SEQLSTM = 15,
        MULTICORE_RNNSEQPRED = 16,   //Test
        LSTMSEQPOOL = 17,
        LSTMNTM = 18,
        DSSM_GRAPH = 19,
        DQN = 20,
        MULTI_DQN = 21,
        GRUSEQLABEL = 22,
        MULTI_GPU_DSSM = 23,
        MULTI_GPU_LSTM = 24,
        LSTM_DSSM = 25,
        KNOWLEDGE_GRAPH_EMBED = 26,
        GRUSEQPOOL = 27,
        MULTITASK_LSTMSEQPOOL = 28,
        MEM_NN = 29,
        ATARI_RL = 30,
        MULTITASK_SEQPOOL = 32,
        SEQ2SEQ_QA = 33,
        SEQ2SEQ_KNOWLEDGE_QA = 34,
        RECURRENT_ATTENTION_GRAPH_EMBED = 35,
        SEQ2SEQ_EMBED_QA = 36,
        CNN2SEQ = 37,
        SEQ2SEQ_EMBED_GRNN = 38,
        DUAL_LSTM_DSSM = 39,
        ENSEMBLE_CNN2SEQ = 40,
        RECURRENT_ATTENTION_GRAPH_EMBED_V2 = 41,
        CNN_DUAL_LSTM_DSSM = 42,
        DSSM_RANK_GRAPH = 43,
        RECURRENT_ATTENTION_GRAPH_EMBED_V3 = 44,
        RECURRENT_ATTENTION_GRAPH_EMBED_V4 = 45,
        RECURRENT_ATTENTION_GRAPH_EMBED_V5 = 46,
        RECURRENT_ATTENTION_GRAPH_EMBED_V6 = 47,
        CONTEXT_QA = 48,
        RECURRENT_ATTENTION_GRAPH_EMBED_V7 = 49,
        SEQ2SEQ_EMBED_LSTM = 50,
        CONTEXT_CNN_QA = 51,
        LSTMSEQPOOL_V2 = 52,
        APP_DSSM_PREDICT = 53,
        APP_CNN_LSTM_CONTEXT_QA = 54,
        APP_DSSM_RANK = 55,
        APP_L3G_CNN_LSTM_QA = 56,
        APP_ITERATIVE_READER_QA = 57,
        APP_RECURRENT_ATTENTION_QA = 58,
        ENSEMBLE_SEQ2SEQ = 59,
        ENSEMBLE_EMBED_SEQ2SEQ = 60,
        APP_REASON_NET_QA = 61,
        APP_REASON_NET_RL_QA = 62,
        ENSEMBLE_GRUEMBED_SEQ2SEQ = 63,
        APP_REASONET_RL_READER = 64,
        APP_REASONET_RL_CBT_READER = 65,
        APP_REASONET_RL_DUAL_READER = 66,
        APP_REASONET_RL_GRU_DUAL_READER = 67,
        APP_REINFORCE_CNN = 68,
        APP_GRAPH_EMBED = 69,
        APP_CDSSM = 70,
        APP_LSTMDSSM = 71,
        APP_MATH_REASONET = 72,
        APP_REASONET_RL_BING_READER = 73,
        APP_REASONET_GENERATION = 74,
        APP_REASONET_MULTIOUTPUT = 75,
        APP_REASONET_PATHGENERATION = 76,
        APP_REASONET_PATHV2GENERATION = 77,
        APP_REASONET_PATHV3GENERATION = 78,
        APP_REASONET_PATHV4GENERATION = 79,
        APP_REASONET_PATHV5GENERATION = 80,
        APP_DISTANCE_REASONET = 81,
        APP_GRAPH_EMBED_V2 = 82,
        APP_GRAPH_EMBED_V3 = 83,
        APP_REASONET_SQUAD_READER = 84,
        APP_REASONET_SQUAD_READER_V2 = 85,
        APP_REASONET_SQUAD_READER_V3 = 86,
        APP_REASONET_SQUAD_READER_V4 = 87,
        APP_REASONET_SQUAD_READER_V5 = 88,
        APP_REASONET_SQUAD_READER_V6 = 89,
        APP_REASONET_SQUAD_READER_V7 = 90,
        APP_REASONET_SQUAD_READER_V8 = 91,
        APP_REASONET_SQUAD_READER_V9 = 92,
        APP_REASONET_SQUAD_READER_V10 = 93,
        APP_REASONET_SQUAD_READER_V11 = 94,
        APP_REASONET_SQUAD_READER_V12 = 95,
        APP_REASONET_SQUAD_READER_V1 = 96,
        APP_REASONET_SQUAD_READER_V13 = 97,
        APP_REASONET_SQUAD_READER_V14 = 98,
        APP_REASONET_SQUAD_READER_V15 = 99,
        APP_BANDIT_DNN = 100,
        APP_REASONET_SQUAD_READER_V16 = 101,
        APP_REASONET_NMT = 102,
        APP_REASONET_SEQ2SEQ = 103,
        APP_REASONET_SQUAD_READER_V17 = 104,
        APP_IL_REASONET = 105,
        APP_REASONET_SQUAD_READER_V18 = 106,
        ENSEMBLE_IL_REASONET_SEQ2SEQ = 107,
        APP_REASONET_SQUAD_READER_V19 = 108,
        ENV_DDPG_AGENT = 109,
        ATARI_A3C_RL = 110,
        APP_GRAPH_EMBED_V4 = 111,
        APP_ACTORCRITIC_CNN = 112,
        APP_GRAPH_EMBED_V5 = 113,
        APP_GRAPH_EMBED_RL = 114,
        APP_IL_REASONET_GRIDWORLD = 115,
        APP_REASONET_IMAGE = 116,
        APP_SYMBOLIC_REASONET = 117,
        APP_REASONET_BUILDER = 118,
        ADVERSARIAL_DSSM = 119,
        ADVERSARIAL_CF = 120,
        APP_GRE_READER = 121,
        ATARI_RL_EMEM = 122,
        APP_REASONET_SQUAD_READER_V20 = 123,
        APP_NEURAL_EXEC = 124,
        APP_ADV_NEURAL_EXEC = 125,
        APP_ADV_V2_NEURAL_EXEC = 126,
        APP_ADV_V3_NEURAL_EXEC = 127,
        APP_MAZE_LANGUAGE = 128,
        APP_EDIT_POLICY_NETWORK = 129,
        APP_REASONET_GRAPH_READER = 130,
        APP_REASONET_GRAPH_READER_V2 = 131,
        APP_REASONET_GRAPH_READER_V3 = 132,
        APP_REASONET_GRAPH_READER_V5 = 133,
        APP_GRAPH_WALKER = 134,
        APP_AC_GRAPH_WALKER = 135,
        APP_MCTS_GRAPH_WALKER = 136,
        APP_MCTS_GRAPH_WALKER_V2 = 137,
        APP_MCTS_GRAPH_WALKER_EXECUTOR = 138,
        APP_REINFORCE_WALK = 139,
        APP_GRAPH_WALKER_REP = 140,
        APP_KBC_REINFORCE_WALK_MCTS = 141,
        APP_KBC_REINFORCE_WALK_V2 = 142,
        APP_SYMBOLIC_REINFORCE_WALK = 143,
        APP_NELL_REINFORCE_WALK_MCTS = 144,
        APP_SYMBOL_REINFORCE_WALK_MCTS = 145,
        APP_SYMBOLIC_MCTS = 146,
        APP_SEQ_MODEL = 147,
        APP_REINFORCE_WALK_SEQ_MODEL = 148,
        APP_REINFORCE_WALK_SEQ_MODEL_V2 = 149,
        APP_REINFORCE_WALK_SEQ_MODEL_V3 = 150,
        APP_REINFORCE_WALK_LINK_PREDICTION = 151,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2 = 152,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2_PRED = 153,
        APP_REINFORCE_WALK_LINK_PREDICTION_V3 = 154,
        APP_REINFORCE_WALK_LINK_PREDICTION_V4 = 155,
        APP_REINFORCE_WALK_LINK_PREDICTION_V5 = 156,
        APP_REINFORCE_WALK_LINK_PREDICTION_V6 = 157,

        APP_SYMBOL_REINFORCE_WALK_MCTS_V2 = 158,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2T = 159,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2Y = 160,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2M = 161,
        APP_SEQ_MATCH_SEQ_NLI = 162,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2_NEW = 163,
        APP_REINFORCE_WALK_LINK_PREDICTION_V3_NEW = 164,
        APP_REINFORCE_WALK_LINK_PREDICTION_V4_NEW = 165,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2_1_NEW = 166,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2_2_NEW = 167,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2_5_NEW = 168,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2_8_NEW = 169,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2_9_NEW = 170,

        APP_REINFORCE_WALK_LINK_PREDICTION_V3_9_NEW = 171,
        APP_REINFORCE_WALK_LINK_PREDICTION_V4_9_NEW = 172,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2_10_NEW = 173,
        APP_REINFORCE_WALK_LINK_PREDICTION_V4_10_NEW = 174,
        APP_REINFORCE_WALK_LINK_PREDICTION_V51 = 175,
        APP_TRANSE = 176,
        APP_TRANSE_V2 = 177,
        APP_TRANSE_V3 = 178,
        APP_MWALK = 179,
        APP_REINFORCE_WALK_LINK_PREDICTION_V2_11_NEW = 180,
        APP_PGWALK_GRU = 181,
        APP_PGWALK_GRUV2 = 182,
        APP_MWALK_PI = 183,
        APP_TRANSE_ = 184,

        APP_NL2SQL = 185,
        APP_MWALK_Q = 186,
        
        APP_QWALK_GRU = 187,

        APP_TRANSNN_ = 188,
        APP_TRANSCONV_ = 189,
        APP_PGWALK_TRANSNN = 190,
    }




    public abstract class Builder 
    {
        public abstract BuilderType Type { get; }

        public int? ParId { get; protected set; }

        Dictionary<string, string> appArgsDict;

        protected int trainIter;

        //public OutModel OutModelCache;
        //public InModel InModelCache;

        public virtual void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
            OptimizerParameters.Parse(fileName);
        }

        public virtual void InitStartup(string fileName, string[] additonalArg)
        {
            if(additonalArg == null)
            {
                InitStartup(fileName);
            }
        }

        public virtual void InitStartup(DNNAppArgs appArgs)
        {
            string config = appArgs.ConfigureFile ?? appArgs.LegacyConfigureFile;

            if (!string.IsNullOrEmpty(config))
            {
                Console.WriteLine("Initialize {0} with config : {1}", Type, config);
                InitStartup(config);
            }

            Console.WriteLine("Load additional parameters from command line");

            // Load parameters from command line to override config file
            LoadParameters(appArgs);
        }

        private void LoadParameters(DNNAppArgs appArgs)
        {
            if (!string.IsNullOrEmpty(appArgs.Servers))
            {
                StreamReader tr = FileUtil.CreateStreamRead(appArgs.Servers);
                string content = tr.ReadToEnd();
                tr.Close();
                File.WriteAllText("servers.txt", content);
            }

            var dict = ModelMetaInfo.StringToDictionary(appArgs.AppArgs, s => s, v => v);

            if (!string.IsNullOrEmpty(appArgs.CheckPoint))
            {
                dict["CHECK-POINT"] = appArgs.CheckPoint;
            }

            if (appArgs.WorkerId.HasValue)
            {
                dict["WORKER-ID"] = appArgs.WorkerId.ToString();
            }

            this.appArgsDict = dict;
            BuilderParameters.LoadFromKeyValuePairs(dict);
        }

        /// <summary>
        /// usually, there are three steps.
        /// 1. Init Data Source. 
        /// 2. Init Model.
        /// </summary>
        public abstract void Rock();

        public static Builder Init(DNNAppArgs appArgs)
        {
            string config = appArgs.LegacyConfigureFile;

            if (!string.IsNullOrEmpty(config))
            {
                return Init(config);
            }
            else if (string.IsNullOrEmpty(appArgs.AppArgs) && string.IsNullOrEmpty(appArgs.ConfigureFile))
            {
                Console.WriteLine("Trainning arguments must be specified from command line -appargs {{string}} or -f {{configure file}}");
                return null;
            }

            BuilderParameters.LoadFromKeyValuePairs(ModelMetaInfo.StringToDictionary(appArgs.AppArgs, s => s, v => v));

            foreach (Type type in Assembly.GetAssembly(typeof(Builder)).GetTypes().Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(Builder))))
            {
                Builder builder = (Builder)Activator.CreateInstance(type);
                if (builder.Type == BuilderParameters.Type)
                {
                    builder.InitStartup(appArgs);
                    Setup();
                    return builder;
                }
            }

            return null;
        }

        public static Builder Init(string fileName)
        {
            BuilderParameters.Parse(fileName);
            ParameterSetting.RANDOM_SEED = BuilderParameters.RandomSeed;

            OptimizerParameters.Parse(fileName);
            foreach (Type type in Assembly.GetAssembly(typeof(Builder)).GetTypes().Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(Builder))))
            {
                Builder builder = (Builder)Activator.CreateInstance(type);
                if (builder.Type == BuilderParameters.Type)
                {
                    builder.InitStartup(fileName);
                    Setup();
                    return builder;
                }
            }

            return null;
        }

        public static Builder Init(string fileName, string[] additionArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additionArg);
            ParameterSetting.RANDOM_SEED = BuilderParameters.RandomSeed;

            OptimizerParameters.Parse(fileName);
            OptimizerParameters.Parse(additionArg);
            foreach (Type type in Assembly.GetAssembly(typeof(Builder)).GetTypes().Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(Builder))))
            {
                Builder builder = (Builder)Activator.CreateInstance(type);
                if (builder.Type == BuilderParameters.Type)
                {
                    builder.InitStartup(fileName, additionArg);
                    Setup();
                    return builder;
                }
            }
            return null;
        }

        private static void Setup()
        {
            // TODO: Load server list file from appArgs
            // if (BuilderParameters.DistMode == PlatformType.AllReduce)
            // {
            //     DistComputationUtils.SetupChaNa("servers.txt", false, BuilderParameters.IoThreadNum);
            // }
            // else if (BuilderParameters.DistMode == PlatformType.AllReduceRDMA)
            // {
            //     DistComputationUtils.SetupChaNa("servers.txt", true, BuilderParameters.IoThreadNum);
            // }
            // else if (BuilderParameters.DistMode == PlatformType.AllReduceNCCL)
            // {
            //     int rank = Cudalib.MPI_Initialization(BuilderParameters.GPUID);
            //     Console.WriteLine("Intialize CudaMPI with GPUID: {0}, RANK: {1}", DSSMGraphParameters.GPUID, rank);
            //     //BuilderParameters.Device = MathOperatorManager.SetDevice(DSSMGraphParameters.GPUID);
            // }
            
            if (BuilderParameters.DistMode == PlatformType.AllReduce || BuilderParameters.DistMode == PlatformType.AllReduceRDMA)
            {
                OptimizerParameters.GradientAgg = new AllReduceGradAggFactory();
            }
            else if (BuilderParameters.DistMode == PlatformType.AllReduceNCCL)
            {
                OptimizerParameters.GradientAgg = new AllReduceGradAggFactory(AllReduceGradAggFactory.AggAlgorithmType.AllReduceNCCL);
            }
        }
    }
}
