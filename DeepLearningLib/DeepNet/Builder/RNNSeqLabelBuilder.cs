﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class RNNSeqLabelBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
                
                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("LAYER-LAG", new ParameterArgument("5", "DNN Layer Dim"));
                Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));

                Argument.Add("STACK", new ParameterArgument("5", "RNN Output Stack Length"));

                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model Path"));

            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }
            
            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static int[] LAYER_LAG { get { return Argument["LAYER-LAG"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int STACK { get { return int.Parse(Argument["STACK"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            
            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModelPath { get { return (Argument["SEED-MODEL"].Value); } }
            
        }


        public override BuilderType Type { get { return BuilderType.RNNSEQLABEL; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }


        public ComputationGraph BuildComputationGraph(DataCashier<SeqSparseDataSource, SequenceDataStat> data, 
            RNNLabelStructure rnnLabelModel, RunnerBehavior behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            /// Data Source Runner. 
            SeqSparseDataSource dataSource = (SeqSparseDataSource)cg.AddRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(data, behavior));

            SeqDenseBatchData rnnOutput = (SeqDenseBatchData)cg.AddRunner(new RNNSparseRunner(rnnLabelModel.RNN.RNNCells[0], dataSource.SequenceData, behavior));

            for (int i = 1; i < rnnLabelModel.RNN.RNNCells.Count; i++)
                rnnOutput = (SeqDenseBatchData)cg.AddRunner(new RNNDenseRunner(rnnLabelModel.RNN.RNNCells[i], rnnOutput, behavior ));

            HiddenBatchData lastOutput = (HiddenBatchData)cg.AddRunner(new SeqLabelRunner<SeqDenseBatchData>(rnnLabelModel.Output, rnnLabelModel.StackLen, rnnOutput, behavior));

            switch (behavior.RunMode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new CrossEntropyRunner(dataSource.InstanceLabel, lastOutput.Output, lastOutput.Deriv, BuilderParameters.Gamma, behavior));
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new AUCDiskDumpRunner(dataSource.InstanceLabel, lastOutput.Output, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                    break;
            }
            return cg;
        }


        public override void Rock()
        {
            
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            RNNStructure rnnModel = null;
            RNNLabelStructure rnnLabelModel = null;
            if (BuilderParameters.SeedModelPath.Equals(string.Empty))
            {
                rnnModel = new RNNStructure(DataPanel.Train.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM,
                                                        BuilderParameters.LAYER_LAG, BuilderParameters.ACTIVATION, BuilderParameters.LAYER_BIAS, device);
                rnnLabelModel = new RNNLabelStructure(rnnModel, BuilderParameters.STACK, 1, device);
            }
            else
                rnnLabelModel = new RNNLabelStructure(new BinaryReader(new FileStream(BuilderParameters.SeedModelPath, FileMode.Open, FileAccess.Read)), device);

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train, rnnLabelModel, new RunnerBehavior(DNNRunMode.Train, device, computeLib));
                    trainCG.InitOptimizer(rnnLabelModel, OptimizerParameters.StructureOptimizer, new RunnerBehavior(DNNRunMode.Train, device, computeLib));

                    ComputationGraph predCG = null;
                    if (BuilderParameters.IsValidFile)
                        predCG = BuildComputationGraph(DataPanel.Valid, rnnLabelModel, new RunnerBehavior(DNNRunMode.Predict, device, computeLib));

                    // To Do : Gradient Check should be put in Computation Graph.
                    //if(RNNSeqLabelBuilderParameters.CheckGrad)
                    //{ 
                    // 
                    //    if (trainer.GradientCheck(DataPanel.Train.GetInstances(false)))
                    //        Logger.WriteLog("Gradient Check Successfully!");
                    //    else
                    //        Logger.WriteLog("Gradient Check Failed!");
                    //}

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        if (BuilderParameters.IsValidFile) predCG.Execute();
                        rnnLabelModel.Save(string.Format(@"{0}\\RNN.{1}.model", BuilderParameters.ModelOutputPath, iter.ToString()));
                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph validCG = predCG = BuildComputationGraph(DataPanel.Valid, rnnLabelModel, 
                        new RunnerBehavior(DNNRunMode.Predict, device, computeLib));
                    validCG.Execute();
                    break;
            }

        }


        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Train = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Valid = null;

            public static void Init()
            {
                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainData);
                    Train.InitThreadSafePipelineCashier(100, true);
                }

                if (BuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidData);
                    Valid.InitThreadSafePipelineCashier(100, false);
                }
            }

            internal static void DeInit()
            {
                if (null != Train) Train.Dispose(); Train = null;
                if (null != Valid) Valid.Dispose(); Valid = null;
            }
        }
    }
}
