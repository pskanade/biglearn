﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class StackDNNBuilderParameters : BaseModelArgument<StackDNNBuilderParameters>
    {
        public static string TrainData { get { return Argument["TRAIN"].Value; } }
        public static string ValidData { get { return Argument["VALID"].Value; } }
        public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }
        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
        public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
        public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
        public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
        public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
        //public static string AUCTOOL { get { return (Argument["AUCTOOL"].Value); } }
        public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }
        public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }

        public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
        public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
        public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }


        /// <summary>
        /// Only need to Specify Data and Archecture of Deep Nets.
        /// </summary>
        static StackDNNBuilderParameters()
        {
            Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
            Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));

            Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "JDSSM Learn Rate."));
            Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));
            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
            Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
            //Argument.Add("AUCTOOL", new ParameterArgument(@"EvaluationToolkit\Evaluation_AUC\AUC.exe", "AUC Tool Path."));
            Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

            Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
            Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));

            Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
            Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
            Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
            Argument.Add("LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));

        }
    }

    public class StackDNNBuilder : Builder
    {
        public override BuilderType Type { get { return BuilderType.STACKDNN; } }

        public override void InitStartup(string fileName)
        {
            StackDNNBuilderParameters.Parse(fileName);
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> feaData, RunnerBehavior Behavior, DNNStructure dnnModel)
        {
            ComputationGraph cg = new ComputationGraph();

            /**************** Get Data from DataCashier *********/
            GeneralBatchInputData fea = (GeneralBatchInputData)cg.AddDataRunner(
                new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(feaData, Behavior));

            /*************** DNN for Source Input. ********************/
            HiddenBatchData output = (HiddenBatchData)cg.AddRunner(
                new DNNRunner<GeneralBatchInputData>(dnnModel, fea, Behavior));

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:

                    cg.AddObjective(new CrossEntropyRunner(fea.InstanceLabel, output.Output, output.Deriv, 1, Behavior));

                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new AUCDiskDumpRunner(fea.InstanceLabel, output.Output, "score.tmp", "metric.tmp"));

                    break;
            }
            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(StackDNNBuilderParameters.LogFile);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            DeviceBehavior device = new DeviceBehavior(StackDNNBuilderParameters.GPUID);

            Logger.WriteLog("Loading StackDNN Structure.");
            DNNStructure dnnModel = new DNNStructure(
                                DataPanel.Train.Stat.MAX_FEATUREDIM,
                                StackDNNBuilderParameters.LAYER_DIM,
                                StackDNNBuilderParameters.ACTIVATION,
                                StackDNNBuilderParameters.LAYER_DROPOUT,
                                StackDNNBuilderParameters.LAYER_BIAS);
            dnnModel.InitOptimizer(OptimizerParameters.StructureOptimizer, device.TrainMode);

            //for (int i = 0; i < dnnModel.NeuralLinks.Count; i++)
            //{
            //    //stackDNNModel.StackOn();
            //    //DataPanel.Train, 
            //    ComputationGraph traincg = BuildComputationGraph(DataPanel.Train, device.TrainMode, dnnModel);

            //    //DataPanel.Valid, 
            //    ComputationGraph evalercg = BuildComputationGraph(DataPanel.Valid, device.PredictMode, dnnModel);

            //    //DataProcessor trainProcessor = new DataProcessor(DataPanel.Train);
            //    //DataProcessor validProcessor = new DataProcessor(DataPanel.Valid);

            //    for (int iter = 0; iter < StackDNNBuilderParameters.Iteration; iter++)
            //    {
            //        float Loss = 0;
            //        traincg.Execute();
            //        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, Loss / (DataPanel.Train.Stat.TotalBatchNumber + float.Epsilon));
            //        evalercg.Execute();
            //    }
            //    if (i == dnnModel.NeuralLinks.Count - 1)  break;

            //    DataPanel.SwitchInit(i);

            //    /// change the trainer behavior Mode;
            //    trainer.Behavior.RunMode = DNNRunMode.Predict;

            //    foreach (GeneralBatchInputData sample in DataPanel.Train.GetInstances(false))
            //    {
            //        trainer.Input = sample;
            //        trainer.Forward();
            //        HiddenBatchData tmpData = ((HiddenBatchData)trainer.LinkRunners[0].Output);
            //        tmpData.Output.Data.CopyOutFromCuda();
            //        sample.SyncToCPU();
            //        DataPanel.TrainSwitch.Concate(sample, tmpData.Output.Data, tmpData.Dim);
            //        DataPanel.TrainSwitch.Save(DataPanel.TrainSwitchWriter);
            //        DataPanel.TrainSwitch.Clear();
            //    }
            //    DataPanel.TrainSwitchStat.Save(DataPanel.TrainSwitchWriter);

            //    foreach (GeneralBatchInputData sample in DataPanel.Valid.GetInstances(false))
            //    {
            //        evaler.Forward();
            //        HiddenBatchData tmpData = ((HiddenBatchData)evaler.LinkRunners[0].Output);
            //        tmpData.Output.Data.CopyOutFromCuda();
            //        sample.SyncToCPU();
            //        DataPanel.ValidSwitch.Concate(sample, tmpData.Output.Data, tmpData.Dim);
            //        DataPanel.ValidSwitch.Save(DataPanel.ValidSwitchWriter);
            //        DataPanel.ValidSwitch.Clear();
            //    }
            //    DataPanel.ValidSwitchStat.Save(DataPanel.ValidSwitchWriter);

            //    DataPanel.Switch(i);
            //}

            Logger.CloseLog();
        }
        //public sealed class StackDNNStructure : Structure
        //{
        //    public StackDNNStructure(int rawFeatureSize, int[] layerDim, A_Func[] af, float[] dropOut, bool[] isbias, StructureLearner learner)
        //    {
        //        RawFeatureSize = rawFeatureSize;
        //        LayerDim = layerDim;
        //        AF = af;
        //        DropOut = dropOut;
        //        IsBias = isbias;
        //        Learner = learner;
        //        StackLayer = 0;
        //    }

        //    public DNNStructure Dnn = null;

        //    int RawFeatureSize { get; set; }
        //    public int[] LayerDim { get; private set; }
        //    A_Func[] AF { get; set; }
        //    float[] DropOut { get; set; }
        //    bool[] IsBias { get; set; }
        //    StructureLearner Learner { get; set; }

        //    int StackLayer = 0;

        //    public bool StackOn()
        //    {
        //        Dispose();
        //        if (StackLayer >= LayerDim.Length)
        //        {
        //            return false;
        //        }
        //        else if (StackLayer == LayerDim.Length - 1)
        //        {
        //            Dnn = new DNNStructure(
        //                StackLayer == 0 ? RawFeatureSize : RawFeatureSize + LayerDim.Take(StackLayer).Sum(),
        //                new int[] { LayerDim.Last() },
        //                new A_Func[] { AF.Last() },
        //                new bool[] { IsBias.Last() }
        //                );
        //        }
        //        else
        //        {
        //            Dnn = new DNNStructure(
        //                    StackLayer == 0 ? RawFeatureSize : RawFeatureSize + LayerDim.Take(StackLayer).Sum(),
        //                    new int[] { LayerDim[StackLayer], LayerDim.Last() },
        //                    new A_Func[] { AF[StackLayer], AF.Last() },
        //                    new float[] { DropOut[StackLayer], DropOut.Last() },
        //                    new bool[] { IsBias[StackLayer], IsBias.Last() }
        //                    );
        //        }
        //        Dnn.Init();
        //        Dnn.InitOptimizer(Learner);
        //        StackLayer += 1;
        //        return true;
        //    }

        //    ~StackDNNStructure()
        //    {
        //        this.Dispose(false);
        //    }

        //    private bool disposed = false;

        //    protected override void Dispose(bool disposing)
        //    {
        //        if (this.disposed)
        //        {
        //            return;
        //        }

        //        this.disposed = true;

        //        if (disposing)
        //        {
        //            if (Dnn != null)
        //                Dnn.Dispose();
        //        }

        //        base.Dispose(disposing);
        //    }
        //}

        public class DataPanel
        {
            //public static MatchBatchInputData Train = null;
            //public static MatchBatchInputData Valid = null;
            public static GeneralBatchInputData TrainSwitch = null;
            public static BinaryWriter TrainSwitchWriter = null;
            public static GeneralBatchInputDataStat TrainSwitchStat = null;

            //public static MatchBatchInputData ValidSwitch = null;
            public static GeneralBatchInputData ValidSwitch = null;
            public static BinaryWriter ValidSwitchWriter = null;
            public static GeneralBatchInputDataStat ValidSwitchStat = null;

            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Train = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Valid = null;
            //public static DataCashier<MatchBatchInputData, MatchInputDatStat> TrainSwitch = null;
            //public static DataCashier<MatchBatchInputData, MatchInputDatStat> ValidSwitch = null;


            public static void Init()
            {
                Train = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(StackDNNBuilderParameters.TrainData);
                Train.InitThreadSafePipelineCashier(100, 13);

                if (StackDNNBuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(StackDNNBuilderParameters.ValidData);
                    Valid.InitThreadSafePipelineCashier(100, false);
                }
            }

            public static void SwitchInit(int stackID)
            {
                TrainSwitchWriter = new BinaryWriter(new FileStream(string.Format("{0}.{1}.stack", StackDNNBuilderParameters.TrainData, stackID),
                                                        FileMode.Create, FileAccess.Write));
                TrainSwitchStat = new GeneralBatchInputDataStat();
                TrainSwitch = new GeneralBatchInputData(TrainSwitchStat, DeviceType.CPU);

                //TrainSwitch = new MatchBatchInputData(string.Format("{0}.{1}.stack", StackDNNBuilderParameters.TrainData, stackID), BatchInputMode.MakeContainer, DeviceType.CPU);

                if (StackDNNBuilderParameters.IsValidFile)
                {
                    ValidSwitchWriter = new BinaryWriter(new FileStream(string.Format("{0}.{1}.stack", StackDNNBuilderParameters.ValidData, stackID),
                                                        FileMode.Create, FileAccess.Write));
                    ValidSwitchStat = new GeneralBatchInputDataStat();
                    ValidSwitch = new GeneralBatchInputData(ValidSwitchStat, DeviceType.CPU);
                }
                //    ValidSwitch = new MatchBatchInputData(string.Format("{0}.{1}.stack", StackDNNBuilderParameters.ValidData, stackID), BatchInputMode.MakeContainer, DeviceType.CPU);
            }

            public static void Switch(int stackID)
            {
                Train = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(string.Format("{0}.{1}.stack", StackDNNBuilderParameters.TrainData, stackID));
                Train.InitThreadSafePipelineCashier(100, 13);
                //    new MatchBatchInputData(string.Format("{0}.{1}.stack", StackDNNBuilderParameters.TrainData, stackID), BatchInputMode.LoadContainer, DeviceType.GPU);
                if (StackDNNBuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(string.Format("{0}.{1}.stack", StackDNNBuilderParameters.ValidData, stackID));
                    Valid.InitThreadSafePipelineCashier(100, false);
                    //Valid = new MatchBatchInputData(string.Format("{0}.{1}.stack", StackDNNBuilderParameters.ValidData, stackID), BatchInputMode.LoadContainer, DeviceType.GPU);
                }
            }
        }
    }
}
