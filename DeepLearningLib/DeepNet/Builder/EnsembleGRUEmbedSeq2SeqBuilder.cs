﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    public class EnsembleGRUEmbedSeq2SeqBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "SRC Valid Data."));
                Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "TGT Valid Data."));

                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                ///Language Word Error Rate.
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.ACCURACY).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-ATTENTION", new ParameterArgument("1", "Layer Attention"));

                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static string ValidSrcData { get { return Argument["VALID-SRC"].Value; } }
            public static string ValidTgtData { get { return Argument["VALID-TGT"].Value; } }
            public static bool IsValidFile { get { return (!ValidSrcData.Equals(string.Empty)) && (!ValidTgtData.Equals(string.Empty)); } }

            public static string Vocab { get { return Argument["VOCAB"].Value; } }
            public static Vocab2Freq mVocabDict = null;
            public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Vocab); return mVocabDict; } }
            public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
            public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }
            /// <summary>
            /// Decode LSTM Dimension.
            /// </summary>
            public static bool[] LAYER_ATTENTION { get { return Argument["LAYER-ATTENTION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }


            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

        }

        public override BuilderType Type { get { return BuilderType.ENSEMBLE_GRUEMBED_SEQ2SEQ; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static List<SeqDenseRecursiveData> AddGRUEncoder(ComputationGraph cg, GRUStructure gruModel, SeqDenseRecursiveData input, RunnerBehavior behavior)
        {
            List<SeqDenseRecursiveData> memory = new List<SeqDenseRecursiveData>();
            SeqDenseRecursiveData SrcGruOutput = input;
            /***************** GRU Encoder ******************************/
            for (int i = 0; i < gruModel.GRUCells.Count; i++)
            {
                FastGRUDenseRunner<SeqDenseRecursiveData> encoderRunner = new FastGRUDenseRunner<SeqDenseRecursiveData>(gruModel.GRUCells[i], SrcGruOutput, behavior);
                SrcGruOutput = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);
                memory.Add(encoderRunner.Output);
            }
            return memory;
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseDataSource, SequenceDataStat> srcData,
                                                             IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
                                                             EmbedStructure[] embedSrc, EmbedStructure[] embedTgt,
                                                             LayerStructure[] transO,
                                                             GRUStructure[] encoder, GRUStructure[] reverseEncoder, GRUStructure[] decoder,
                                                             List<MLPAttentionStructure>[] attentions,
                                                             LayerStructure[] U0, LayerStructure[] V0, LayerStructure[] C0,
                                                             EmbedStructure[] decodeEmbedTgt,
                                                             RunnerBehavior Behavior, int EnsembleNum)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

            /**************** Get Source and Target Data from DataCashier *********/
            SeqSparseDataSource SrcData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(srcData, Behavior));
            SeqSparseDataSource TgtData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(tgtData, Behavior));

            EnsembleBeamSearchRunner ensembleBeamSearchRunner = new EnsembleBeamSearchRunner(BuilderParameters.BeginWordIndex, BuilderParameters.TerminalWordIndex,
                 BuilderParameters.BeamSearchCandidate, BuilderParameters.BeamSearchDepth, decodeEmbedTgt.Last().VocabSize, TgtData.Stat.MAX_BATCHSIZE, Behavior);

            for (int ensemble = 0; ensemble < EnsembleNum; ensemble++)
            {
                SeqDenseRecursiveData SrcD1Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc[ensemble], SrcData.SequenceData, false, Behavior));
                SeqDenseRecursiveData SrcD2Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc[ensemble], SrcData.SequenceData, true, Behavior));

                List<SeqDenseRecursiveData> SrcD1Memory = AddGRUEncoder(cg, encoder[ensemble], SrcD1Embed, Behavior);
                List<SeqDenseRecursiveData> SrcD2Memory = AddGRUEncoder(cg, reverseEncoder[ensemble], SrcD2Embed, Behavior);

                /**************** Bidirection Source LSTM Encoder Ensemble *********/
                List<SeqDenseBatchData> SrcEnsembleMemory = new List<SeqDenseBatchData>();
                List<HiddenBatchData> SrcEnsembleStatus = new List<HiddenBatchData>();
                for (int i = 0; i < SrcD1Memory.Count; i++)
                {
                    HiddenBatchData SrcD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i], false, 0, SrcD2Memory[i].MapForward, Behavior));
                    HiddenBatchData newSrcO = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(transO[ensemble], SrcD2O, Behavior));
                    SrcEnsembleStatus.Add(newSrcO);

                    SeqDenseBatchData SrcSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i], Behavior));
                    SeqDenseBatchData SrcSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i], Behavior));

                    SeqDenseBatchData SrcSeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1O, SrcSeqD2O }, Behavior));
                    SrcEnsembleMemory.Add(SrcSeqO);
                }
                List<MLPAttentionV2Runner> decodeAttentionRunners = new List<MLPAttentionV2Runner>();
                for (int i = 0; i < SrcEnsembleMemory.Count; i++)
                    decodeAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
                        new MLPAttentionV2Runner(attentions[ensemble][i], SrcEnsembleMemory[i], TgtData.Stat.MAX_BATCHSIZE, 
                        BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior) : null);

                // the probability of exactly generate the true tgt.
                GRNNEmbedDecodingRunner decodeRunner = new GRNNEmbedDecodingRunner(decoder[ensemble], embedTgt[ensemble], decodeEmbedTgt[ensemble], SrcEnsembleStatus,
                    decodeAttentionRunners.Select(i => (BasicMLPAttentionRunner)i).ToList(), V0[ensemble], C0[ensemble], U0[ensemble], Behavior);

                ensembleBeamSearchRunner.AddBeamSearch(decodeRunner);
            }
            cg.AddRunner(ensembleBeamSearchRunner);

            switch (BuilderParameters.Evaluation)
            {
                case EvaluationType.ACCURACY:
                    cg.AddRunner(new BeamAccuracyRunner(ensembleBeamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel));
                    break;
                case EvaluationType.BSBLEU:
                    cg.AddRunner(new BLEUDiskDumpRunner(ensembleBeamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                    break;
                case EvaluationType.ROUGE:
                    cg.AddRunner(new ROUGEDiskDumpRunner(ensembleBeamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath));
                    break;
                case EvaluationType.TOPKBEAM:
                    cg.AddRunner(new TopKBeamRunner(ensembleBeamSearchRunner.BatchResult, BuilderParameters.BeamSearchCandidate, BuilderParameters.ScoreOutputPath));
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            List<CompositeNNStructure> modelStructures = new List<CompositeNNStructure>();
            string[] models = BuilderParameters.ModelOutputPath.Split(';').ToArray();

            foreach (string model in models)
            {
                modelStructures.Add(new CompositeNNStructure(new BinaryReader(new FileStream(model, FileMode.Open, FileAccess.Read)), DeviceType.GPU));
            }

            //modelStructure.AddLayer(D1encodeStructure);
            //modelStructure.AddLayer(D2encodeStructure);
            //modelStructure.AddLayer(decodeStructure);

            //modelStructure.AddLayer(transO);
            //modelStructure.AddLayer(embedSrc);
            //modelStructure.AddLayer(embedTgt);
            //modelStructure.AddLayer(embedDecodeTgt);

            //modelStructure.AddLayer(Vo);
            //modelStructure.AddLayer(Co);
            //modelStructure.AddLayer(Uo);
            //for (int i = 0; i < attentionStructures.Count; i++)


            List<MLPAttentionStructure>[] attentionStructures = new List<MLPAttentionStructure>[models.Length];
            for (int t = 0; t < modelStructures.Count; t++)
            {
                attentionStructures[t] = new List<MLPAttentionStructure>();
                for (int i = 10; i < modelStructures[t].CompositeLinks.Count; i++)
                    attentionStructures[t].Add((MLPAttentionStructure)modelStructures[t].CompositeLinks[i]);
            }

            //public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseDataSource, SequenceDataStat> srcData,
            //                                                 IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
            //                                                 EmbedStructure[] embedSrc, EmbedStructure[] embedTgt,
            //                                                 LayerStructure[] transO,
            //                                                 GRUStructure[] encoder, GRUStructure[] reverseEncoder, GRUStructure[] decoder,
            //                                                 List<MLPAttentionStructure>[] attentions,
            //                                                 LayerStructure[] U0, LayerStructure[] V0, LayerStructure[] C0,
            //                                                 EmbedStructure[] decodeEmbedTgt,
            //                                                 RunnerBehavior Behavior, int EnsembleNum)

            ComputationGraph cg = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin,
                modelStructures.Select(m => (EmbedStructure)m.CompositeLinks[4]).Reverse().ToArray(),
                modelStructures.Select(m => (EmbedStructure)m.CompositeLinks[5]).Reverse().ToArray(),

                modelStructures.Select(m => (LayerStructure)m.CompositeLinks[3]).Reverse().ToArray(),

                modelStructures.Select(m => (GRUStructure)m.CompositeLinks[0]).Reverse().ToArray(),
                modelStructures.Select(m => (GRUStructure)m.CompositeLinks[1]).Reverse().ToArray(),
                modelStructures.Select(m => (GRUStructure)m.CompositeLinks[2]).Reverse().ToArray(),
                attentionStructures.Reverse().ToArray(),
                modelStructures.Select(m => (LayerStructure)m.CompositeLinks[9]).Reverse().ToArray(),
                modelStructures.Select(m => (LayerStructure)m.CompositeLinks[7]).Reverse().ToArray(),
                modelStructures.Select(m => (LayerStructure)m.CompositeLinks[8]).Reverse().ToArray(),
                modelStructures.Select(m => (EmbedStructure)m.CompositeLinks[6]).Reverse().ToArray(),
                 new RunnerBehavior() { Computelib = computeLib, Device = device, RunMode = DNNRunMode.Train }, models.Length);

            double predScore = cg.Execute();
            Logger.WriteLog("Prediction Score {0}", predScore);
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;

            public static void Init()
            {
                if (BuilderParameters.IsValidFile)
                {
                    ValidSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidSrcData);
                    ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidTgtData);

                    ValidSrcBin.InitThreadSafePipelineCashier(100, false);
                    ValidTgtBin.InitThreadSafePipelineCashier(100, false);
                }
            }
        }
    }
}