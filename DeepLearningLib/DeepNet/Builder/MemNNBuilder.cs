﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class MemNNBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string TrainCursorData { get { return Argument["TRAIN-CURSOR"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }

            public static string TestData { get { return Argument["TEST"].Value; } }
            public static bool IsTestFile { get { return !TestData.Equals(string.Empty); } }
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
            public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
            public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
            public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
            public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }

            public static int BatchIteration { get { return int.Parse(Argument["BATCH-ITERATION"].Value); } }

            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }
            public static int[] LAYER_MAXOUT { get { return Argument["LAYER-MAXOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static N_Type[] LAYER_ARCH { get { return Argument["LAYER-ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }


            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static DeviceType Device { get { return GPUID >= 0 ? DeviceType.GPU : DeviceType.CPU; } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static int DebugBatchNum { get { return int.Parse(Argument["DEBUG-BATCH"].Value); } }

            public static float WeightInitBias { get { return float.Parse(Argument["INIT-WEIGHT-BIAS"].Value); } }

            public static float WeightInitScale { get { return float.Parse(Argument["INIT-WEIGHT-SCALE"].Value); } }

            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            /// <summary>
            /// Only need to Specify Data and Archecture of Deep Nets.
            /// </summary>
            static BuilderParameters()
            {
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("TRAIN-CURSOR", new ParameterArgument(string.Empty, "Train CURSOR Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "TEST Data."));
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
                Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
                Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
                Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));

                Argument.Add("BATCH-ITERATION", new ParameterArgument("0", "Train Batch Iteration."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "DNN Seed Model."));


                Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.Regression).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.MSE).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
                Argument.Add("LAYER-ARCH", new ParameterArgument(string.Empty, ParameterUtil.EnumValues(typeof(N_Type))));
                Argument.Add("LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));
                Argument.Add("LAYER-MAXOUT", new ParameterArgument("1", "DNN Layer Maxout"));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("DEBUG-BATCH", new ParameterArgument("0", "Evaluation on Every Debug Batch Number"));

                Argument.Add("INIT-WEIGHT-BIAS", new ParameterArgument("0", "Init Model Weight Bias"));
                Argument.Add("INIT-WEIGHT-SCALE", new ParameterArgument("-1", "Init Model Weight Scale"));
            }
        }

        public override BuilderType Type { get { return BuilderType.MEM_NN; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> data, DNNRunMode mode, 
            CompositeNNStructure dnnModel)
        {
            ComputationGraph cg = new ComputationGraph();
            //cg.SetDataSource(data);

            GeneralBatchInputData inputData = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(data,
                            new RunnerBehavior() { RunMode = mode, Device = BuilderParameters.Device }));

            HiddenBatchData hiddenOutput = null;
            switch (dnnModel.CompositeLinks[0].Type)
            {
                case DataSourceID.MemoryStructure:
                    hiddenOutput = (HiddenBatchData)cg.AddRunner(new MemoryRunner((MemoryStructure)dnnModel.CompositeLinks[0], inputData,
                        new RunnerBehavior() { RunMode = mode, Device = BuilderParameters.Device }));
                    break;
                case DataSourceID.LayerStructure:
                    hiddenOutput = (HiddenBatchData)cg.AddRunner(new FullyConnectInputRunner<GeneralBatchInputData>((LayerStructure)dnnModel.CompositeLinks[0],
                        inputData, new RunnerBehavior() { RunMode = mode, Device = BuilderParameters.Device }));
                    break;
            }

            for (int i = 1; i < dnnModel.CompositeLinks.Count; i++)
            {
                switch (dnnModel.CompositeLinks[i].Type)
                {
                    case DataSourceID.MemoryStructure:
                        hiddenOutput = (HiddenBatchData)cg.AddRunner(new MemoryRunner((MemoryStructure)dnnModel.CompositeLinks[i], hiddenOutput,
                            new RunnerBehavior() { RunMode = mode, Device = BuilderParameters.Device }));
                        break;
                    case DataSourceID.LayerStructure:
                        hiddenOutput = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>((LayerStructure)dnnModel.CompositeLinks[i],
                            hiddenOutput, new RunnerBehavior() { RunMode = mode, Device = BuilderParameters.Device }));
                        break;
                }
            }
            switch (mode)
            {
                case DNNRunMode.Train:
                    switch (BuilderParameters.LossFunction)
                    {
                        case LossFunctionType.BinaryClassification:
                            //cg.AddObjective(new CrossEntropyRunner(inputData.MatchLabelValue, hiddenOutput.Output, hiddenOutput.Deriv, BuilderParameters.Gamma,
                            //    new RunnerBehavior() { RunMode = mode, Device = BuilderParameters.Device }));
                            break;
                    }
                    break;
                case DNNRunMode.Predict:
                    switch (BuilderParameters.Evaluation)
                    {
                        case EvaluationType.AUC:
                            //cg.AddRunner(new AUCDiskDumpRunner(inputData.MatchLabelValue, hiddenOutput.Output, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                            break;
                    }
                    break;
            }

            return cg;
        }
        

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            if (BuilderParameters.Device == DeviceType.GPU) Cudalib.CudaInit(BuilderParameters.GPUID);
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    Logger.WriteLog("Loading Memory Network Structure.");

                    CompositeNNStructure dnnModel = null;

                    if (!BuilderParameters.SEED_MODEL.Equals(string.Empty))
                    {
                        dnnModel = new CompositeNNStructure(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL,FileMode.Open, FileAccess.Read)) , BuilderParameters.Device);
                    }
                    else
                    {
                        dnnModel = new CompositeNNStructure();
                        dnnModel.AddLayer(new MemoryStructure(30, DataPanel.Train.Stat.MAX_FEATUREDIM, 30, BuilderParameters.Device));
                        dnnModel.AddLayer(new LayerStructure(30, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true));
                    }

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);
                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train, DNNRunMode.Train, dnnModel);
                    ComputationGraph validCG = BuilderParameters.IsValidFile ? BuildComputationGraph(DataPanel.Valid, DNNRunMode.Predict, dnnModel) : null;
                    ComputationGraph testCG = BuilderParameters.IsTestFile ? BuildComputationGraph(DataPanel.Test, DNNRunMode.Predict, dnnModel) : null;

                    //if (trainer.GradientCheckRandomVector(DataPanel.Train.GetInstances(false)))
                    //    Logger.WriteLog("Gradient Check Successfully!");
                    //else
                    //    Logger.WriteLog("Gradient Check Failed!");
                    dnnModel.InitOptimizer(new StructureLearner()
                    {
                        LearnRate = BuilderParameters.LearnRate,
                        Optimizer = BuilderParameters.Optimizer,
                        AdaBoost = BuilderParameters.AdaBoost,
                        ShrinkLR = BuilderParameters.ShrinkLR,
                        BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
                        EpochNum = BuilderParameters.Iteration
                    });

                    for (int iter = 0; iter < BuilderParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        if (BuilderParameters.IsValidFile) validCG.Execute();
                        if (BuilderParameters.IsTestFile) testCG.Execute();
                    }
                    /////Batch Iteration.
                    //dnnModel.InitGlobalOptimizer(new StructureLearner()
                    //{
                    //    LearnRate = DNNBuilderParameters.LearnRate,
                    //    Optimizer = DNNOptimizer.BatchLBFGS,
                    //    AdaBoost = DNNBuilderParameters.AdaBoost,
                    //    BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
                    //    EpochNum = DNNBuilderParameters.BatchIteration,
                    //    ShrinkLR = DNNBuilderParameters.ShrinkLR
                    //});
                    //DataPanel.Train.TrainBatchNum = 0;
                    //for (int i = 0; i < DNNBuilderParameters.BatchIteration; i++)
                    //{
                    //    dnnModel.ResetGlobalOptimization();
                    //    double loss = trainer.Train(DataPanel.Train.GetInstances(false));
                    //    dnnModel.EndGlobalOptimization(loss, DataPanel.Train.GetInstances(false), trainer.Loss);
                    //    Logger.WriteLog("Iteration {0} \t Train Loss {1}", i, loss);
                    //    Logger.WriteLog("Validation Set Evaluation!");
                    //    if (DNNBuilderParameters.IsValidFile) evaler.Predict(DataPanel.Valid.GetInstances(false), evalParameter);
                    //    if (DNNBuilderParameters.IsTestFile) tester.Predict(DataPanel.Test.GetInstances(false), evalParameter);
                    //}
                    //trainer.Dispose();
                    //if (DNNBuilderParameters.IsValidFile) evaler.Dispose();
                    //if (DNNBuilderParameters.IsTestFile) tester.Dispose();
                    break;
                case DNNRunMode.Predict:
                    break;
            }
            DataPanel.DeInit();
            Logger.CloseLog();
        }


        public class DataPanel
        {
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Train = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Valid = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Test = null;

            public static void Init()
            {
                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TrainData, BuilderParameters.TrainCursorData);
                    Train.InitThreadSafePipelineCashier(100, true);
                    //Train.Stat.IsInverseMatch = LossFunction.IsRankFunction(BuilderParameters.LossFunction);
                    //Train.InitPipelineCashier(100, 10);
                }

                if (BuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.ValidData);
                    Valid.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.IsTestFile)
                {
                    Test = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TestData);
                    Test.InitThreadSafePipelineCashier(100, false);
                }
            }

            internal static void DeInit()
            {
                if (null != Train)
                {
                    Train.Dispose();
                    Train = null;
                }

                if (null != Valid)
                {
                    Valid.Dispose();
                    Valid = null;
                }

                if (null != Test)
                {
                    Test.Dispose();
                    Test = null;
                }
            }
        }
    }
}
