﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    public class ContextCNNQABuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                // training data folder.
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("TRAIN-FOLDER", new ParameterArgument(string.Empty, "Training Folder"));
                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));
                Argument.Add("TEST-FOLDER", new ParameterArgument(string.Empty, "Testing Folder"));
                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));

                Argument.Add("CON-LAYER-DIM", new ParameterArgument("300,300", "DNN Layer Dim"));
                Argument.Add("CON-LAYER-WIN", new ParameterArgument("5,5", "DNN Layer WindowSize"));

                Argument.Add("QUERY-LAYER-DIM", new ParameterArgument("300,300", "DNN Layer Dim"));
                Argument.Add("QUERY-LAYER-WIN", new ParameterArgument("5,5", "DNN Layer WindowSize"));

                ///Language Word Error Rate.
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                // Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string TrainFolder { get { return Argument["TRAIN-FOLDER"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTrainFile { get { return (!TrainFolder.Equals(string.Empty)); } }
            public static string Vocab { get { return Argument["VOCAB"].Value == string.Empty ? TrainFolder + ".vocab" : Argument["VOCAB"].Value; } }
            public static string TrainIndexData { get { return TrainFolder + ".idx.tsv"; } }
            public static string TrainContextBinary { get { return TrainFolder + ".cox.bin"; } }
            public static string TrainQueryBinary { get { return TrainFolder + ".qry.bin"; } }
            public static string TrainAnswerBinary { get { return TrainFolder + ".anw.bin"; } }

            public static string TestFolder { get { return Argument["TEST-FOLDER"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTestFile { get { return (!TestFolder.Equals(string.Empty)); } }
            public static string TestIndexData { get { return TestFolder + ".idx.tsv"; } }
            public static string TestContextBinary { get { return TestFolder + ".cox.bin"; } }
            public static string TestQueryBinary { get { return TestFolder + ".qry.bin"; } }
            public static string TestAnswerBinary { get { return TestFolder + ".anw.bin"; } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static int[] CON_LAYER_DIM { get { return Argument["CON-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] QUERY_LAYER_DIM { get { return Argument["QUERY-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] CON_LAYER_WIN { get { return Argument["CON-LAYER-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] QUERY_LAYER_WIN { get { return Argument["QUERY-LAYER-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            /// <summary>
            /// DNN Run Mode.
            /// </summary>
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
        }

        public override BuilderType Type { get { return BuilderType.CONTEXT_CNN_QA; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        class GenerateSelectionMatchRunner : StructRunner
        {
            public new BiMatchBatchData Output { get { return (BiMatchBatchData)base.Output; } set { base.Output = value; } }

            GeneralBatchInputData CandidateIndexData { get; set; }

            SeqDenseBatchData FullContextMem { get; set; }

            public GenerateSelectionMatchRunner(GeneralBatchInputData candidateData, SeqDenseBatchData fullContextMem, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                CandidateIndexData = candidateData;
                FullContextMem = fullContextMem;

                Output = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = candidateData.Stat.MAX_BATCHSIZE,
                    MAX_TGT_BATCHSIZE = FullContextMem.Stat.MAX_SEQUENCESIZE, // candidateData.Stat.MAX_ELEMENTSIZE,
                    MAX_MATCH_BATCHSIZE = candidateData.Stat.MAX_ELEMENTSIZE
                }, Behavior.Device);
            }

            public override void Forward()
            {
                CandidateIndexData.BatchIdx.SyncToCPU(CandidateIndexData.BatchSize);
                CandidateIndexData.FeatureIdx.SyncToCPU(CandidateIndexData.ElementSize);
                CandidateIndexData.FeatureValue.SyncToCPU(CandidateIndexData.ElementSize);
                FullContextMem.SampleIdx.SyncToCPU(CandidateIndexData.BatchSize);

                Output.SrcSize = CandidateIndexData.BatchSize;
                Output.TgtSize = FullContextMem.SampleIdx.MemPtr[CandidateIndexData.BatchSize - 1];
                Output.MatchSize = CandidateIndexData.ElementSize;

                int matchIdx = 0;
                for (int i = 0; i < CandidateIndexData.BatchSize; i++)
                {
                    int feaBegin = i == 0 ? 0 : CandidateIndexData.BatchIdx.MemPtr[i - 1];
                    int feaEnd = CandidateIndexData.BatchIdx.MemPtr[i];

                    int fullMemBegin = i == 0 ? 0 : FullContextMem.SampleIdx.MemPtr[i - 1];
                    for (int f = feaBegin; f < feaEnd; f++)
                    {
                        Output.SrcIdx.MemPtr[matchIdx] = i;
                        Output.TgtIdx.MemPtr[matchIdx] = fullMemBegin + CandidateIndexData.FeatureIdx.MemPtr[f];
                        Output.MatchInfo.MemPtr[matchIdx] = CandidateIndexData.FeatureValue.MemPtr[f];
                        matchIdx++;
                    }
                }

                Util.InverseMatchIdx(Output.SrcIdx.MemPtr, Output.MatchSize, Output.Src2MatchIdx.MemPtr, Output.Src2MatchElement.MemPtr, Output.SrcSize);
                Util.InverseMatchIdx(Output.TgtIdx.MemPtr, Output.MatchSize, Output.Tgt2MatchIdx.MemPtr, Output.Tgt2MatchElement.MemPtr, Output.TgtSize);

                Output.SrcIdx.SyncFromCPU(Output.MatchSize);
                Output.TgtIdx.SyncFromCPU(Output.MatchSize);
                Output.MatchInfo.SyncFromCPU(Output.MatchSize);

                Output.Src2MatchIdx.SyncFromCPU(Output.SrcSize);
                Output.Src2MatchElement.SyncFromCPU(Output.MatchSize);
                Output.Tgt2MatchIdx.SyncFromCPU(Output.TgtSize);
                Output.Tgt2MatchElement.SyncFromCPU(Output.MatchSize);
            }
        }

        class GenerateSelectionPredictionRunner : ObjectiveRunner
        {
            GeneralBatchInputData CandidateInputData { get; set; }

            SeqSparseBatchData ContextInputData { get; set; }

            CudaPieceFloat CandScoreData { get; set; }

            float Gamma { get; set; }

            int SampleNum = 0;
            int HitNum = 0;
            string OutputPath = "";
            StreamWriter OutputWriter = null;
            public GenerateSelectionPredictionRunner(
                GeneralBatchInputData candInputData, SeqSparseBatchData contextInputData, CudaPieceFloat candScoreData, float gamma, RunnerBehavior behavior, 
                string outputFile = "") : base(Structure.Empty, behavior)
            {
                CandidateInputData = candInputData;
                ContextInputData = contextInputData;
                Gamma = gamma;
                CandScoreData = candScoreData;

                OutputPath = outputFile;
            }

            public override void Init()
            {
                SampleNum = 0;
                HitNum = 0;

                if (!OutputPath.Equals(""))
                {
                    OutputWriter = new StreamWriter(OutputPath);
                }
            }
            public override void Forward()
            {
                CandidateInputData.BatchIdx.SyncToCPU(CandidateInputData.BatchSize);
                CandidateInputData.FeatureIdx.SyncToCPU(CandidateInputData.ElementSize);
                CandidateInputData.FeatureValue.SyncToCPU(CandidateInputData.ElementSize);
                CandScoreData.SyncToCPU(CandidateInputData.ElementSize);

                ContextInputData.SampleIdx.SyncToCPU(ContextInputData.BatchSize);
                ContextInputData.FeaIdx.SyncToCPU(ContextInputData.ElementSize);

                for (int i = 0; i < CandidateInputData.BatchSize; i++)
                {
                    Dictionary<int, float> candScoreDict = new Dictionary<int, float>();
                    int trueCandIdx = -1;

                    int sentBgn = i == 0 ? 0 : ContextInputData.SampleIdx.MemPtr[i - 1];
                    int sentEnd = ContextInputData.SampleIdx.MemPtr[i];

                    int candBgn = i == 0 ? 0 : CandidateInputData.BatchIdx.MemPtr[i - 1];
                    int candEnd = CandidateInputData.BatchIdx.MemPtr[i];

                    for (int c = candBgn; c < candEnd; c++)
                    {
                        int candPos = CandidateInputData.FeatureIdx.MemPtr[c];
                        int candIdx = ContextInputData.FeaIdx.MemPtr[candPos + sentBgn];
                        float candScore = CandScoreData.MemPtr[c];
                        float candLabel = CandidateInputData.FeatureValue.MemPtr[c];

                        if (candLabel > 0 && trueCandIdx >= 0 && trueCandIdx != candIdx) { throw new Exception("Candidate Label Error 1 !!"); }

                        if (candLabel > 0) { trueCandIdx = candIdx; }

                        if (!candScoreDict.ContainsKey(candIdx)) { candScoreDict[candIdx] = 0; }
                        candScoreDict[candIdx] += (float)Math.Exp(Gamma * candScore);
                    }

                    if (trueCandIdx == -1) { throw new Exception("Candidate Label Error 2 !!"); }
                    var predCandIdx = candScoreDict.FirstOrDefault(x => x.Value == candScoreDict.Values.Max()).Key;

                    float expSum = candScoreDict.Values.Sum();

                    foreach (int k in new List<int>(candScoreDict.Keys)) candScoreDict[k] = candScoreDict[k] / expSum;

                    OutputWriter.WriteLine("{0}\t{1}\t{2}", trueCandIdx, TextUtil.Dict2Str(candScoreDict), predCandIdx);
                    SampleNum++;
                    if (predCandIdx == trueCandIdx) HitNum++;
                }
            }

            public override void Complete()
            {
                Logger.WriteLog("Sample {0}, Hit {1}, Accuracy {2}", SampleNum, HitNum, HitNum * 1.0 / SampleNum );
                ObjectiveScore = HitNum * 1.0 / SampleNum;
                OutputWriter.Close();
            }
        }

        // This is the main code.
        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> context,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> query,
                                                             IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> answer,
                                                             // context cnn 
                                                             List<LayerStructure> ContextCNN,
                                                             // query CNN
                                                             List<LayerStructure> QueryCNN,
                                                             LayerStructure MatchLayer,
                                                             // model.
                                                             CompositeNNStructure model, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            /************************************************************ Bi direction LSTM of context data *********/
            SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(context, Behavior));

            SeqDenseBatchData ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(ContextCNN[0], ContextData, Behavior));
            for (int i = 1; i < ContextCNN.Count; i++)
                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(ContextCNN[i], ContextEmbedOutput, Behavior));

            // standard order -> word to vec layer.
            //SeqDenseRecursiveData ContextD1Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(wordEmbed, ContextData, false, Behavior));
            // reverse order -> word to vec layer.
            //SeqDenseRecursiveData ContextD2Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(wordEmbed, ContextData, true, Behavior));
            // standard order -> lstm embedding. 
            //SeqDenseRecursiveData ContextD1LSTMData = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(contextD1LSTM.LSTMCells[0], ContextD1Embed, Behavior));
            // reverse order -> lstm embedding.
            //SeqDenseRecursiveData ContextD2LSTMData = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(contextD2LSTM.LSTMCells[0], ContextD2Embed, Behavior));

            //SeqDenseBatchData ContextD1LSTMEmbed = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(ContextD1LSTMData, Behavior));
            //SeqDenseBatchData ContextD2LSTMEmbed = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(ContextD2LSTMData, Behavior));
            //SeqDenseBatchData ContextEnsembleLSTMEmbed = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { ContextD1LSTMEmbed, ContextD2LSTMEmbed }, Behavior));

            /************************************************************ Bi direction LSTM of query data *********/
            SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));
            SeqDenseBatchData QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(QueryCNN[0], QueryData, Behavior));
            for (int i = 1; i < QueryCNN.Count; i++)
                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(QueryCNN[i], QueryEmbedOutput, Behavior));

            HiddenBatchData QueryMaxPoolingOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(QueryEmbedOutput, Behavior));

            HiddenBatchData QueryOutput = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(MatchLayer, QueryMaxPoolingOutput, Behavior));

            //SeqDenseBatchData QueryD1LSTMEmbed = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(QueryD1LSTMData, Behavior));
            //SeqDenseBatchData QueryD2LSTMEmbed = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(QueryD2LSTMData, Behavior));
            //SeqDenseBatchData QueryEnsembleLSTMEmbed = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QueryD1LSTMEmbed, QueryD2LSTMEmbed }, Behavior));
            /************************************************************ Max-Pooling for Query Ensemble Embedding *********/
            //HiddenBatchData QueryOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(QueryEnsembleLSTMEmbed, Behavior));

            /************************************************************ Candidate and Answer data *********/
            GeneralBatchInputData CandidateAnswerData = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(answer, Behavior));

            HiddenBatchData ContextOutput = new HiddenBatchData(ContextEmbedOutput.MAX_SENTSIZE, ContextEmbedOutput.Dim, ContextEmbedOutput.SentOutput, ContextEmbedOutput.SentDeriv, Behavior.Device);
            //(HiddenBatchData)cg.AddRunner(new Transform_SeqMatrix2MatrixRunner(ContextEmbedOutput, Behavior));
            // select candidate entities.
            BiMatchBatchData candidateMatchData = (BiMatchBatchData)cg.AddRunner(new GenerateSelectionMatchRunner(CandidateAnswerData, ContextEmbedOutput, Behavior));
            // similarity between source text and target text.
            HiddenBatchData simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(QueryOutput, ContextOutput, candidateMatchData, SimilarityType.CosineSimilarity, Behavior));

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:

                    if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                        cg.AddRunner(new ModelDiskDumpRunner(model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\cnnAttention"));

                    cg.AddObjective(new BayesianRatingRunner(simiOutput.Output.Data, simiOutput.Deriv.Data, candidateMatchData.MatchInfo, candidateMatchData, BuilderParameters.Gamma, Behavior));
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new GenerateSelectionPredictionRunner(CandidateAnswerData, ContextData, simiOutput.Output.Data, BuilderParameters.Gamma, Behavior, BuilderParameters.ScoreOutputPath));
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");

            CompositeNNStructure modelStructure = new CompositeNNStructure();
            List<LayerStructure> contextLayers = new List<LayerStructure>();
            List<LayerStructure> queryLayers = new List<LayerStructure>();
            LayerStructure matchLayer = null;

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                int inputDim = DataPanel.WordDim;
                for (int i = 0; i < BuilderParameters.CON_LAYER_DIM.Length; i++)
                {
                    contextLayers.Add((new LayerStructure(inputDim, BuilderParameters.CON_LAYER_DIM[i], A_Func.Tanh, 
                        N_Type.Convolution_layer, BuilderParameters.CON_LAYER_WIN[i], 0, true, device)));
                    inputDim = BuilderParameters.CON_LAYER_DIM[i];
                }

                inputDim = DataPanel.WordDim;
                for (int i = 0; i < BuilderParameters.QUERY_LAYER_DIM.Length; i++)
                {
                    queryLayers.Add((new LayerStructure(inputDim, BuilderParameters.QUERY_LAYER_DIM[i], A_Func.Tanh, 
                        N_Type.Convolution_layer, BuilderParameters.QUERY_LAYER_WIN[i], 0, true, device)));
                    inputDim = BuilderParameters.QUERY_LAYER_DIM[i];
                }

                matchLayer = new LayerStructure(BuilderParameters.QUERY_LAYER_DIM.Last(), BuilderParameters.CON_LAYER_DIM.Last(), A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true);
                for (int i = 0; i < contextLayers.Count; i++) modelStructure.AddLayer(contextLayers[i]);
                for (int i = 0; i < queryLayers.Count; i++) modelStructure.AddLayer(queryLayers[i]);
                modelStructure.AddLayer(matchLayer);
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, DeviceType.GPU);
                    for (int i = 0; i < BuilderParameters.CON_LAYER_DIM.Length; i++) contextLayers.Add((LayerStructure)modelStructure.CompositeLinks[i]);
                    for (int i = 0; i < BuilderParameters.QUERY_LAYER_DIM.Length; i++) queryLayers.Add((LayerStructure)modelStructure.CompositeLinks[i + BuilderParameters.CON_LAYER_DIM.Length]);
                    matchLayer = (LayerStructure)modelStructure.CompositeLinks.Last();
                }
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainAnswer,
                        contextLayers, queryLayers, matchLayer, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });


                    ComputationGraph validCG = null;

                    if(BuilderParameters.IsTestFile)
                        validCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                            contextLayers, queryLayers, matchLayer, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                    double bestValidScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            string model_prefix = Path.Combine(BuilderParameters.ModelOutputPath, string.Format("cnnAtt.iter.{0}", iter));
                            modelStructure.Serialize(new BinaryWriter(new FileStream(model_prefix, FileMode.Create, FileAccess.Write)));
                        }

                        if(validCG != null)
                        {
                            double validScore = validCG.Execute();
                            if(validScore > bestValidScore) { bestValidScore = validScore; bestIter = iter; }
                            Logger.WriteLog("Best Valid Score {0}, At iter {1}", bestValidScore, bestIter);
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, "cnnAtt.best.model"), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }
                        
                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                        contextLayers, queryLayers, matchLayer, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
                    predCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQuery = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TrainAnswer = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestQuery = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TestAnswer = null;

            static ItemFreqIndexDictionary wordFreqDict = null;
            static ItemFreqIndexDictionary entityFreqDict = null;

            public static int WordDim { get { return 1 + entityFreqDict.ItemDictSize + wordFreqDict.ItemDictSize; } }

            const int WordVocabLimited = 50000;

            static void ExtractVocab(string questFolder, string vocab)
            {
                wordFreqDict = new ItemFreqIndexDictionary("WordVocab");
                entityFreqDict = new ItemFreqIndexDictionary("EntityVocab");

                int fileIdx = 0;
                DirectoryInfo directory = new DirectoryInfo(questFolder);
                foreach (FileInfo sampleFile in directory.EnumerateFiles())
                {
                    using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                    {
                        string url = mreader.ReadLine(); mreader.ReadLine();
                        string context = mreader.ReadLine(); mreader.ReadLine();
                        string query = mreader.ReadLine(); mreader.ReadLine();
                        string answer = mreader.ReadLine(); mreader.ReadLine();

                        foreach (string word in context.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }

                        foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else if (word.StartsWith("@placeholder")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }
                    }
                    if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract Vocab from Corpus {0}", fileIdx); }
                }

                /// keep top 50K vocabary.
                wordFreqDict.Filter(0, WordVocabLimited);
                using (StreamWriter mwriter = new StreamWriter(vocab))
                {
                    entityFreqDict.Save(mwriter);
                    wordFreqDict.Save(mwriter);
                }
            }

            /// <summary>
            /// Unknown Index = 0;
            /// </summary>
            static void ExtractCorpusIndex(string questFolder, string questIndexFile)
            {
                using (StreamWriter indexWriter = new StreamWriter(questIndexFile))
                {
                    int fileIdx = 0;
                    DirectoryInfo directory = new DirectoryInfo(questFolder);
                    foreach (FileInfo sampleFile in directory.EnumerateFiles())
                    {
                        using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                        {
                            string url = mreader.ReadLine(); mreader.ReadLine();
                            string context = mreader.ReadLine(); mreader.ReadLine();
                            string query = mreader.ReadLine(); mreader.ReadLine();
                            string answer = mreader.ReadLine(); mreader.ReadLine();

                            int answerIndex = entityFreqDict.IndexItem(answer);

                            Dictionary<int, float> answerLabel = new Dictionary<int, float>();

                            List<int> contextIndex = new List<int>();
                            int pos = 0;
                            foreach (string word in context.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                if (word.StartsWith("@entity"))
                                {
                                    int idx = entityFreqDict.IndexItem(word); contextIndex.Add(idx + 1);
                                    if (answerIndex == idx) { answerLabel.Add(pos, 1); }
                                    else { answerLabel.Add(pos, 0); }
                                }
                                else { int idx = wordFreqDict.IndexItem(word); contextIndex.Add(idx >= 0 ? idx + entityFreqDict.ItemDictSize + 1 : 0); }
                                pos++;
                            }

                            List<int> queryIndex = new List<int>();
                            foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                if (word.StartsWith("@entity")) { int idx = entityFreqDict.IndexItem(word); queryIndex.Add(idx + 1); }
                                else if (word.StartsWith("@placeholder")) { int idx = entityFreqDict.IndexItem(word); queryIndex.Add(idx + 1); }
                                else { int idx = wordFreqDict.IndexItem(word); queryIndex.Add(idx >= 0 ? idx + entityFreqDict.ItemDictSize + 1 : 0); }
                            }

                            indexWriter.WriteLine("{0}\t{1}\t{2}", string.Join(" ", contextIndex), string.Join(" ", queryIndex), TextUtil.Dict2Str(answerLabel));
                        }
                        if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract Index from Corpus {0}", fileIdx); }
                    }
                }
            }

            static void ExtractCorpusBinary(string questIndexFile, string contextBin, string queryBin, string answerBin, int miniBatchSize)
            {
                SeqSparseBatchData context = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData query = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                GeneralBatchInputData answer = new GeneralBatchInputData(new GeneralBatchInputDataStat() { FeatureType = FeatureDataType.SparseFeature, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);

                BinaryWriter contextWriter = FileUtil.CreateBinaryWrite(contextBin);
                BinaryWriter queryWriter = FileUtil.CreateBinaryWrite(queryBin);
                BinaryWriter answerWriter = FileUtil.CreateBinaryWrite(answerBin);

                using (StreamReader indexReader = new StreamReader(questIndexFile))
                {
                    int lineIdx = 0;
                    while (!indexReader.EndOfStream)
                    {
                        string[] items = indexReader.ReadLine().Split('\t');

                        List<Dictionary<int, float>> contextFea = new List<Dictionary<int, float>>();
                        foreach (string term in items[0].Split(' ')) { var tmp = new Dictionary<int, float>(); tmp[int.Parse(term)] = 1; contextFea.Add(tmp); }
                        context.PushSample(contextFea);

                        List<Dictionary<int, float>> queryFea = new List<Dictionary<int, float>>();
                        foreach (string term in items[1].Split(' ')) { var tmp = new Dictionary<int, float>(); tmp[int.Parse(term)] = 1; queryFea.Add(tmp); }
                        query.PushSample(queryFea);

                        Dictionary<int, float> answerFea = TextUtil.Str2Dict(items[2]);
                        answer.PushSample(answerFea);

                        if (context.BatchSize >= miniBatchSize)
                        {
                            context.PopBatchToStat(contextWriter);
                            query.PopBatchToStat(queryWriter);
                            answer.PopBatchToStat(answerWriter);
                        }

                        if (++lineIdx % 1000 == 0) { Console.WriteLine("Extract Binary from Corpus {0}", lineIdx); }
                    }
                    context.PopBatchCompleteStat(contextWriter);
                    query.PopBatchCompleteStat(queryWriter);
                    answer.PopBatchCompleteStat(answerWriter);

                    Console.WriteLine("Context Stat {0}", context.Stat.ToString());
                    Console.WriteLine("Query Stat {0}", query.Stat.ToString());
                    Console.WriteLine("Answer Stat {0}", answer.Stat.ToString());
                }
            }


            public static void Init()
            {
                #region Preprocess Data.
                /// Step 1 : Extract Vocab from training Corpus.
                if (!File.Exists(BuilderParameters.Vocab)) ExtractVocab(BuilderParameters.TrainFolder, BuilderParameters.Vocab);
                else
                {
                    using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab))
                    {
                        entityFreqDict = new ItemFreqIndexDictionary(mreader);
                        wordFreqDict = new ItemFreqIndexDictionary(mreader);
                    }
                }

                /// Step 2 : Corpus 2 Index.
                if (BuilderParameters.IsTrainFile && !File.Exists(BuilderParameters.TrainIndexData))
                {
                    ExtractCorpusIndex(BuilderParameters.TrainFolder, BuilderParameters.TrainIndexData);
                }
                if (BuilderParameters.IsTestFile && !File.Exists(BuilderParameters.TestIndexData))
                {
                    ExtractCorpusIndex(BuilderParameters.TestFolder, BuilderParameters.TestIndexData);
                }

                /// Step 3 : Index 2 Binary Data.
                if (BuilderParameters.IsTrainFile &&
                    (!File.Exists(BuilderParameters.TrainContextBinary) || !File.Exists(BuilderParameters.TrainQueryBinary) || !File.Exists(BuilderParameters.TrainAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.TrainIndexData, BuilderParameters.TrainContextBinary, BuilderParameters.TrainQueryBinary, BuilderParameters.TrainAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                if (BuilderParameters.IsTestFile &&
                    (!File.Exists(BuilderParameters.TestContextBinary) || !File.Exists(BuilderParameters.TestQueryBinary) || !File.Exists(BuilderParameters.TestAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.TestIndexData, BuilderParameters.TestContextBinary, BuilderParameters.TestQueryBinary, BuilderParameters.TestAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                #endregion.

                if (BuilderParameters.IsTrainFile)
                {
                    TrainContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBinary);
                    TrainQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBinary);
                    TrainAnswer = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TrainAnswerBinary);

                    TrainContext.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQuery.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainAnswer.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                }

                if (BuilderParameters.IsTestFile)
                {
                    TestContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestContextBinary);
                    TestQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestQueryBinary);
                    TestAnswer = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TestAnswerBinary);

                    TestContext.InitThreadSafePipelineCashier(64, false);
                    TestQuery.InitThreadSafePipelineCashier(64, false);
                    TestAnswer.InitThreadSafePipelineCashier(64, false);
                }
            }
        }
    }
}