﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    /// <summary>
    /// CNN for Image.
    /// </summary>
    public class ImageCNNBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }

            public static string TrainData { get { return Argument["TRAIN-DATA"].Value; } }
            public static string TrainLabel { get { return Argument["TRAIN-LABEL"].Value; } }

            public static string ValidData { get { return Argument["VALID-DATA"].Value; } }
            public static string ValidLabel { get { return Argument["VALID-LABEL"].Value; } }

            public static int[] SX { get { return Argument["SX"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] Stride { get { return Argument["STRIDE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] O { get { return Argument["O"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] Pad { get { return Argument["PAD"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] Afs { get { return Argument["AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] PoolSX { get { return Argument["POOLSX"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] PoolStride { get { return Argument["POOLSTRIDE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static List<ImageFilterParameter> Filters(int depth)
            {
                List<ImageFilterParameter> mFilters = new List<ImageFilterParameter>();
                int mdepth = depth;
                for (int i = 0; i < SX.Length; i++)
                {
                    mFilters.Add(new ImageFilterParameter(SX[i], Stride[i], mdepth, O[i], Pad[i], Afs[i], PoolSX[i], PoolStride[i]));
                    mdepth = O[i];
                }
                return mFilters;
            }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                Argument.Add("TRAIN-DATA", new ParameterArgument(string.Empty, "Train Image Data"));
                Argument.Add("TRAIN-LABEL", new ParameterArgument(string.Empty, "Train Image Label"));

                Argument.Add("VALID-DATA", new ParameterArgument(string.Empty, "Valid Image Data"));
                Argument.Add("VALID-LABEL", new ParameterArgument(string.Empty, "Valid Image Label"));

                Argument.Add("SX", new ParameterArgument(string.Empty, "Convolutional Filter SX"));
                Argument.Add("STRIDE", new ParameterArgument(string.Empty, "Convolutional Filter Stride"));
                Argument.Add("O", new ParameterArgument(string.Empty, "Convolutional Output Dim"));
                Argument.Add("PAD", new ParameterArgument(string.Empty, "Pad space for image"));
                Argument.Add("AF", new ParameterArgument(string.Empty, "Activation functions"));
                Argument.Add("POOLSX", new ParameterArgument(string.Empty, "Pooling SX"));
                Argument.Add("POOLSTRIDE", new ParameterArgument(string.Empty, "Pooling Stride"));

                Argument.Add("LAYER-DIM", new ParameterArgument(string.Empty, "Layer Dimension"));
                Argument.Add("ACTIVATION", new ParameterArgument(string.Empty, "Activation NN"));
                Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
                Argument.Add("LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));

                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
            }
        }

        public override BuilderType Type { get { return BuilderType.IMAGECNN; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
            //Cudalib.CudaInit(ImageCNNBuilderParameters.GPUID);
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<ImageDataSource, ImageDataStat> imgData,
            IDataCashier<DenseBatchData, DenseDataStat> labelData,
            RunnerBehavior Behavior, List<ImageLinkStructure> ConvStruct, List<LayerStructure> LayerStruct)
        {
            ComputationGraph cg = new ComputationGraph();

            /**************** Get Data from DataCashier *********/
            ImageDataSource image = (ImageDataSource)cg.AddDataRunner(new DataRunner<ImageDataSource, ImageDataStat>(imgData, Behavior));
            DenseBatchData label = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(labelData, Behavior));

            ImageDataSource layerInput = image;
            foreach (ImageLinkStructure linkModel in ConvStruct)
            {
                ImageConvRunner<ImageDataSource> linkRunner = new ImageConvRunner<ImageDataSource>(linkModel, layerInput, Behavior);
                cg.AddRunner(linkRunner);
                layerInput = linkRunner.Output;
            }
            ImageFullyConnectRunner<ImageDataSource> fullRunner = new ImageFullyConnectRunner<ImageDataSource>(LayerStruct[0], layerInput, Behavior);
            cg.AddRunner(fullRunner);
            HiddenBatchData nnLayerInput = fullRunner.Output;
            for (int i = 1; i < LayerStruct.Count; i++)
            {
                FullyConnectHiddenRunner<HiddenBatchData> nnRunner = new FullyConnectHiddenRunner<HiddenBatchData>(LayerStruct[i], nnLayerInput, Behavior);
                cg.AddRunner(nnRunner);
                nnLayerInput = nnRunner.Output;
            }

            /*************** DNN for Source Input. ********************/
            HiddenBatchData pred = nnLayerInput;

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new MultiClassSoftmaxRunner(label.Data, pred.Output, pred.Deriv, BuilderParameters.Gamma, Behavior));
                    break;

                case DNNRunMode.Predict:
                    cg.AddRunner(new AccuracyDiskDumpRunner(label.Data, pred.Output, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath));
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager ComputeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            CompositeNNStructure deepNet = new CompositeNNStructure();

            //Console.WriteLine(Cudalib.CudaDeviceProperties(0));
            List<ImageLinkStructure> ConvLinkStruct = new List<ImageLinkStructure>();
            List<LayerStructure> FullLinkStruct = new List<LayerStructure>();

            Logger.WriteLog("Loading CNN Structure.");

            
                int outWidth = DataPanel.TrainImage.Stat.Width;
                int outHeight = DataPanel.TrainImage.Stat.Height;
                List<ImageFilterParameter> filters = BuilderParameters.Filters(DataPanel.TrainImage.Stat.Depth);
                foreach (ImageFilterParameter filter in filters)
                {
                    ConvLinkStruct.Add(new ImageLinkStructure(filter, device));
                    outWidth = filter.OutputWidth(outWidth);
                    outHeight = filter.OutputHeight(outHeight);
                }
                int featureNum = filters.Last().O * outWidth * outHeight;
                for (int i = 0; i < BuilderParameters.LAYER_DIM.Length; i++)
                {
                    LayerStructure link = new LayerStructure(featureNum, BuilderParameters.LAYER_DIM[i], BuilderParameters.ACTIVATION[i], N_Type.Fully_Connected, 1, 0, true, device);
                    FullLinkStruct.Add(link);
                    featureNum = BuilderParameters.LAYER_DIM[i];
                }

            for (int i = 0; i < ConvLinkStruct.Count; i++)
            {
                deepNet.AddLayer(ConvLinkStruct[i]);
            }
            for (int i = 0; i < FullLinkStruct.Count; i++)
            {
                deepNet.AddLayer(FullLinkStruct[i]);
            }


            ComputationGraph train_cg = BuildComputationGraph(DataPanel.TrainImage, DataPanel.TrainLabel, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = ComputeLib },
                    ConvLinkStruct, FullLinkStruct);
            train_cg.InitOptimizer(deepNet, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = ComputeLib });

            ComputationGraph valid_cg = BuildComputationGraph(DataPanel.ValidImage, DataPanel.ValidLabel, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = ComputeLib },
                ConvLinkStruct, FullLinkStruct);

            double best_score = 0;
            double best_iter = -1;
            for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
            {
                double loss = train_cg.Execute();
                Logger.WriteLog("Train Loss {0}", loss);
                double score = valid_cg.Execute();
                Logger.WriteLog("Prediction Accuracy {0}, at Iteration {1}", score, iter);

                if (score > best_score)
                {
                    best_score = score;
                    best_iter = iter;
                }
                Logger.WriteLog("Best Accuracy {0}, at Iteration {1}", best_score, best_iter);

            }
            Logger.CloseLog();
        }

        public class DataPanel
        {
            public static DataCashier<ImageDataSource, ImageDataStat> TrainImage = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainLabel = null;

            public static DataCashier<ImageDataSource, ImageDataStat> ValidImage = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidLabel = null;

            public static void Init()
            {
                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    TrainImage = new DataCashier<ImageDataSource, ImageDataStat>(BuilderParameters.TrainData);
                    TrainLabel = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainLabel);

                    TrainImage.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainLabel.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                }

                ValidImage = new DataCashier<ImageDataSource, ImageDataStat>(BuilderParameters.ValidData);
                ValidLabel = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidLabel);

                ValidImage.InitThreadSafePipelineCashier(100, false);
                ValidLabel.InitThreadSafePipelineCashier(100, false);
            }
        }
    }
}
