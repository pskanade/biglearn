﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class CompositeNNBuilderParameters : BaseModelArgument<CompositeNNBuilderParameters>
    {
        public static string TrainData { get { return Argument["TRAIN"].Value; } }
        public static string TrainCursorData { get { return Argument["TRAIN-CURSOR"].Value; } }
        public static string ValidData { get { return Argument["VALID"].Value; } }
        public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }

        public static string TestData { get { return Argument["TEST"].Value; } }
        public static bool IsTestFile { get { return !TestData.Equals(string.Empty); } }
        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

        public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
        public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
        public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
        public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
        public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }

        public static int BatchIteration { get { return int.Parse(Argument["BATCH-ITERATION"].Value); } }

        public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
        public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

        public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
        public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
        public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }
        //public static int[] LAYER_MAXOUT { get { return Argument["LAYER-MAXOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static N_Type[] LAYER_ARCH { get { return Argument["LAYER-ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
        public static bool[] LAYER_NORM { get { return Argument["LAYER-NORM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }

        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
        public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
        public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
        public static DeviceType Device { get { return GPUID >= 0 ? DeviceType.GPU : DeviceType.CPU; } }

        public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
        public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }

        public static float LabelMinValue { get { return float.Parse(Argument["LABEL-MIN"].Value); } }
        public static float LabelMaxValue { get { return float.Parse(Argument["LABEL-MAX"].Value); } }

        public static int DebugBatchNum { get { return int.Parse(Argument["DEBUG-BATCH"].Value); } }

        public static float WeightInitBias { get { return float.Parse(Argument["INIT-WEIGHT-BIAS"].Value); } }

        public static float WeightInitScale { get { return float.Parse(Argument["INIT-WEIGHT-SCALE"].Value); } }

        public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }


        /// <summary>
        /// Only need to Specify Data and Archecture of Deep Nets.
        /// </summary>
        static CompositeNNBuilderParameters()
        {
            Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

            Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
            Argument.Add("TRAIN-CURSOR", new ParameterArgument(string.Empty, "Train CURSOR Data."));
            Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
            Argument.Add("TEST", new ParameterArgument(string.Empty, "TEST Data."));
            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

            Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
            Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
            Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
            Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
            Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));

            Argument.Add("BATCH-ITERATION", new ParameterArgument("0", "Train Batch Iteration."));

            Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "DNN Seed Model."));


            Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.Regression).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
            Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.MSE).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

            //Argument.Add("LAYERS-INFO", 

            Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
            Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
            Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
            Argument.Add("LAYER-ARCH", new ParameterArgument(string.Empty, ParameterUtil.EnumValues(typeof(N_Type))));
            Argument.Add("LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));
            Argument.Add("LAYER-NORM", new ParameterArgument("1", "DNN Normalize Layer"));

            Argument.Add("MODEL-PATH", new ParameterArgument(@"\\DEEP-06\Test-JDSSM\Model_Path\", "Model Path"));
            Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
            Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
            Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
            Argument.Add("LABEL-MIN", new ParameterArgument("0", "Label Start with 0 by default;"));
            Argument.Add("LABEL-MAX", new ParameterArgument("5", "Label End with 5 by default;"));

            Argument.Add("DEBUG-BATCH", new ParameterArgument("0", "Evaluation on Every Debug Batch Number"));

            Argument.Add("INIT-WEIGHT-BIAS", new ParameterArgument("0", "Init Model Weight Bias"));
            Argument.Add("INIT-WEIGHT-SCALE", new ParameterArgument("-1", "Init Model Weight Scale"));
        }
    }

    public class CompositeNNBuilder : Builder
    {
        public override BuilderType Type { get { return BuilderType.COMPISITENN; } }

        public override void InitStartup(string fileName)
        {
            CompositeNNBuilderParameters.Parse(fileName);
            if (CompositeNNBuilderParameters.Device == DeviceType.GPU) Cudalib.CudaSetDevice(CompositeNNBuilderParameters.GPUID);
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> data, DNNRunMode mode, CompositeNNStructure NNModel)
        {
            ComputationGraph cg = new ComputationGraph();
            //cg.SetDataSource(data);

            GeneralBatchInputData inputData = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(data,
                            new RunnerBehavior() { RunMode = mode, Device = CompositeNNBuilderParameters.Device }));

            int linkId = 0;
            if(mode == DNNRunMode.Train) 
                NNModel.AddLayer(new LayerStructure(inputData.Stat.MAX_FEATUREDIM, CompositeNNBuilderParameters.LAYER_DIM[0], A_Func.Linear,
                        N_Type.Fully_Connected, 1, 0, CompositeNNBuilderParameters.LAYER_BIAS[0], CompositeNNBuilderParameters.Device));
            LayerStructure hidLayer = (LayerStructure)NNModel.CompositeLinks[linkId++];

            HiddenBatchData hidData = (HiddenBatchData)cg.AddRunner(new FullyConnectInputRunner<GeneralBatchInputData>(hidLayer, inputData,
                new RunnerBehavior() { RunMode = mode, Device = CompositeNNBuilderParameters.Device }));

            if (CompositeNNBuilderParameters.LAYER_NORM[0])
            {
                //if (mode == DNNRunMode.Train)
                //    NNModel.AddLayer(new NormLayerStructure(CompositeNNBuilderParameters.LAYER_DIM[0]));
                
                //NormLayerStructure normLayer = (NormLayerStructure)NNModel.CompositeLinks[linkId++];

                //hidData = (HiddenBatchData)cg.AddRunner(new NormLayerRunner<HiddenBatchData>(normLayer, hidData,
                //    new RunnerBehavior() { RunMode = mode, Device = CompositeNNBuilderParameters.Device }));
            }

            hidData = (HiddenBatchData)cg.AddRunner(new ActivationRunner(hidData, CompositeNNBuilderParameters.ACTIVATION[0],
                new RunnerBehavior() { RunMode = mode, Device = CompositeNNBuilderParameters.Device }));

            for (int i = 1; i < CompositeNNBuilderParameters.LAYER_DIM.Length; i++)
            {
                if (mode == DNNRunMode.Train)
                    NNModel.AddLayer(new LayerStructure(CompositeNNBuilderParameters.LAYER_DIM[i - 1], CompositeNNBuilderParameters.LAYER_DIM[i], A_Func.Linear,
                        N_Type.Fully_Connected, 1, 0, CompositeNNBuilderParameters.LAYER_BIAS[i], CompositeNNBuilderParameters.Device));
                hidLayer = (LayerStructure)NNModel.CompositeLinks[linkId++];

                hidData = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(hidLayer, hidData,
                    new RunnerBehavior() { RunMode = mode, Device = CompositeNNBuilderParameters.Device }));

                //if (CompositeNNBuilderParameters.LAYER_NORM[i])
                //{
                //    if (mode == DNNRunMode.Train)
                //        NNModel.AddLayer(new NormLayerStructure(CompositeNNBuilderParameters.LAYER_DIM[i]));
                //    NormLayerStructure normLayer = (NormLayerStructure)NNModel.CompositeLinks[linkId++];

                //    hidData = (HiddenBatchData)cg.AddRunner(new NormLayerRunner<HiddenBatchData>(normLayer, hidData,
                //        new RunnerBehavior() { RunMode = mode, Device = CompositeNNBuilderParameters.Device }));
                //}
                hidData = (HiddenBatchData)cg.AddRunner(new ActivationRunner(hidData, CompositeNNBuilderParameters.ACTIVATION[i],
                    new RunnerBehavior() { RunMode = mode, Device = CompositeNNBuilderParameters.Device }));
            }

            switch (mode)
            {
                case DNNRunMode.Train:
                    switch (CompositeNNBuilderParameters.LossFunction)
                    {
                        case LossFunctionType.BinaryClassification:
                            //cg.AddObjective(new CrossEntropyRunner(inputData.MatchLabelValue, hidData.Output, hidData.Deriv, CompositeNNBuilderParameters.Gamma,
                            //                    new RunnerBehavior() { RunMode = mode, Device = CompositeNNBuilderParameters.Device }));
                            break;
                    }
                    break;
                case DNNRunMode.Predict:
                    switch (CompositeNNBuilderParameters.Evaluation)
                    {
                        case EvaluationType.AUC:
                            //cg.AddRunner(new AUCDiskDumpRunner(inputData.MatchLabelValue, hidData.Output, CompositeNNBuilderParameters.ScoreOutputPath, CompositeNNBuilderParameters.MetricOutputPath));
                            break;
                    }
                    break;
            }
            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(CompositeNNBuilderParameters.LogFile);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            switch (CompositeNNBuilderParameters.RunMode)
            {
                case DNNRunMode.Train:

                    Logger.WriteLog("Loading DNN Structure.");
                    CompositeNNStructure NNModel = new CompositeNNStructure();

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train, DNNRunMode.Train, NNModel);

                    ComputationGraph validCG = null;
                    ComputationGraph testCG = null;

                    if (CompositeNNBuilderParameters.IsValidFile)
                        validCG = BuildComputationGraph(DataPanel.Valid, DNNRunMode.Predict, NNModel);

                    if (CompositeNNBuilderParameters.IsTestFile)
                        testCG = BuildComputationGraph(DataPanel.Test, DNNRunMode.Predict, NNModel);

                    //GradientCheckFlow gradCheck = new GradientCheckFlow(NNModel, CompositeNNBuilderParameters.Device, trainCG);
                    //gradCheck.Run();

                    //if (trainer.GradientCheck(DataPanel.Train.GetInstances(false)))
                    //    Logger.WriteLog("Gradient Check Successfully!");
                    //else
                    //    Logger.WriteLog("Gradient Check Failed!");

                    NNModel.InitOptimizer(new StructureLearner()
                    {
                        LearnRate = CompositeNNBuilderParameters.LearnRate,
                        Optimizer = CompositeNNBuilderParameters.Optimizer,
                        AdaBoost = CompositeNNBuilderParameters.AdaBoost,
                        ShrinkLR = CompositeNNBuilderParameters.ShrinkLR,
                        BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
                        EpochNum = CompositeNNBuilderParameters.Iteration
                    });

                    if (!Directory.Exists(CompositeNNBuilderParameters.ModelOutputPath))
                        Directory.CreateDirectory(CompositeNNBuilderParameters.ModelOutputPath);

                    DataPanel.Train.TrainBatchNum = CompositeNNBuilderParameters.DebugBatchNum;
                    for (int iter = 0; iter < CompositeNNBuilderParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        //.Train(DataPanel.Train.GetInstances(false));
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss / (DataPanel.Train.Stat.TotalBatchNumber + float.Epsilon));
                        if (CompositeNNBuilderParameters.IsValidFile) validCG.Execute(); // .Predict(DataPanel.Valid.GetInstances(false), evalParameter);
                        if (CompositeNNBuilderParameters.IsTestFile) testCG.Execute(); // .Predict(DataPanel.Test.GetInstances(false), evalParameter);
                        NNModel.Save(string.Format(@"{0}\\DNN.{1}.model", CompositeNNBuilderParameters.ModelOutputPath, iter.ToString()));
                    }

                    ///Batch Iteration.
                    //NNModel.InitGlobalOptimizer(new StructureLearner()
                    //{
                    //    LearnRate = CompositeNNBuilderParameters.LearnRate,
                    //    Optimizer = DNNOptimizer.BatchLBFGS,
                    //    AdaBoost = CompositeNNBuilderParameters.AdaBoost,
                    //    BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
                    //    EpochNum = CompositeNNBuilderParameters.BatchIteration,
                    //    ShrinkLR = CompositeNNBuilderParameters.ShrinkLR
                    //});
                    //DataPanel.Train.TrainBatchNum = 0;
                    //for (int i = 0; i < CompositeNNBuilderParameters.BatchIteration; i++)
                    //{
                    //    NNModel.ResetGlobalOptimization();
                    //    double loss = trainer.Train(DataPanel.Train.GetInstances(false));
                    //    NNModel.EndGlobalOptimization(loss, DataPanel.Train.GetInstances(false), trainer.Loss);
                    //    Logger.WriteLog("Iteration {0} \t Train Loss {1}", i, loss);
                    //    Logger.WriteLog("Validation Set Evaluation!");
                    //    if (CompositeNNBuilderParameters.IsValidFile) evaler.Predict(DataPanel.Valid.GetInstances(false), evalParameter);
                    //    if (CompositeNNBuilderParameters.IsTestFile) tester.Predict(DataPanel.Test.GetInstances(false), evalParameter);
                    //    NNModel.Save(string.Format(@"{0}\\DNN.Batch.{1}.model", CompositeNNBuilderParameters.ModelOutputPath, i.ToString()));
                    //}
                    break;
                case DNNRunMode.Predict:
                    break;
            }
            DataPanel.DeInit();
            Logger.CloseLog();
        }

        
        public class DataPanel
        {
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Train = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Valid = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Test = null;

            public static void Init()
            {
                if (CompositeNNBuilderParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(CompositeNNBuilderParameters.TrainData, CompositeNNBuilderParameters.TrainCursorData);
                    Train.InitThreadSafePipelineCashier(100, true);
                }

                if (CompositeNNBuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(CompositeNNBuilderParameters.ValidData);
                    Valid.InitThreadSafePipelineCashier(100, false);
                }

                if (CompositeNNBuilderParameters.IsTestFile)
                {
                    Test = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(CompositeNNBuilderParameters.TestData);
                    Test.InitThreadSafePipelineCashier(100, false);
                }
            }

            internal static void DeInit()
            {
                if (null != Train)
                {
                    Train.Dispose();
                    Train = null;
                }

                if (null != Valid)
                {
                    Valid.Dispose();
                    Valid = null;
                }

                if (null != Test)
                {
                    Test.Dispose();
                    Test = null;
                }
            }
        }
    }

}
