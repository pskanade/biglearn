﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    
    public class RNNSeqPredBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("LAYER-LAG", new ParameterArgument("5", "DNN Layer Dim"));
                Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
                Argument.Add("LAYER-DROPOUT", new ParameterArgument("0", "Layer Dropout"));

                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument(string.Empty, "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model Path"));

            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static DNNRunMode RunMode
            {
                get
                {
                    if (IsTrainFile) return DNNRunMode.Train;
                    else return DNNRunMode.Predict;
                }
            }

            public static string TrainData { get { return Argument["TRAIN"].Value; } }

            public static bool IsTrainFile { get { return !TrainData.Equals(string.Empty); } }

            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)Enum.Parse(typeof(A_Func), i, true)).ToArray(); } }
            public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static int[] LAYER_LAG { get { return Argument["LAYER-LAG"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value == string.Empty ? LogFile + ".tmp.score" : Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value == string.Empty ? LogFile + ".tmp.metric" : Argument["METRIC-PATH"].Value; } }

            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModelPath { get { return (Argument["SEED-MODEL"].Value); } }

        }


        public override BuilderType Type { get { return BuilderType.RNNSEQPRED; } }
        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseDataSource, SequenceDataStat> data,
            RNNStructure rnnModel, RunnerBehavior behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            /// Data Source Runner. 
            SeqSparseDataSource dataSource = (SeqSparseDataSource)cg.AddRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(data, behavior));
            SeqDenseBatchData rnnOutput = (SeqDenseBatchData)cg.AddRunner(new RNNSparseRunner(rnnModel.RNNCells[0], dataSource.SequenceData, behavior));


            for (int i = 1; i < rnnModel.RNNCells.Count; i++)
            {
                if (BuilderParameters.LAYER_DROPOUT.Length > i - 1 && BuilderParameters.LAYER_DROPOUT[i - 1] > 0 && behavior.RunMode == DNNRunMode.Train)
                {
                    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(rnnOutput.MAX_SENTSIZE, rnnOutput.Dim, rnnOutput.SentOutput, rnnOutput.SentDeriv, rnnOutput.DeviceType),
                       BuilderParameters.LAYER_DROPOUT[i - 1], behavior, false));
                }
                rnnOutput = (SeqDenseBatchData)cg.AddRunner(new RNNDenseRunner(rnnModel.RNNCells[i], rnnOutput, behavior));
            }
            switch (behavior.RunMode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new CrossEntropyRunner(dataSource.SequenceLabel,
                         new DenseBatchData(new DenseDataStat() { Dim = rnnOutput.Dim, MAX_BATCHSIZE = rnnOutput.Stat.MAX_SEQUENCESIZE }, rnnOutput.SentOutput, rnnOutput.DeviceType),
                         new DenseBatchData(new DenseDataStat() { Dim = rnnOutput.Dim, MAX_BATCHSIZE = rnnOutput.Stat.MAX_SEQUENCESIZE }, rnnOutput.SentDeriv, rnnOutput.DeviceType),
                         BuilderParameters.Gamma, behavior));
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new AUCDiskDumpRunner(dataSource.SequenceLabel,
                        new DenseBatchData(new DenseDataStat() { Dim = rnnOutput.Dim, MAX_BATCHSIZE = rnnOutput.Stat.MAX_SEQUENCESIZE }, rnnOutput.SentOutput, rnnOutput.DeviceType),
                        BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                    break;
            }
            return cg;
        }

        public unsafe override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            RNNStructure rnnModel = null;

            if(BuilderParameters.SeedModelPath.Equals(string.Empty))
            {
                rnnModel = new RNNStructure(DataPanel.Train.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM, BuilderParameters.LAYER_LAG, BuilderParameters.ACTIVATION, BuilderParameters.LAYER_BIAS, device);
            }
            else
            {
                rnnModel = new RNNStructure(new BinaryReader(new FileStream(BuilderParameters.SeedModelPath, FileMode.Open, FileAccess.Read)), device);
            }

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    
                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train, rnnModel, new RunnerBehavior(DNNRunMode.Train, device, computeLib));
                    trainCG.InitOptimizer(rnnModel, OptimizerParameters.StructureOptimizer, new RunnerBehavior(DNNRunMode.Train, device, computeLib));
                    
                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraph(DataPanel.Valid, rnnModel, new RunnerBehavior(DNNRunMode.Predict, device, computeLib));

                    double validScore = 0;
                    double bestValidScore = double.MinValue;
                    double bestValidIter = -1;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        rnnModel.Save(string.Format(@"{0}\\RNN.{1}.model", BuilderParameters.ModelOutputPath, iter.ToString()));

                        if (BuilderParameters.IsValidFile)
                        {
                            Logger.WriteLog("Evaluation Tool Path {0}; Output Score Path {1}", EvaluationToolkit.Path, BuilderParameters.ScoreOutputPath);
                            validScore = validCG.Execute();
                            if (validScore >= bestValidScore)
                            {
                                bestValidScore = validScore;
                                bestValidIter = iter;
                                File.Copy(string.Format(@"{0}\\RNN.{1}.model", BuilderParameters.ModelOutputPath, bestValidIter.ToString()),
                                          string.Format(@"{0}\\RNN.{1}.model", BuilderParameters.ModelOutputPath, "done"), true);
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                        }
                    }
                    Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);

                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.Valid, rnnModel, new RunnerBehavior(DNNRunMode.Predict, device, computeLib));
                    predCG.Execute();
                    break;
            }
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Train = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Valid = null;

            public static void Init()
            {
                if (BuilderParameters.IsValidFile)
                {
                    //if (!RNNSeqPredBuilderParameters.ValidData.EndsWith(".eds"))
                    //{
                    //    
                    //}
                    //else
                    //{
                    //    Valid = new LocalDataCashier<SeqSparseDataSource, SequenceDataStat>(RNNSeqPredBuilderParameters.ValidData);
                    //}
                    Valid = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidData);
                    Valid.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainData);
                    //if (!RNNSeqPredBuilderParameters.TrainData.EndsWith(".eds"))
                    //{ }
                    //else
                    //{
                    //    Train = new LocalDataCashier<SeqSparseDataSource, SequenceDataStat>(RNNSeqPredBuilderParameters.TrainData);
                    //}
                    Train.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                }
            }

            internal static void DeInit()
            {
                if (null != Train) Train.Dispose(); Train = null;
                if (null != Valid) Valid.Dispose(); Valid = null;
            }
        }
    }
}
