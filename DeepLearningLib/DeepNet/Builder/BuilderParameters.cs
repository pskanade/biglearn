using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    public class BuilderParameters : BaseModelArgument<BuilderParameters>
    {
        public static BuilderType Type { get { return (BuilderType)Enum.Parse(typeof(BuilderType), Argument["BUILDER"].Value.ToUpper()); } }

        public static string EvalToolPath { get { return (Argument["EVAL-TOOL-PATH"].Value); } }

        public static int ModelSaveIteration { get { return int.Parse(Argument["MODEL-PER-ITERATION"].Value); } }

        public static bool ModelSavePerIteration { get { return int.Parse(Argument["MODEL-PER-ITERATION"].Value) > 0; } }

        public static int RandomSeed { get { return int.Parse(Argument["RANDOM-SEED"].Value); } }

        public static int ModelUpdatePerSave { get { return int.Parse(Argument["MODEL-PER-UPDATE"].Value); } }

        public static bool ParameterDebug { get { return int.Parse(Argument["PARAMETER-DEBUG"].Value) > 0; } }
        
        public static int? gpuid;
        public static int GPUID
        {
            get
            {
                if (!gpuid.HasValue)
                {
                    gpuid = int.Parse(Argument["GPUID"].Value);
                }

                return gpuid.Value;
            }
            set
            {
                gpuid = value;
            }
        }

        public static int? WorkerId { get { return string.IsNullOrEmpty(Argument["WORKER-ID"].Value) ? (int?)null : int.Parse(Argument["WORKER-ID"].Value); } }

        public static string CheckPoint { get { return Argument["CHECK-POINT"].Value; } }

        public static PlatformType DistMode { get { return (PlatformType)Enum.Parse(typeof(PlatformType), Argument["DIST-MODE"].Value, true); } }

        public static int IoThreadNum { get { return int.Parse(Argument["IO-THREAD-NUM"].Value); } }

        public static bool IsLogFile { get { return int.Parse(Argument["IS-LOG-FILE"].Value) > 0; } }

        public static DeviceType Device
        {
            get
            {
                int gpuid = int.Parse(Argument["GPUID"].Value);
                if (gpuid >= 0)
                    return DeviceType.GPU;
                if (gpuid == -1)
                    return DeviceType.CPU;
                else
                    return DeviceType.CPU_FAST_VECTOR;
            }
        }

        static BuilderParameters()
        {
            Argument.Add("BUILDER", new ParameterArgument((BuilderType.JDSSM).ToString(), ParameterUtil.EnumValues(typeof(BuilderType))));
            Argument.Add("EVAL-TOOL-PATH", new ParameterArgument(@"\\deep-05\EvaluationToolkit\", "Evaluation Tool Folder"));
            Argument.Add("MODEL-PER-ITERATION", new ParameterArgument("1", "0 : Don't save model per iteration; 1 : Save Model Per iteration"));
            Argument.Add("MODEL-PER-UPDATE", new ParameterArgument("0", "0 : Don't save model per update; Save Model Per update"));
            Argument.Add("IS-LOG-FILE", new ParameterArgument("1", "0 : No Log File; 1 : Log File;"));
            Argument.Add("RANDOM-SEED", new ParameterArgument("13", "Model Random Seed."));
            Argument.Add("PARAMETER-DEBUG", new ParameterArgument("0", "Is Inspect model Parameter?"));

            Argument.Add("DIST-MODE", new ParameterArgument(@"0", "Distribute mode"));
            Argument.Add("IO-THREAD-NUM", new ParameterArgument(@"2", "AllReduce IO thread number"));
            Argument.Add("WORKER-ID", new ParameterArgument("", "The id of this worker."));
            Argument.Add("CHECK-POINT", new ParameterArgument("", "The check point path."));
            Argument.Add("GPUID", new ParameterArgument("1", "GPU Device ID"));
        }

    }

    /// <summary>
    /// Parameters for Optimizer.
    /// </summary>
    public class OptimizerParameters : BaseModelArgument<OptimizerParameters>
    {
        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

        /// <summary>
        /// AutoAdjust Learning Rate.
        /// </summary>
        public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }

        /// <summary>
        /// Initial Learning Rate.
        /// </summary>
        public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }

        /// <summary>
        /// Training Iterations.
        /// </summary>
        public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }

        /// <summary>
        /// Optimization Method.
        /// </summary>
        public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }

        public static string OptimizerArgs { get { return Argument["OPTIMIZER-ARGS"].Value; } }

        public static IGradientAggregatorFactory GradientAgg { get; set; }

        public static List<Tuple<int, float>> ScheduleLR
        {
            get
            {
                if (Argument["SCHEDULE-LR"].Value.Equals(string.Empty)) return new List<Tuple<int, float>>();
                return Argument["SCHEDULE-LR"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
            }
            set
            {
                string mv = string.Join(",", value.Select(i => string.Format("{0}:{1}", i.Item1, i.Item2)));

                if (Argument.ContainsKey("SCHEDULE-LR"))
                    Argument["SCHEDULE-LR"].Value = mv;
                else
                    Argument.Add("SCHEDULE-LR", new ParameterArgument(mv, ""));

            }
        }

        public static int ScheduleLRTyp { get { return int.Parse(Argument["SCHEDULE-LR-TYPE"].Value); } }


        static StructureLearner mStructureOptimizer = null;
        /// <summary>
        /// call OptimizerParameters.Parse(config) before using StructureOptimizer;
        /// </summary>
        public static StructureLearner StructureOptimizer
        {
            get
            {
                if (mStructureOptimizer == null)
                {

                    mStructureOptimizer = new StructureLearner()
                    {
                        LearnRate = LearnRate,
                        Optimizer = Optimizer,
                        ShrinkLR = ShrinkLR,
                        EpochNum = Iteration,
                        ClipDelta = float.Parse(Argument["DELTA-CLIP"].Value),
                        ClipWeight = float.Parse(Argument["WEIGHT-CLIP"].Value),
                        EMA_Decay = float.Parse(Argument["EMA-DECAY"].Value),
                        AdaDelta_Rho = float.Parse(Argument["ADADELTA-RHO"].Value),
                        AdaDelta_Epsilon = float.Parse(Argument["ADADELTA-EPSILON"].Value),

                        Adam_Beta1 = float.Parse(Argument["ADAM-BETA1"].Value),
                        Adam_Beta2 = float.Parse(Argument["ADAM-BETA2"].Value),
                        Adam_Epsilon = float.Parse(Argument["ADAM-EPSILON"].Value),

                        RmsProp_Decay = float.Parse(Argument["RMSPROP-DECAY"].Value),
                        RmsProp_Epsilon = float.Parse(Argument["RMSPROP-EPSILON"].Value),

                        OptArgs = OptimizerArgs,

                        ScheduleLR = ScheduleLR,
                        ScheduleLRTyp = ScheduleLRTyp,

                        device = MathOperatorManager.Device(GPUID),
                    };
                }
                return mStructureOptimizer;
            }
        }

        static OptimizerParameters()
        {
            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
            Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
            Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
            Argument.Add("ITERATION", new ParameterArgument("200", "Train Iteration."));
            Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));
            Argument.Add("DELTA-CLIP", new ParameterArgument("0", "Model Update Clip."));
            Argument.Add("WEIGHT-CLIP", new ParameterArgument("0", "Model Weight Clip"));
            
            Argument.Add("EMA-DECAY", new ParameterArgument("1.0", "Expontenial Moving Average Decay."));

            Argument.Add("ADADELTA-RHO", new ParameterArgument("0.9995", "AdaDelta-Rho"));
            Argument.Add("ADADELTA-EPSILON", new ParameterArgument("1.0e-6", "AdaDelta-Epsilon"));

            Argument.Add("ADAM-BETA1", new ParameterArgument("0.95", "AdaDelta-Epsilon"));
            Argument.Add("ADAM-BETA2", new ParameterArgument("0.999", "AdaDelta-Rho"));
            Argument.Add("ADAM-EPSILON", new ParameterArgument("1.0e-8", "AdaDelta-Epsilon"));

            Argument.Add("RMSPROP-DECAY", new ParameterArgument("0.95", "RmsProp-Decay"));
            Argument.Add("RMSPROP-EPSILON", new ParameterArgument("1.0e-6", "RmsProp-Epsilon"));

            Argument.Add("OPTIMIZER-ARGS", new ParameterArgument("", "The optimizer arguments."));

            Argument.Add("SCHEDULE-LR", new ParameterArgument(string.Empty, "Learning Rate Schedule."));
            Argument.Add("SCHEDULE-LR-TYPE", new ParameterArgument("0", "0:LearningRateLRJump; 1:LearningRateLRSmooth"));

        }
    }
}
