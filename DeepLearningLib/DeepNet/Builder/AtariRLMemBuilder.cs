﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    //public class AtariRLMemBuilder : Builder
    //{
    //    public class BuilderParameters : BaseModelArgument<BuilderParameters>
    //    {
    //        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

    //        public static int[] SX { get { return Argument["SX"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static int[] STRIDE { get { return Argument["STRIDE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static int[] O { get { return Argument["O"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static A_Func[] AFS { get { return Argument["AFS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

    //        public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
    //        public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }

    //        public static DeviceType Device { get { return GPUID >= 0 ? DeviceType.GPU : DeviceType.CPU; } }

    //        public static string EnvirPath { get { return Argument["ENVIR-PATH"].Value; } }
    //        public static string RomPath { get { return Argument["ROM-PATH"].Value; } }

    //        public static ALEAction[] ACTIONS { get { return Argument["ACTIONS"].Value.Split(',').Select(i => (ALEAction)int.Parse(i)).ToArray(); } }

    //        public static int SKIP_FRAME { get { return int.Parse(Argument["SKIP-FRAME"].Value); } }
    //        public static int HIS_FRAME { get { return int.Parse(Argument["HIS-FRAME"].Value); } }
    //        public static int[] CROP_FRAME { get { return Argument["CROP-FRAME"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static bool DISPLAY { get { return int.Parse(Argument["DISPLAY"].Value) > 0; } }

    //        public static List<Tuple<int, float>> ScheduleEpsilon
    //        {
    //            get
    //            {
    //                return Argument["SCHEDULE-EPSILON"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
    //            }
    //        }

    //        public static List<Tuple<int, float>> ScheduleRewardDiscount
    //        {
    //            get
    //            {
    //                return Argument["SCHEDULE-REWARD-DISCOUNT"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
    //            }
    //        }

    //        public static float Epsilon(int step)
    //        {
    //            for (int i = 0; i < ScheduleEpsilon.Count; i++)
    //            {
    //                if (step < ScheduleEpsilon[i].Item1)
    //                {
    //                    float lambda = (step - ScheduleEpsilon[i - 1].Item1) * 1.0f / (ScheduleEpsilon[i].Item1 - ScheduleEpsilon[i - 1].Item1);
    //                    return lambda * ScheduleEpsilon[i].Item2 + (1 - lambda) * ScheduleEpsilon[i - 1].Item2;
    //                }
    //            }
    //            return ScheduleEpsilon.Last().Item2;
    //        }

    //        public static int TargetSyncup { get { return int.Parse(Argument["TARGET-SYNCUP"].Value); } }
    //        public static int ReplayBuffer { get { return int.Parse(Argument["REPLAY-BUFFER"].Value); } }
    //        public static int ReplayColdStart { get { return int.Parse(Argument["REPLAY-COLDSTART"].Value); } }
    //        public static int ReplayBatchSize { get { return int.Parse(Argument["REPLAY-BATCHSIZE"].Value); } }


    //        //public static float RewardDiscount { get { return int.Parse(Argument["REWARD-DISCOUNT"].Value); } }

    //        public static int TestEpsoid { get { return int.Parse(Argument["TEST-EPSOID"].Value); } }

    //        public static float TerminateReward { get { return float.Parse(Argument["TERMINATE-REWARD"].Value); } }

    //        public static int UpdateFreq { get { return int.Parse(Argument["UPDATE-FREQ"].Value); } }

    //        public static int ValidFreq { get { return int.Parse(Argument["VALID-FREQ"].Value); } }
    //        public static float ValidEps { get { return float.Parse(Argument["VALID-EPS"].Value); } }

    //        public static bool ClipReward { get { return int.Parse(Argument["CLIP-REWARD"].Value) > 0; } }
    //        public static float ClipDelta { get { return float.Parse(Argument["CLIP-DELTA"].Value); } }

    //        public static bool AVG_LOSS { get { return int.Parse(Argument["IS-AVG-LOSS"].Value) > 0; } }

    //        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

    //        public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }

    //        public static float WeightInitBias { get { return float.Parse(Argument["INIT-WEIGHT-BIAS"].Value); } }

    //        public static float WeightInitScale { get { return float.Parse(Argument["INIT-WEIGHT-SCALE"].Value); } }

    //        public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }


    //        /// <summary>
    //        /// Only need to Specify Data and Archecture of Deep Nets.
    //        /// </summary>
    //        static BuilderParameters()
    //        {
    //            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

    //            Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "DNN Seed Model."));

    //            Argument.Add("SX", new ParameterArgument("8,4,3", "DNN Layer Dim"));
    //            Argument.Add("STRIDE", new ParameterArgument("4,2,1", "DNN Layer Dim"));
    //            Argument.Add("O", new ParameterArgument("32,64,64", "DNN Layer Bias"));
    //            Argument.Add("AFS", new ParameterArgument(string.Format("{0},{1},{2}", (int)A_Func.Rectified, (int)A_Func.Rectified, (int)A_Func.Rectified), ParameterUtil.EnumValues(typeof(A_Func))));

    //            Argument.Add("LAYER-DIM", new ParameterArgument("100,4", "DNN Layer Dim"));
    //            Argument.Add("ACTIVATION", new ParameterArgument(string.Format("{0},{1}", (int)A_Func.Rectified, (int)A_Func.Linear), ParameterUtil.EnumValues(typeof(A_Func))));
    //            Argument.Add("LAYER-BIAS", new ParameterArgument("1,1", "DNN Layer Bias"));

    //            Argument.Add("ENVIR-PATH", new ParameterArgument(string.Empty, "Envir path"));
    //            Argument.Add("ROM-PATH", new ParameterArgument(string.Empty, "Rom path"));
    //            Argument.Add("DISPLAY", new ParameterArgument("1", "Display the game"));

    //            Argument.Add("ACTIONS", new ParameterArgument("1,4,3", "Action Space; PLAYER_A_NOOP = 0, PLAYER_A_FIRE = 1, PLAYER_A_UP = 2, PLAYER_A_RIGHT = 3, PLAYER_A_LEFT = 4, "));

    //            Argument.Add("SKIP-FRAME", new ParameterArgument("6", "SKIP Frame Number."));
    //            Argument.Add("HIS-FRAME", new ParameterArgument("4", "History Frame Number."));
    //            Argument.Add("CROP-FRAME", new ParameterArgument("84,100,0,84,16,84", "CROP Frame Region"));

    //            Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
    //            Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));

    //            Argument.Add("INIT-WEIGHT-BIAS", new ParameterArgument("0", "Init Model Weight Bias"));
    //            Argument.Add("INIT-WEIGHT-SCALE", new ParameterArgument("-1", "Init Model Weight Scale"));

    //            Argument.Add("SCHEDULE-EPSILON", new ParameterArgument("0:1,3000000:0.1,10000000:0.001,50000000:0.000001", "Epsilon Schedule"));
    //            Argument.Add("SCHEDULE-REWARD-DISCOUNT", new ParameterArgument("0:0.1,50000:0.5,100000:0.8,1000000:0.99", "Reward Discount Schedule"));

    //            Argument.Add("TARGET-SYNCUP", new ParameterArgument("8000", "Target Network Syncup Frequency"));
    //            Argument.Add("REPLAY-BUFFER", new ParameterArgument("65536", "Replay Buffer Size."));
    //            Argument.Add("REPLAY-COLDSTART", new ParameterArgument("1024", "Replay cold Starting."));
    //            Argument.Add("REPLAY-BATCHSIZE", new ParameterArgument("32", "Replay cold Starting."));

    //            Argument.Add("TERMINATE-REWARD", new ParameterArgument("-1", "Terminate reward."));
    //            Argument.Add("CLIP-REWARD", new ParameterArgument("0", "0:origin reward, 1:clip reward 1,-1"));
    //            Argument.Add("CLIP-DELTA", new ParameterArgument("0", "0:no clip delta."));

    //            Argument.Add("TEST-EPSOID", new ParameterArgument("0", "Test Epsoid."));
    //            Argument.Add("UPDATE-FREQ", new ParameterArgument("1", "Update Freq"));
    //            Argument.Add("VALID-FREQ", new ParameterArgument("0", "Valid Freq"));
    //            Argument.Add("VALID-EPS", new ParameterArgument("0.001", "Valid Eps"));

    //            Argument.Add("IS-AVG-LOSS", new ParameterArgument("1", "0:sum loss function; 1:average loss function"));
    //        }
    //    }

    //    public override BuilderType Type { get { return BuilderType.ATARI_RL_EMEM; } }
    //    IntPtr AtariEnvPtr { get; set; }
    //    public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

    //    public ComputationGraph BuildComputationGraph(IntPtr atariEnvPtr, ImageCNNStructure Q_HNet, ECMemory[] Q_Table, DNNStructure Q_SNet,
    //        ImageCNNStructure TargetNetwork, RunnerBehavior behavior)
    //    {
    //        ComputationGraph cg = new ComputationGraph();

    //        /// Current Image.
    //        ImageDataSource senseImage = (ImageDataSource)cg.AddDataRunner(new AtariDataRunner(atariEnvPtr, behavior) { IsBackProp = false });

    //        HiddenBatchData h = (HiddenBatchData)cg.AddRunner(new ImageCNNRunner(Q_HNet, senseImage, behavior) { IsBackProp = false });
    //        cg.AddRunner(new VecNormRunner(h, behavior) { IsBackProp = false });

    //        // alpha * dqnScore + beta * qTable
    //        HiddenBatchData dqnScore = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(Q_SNet, h, behavior) { IsBackProp = false });

    //        ECMemoryQueryRunner ecQueryRunner = new ECMemoryQueryRunner(h, Q_Table, 20, behavior) { IsBackProp = false };
    //        cg.AddRunner(ecQueryRunner);

    //        AtariActionRunner actionRunner = new AtariActionRunner(atariEnvPtr, dqnScore, ecQueryRunner.QValue, ecQueryRunner.QCount, behavior) { IsBackProp = false };
    //        cg.AddRunner(actionRunner);

    //        AtariReplayBatchData replayData = (AtariReplayBatchData)cg.AddRunner(new AtariReplayMemoryRunner(senseImage, actionRunner.ActionOutput,
    //            actionRunner.RewardData, actionRunner.IsTerminal, behavior)
    //        { IsBackProp = false });


    //        cg.AddRunner(new ModelSyncRunner(QNetwork, TargetNetwork, BuilderParameters.TargetSyncup, behavior) { IsBackProp = false });


    //        if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
    //        {
    //            cg.AddRunner(new ModelDiskDumpRunner(QNetwork, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "model"));
    //        }
    //        HiddenBatchData targetAction = (HiddenBatchData)cg.AddRunner(new ImageCNNRunner(TargetNetwork, replayData.Target, behavior) { IsBackProp = false, IsUpdate = false });
    //        HiddenBatchData sourceAction = (HiddenBatchData)cg.AddRunner(new ImageCNNRunner(QNetwork, replayData.Source, behavior));
    //        DenseBatchData updateReward = (DenseBatchData)cg.AddRunner(new RewardUpdateRunner(replayData.Reward, replayData.TargetMapping, targetAction,
    //            BuilderParameters.ScheduleRewardDiscount, behavior)
    //        { IsBackProp = false });
    //        cg.AddObjective(new MSERunner(sourceAction.Output, updateReward, replayData.Action.Data, sourceAction.Deriv, behavior, BuilderParameters.AVG_LOSS, BuilderParameters.ClipDelta) { IsBackProp = false });
    //        return cg;
    //    }

    //    public static List<ImageFilterParameter> Filters(int depth, int[] SX, int[] Stride, int[] O, A_Func[] afs)
    //    {
    //        List<ImageFilterParameter> mFilters = new List<ImageFilterParameter>();
    //        int mdepth = depth;
    //        for (int i = 0; i < SX.Length; i++)
    //        {
    //            mFilters.Add(new ImageFilterParameter(SX[i], Stride[i], mdepth, O[i], 0, afs[i], 1, 1));
    //            mdepth = O[i];
    //        }
    //        return mFilters;
    //    }

    //    public override void Rock()
    //    {
    //        Logger.OpenLog(BuilderParameters.LogFile);

    //        string arg = string.Format(@"place_holder -display_screen {1} -frame_skip {0} -use_environment_distribution false -restricted_action_set true -run_length_encoding false -disable_color_averaging true -use_starting_actions false -max_num_episodes 0 -system_reset_steps 1 -player_agent keyboard_agent {2}",
    //            BuilderParameters.SKIP_FRAME, BuilderParameters.DISPLAY ? "true" : "false", BuilderParameters.RomPath);
    //        string[] arguments = arg.Split(' ');
    //        Logger.WriteLog(System.AppDomain.CurrentDomain.BaseDirectory);
    //        AtariEnvPtr = ALELib.CreateEnvironment(arguments.Length, arguments);

    //        DeviceBehavior device = new DeviceBehavior(BuilderParameters.GPUID);

    //        ImageCNNStructure QNetwork = null;
    //        ImageCNNStructure TargetNetwork = null;

    //        Logger.WriteLog("Loading CNN Structure.");

    //        //int[] SX = new int[] { 8, 4, 3};
    //        //int[] Stride = new int[] {4, 2, 1};
    //        //int[] O = new int[] { 32, 64, 64 };
    //        //int[] Pad = new int[] { 0, 0, 0 };
    //        //A_Func[] Afs = new A_Func[] { A_Func.Rectified, A_Func.Rectified, A_Func.Rectified };
    //        //int[] PoolSX = new int[] { 1, 1, 1 };
    //        //int[] PoolStride = new int[] { 1, 1, 1};

    //        //int[] NNLayerDim = new int[] { 512, BuilderParameters.ACTIONS.Length };
    //        //A_Func[] NNAfs = new A_Func[] { A_Func.Rectified, A_Func.Linear };
    //        //bool[] NNBias = new bool[] { true, false };
    //        // previous 4 frames.

    //        if (BuilderParameters.SEED_MODEL == string.Empty)
    //        {
    //            QNetwork = new ImageCNNStructure(new ImageInputParameter() { Width = AtariDataRunner.ActWidth, Height = AtariDataRunner.ActHeight, Depth = AtariDataRunner.History },
    //                Filters(AtariDataRunner.History, BuilderParameters.SX, BuilderParameters.STRIDE, BuilderParameters.O, BuilderParameters.AFS), BuilderParameters.LAYER_DIM,
    //                BuilderParameters.ACTIVATION, Enumerable.Range(0, BuilderParameters.LAYER_DIM.Length).Select(i => 0.0f).ToArray(), BuilderParameters.LAYER_BIAS);
    //            QNetwork.Init();
    //        }
    //        else
    //        {
    //            QNetwork = new ImageCNNStructure(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device.Device);
    //        }
    //        //    QNetwork = new ImageCNNStructure(
    //        //                new ImageInputParameter() { Width = DataPanel.Train.Stat.SrcStat.Width, Height = DataPanel.Train.Stat.SrcStat.Height, Depth = DataPanel.Train.Stat.SrcStat.Depth },
    //        //                ImageCNNBuilderParameters.Filters(DataPanel.Train.Stat.SrcStat.Depth),
    //        //                ImageCNNBuilderParameters.LAYER_DIM, BuilderParameters.ACTIVATION, ImageCNNBuilderParameters.LAYER_DROPOUT, ImageCNNBuilderParameters.LAYER_BIAS);

    //        TargetNetwork = new ImageCNNStructure(new ImageInputParameter() { Width = AtariDataRunner.ActWidth, Height = AtariDataRunner.ActHeight, Depth = AtariDataRunner.History },
    //                Filters(AtariDataRunner.History, BuilderParameters.SX, BuilderParameters.STRIDE, BuilderParameters.O, BuilderParameters.AFS), BuilderParameters.LAYER_DIM,
    //                BuilderParameters.ACTIVATION, Enumerable.Range(0, BuilderParameters.LAYER_DIM.Length).Select(i => 0.0f).ToArray(), BuilderParameters.LAYER_BIAS);
    //        TargetNetwork.CopyFrom(QNetwork);

    //        //sense.StatusFeatureDim, BuilderParameters.LAYER_DIM, BuilderParameters.ACTIVATION, BuilderParameters.LAYER_BIAS);
    //        //    TargetNetwork.Init(QNetwork);
    //        //}
    //        //Logger.WriteLog(QNetwork.DNN_Descr());
    //        //Logger.WriteLog("Load DNN Structure Finished.");
    //        QNetwork.InitOptimizer(OptimizerParameters.StructureOptimizer, device.TrainMode);

    //        ComputationGraph trainCG = BuildComputationGraph(AtariEnvPtr, QNetwork, TargetNetwork, device.TrainMode);
    //        trainCG.SetDelegateModel(QNetwork);

    //        if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);
    //        //for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
    //        //{
    //        trainCG.Execute();
    //        //    QNetwork.Save(string.Format(@"{0}\\CNN.{1}.model", BuilderParameters.ModelOutputPath, iter.ToString()));
    //        //}
    //        //enviroument.CloseReplayStream();
    //        Logger.CloseLog();
    //        ALELib.ExitEnvironment(AtariEnvPtr);
    //    }

    //    /// <summary>
    //    /// please not use eps-greedy for exploration.
    //    /// </summary>
    //    public class ECMemory
    //    {
    //        public CudaPieceFloat Buffer;


    //        public float[] V;
    //        public float[] Count;
    //        public int[] Time;

    //        public int Num;
    //        public int BufferSize;
    //        int Dim;
    //        int K = 20;
    //        float eps = 0.001f;
    //        public int MemoryTime;

    //        public ECMemory(int bufferSize, int dim, DeviceType device)
    //        {
    //            Buffer = new CudaPieceFloat(bufferSize * dim, device);
    //            V = new float[bufferSize];
    //            Count = new float[bufferSize];
    //            Time = new int[bufferSize];
    //            Num = 0;
    //            BufferSize = bufferSize;
    //            Dim = dim;
    //            MemoryTime = 0;
    //        }

    //        //float Distance(float[] p1, float[] p2, int start, int dim)
    //        //{
    //        //    float d = 0;
    //        //    for (int i = 0; i < dim; i++)
    //        //    {
    //        //        d += (p1[i] - p2[start + i]) * (p1[i] - p2[start + i]);
    //        //    }
    //        //    return d;
    //        //}

    //        /// <summary>
    //        /// return q, count, target, order.
    //        /// </summary>
    //        /// <param name="q"></param>
    //        /// <returns></returns>
    //        //public Tuple<float, float, int, int> Query(float[] q)
    //        //{
    //        //    MinMaxHeap<int> heap = new MinMaxHeap<int>(K, -1);
    //        //    for (int i = 0; i < Num; i++)
    //        //    {
    //        //        int idx = Index[i];
    //        //        float v = Distance(q, Buffer, idx * Dim, Dim);
    //        //        heap.push_pair(i, v);
    //        //    }
    //        //    heap.topK.Sort((firstPair, nextPair) =>
    //        //    {
    //        //        return firstPair.Value.CompareTo(nextPair.Value);
    //        //    });

    //        //    int target = -1;
    //        //    int order = -1;
    //        //    if (heap.topK[0].Value < eps)
    //        //    {
    //        //        order = heap.topK[0].Key;
    //        //        target = Index[order];
    //        //    }

    //        //    float[] weis = heap.topK.Select(i => 1.0f / (i.Value + 0.0001f)).ToArray();
    //        //    float w_sum = weis.Sum();

    //        //    float Q = 0;
    //        //    float H = 0;
    //        //    for (int i = 0; i < weis.Length; i++)
    //        //    {
    //        //        int idx = Index[heap.topK[i].Key];
    //        //        Q += V[idx] * weis[i] / w_sum;
    //        //        H += Count[idx] * weis[i] / w_sum;
    //        //    }
    //        //    return new Tuple<float, float, int, int>(Q, H, target, order);
    //        //}

    //        //void Refresh(int target)
    //        //{
    //        //List<int> x = new List<int>();
    //        //x.RemoveAt();
    //        //x.Add();
    //        //}

    //        public void Push(float[] q, int target, int order, float v)
    //        {
    //            //take the idx. 
    //            if (target != -1)
    //            {
    //                Array.Copy(q, 0, Buffer, target * Dim, Dim);
    //                V[target] = v;
    //                Count[target] += 1;

    //                Mask.MemPtr.
    //                Index.RemoveAt(order);
    //                Index.Insert(Num, target);
    //            }
    //            else
    //            {
    //                int idx = Index[Num];

    //                Array.Copy(q, 0, Buffer, idx * Dim, Dim);
    //                V[idx] = v;
    //                Count[idx] = 1;
    //                Num = (Num + 1);
    //                if (Num == BufferSize)
    //                {
    //                    int old_idx = Index[0];
    //                    Index.RemoveAt(0);
    //                    Index.Insert(Num - 1, old_idx);
    //                    Num = BufferSize - 1;
    //                }
    //            }
    //        }

    //    }

    //    public class ECMemoryUpdateRunner : StructRunner
    //    {
    //        public float[] QValue;
    //        public float[] QCount;
    //        public int[] QTarget;

    //        HiddenBatchData H = null;
    //        HiddenBatchData Q = null;
    //        ECMemory[] Memories;
    //        int K { get; set; }
    //        float Eps = 0.00001f;
    //        float Delta = 0.0001f;
    //        CudaPieceInt BestIdx;
    //        CudaPieceFloat BestValue;

    //        public ECMemoryUpdateRunner(HiddenBatchData query, int[] action, float[] reward, bool[] isTerminate, int k, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            K = k;
    //            Memories = memories;
    //            Q = query;
    //            H = new HiddenBatchData(Q.MAX_BATCHSIZE, Memories[0].BufferSize, behavior.RunMode, behavior.Device);

    //            BestIdx = new CudaPieceInt(Q.MAX_BATCHSIZE * K, behavior.Device);
    //            BestValue = new CudaPieceFloat(Q.MAX_BATCHSIZE * K, behavior.Device);

    //            QValue = new float[Q.MAX_BATCHSIZE * Memories.Length];
    //            QCount = new float[Q.MAX_BATCHSIZE * Memories.Length];
    //            QTarget = new int[Q.MAX_BATCHSIZE * Memories.Length];
    //        }

    //        public override void Forward()
    //        {
    //            for (int i = 0; i < Memories.Length; i++)
    //            {
    //                int batchSize = Q.BatchSize;
    //                int tk = Math.Min(K, Memories[i].Num);

    //                if (Memories[i].Num > 0)
    //                {
    //                    ComputeLib.Sgemm(Q.Output.Data, 0, Memories[i].Buffer, 0, H.Output.Data, 0,
    //                          Q.BatchSize, Q.Dim, Memories[i].Num, 0, 1, false, true);

    //                    ComputeLib.KLargestValueBatch(H.Output.Data, Q.BatchSize, Memories[i].Num, tk, 1, BestValue, BestIdx);

    //                    BestValue.SyncToCPU(tk * batchSize);
    //                    BestIdx.SyncToCPU(tk * batchSize);
    //                }

    //                for (int b = 0; b < batchSize; b++)
    //                {
    //                    if (tk > 0)
    //                    {
    //                        float[] vs = new float[tk];
    //                        int[] idx = new int[tk];
    //                        Array.Copy(BestValue.MemPtr, b * tk, vs, 0, tk);
    //                        Array.Copy(BestIdx.MemPtr, b * tk, idx, 0, tk);
    //                        MinMaxHeap<int> heap = new MinMaxHeap<int>(1, vs, idx, tk);

    //                        KeyValuePair<int, float>[] idxs = new KeyValuePair<int, float>[tk];
    //                        for (int m = 0; m < tk; m++)
    //                        {
    //                            idxs[tk - 1 - m] = heap.PopTop();
    //                        }

    //                        int target = -1;
    //                        if (idxs[0].Value < Eps)
    //                        {
    //                            target = idxs[0].Key;
    //                        }

    //                        float[] weis = idxs.Select(m => 1.0f / (m.Value + Delta)).ToArray();
    //                        float w_sum = weis.Sum();

    //                        float q = 0;
    //                        float c = 0;
    //                        for (int m = 0; m < weis.Length; m++)
    //                        {
    //                            int k = idxs[m].Key;
    //                            q += Memories[i].V[k] * weis[m] / w_sum;

    //                            float dis = (float)Math.Exp((Memories[i].Time[k] - Memories[i].MemoryTime) / 100.0f);
    //                            c += dis * Memories[i].Count[k] * weis[i] / w_sum;
    //                        }
    //                        QValue[b * Memories.Length + i] = q;
    //                        QCount[b * Memories.Length + i] = c;
    //                        QTarget[b * Memories.Length + i] = target;
    //                    }
    //                    else
    //                    {
    //                        QValue[b * Memories.Length + i] = 0;
    //                        QCount[b * Memories.Length + i] = 0;
    //                        QTarget[b * Memories.Length + i] = -1;
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    public class ECMemoryQueryRunner : StructRunner
    //    {
    //        public float[] QValue;
    //        public float[] QCount;
    //        public int[] QTarget;

    //        HiddenBatchData H = null;
    //        HiddenBatchData Q = null;
    //        ECMemory[] Memories;
    //        int K { get; set; }
    //        float Eps = 0.00001f;
    //        float Delta = 0.0001f;
    //        CudaPieceInt BestIdx;
    //        CudaPieceFloat BestValue;
    //        public ECMemoryQueryRunner(HiddenBatchData query, ECMemory[] memories, int k, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            K = k;
    //            Memories = memories;
    //            Q = query;

    //            H = new HiddenBatchData(Q.MAX_BATCHSIZE, Memories[0].BufferSize, behavior.RunMode, behavior.Device);

    //            BestIdx = new CudaPieceInt(Q.MAX_BATCHSIZE * K, behavior.Device);
    //            BestValue = new CudaPieceFloat(Q.MAX_BATCHSIZE * K, behavior.Device);

    //            QValue = new float[Q.MAX_BATCHSIZE * Memories.Length];
    //            QCount = new float[Q.MAX_BATCHSIZE * Memories.Length];
    //            QTarget = new int[Q.MAX_BATCHSIZE * Memories.Length];
    //        }

    //        public override void Forward()
    //        {
    //            for (int i = 0; i < Memories.Length; i++)
    //            {
    //                int batchSize = Q.BatchSize;
    //                int tk = Math.Min(K, Memories[i].Num);

    //                if (Memories[i].Num > 0)
    //                {
    //                    ComputeLib.Sgemm(Q.Output.Data, 0, Memories[i].Buffer, 0, H.Output.Data, 0,
    //                          Q.BatchSize, Q.Dim, Memories[i].Num, 0, 1, false, true);

    //                    ComputeLib.KLargestValueBatch(H.Output.Data, Q.BatchSize, Memories[i].Num, tk, 1, BestValue, BestIdx);

    //                    BestValue.SyncToCPU(tk * batchSize);
    //                    BestIdx.SyncToCPU(tk * batchSize);
    //                }

    //                for (int b = 0; b < batchSize; b++)
    //                {
    //                    if (tk > 0)
    //                    {
    //                        float[] vs = new float[tk];
    //                        int[] idx = new int[tk];
    //                        Array.Copy(BestValue.MemPtr, b * tk, vs, 0, tk);
    //                        Array.Copy(BestIdx.MemPtr, b * tk, idx, 0, tk);
    //                        MinMaxHeap<int> heap = new MinMaxHeap<int>(1, vs, idx, tk);

    //                        KeyValuePair<int, float>[] idxs = new KeyValuePair<int, float>[tk];
    //                        for (int m = 0; m < tk; m++)
    //                        {
    //                            idxs[tk - 1 - m] = heap.PopTop();
    //                        }

    //                        int target = -1;
    //                        if (idxs[0].Value < Eps)
    //                        {
    //                            target = idxs[0].Key;
    //                        }

    //                        float[] weis = idxs.Select(m => 1.0f / (m.Value + Delta)).ToArray();
    //                        float w_sum = weis.Sum();

    //                        float q = 0;
    //                        float c = 0;
    //                        for (int m = 0; m < weis.Length; m++)
    //                        {
    //                            int k = idxs[m].Key;
    //                            q += Memories[i].V[k] * weis[m] / w_sum;

    //                            float dis = (float)Math.Exp((Memories[i].Time[k] - Memories[i].MemoryTime) / 100.0f);
    //                            c += dis * Memories[i].Count[k] * weis[i] / w_sum;
    //                        }
    //                        QValue[b * Memories.Length + i] = q;
    //                        QCount[b * Memories.Length + i] = c;
    //                        QTarget[b * Memories.Length + i] = target;
    //                    }
    //                    else
    //                    {
    //                        QValue[b * Memories.Length + i] = 0;
    //                        QCount[b * Memories.Length + i] = 0;
    //                        QTarget[b * Memories.Length + i] = -1;
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    public class AtariReplayBatchData : BatchData
    //    {
    //        public ImageDataSource Source;
    //        public ImageDataSource Target;

    //        public DenseBatchData Action;
    //        public DenseBatchData Reward;
    //        public int[] TargetMapping;
    //        public AtariReplayBatchData(int maxBatchSize, int width, int height, int depth, DeviceType device)
    //        {
    //            Source = new ImageDataSource(maxBatchSize, width, height, depth, true, device);
    //            Target = new ImageDataSource(maxBatchSize, width, height, depth, true, device);

    //            TargetMapping = new int[maxBatchSize];
    //            Action = new DenseBatchData(maxBatchSize, 1, DeviceType.CPU);// int[maxBatchSize]; // CudaPieceInt(maxBatchSize, true, false);
    //            Reward = new DenseBatchData(maxBatchSize, 1, DeviceType.CPU);
    //        }

    //    }

    //    public class AtariReplayMemoryRunner : StructRunner
    //    {
    //        static int ReplayMemorySize = BuilderParameters.ReplayBuffer;
    //        static int ReplayColdStart = BuilderParameters.ReplayColdStart;
    //        static int MiniBatchSize = BuilderParameters.ReplayBatchSize;

    //        public class ReplayMemoryCache
    //        {
    //            List<float[]> Screen = new List<float[]>();
    //            int[] Action;
    //            float[] Reward;
    //            bool[] IsTerminal;

    //            int ScreenSize { get; set; }
    //            int History { get; set; }

    //            int LastReplayIndex { get; set; }
    //            int FirstReplayIndex { get; set; }
    //            int TotalFrameNum { get; set; }

    //            RandomUtil random = new RandomUtil(ParameterSetting.Random.Next());

    //            string DumpFile = string.Empty;
    //            BinaryWriter DumpWriter = null;
    //            bool IsDumped { get { return DumpFile != string.Empty; } }

    //            public ReplayMemoryCache(int screenSize, int history) : this(screenSize, history, string.Empty)
    //            { }

    //            public ReplayMemoryCache(int screenSize, int history, string dumpFile)
    //            {
    //                ScreenSize = screenSize;
    //                History = history;
    //                for (int i = 0; i < ReplayMemorySize; i++) { Screen.Add(new float[ScreenSize]); }

    //                Action = new int[ReplayMemorySize];
    //                Reward = new float[ReplayMemorySize];
    //                IsTerminal = new bool[ReplayMemorySize];

    //                LastReplayIndex = 0;
    //                FirstReplayIndex = 0;
    //                TotalFrameNum = 0;

    //                DumpFile = dumpFile;
    //                if (IsDumped)
    //                {
    //                    DumpWriter = new BinaryWriter(new FileStream(dumpFile, FileMode.Create, FileAccess.Write));
    //                    //    DumpWriter.Write(ScreenSize);
    //                }
    //            }

    //            //public bool IsNewEpolisde
    //            //{
    //            //    get
    //            //    {
    //            //        if (LastReplayIndex < 0 || LastReplayIndex >= ReplayMemorySize) { return true; }
    //            //        else return IsTerminal[LastReplayIndex];
    //            //    }
    //            //}

    //            public bool IsColdStarted { get { return TotalFrameNum >= ReplayColdStart; } }

    //            public void PushScreen(float[] memory, int offset, float reward, int action, bool isTerminal)
    //            {
    //                int posIdx = LastReplayIndex % ReplayMemorySize;
    //                Array.Copy(memory, offset, Screen[posIdx], 0, ScreenSize);
    //                Reward[posIdx] = reward;
    //                Action[posIdx] = action;
    //                IsTerminal[posIdx] = isTerminal;

    //                if (IsDumped)
    //                {
    //                    for (int i = 0; i < ScreenSize; i++) DumpWriter.Write((int)(255 * memory[offset + i]));
    //                    DumpWriter.Write(action);
    //                    DumpWriter.Write((int)(255 * reward));
    //                    DumpWriter.Write((int)(isTerminal ? 1 : 0));
    //                }

    //                //if (LastReplayIndex - FirstReplayIndex < History && LastReplayIndex - FirstReplayIndex > 0)
    //                //{
    //                //    FirstReplayIndex = SamplableMemory.Dequeue();
    //                //}
    //                //else if (LastReplayIndex + ReplayMemorySize - FirstReplayIndex < History)
    //                //{
    //                //    FirstReplayIndex = SamplableMemory.Dequeue();
    //                //}
    //                //if (SamplableMemory.Count >= ReplayMemorySize)
    //                //{
    //                //    Console.WriteLine("Memory Count oversize {0}", SamplableMemory.Count);
    //                //}
    //                LastReplayIndex += 1;
    //                if (ReplayMemorySize - (LastReplayIndex - FirstReplayIndex) <= 10)
    //                {
    //                    FirstReplayIndex += 1;
    //                }
    //                TotalFrameNum += 1;
    //            }

    //            //void ScreenCopy(int srcIdx, float[] tgt, int tgtIdx)
    //            //{
    //            //    if(srcIdx + History <= ReplayMemorySize)
    //            //    {
    //            //        Buffer.BlockCopy(Screen, srcIdx * sizeof(float) * ScreenSize,
    //            //            tgt, tgtIdx * sizeof(float) * ScreenSize * History, sizeof(float) * ScreenSize * History);
    //            //    }
    //            //    else
    //            //    {
    //            //        int part1 = ReplayMemorySize - srcIdx;
    //            //        Buffer.BlockCopy(Screen, srcIdx * sizeof(float) * ScreenSize,
    //            //            tgt, tgtIdx * sizeof(float) * ScreenSize * History, sizeof(float) * ScreenSize * part1);

    //            //        int part2 = History - part1;
    //            //        Buffer.BlockCopy(Screen, 0,
    //            //            tgt, (tgtIdx * History + part1) * sizeof(float) * ScreenSize , sizeof(float) * ScreenSize * part2);
    //            //    }
    //            //}

    //            /// <summary>
    //            /// 
    //            /// </summary>
    //            /// <param name="replayIdx"></param>
    //            /// <param name="k">1, 2, 3, 4</param>
    //            /// <param name="tgtbuffer"></param>
    //            /// <param name="offset"></param>
    //            /// <returns></returns>
    //            public int GetPreviousKOb(int replayIdx, int k, float[] tgtbuffer, int offset)
    //            {
    //                if (replayIdx >= LastReplayIndex || replayIdx < FirstReplayIndex) return 0;
    //                int posIdx = replayIdx % ReplayMemorySize;

    //                Array.Copy(Screen[posIdx], 0, tgtbuffer, offset, ScreenSize);
    //                for (int p = 1; p < k; p++)
    //                {
    //                    int nIdx = (replayIdx - p) % ReplayMemorySize;
    //                    if (replayIdx - p < FirstReplayIndex || IsTerminal[nIdx]) return p;
    //                    Array.Copy(Screen[nIdx], 0, tgtbuffer, offset + p * ScreenSize, ScreenSize);
    //                }
    //                return k;
    //            }

    //            public void SampleMiniBatch(int miniBatchSize, AtariReplayBatchData data)
    //            {
    //                int[] randomBatchIdx = Enumerable.Range(FirstReplayIndex, LastReplayIndex - FirstReplayIndex - 10).ToArray();
    //                random.RandomShuffle(randomBatchIdx, miniBatchSize);

    //                Array.Clear(data.Source.Data.MemPtr, 0, miniBatchSize * History * ScreenSize);
    //                Array.Clear(data.Target.Data.MemPtr, 0, miniBatchSize * History * ScreenSize);
    //                //Array.Clear(ReplayBatch.Action.Output.MemPtr, 0, MiniBatchSize * ActDim);
    //                //Array.Clear(ReplayBatch.Reward.Data.MemPtr, 0, MiniBatchSize);

    //                int srcIdx = 0;
    //                int tgtIdx = 0;
    //                foreach (int replayIdx in randomBatchIdx)
    //                {
    //                    GetPreviousKOb(replayIdx, History, data.Source.Data.MemPtr, srcIdx * ScreenSize * History);
    //                    //ReplayMemory.GetPreviousKAct(replayIdx, 1, ReplayBatch.Action.Output.MemPtr, srcIdx * ActDim);
    //                    data.Action.Data.MemPtr[srcIdx] = Action[replayIdx % ReplayMemorySize];
    //                    data.Reward.Data.MemPtr[srcIdx] = Reward[replayIdx % ReplayMemorySize];

    //                    if (IsTerminal[replayIdx % ReplayMemorySize])
    //                    {
    //                        data.TargetMapping[srcIdx] = -1;
    //                    }
    //                    else
    //                    {
    //                        data.TargetMapping[srcIdx] = tgtIdx;
    //                        GetPreviousKOb(replayIdx + 1, History, data.Target.Data.MemPtr, tgtIdx * ScreenSize * History);
    //                        tgtIdx += 1;
    //                    }
    //                    srcIdx += 1;
    //                    if (srcIdx >= miniBatchSize) break;
    //                }

    //                //data.Source.Row = srcIdx;
    //                //data.Target.Row = tgtIdx;
    //                //data.Action.Row = srcIdx;
    //                //data.Reward.BatchSize = srcIdx;

    //                //ReplayBatch.Source.Output.SyncFromCPU(srcIdx * XDim);
    //                //ReplayBatch.Target.Output.SyncFromCPU(tgtIdx * XDim);
    //                //ReplayBatch.Action.Output.SyncFromCPU(srcIdx * ActDim);
    //                //ReplayBatch.Reward.Data.SyncFromCPU(srcIdx);

    //                //int availableReplaySize = SamplableMemory.Count - 1;
    //                //int batchSize = Math.Min(miniBatchSize, availableReplaySize);
    //                //if (batchSize <= 0) return;
    //                //int[] RandomBatchIdx = SamplableMemory.ToArray();
    //                //int srcBatIdx = 0;
    //                //int tgtBatIdx = 0;
    //                //while (srcBatIdx < batchSize)
    //                //{
    //                //    int randomPos = Util.URandom.Next(srcBatIdx, availableReplaySize);
    //                //    int id = RandomBatchIdx[randomPos];
    //                //    RandomBatchIdx[randomPos] = RandomBatchIdx[srcBatIdx];
    //                //    RandomBatchIdx[srcBatIdx] = id;

    //                //    ScreenCopy(id, data.Source.Data.MemPtr, srcBatIdx);

    //                //    if(IsTerminal[id])
    //                //    {
    //                //        data.TargetMapping[srcBatIdx] = -1;
    //                //    }
    //                //    else
    //                //    {
    //                //        int shifId = (id - 1 + ReplayMemorySize) % ReplayMemorySize;
    //                //        ScreenCopy(shifId, data.Target.Data.MemPtr, tgtBatIdx);

    //                //        data.TargetMapping[srcBatIdx] = tgtBatIdx;
    //                //        tgtBatIdx += 1;
    //                //    }

    //                //    data.Reward.Data.MemPtr[srcBatIdx] = Reward[id];
    //                //    data.Action.Data.MemPtr[srcBatIdx] = Action[id];
    //                //    srcBatIdx++;
    //                //}
    //                data.Source.BatchSize = srcIdx;
    //                data.Target.BatchSize = tgtIdx;
    //                data.Reward.BatchSize = srcIdx;
    //                data.Action.BatchSize = srcIdx;

    //                data.Source.Data.SyncFromCPU();
    //                data.Target.Data.SyncFromCPU();
    //                data.Reward.SyncFromCPU();
    //                data.Action.Data.SyncFromCPU();
    //            }
    //        }

    //        public new AtariReplayBatchData Output { get { return (AtariReplayBatchData)base.Output; } protected set { base.Output = value; } }

    //        ImageDataSource InImageInput { get; set; }
    //        int[] InAction { get; set; }
    //        float[] InReward { get; set; }
    //        bool[] InIsTerminal { get; set; }
    //        ReplayMemoryCache ReplayMemory = null;
    //        int ReplayStep = 0;
    //        //unsafe void PushScreen(int offset)
    //        public string DumpPath = BuilderParameters.EnvirPath; //string.Empty; // 
    //        public AtariReplayMemoryRunner(ImageDataSource imageInput, int[] action, float[] reward, bool[] isTerminal, RunnerBehavior behavior)
    //            : base(Structure.Empty, behavior)
    //        {
    //            InImageInput = imageInput;
    //            InAction = action;
    //            InReward = reward;
    //            InIsTerminal = isTerminal;
    //            ReplayMemory = new ReplayMemoryCache(InImageInput.Stat.Width * InImageInput.Stat.Height, InImageInput.Stat.Depth, DumpPath);
    //            Output = new AtariReplayBatchData(MiniBatchSize, InImageInput.Stat.Width, InImageInput.Stat.Height, InImageInput.Stat.Depth, Behavior.Device);
    //        }

    //        public override void Forward()
    //        {
    //            ReplayStep++;
    //            ReplayMemory.PushScreen(InImageInput.Data.MemPtr, 0, InReward[0], InAction[0], InIsTerminal[0]);
    //            if (ReplayMemory.IsColdStarted && ReplayStep % BuilderParameters.UpdateFreq == 0)
    //            {
    //                ReplayMemory.SampleMiniBatch(MiniBatchSize, Output);
    //                IsContinue = true;
    //            }
    //            else
    //            {
    //                IsContinue = false;
    //            }
    //        }
    //    }

    //    public class AtariActionRunner : StructRunner
    //    {
    //        IntPtr AtariEnvPtr { get; set; }

    //        HiddenBatchData ActionInput { get; set; }
    //        float[] QTable;
    //        float[] QCount;

    //        public float[] RewardData;
    //        public int[] ActionOutput;
    //        public bool[] IsTerminal;

    //        ALEAction Action_a = ALEAction.PLAYER_A_NOOP;
    //        ALEAction Action_b = ALEAction.PLAYER_B_NOOP;

    //        int step = 0;
    //        float avgReward = 0;
    //        int epsoid = 1;
    //        float avgQ = 0;

    //        /***Epsilon Schedule**/
    //        float epsilon = 0;

    //        Random mRandom = new Random(13);

    //        int[] DirectStat = new int[BuilderParameters.ACTIONS.Length];

    //        float BestRewardinHistory = 0;
    //        float accumulateReward = 0;
    //        int accumulateStep = 0;
    //        public AtariActionRunner(IntPtr atariEnvPtr, HiddenBatchData dqnScore, float[] qTable, float[] qCount, RunnerBehavior behavior)
    //            : base(Structure.Empty, behavior)
    //        {
    //            AtariEnvPtr = atariEnvPtr;

    //            ActionInput = dqnScore;
    //            QTable = qTable;
    //            QCount = qCount;

    //            RewardData = new float[] { 0 };
    //            ActionOutput = new int[] { 0 };
    //            IsTerminal = new bool[] { false };
    //        }

    //        public override void Forward()
    //        {
    //            epsilon = BuilderParameters.Epsilon(step);

    //            if (BuilderParameters.ValidFreq > 0 && epsoid % BuilderParameters.ValidFreq == 0)
    //            {
    //                epsilon = BuilderParameters.ValidEps;
    //            }

    //            ActionInput.Output.Data.SyncToCPU();
    //            float[] qValues = new float[ActionInput.Dim];
    //            Array.Copy(ActionInput.Output.Data.MemPtr, qValues, ActionInput.Dim);

    //            double mp = mRandom.NextDouble();
    //            if (mp >= epsilon) // || (epsoid % BuilderParameters.TestEpsoid == 0 && accumulateStep <= 1024))
    //            {

    //                for (int i = 0; i < qValues.Length; i++)
    //                {
    //                    qValues[i] = ActionInput.Output.Data.MemPtr[i] / (1 + QCount[i]);
    //                    if (QCount[i] > 0.0001f)
    //                    {
    //                        qValues[i] += QTable[i] / QCount[i];
    //                    }
    //                }
    //                ActionOutput[0] = Util.MaximumValue(qValues);
    //            }
    //            else
    //            {
    //                ActionOutput[0] = mRandom.Next(ActionInput.Dim);
    //            }

    //            Action_a = BuilderParameters.ACTIONS[ActionOutput[0]];
    //            float q = qValues[ActionOutput[0]];

    //            //switch (ActionOutput[0])
    //            //{
    //            //    case 0: Action_a = ALEAction.PLAYER_A_FIRE; break;
    //            //    case 1: Action_a = ALEAction.PLAYER_A_LEFT; break;
    //            //    case 2: Action_a = ALEAction.PLAYER_A_RIGHT; break;
    //            //    //case 3: Action_a = Action.PLAYER_A_FIRE; break;
    //            //}
    //            float reward = ALELib.FIFOController_applyActions(AtariEnvPtr, Action_a, Action_b);
    //            RewardData[0] = reward;


    //            IsTerminal[0] = ALELib.FIFOController_isTerminal(AtariEnvPtr);

    //            if (BuilderParameters.DISPLAY) ALELib.FIFOController_display(AtariEnvPtr);

    //            accumulateReward += reward;
    //            accumulateStep += 1;
    //            if (accumulateReward > BestRewardinHistory)
    //            {
    //                BestRewardinHistory = accumulateReward;
    //            }

    //            if (BuilderParameters.ClipReward)
    //            {
    //                if (RewardData[0] > 0) RewardData[0] = 1;
    //                else if (RewardData[0] < 0) RewardData[0] = -1;
    //            }

    //            /*
    //            if(reward > 0)
    //            {
    //                Console.WriteLine("I have reward {0}", reward);
    //            }
    //            */
    //            if (IsTerminal[0])
    //            {
    //                Logger.WriteLog("I put Terminal True, current reward\t{0}, eps \t {1}", accumulateReward, epsilon);

    //                reward = BuilderParameters.TerminateReward;
    //                RewardData[0] = reward;

    //                //if (epsoid % BuilderParameters.TestEpsoid == 0) { Logger.WriteLog("!!!! without epsilon."); }
    //                //RewardData[0] = 0;
    //                //reward = 0;
    //                accumulateReward = 0;
    //                accumulateStep = 0;
    //                epsoid += 1;

    //                if (BuilderParameters.TestEpsoid > 0 && epsoid >= BuilderParameters.TestEpsoid) { IsTerminate = true; IsContinue = false; }
    //            }

    //            avgReward = step * 1.0f / (step + 1) * avgReward + 1.0f / (step + 1) * reward;
    //            avgQ = step * 1.0f / (step + 1) * avgQ + 1.0f / (step + 1) * q;

    //            /* It is replaced with Epsilon Schedule.
    //            if (step < step_threshold) epsilon = epsilon - (1 - epsilon_min) / (step_threshold + 1);
    //            epsilon = Math.Max(epsilon, epsilon_min);

    //            if (step > step_threshold && step < step_threshold_2)
    //            {
    //                float r = (float) ((step - step_threshold) * 1.0 / (step_threshold_2 - step_threshold));
    //                epsilon = (1 - r) * epsilon_min + r * 0.0001f;
    //            }
    //            if(step > step_threshold_2) epsilon = 0.0001f;
    //            */

    //            step = step + 1;

    //            //ALELib.FIFOController_applyActions(AtariEnvPtr, Action.PLAYER_A_FIRE, Action_b);

    //            DirectStat[ActionOutput[0]] += 1;
    //            if (step % 500 == 0)
    //            {
    //                Logger.WriteLog("Avg Q\t{0}, Avg Reward\t{1}, Step\t{2}, Epsilon\t{3},  Best Reward\t{4}", avgQ, avgReward, step, epsilon, BestRewardinHistory);
    //                Logger.WriteLog("Direction Statistic ::: {0}", string.Join(",", DirectStat));
    //            }
    //        }
    //    }

    //    public class AtariDataRunner : StructRunner
    //    {
    //        //GameDataCashier Interface { get; set; }
    //        public new ImageDataSource Output { get { return (ImageDataSource)base.Output; } protected set { base.Output = value; } }

    //        public static int Width = BuilderParameters.CROP_FRAME[0];
    //        public static int Height = BuilderParameters.CROP_FRAME[1];

    //        public static int CropX1 = BuilderParameters.CROP_FRAME[2];
    //        public static int CropX2 = BuilderParameters.CROP_FRAME[3];

    //        public static int CropY1 = BuilderParameters.CROP_FRAME[4];
    //        public static int CropY2 = BuilderParameters.CROP_FRAME[5];


    //        public static int History = BuilderParameters.HIS_FRAME;

    //        public static int ActWidth { get { return CropX2 - CropX1; } }
    //        public static int ActHeight { get { return CropY2 - CropY1; } }


    //        //ALEAction Action_a = ALEAction.PLAYER_A_NOOP;
    //        //ALEAction Action_b = ALEAction.PLAYER_B_NOOP;

    //        IntPtr AtariEnvPtr { get; set; }
    //        bool IsNewEposide { get; set; }
    //        public AtariDataRunner(IntPtr atariPtr, RunnerBehavior behavior)
    //            : base(Structure.Empty, behavior)
    //        {
    //            //Interface = gameInterface;
    //            Output = new ImageDataSource(1, ActWidth, ActHeight, History, true, Behavior.Device);

    //            AtariEnvPtr = atariPtr;
    //            IsNewEposide = true;
    //            IsBackProp = false;
    //        }

    //        public override void Init()
    //        {
    //            Logger.WriteLog("Atari Game RL Start.");
    //        }

    //        public override void Complete()
    //        {
    //            Logger.WriteLog("Atari Game RL Complete.");
    //        }

    //        unsafe public override void Forward()
    //        {
    //            if ((ALELib.FIFOController_isTerminal(AtariEnvPtr)))
    //            {
    //                Console.WriteLine("Start system again.");
    //                //for (int i = 0; i < 4; i++)
    //                //{

    //                //int resetFrame = ParameterSetting.Random.Next(4, 10);
    //                //for (int i = 0; i < resetFrame; i++)
    //                //{
    //                ALELib.FIFOController_applyActions(AtariEnvPtr, ALEAction.SYSTEM_RESET, ALEAction.PLAYER_B_NOOP);
    //                //}
    //                //ALELib.FIFOController_applyActions(AtariEnvPtr, ALEAction.RESET, ALEAction.SYSTEM_RESET);

    //                //ALELib.FIFOController_applyActions(AtariEnvPtr, ALEAction.PLAYER_A_FIRE, ALEAction.PLAYER_B_NOOP);
    //                //}
    //                //ALELib.FIFOController_applyActions(AtariEnvPtr, ALEAction.PLAYER_A_FIRE, ALEAction.PLAYER_B_NOOP);
    //                //ALELib.FIFOController_applyActions(AtariEnvPtr, ALEAction.PLAYER_A_FIRE, ALEAction.PLAYER_B_NOOP);
    //                //ALELib.FIFOController_applyActions(AtariEnvPtr, ALEAction.PLAYER_A_FIRE, ALEAction.PLAYER_B_NOOP);
    //                //ALELib.FIFOController_applyActions(AtariEnvPtr, ALEAction.PLAYER_A_FIRE, ALEAction.PLAYER_B_NOOP);
    //                Array.Clear(Output.Data.MemPtr, 0, History * ActWidth * ActHeight);
    //            }
    //            else
    //            {
    //                for (int h = History - 2; h >= 0; h--)
    //                {
    //                    Array.Copy(Output.Data.MemPtr, h * ActWidth * ActHeight, Output.Data.MemPtr, (h + 1) * ActWidth * ActHeight, ActWidth * ActHeight);
    //                }
    //            }
    //            //    for (int h = History - 1; h >= 0; h--)
    //            //    {
    //            //        fixed (float* pG = &Output.Data.MemPtr[h * ActWidth * ActHeight])
    //            //        {
    //            //            ALELib.FIFOController_getScreenValueScale(AtariEnvPtr, (IntPtr)pG, Width, Height, CropX1, CropY1, CropX2, CropY2);
    //            //        }
    //            //        Action_a = ALEAction.PLAYER_A_NOOP;
    //            //        ALELib.FIFOController_applyActions(AtariEnvPtr, Action_a, Action_b);
    //            //    }
    //            //    IsNewEposide = false;
    //            //}
    //            //else
    //            //{

    //            fixed (float* pG = &Output.Data.MemPtr[0])
    //            {
    //                ALELib.FIFOController_getScreenValueScaleV2(AtariEnvPtr, (IntPtr)pG, Width, Height, CropX1, CropY1, CropX2, CropY2);
    //            }
    //            //}
    //            Output.BatchSize = 1;
    //            Output.Data.SyncFromCPU();
    //        }
    //    }

    //}
}
