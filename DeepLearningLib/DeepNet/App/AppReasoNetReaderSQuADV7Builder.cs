﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class AppReasoNetReaderSQuADV7Builder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Training Folder"));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Testing Folder"));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Validation Folder"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));

                Argument.Add("IS-EMBED-UPDATE", new ParameterArgument("0", "0 : Fix embedding; 1 : Update Embedding."));
                Argument.Add("EMBED-LAYER-DIM", new ParameterArgument("300,300", "DNN Layer Dim"));
                Argument.Add("EMBED-LAYER-WIN", new ParameterArgument("5,5", "DNN Layer WindowSize"));
                Argument.Add("EMBED-LAYER-AF", new ParameterArgument("1,1", "DNN Layer AF" + ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("EMBED-LAYER-DROPOUT", new ParameterArgument("0,0", "DNN Layer Dropout."));
                Argument.Add("WORD-EMBEDDING", new ParameterArgument(string.Empty, "Word Embedding File"));
                Argument.Add("UNK-INIT", new ParameterArgument("0", "0 : zero initialize; 1 : random initialize"));

                Argument.Add("L3G-EMBED-DIM", new ParameterArgument("0", "L3G Embedding Dim."));
                Argument.Add("L3G-EMBED-WIN", new ParameterArgument("3", "L3G Embedding Win."));

                Argument.Add("CHAR-EMBED-DIM", new ParameterArgument("0", "Char Embedding Dim."));
                Argument.Add("CHAR-EMBED-WIN", new ParameterArgument("5", "Char Embedding Win."));

                Argument.Add("HIGHWAY-LAYER", new ParameterArgument("2", "Highway Layers."));
                Argument.Add("HIGHWAY-SHARE", new ParameterArgument("0", "0: Nonshare highway; 1 : share highway;"));

                Argument.Add("GRU-DIM", new ParameterArgument("300", "GRU Dimension."));
                Argument.Add("CNN-DIM", new ParameterArgument("0", "CNN Dimension."));
                Argument.Add("CNN-WIN", new ParameterArgument("5", "CNN Window Size."));
                Argument.Add("IS-SHARE-RNN", new ParameterArgument("0", "0 : non-share; 1 : share"));
                Argument.Add("MEMORY-ENSEMBLE", new ParameterArgument("2", "2 : two-way ensemble; 3 : three-way ensemble;"));

                //public static float Soft1Dropout { get { return int.Parse(Argument["SOFT1-DROPOUT"].Value); } }
                //public static float Soft2Dropout { get { return int.Parse(Argument["SOFT2-DROPOUT"].Value); } }
                Argument.Add("SOFT1-DROPOUT", new ParameterArgument("0", "Soft 1 Dropout;"));
                Argument.Add("SOFT2-DROPOUT", new ParameterArgument("0", "Soft 2 Dropout;"));
                Argument.Add("LSTM-DROPOUT", new ParameterArgument("0", "LSTM Embed Dropout;"));
                Argument.Add("EMBED-DROPOUT", new ParameterArgument("0", "Embeding Dropout;"));
                Argument.Add("COATT-DROPOUT", new ParameterArgument("0", "CoATT Dropout;"));

                //Argument.Add("STARTPOINT-CONFIG", new ParameterArgument("0", "0: old M1; 1: new M1; 2: all new M1;"));
                //Argument.Add("ENDPOINT-CONFIG", new ParameterArgument("0", "0: old M2; 1: new M2;"));
                Argument.Add("MEMORY-CONFIG", new ParameterArgument("0", "0 : M1; 1: G+M1;"));

                //Argument.Add("ATT-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                //Argument.Add("ATT-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));
                //Argument.Add("ATT-MEM", new ParameterArgument("0", "0 : LSTM; 1 : CoAtt LSTM; 2 : LSTM + CoAtt LSTM; 3 : CoAttOAtt LSTM; 4 : LSTM + CoAttOAtt"));

                //Argument.Add("ATT2-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                //Argument.Add("ATT2-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));
                //Argument.Add("ATT2-MEM", new ParameterArgument("0", "0 : LSTM; 1 : CoAtt LSTM; 2 : LSTM + CoAtt LSTM;"));

                Argument.Add("ANS-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ANS-TYPE", new ParameterArgument(((int)CrossSimType.Cosine).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));
                //Argument.Add("ANS-MEM", new ParameterArgument("0", "0 : LSTM; 1 : CoAtt LSTM; 2 : LSTM + CoAtt LSTM; 3 : CoAttOAtt LSTM; 4 : LSTM + CoAttOAtt"));

                Argument.Add("RECURRENT-DIM", new ParameterArgument("300", "Recurrent Dimension"));
                Argument.Add("RECURRENT-STEP", new ParameterArgument("3", "Recurrent Steps"));
                Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));
                Argument.Add("PRED-RL", new ParameterArgument((((int)PredType.RL_MAXITER)).ToString(), "RL Pred Type : " + ParameterUtil.EnumValues(typeof(PredType))));
                Argument.Add("IS-NORM-REWARD", new ParameterArgument("0", "0 : No Norm Reward; 1 : Norm Reward."));

                // Softmax Randomup.
                Argument.Add("ATT-GAMMA", new ParameterArgument("5", " Attention Smooth Parameter."));
                Argument.Add("ANS-GAMMA", new ParameterArgument("8", " Answer Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

                Argument.Add("DEBUG-FILE", new ParameterArgument(string.Empty, "DEBUG File."));
                Argument.Add("R-INIT", new ParameterArgument(((int)RndRecurrentInit.RndNorm).ToString(), "Recurrent Weight Init."));

                Argument.Add("SPAN-LENGTH", new ParameterArgument("0", "Span length"));
                Argument.Add("DEBUG-MODE", new ParameterArgument("0", "0: exact compute Model; 1: atomicAdd Model; "));
            }

            /// <summary>
            /// DNN Run Mode.
            /// </summary>
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string Vocab { get { return Argument["VOCAB"].Value; } }

            #region train, test, validation dataset.
            public static string Train { get { return Argument["TRAIN"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTrainFile { get { return (!Train.Equals(string.Empty)); } }
            public static string TrainContext { get { return string.Format("{0}.para", Train); } }
            public static string TrainQuery { get { return string.Format("{0}.ques", Train); } }
            public static string TrainPos { get { return string.Format("{0}.pos", Train); } }

            public static string TrainContextBin { get { return string.Format("{0}.{1}.bin", TrainContext, MiniBatchSize); } }
            public static string TrainQueryBin { get { return string.Format("{0}.{1}.bin", TrainQuery, MiniBatchSize); } }
            public static string TrainPosBin { get { return string.Format("{0}.{1}.bin", TrainPos, MiniBatchSize); } }

            public static string TestFolder { get { return Argument["TEST"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTestFile { get { return (!TestFolder.Equals(string.Empty)); } }

            //public static string TestContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", TestFolder, VocabSize, MiniBatchSize); } }
            //public static string TestQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", TestFolder, VocabSize, MiniBatchSize); } }
            //public static string TestAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", TestFolder, VocabSize, MiniBatchSize); } }

            public static string Valid { get { return Argument["VALID"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsValidFile { get { return (!Valid.Equals(string.Empty)); } }
            public static string ValidContext { get { return string.Format("{0}.para", Valid); } }
            public static string ValidQuery { get { return string.Format("{0}.ques", Valid); } }
            public static string ValidPos { get { return string.Format("{0}.pos", Valid); } }
            public static string ValidIds { get { return string.Format("{0}.ids", Valid); } }
            public static string ValidSpan { get { return string.Format("{0}.spans", Valid); } }


            public static string ValidContextBin { get { return string.Format("{0}.{1}.bin", ValidContext, MiniBatchSize); } }
            public static string ValidQueryBin { get { return string.Format("{0}.{1}.bin", ValidQuery, MiniBatchSize); } }
            public static string ValidPosBin { get { return string.Format("{0}.{1}.bin", ValidPos, MiniBatchSize); } }

            //public static string ValidIndexData { get { return string.Format("{0}.{1}.idx.tsv", ValidFolder, VocabSize); } }
            //public static string ValidContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            //public static string ValidQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            //public static string ValidAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            #endregion.

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static bool IS_EMBED_UPDATE { get { return int.Parse(Argument["IS-EMBED-UPDATE"].Value) > 0; } }
            public static int[] EMBED_LAYER_DIM { get { return Argument["EMBED-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] EMBED_ACTIVATION { get { return Argument["EMBED-LAYER-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] EMBED_LAYER_WIN { get { return Argument["EMBED-LAYER-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] EMBED_DROPOUT
            {
                get
                {
                    if (Argument["EMBED-LAYER-DROPOUT"].Value.Equals(string.Empty)) return EMBED_LAYER_DIM.Select(i => 0.0f).ToArray();
                    else return Argument["EMBED-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray();
                }
            }
            public static string InitWordEmbedding { get { return Argument["WORD-EMBEDDING"].Value; } }
            public static int UNK_INIT { get { return int.Parse(Argument["UNK-INIT"].Value); } }

            public static int L3G_EMBED_DIM { get { return int.Parse(Argument["L3G-EMBED-DIM"].Value); } }
            public static int L3G_EMBED_WIN { get { return int.Parse(Argument["L3G-EMBED-WIN"].Value); } }

            public static int[] CHAR_EMBED_DIM { get { return Argument["CHAR-EMBED-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] CHAR_EMBED_WIN { get { return Argument["CHAR-EMBED-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int HighWay_Layer { get { return int.Parse(Argument["HIGHWAY-LAYER"].Value); } }
            public static bool HighWay_Share { get { return int.Parse(Argument["HIGHWAY-SHARE"].Value) > 0; } }

            public static int GRU_DIM { get { return int.Parse(Argument["GRU-DIM"].Value); } }
            public static int CNN_DIM { get { return int.Parse(Argument["CNN-DIM"].Value); } }
            public static int CNN_WIN { get { return int.Parse(Argument["CNN-WIN"].Value); } }
            public static bool IS_SHARE_RNN { get { return int.Parse(Argument["IS-SHARE-RNN"].Value) > 0; } }
            public static int MEMORY_ENSEMBLE { get { return int.Parse(Argument["MEMORY-ENSEMBLE"].Value); } }

            public static float Soft1Dropout { get { return float.Parse(Argument["SOFT1-DROPOUT"].Value); } }
            public static float Soft2Dropout { get { return float.Parse(Argument["SOFT2-DROPOUT"].Value); } }
            public static float LSTMDropout { get { return float.Parse(Argument["LSTM-DROPOUT"].Value); } }
            public static float CoAttDropout { get { return float.Parse(Argument["COATT-DROPOUT"].Value); } }
            public static float EMBEDDropout { get { return float.Parse(Argument["EMBED-DROPOUT"].Value); } }

            //public static bool IS_ENTITY_LSTM { get { return int.Parse(Argument["IS-ENTITY-LSTM"].Value) > 0; } }
            public static Recurrent_Unit Recurrent_Gate_Type { get { return (Recurrent_Unit)int.Parse(Argument["RECURRENT-GATE-TYPE"].Value); } }

            //public static int ENDPOINT_CONFIG { get { return int.Parse(Argument["ENDPOINT-CONFIG"].Value); } }
            //public static int STARTPOINT_CONFIG { get { return int.Parse(Argument["STARTPOINT-CONFIG"].Value); } }
            public static int MEMORY_CONFIG { get { return int.Parse(Argument["MEMORY-CONFIG"].Value); } }

            //public static int ATT_HID_DIM { get { return int.Parse(Argument["ATT-HID"].Value); } }
            //public static CrossSimType AttType { get { return (CrossSimType)int.Parse(Argument["ATT-TYPE"].Value); } }
            //public static int ATT_MEM { get { return int.Parse(Argument["ATT-MEM"].Value); } }

            //public static int ATT2_HID_DIM { get { return int.Parse(Argument["ATT2-HID"].Value); } }
            //public static CrossSimType Att2Type { get { return (CrossSimType)int.Parse(Argument["ATT2-TYPE"].Value); } }
            //public static int ATT2_MEM { get { return int.Parse(Argument["ATT2-MEM"].Value); } }

            public static int ANS_HID_DIM { get { return int.Parse(Argument["ANS-HID"].Value); } }
            public static CrossSimType AnsType { get { return (CrossSimType)int.Parse(Argument["ANS-TYPE"].Value); } }
            //public static int ANS_MEM { get { return int.Parse(Argument["ANS-MEM"].Value); } }

            public static int RECURRENT_DIM { get { return int.Parse(Argument["RECURRENT-DIM"].Value); } }
            public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            public static float RL_DISCOUNT { get { return float.Parse(Argument["RL-DISCOUNT"].Value); } }
            public static PredType PRED_RL { get { return (PredType)int.Parse(Argument["PRED-RL"].Value); } }
            public static bool IS_NORM_REWARD { get { return int.Parse(Argument["IS-NORM-REWARD"].Value) > 0; } }

            public static float Att_Gamma { get { return float.Parse(Argument["ATT-GAMMA"].Value); } }
            public static float Ans_Gamma { get { return float.Parse(Argument["ANS-GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

            public static string DebugFile { get { return Argument["DEBUG-FILE"].Value; } }
            public static RndRecurrentInit RndInit { get { return (RndRecurrentInit)int.Parse(Argument["R-INIT"].Value); } }

            public static int SPAN_LENGTH { get { return int.Parse(Argument["SPAN-LENGTH"].Value); } }
            public static int DEBUG_MODE { get { return int.Parse(Argument["DEBUG-MODE"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.APP_REASONET_SQUAD_READER_V7; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }
        enum PredType { RL_MAXITER, RL_AVGSIM, RL_AVGPROB }

        /// <summary>
        /// Memory Retrieval Runner.
        /// </summary>
        class MemoryRetrievalRunner : StructRunner
        {
            HiddenBatchData Address { get; set; }
            public HiddenBatchData SoftmaxAddress { get; set; }

            SeqDenseBatchData Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }
            float Gamma { get; set; }
            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
            CudaPieceInt Tgt2Src;
            CudaPieceFloat Tgt2SrcSoftmax;

            //CudaPieceFloat tmpMem;
            //CudaPieceFloat tmpMem2;
            public MemoryRetrievalRunner(HiddenBatchData address, SeqDenseBatchData memory, BiMatchBatchData matchData, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Address = address;
                Memory = memory;
                MatchData = matchData;
                Gamma = gamma;
                SoftmaxAddress = new HiddenBatchData(Address.MAX_BATCHSIZE, Address.Dim, Behavior.RunMode, Behavior.Device);
                Output = new HiddenBatchData(MatchData.Stat.MAX_SRC_BATCHSIZE, Memory.Dim, Behavior.RunMode, Behavior.Device);
                Tgt2Src = new CudaPieceInt(MatchData.Stat.MAX_MATCH_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                Tgt2SrcSoftmax = new CudaPieceFloat(MatchData.Stat.MAX_MATCH_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);


                //tmpMem = new CudaPieceFloat(Memory.SentDeriv.Size, true, true);
                //tmpMem2 = new CudaPieceFloat(Memory.SentDeriv.Size, true, true);
            }

            public override void Forward()
            {
                Output.BatchSize = MatchData.SrcSize;

                // softmax 
                // softmax of attention
                ComputeLib.SparseSoftmax(MatchData.Src2MatchIdx, Address.Output.Data, SoftmaxAddress.Output.Data, Gamma, MatchData.SrcSize);

                ComputeLib.ColumnWiseSumMask(Memory.SentOutput, 0, MatchData.TgtIdx, 0,
                    SoftmaxAddress.Output.Data, MatchData.Src2MatchIdx, MatchData.SrcSize,
                    Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                    Memory.SentSize, Memory.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {

                //tmpMem.CopyFrom(Memory.SentDeriv);
                //tmpMem.Zero();


                // outputDeriv -> memoryContentDeriv.

                //tmpMem2.CopyFrom(Memory.SentDeriv);
                if (BuilderParameters.DEBUG_MODE == 0)
                {
                    SoftmaxAddress.Output.Data.SyncToCPU(MatchData.MatchSize);
                    for (int i = 0; i < MatchData.MatchSize; i++)
                    {
                        int matchIdx = MatchData.Tgt2MatchElement.MemPtr[i];
                        Tgt2Src.MemPtr[i] = MatchData.SrcIdx.MemPtr[matchIdx];
                        Tgt2SrcSoftmax.MemPtr[i] = SoftmaxAddress.Output.Data.MemPtr[matchIdx];
                    }
                    Tgt2Src.SyncFromCPU(MatchData.MatchSize);
                    Tgt2SrcSoftmax.SyncFromCPU(MatchData.MatchSize);
                    ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, Tgt2Src, 0, Tgt2SrcSoftmax, // SoftmaxAddress.Output.Data,
                                                 MatchData.Tgt2MatchIdx, MatchData.TgtSize, Memory.SentDeriv /*Memory.SentDeriv*/, 0, CudaPieceInt.Empty, 0,
                                                 MatchData.MatchSize, Memory.Dim, 1, 1);
                    //ComputeLib.Matrix_Add(Memory.SentDeriv, tmpMem, Memory.SentSize, Memory.Dim, 1);
                }
                else if (BuilderParameters.DEBUG_MODE == 1)
                {
                    //tmpMem2.Zero();
                    ComputeLib.AccurateScale_Matrix(Output.Deriv.Data, 0, MatchData.SrcIdx, 0,
                                                Memory.SentDeriv, 0, MatchData.TgtIdx, 0,
                                                Memory.Dim, MatchData.MatchSize, SoftmaxAddress.Output.Data);
                    //ComputeLib.Matrix_Add(Memory.SentDeriv, tmpMem2, Memory.SentSize, Memory.Dim, 1);
                }

                //tmpMem.SyncToCPU(Memory.SentSize * Memory.Dim);
                //tmpMem2.SyncToCPU(Memory.SentSize * Memory.Dim);
                //for (int i = 0; i < Memory.SentSize * Memory.Dim; i++)
                //{
                //    if (Math.Abs(tmpMem2.MemPtr[i] - tmpMem.MemPtr[i]) >= 0.000000001)
                //    {
                //        Console.WriteLine("Adopt Debug Model {3},  Error! {0}, {1}, {2}", tmpMem.MemPtr[i], tmpMem2.MemPtr[i], i, BuilderParameters.DEBUG_MODE);
                //    }
                //}
                //ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, )

                // outputDeriv -> AddressDeriv.
                ComputeLib.Inner_Product_Matching(Output.Deriv.Data, 0, Memory.SentOutput, 0, SoftmaxAddress.Deriv.Data, 0,
                    MatchData.SrcIdx, MatchData.TgtIdx, Output.BatchSize, Memory.SentSize, MatchData.MatchSize,
                    Memory.Dim, Util.GPUEpsilon);

                ComputeLib.DerivSparseMultiClassSoftmax(MatchData.Src2MatchIdx,
                    SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, Gamma, MatchData.SrcSize);

                ComputeLib.Add_Vector(Address.Deriv.Data, SoftmaxAddress.Deriv.Data, MatchData.MatchSize, 1, 1);
            }
        }

        /// <summary>
        /// GRU Query Runner.
        /// </summary>
        public class GRUQueryRunner : StructRunner<GRUCell, HiddenBatchData>
        {
            public HiddenBatchData Query { get; set; }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            HiddenBatchData GateR; //reset gate
            HiddenBatchData GateZ; //update gate
            HiddenBatchData HHat; //new memory
            HiddenBatchData RestH; //new memory

            CudaPieceFloat Ones;
            CudaPieceFloat Tmp;
            public GRUQueryRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
            {
                Query = query;

                Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

                GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

                Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
                Ones.Init(1);

                Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
            }

            public override void Forward()
            {
                //SubQuery.BatchSize = 1;
                /*Wr X -> GateR*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateR*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Query.BatchSize, Model.HiddenStateDim);

                /*Wz X -> GateZ*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateZ*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Query.BatchSize, Model.HiddenStateDim);

                /*Wh X -> HHat*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Query.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Query.BatchSize, Model.HiddenStateDim);

                Output.BatchSize = Query.BatchSize;

                //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
                // tmp = (1 - GateZ(t))
                ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
                                               GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

                // NewQuery = GateZ(t) * HHat
                ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);

                // NewQuery += tmp * h_t-1
                ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Query.BatchSize, Model.HiddenStateDim, 1);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
                ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Query.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

                ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);

                ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
                ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);

                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.BeforeGradient();
                    Model.WrMatrixOptimizer.BeforeGradient();
                    Model.WzMatrixOptimizer.BeforeGradient();

                    Model.UhMatrixOptimizer.BeforeGradient();
                    Model.UrMatrixOptimizer.BeforeGradient();
                    Model.UzMatrixOptimizer.BeforeGradient();

                    Model.BhMatrixOptimizer.BeforeGradient();
                    Model.BrMatrixOptimizer.BeforeGradient();
                    Model.BzMatrixOptimizer.BeforeGradient();

                }
                ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);

                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.AfterGradient();
                    Model.WrMatrixOptimizer.AfterGradient();
                    Model.WzMatrixOptimizer.AfterGradient();

                    Model.UhMatrixOptimizer.AfterGradient();
                    Model.UrMatrixOptimizer.AfterGradient();
                    Model.UzMatrixOptimizer.AfterGradient();

                    Model.BhMatrixOptimizer.AfterGradient();
                    Model.BrMatrixOptimizer.AfterGradient();
                    Model.BzMatrixOptimizer.AfterGradient();
                }
            }
        }

        /// <summary>
        /// Fix Iteration, SoftAttention, Supervised Training.
        /// </summary>
        class ReasonNetRunner : StructRunner
        {
            /// <summary>
            /// Input Query;
            /// </summary>
            LayerStructure InitStates { get; set; }
            

            SeqDenseBatchData Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }
            
            CrossSimType AnsAttType { get; set; }
            MLPAttentionStructure AnsStartStruct = null;
            MLPAttentionStructure AnsEndStruct = null;
            GRUCell GruCell = null;
            LayerStructure TermStruct = null;
            
            string DebugFile { get; set; }
            
            /// <summary>
            /// maximum iteration number.
            /// </summary>
            int MaxIterationNum { get; set; }
            float AttGamma { get; set; }
            float AnsGamma { get; set; }
            SeqDenseBatchData AnsStartHidden = null;
            SeqDenseBatchData AnsEndHidden = null;
            HiddenBatchData InitQuery = null;

            public HiddenBatchData[] AnsStartData = null;
            public HiddenBatchData[] AnsEndData = null;
            HiddenBatchData[] TerminalData = null;

            /// <summary>
            /// tree probability.
            /// </summary>
            public HiddenBatchData[] AnswerProb = null;

            /// <summary>
            /// predict final start Ans.
            /// </summary>
            public HiddenBatchData FinalStartAns = null;

            /// <summary>
            /// predict final end Ans.
            /// </summary>
            public HiddenBatchData FinalEndAns = null;

            
            List<StructRunner> SubRunners = new List<StructRunner>();

            public ReasonNetRunner(LayerStructure initStates, int maxIterateNum,
                SeqDenseBatchData mem, BiMatchBatchData matchData,
                MLPAttentionStructure ansStartStruct, MLPAttentionStructure ansEndStruct, CrossSimType ansAttType,
                GRUCell gruCell, LayerStructure termStruct, float attgamma, float ansgamma, RunnerBehavior behavior, string debugFile = "") : base(Structure.Empty, behavior)
            {
                InitStates = initStates;
                MaxIterationNum = maxIterateNum;

                Memory = mem;
                MatchData = matchData;

                AnsAttType = ansAttType;
                AnsStartStruct = ansStartStruct;
                AnsEndStruct = ansEndStruct;
                
                GruCell = gruCell;
                TermStruct = termStruct;

                AttGamma = attgamma;
                AnsGamma = ansgamma;
                DebugFile = debugFile;

                if (AnsAttType == CrossSimType.Addition || AnsAttType == CrossSimType.Product || AnsAttType == CrossSimType.SubCosine)
                {
                    AnsStartHidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = Memory.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = Memory.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = AnsStartStruct.HiddenDim
                    }, Memory.SampleIdx, Memory.SentMargin, Behavior.Device);

                    AnsEndHidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = Memory.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = Memory.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = AnsEndStruct.HiddenDim
                    }, Memory.SampleIdx, Memory.SentMargin, Behavior.Device);
                }
                //else if (BuilderParameters.AnsType == CrossSimType.Cosine)
                //{
                //    SoftCosineSimRunner initAnsStartRunner = new SoftCosineSimRunner(InitStatus, AnsMem, AnsMatchData, Behavior);
                //    AnsStartRunner.Add(initAnsStartRunner);

                //    SoftCosineSimRunner initAnsEndRunner = new SoftCosineSimRunner(InitStatus, AnsMem, AnsMatchData, Behavior);
                //    AnsEndRunner.Add(initAnsEndRunner);
                //}

                // h
                InitQuery = new HiddenBatchData(Memory.Stat.MAX_BATCHSIZE, GruCell.HiddenStateDim, Behavior.RunMode, Behavior.Device);

                AnsStartData = new HiddenBatchData[MaxIterationNum];
                AnsEndData = new HiddenBatchData[MaxIterationNum];
                TerminalData = new HiddenBatchData[MaxIterationNum];

                //pre-question:
                //      InitQuery
                //      AnsStartHidden
                //      AnsEndHidden
                HiddenBatchData query = InitQuery;
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    SoftLinearSimRunner startPointRunner = new SoftLinearSimRunner(query, AnsStartHidden, MatchData, AnsAttType, AnsStartStruct, Behavior);
                    SubRunners.Add(startPointRunner);
                    // attention score.
                    HiddenBatchData startAtt = startPointRunner.Output;
                    AnsStartData[i] = startAtt;

                    MemoryRetrievalRunner startRetrieveRunner = new MemoryRetrievalRunner(startAtt, Memory, MatchData, AttGamma, Behavior);
                    SubRunners.Add(startRetrieveRunner);
                    // h
                    HiddenBatchData startVec = startRetrieveRunner.Output;

                    EnsembleMatrixRunner queryEnsembleRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { query, startVec }, Behavior);
                    SubRunners.Add(queryEnsembleRunner);
                    // 2 h
                    HiddenBatchData endQuery = queryEnsembleRunner.Output;

                    SoftLinearSimRunner endPointRunner = new SoftLinearSimRunner(endQuery, AnsEndHidden, MatchData, AnsAttType, AnsEndStruct, Behavior);
                    SubRunners.Add(endPointRunner);
                    // attention score.
                    HiddenBatchData endAtt = endPointRunner.Output;
                    AnsEndData[i] = endAtt;

                    MemoryRetrievalRunner endRetrieveRunner = new MemoryRetrievalRunner(endAtt, Memory, MatchData, AttGamma, Behavior);
                    SubRunners.Add(endRetrieveRunner);
                    // h
                    HiddenBatchData endVec = startRetrieveRunner.Output;

                    EnsembleMatrixRunner inputEnsembleRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { startVec, endVec }, Behavior);
                    SubRunners.Add(inputEnsembleRunner);
                    // 2 h
                    HiddenBatchData inputVec = inputEnsembleRunner.Output;

                    EnsembleMatrixRunner stateEnsembleRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { query, inputVec }, Behavior);
                    SubRunners.Add(stateEnsembleRunner);
                    // 3 h
                    HiddenBatchData state = stateEnsembleRunner.Output;

                    FullyConnectHiddenRunner<HiddenBatchData> terminalRunner = new FullyConnectHiddenRunner<HiddenBatchData>(termStruct, state, Behavior);
                    SubRunners.Add(terminalRunner);
                    HiddenBatchData terminalScore = terminalRunner.Output;
                    TerminalData[i] = terminalScore;

                    if (i < MaxIterationNum - 1)
                    {
                        GRUQueryRunner queryRunner = new GRUQueryRunner(gruCell, query, inputVec, Behavior);
                        SubRunners.Add(queryRunner);
                        query = queryRunner.Output;
                    }
                }

                {
                    AnswerProb = new HiddenBatchData[MaxIterationNum];
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i] = new HiddenBatchData(Memory.Stat.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                    }
                    FinalStartAns = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                    FinalEndAns = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                }
            }

            /// <summary>
            /// Output Debug File.
            /// </summary>
            public override void Init()
            {
                if (DebugFile != "" && !DebugFile.Equals(string.Empty))
                {
                    //DebugWriter = new StreamWriter(DebugFile + DateTime.Now.ToFileTime().ToString() + "." + DebugIter.ToString() + ".iter");
                    //DebugSample = 0;
                    //DebugIter += 1;
                }
            }

            /// <summary>
            /// Save Debug File.
            /// </summary>
            public override void Complete()
            {
                //if (DebugWriter != null)
                //{
                //    DebugWriter.Close();
                //}
            }

            public override void Forward()
            {
                if (AnsAttType == CrossSimType.Addition || AnsAttType == CrossSimType.Product || AnsAttType == CrossSimType.SubCosine)
                {
                    AnsStartHidden.BatchSize = Memory.BatchSize;
                    AnsStartHidden.SentSize = Memory.SentSize;
                    ComputeLib.Sgemm(Memory.SentOutput, 0, AnsStartStruct.Wm, 0, AnsStartHidden.SentOutput, 0,
                                     Memory.SentSize, Memory.Dim, AnsStartStruct.HiddenDim, 0, 1, false, false);
                    if (AnsStartStruct.IsBias) ComputeLib.Matrix_Add_Linear(AnsStartHidden.SentOutput, AnsStartStruct.B, Memory.SentSize, AnsStartStruct.HiddenDim);

                    AnsEndHidden.BatchSize = Memory.BatchSize;
                    AnsEndHidden.SentSize = Memory.SentSize;
                    ComputeLib.Sgemm(Memory.SentOutput, 0, AnsEndStruct.Wm, 0, AnsEndHidden.SentOutput, 0,
                                     Memory.SentSize, Memory.Dim, AnsEndStruct.HiddenDim, 0, 1, false, false);
                    if (AnsEndStruct.IsBias) ComputeLib.Matrix_Add_Linear(AnsEndHidden.SentOutput, AnsEndStruct.B, Memory.SentSize, AnsEndStruct.HiddenDim);
                }

                InitQuery.BatchSize = Memory.BatchSize;
                ComputeLib.Matrix_AdditionEx(InitStates.weight, 0, 0,
                                             InitStates.weight, 0, 0,
                                             InitQuery.Output.Data, 0, InitQuery.Dim,
                                             InitQuery.Dim, Memory.BatchSize,
                                             1, 0, 0);

                foreach (StructRunner runner in SubRunners) { runner.Forward(); }

                #region RL Pooling.
                //else if (ReasonPool == PoolingType.RL)
                {
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        ComputeLib.Logistic(TerminalData[i].Output.Data, 0, TerminalData[i].Output.Data, 0, TerminalData[i].Output.BatchSize, 1);
                        ComputeLib.ClipVector(TerminalData[i].Output.Data, TerminalData[i].BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                        TerminalData[i].Output.Data.SyncToCPU(TerminalData[i].BatchSize);
                        AnsStartData[i].Output.Data.SyncToCPU(AnsStartData[i].BatchSize);
                        AnsEndData[i].Output.Data.SyncToCPU(AnsEndData[i].BatchSize);
                    }

                    FinalStartAns.BatchSize = AnsStartData.Last().BatchSize;
                    Array.Clear(FinalStartAns.Output.Data.MemPtr, 0, FinalStartAns.BatchSize);

                    FinalEndAns.BatchSize = AnsEndData.Last().BatchSize;
                    Array.Clear(FinalEndAns.Output.Data.MemPtr, 0, FinalEndAns.BatchSize);

                    for (int b = 0; b < MatchData.SrcSize; b++)
                    {
                        int max_iter = 0;
                        double max_p = 0;

                        float acc_log_t = 0;
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float t_i = TerminalData[i].Output.Data.MemPtr[b];
                            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                            float p = acc_log_t + (float)Math.Log(t_i);
                            if (i == MaxIterationNum - 1) p = acc_log_t;

                            AnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
                            acc_log_t += (float)Math.Log(1 - t_i);

                            if (Math.Exp(p) > max_p)
                            {
                                max_p = Math.Exp(p);
                                max_iter = i;
                            }

                            if (BuilderParameters.PRED_RL == PredType.RL_AVGSIM)
                            {
                                int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                                int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];
                                for (int m = matchBgn; m < matchEnd; m++)
                                {
                                    FinalStartAns.Output.Data.MemPtr[m] += (float)Math.Exp(p) * AnsStartData[i].Output.Data.MemPtr[m];
                                    FinalEndAns.Output.Data.MemPtr[m] += (float)Math.Exp(p) * AnsEndData[i].Output.Data.MemPtr[m];
                                }
                            }
                            if (BuilderParameters.PRED_RL == PredType.RL_AVGPROB)
                            {
                                int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                                int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];
                                double expSum_start = 0;
                                double expSum_end = 0;
                                for (int m = matchBgn; m < matchEnd; m++)
                                {
                                    expSum_start += Math.Exp(AnsGamma * AnsStartData[i].Output.Data.MemPtr[m]);
                                    expSum_end += Math.Exp(AnsGamma * AnsEndData[i].Output.Data.MemPtr[m]);
                                }

                                for (int m = matchBgn; m < matchEnd; m++)
                                {
                                    FinalStartAns.Output.Data.MemPtr[m] += (float)(Math.Exp(p) * Math.Exp(AnsGamma * AnsStartData[i].Output.Data.MemPtr[m]) / expSum_start);
                                    FinalEndAns.Output.Data.MemPtr[m] += (float)(Math.Exp(p) * Math.Exp(AnsGamma * AnsEndData[i].Output.Data.MemPtr[m]) / expSum_end);
                                }
                            }
                        }
                        if (BuilderParameters.PRED_RL == PredType.RL_MAXITER)
                        {
                            int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];

                            double expSum_start = 0;
                            double expSum_end = 0;
                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                expSum_start += Math.Exp(AnsGamma * AnsStartData[max_iter].Output.Data.MemPtr[m]);
                                expSum_end += Math.Exp(AnsGamma * AnsEndData[max_iter].Output.Data.MemPtr[m]);
                            }

                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                FinalStartAns.Output.Data.MemPtr[m] = (float)(Math.Exp(AnsGamma * AnsStartData[max_iter].Output.Data.MemPtr[m]) / expSum_start);  //AnsStartData[max_iter].Output.Data.MemPtr[m];
                                FinalEndAns.Output.Data.MemPtr[m] = (float)(Math.Exp(AnsGamma * AnsEndData[max_iter].Output.Data.MemPtr[m]) / expSum_end); // AnsEndData[max_iter].Output.Data.MemPtr[m];
                            }
                        }
                    }
                    FinalStartAns.Output.Data.SyncFromCPU(FinalStartAns.BatchSize);
                    FinalEndAns.Output.Data.SyncFromCPU(FinalEndAns.BatchSize);
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i].BatchSize = MatchData.SrcSize;
                        AnswerProb[i].Output.Data.SyncFromCPU(AnswerProb[i].BatchSize);
                    }
                }
                #endregion.
            }
            public override void CleanDeriv()
            {
                if (AnsAttType == CrossSimType.Addition || AnsAttType == CrossSimType.Product || AnsAttType == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(AnsStartHidden.SentDeriv, AnsStartHidden.SentSize * AnsStartHidden.Dim);
                    ComputeLib.Zero(AnsEndHidden.SentDeriv, AnsEndHidden.SentSize * AnsEndHidden.Dim);
                }
                ComputeLib.Zero(InitQuery.Deriv.Data, InitQuery.BatchSize * InitQuery.Dim);

                foreach (StructRunner runner in SubRunners) { runner.CleanDeriv(); }
            }

            /// <summary>
            /// Stage 1 : Iterative Attention Model (Supervised Training).
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                #region RL Pool.
                //else if (ReasonPool == PoolingType.RL)
                {
                    /// according to loss, assign reward to each terminal node.
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(TerminalData[i].Deriv.Data.MemPtr, 0, TerminalData[i].BatchSize);
                    }

                    for (int b = 0; b < MatchData.SrcSize; b++)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float reward = AnswerProb[i].Deriv.Data.MemPtr[b];

                            if (BuilderParameters.IS_NORM_REWARD) reward = reward / (i + 1);

                            TerminalData[i].Deriv.Data.MemPtr[b] += reward * (1 - TerminalData[i].Output.Data.MemPtr[b]);
                            for (int hp = 0; hp < i; hp++)
                            {
                                TerminalData[hp].Deriv.Data.MemPtr[b] += -(float)Math.Pow(BuilderParameters.RL_DISCOUNT, i - hp) * reward * TerminalData[hp].Output.Data.MemPtr[b];
                            }

                            if (TerminalData[i].Deriv.Data.MemPtr[b] > Math.Abs(1))
                            {
                                Logger.WriteLog("TerminalRunner Deriv is too large {0}, step {1}", TerminalData[i].Deriv.Data.MemPtr[b], i);
                            }
                        }
                    }
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        TerminalData[i].Deriv.Data.SyncFromCPU(TerminalData[i].BatchSize);
                    }
                }
                #endregion.

                foreach (StructRunner runner in Enumerable.Reverse(SubRunners)) { runner.Backward(false); }


                if (AnsAttType == CrossSimType.Addition || AnsAttType == CrossSimType.Product || AnsAttType == CrossSimType.SubCosine)
                {
                    ComputeLib.Sgemm(AnsStartHidden.SentDeriv, 0,
                                 AnsStartStruct.Wm, 0,
                                 Memory.SentDeriv, 0,
                                 Memory.SentSize, AnsStartStruct.HiddenDim, AnsStartStruct.MemoryDim, 1, 1, false, true);

                    ComputeLib.Sgemm(AnsEndHidden.SentDeriv, 0,
                                 AnsEndStruct.Wm, 0,
                                 Memory.SentDeriv, 0,
                                 Memory.SentSize, AnsEndStruct.HiddenDim, AnsEndStruct.MemoryDim, 1, 1, false, true);
                }
            }

            public override void Update()
            {
                if (AnsAttType == CrossSimType.Addition || AnsAttType == CrossSimType.Product || AnsAttType == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AnsStartStruct.WmMatrixOptimizer.BeforeGradient();
                        if (AnsStartStruct.IsBias) AnsStartStruct.BMatrixOptimizer.BeforeGradient();

                        AnsEndStruct.WmMatrixOptimizer.BeforeGradient();
                        if (AnsEndStruct.IsBias) AnsEndStruct.BMatrixOptimizer.BeforeGradient();
                    }

                    if (AnsStartStruct.IsBias) ComputeLib.ColumnWiseSum(AnsStartHidden.SentDeriv, AnsStartStruct.BMatrixOptimizer.Gradient, Memory.SentSize, AnsStartStruct.HiddenDim, AnsStartStruct.BMatrixOptimizer.GradientStep);
                    if (AnsEndStruct.IsBias) ComputeLib.ColumnWiseSum(AnsEndHidden.SentDeriv, AnsEndStruct.BMatrixOptimizer.Gradient, Memory.SentSize, AnsEndStruct.HiddenDim, AnsEndStruct.BMatrixOptimizer.GradientStep);

                    ComputeLib.Sgemm(Memory.SentOutput, 0,
                                     AnsStartHidden.SentDeriv, 0,
                                     AnsStartStruct.WmMatrixOptimizer.Gradient, 0,
                                     Memory.SentSize, AnsStartStruct.MemoryDim, AnsStartStruct.HiddenDim,
                                     1, AnsStartStruct.WmMatrixOptimizer.GradientStep, true, false);

                    ComputeLib.Sgemm(Memory.SentOutput, 0,
                                     AnsEndHidden.SentDeriv, 0,
                                     AnsEndStruct.WmMatrixOptimizer.Gradient, 0,
                                     Memory.SentSize, AnsEndStruct.MemoryDim, AnsEndStruct.HiddenDim,
                                     1, AnsEndStruct.WmMatrixOptimizer.GradientStep, true, false);

                    if (!IsDelegateOptimizer)
                    {
                        AnsStartStruct.WmMatrixOptimizer.AfterGradient();
                        if (AnsStartStruct.IsBias) AnsStartStruct.BMatrixOptimizer.AfterGradient();

                        AnsEndStruct.WmMatrixOptimizer.AfterGradient();
                        if (AnsEndStruct.IsBias) AnsEndStruct.BMatrixOptimizer.AfterGradient();
                    }
                }

                if (!IsDelegateOptimizer) { InitStates.WeightOptimizer.BeforeGradient(); }
                ComputeLib.ColumnWiseSum(InitQuery.Deriv.Data, InitStates.WeightOptimizer.Gradient, InitQuery.BatchSize, InitQuery.Dim, InitStates.WeightOptimizer.GradientStep);
                if (!IsDelegateOptimizer) { InitStates.WeightOptimizer.AfterGradient(); }

                foreach (StructRunner runner in SubRunners) { runner.Update(); }
            }
        }

        class ExactMatchPredictionRunner : ObjectiveRunner
        {
            HiddenBatchData StartPosData;
            HiddenBatchData EndPosData;
            List<HashSet<string>> PosTrueData;
            BiMatchBatchData AnsMatch;

            float Gamma = 1;
            int SampleNum = 0;
            int HitNum = 0;
            int Iteration = 0;

            //List<Dictionary<string, float>> Result = null;
            //List<List<Dictionary<string, float>>> HisResults = new List<List<Dictionary<string, float>>>();

            public ExactMatchPredictionRunner(
                HiddenBatchData startPosData, HiddenBatchData endPosData, BiMatchBatchData ansMatch,
                List<HashSet<string>> posTrueData,
                RunnerBehavior behavior, string outputFile = "", float gamma = 1) : base(Structure.Empty, behavior)
            {
                StartPosData = startPosData;
                EndPosData = endPosData;
                PosTrueData = posTrueData;
                AnsMatch = ansMatch;

                OutputPath = outputFile;
                Gamma = gamma;
            }
            string OutputPath = "";
            string resultFile = string.Empty;
            StreamWriter OutputWriter = null;

            string dumpFile = string.Empty;
            StreamWriter DumpWriter = null;
            public override void Init()
            {
                SampleNum = 0;
                HitNum = 0;
                if (!OutputPath.Equals(""))
                {
                    resultFile = OutputPath + "." + Iteration.ToString() + ".result";
                    dumpFile = OutputPath + "." + Iteration.ToString() + ".dump";
                    OutputWriter = new StreamWriter(resultFile);
                    DumpWriter = new StreamWriter(dumpFile);
                }
            }

            public override void Complete()
            {
                Logger.WriteLog("Sample {0}, Hit {1}, Accuracy {2}", SampleNum, HitNum, HitNum * 1.0 / SampleNum);
                if (OutputWriter != null)
                {
                    OutputWriter.Close();
                    DumpWriter.Close();

                    //restore.py --span_path dev.nltk.spans --raw_path dev.doc --ids_path dev.ids --fin score.4.result --fout dump.json
                    using (Process callProcess = new Process()
                    {
                        StartInfo = new ProcessStartInfo()
                        {
                            FileName = @"C:\cygwin64\bin\python2.7.exe", // executiveFile,
                            Arguments = string.Format("\"{0}\" --span_path \"{1}\" --raw_path \"{2}\" --ids_path \"{3}\" --fin \"{4}\" --fout {5}",
                            @"\PublicDataSource\SQuADV3\restore.py",
                            BuilderParameters.ValidSpan,
                            @"\PublicDataSource\SQuADV3\dev.doc",
                            @"\PublicDataSource\SQuADV3\dev.ids",
                            resultFile,
                            resultFile + ".json"
                            ),
                            CreateNoWindow = true,
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                        }
                    })
                    {
                        callProcess.Start();
                        callProcess.WaitForExit();
                    }

                    string resultsOutput = "";
                    using (Process callProcess = new Process()
                    {
                        StartInfo = new ProcessStartInfo()
                        {
                            FileName = @"C:\cygwin64\bin\python2.7.exe", // executiveFile,
                            Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\"",
                            @"\PublicDataSource\SQuADV3\evaluate-v1.1.py",
                            @"\PublicDataSource\SQuADV3\dev-v1.1.json",
                            resultFile + ".json"
                            ),
                            CreateNoWindow = true,
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                        }
                    })
                    {
                        callProcess.Start();
                        resultsOutput = callProcess.StandardOutput.ReadToEnd();
                        callProcess.WaitForExit();
                    }

                    Logger.WriteLog("Let see results here {0}", resultsOutput);
                }
                ObjectiveScore = HitNum * 1.0 / SampleNum;
                Iteration += 1;
            }

            public override void Forward()
            {
                ComputeLib.SparseSoftmax(AnsMatch.Src2MatchIdx, StartPosData.Output.Data, StartPosData.Output.Data, Gamma, AnsMatch.SrcSize);
                ComputeLib.SparseSoftmax(AnsMatch.Src2MatchIdx, EndPosData.Output.Data, EndPosData.Output.Data, Gamma, AnsMatch.SrcSize);

                StartPosData.Output.Data.SyncToCPU(StartPosData.BatchSize);
                EndPosData.Output.Data.SyncToCPU(EndPosData.BatchSize);
                AnsMatch.Src2MatchIdx.SyncToCPU(AnsMatch.BatchSize);

                for (int i = 0; i < AnsMatch.SrcSize; i++)
                {
                    Dictionary<string, float> candScoreDict = new Dictionary<string, float>();
                    string trueCandIdx = string.Empty;

                    int candBgn = i == 0 ? 0 : AnsMatch.Src2MatchIdx.MemPtr[i - 1];
                    int candEnd = AnsMatch.Src2MatchIdx.MemPtr[i];

                    List<string> prob = new List<string>();
                    for (int c = candBgn; c < candEnd; c++)
                    {
                        float candScoreStart = StartPosData.Output.Data.MemPtr[c]; // Util.Logistic();
                        float candScoreEnd = EndPosData.Output.Data.MemPtr[c]; // Util.Logistic(EndPosData.Output.Data.MemPtr[c2]);
                        int index = c - candBgn;
                        prob.Add(string.Format("{0}#{1}#{2}", index, candScoreStart, candScoreEnd));
                    }

                    if (DumpWriter != null) { DumpWriter.WriteLine(string.Join(" ", prob)); }

                    float maxP = 0;
                    float sP = 0;
                    float eP = 0;
                    int maxStart = -1;
                    int maxEnd = -1;
                    for (int c = candBgn; c < candEnd; c++)
                    {
                        float candScoreStart = StartPosData.Output.Data.MemPtr[c]; // Util.Logistic();
                        for (int c2 = c; c2 < candEnd; c2++)
                        {
                            float candScoreEnd = EndPosData.Output.Data.MemPtr[c2]; // Util.Logistic(EndPosData.Output.Data.MemPtr[c2]);

                            if (BuilderParameters.SPAN_LENGTH > 0 && c2 - c + 1 > BuilderParameters.SPAN_LENGTH) continue;
                            if (candScoreStart * candScoreEnd > maxP)
                            {
                                maxP = candScoreStart * candScoreEnd;
                                sP = candScoreStart;
                                eP = candScoreEnd;
                                maxStart = c - candBgn;
                                maxEnd = c2 - candBgn;
                            }
                        }
                    }
                    if (OutputWriter != null) { OutputWriter.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", maxStart, maxEnd, sP, eP, maxP); }
                    if (PosTrueData[SampleNum + i].Contains(string.Format("{0}#{1}", maxStart, maxEnd))) { HitNum += 1; }
                }
                SampleNum += AnsMatch.SrcSize;
            }

        }

        class TwoWayEnsembleRunner : StructRunner
        {
            public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
            //CudaPieceInt OutputMask;
            SeqDenseBatchData AData { get; set; }
            SeqDenseBatchData BData { get; set; }
            CudaPieceInt EnsembleMask;

            public TwoWayEnsembleRunner(SeqDenseBatchData aData, SeqDenseBatchData bData, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
            {
                AData = aData;
                BData = bData;

                Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 3, Behavior.Device);

                EnsembleMask = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask.MemPtr[s] = 2 + (s * 3);
                EnsembleMask.SyncFromCPU(AData.MAX_SENTSIZE);
            }

            public override void Forward()
            {
                Output.BatchSize = AData.BatchSize;
                Output.SentSize = AData.SentSize;

                ComputeLib.Matrix_AdditionEx(AData.SentOutput, 0, AData.Dim,
                                             AData.SentOutput, 0, AData.Dim,
                                             Output.SentOutput, 0, Output.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 0);

                ComputeLib.Matrix_AdditionEx(BData.SentOutput, 0, BData.Dim,
                                             BData.SentOutput, 0, BData.Dim,
                                             Output.SentOutput, AData.Dim, Output.Dim,
                                             BData.Dim, BData.SentSize, 1, 0, 0);

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, BData.SentOutput, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, EnsembleMask, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, BData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              AData.SentSize, AData.Dim, 1, 1);

                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, BData.SentOutput, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  BData.SentSize, BData.Dim, 1, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, AData.Dim, Output.Dim,
                                             BData.SentDeriv, 0, BData.Dim,
                                             BData.SentDeriv, 0, BData.Dim,
                                             BData.Dim, BData.SentSize, 1, 0, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, 0, Output.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 1);

            }
        }

        class ThreeWayEnsembleRunner : StructRunner
        {
            public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
            //CudaPieceInt OutputMask;
            SeqDenseBatchData AData { get; set; }
            SeqDenseBatchData BData { get; set; }

            HiddenBatchData CData { get; set; }

            CudaPieceInt EnsembleMask1;
            CudaPieceInt EnsembleMask2;

            CudaPieceFloat tmpOutput;
            public ThreeWayEnsembleRunner(SeqDenseBatchData aData, SeqDenseBatchData bData, HiddenBatchData cData, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
            {
                AData = aData;
                BData = bData;
                CData = cData;

                Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 4, Behavior.Device);
                tmpOutput = new CudaPieceFloat(AData.MAX_SENTSIZE * AData.Dim, true, Behavior.Device == DeviceType.GPU);
                EnsembleMask1 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask1.MemPtr[s] = 2 + (s * 4);
                EnsembleMask1.SyncFromCPU(AData.MAX_SENTSIZE);

                EnsembleMask2 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask2.MemPtr[s] = 3 + (s * 4);
                EnsembleMask2.SyncFromCPU(AData.MAX_SENTSIZE);
            }

            public override void Forward()
            {
                Output.BatchSize = AData.BatchSize;
                Output.SentSize = AData.SentSize;

                ComputeLib.Matrix_AdditionEx(AData.SentOutput, 0, AData.Dim,
                                             AData.SentOutput, 0, AData.Dim,
                                             Output.SentOutput, 0, Output.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 0);

                ComputeLib.Matrix_AdditionEx(BData.SentOutput, 0, BData.Dim,
                                             BData.SentOutput, 0, BData.Dim,
                                             Output.SentOutput, AData.Dim, Output.Dim,
                                             BData.Dim, BData.SentSize, 1, 0, 0);

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, BData.SentOutput, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, EnsembleMask1, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, CData.Output.Data, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, Output.SentMargin, 0, EnsembleMask2, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, CData.Output.Data, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask2, 0, Output.SentMargin, 0, CudaPieceInt.Empty, 0,
                                              Output.SentSize, AData.Dim, 1, 1);


                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, tmpOutput, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  Output.SentSize, AData.Dim, 0, 1);

                ComputeLib.ColumnWiseSumMask(tmpOutput, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
                                            Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
                //ComputeLib.AccurateElementwiseProduct(Output.SentDeriv, 0, AData.SentOutput, 0, CData.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
                //                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, Output.SentMargin, 0,
                //                                  Output.SentSize, AData.Dim, 1);


                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, BData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask1, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              AData.SentSize, AData.Dim, 1, 1);

                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, BData.SentOutput, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask1, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  BData.SentSize, BData.Dim, 1, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, AData.Dim, Output.Dim,
                                             BData.SentDeriv, 0, BData.Dim,
                                             BData.SentDeriv, 0, BData.Dim,
                                             BData.Dim, BData.SentSize, 1, 0, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, 0, Output.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 1);

            }
        }


        class TwoAdvWayEnsembleRunner : StructRunner
        {
            public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
            //CudaPieceInt OutputMask;
            SeqDenseBatchData AData { get; set; }

            HiddenBatchData CData { get; set; }

            CudaPieceInt EnsembleMask1;
            CudaPieceInt EnsembleMask2;

            CudaPieceFloat tmpOutput;
            public TwoAdvWayEnsembleRunner(SeqDenseBatchData aData, HiddenBatchData cData, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
            {
                AData = aData;

                CData = cData;

                Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 2, Behavior.Device);

                tmpOutput = new CudaPieceFloat(AData.MAX_SENTSIZE * AData.Dim, true, Behavior.Device == DeviceType.GPU);

                EnsembleMask1 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask1.MemPtr[s] = 0 + (s * 2);
                EnsembleMask1.SyncFromCPU(AData.MAX_SENTSIZE);

                EnsembleMask2 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask2.MemPtr[s] = 1 + (s * 2);
                EnsembleMask2.SyncFromCPU(AData.MAX_SENTSIZE);
            }

            public override void Forward()
            {
                Output.BatchSize = AData.BatchSize;
                Output.SentSize = AData.SentSize;


                ComputeLib.Matrix_AdditionMask(CData.Output.Data, 0, AData.SentMargin, 0,
                                               CData.Output.Data, 0, AData.SentMargin, 0,
                                               Output.SentOutput, 0, EnsembleMask1, 0,
                                               CData.Dim, AData.SentSize, 1, 0, 0);

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, CData.Output.Data, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, Output.SentMargin, 0, EnsembleMask2, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);

            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, CData.Output.Data, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask2, 0, Output.SentMargin, 0, CudaPieceInt.Empty, 0,
                                              Output.SentSize, AData.Dim, 1, 1);


                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, tmpOutput, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  Output.SentSize, AData.Dim, 0, 1);

                ComputeLib.ColumnWiseSumMask(tmpOutput, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
                                            Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
                //ComputeLib.AccurateElementwiseProduct(Output.SentDeriv, 0, AData.SentOutput, 0, CData.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
                //                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, Output.SentMargin, 0,
                //                                  Output.SentSize, AData.Dim, 1);

                ComputeLib.ColumnWiseSumMask(Output.SentDeriv, 0, EnsembleMask1, 0, CudaPieceFloat.Empty,
                                             Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
            }
        }



        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> context,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> query,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> contextl3g,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> queryl3g,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> contextchar,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> querychar,
                                                             DataCashier<DenseBatchData, DenseDataStat> startingPos,
                                                             DataCashier<DenseBatchData, DenseDataStat> endPos,
                                                             //IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> answer,

                                                             // text embedding cnn.
                                                             LayerStructure EmbedCNN,
                                                             LayerStructure L3GEmbedCNN,
                                                             List<LayerStructure> CharEmbedCNN,

                                                             List<LayerStructure> HighwayGate,
                                                             List<LayerStructure> HighwayConnect,

                                                             // context lstm 
                                                             LSTMStructure ContextD1LSTM, LSTMStructure ContextD2LSTM,

                                                             LayerStructure ContextCNN,

                                                             // query lstm
                                                             LSTMStructure QueryD1LSTM, LSTMStructure QueryD2LSTM,

                                                             LayerStructure QueryCNN,

                                                             LayerStructure CoAttLayer,

                                                             LSTMStructure M1D1LSTM, LSTMStructure M1D2LSTM,
                                                             LSTMStructure M2D1LSTM, LSTMStructure M2D2LSTM,

                                                             LayerStructure InitStateStruct,
                                                             GRUCell GruCell,
                                                             MLPAttentionStructure AnsStartStruct, MLPAttentionStructure AnsEndStruct,
                                                             LayerStructure TermalStruct,

                                                             LayerStructure AnsStartLayer, LayerStructure AnsEndLayer,
                                                             // model.
                                                             CompositeNNStructure model, RunnerBehavior Behavior, string resultFile)
        {
            ComputationGraph cg = new ComputationGraph();

            /************************************************************ Query Passage Candidate data *********/
            SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));
            SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(context, Behavior));
            SeqSparseBatchData Queryl3gData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(queryl3g, Behavior));
            SeqSparseBatchData Contextl3gData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(contextl3g, Behavior));
            SeqSparseBatchData QuerycharData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(querychar, Behavior));
            SeqSparseBatchData ContextcharData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(contextchar, Behavior));

            DenseBatchData StartPos = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(startingPos, Behavior));
            DenseBatchData EndPos = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(endPos, Behavior));

            /************************************************************ Word Embedding on Query data *********/
            SeqDenseBatchData QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN, QueryData, Behavior) { IsBackProp = BuilderParameters.IS_EMBED_UPDATE, IsUpdate = BuilderParameters.IS_EMBED_UPDATE });

            /************************************************************ Word Embedding on Passage data *********/
            SeqDenseBatchData ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN, ContextData, Behavior) { IsBackProp = BuilderParameters.IS_EMBED_UPDATE, IsUpdate = BuilderParameters.IS_EMBED_UPDATE });


            if (L3GEmbedCNN != null)
            {
                SeqDenseBatchData l3gQueryEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
                    L3GEmbedCNN, Queryl3gData, Behavior));

                SeqDenseBatchData l3gContextEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
                    L3GEmbedCNN, Contextl3gData, Behavior));

                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QueryEmbedOutput, l3gQueryEmbed }, Behavior));

                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { ContextEmbedOutput, l3gContextEmbed }, Behavior));
            }

            if (CharEmbedCNN.Count > 0)
            {
                SeqDenseBatchData letterQueryEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
                    CharEmbedCNN[0], QuerycharData, Behavior));
                for (int i = 1; i < CharEmbedCNN.Count; i++)
                {
                    letterQueryEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(
                        CharEmbedCNN[i], letterQueryEmbed, Behavior));
                }
                HiddenBatchData letterQueryMaxpool = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(letterQueryEmbed, Behavior));
                SeqDenseBatchData charQueryEmbed = new SeqDenseBatchData(
                    new SequenceDataStat(QueryEmbedOutput.MAX_BATCHSIZE, QueryEmbedOutput.MAX_SENTSIZE, letterQueryMaxpool.Dim),
                    QueryEmbedOutput.SampleIdx, QueryEmbedOutput.SentMargin,
                    letterQueryMaxpool.Output.Data, letterQueryMaxpool.Deriv.Data, Behavior.Device);
                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QueryEmbedOutput, charQueryEmbed }, Behavior));

                SeqDenseBatchData letterContextEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
                    CharEmbedCNN[0], ContextcharData, Behavior));
                for (int i = 1; i < CharEmbedCNN.Count; i++)
                {
                    letterContextEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(
                        CharEmbedCNN[i], letterContextEmbed, Behavior));
                }
                HiddenBatchData letterContextMaxpool = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(letterContextEmbed, Behavior));
                SeqDenseBatchData charContextEmbed = new SeqDenseBatchData(
                    //ContextEmbedOutput.Stat, 
                    new SequenceDataStat(ContextEmbedOutput.MAX_BATCHSIZE, ContextEmbedOutput.MAX_SENTSIZE, letterContextMaxpool.Dim),
                    ContextEmbedOutput.SampleIdx, ContextEmbedOutput.SentMargin,
                    letterContextMaxpool.Output.Data, letterContextMaxpool.Deriv.Data, Behavior.Device);
                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { ContextEmbedOutput, charContextEmbed }, Behavior));
            }
            /************************************************************ Highway net on Query and Passage Data *********/

            int highWayLayer = BuilderParameters.HighWay_Layer;

            if (highWayLayer > 0)
            {
                HiddenBatchData qtmpembed = new HiddenBatchData(QueryEmbedOutput.MAX_SENTSIZE, QueryEmbedOutput.Dim,
                                                                QueryEmbedOutput.SentOutput, QueryEmbedOutput.SentDeriv, Behavior.Device);

                HiddenBatchData dtmpembed = new HiddenBatchData(ContextEmbedOutput.MAX_SENTSIZE, ContextEmbedOutput.Dim,
                                                                ContextEmbedOutput.SentOutput, ContextEmbedOutput.SentDeriv, Behavior.Device);

                qtmpembed = (HiddenBatchData)cg.AddRunner(new HighwayNetRunner(HighwayConnect, HighwayGate, qtmpembed, Behavior));
                dtmpembed = (HiddenBatchData)cg.AddRunner(new HighwayNetRunner(HighwayConnect, HighwayGate, dtmpembed, Behavior));

                if (BuilderParameters.EMBEDDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
                {
                    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(qtmpembed, BuilderParameters.EMBEDDropout, Behavior, false));
                    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(dtmpembed, BuilderParameters.EMBEDDropout, Behavior, false));
                }

                QueryEmbedOutput = new SeqDenseBatchData(new SequenceDataStat(QueryEmbedOutput.MAX_BATCHSIZE, QueryEmbedOutput.MAX_SENTSIZE, QueryEmbedOutput.Dim),
                                                                    QueryEmbedOutput.SampleIdx, QueryEmbedOutput.SentMargin,
                                                                    qtmpembed.Output.Data, qtmpembed.Deriv.Data, Behavior.Device);

                ContextEmbedOutput = new SeqDenseBatchData(new SequenceDataStat(ContextEmbedOutput.MAX_BATCHSIZE, ContextEmbedOutput.MAX_SENTSIZE, ContextEmbedOutput.Dim),
                                                                    ContextEmbedOutput.SampleIdx, ContextEmbedOutput.SentMargin,
                                                                    dtmpembed.Output.Data, dtmpembed.Deriv.Data, Behavior.Device);
            }


            /************************************************************ LSTM on Query data *********/
            SeqDenseBatchData QuerySeqO = QuerySeqO = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(QueryD1LSTM, QueryD2LSTM, QueryEmbedOutput, Behavior));
            if (QueryCNN != null)
            {
                SeqDenseBatchData QueryCNNEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(QueryCNN, QueryEmbedOutput, Behavior));
                QuerySeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QuerySeqO, QueryCNNEmbed }, Behavior));
            }
            BiMatchBatchData QueryMatch = (BiMatchBatchData)cg.AddRunner(new SeqBiMatchRunner(QuerySeqO, Behavior) { IsUpdate = true });

            /************************************************************ LSTM on Passage data *********/
            SeqDenseBatchData MemorySeqO = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(ContextD1LSTM, ContextD2LSTM, ContextEmbedOutput, Behavior));
            if (ContextCNN != null)
            {
                SeqDenseBatchData ContextCNNEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(ContextCNN, ContextEmbedOutput, Behavior));
                MemorySeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { MemorySeqO, ContextCNNEmbed }, Behavior));
            }
            BiMatchBatchData MemoryMatch = (BiMatchBatchData)cg.AddRunner(new SeqBiMatchRunner(MemorySeqO, Behavior) { IsUpdate = true });


            if (BuilderParameters.CoAttDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(QuerySeqO.MAX_SENTSIZE, QuerySeqO.Dim, QuerySeqO.SentOutput, QuerySeqO.SentDeriv, Behavior.Device),
                    BuilderParameters.CoAttDropout, Behavior, false));

                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(MemorySeqO.MAX_SENTSIZE, MemorySeqO.Dim, MemorySeqO.SentOutput, MemorySeqO.SentDeriv, Behavior.Device),
                    BuilderParameters.CoAttDropout, Behavior, false));
            }

            SoftSeqLinearSimRunner coattRunner = new SoftSeqLinearSimRunner(MemorySeqO, QuerySeqO, DataPanel.MaxCrossQDLen, CoAttLayer, Behavior) { IsUpdate = true };
            BiMatchBatchData d2qMatchData = coattRunner.MatchData;
            HiddenBatchData CoAttL = (HiddenBatchData)cg.AddRunner(coattRunner);

            HiddenBatchData d2qData = (HiddenBatchData)cg.AddRunner(new MemoryRetrievalRunner(CoAttL, QuerySeqO, d2qMatchData, 1, Behavior) { IsUpdate = true });
            SeqDenseBatchData MemorySeqO2 = new SeqDenseBatchData(new SequenceDataStat(MemorySeqO.MAX_BATCHSIZE, MemorySeqO.MAX_SENTSIZE, d2qData.Dim),
                                                                MemorySeqO.SampleIdx, MemorySeqO.SentMargin,
                                                                d2qData.Output.Data, d2qData.Deriv.Data, Behavior.Device);

            SeqDenseBatchData DocG = null;
            if (BuilderParameters.MEMORY_ENSEMBLE == 2)
            {
                DocG = (SeqDenseBatchData)cg.AddRunner(new TwoWayEnsembleRunner(MemorySeqO, MemorySeqO2, Behavior) { IsUpdate = true });
            }
            else if (BuilderParameters.MEMORY_ENSEMBLE == 3)
            {
                HiddenBatchData maxCoeff = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(
                                new SeqDenseBatchData(new SequenceDataStat()
                                {
                                    MAX_BATCHSIZE = d2qMatchData.Stat.MAX_SRC_BATCHSIZE,
                                    MAX_SEQUENCESIZE = d2qMatchData.Stat.MAX_MATCH_BATCHSIZE,
                                    FEATURE_DIM = CoAttL.Dim
                                }, d2qMatchData.Src2MatchIdx, d2qMatchData.SrcIdx,
                                CoAttL.Output.Data, CoAttL.Deriv.Data, Behavior.Device), Behavior));
                HiddenBatchData d2qMaxData = (HiddenBatchData)cg.AddRunner(new MemoryRetrievalRunner(maxCoeff, MemorySeqO, MemoryMatch, 1, Behavior) { IsUpdate = true });
                DocG = (SeqDenseBatchData)cg.AddRunner(new ThreeWayEnsembleRunner(MemorySeqO, MemorySeqO2, d2qMaxData, Behavior) { IsUpdate = true });
            }
            if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(DocG.MAX_SENTSIZE, DocG.Dim, DocG.SentOutput, DocG.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false));
            }

            SeqDenseBatchData DocM1 = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(M1D1LSTM, M1D2LSTM, DocG, Behavior));
            if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(DocM1.MAX_SENTSIZE, DocM1.Dim, DocM1.SentOutput, DocM1.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false));
            }

            // final memory.
            //[p0, g1] = 4d + 2h
            SeqDenseBatchData DocGM1 = null;

            switch(BuilderParameters.MEMORY_CONFIG)
            {
                // 2h
                case 0: DocGM1 = DocM1; break;  

                // 4d + 2h (M1 + G)
                case 1: DocGM1 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocG, DocM1 }, Behavior)); break;
                case 2:
                case 3:
                case 4:
                    SeqDenseBatchData DocM2 = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(M1D1LSTM, M1D2LSTM, DocM1, Behavior));
                    if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
                    {
                        cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(DocM2.MAX_SENTSIZE, DocM2.Dim, DocM2.SentOutput, DocM2.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false));
                    }

                    switch(BuilderParameters.MEMORY_CONFIG)
                    {
                        // 2h
                        case 2: DocGM1 = DocM2; break;
                        // 4d + 2h (M2 + G)
                        case 3: DocGM1 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocG, DocM2 }, Behavior)); break;
                        // 4h
                        case 4: DocGM1 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocM1, DocM2 }, Behavior)); break;
                    }
                    break;
            }

            //HiddenBatchData startInput = new HiddenBatchData(DocGM1.MAX_SENTSIZE, DocGM1.Dim, DocGM1.SentOutput, DocGM1.SentDeriv, Behavior.Device);
            //if (BuilderParameters.Soft1Dropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            //{
            //    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(startInput, BuilderParameters.Soft1Dropout, Behavior, false) { IsUpdate = true });
            //}
            //HiddenBatchData StartP = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStartStruct, startInput, Behavior) { IsUpdate = true });


            //SeqDenseBatchData DocGM2 = null;
            //if (BuilderParameters.ENDPOINT_CONFIG == 0)
            //{
            //    //DocM1 = DocM1;
            //    SeqDenseBatchData DocM2 = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(M2D1LSTM, M2D2LSTM, DocM1, Behavior));
            //    if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            //    {
            //        cg.AddRunner(new DropOutProcessor<HiddenBatchData>(
            //            new HiddenBatchData(DocM2.MAX_SENTSIZE, DocM2.Dim, DocM2.SentOutput, DocM2.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false));
            //    }
            //    DocGM2 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocG, DocM2 }, Behavior) { IsUpdate = true });

            //}
            //else if (BuilderParameters.ENDPOINT_CONFIG == 1)
            //{
            //    // ali = 2h
            //    HiddenBatchData a1i = (HiddenBatchData)cg.AddRunner(new MemoryRetrievalRunner(StartP, DocM1, MemoryMatch, 1, Behavior) { IsUpdate = true });
            //    //[ali, g1 * ali] = 4h
            //    SeqDenseBatchData DocM1_Adv = (SeqDenseBatchData)cg.AddRunner(new TwoAdvWayEnsembleRunner(DocM1, a1i, Behavior) { IsUpdate = true });

            //    //[p0, g1, ali, g1 * ali] = 4d + 6h
            //    SeqDenseBatchData DocM1_New = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocGM1, DocM1_Adv }, Behavior) { IsUpdate = true });

            //    if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            //    {
            //        cg.AddRunner(new DropOutProcessor<HiddenBatchData>(
            //            new HiddenBatchData(DocM1_New.MAX_SENTSIZE, DocM1_New.Dim, DocM1_New.SentOutput, DocM1_New.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false)
            //        { IsUpdate = true });
            //    }

            //    SeqDenseBatchData DocM2 = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(M2D1LSTM, M2D2LSTM, DocM1_New, Behavior));
            //    if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            //    {
            //        cg.AddRunner(new DropOutProcessor<HiddenBatchData>(
            //            new HiddenBatchData(DocM2.MAX_SENTSIZE, DocM2.Dim, DocM2.SentOutput, DocM2.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false));
            //    }
            //    DocGM2 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocG, DocM2 }, Behavior) { IsUpdate = true });
            //}
            //else if (BuilderParameters.ENDPOINT_CONFIG == 2)
            //{
            //    HiddenBatchData a1i = (HiddenBatchData)cg.AddRunner(new MemoryRetrievalRunner(StartP, DocGM1, MemoryMatch, 1, Behavior) { IsUpdate = true });
            //    //[ali, g1 * ali] = 4h
            //    SeqDenseBatchData DocM1_Adv = (SeqDenseBatchData)cg.AddRunner(new TwoAdvWayEnsembleRunner(DocGM1, a1i, Behavior) { IsUpdate = true });

            //    if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            //    {
            //        cg.AddRunner(new DropOutProcessor<HiddenBatchData>(
            //            new HiddenBatchData(DocM1_Adv.MAX_SENTSIZE, DocM1_Adv.Dim, DocM1_Adv.SentOutput, DocM1_Adv.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false)
            //        { IsUpdate = true });
            //    }

            //    SeqDenseBatchData DocM2 = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(M2D1LSTM, M2D2LSTM, DocM1_Adv, Behavior));
            //    if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            //    {
            //        cg.AddRunner(new DropOutProcessor<HiddenBatchData>(
            //            new HiddenBatchData(DocM2.MAX_SENTSIZE, DocM2.Dim, DocM2.SentOutput, DocM2.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false));
            //    }
            //    DocGM2 = DocM2; // (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocG, DocM2 }, Behavior) { IsUpdate = true });
            //}

            //HiddenBatchData endInput = new HiddenBatchData(DocGM2.MAX_SENTSIZE, DocGM2.Dim, DocGM2.SentOutput, DocGM2.SentDeriv, Behavior.Device);
            //if (BuilderParameters.Soft2Dropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            //{
            //    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(endInput, BuilderParameters.Soft2Dropout, Behavior, false) { IsUpdate = true });
            //}
            //HiddenBatchData EndP = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsEndStruct, endInput, Behavior) { IsUpdate = true });

            //ReasonNetRunner reasonRunner = new ReasonNetRunner(InitStateStruct, BuilderParameters.RECURRENT_STEP,
            //    DocGM1, MemoryMatch, AnsStartStruct, AnsEndStruct, BuilderParameters.AnsType, GruCell, TermalStruct, BuilderParameters.Att_Gamma, BuilderParameters.Ans_Gamma, Behavior);
            //cg.AddRunner(reasonRunner);


            HiddenBatchData startInput = new HiddenBatchData(DocGM1.MAX_SENTSIZE, DocGM1.Dim, DocGM1.SentOutput, DocGM1.SentDeriv, Behavior.Device);
            if (BuilderParameters.Soft1Dropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(startInput, BuilderParameters.Soft1Dropout, Behavior, false) { IsUpdate = true });
            }
            HiddenBatchData StartP = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStartLayer, startInput, Behavior) { IsUpdate = true });

            HiddenBatchData a1i = (HiddenBatchData)cg.AddRunner(new MemoryRetrievalRunner(StartP, DocGM1, MemoryMatch, 1, Behavior) { IsUpdate = true });
            //[ali, g1 * ali] = 4h
            SeqDenseBatchData DocM1_Adv = (SeqDenseBatchData)cg.AddRunner(new TwoAdvWayEnsembleRunner(DocGM1, a1i, Behavior) { IsUpdate = true });

            if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(DocM1_Adv.MAX_SENTSIZE, DocM1_Adv.Dim, DocM1_Adv.SentOutput, DocM1_Adv.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false)
                { IsUpdate = true });
            }
            HiddenBatchData endInput = new HiddenBatchData(DocM1_Adv.MAX_SENTSIZE, DocM1_Adv.Dim, DocM1_Adv.SentOutput, DocM1_Adv.SentDeriv, Behavior.Device);
            if (BuilderParameters.Soft2Dropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(endInput, BuilderParameters.Soft2Dropout, Behavior, false) { IsUpdate = true });
            }
            HiddenBatchData EndP = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsEndLayer, endInput, Behavior) { IsUpdate = true });

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new BayesianPairRunner(StartPos.Data, EndPos.Data, StartP, EndP, MemoryMatch, 1, Behavior));
                    //cg.AddObjective(new BayesianContrastivePairRewardRunner(
                    //    StartPos.Data, EndPos.Data, reasonRunner.AnsStartData, reasonRunner.AnsEndData, reasonRunner.AnswerProb, MemoryMatch, BuilderParameters.Ans_Gamma, Behavior));
                    break;
                case DNNRunMode.Predict:
                    //cg.AddRunner(new ExactMatchPredictionRunner(reasonRunner.FinalStartAns, reasonRunner.FinalEndAns, MemoryMatch, DataPanel.ValidResults, Behavior, BuilderParameters.ScoreOutputPath, BuilderParameters.Ans_Gamma));
                    cg.AddRunner(new ExactMatchPredictionRunner(StartP, EndP, MemoryMatch, DataPanel.ValidResults, Behavior, BuilderParameters.ScoreOutputPath));
                    break;
            }

            return cg;
        }

        private LayerStructure InitWordEmbedLayers(DeviceType device)
        {
            LayerStructure wordEmbedding = null;
            wordEmbedding = (new LayerStructure(DataPanel.WordSize, BuilderParameters.EMBED_LAYER_DIM[0],
                    BuilderParameters.EMBED_ACTIVATION[0], N_Type.Convolution_layer, BuilderParameters.EMBED_LAYER_WIN[0],
                    BuilderParameters.EMBED_DROPOUT[0], false, device));

            #region Glove Work Embedding Initialization.

            if (!BuilderParameters.InitWordEmbedding.Equals(string.Empty))
            {
                int wordDim = BuilderParameters.EMBED_LAYER_DIM[0];

                int halfWinStart = BuilderParameters.EMBED_LAYER_WIN[0] / 2; // * DataPanel.WordDim;

                Logger.WriteLog("Init Glove Word Embedding ...");
                int hit = 0;
                using (StreamReader mreader = new StreamReader(BuilderParameters.InitWordEmbedding))
                {
                    int[] HitWords = new int[DataPanel.wordFreqDict.ItemDictSize];
                    while (!mreader.EndOfStream)
                    {
                        string[] items = mreader.ReadLine().Split(' ');
                        string word = items[0];
                        float[] vec = new float[items.Length - 1];
                        for (int i = 1; i < items.Length; i++)
                        {
                            vec[i - 1] = float.Parse(items[i]);
                        }

                        int wordIdx = DataPanel.wordFreqDict.IndexItem(word);

                        if (wordIdx >= 0)
                        {
                            HitWords[wordIdx] = 1;
                            int feaIdx = wordIdx;

                            for (int win = -halfWinStart; win < BuilderParameters.EMBED_LAYER_WIN[0] - halfWinStart; win++)
                            {
                                for (int i = 0; i < wordDim; i++)
                                {
                                    wordEmbedding.weight.MemPtr[((win + halfWinStart) * DataPanel.WordSize + feaIdx) * wordDim + i] = vec[i];
                                }
                            }
                            hit++;
                        }
                        else
                        {
                            //....
                        }
                    }
                    if (BuilderParameters.UNK_INIT == 0)
                    {
                        Logger.WriteLog("UNK is initalized to zero!");
                        for (int u = 0; u < DataPanel.wordFreqDict.ItemDictSize; u++)
                        {
                            if (HitWords[u] == 0)
                            {
                                int unkIdx = u;
                                for (int win = -halfWinStart; win < BuilderParameters.EMBED_LAYER_WIN[0] - halfWinStart; win++)
                                {
                                    for (int i = 0; i < wordDim; i++)
                                    {
                                        wordEmbedding.weight.MemPtr[((win + halfWinStart) * DataPanel.WordSize + unkIdx) * wordDim + i] = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                wordEmbedding.weight.SyncFromCPU();
                Logger.WriteLog("Init Glove Word Embedding Done, {0} words initalized in total {1}.", hit, DataPanel.wordFreqDict.ItemDictSize);
            }
            #endregion.

            return wordEmbedding;
        }
        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");

            CompositeNNStructure modelStructure = new CompositeNNStructure();

            // word embedding layer.
            LayerStructure wordembedLayer = null;
            // l3g embedding layer.
            LayerStructure l3gembedLayer = null;
            // char embedding layer.
            List<LayerStructure> charembedLayer = new List<LayerStructure>();
            List<LayerStructure> highwayConnect = new List<LayerStructure>();
            List<LayerStructure> highwayGate = new List<LayerStructure>();

            LayerStructure CNNembed = null;
            //context lstm layer.
            LSTMStructure contextD1LSTM = null;
            LSTMStructure contextD2LSTM = null;
            //query lstm layer.
            LSTMStructure queryD1LSTM = null;
            LSTMStructure queryD2LSTM = null;

            LayerStructure CoAttLayer = null;

            LSTMStructure M1D1LSTM = null;
            LSTMStructure M1D2LSTM = null;
            LSTMStructure M2D1LSTM = null;
            LSTMStructure M2D2LSTM = null;

            LayerStructure InitStateStruct = null;
            GRUCell GruCell = null;
            MLPAttentionStructure AnsStartStruct = null;
            MLPAttentionStructure AnsEndStruct = null;
            LayerStructure TermalStruct = null;

            LayerStructure AnsStartLayer = null;
            LayerStructure AnsEndLayer = null;

            //LayerStructure AnsStartStruct = null;
            //LayerStructure AnsEndStruct = null;

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                wordembedLayer = InitWordEmbedLayers(device);
                if (BuilderParameters.IS_EMBED_UPDATE) { modelStructure.AddLayer(wordembedLayer); }
                else { wordembedLayer.StructureOptimizer.Clear(); }

                int embedDim = wordembedLayer.Neural_Out;
                int l3gembedDim = BuilderParameters.L3G_EMBED_DIM;
                if (l3gembedDim > 0)
                {
                    l3gembedLayer = (new LayerStructure(DataPanel.L3gSize, BuilderParameters.L3G_EMBED_DIM, A_Func.Tanh, N_Type.Convolution_layer, BuilderParameters.L3G_EMBED_WIN, 0, false, device));
                    modelStructure.AddLayer(l3gembedLayer);
                }

                int charembedDim = BuilderParameters.CHAR_EMBED_DIM.Last();
                if (charembedDim > 0)
                {
                    int inputDim = DataPanel.CharSize;
                    for (int i = 0; i < BuilderParameters.CHAR_EMBED_DIM.Length; i++)
                    {
                        LayerStructure charEmbed = (new LayerStructure(inputDim, BuilderParameters.CHAR_EMBED_DIM[i], A_Func.Tanh, N_Type.Convolution_layer, BuilderParameters.CHAR_EMBED_WIN[i], 0, false, device));
                        inputDim = BuilderParameters.CHAR_EMBED_DIM[i];
                        charembedLayer.Add(charEmbed);
                        modelStructure.AddLayer(charEmbed);
                    }
                }

                embedDim = embedDim + l3gembedDim + charembedDim;
                for (int i = 0; i < BuilderParameters.HighWay_Layer; i++)
                {
                    if (i == 0 || !BuilderParameters.HighWay_Share)
                    {
                        highwayConnect.Add(new LayerStructure(embedDim, embedDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true, device));
                        highwayGate.Add(new LayerStructure(embedDim, embedDim, A_Func.Sigmoid, N_Type.Fully_Connected, 1, 0, true, device));

                        modelStructure.AddLayer(highwayConnect[i]);
                        modelStructure.AddLayer(highwayGate[i]);
                    }
                    else
                    {
                        highwayConnect.Add(highwayConnect[0]);
                        highwayGate.Add(highwayGate[0]);
                    }
                }
                {
                    contextD1LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    contextD2LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);

                    modelStructure.AddLayer(contextD1LSTM);
                    modelStructure.AddLayer(contextD2LSTM);
                }

                if (!BuilderParameters.IS_SHARE_RNN)
                {
                    queryD1LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    queryD2LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);

                    modelStructure.AddLayer(queryD1LSTM);
                    modelStructure.AddLayer(queryD2LSTM);
                }
                else
                {
                    queryD1LSTM = contextD1LSTM;
                    queryD2LSTM = contextD2LSTM;
                }

                if (BuilderParameters.CNN_DIM > 0)
                {
                    CNNembed = (new LayerStructure(embedDim, BuilderParameters.CNN_DIM, A_Func.Tanh, N_Type.Convolution_layer, 
                        BuilderParameters.CNN_WIN, 0, true, device));
                    modelStructure.AddLayer(CNNembed);
                }

                int D = BuilderParameters.GRU_DIM * 2 + BuilderParameters.CNN_DIM;
                int H = BuilderParameters.GRU_DIM;

                CoAttLayer = new LayerStructure(3 * D, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                modelStructure.AddLayer(CoAttLayer);

                int GDim = 4 * D;
                switch (BuilderParameters.MEMORY_ENSEMBLE)
                {
                    case 2: GDim = 3 * D; break;
                    case 3: GDim = 4 * D; break;
                }

                M1D1LSTM = new LSTMStructure(GDim, new int[] { H }, device, BuilderParameters.RndInit);
                M1D2LSTM = new LSTMStructure(GDim, new int[] { H }, device, BuilderParameters.RndInit);
                modelStructure.AddLayer(M1D1LSTM);
                modelStructure.AddLayer(M1D2LSTM);

                int MemoryDim = 2 * H;
                switch (BuilderParameters.MEMORY_CONFIG)
                {
                    case 0: MemoryDim = 2 * H; break;
                    case 1: MemoryDim = 4 * D + 2 * H; break;
                    case 2: 
                    case 3: 
                    case 4:
                        M2D1LSTM = new LSTMStructure(2 * H, new int[] { H }, device, BuilderParameters.RndInit);
                        M2D2LSTM = new LSTMStructure(2 * H, new int[] { H }, device, BuilderParameters.RndInit);
                        modelStructure.AddLayer(M2D1LSTM);
                        modelStructure.AddLayer(M2D2LSTM);

                        switch(BuilderParameters.MEMORY_CONFIG)
                        {
                            case 2: MemoryDim = 2 * H; break;
                            case 3: MemoryDim = 4 * D + 2 * H; break;
                            case 4: MemoryDim = 4 * H; break;
                        }
                        break;
                }

                InitStateStruct = new LayerStructure(BuilderParameters.RECURRENT_DIM, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                modelStructure.AddLayer(InitStateStruct);

                GruCell = new GRUCell(2 * MemoryDim, BuilderParameters.RECURRENT_DIM, device, BuilderParameters.RndInit);
                modelStructure.AddLayer(GruCell);

                AnsStartStruct = new MLPAttentionStructure(BuilderParameters.RECURRENT_DIM, MemoryDim, BuilderParameters.ANS_HID_DIM, device);
                modelStructure.AddLayer(AnsStartStruct);

                AnsEndStruct = new MLPAttentionStructure(BuilderParameters.RECURRENT_DIM + MemoryDim, MemoryDim, BuilderParameters.ANS_HID_DIM, device);
                modelStructure.AddLayer(AnsEndStruct);

                TermalStruct = new LayerStructure(BuilderParameters.RECURRENT_DIM + 2 * MemoryDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device);
                modelStructure.AddLayer(TermalStruct);

                AnsStartLayer = new LayerStructure(MemoryDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                modelStructure.AddLayer(AnsStartLayer);

                AnsEndLayer = new LayerStructure(2 * MemoryDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                modelStructure.AddLayer(AnsEndLayer);
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, device);
                    int link = 0;

                    if (BuilderParameters.IS_EMBED_UPDATE) { wordembedLayer = (LayerStructure)modelStructure.CompositeLinks[link++]; }
                    else { wordembedLayer = InitWordEmbedLayers(device); }
                    if (BuilderParameters.L3G_EMBED_DIM > 0) { l3gembedLayer = (LayerStructure)modelStructure.CompositeLinks[link++]; }
                    if (BuilderParameters.CHAR_EMBED_DIM.Last() > 0)
                    {
                        for (int i = 0; i < BuilderParameters.CHAR_EMBED_DIM.Length; i++)
                        {
                            charembedLayer.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                        }
                    }

                    for (int i = 0; i < BuilderParameters.HighWay_Layer; i++)
                    {
                        if (i == 0 || !BuilderParameters.HighWay_Share)
                        {
                            highwayConnect.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                            highwayGate.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                        }
                        else
                        {
                            highwayConnect.Add(highwayConnect[0]);
                            highwayGate.Add(highwayGate[0]);
                        }
                    }

                    {
                        contextD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                        contextD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    }
                    if (!BuilderParameters.IS_SHARE_RNN)
                    {
                        queryD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                        queryD2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    }
                    else
                    {
                        queryD1LSTM = contextD1LSTM;
                        queryD2LSTM = contextD2LSTM;
                    }
                    if (BuilderParameters.CNN_DIM > 0) { CNNembed = (LayerStructure)modelStructure.CompositeLinks[link++]; }

                    CoAttLayer = (LayerStructure)modelStructure.CompositeLinks[link++];
                    M1D1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    M1D2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];

                    switch (BuilderParameters.MEMORY_CONFIG)
                    {
                        case 2:
                        case 3:
                        case 4:
                            M2D1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                            M2D2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                            break;
                    }

                    InitStateStruct = (LayerStructure)modelStructure.CompositeLinks[link++];

                    GruCell = (GRUCell)modelStructure.CompositeLinks[link++];
                    AnsStartStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    AnsEndStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    TermalStruct = (LayerStructure)modelStructure.CompositeLinks[link++];

                    AnsStartLayer = (LayerStructure)modelStructure.CompositeLinks[link++];
                    AnsEndLayer = (LayerStructure)modelStructure.CompositeLinks[link++];
                }
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainContextL3G, DataPanel.TrainQueryL3G, DataPanel.TrainContextChar, DataPanel.TrainQueryChar, DataPanel.TrainStartPos, DataPanel.TrainEndPos,
                        wordembedLayer, l3gembedLayer, charembedLayer, highwayGate, highwayConnect, contextD1LSTM, contextD2LSTM, CNNembed, queryD1LSTM, queryD2LSTM, CNNembed,
                        CoAttLayer, M1D1LSTM, M1D2LSTM, M2D1LSTM, M2D2LSTM, InitStateStruct, GruCell, AnsStartStruct, AnsEndStruct, TermalStruct, AnsStartLayer, AnsEndLayer, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }, string.Empty);
                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    //ComputationGraph testCG = null;
                    //if (BuilderParameters.IsTestFile)
                    //    testCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                    //        embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                    //        stateStruct, attStruct, att2Struct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.ScoreOutputPath + ".Test");

                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraph(DataPanel.ValidContext, DataPanel.ValidQuery, DataPanel.ValidContextL3G, DataPanel.ValidQueryL3G, DataPanel.ValidContextChar, DataPanel.ValidQueryChar, DataPanel.ValidStartPos, DataPanel.ValidEndPos,
                            wordembedLayer, l3gembedLayer, charembedLayer, highwayGate, highwayConnect, contextD1LSTM, contextD2LSTM, CNNembed, queryD1LSTM, queryD2LSTM, CNNembed,
                            CoAttLayer, M1D1LSTM, M1D2LSTM, M2D1LSTM, M2D2LSTM, InitStateStruct, GruCell, AnsStartStruct, AnsEndStruct, TermalStruct, AnsStartLayer, AnsEndLayer, modelStructure, 
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, string.Empty);

                    double bestValidScore = double.MinValue; // validCG.Execute(); //double.MinValue;
                    //double bestTestScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }

                        double validScore = 0;
                        if (validCG != null)
                        {
                            validScore = validCG.Execute();
                            Logger.WriteLog("Valid Score {0}, At iter {1}", validScore, iter);
                        }

                        if (validScore > bestValidScore)
                        {
                            bestValidScore = validScore; bestIter = iter;
                        }
                        Logger.WriteLog("Best Valid Score {0},  At iter {1}", bestValidScore, bestIter);
                    }
                    break;
                case DNNRunMode.Predict:
                    break;
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQuery = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContextL3G = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQueryL3G = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContextChar = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQueryChar = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainStartPos = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainEndPos = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQuery = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContextL3G = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQueryL3G = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContextChar = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQueryChar = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidStartPos = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidEndPos = null;
            public static string[] ValidIds = null;
            public static List<HashSet<string>> ValidResults = new List<HashSet<string>>();

            public static ItemFreqIndexDictionary wordFreqDict = null;
            public static ItemFreqIndexDictionary l3gFreqDict = null;
            public static ItemFreqIndexDictionary charFreqDict = null;
            static Dictionary<int, List<int>> word2l3g = new Dictionary<int, List<int>>();
            static Dictionary<int, List<int>> word2char = new Dictionary<int, List<int>>();
            public static int L3gSize { get { return l3gFreqDict.ItemDictSize; } }
            public static int WordSize { get { return wordFreqDict.ItemDictSize; } }
            public static int CharSize { get { return charFreqDict.ItemDictSize; } }

            public static int MaxQueryLen = 0;
            public static int MaxDocLen = 0;
            public static int MaxCrossQDLen = 0;
            static IEnumerable<Tuple<string, string, string>> ExtractQAS(string paraFile, string queryFile, string ansFile)
            {
                using (StreamReader paraReader = new StreamReader(paraFile))
                using (StreamReader queryReader = new StreamReader(queryFile))
                using (StreamReader ansReader = new StreamReader(ansFile))
                {
                    int lineIdx = 0;
                    while (!ansReader.EndOfStream)
                    {
                        string p = paraReader.ReadLine();
                        string q = queryReader.ReadLine();
                        string a = ansReader.ReadLine();
                        //Console.WriteLine("{0},{1}", p, q);
                        yield return new Tuple<string, string, string>(p, q, a);
                        lineIdx += 1;
                    }
                    Console.WriteLine("Total Number of answers {0}", lineIdx);
                }
            }

            static Tuple<int, int> GetSpan(List<string[]> sentenceIdxs, int startSent, int startIdx, int endSent, int endIdx)
            {
                if (startSent == -1 || startIdx == -1 || endSent == -1 || endIdx == -1)
                {
                    return new Tuple<int, int>(-1, -1);
                }
                if (sentenceIdxs.Count <= startSent || sentenceIdxs.Count <= endSent)
                {
                    Console.WriteLine("Sentence number is not enough! {0}, start {1}, end {2}", sentenceIdxs.Count, startSent, endSent);
                    Console.ReadLine();
                }

                if (startSent != endSent)
                {
                    Console.WriteLine("Sentence start is not equals to Sentence end! {0} and {1}", startSent, endSent);
                }

                int answerSentStart = sentenceIdxs.Take(startSent).Sum(i => i.Length) + startIdx;
                int answerSentEnd = sentenceIdxs.Take(endSent).Sum(i => i.Length) + endIdx;

                return new Tuple<int, int>(answerSentStart, answerSentEnd);
            }

            static void ExtractCorpusBinary(string paraFile, string queryFile, string ansFile, string paraFileBin, string queryFileBin, string ansFileBin, bool isShuffle, int miniBatchSize,
                int spanSize, List<HashSet<string>> resultSave)
            {
                SeqSparseBatchData context = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData query = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData contextL3g = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = L3gSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData queryL3g = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = L3gSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData contextChar = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = CharSize }, DeviceType.CPU);
                SeqSparseBatchData queryChar = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = CharSize }, DeviceType.CPU);

                DenseBatchData ansStart = new DenseBatchData(new DenseDataStat() { Dim = 1 }, DeviceType.CPU);
                DenseBatchData ansEnd = new DenseBatchData(new DenseDataStat() { Dim = 1 }, DeviceType.CPU);

                BinaryWriter contextWriter = FileUtil.CreateBinaryWrite(paraFileBin);
                BinaryWriter queryWriter = FileUtil.CreateBinaryWrite(queryFileBin);
                BinaryWriter contextL3GWriter = FileUtil.CreateBinaryWrite(paraFileBin + ".l3g");
                BinaryWriter queryL3GWriter = FileUtil.CreateBinaryWrite(queryFileBin + ".l3g");
                BinaryWriter contextCharWriter = FileUtil.CreateBinaryWrite(paraFileBin + ".char");
                BinaryWriter queryCharWriter = FileUtil.CreateBinaryWrite(queryFileBin + ".char");
                BinaryWriter ansStartWriter = FileUtil.CreateBinaryWrite(ansFileBin + ".1");
                BinaryWriter ansEndWriter = FileUtil.CreateBinaryWrite(ansFileBin + ".2");

                IEnumerable<Tuple<string, string, string>> QAS = ExtractQAS(paraFile, queryFile, ansFile);

                IEnumerable<Tuple<string, string, string>> Input = QAS;
                if (isShuffle) Input = CommonExtractor.RandomShuffle<Tuple<string, string, string>>(QAS, 1000000, DeepNet.BuilderParameters.RandomSeed);

                int crossLen = 0;
                int lineIdx = 0;
                foreach (Tuple<string, string, string> smp in Input)
                {
                    string p = smp.Item1;
                    string q = smp.Item2;
                    string a = smp.Item3;

                    List<string[]> sentenceIdxs = p.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries).
                        Select(i => i.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)).ToList();
                    string[] queryIdxs = q.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    List<Dictionary<int, float>> contextFea = new List<Dictionary<int, float>>();
                    List<Dictionary<int, float>> contextL3GFea = new List<Dictionary<int, float>>();
                    foreach (string[] term in sentenceIdxs)
                    {
                        foreach (string id in term)
                        {
                            int termIdx = int.Parse(id);
                            var tmp = new Dictionary<int, float>();
                            tmp[termIdx] = 1;
                            contextFea.Add(tmp);
                            var l3gtmp = new Dictionary<int, float>();
                            foreach (int l3gid in word2l3g[termIdx])
                            {
                                if (l3gtmp.ContainsKey(l3gid))
                                {
                                    l3gtmp[l3gid] += 1;
                                }
                                else
                                {
                                    l3gtmp.Add(l3gid, 1);
                                }
                            }
                            contextL3GFea.Add(l3gtmp);

                            List<Dictionary<int, float>> contextCharFea = new List<Dictionary<int, float>>();
                            foreach (int cid in word2char[termIdx])
                            {
                                var chartmp = new Dictionary<int, float>();
                                chartmp[cid] = 1;
                                contextCharFea.Add(chartmp);
                            }
                            contextChar.PushSample(contextCharFea);
                        }
                    }

                    List<Dictionary<int, float>> queryFea = new List<Dictionary<int, float>>();
                    List<Dictionary<int, float>> queryL3GFea = new List<Dictionary<int, float>>();
                    foreach (string term in queryIdxs)
                    {
                        int termIdx = int.Parse(term);
                        var tmp = new Dictionary<int, float>();
                        tmp[termIdx] = 1;
                        queryFea.Add(tmp);
                        var l3gtmp = new Dictionary<int, float>();
                        foreach (int l3gid in word2l3g[termIdx])
                        {
                            if (l3gtmp.ContainsKey(l3gid))
                            {
                                l3gtmp[l3gid] += 1;
                            }
                            else
                            {
                                l3gtmp.Add(l3gid, 1);
                            }
                        }
                        queryL3GFea.Add(l3gtmp);

                        List<Dictionary<int, float>> queryCharFea = new List<Dictionary<int, float>>();
                        foreach (int cid in word2char[termIdx])
                        {
                            var chartmp = new Dictionary<int, float>();
                            chartmp[cid] = 1;
                            queryCharFea.Add(chartmp);
                        }
                        queryChar.PushSample(queryCharFea);
                    }

                    string[] poses = a.Split(new char[] { ',', ')', '(', '#' }, StringSplitOptions.RemoveEmptyEntries);

                    int answerSentStart = -1;
                    int answerSentEnd = -1;
                    if (resultSave != null) { resultSave.Add(new HashSet<string>()); }
                    for (int m = 0; m < poses.Length / 4; m++)
                    {
                        int startSent = int.Parse(poses[4 * m + 0]);
                        int startIdx = int.Parse(poses[4 * m + 1]);
                        int endSent = int.Parse(poses[4 * m + 2]);
                        int endIdx = int.Parse(poses[4 * m + 3]);
                        Tuple<int, int> r = GetSpan(sentenceIdxs, startSent, startIdx, endSent, endIdx);
                        answerSentStart = r.Item1;
                        answerSentEnd = r.Item2;

                        if (resultSave != null) { resultSave[lineIdx].Add(string.Format("{0}#{1}", answerSentStart, answerSentEnd)); }
                    }

                    if (spanSize > 0 && answerSentEnd - answerSentStart + 1 > spanSize) continue;
                    if (resultSave == null && (answerSentEnd == -1 || answerSentStart == -1)) continue;

                    float[] tmpAnsStart = new float[contextFea.Count];
                    float[] tmpAnsEnd = new float[contextFea.Count];
                    for (int i = 0; i < contextFea.Count; i++)
                    {
                        tmpAnsStart[i] = 0;
                        tmpAnsEnd[i] = 0;
                        if (i == answerSentStart) { tmpAnsStart[i] = 1; }
                        if (i == answerSentEnd) { tmpAnsEnd[i] = 1; }
                    }

                    if (contextFea.Count > MaxDocLen) { MaxDocLen = contextFea.Count; }
                    if (queryFea.Count > MaxQueryLen) { MaxQueryLen = queryFea.Count; }
                    crossLen += contextFea.Count * queryFea.Count;
                    if (crossLen > MaxCrossQDLen) { MaxCrossQDLen = crossLen; }

                    context.PushSample(contextFea);
                    query.PushSample(queryFea);
                    contextL3g.PushSample(contextL3GFea);
                    queryL3g.PushSample(queryL3GFea);
                    ansStart.PushSample(tmpAnsStart, contextFea.Count);
                    ansEnd.PushSample(tmpAnsEnd, contextFea.Count);
                    if (answerSentEnd < answerSentStart)
                    {
                        Console.WriteLine("answer sent end smaller than answer sent start {0} {1} {2} ### {3}", lineIdx, answerSentEnd, answerSentStart, string.Join("##", poses));
                        Console.ReadLine();
                    }
                    if (context.BatchSize >= miniBatchSize)
                    {
                        context.PopBatchToStat(contextWriter);
                        query.PopBatchToStat(queryWriter);
                        contextL3g.PopBatchToStat(contextL3GWriter);
                        queryL3g.PopBatchToStat(queryL3GWriter);
                        contextChar.PopBatchToStat(contextCharWriter);
                        queryChar.PopBatchToStat(queryCharWriter);
                        ansStart.PopBatchToStat(ansStartWriter);
                        ansEnd.PopBatchToStat(ansEndWriter);
                        crossLen = 0;
                    }

                    if (++lineIdx % 1000 == 0) { Console.WriteLine("Extract Binary from Corpus {0}", lineIdx); }
                }
                context.PopBatchCompleteStat(contextWriter);
                query.PopBatchCompleteStat(queryWriter);
                contextL3g.PopBatchCompleteStat(contextL3GWriter);
                queryL3g.PopBatchCompleteStat(queryL3GWriter);
                contextChar.PopBatchCompleteStat(contextCharWriter);
                queryChar.PopBatchCompleteStat(queryCharWriter);
                ansStart.PopBatchCompleteStat(ansStartWriter);
                ansEnd.PopBatchCompleteStat(ansEndWriter);

                if (resultSave != null)
                {
                    using (StreamWriter ansWriter = new StreamWriter(ansFile + ".index"))
                    {
                        foreach (HashSet<string> results in resultSave)
                        {
                            ansWriter.WriteLine(string.Join("\t", results.ToArray()));
                        }
                    }
                }
                Console.WriteLine("Context Stat {0}", context.Stat.ToString());
                Console.WriteLine("Query Stat {0}", query.Stat.ToString());
                Console.WriteLine("Context l3g Stat {0}", contextL3g.Stat.ToString());
                Console.WriteLine("Query l3g Stat {0}", queryL3g.Stat.ToString());
                Console.WriteLine("Context char Stat {0}", contextChar.Stat.ToString());
                Console.WriteLine("Query char Stat {0}", queryChar.Stat.ToString());
                Console.WriteLine("Answer Start Stat {0}", ansStart.Stat.ToString());
                Console.WriteLine("Answer End Stat {0}", ansEnd.Stat.ToString());
                Console.WriteLine("Max Q Len {0}, Max D Len {1}, Max QD Cross Len {2}", MaxQueryLen, MaxDocLen, MaxCrossQDLen);
            }



            public static void Init()
            {
                #region Preprocess Data.
                // Step 1 : Load Word Vocab .
                {
                    using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab))
                    {
                        wordFreqDict = new ItemFreqIndexDictionary(mreader, false);
                    }
                }

                SimpleTextTokenizer textTokenize = new SimpleTextTokenizer();

                // Step 2 : Extract L3G vocab from word vocab.
                l3gFreqDict = new ItemFreqIndexDictionary("l3g dictionary");
                foreach (KeyValuePair<string, int> wordItem in wordFreqDict.ItemIndex.NameDict)
                {
                    int wordIdx = wordItem.Value;
                    string wordStr = wordItem.Key;
                    word2l3g.Add(wordIdx, new List<int>());
                    IEnumerable<string> strList = textTokenize.LnGTokenize(wordStr, 3);
                    foreach (string ngram in strList)
                    {
                        int ngramIdx = l3gFreqDict.ItemIndex.Index(ngram, true);
                        word2l3g[wordIdx].Add(ngramIdx);
                    }
                }
                Console.WriteLine("l3g vocab size {0}", l3gFreqDict.ItemDictSize);

                // Step 3 : Extract char vocab from word vocab.
                charFreqDict = new ItemFreqIndexDictionary("char dictionary");
                foreach (KeyValuePair<string, int> wordItem in wordFreqDict.ItemIndex.NameDict)
                {
                    int wordIdx = wordItem.Value;
                    string wordStr = wordItem.Key;
                    word2char.Add(wordIdx, new List<int>());
                    IEnumerable<string> strList = textTokenize.LetterTokenize(wordStr);
                    foreach (string c in strList)
                    {
                        int cIdx = charFreqDict.ItemIndex.Index(c, true);
                        word2char[wordIdx].Add(cIdx);
                    }
                }
                Console.WriteLine("char vocab size {0}", charFreqDict.ItemDictSize);
                //Console.ReadLine();

                // Step 4 : Index 2 Binary Data.
                if (BuilderParameters.IsTrainFile)
                //&& 
                //!FileUtil.IsFileinUse(new FileInfo(BuilderParameters.TrainQueryBin)) &&
                //!FileUtil.IsFileinUse(new FileInfo(BuilderParameters.TrainContextBin)))
                {
                    ExtractCorpusBinary(
                        BuilderParameters.TrainContext, BuilderParameters.TrainQuery, BuilderParameters.TrainPos,
                        BuilderParameters.TrainContextBin, BuilderParameters.TrainQueryBin, BuilderParameters.TrainPosBin,
                        true, BuilderParameters.MiniBatchSize, BuilderParameters.SPAN_LENGTH, null);
                }

                if (BuilderParameters.IsValidFile)
                //&&
                //!FileUtil.IsFileinUse(new FileInfo(BuilderParameters.ValidQueryBin)) &&
                //!FileUtil.IsFileinUse(new FileInfo(BuilderParameters.ValidContextBin)))
                {
                    ExtractCorpusBinary(
                        BuilderParameters.ValidContext, BuilderParameters.ValidQuery, BuilderParameters.ValidPos,
                        BuilderParameters.ValidContextBin, BuilderParameters.ValidQueryBin, BuilderParameters.ValidPosBin,
                        false, BuilderParameters.MiniBatchSize, 0, ValidResults);
                }
                else
                {
                    using (StreamReader ansReader = new StreamReader(BuilderParameters.ValidPos + ".index"))
                    {
                        while (!ansReader.EndOfStream)
                        {
                            string[] items = ansReader.ReadLine().Split('\t');
                            ValidResults.Add(new HashSet<string>(items));
                        }
                    }
                }
                ValidIds = File.ReadAllLines(BuilderParameters.ValidIds);
                #endregion.

                #region Load Data.
                if (BuilderParameters.IsTrainFile)
                {
                    TrainContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin);
                    TrainQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin);
                    TrainStartPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainPosBin + ".1");
                    TrainEndPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainPosBin + ".2");
                    TrainContextL3G = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin + ".l3g");
                    TrainQueryL3G = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin + ".l3g");
                    TrainContextChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin + ".char");
                    TrainQueryChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin + ".char");

                    TrainContext.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQuery.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainContextL3G.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQueryL3G.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainContextChar.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQueryChar.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainStartPos.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainEndPos.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                }

                if (BuilderParameters.IsValidFile)
                {
                    ValidContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin);
                    ValidQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin);
                    ValidStartPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidPosBin + ".1");
                    ValidEndPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidPosBin + ".2");
                    ValidContextL3G = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin + ".l3g");
                    ValidQueryL3G = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin + ".l3g");
                    ValidContextChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin + ".char");
                    ValidQueryChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin + ".char");

                    ValidContext.InitThreadSafePipelineCashier(64, false);
                    ValidQuery.InitThreadSafePipelineCashier(64, false);
                    ValidContextL3G.InitThreadSafePipelineCashier(64, false);
                    ValidQueryL3G.InitThreadSafePipelineCashier(64, false);
                    ValidContextChar.InitThreadSafePipelineCashier(64, false);
                    ValidQueryChar.InitThreadSafePipelineCashier(64, false);
                    ValidStartPos.InitThreadSafePipelineCashier(64, false);
                    ValidEndPos.InitThreadSafePipelineCashier(64, false);
                }
                #endregion.
            }
        }
    }
}
