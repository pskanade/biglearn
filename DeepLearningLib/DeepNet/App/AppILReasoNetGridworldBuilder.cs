﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;
namespace BigLearn.DeepNet
{
    /// <summary>
    /// Sequence 2 Sequence LSTM Model.
    /// </summary>
    public class AppILReasoNetGridworldBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Mini Batch Size"));

                //Argument.Add("TRAIN-IMG", new ParameterArgument(string.Empty, "Train Image Data."));
                //Argument.Add("TRAIN-ACT", new ParameterArgument(string.Empty, "Train Action Data."));

                //Argument.Add("DEV-IMG", new ParameterArgument(string.Empty, "Dev Image Data."));
                //Argument.Add("DEV-ACT", new ParameterArgument(string.Empty, "Dev Action Data."));

                Argument.Add("TASK", new ParameterArgument("gridworld", "GRIDWORLD/MNIST"));

                Argument.Add("GRID-DIM", new ParameterArgument("28", "Size of grid world."));
                Argument.Add("ACT-DIM", new ParameterArgument("8", "Action Dimension."));

                // Map Input to Status. 
                Argument.Add("SX", new ParameterArgument(string.Empty, "Convolutional Filter SX"));
                Argument.Add("STRIDE", new ParameterArgument(string.Empty, "Convolutional Filter Stride"));
                Argument.Add("O", new ParameterArgument(string.Empty, "Convolutional Output Dim"));
                Argument.Add("PAD", new ParameterArgument(string.Empty, "Pad space for image"));
                Argument.Add("AF", new ParameterArgument(string.Empty, "Activation functions"));
                Argument.Add("POOLSX", new ParameterArgument(string.Empty, "Pooling SX"));
                Argument.Add("POOLSTRIDE", new ParameterArgument(string.Empty, "Pooling Stride"));

                Argument.Add("RECURRENT-T", new ParameterArgument("10,10,1", "Recurrent Terminate Net."));

                Argument.Add("MEM-ADD-DIM", new ParameterArgument("256", "memory address dimension"));
                Argument.Add("MEM-CON-DIM", new ParameterArgument("256", "memory context dimension"));
                Argument.Add("MEM-SIZE", new ParameterArgument("512", "memory size"));

                Argument.Add("MEM-ATT-GAMMA", new ParameterArgument("10", " attention gamma"));
                Argument.Add("MEM-ATT-HIDDEN", new ParameterArgument("128", " attention hidden size."));
                Argument.Add("MEM-ATT-TYPE", new ParameterArgument("1", "0:inner product; 1: weighted inner product;"));

                

                Argument.Add("RECURRENT-STEP", new ParameterArgument("10", "Reasoning Steps."));

                //Argument.Add("TRAIN-MINI-BATCH", new ParameterArgument("64", "Mini Batch for Train."));
                //Argument.Add("DEV-MINI-BATCH", new ParameterArgument("32", "Mini Batch for Dev."));

                //Argument.Add("MAX-TRAIN-SRC-LENGTH", new ParameterArgument("50", "MAX src length."));
                //Argument.Add("MAX-TRAIN-TGT-LENGTH", new ParameterArgument("52", "MAX tgt length."));

                /////Language Word Error Rate.
                //Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));


                //Argument.Add("SRC-EMBED", new ParameterArgument("620", "Src String Dim"));
                //Argument.Add("SRC-LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));

                //Argument.Add("TGT-EMBED", new ParameterArgument("620", "Tgt String Dim"));
                //Argument.Add("TGT-LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));

                //Argument.Add("IS-SHARE-EMBED", new ParameterArgument("0", "0 : nonshared input and output; 1 : shared input and output;"));
                //Argument.Add("TGT-DECODE-EMBED", new ParameterArgument("620", "Tgt String  Decode Dim"));

                //Argument.Add("CONTEXT-LEN", new ParameterArgument("0", "Context Vector length"));
                //Argument.Add("DECODE-DIM", new ParameterArgument("200", "Decoding Dimension"));
                /////Softmax Randomup.
                //Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                //Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                //Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));
                //Argument.Add("TGT-MAX-LEN", new ParameterArgument("30", " Tgt Seq Max Length."));

                //Argument.Add("ENSEMBLE-RL", new ParameterArgument("0", "0:max terminate prob; 1:average terminate prob."));
                Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                //Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            //public static string TrainImg { get { return Argument["TRAIN-IMG"].Value; } }
            //public static string TrainAct { get { return Argument["TRAIN-ACT"].Value; } }

            //public static string ValidImg { get { return Argument["VALID-IMG"].Value; } }
            //public static string ValidAct { get { return Argument["VALID-ACT"].Value; } }

            public static string Task { get { return Argument["TASK"].Value; } }

            public static int BatchSize { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }

            public static int GridDim { get { return int.Parse(Argument["GRID-DIM"].Value); } }
            public static int ActionDim { get { return int.Parse(Argument["ACT-DIM"].Value); } }

            public static int[] SX { get { return Argument["SX"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] Stride { get { return Argument["STRIDE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] O { get { return Argument["O"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] Pad { get { return Argument["PAD"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] Afs { get { return Argument["AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] PoolSX { get { return Argument["POOLSX"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] PoolStride { get { return Argument["POOLSTRIDE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] Recurrent_T { get { return Argument["RECURRENT-T"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int MemAddDim { get { return int.Parse(Argument["MEM-ADD-DIM"].Value); } }
            public static int MemConDim { get { return int.Parse(Argument["MEM-CON-DIM"].Value); } }
            public static int MemSize { get { return int.Parse(Argument["MEM-SIZE"].Value); } }

            public static float MemAttGamma { get { return float.Parse(Argument["MEM-ATT-GAMMA"].Value); } }
            public static int MemAttHidden { get { return int.Parse(Argument["MEM-ATT-HIDDEN"].Value); } }
            public static int MemAttType { get { return int.Parse(Argument["MEM-ATT-TYPE"].Value); } }

            public static int Recurrent_Step { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            
            public static float RL_DISCOUNT { get { return float.Parse(Argument["RL-DISCOUNT"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
        }

        public override BuilderType Type { get { return BuilderType.APP_IL_REASONET_GRIDWORLD; } }
        public enum PredType { RL_MAXITER, RL_AVGPROB }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }
        

        public static ComputationGraph BuildComputationGraph(IDataCashier<ImageDataSource, ImageDataStat> imgData,
                                                             IDataCashier<DenseBatchData, DenseDataStat> actData,
                                                             List<ImageLinkStructure> cnn,
                                                             MemoryStructure memory,
                                                               MLPAttentionStructure attStruct,
                                                             GRUCell gruCell,
                                                             List<LayerStructure> termStruct,
                                                             LayerStructure actStruct,
                                                             RunnerBehavior Behavior, CompositeNNStructure Model)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

            /**************** Get Source and Target Data from DataCashier *********/
            ImageDataSource inputImg = (ImageDataSource)cg.AddDataRunner(new DataRunner<ImageDataSource, ImageDataStat>(imgData, Behavior));
            DenseBatchData outputAct = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(actData, Behavior));

            ImageDataSource input = inputImg;
            foreach (ImageLinkStructure linkModel in cnn)
            {
                ImageConvRunner<ImageDataSource> linkRunner = new ImageConvRunner<ImageDataSource>(linkModel, input, Behavior);
                cg.AddRunner(linkRunner);
                input = linkRunner.Output;
            }

            MatrixData status = new MatrixData(input);

            // Reasoning process.
            BasicReasoNetRunner ReasonRunner = new BasicReasoNetRunner(new HiddenBatchData(status), BuilderParameters.Recurrent_Step,
                                                               memory, attStruct, BuilderParameters.MemAttGamma, BuilderParameters.RL_DISCOUNT,
                                                               gruCell, termStruct, Behavior, (Att_Type)BuilderParameters.MemAttType);
            cg.AddRunner(ReasonRunner);

            List<HiddenBatchData> Actions = new List<HiddenBatchData>();
            for (int i = 0; i < BuilderParameters.Recurrent_Step; i++)
            {
                HiddenBatchData o = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(actStruct, ReasonRunner.StatusData[i], Behavior));
                Actions.Add(o);
            }

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new MultiClassContrastiveRewardRunner(outputAct.Data, Actions.ToArray(), ReasonRunner.AnsProb, 1, Behavior));
                    break;
                case DNNRunMode.Predict:
                    //ReasonRunner.AnsProb
                    for (int d = 0; d < BuilderParameters.Recurrent_Step; d++)
                    {
                        cg.AddRunner(new MatrixSoftmaxProcessor(new MatrixData(Actions[d]), 1, Behavior));
                    }
                    HiddenBatchData finalOutput = (HiddenBatchData)cg.AddRunner(new WAdditionMatrixRunner(Actions, ReasonRunner.AnsProb.ToList(), Behavior));
                    cg.AddRunner(new AccuracyDiskDumpRunner(outputAct.Data, finalOutput.Output, 1, BuilderParameters.ScoreOutputPath, false));
                    break; 
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation Data.");

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            CompositeNNStructure deepNet = new CompositeNNStructure();

            List<ImageLinkStructure> cnn = new List<ImageLinkStructure>();
            MemoryStructure memory = null;
            MLPAttentionStructure attStruct = null;
            GRUCell gruStruct = null;
            List<LayerStructure> termStruct = new List<LayerStructure>();
            LayerStructure actStruct = null;

            Logger.WriteLog("Loading CNN Structure.");

            int width = DataPanel.TrainImage.Stat.Width;
            int height = DataPanel.TrainImage.Stat.Height;
            int channel = DataPanel.TrainImage.Stat.Depth;

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                for (int i = 0; i < BuilderParameters.SX.Length; i++)
                {
                    ImageFilterParameter filter = new ImageFilterParameter(BuilderParameters.SX[i], BuilderParameters.Stride[i], channel, BuilderParameters.O[i],
                        BuilderParameters.Pad[i], BuilderParameters.Afs[i], BuilderParameters.PoolSX[i], BuilderParameters.PoolStride[i]);
                    ImageLinkStructure link = new ImageLinkStructure(filter, device);
                    width = filter.OutputWidth(width);
                    height = filter.OutputHeight(height);
                    channel = BuilderParameters.O[i];

                    cnn.Add(link);
                    deepNet.AddLayer(cnn[i]);
                }
                int statusDim = channel * width * height;

                memory = new MemoryStructure(BuilderParameters.MemSize, BuilderParameters.MemAddDim, BuilderParameters.MemConDim, device);
                attStruct = new MLPAttentionStructure(statusDim, BuilderParameters.MemAddDim, BuilderParameters.MemAttHidden, device);
                gruStruct = new GRUCell(BuilderParameters.MemConDim, statusDim, device, RndRecurrentInit.RndNorm);
                deepNet.AddLayer(memory);
                deepNet.AddLayer(attStruct);
                deepNet.AddLayer(gruStruct);

                int t = statusDim;
                for (int i = 0; i < BuilderParameters.Recurrent_T.Length; i++)
                {
                    termStruct.Add(new LayerStructure(t, BuilderParameters.Recurrent_T[i],
                        i == BuilderParameters.Recurrent_T.Length - 1 ? A_Func.Linear : A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false, device));
                    t = BuilderParameters.Recurrent_T[i];
                    deepNet.AddLayer(termStruct[i]);
                }

                actStruct = new LayerStructure(statusDim, BuilderParameters.ActionDim, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                deepNet.AddLayer(actStruct);
            }

            ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainImage, DataPanel.TrainLabel, cnn, memory, attStruct, gruStruct, termStruct, actStruct, 
                new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }, deepNet);
            trainCG.InitOptimizer(deepNet, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

            ComputationGraph validCG = BuildComputationGraph(DataPanel.ValidImage, DataPanel.ValidLabel, cnn, memory, attStruct, gruStruct, termStruct, actStruct,
                new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, deepNet);

            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
            {
                double loss = trainCG.Execute();
                Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                double accuracy = validCG.Execute();
                Logger.WriteLog("Iteration {0}, Accuracy {1}", iter, accuracy);
            }

            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<ImageDataSource, ImageDataStat> TrainImage = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainLabel = null;

            public static DataCashier<ImageDataSource, ImageDataStat> ValidImage = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidLabel = null;

            static string WorkingFolder = @"\\toronto-g1\PublicDataSource\gridworld\";

            static string x_file(string mode) { return WorkingFolder + "gridworld_28.mat.x." + mode; }
            static string v_file(string mode) { return WorkingFolder + "gridworld_28.mat.v." + mode; }
            static string y_file(string mode) { return WorkingFolder + "gridworld_28.mat.y." + mode; }
            static string s1_file(string mode) { return WorkingFolder + "gridworld_28.mat.s1." + mode; }
            static string s2_file(string mode) { return WorkingFolder + "gridworld_28.mat.s2." + mode; }

            static string img_file(string mode) { return WorkingFolder + BuilderParameters.Task + ".img." + mode + ".bin"; }
            static string lab_file(string mode) { return WorkingFolder + BuilderParameters.Task + ".lab." + mode + ".bin"; }

            static void LoadArray(string fileName, List<float[]> d, bool clip)
            {
                using (StreamReader mreader = new StreamReader(fileName))
                {
                    while (!mreader.EndOfStream)
                    {
                        string line = mreader.ReadLine();
                        float[] values = line.Trim().Split(' ').Select(i => (float) (clip ? (float.Parse(i) > 0 ? 1 : 0) : float.Parse(i)) ).ToArray();
                        d.Add(values);
                    }
                }
            }

            static void Preprocess(string mode)
            {
                if (File.Exists(img_file(mode)) && File.Exists(lab_file(mode))) return;

                ImageDataSource imageData = new ImageDataSource(BuilderParameters.BatchSize, 28, 28, 3, true, DeviceType.CPU);
                DenseBatchData labelData = new DenseBatchData(BuilderParameters.BatchSize, 1, DeviceType.CPU);
                using (BinaryWriter img_writer = new BinaryWriter(new FileStream(img_file(mode), FileMode.Create, FileAccess.Write)))
                using (BinaryWriter lab_writer = new BinaryWriter(new FileStream(lab_file(mode), FileMode.Create, FileAccess.Write)))
                {
                    List<float[]> x = new List<float[]>();
                    List<float[]> v = new List<float[]>();
                    List<float[]> y = new List<float[]>();
                    List<float[]> s1 = new List<float[]>();
                    List<float[]> s2 = new List<float[]>();

                    LoadArray(x_file(mode), x, true);
                    LoadArray(v_file(mode), v, true);
                    LoadArray(y_file(mode), y, false);
                    LoadArray(s1_file(mode), s1, false);
                    LoadArray(s2_file(mode), s2, false);

                    List<Tuple<float[], float[], float, float, float>> data = new List<Tuple<float[], float[], float, float, float>>();
                    for (int i = 0; i < x.Count; i++)
                    {
                        for (int s = 0; s < y[i].Length; s++)
                        {
                            data.Add(new Tuple<float[], float[], float, float, float>(x[i], v[i], y[i][s], s1[i][s], s2[i][s]));
                        }
                    }

                    foreach (Tuple<float[], float[], float, float, float> input in CommonExtractor.RandomShuffle<Tuple<float[], float[], float, float, float>>(data, 50000, 10))
                    {
                        float[] inputImage = new float[28 * 28 * 3];
                        float[] inputLabel = new float[1];

                        inputLabel[0] = input.Item3;
                        Array.Copy(input.Item1, inputImage, 28 * 28);
                        Array.Copy(input.Item2, 0, inputImage, 28 * 28, 28 * 28);

                        int p1 = (int)input.Item4;
                        int p2 = (int)input.Item5;
                        inputImage[2 * 28 * 28 + p1 * 28 + p2] = 1;

                        imageData.PushSample(inputImage, 1);
                        labelData.PushSample(inputLabel, 1);

                        if (imageData.BatchSize >= BuilderParameters.BatchSize)
                        {
                            imageData.PopBatchToStat(img_writer);
                            labelData.PopBatchToStat(lab_writer);
                        }
                    }

                    imageData.PopBatchCompleteStat(img_writer);
                    labelData.PopBatchCompleteStat(lab_writer);
                }
            }

            public static void Init()
            {
                Preprocess("train");
                Preprocess("test");

                TrainImage = new DataCashier<ImageDataSource, ImageDataStat>(img_file("train"));
                TrainLabel = new DataCashier<DenseBatchData, DenseDataStat>(lab_file("train"));
                TrainImage.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                TrainLabel.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);

                ValidImage = new DataCashier<ImageDataSource, ImageDataStat>(img_file("test"));
                ValidLabel = new DataCashier<DenseBatchData, DenseDataStat>(lab_file("test"));
                ValidImage.InitThreadSafePipelineCashier(100, false);
                ValidLabel.InitThreadSafePipelineCashier(100, false);
            }
        }
    }
}