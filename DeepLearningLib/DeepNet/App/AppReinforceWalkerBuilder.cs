﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    //public class AppReinforceWalkBuilder : Builder
    //{
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public class BuilderParameters : BaseModelArgument<BuilderParameters>
    //    {
    //        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
    //        public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

    //        #region Input Data Argument.
    //        public static string Entity2Id { get { return Argument["ENTITY2ID"].Value; } }
    //        public static int EntityNum { get { return int.Parse(Argument["ENTITYNUM"].Value); } }

    //        public static string Relation2Id { get { return Argument["RELATION2ID"].Value; } }
    //        public static int RelationNum { get { return int.Parse(Argument["RELATIONNUM"].Value); } }

    //        public static string TrainData { get { return Argument["TRAIN"].Value; } }
    //        public static string ValidData { get { return Argument["VALID"].Value; } }
    //        public static string TestData { get { return Argument["TEST"].Value; } }
    //        public static bool IsTrainFile { get { return !(TrainData.Equals(string.Empty)); } }
    //        public static bool IsValidFile { get { return !(ValidData.Equals(string.Empty)); } }
    //        public static bool IsTestFile { get { return !(TestData.Equals(string.Empty)); } }
    //        public static string ValidPathFolder { get { return Argument["VALID-PATH"].Value; } }
    //        #endregion.

    //        public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }
    //        public static int TrainBeamSize { get { return int.Parse(Argument["TRAIN-BEAM-SIZE"].Value); } }
    //        public static int TrainSetting { get { return int.Parse(Argument["TRAIN-SETTING"].Value); } }

    //        public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
    //        public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

    //        public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
    //        public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
    //        public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

    //        public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

    //        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
    //        public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

    //        public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }

    //        public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
    //        public static float T_MAX_CLIP { get { return float.Parse(Argument["T-MAX-CLIP"].Value); } }
    //        public static float T_MIN_CLIP { get { return float.Parse(Argument["T-MIN-CLIP"].Value); } }

    //        public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

    //        public static int TRAIN_GROUP { get { return int.Parse(Argument["TRAIN-GROUP"].Value); } }
    //        public static float GUID_RATE { get { return float.Parse(Argument["GUID-RATE"].Value); } }
    //        public static float RAND_RATE { get { return float.Parse(Argument["RAND-RATE"].Value); } }

    //        public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
    //        public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
    //        public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }

    //        public static float NORM_REWARD { get { return float.Parse(Argument["NORM-REWARD"].Value); } }

    //        public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

    //        public static int TRAIN_DEBUG { get { return int.Parse(Argument["TRAIN-DEBUG"].Value); } }

    //        public static int MCTS_NUM { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

    //        public static int VERSION { get { return int.Parse(Argument["VERSION"].Value); } }

    //        public static float RAND_TERM { get { return float.Parse(Argument["RAND-TERM"].Value); } }

    //        public static int POS_EMBED_DIM { get { return int.Parse(Argument["POS-EMBED-DIM"].Value); } }

    //        static BuilderParameters()
    //        {
    //            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
    //            Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

    //            #region Input Data Argument.
    //            Argument.Add("ENTITYNUM", new ParameterArgument("0", "Entity Number."));
    //            Argument.Add("RELATIONNUM", new ParameterArgument("0", "Relation Number."));

    //            Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
    //            Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
    //            Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));
    //            #endregion.

    //            Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
    //            Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
    //            Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));

    //            Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));
    //            Argument.Add("TRAIN-BEAM-SIZE", new ParameterArgument("10", "beam size."));

    //            Argument.Add("TRAIN-SETTING", new ParameterArgument("0", "0:supervised  1:reinforcement"));

    //            Argument.Add("TRAIN-GROUP", new ParameterArgument("32", "Mini Batch Size."));
    //            Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
    //            Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

    //            Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
    //            Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));

    //            Argument.Add("GUID-RATE", new ParameterArgument("1", "guided path rate."));
    //            Argument.Add("RAND-RATE", new ParameterArgument("10", "rand path rate."));

    //            Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
    //            Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
    //            Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

    //            Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.9", "Reward discount"));

    //            Argument.Add("T-MAX-CLIP", new ParameterArgument("0", "Maximum T Clip"));
    //            Argument.Add("T-MIN-CLIP", new ParameterArgument("0", "Minimum T Clip"));

    //            Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

    //            Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

    //            Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score File."));
    //            Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));

    //            Argument.Add("NORM-REWARD", new ParameterArgument("0", "normalize reward."));
    //            Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));

    //            Argument.Add("TRAIN-DEBUG", new ParameterArgument("0", "0:use dev set as train; 1: use train set"));

    //            Argument.Add("MCTS-NUM", new ParameterArgument("10", "Monto Carlo Tree Search Number"));
    //            Argument.Add("VERSION", new ParameterArgument("0", "Version 0: old version; Version 1: new version;"));

    //            Argument.Add("RAND-TERM", new ParameterArgument("0.3", "Random termination gate."));
    //            Argument.Add("POS-EMBED-DIM", new ParameterArgument("0","0: disable position embedding; num: enable position embedding; "));
    //        }
    //    }

    //    public override BuilderType Type { get { return BuilderType.APP_REINFORCE_WALK; } }

    //    public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

    //    /// <summary>
    //    /// episodic memory structure.
    //    /// </summary>
    //    public class EpisodicMemory
    //    {
    //        Dictionary<string, Tuple<List<Tuple<float, float>>, int>> Mem = new Dictionary<string, Tuple<List<Tuple<float, float>>, int>>();
    //        int Time { get; set; }
    //        public EpisodicMemory()
    //        {
    //            Time = 0;
    //        }
    //        public void Clear()
    //        {
    //            Mem.Clear();
    //            Time = 0;
    //        }

    //        /// <summary>
    //        /// memory index.
    //        /// </summary>
    //        /// <param name="key"></param>
    //        /// <param name="actionCount"></param>
    //        /// <param name="topK"></param>
    //        /// <returns></returns>
    //        public List<Tuple<float, float>> Search(string key)
    //        {
    //            if (Mem.ContainsKey(key))
    //            {
    //                return Mem[key].Item1;
    //            }
    //            else
    //            {
    //                return null;
    //            }
    //        }

    //        public void Update(string key, int actid, int totalAct, float reward)
    //        {
    //            float new_u = 0;
    //            float new_c = 0;

    //            // update memory.
    //            if (Mem.ContainsKey(key))
    //            {
    //                float u = Mem[key].Item1[actid].Item1;
    //                float c = Mem[key].Item1[actid].Item2;
    //                new_u = c / (c + 1.0f) * u + 1.0f / (c + 1.0f) * reward;
    //                new_c = c + 1;
    //            }
    //            else
    //            {
    //                Mem.Add(key, new Tuple<List<Tuple<float, float>>, int>(new List<Tuple<float, float>>(), Time));

    //                for (int a = 0; a < totalAct; a++)
    //                {
    //                    Mem[key].Item1.Add(new Tuple<float, float>(0, 0));
    //                }
    //                new_u = reward;
    //                new_c = 1;
    //            }
    //            Mem[key].Item1[actid] = new Tuple<float, float>(new_u, new_c);
    //        }

    //        public void UpdateTiming()
    //        {
    //            Time += 1;
    //        }
    //    }

    //    public class GraphQueryData : BatchData
    //    {
    //        public List<int> RawSource = new List<int>();
    //        public List<Tuple<int, int>> RawQuery = new List<Tuple<int, int>>();
    //        public List<int> RawTarget = new List<int>();
    //        public List<HashSet<int>> BlackTargets = new List<HashSet<int>>();

    //        public List<int> RawIndex = new List<int>();
    //        public List<int> RawStrategy = new List<int>();

    //        public int BatchSize { get { return RawQuery.Count; } }
    //        public int MaxBatchSize { get; set; }

    //        public List<StatusData> StatusPath { get; set; }

    //        public List<Tuple<int, int>> Results = new List<Tuple<int, int>>();

    //        /// <summary>
    //        /// max group number, and group size;
    //        /// </summary>
    //        /// <param name="maxGroupNum"></param>
    //        /// <param name="groupSize"></param>
    //        /// <param name="device"></param>
    //        public GraphQueryData(int maxBatchSize, DeviceType device)
    //        {
    //            MaxBatchSize = maxBatchSize;
    //            StatusPath = new List<StatusData>();
    //        }

    //        public List<int> GetBatchIdxs(int b, int step)
    //        {
    //            List<int> r = new List<int>();
    //            for (int i = 0; i < StatusPath[step].BatchSize; i++)
    //            {
    //                if (StatusPath[step].GetOriginalStatsIndex(i) == b)
    //                    r.Add(i);
    //            }
    //            return r;
    //        }

    //        public bool Isfinished(int b)
    //        {
    //            foreach(Tuple<int,int> item in Results)
    //            {
    //                if (item.Item2 == b) { return true; }
    //            }
    //            return false;
    //        }
    //    }

    //    public class StatusData : BatchData
    //    {
    //        /// <summary>
    //        /// Raw Query.
    //        /// </summary>
    //        public GraphQueryData GraphQuery { get; set; }

    //        /// <summary>
    //        /// Node ID.
    //        /// </summary>
    //        public List<int> NodeID { get; set; }

    //        /// <summary>
    //        /// Termination Probability.
    //        /// </summary>
    //        public HiddenBatchData Term = null;

    //        /// <summary>
    //        /// LogProbability to this node.
    //        /// </summary>
    //        List<float> LogProb { get; set; }

    //        /// <summary>
    //        /// MatchCandidate and MatchCandidateProb.
    //        /// </summary>
    //        public List<Tuple<int, int>> MatchCandidate = null;
    //        public SeqVectorData MatchCandidateProb = null;

    //        public List<int> PreSelActIndex = null;
    //        public List<int> PreStatusIndex = null;
    //        public int GetPreStatusIndex(int batchIdx)
    //        {
    //            if (Step == 0) { return batchIdx; }
    //            else return PreStatusIndex[batchIdx];
    //        }
    //        public int GetOriginalStatsIndex(int batchIdx)
    //        {
    //            if (Step == 0) { return batchIdx; }
    //            else
    //            {
    //                int b = GetPreStatusIndex(batchIdx);
    //                return GraphQuery.StatusPath[Step - 1].GetOriginalStatsIndex(b);
    //            }
    //        }

    //        public List<string> StatusKey = null;
    //        public string GetStatusKey(int b)
    //        {
    //            if (Step == 0) return string.Format("N:{0}-R:{1}", GraphQuery.RawQuery[b].Item1, GraphQuery.RawQuery[b].Item2);
    //            else return StatusKey[b];
    //        }

    //        /// <summary>
    //        /// constrastive reward.
    //        /// </summary>
    //        public List<float> CR { get; set; }

    //        public int MaxBatchSize { get { return StateEmbed.MAX_BATCHSIZE; } }
    //        public int BatchSize { get { return StateEmbed.BatchSize; } }

    //        /// <summary>
    //        /// logprobability of the node.
    //        /// </summary>
    //        /// <param name="batchIdx"></param>
    //        /// <returns></returns>
    //        public float GetLogProb(int batchIdx)
    //        {
    //            if (Step == 0) { return 0; }
    //            else { return LogProb[batchIdx]; }
    //        }

    //        /// <summary>
    //        /// Log Termination Probability.
    //        /// </summary>
    //        /// <param name="b"></param>
    //        /// <param name="isT"></param>
    //        /// <returns></returns>
    //        public float LogPTerm(int b, bool isT)
    //        {
    //            if (isT) { return (float)Util.LogLogistial(Term.Output.Data.MemPtr[b]); }
    //            else { return (float)Util.LogNLogistial(Term.Output.Data.MemPtr[b]); }
    //        }

    //        /// <summary>
    //        /// IsBlack Node.
    //        /// </summary>
    //        /// <param name="b"></param>
    //        /// <returns></returns>
    //        public bool IsBlack(int b)
    //        {
    //            int queryIdx = GetOriginalStatsIndex(b);
    //            if (GraphQuery.BlackTargets[queryIdx].Contains(NodeID[b])) { return true; }
    //            else return false;
    //        }


    //        /// <summary>
    //        /// Embedding of the State.
    //        /// </summary>
    //        public HiddenBatchData StateEmbed { get; set; }

    //        public int Step;

    //        public StatusData(GraphQueryData interData, List<int> nodeIndex, HiddenBatchData stateEmbed, DeviceType device) :
    //            this(interData, nodeIndex, null, null, null, null, stateEmbed, device)
    //        { }

    //        public StatusData(GraphQueryData interData,
    //                          List<int> nodeIndex, List<float> logProb, List<int> selectedAction, List<int> preStatusIndex, List<string> statusKey,
    //                          HiddenBatchData stateEmbed, DeviceType device)
    //        {
    //            GraphQuery = interData;
    //            GraphQuery.StatusPath.Add(this);

    //            NodeID = nodeIndex;
    //            StateEmbed = stateEmbed;

    //            LogProb = logProb;
    //            PreSelActIndex = selectedAction;
    //            PreStatusIndex = preStatusIndex;
    //            StatusKey = statusKey;
    //            //SelectedAction = selectedAction;

    //            CR = new List<float>();

    //            Step = GraphQuery.StatusPath.Count - 1;
    //        }
    //    }

    //    /// <summary>
    //    /// Sample Graph. 
    //    /// </summary>
    //    class SampleRunner : StructRunner
    //    {
    //        List<Tuple<int, int, int>> Graph { get; set; }
    //        public new GraphQueryData Output { get; set; }
    //        int MaxBatchSize { get; set; }
    //        DataRandomShuffling Shuffle { get; set; }
    //        Random random = new Random(14);

    //        int Group = 1;
    //        public SampleRunner(List<Tuple<int, int, int>> graph, int group, int maxBatchSize, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Graph = graph;
    //            MaxBatchSize = maxBatchSize * group;
    //            Shuffle = new DataRandomShuffling(Graph.Count * 2, random);
    //            Output = new GraphQueryData(maxBatchSize * group, behavior.Device);
    //            Group = group;
    //        }

    //        public override void Init()
    //        {
    //            IsTerminate = false;
    //            IsContinue = true;
    //            Shuffle.Init();
    //        }

    //        public override void Forward()
    //        {
    //            Output.RawQuery.Clear();
    //            Output.RawSource.Clear();
    //            Output.RawTarget.Clear();
    //            Output.BlackTargets.Clear();

    //            Output.RawStrategy.Clear();
    //            Output.RawIndex.Clear();

    //            int groupIdx = 0;
    //            while (groupIdx < MaxBatchSize / Group)
    //            {
    //                int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext();
    //                if (idx <= -1) { break; }
    //                int srcId = idx < Graph.Count ? Graph[idx].Item1 : Graph[idx - Graph.Count].Item2;
    //                int tgtId = idx < Graph.Count ? Graph[idx].Item2 : Graph[idx - Graph.Count].Item1;
    //                int linkId = idx < Graph.Count ? Graph[idx].Item3 : (Graph[idx - Graph.Count].Item3 + DataPanel.RelationNum);

    //                int rand_num = (int)(Group * BuilderParameters.RAND_RATE);

    //                for (int b = 0; b < Group; b++)
    //                {
    //                    Output.RawQuery.Add(new Tuple<int, int>(srcId, linkId));
    //                    Output.RawSource.Add(srcId);
    //                    Output.RawTarget.Add(tgtId);
    //                    Output.RawIndex.Add(idx);

    //                    if (b < rand_num)
    //                    {
    //                        Output.RawStrategy.Add(0);
    //                    }
    //                    else
    //                    {
    //                        Output.RawStrategy.Add(1);
    //                    }

    //                    HashSet<int> blackTarget = null;

    //                    if (Behavior.RunMode == DNNRunMode.Train)
    //                    {
    //                        blackTarget = DataPanel.knowledgeGraph.TrainNeighborHash[srcId].ContainsKey(linkId) ?
    //                                                DataPanel.knowledgeGraph.TrainNeighborHash[srcId][linkId] : new HashSet<int>();
    //                    }
    //                    else if (Behavior.RunMode == DNNRunMode.Predict)
    //                    {
    //                        blackTarget = DataPanel.knowledgeGraph.AllNeighborHash[srcId].ContainsKey(linkId) ?
    //                                                DataPanel.knowledgeGraph.AllNeighborHash[srcId][linkId] : new HashSet<int>();
    //                    }
    //                    Output.BlackTargets.Add(blackTarget);
    //                }

    //                //}
    //                groupIdx += 1;
    //            }
    //            if (groupIdx == 0) { IsTerminate = true; return; }
    //        }
    //    }

    //    class StatusEmbedRunner : CompositeNetRunner
    //    {
    //        public new List<Tuple<int, int>> Input { get; set; }

    //        public new HiddenBatchData Output { get; set; }

    //        int Pos { get; set; }

    //        EmbedStructure InNodeEmbed { get; set; }
    //        EmbedStructure LinkEmbed { get; set; }

    //        EmbedStructure PosEmbed { get; set; }

    //        public StatusEmbedRunner(List<Tuple<int, int>> input, int maxBatchSize, int pos, EmbedStructure inNodeEmbed, EmbedStructure linkEmbed, EmbedStructure posEmbed, RunnerBehavior behavior) : base(behavior)
    //        {
    //            Pos = pos;
    //            Input = input;
    //            InNodeEmbed = inNodeEmbed;
    //            LinkEmbed = linkEmbed;
    //            PosEmbed = posEmbed;

    //            // concate of node embedding and relation embedding.
    //            Output = new HiddenBatchData(maxBatchSize, inNodeEmbed.Dim + linkEmbed.Dim + (posEmbed == null ? 0 : posEmbed.Dim), DNNRunMode.Train, behavior.Device);
    //        }

    //        public override void Forward()
    //        {
    //            InNodeEmbed.Embedding.SyncToCPU();
    //            LinkEmbed.Embedding.SyncToCPU();
    //            if(PosEmbed != null) PosEmbed.Embedding.SyncToCPU();

    //            //BatchLinks.Clear();
    //            int batchSize = 0;
    //            while (batchSize < Input.Count)
    //            {
    //                int srcId = Input[batchSize].Item1;
    //                int linkId = Input[batchSize].Item2;

    //                int bidx = batchSize;

    //                if (BuilderParameters.N_EmbedDim > 0)
    //                {
    //                    FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim,
    //                                            InNodeEmbed.Embedding.MemPtr, srcId * InNodeEmbed.Dim, InNodeEmbed.Dim, 0, 1);
    //                }
    //                FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim + InNodeEmbed.Dim,
    //                                        LinkEmbed.Embedding.MemPtr, linkId * LinkEmbed.Dim, LinkEmbed.Dim, 0, 1);

    //                if(BuilderParameters.POS_EMBED_DIM > 0)
    //                {
    //                    FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim + InNodeEmbed.Dim + LinkEmbed.Dim,
    //                                        PosEmbed.Embedding.MemPtr, Pos * PosEmbed.Dim, PosEmbed.Dim, 0, 1);
    //                }

    //                batchSize += 1;
    //            }
    //            Output.BatchSize = batchSize;
    //            Output.Output.Data.SyncFromCPU(Output.BatchSize * Output.Dim);
    //        }

    //        public override void CleanDeriv()
    //        {
    //            ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
    //        }

    //        public override void Update()
    //        {
    //            if(Output.BatchSize == 0) { return; }
    //            Output.Deriv.Data.SyncToCPU(Output.Dim * Output.BatchSize);

    //            InNodeEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();
    //            LinkEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();
    //            if (PosEmbed != null) PosEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();

    //            for (int b = 0; b < Output.BatchSize; b++)
    //            {
    //                int srcIdx = Input[b].Item1;
    //                int linkIdx = Input[b].Item2;

    //                if (BuilderParameters.N_EmbedDim > 0)
    //                {
    //                    FastVector.Add_Vector(InNodeEmbed.EmbeddingOptimizer.Gradient.MemPtr, srcIdx * InNodeEmbed.Dim,
    //                                    Output.Deriv.Data.MemPtr, b * Output.Dim, InNodeEmbed.Dim,
    //                                    1, 1);
    //                }

    //                FastVector.Add_Vector(LinkEmbed.EmbeddingOptimizer.Gradient.MemPtr, linkIdx * LinkEmbed.Dim,
    //                                Output.Deriv.Data.MemPtr, b * Output.Dim + InNodeEmbed.Dim, LinkEmbed.Dim,
    //                                1, 1);

    //                if(BuilderParameters.POS_EMBED_DIM > 0)
    //                {
    //                    FastVector.Add_Vector(PosEmbed.EmbeddingOptimizer.Gradient.MemPtr, Pos * PosEmbed.Dim,
    //                                Output.Deriv.Data.MemPtr, b * Output.Dim + InNodeEmbed.Dim + LinkEmbed.Dim, PosEmbed.Dim,
    //                                1, 1);
    //                }
    //            }

    //            InNodeEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
    //            LinkEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
    //            if (PosEmbed != null) PosEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
    //        }
    //    }

    //    class CandidateActionRunner : CompositeNetRunner
    //    {
    //        public new StatusData Input;

    //        public new List<Tuple<int, int>> Output = new List<Tuple<int, int>>();
    //        public BiMatchBatchData Match;

    //        /// <summary>
    //        /// Input : State.
    //        /// node and rel embed.
    //        /// </summary>
    //        /// <param name="input"></param>
    //        /// <param name="nodeEmbed"></param>
    //        /// <param name="relEmbed"></param>
    //        /// <param name="behavior"></param>
    //        public CandidateActionRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
    //        {
    //            Input = input;

    //            Match = new BiMatchBatchData(new BiMatchBatchDataStat()
    //            {
    //                MAX_MATCH_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
    //                MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
    //                MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount
    //            }, behavior.Device);
    //        }

    //        Random random = new Random(15);
    //        public override void Forward()
    //        {
    //            Output.Clear();

    //            List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
    //            int cursor = 0;
    //            for (int b = 0; b < Input.BatchSize; b++)
    //            {
    //                int seedNode = Input.NodeID[b];

    //                int qid = Input.GetOriginalStatsIndex(b); // GetQueryIndex(b);
    //                int rawSrc = Input.GraphQuery.RawQuery[qid].Item1;
    //                int rawR = Input.GraphQuery.RawQuery[qid].Item2;
    //                int rawTgt = Input.GraphQuery.RawTarget[qid];

    //                int candidateNum = 0;
    //                for (int nei = 0; nei < DataPanel.knowledgeGraph.TrainNeighborLink[seedNode].Count; nei++)
    //                {
    //                    int lid = DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item1 > 0 ?
    //                        DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 :
    //                        DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 + DataPanel.RelationNum;

    //                    int relid = DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item1 > 0 ?
    //                        DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 + DataPanel.RelationNum :
    //                        DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2;

    //                    int tgtNode = DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item3;
    //                    if (seedNode == rawSrc && lid == rawR && tgtNode == rawTgt) { continue; }
    //                    if (seedNode == rawTgt && relid == rawR && tgtNode == rawSrc) { continue; }

    //                    Output.Add(new Tuple<int, int>(tgtNode, lid));
    //                    match.Add(new Tuple<int, int, float>(b, cursor, 1));
    //                    cursor += 1;
    //                    candidateNum += 1;
    //                }

    //                if (candidateNum == 0)
    //                {
    //                    Output.Add(new Tuple<int, int>(seedNode, random.Next(DataPanel.RelationNum * 2)));
    //                    match.Add(new Tuple<int, int, float>(b, cursor, 1));
    //                    cursor += 1;
    //                }
    //            }
    //            Match.SetMatch(match);
    //        }
    //    }

    //    static Random SampleRandom = new Random(10);

    //    public class BasicPolicyRunner : CompositeNetRunner
    //    {
    //        public new StatusData Input { get; set; }

    //        /// <summary>
    //        /// MatchData.
    //        /// </summary>
    //        public BiMatchBatchData MatchPath = null;

    //        /// <summary>
    //        /// new Node Index.
    //        /// </summary>
    //        public List<int> NodeIndex { get; set; }

    //        /// <summary>
    //        /// next step log probability.
    //        /// </summary>
    //        public List<float> LogProb { get; set; }

    //        /// <summary>
    //        /// pre sel action and pre status. 
    //        /// </summary>
    //        public List<int> PreSelActIndex { get; set; }
    //        public List<int> PreStatusIndex { get; set; }

    //        public List<string> StatusKey { get; set; }

    //        public BasicPolicyRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
    //        {
    //            Input = input;

    //            NodeIndex = new List<int>();
    //            LogProb = new List<float>();
    //            PreSelActIndex = new List<int>();
    //            PreStatusIndex = new List<int>();
    //            StatusKey = new List<string>();
    //        }
    //    }

    //    class OldActionSamplingRunner : BasicPolicyRunner
    //    {
    //        int lineIdx = 0;
    //        public override void Init()
    //        {
    //            lineIdx = 0;
    //        }
    //        /// <summary>
    //        /// match candidate, and match probability.
    //        /// </summary>
    //        /// <param name="input"></param>
    //        /// <param name="behavior"></param>
    //        public OldActionSamplingRunner(StatusData input, RunnerBehavior behavior) : base(input, behavior)
    //        {
    //            MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
    //            {
    //                MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
    //                MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
    //                MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
    //            }, behavior.Device);
    //        }

    //        public override void Forward()
    //        {
    //            //NodeIndex.Clear();
    //            //LogProb.Clear();
    //            //NodeAction.Clear();

    //            NodeIndex.Clear();
    //            LogProb.Clear();
    //            PreSelActIndex.Clear();
    //            PreStatusIndex.Clear();
    //            StatusKey.Clear();

    //            Input.MatchCandidateProb.Output.SyncToCPU();
    //            if (Input.Term != null) Input.Term.Output.SyncToCPU();

    //            int currentStep = Input.Step;
    //            List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

    //            for (int i = 0; i < Input.MatchCandidateProb.Segment; i++)
    //            {
    //                int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
    //                int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

    //                int selectIdx = -1;

    //                // draw sample actions.
    //                if (Input.GraphQuery.RawStrategy[i] == 1)
    //                {
    //                    int idx = Util.Sample(Input.MatchCandidateProb.Output.MemPtr, s, e - s, SampleRandom);
    //                    selectIdx = s + idx;
    //                }
    //                // draw random actions.
    //                else
    //                {
    //                    selectIdx = SampleRandom.Next(s, e);
    //                }

    //                NodeIndex.Add(Input.MatchCandidate[selectIdx].Item1);

    //                PreSelActIndex.Add(selectIdx);
    //                PreStatusIndex.Add(i);

    //                //NodeAction.Add(new Tuple<int, int>(i, selectIdx));

    //                float nonLogTerm = 0;
    //                if (Input.Term != null)
    //                {
    //                    nonLogTerm = Input.LogPTerm(i, false);
    //                }
    //                float prob = Input.GetLogProb(i) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
    //                LogProb.Add(prob);

    //                match.Add(new Tuple<int, int, float>(i, selectIdx, 1));
    //            }

    //            MatchPath.SetMatch(match);

    //            lineIdx += Input.MatchCandidateProb.Segment / BuilderParameters.BeamSize;
    //        }
    //    }

    //    /// <summary>
    //    /// it is a little difficult.
    //    /// </summary>
    //    class OldRewardRunner : ObjectiveRunner
    //    {
    //        Random random = new Random(14);
    //        new GraphQueryData Input { get; set; }
    //        public OldRewardRunner(GraphQueryData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Input = input;
    //        }

    //        public override void Forward()
    //        {
    //            // calculate termination probability.
    //            for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
    //            {
    //                if (Input.StatusPath[t].Term != null)
    //                {
    //                    ComputeLib.Logistic(Input.StatusPath[t].Term.Output.Data, 0, Input.StatusPath[t].Term.Output.Data, 0, Input.BatchSize, 1);
    //                    ComputeLib.ClipVector(Input.StatusPath[t].Term.Output.Data, Input.BatchSize, BuilderParameters.T_MAX_CLIP, BuilderParameters.T_MIN_CLIP); // 1 - Util.GPUEpsilon, Util.GPUEpsilon);
    //                    Input.StatusPath[t].Term.Output.Data.SyncToCPU();
    //                    Array.Clear(Input.StatusPath[t].Term.Deriv.Data.MemPtr, 0, Input.StatusPath[t].BatchSize);
    //                }
    //                if (Input.StatusPath[t].MatchCandidateProb != null)
    //                {
    //                    Array.Clear(Input.StatusPath[t].MatchCandidateProb.Deriv.MemPtr, 0, Input.StatusPath[t].MatchCandidateProb.Length);
    //                }
    //            }

    //            // calculate the probability of each stopable sample.
    //            List<int> SelectedStep = new List<int>();
    //            for (int b = 0; b < Input.BatchSize; b++)
    //            {
    //                int selectedStep = Input.StatusPath.Count - 1;

    //                for (int t = 1; t < Input.StatusPath.Count; t++)
    //                {
    //                    StatusData st = Input.StatusPath[t];
    //                    float tp = st.Term.Output.Data.MemPtr[b];
    //                    if (random.NextDouble() <= tp)
    //                    {
    //                        selectedStep = t;
    //                        break;
    //                    }
    //                }
    //                SelectedStep.Add(selectedStep);
    //            }

    //            int targetNum = 0;
    //            int blackNum = 0;
    //            int falseNum = 0;

    //            ObjectiveDict = new Dictionary<string, double>();

    //            for (int g = 0; g < Input.BatchSize; g++)
    //            {
    //                int t = SelectedStep[g];

    //                string mkey = string.Format("Term {0}", t);
    //                if (!ObjectiveDict.ContainsKey(mkey)) { ObjectiveDict[mkey] = 0; }
    //                ObjectiveDict[mkey] += 1;


    //                float cr = 0;

    //                int targetId = Input.StatusPath[t].NodeID[g];
    //                if (targetId == Input.RawTarget[g])
    //                {
    //                    targetNum += 1;
    //                    cr = BuilderParameters.POS_R;// 1.0f; // 1.0f / targetNum - targetNum * 1.0f / (targetNum + falseNum + float.Epsilon);
    //                }
    //                else if (Input.BlackTargets[g].Contains(targetId))
    //                {
    //                    blackNum += 1;
    //                    cr = BuilderParameters.BLACK_R;
    //                }
    //                else
    //                {
    //                    falseNum += 1;
    //                    cr = BuilderParameters.NEG_R;
    //                }

    //                StatusData st = Input.StatusPath[t];
    //                st.Term.Deriv.Data.MemPtr[g] = cr * (1 - st.Term.Output.Data.MemPtr[g]);
    //                for (int pt = t - 1; pt >= 0; pt--)
    //                {
    //                    StatusData pst = Input.StatusPath[pt];
    //                    if (pst.Term != null)
    //                    {
    //                        pst.Term.Deriv.Data.MemPtr[g] = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt) * cr * (-pst.Term.Output.Data.MemPtr[g]);
    //                    }
    //                }

    //                for (int pt = t - 1; pt >= 0; pt--)
    //                {
    //                    StatusData pst = Input.StatusPath[pt];
    //                    int sidx = st.PreSelActIndex[g]; // .SelectedAction[bidx].Item2;
    //                    pst.MatchCandidateProb.Deriv.MemPtr[sidx] = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt) * cr;
    //                    st = pst;
    //                }
    //            }
                
    //            //average ground truth results.
    //            ObjectiveScore = targetNum / (targetNum + falseNum + float.Epsilon);

    //            for (int i = Input.StatusPath.Count - 1; i >= 0; i--)
    //            {
    //                StatusData st = Input.StatusPath[i];
    //                if (st.Term != null) st.Term.Deriv.Data.SyncFromCPU();
    //                if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
    //            }
    //        }
    //    }



    //    /// <summary>
    //    /// take action.
    //    /// </summary>
    //    class ActionSamplingRunner : BasicPolicyRunner
    //    {
    //        /// <summary>
    //        /// match candidate, and match probability.
    //        /// </summary>
    //        /// <param name="input"></param>
    //        /// <param name="behavior"></param>
    //        public ActionSamplingRunner(StatusData input, RunnerBehavior behavior) : base(input, behavior)
    //        {
    //            MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
    //            {
    //                MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
    //                MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
    //                MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
    //            }, behavior.Device);
    //        }

    //        public override void Forward()
    //        {
    //            NodeIndex.Clear();
    //            LogProb.Clear();
    //            PreSelActIndex.Clear();
    //            PreStatusIndex.Clear();
    //            StatusKey.Clear();

    //            Input.MatchCandidateProb.Output.SyncToCPU();
    //            if (Input.Term != null) Input.Term.Output.SyncToCPU();

    //            int currentStep = Input.Step;
    //            List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

    //            for (int i = 0; i < Input.MatchCandidateProb.Segment; i++)
    //            {
    //                int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
    //                int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

    //                string Qkey = Input.GetStatusKey(i);

    //                int selectIdx = s;

    //                float[] policy_pi = null;

    //                int actionDim = e - s;
    //                if (Input.Term != null)
    //                {
    //                    policy_pi = new float[actionDim + 1];
    //                    Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, policy_pi, 0, actionDim);

    //                    float t = Util.Logistic(Input.Term.Output.Data.MemPtr[i]);
    //                    if (t >= BuilderParameters.T_MAX_CLIP) { t = BuilderParameters.T_MAX_CLIP; }
    //                    if (t <= BuilderParameters.T_MIN_CLIP) { t = BuilderParameters.T_MIN_CLIP; }

    //                    policy_pi[actionDim] = t;

    //                    for (int a = 0; a < actionDim; a++) { policy_pi[a] = policy_pi[a] * (1 - t); }
    //                }
    //                else
    //                {
    //                    policy_pi = new float[actionDim];
    //                    Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, policy_pi, 0, actionDim);
    //                }

    //                int idx = 0;
    //                //float r = (float)SampleRandom.NextDouble();

    //                int origin_b = Input.GetOriginalStatsIndex(i);

    //                // draw random sample actions.
    //                if (Input.GraphQuery.RawStrategy[origin_b] == 1) { idx = Util.Sample(policy_pi, SampleRandom); }                    // draw random actions.
    //                else
    //                {
    //                    if (policy_pi.Length > actionDim)
    //                    {
    //                        float tf = (float)SampleRandom.NextDouble();
    //                        if (tf > BuilderParameters.RAND_TERM) { idx = actionDim; }
    //                        else idx = SampleRandom.Next(0, actionDim);
    //                    }
    //                    else
    //                    {
    //                        idx = SampleRandom.Next(0, actionDim);
    //                    }
    //                }

    //                selectIdx = s + idx;

    //                if (idx == actionDim)
    //                {
    //                    Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, i));
    //                }
    //                else
    //                {
    //                    PreSelActIndex.Add(selectIdx);
    //                    PreStatusIndex.Add(i);

    //                    string statusQ = string.Format("{0}-N:{1}-R:{2}", Qkey, Input.MatchCandidate[selectIdx].Item1, Input.MatchCandidate[selectIdx].Item2);
    //                    NodeIndex.Add(Input.MatchCandidate[selectIdx].Item1);

    //                    StatusKey.Add(statusQ);

    //                    float nonLogTerm = 0;
    //                    if (Input.Term != null) { nonLogTerm = Input.LogPTerm(i, false); }

    //                    float prob = Input.GetLogProb(i) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
    //                    LogProb.Add(prob);

    //                    match.Add(new Tuple<int, int, float>(i, selectIdx, 1));
    //                }
    //            }
    //            MatchPath.SetMatch(match);
    //        }
    //    }

    //    class BeamSearchActionRunner : BasicPolicyRunner
    //    {
    //        int BeamSize = 1;

    //        /// <summary>
    //        /// group aware beam search.
    //        /// </summary>
    //        /// <param name="input"></param>
    //        /// <param name="behavior"></param>
    //        public BeamSearchActionRunner(StatusData input, int beamSize, RunnerBehavior behavior) : base(input, behavior)
    //        {
    //            BeamSize = beamSize;
    //            MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
    //            {
    //                MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
    //                MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
    //                MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
    //            }, behavior.Device);
    //            //GroupIndex = new List<int>();
    //        }

    //        public override void Forward()
    //        {
    //            if (Input.MatchCandidateProb.Segment != Input.BatchSize)
    //            {
    //                throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
    //                                Input.MatchCandidateProb.Segment, Input.BatchSize));
    //            }

    //            Input.MatchCandidateProb.Output.SyncToCPU();
    //            if (Input.Term != null) Input.Term.Output.SyncToCPU();

    //            NodeIndex.Clear();
    //            LogProb.Clear();
    //            PreSelActIndex.Clear();
    //            PreStatusIndex.Clear();
    //            StatusKey.Clear();

    //            List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
    //            // for each beam.
    //            for (int i = 0; i < Input.GraphQuery.BatchSize; i++) //.MatchCandidateProb.Segment; i++)
    //            {
    //                List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);

    //                MinMaxHeap<Tuple<int, int>> topKheap = new MinMaxHeap<Tuple<int, int>>(BeamSize, 1);

    //                foreach (int b in idxs)
    //                {
    //                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[b];
    //                    int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

    //                    float nonLogTerm = 0;
    //                    if (Input.Step > 0)
    //                    {
    //                        nonLogTerm = Input.LogPTerm(b, false);
    //                        Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, b));
    //                    }

    //                    for (int t = s; t < e; t++)
    //                    {
    //                        float prob = Input.GetLogProb(b) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t]);
    //                        topKheap.push_pair(new Tuple<int, int>(b, t), prob);
    //                    }

    //                    //string Qkey = Input.GetStatusKey(b);

    //                    //if (Input.Term != null)
    //                    //{
    //                    //    //float p_t = Input.LogPTerm(i, true);
    //                    //    //float p_n = Input.LogPTerm(i, false);  
    //                    //    float t = Util.Logistic(Input.Term.Output.Data.MemPtr[i]);
    //                    //    if (t >= BuilderParameters.T_MAX_CLIP) { t = BuilderParameters.T_MAX_CLIP; }
    //                    //    if(t <= BuilderParameters.T_MIN_CLIP) { t = BuilderParameters.T_MIN_CLIP; }
    //                    //    //topKheap.push_pair(new Tuple<int, int>(b, -1), Input.GetLogProb(b) + (float)Math.Log(t));
    //                    //    for (int c = s; c < e; c++)
    //                    //    {
    //                    //        topKheap.push_pair(new Tuple<int, int>(b, c),
    //                    //            Input.GetLogProb(b) + (float)Math.Log(1 - t) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[c]));
    //                    //    }
    //                    //    Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, b));
    //                    //}
    //                    //else
    //                    //{
    //                    //    for (int c = s; c < e; c++)
    //                    //    {
    //                    //        topKheap.push_pair(new Tuple<int, int>(b, c),
    //                    //            Input.GetLogProb(b) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[c]));
    //                    //    }
    //                    //}

    //                }

    //                while (!topKheap.IsEmpty)
    //                {
    //                    KeyValuePair<Tuple<int, int>, float> p = topKheap.PopTop();
    //                    match.Add(new Tuple<int, int, float>(p.Key.Item1, p.Key.Item2, p.Value));

    //                    NodeIndex.Add(Input.MatchCandidate[p.Key.Item2].Item1);
    //                    LogProb.Add(p.Value);

    //                    PreStatusIndex.Add(p.Key.Item1);
    //                    PreSelActIndex.Add(p.Key.Item2);

    //                    string statusQ = string.Format("{0}-N:{1}-R:{2}", Input.GetStatusKey(p.Key.Item1), Input.MatchCandidate[p.Key.Item2].Item1, Input.MatchCandidate[p.Key.Item2].Item2);

    //                    StatusKey.Add(statusQ);
    //                }
    //            }
    //            MatchPath.SetMatch(match);
    //        }
    //    }

    //    /// <summary>
    //    /// it is a little difficult.
    //    /// </summary>
    //    class RewardRunner : ObjectiveRunner
    //    {
    //        Random random = new Random(13);
    //        new GraphQueryData Input { get; set; }
    //        public RewardRunner(GraphQueryData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Input = input;
    //        }

    //        Dictionary<int, float> Success = new Dictionary<int, float>();
    //        Dictionary<int, float> StepSuccess = new Dictionary<int, float>();
    //        public override void Init()
    //        {
    //            Success.Clear();
    //            StepSuccess.Clear();
    //        }

    //        public override void Complete()
    //        {
    //            foreach(KeyValuePair<int, float> i in StepSuccess)
    //            {
    //                Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
    //            }

    //            int sc =  Success.Where(i => i.Value > 0).Count();
    //            int t = Success.Count;
    //            Logger.WriteLog("Success validation rate {0}", sc * 1.0f / t);
    //        }

    //        public override void Forward()
    //        {
    //            // calculate termination probability and clear derivation.
    //            for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
    //            {
    //                if (Input.StatusPath[t].Term != null)
    //                {
    //                    ComputeLib.Logistic(Input.StatusPath[t].Term.Output.Data, 0, Input.StatusPath[t].Term.Output.Data, 0, Input.BatchSize, 1);
    //                    ComputeLib.ClipVector(Input.StatusPath[t].Term.Output.Data, Input.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);
    //                    Input.StatusPath[t].Term.Output.Data.SyncToCPU();
    //                    Array.Clear(Input.StatusPath[t].Term.Deriv.Data.MemPtr, 0, Input.StatusPath[t].BatchSize);
    //                }
    //                if (Input.StatusPath[t].MatchCandidateProb != null)
    //                {
    //                    Array.Clear(Input.StatusPath[t].MatchCandidateProb.Deriv.MemPtr, 0, Input.StatusPath[t].MatchCandidateProb.Length);
    //                }
    //            }

    //            //HashSet<int> finishedBatchSize = new HashSet<int>();
    //            //for (int i = 0; i < Input.Results.Count; i++)
    //            //{
    //            //    int b = Input.Results[i].Item2;
    //            //    if (!finishedBatchSize.Contains(b)) { finishedBatchSize.Add(b); }
    //            //}


    //            // add last step nodes.
    //            //if (Input.StatusPath.Count == BuilderParameters.MAX_HOP + 1)
    //            //{
    //            for (int b = 0; b < Input.StatusPath[BuilderParameters.MAX_HOP].BatchSize; b++)
    //            {
    //                //if (!finishedBatchSize.Contains(b))
    //                //{
    //                Input.Results.Add(new Tuple<int, int>(BuilderParameters.MAX_HOP, b));
    //                //}
    //            }
    //            //}


    //            ObjectiveDict = new Dictionary<string, double>();
    //            float posReward = 0;
    //            float negReward = 0;

    //            // calculate the results reward.
    //            for (int i = 0; i < Input.Results.Count; i++)
    //            {
    //                // termination step.
    //                int t = Input.Results[i].Item1;

    //                // batch index.
    //                int b = Input.Results[i].Item2;


    //                string mkey = string.Format("Term {0}", t);
    //                if (!ObjectiveDict.ContainsKey(mkey)) { ObjectiveDict[mkey] = 0; }
    //                ObjectiveDict[mkey] += 1;

    //                int origialB = Input.StatusPath[t].GetOriginalStatsIndex(b);
    //                int predId = Input.StatusPath[t].NodeID[b];

    //                int targetId = Input.RawTarget[origialB];

    //                int rawIdx = Input.RawIndex[origialB];
                    
    //                float cr = 0;
    //                if (predId == targetId)
    //                {
    //                    cr = BuilderParameters.POS_R;
    //                    posReward += 1;
    //                }
    //                else if (Input.BlackTargets[origialB].Contains(predId))
    //                {
    //                    cr = BuilderParameters.BLACK_R;
    //                }
    //                else
    //                {
    //                    cr = BuilderParameters.NEG_R;
    //                    negReward += 1;
    //                }


    //                if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
    //                Success[rawIdx] += cr;

    //                if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = 0;
    //                StepSuccess[t] += cr;


    //                StatusData st = Input.StatusPath[t];
    //                //if (t != BuilderParameters.MAX_HOP)
    //                st.Term.Deriv.Data.MemPtr[b] = cr * (1 - st.Term.Output.Data.MemPtr[b]);

    //                for (int pt = t - 1; pt >= 0; pt--)
    //                {
    //                    int pb = st.GetPreStatusIndex(b);
    //                    int sidx = st.PreSelActIndex[b];// - pst.GetActionStartIndex(pb);
    //                    StatusData pst = Input.StatusPath[pt];
    //                    if (pst.Term != null)
    //                    {
    //                        pst.Term.Deriv.Data.MemPtr[pb] = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt) * cr * (-pst.Term.Output.Data.MemPtr[pb]);
    //                    }
    //                    pst.MatchCandidateProb.Deriv.MemPtr[sidx] = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt) * cr;

    //                    st = pst;
    //                    b = pb;
    //                }
    //            }


    //            ObjectiveScore = posReward / (posReward + negReward + float.Epsilon);

    //            for (int i = Input.StatusPath.Count - 1; i >= 0; i--)
    //            {
    //                StatusData st = Input.StatusPath[i];
    //                if (st.Term != null) st.Term.Deriv.Data.SyncFromCPU();
    //                if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
    //            }

    //            Input.Results.Clear();
    //        }
    //    }

    //    /// <summary>
    //    /// REINFORCE-WALK prediction Runner.
    //    /// </summary>
    //    class TopKPredictionRunner : StructRunner
    //    {
    //        int lsum = 0, lsum_filter = 0;
    //        int rsum = 0, rsum_filter = 0;

    //        float _lsum = 0, _lsum_filter = 0;
    //        float _rsum = 0, _rsum_filter = 0;

    //        int lp_n = 0, lp_n_filter = 0;
    //        int rp_n = 0, rp_n_filter = 0;
    //        int HitK = 10;

    //        int Iteration = 0;
    //        int SmpIdx = 0;
    //        GraphQueryData Query { get; set; }

    //        public TopKPredictionRunner(GraphQueryData query, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Query = query;
    //            Iteration = 0;
    //        }

    //        public override void Init()
    //        {
    //            SmpIdx = 0;
    //            lsum = 0;
    //            lsum_filter = 0;
    //            _lsum = 0;
    //            _lsum_filter = 0;

    //            rsum = 0;
    //            rsum_filter = 0;
    //            _rsum = 0;
    //            _rsum_filter = 0;

    //            lp_n = 0;
    //            lp_n_filter = 0;
    //            rp_n = 0;
    //            rp_n_filter = 0;
    //        }

    //        public void Report()
    //        {
    //            Logger.WriteLog("Sample Idx {0}", SmpIdx);

    //            Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Left Mean Re Rank {0}", _lsum * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Left Filter Mean Re Rank {0}", _lsum_filter * 2.0 / SmpIdx));

    //            Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Right Mean Re Rank {0}", _rsum * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Right Filter Mean Re Rank {0}", _rsum_filter * 2.0 / SmpIdx));

    //            Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 1.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 1.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 1.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 1.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Overall Mean Re Rank {0}", (_lsum + _rsum) * 1.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Overall Filter Mean Re Rank {0}", (_lsum_filter + _rsum_filter) * 1.0 / SmpIdx));

    //        }

    //        public override void Complete()
    //        {
    //            Logger.WriteLog("Final Report!");
    //            {
    //                Report();
    //            }
    //            Iteration += 1;
    //        }
    //        public override void Forward()
    //        {
    //            Dictionary<int, float>[] scoreDict = new Dictionary<int, float>[Query.BatchSize];
    //            for (int i = 0; i < Query.BatchSize; i++)
    //            {
    //                scoreDict[i] = new Dictionary<int, float>();
    //            }

    //            // add last step nodes.
    //            if (Query.StatusPath.Count == BuilderParameters.MAX_HOP + 1)
    //            {
    //                for (int b = 0; b < Query.StatusPath[BuilderParameters.MAX_HOP].BatchSize; b++)
    //                {
    //                    Query.Results.Add(new Tuple<int, int>(BuilderParameters.MAX_HOP, b));
    //                }
    //            }

    //            for (int t = Query.StatusPath.Count - 1; t >= 0; t--)
    //            {
    //                if (Query.StatusPath[t].Term != null) { Query.StatusPath[t].Term.Output.Data.SyncToCPU(); }
    //            }

    //            for (int i = 0; i < Query.Results.Count; i++)
    //            {
    //                int t = Query.Results[i].Item1;
    //                int b = Query.Results[i].Item2;
    //                int origialB = Query.StatusPath[t].GetOriginalStatsIndex(b);
    //                int predId = Query.StatusPath[t].NodeID[b];

    //                int targetId = Query.RawTarget[origialB];

    //                float v = Query.StatusPath[t].GetLogProb(b);

    //                //if (t != BuilderParameters.MAX_HOP)
    //                { v = v + (float)Query.StatusPath[t].LogPTerm(b, true); }

    //                if (!scoreDict[origialB].ContainsKey(predId))
    //                {
    //                    scoreDict[origialB][predId] = v;
    //                }
    //                else
    //                {
    //                    scoreDict[origialB][predId] = Util.LogAdd(scoreDict[origialB][predId], v);
    //                }
    //            }


    //            for (int g = 0; g < Query.BatchSize; g++)
    //            {
    //                int relation = Query.RawQuery[g].Item2;
    //                int target = Query.RawTarget[g];
    //                HashSet<int> blackTargets = Query.BlackTargets[g];

    //                Dictionary<int, float> score = scoreDict[g];
    //                var sortD = score.OrderByDescending(pair => pair.Value);

    //                //Console.WriteLine("Sample {0}", g);
    //                //foreach (KeyValuePair<int, float> v in sortD)
    //                //{
    //                //    Console.WriteLine("{0}:{1}", v.Key, v.Value);
    //                //}

    //                int filter = 1;
    //                int nonFilter = 1;
    //                bool isFound = false;
    //                foreach (KeyValuePair<int, float> d in sortD)
    //                {
    //                    int ptail = d.Key;
    //                    if (ptail == target)
    //                    {
    //                        isFound = true;
    //                        break;
    //                    }
    //                    nonFilter += 1;
    //                    if (!blackTargets.Contains(ptail)) { filter++; }
    //                }

    //                if (!isFound)
    //                {
    //                    int tail = (DataPanel.EntityNum - nonFilter);
    //                    int topBlack = nonFilter - filter;
    //                    int tailBlack = blackTargets.Count - topBlack;
    //                    nonFilter = nonFilter + tail / 2;
    //                    filter = filter + (tail - tailBlack) / 2;
    //                }


    //                if (relation < DataPanel.RelationNum)
    //                {
    //                    Interlocked.Add(ref lsum, nonFilter);
    //                    Interlocked.Add(ref lsum_filter, filter);

    //                    _lsum += 1.0f / (float)nonFilter;
    //                    _lsum_filter += 1.0f / filter;

    //                    if (nonFilter <= HitK) Interlocked.Increment(ref lp_n); // lp_n += 1;
    //                    if (filter <= HitK) Interlocked.Increment(ref lp_n_filter); // += 1;
    //                }
    //                else
    //                {
    //                    Interlocked.Add(ref rsum, nonFilter);
    //                    Interlocked.Add(ref rsum_filter, filter);

    //                    _rsum += 1.0f / (float)nonFilter;
    //                    _rsum_filter += 1.0f / filter;

    //                    if (nonFilter <= HitK) Interlocked.Increment(ref rp_n); // lp_n += 1;
    //                    if (filter <= HitK) Interlocked.Increment(ref rp_n_filter); // += 1;
    //                }
    //            }

    //            SmpIdx += Query.BatchSize;
    //            Query.Results.Clear();
    //            //Report();
    //        }
    //    }

    //    public class NeuralWalkerModel : CompositeNNStructure
    //    {
    //        public EmbedStructure InNodeEmbed { get; set; }
    //        public EmbedStructure InRelEmbed { get; set; }

    //        public EmbedStructure CNodeEmbed { get; set; }
    //        public EmbedStructure CRelEmbed { get; set; }

    //        public EmbedStructure PosEmbed { get; set; }

    //        public DNNStructure SrcDNN { get; set; }
    //        public DNNStructure TgtDNN { get; set; }

    //        public LayerStructure AttEmbed { get; set; }
    //        public DNNStructure TermDNN { get; set; }

    //        public GRUCell GruCell { get; set; }

    //        public int NodeDim { get; set; }
    //        public int RelDim { get; set; }
    //        public int PosDim { get; set; }

    //        public int StatusDim { get { return NodeDim + RelDim + PosDim; } }

    //        public NeuralWalkerModel(int nodeDim, int relDim, int posDim, DeviceType device)
    //        {
    //            NodeDim = nodeDim;
    //            RelDim = relDim;
    //            PosDim = posDim;

    //            InNodeEmbed = AddLayer(new EmbedStructure(DataPanel.EntityNum, nodeDim, device)); // DeviceType.CPU_FAST_VECTOR));
    //            InRelEmbed = AddLayer(new EmbedStructure(DataPanel.RelationNum * 2, relDim, device)); // DeviceType.CPU_FAST_VECTOR));

    //            CNodeEmbed = AddLayer(new EmbedStructure(DataPanel.EntityNum, nodeDim, device));// DeviceType.CPU_FAST_VECTOR));
    //            CRelEmbed = AddLayer(new EmbedStructure(DataPanel.RelationNum * 2, relDim, device)); // DeviceType.CPU_FAST_VECTOR));

    //            if (posDim > 0)
    //            {
    //                PosEmbed = AddLayer(new EmbedStructure(BuilderParameters.MAX_HOP + 1, posDim, device));
    //            }

    //            SrcDNN = AddLayer(new DNNStructure(StatusDim, BuilderParameters.DNN_DIMS,
    //                                       BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
    //                                       BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
    //                                       device));

    //            TgtDNN = AddLayer(new DNNStructure(StatusDim, BuilderParameters.DNN_DIMS,
    //                                       BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
    //                                       BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
    //                                       device));

    //            AttEmbed = AddLayer(new LayerStructure(BuilderParameters.DNN_DIMS.Last(), 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device));


    //            TermDNN = AddLayer(new DNNStructure(StatusDim, BuilderParameters.T_NET, BuilderParameters.T_AF,
    //                                     BuilderParameters.T_NET.Select(i => true).ToArray(), device));

    //            GruCell = AddLayer(new GRUCell(StatusDim, StatusDim, device));

    //        }
    //        public NeuralWalkerModel(BinaryReader reader, DeviceType device)
    //        {
    //            int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

    //            InNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
    //            InRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);

    //            CNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
    //            CRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);

    //            if(BuilderParameters.POS_EMBED_DIM > 0)
    //            {
    //                PosEmbed = (EmbedStructure)DeserializeNextModel(reader, device);
    //            }

    //            SrcDNN = (DNNStructure)DeserializeNextModel(reader, device);
    //            TgtDNN = (DNNStructure)DeserializeNextModel(reader, device);

    //            AttEmbed = (LayerStructure)DeserializeNextModel(reader, device);
    //            TermDNN = (DNNStructure)DeserializeNextModel(reader, device);
    //            GruCell = (GRUCell)DeserializeNextModel(reader, device);

    //            NodeDim = BuilderParameters.N_EmbedDim;
    //            RelDim = BuilderParameters.R_EmbedDim;
    //            PosDim = BuilderParameters.POS_EMBED_DIM;
    //        }

    //        public void InitOptimization(RunnerBehavior behavior)
    //        {
    //            //InNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
    //            //InRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

    //            //CNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
    //            //CRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

    //            InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
    //        }
    //    }

    //    public static ComputationGraph BuildComputationGraph(List<Tuple<int, int, int>> graph, int batchSize,
    //                                                         NeuralWalkerModel model, RunnerBehavior Behavior)
    //    {
    //        ComputationGraph cg = new ComputationGraph();

    //        // sample a list of tuplet.
    //        SampleRunner SmpRunner = new SampleRunner(graph, Behavior.RunMode == DNNRunMode.Predict? 1: BuilderParameters.TRAIN_GROUP, batchSize, Behavior);
    //        cg.AddDataRunner(SmpRunner);
    //        GraphQueryData interface_data = SmpRunner.Output;

    //        StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, 0, model.InNodeEmbed, model.InRelEmbed, model.PosEmbed, Behavior);
    //        cg.AddRunner(statusEmbedRunner);

    //        StatusData status = new StatusData(interface_data, interface_data.RawSource, statusEmbedRunner.Output, Behavior.Device);
    //        #region multi-hop expan
    //        // travel four steps in the knowledge graph.
    //        for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
    //        {
    //            //given status, obtain the match. miniBatch * maxNeighbor number.
    //            CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
    //            cg.AddRunner(candidateActionRunner);

    //            // miniMatch * maxNeighborNumber.
    //            StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, i + 1, model.CNodeEmbed, model.CRelEmbed, model.PosEmbed, Behavior);
    //            cg.AddRunner(candEmbedRunner);

    //            DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
    //            cg.AddRunner(srcHiddenRunner);

    //            DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
    //            cg.AddRunner(candHiddenRunner);

    //            // alignment miniBatch * miniBatch * neighbor
    //            VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
    //                                                                        candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
    //            cg.AddRunner(attentionRunner);

    //            SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
    //                                                                        status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
    //                                                                        candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
    //                                                                        Behavior.Device), 1, Behavior,
    //                                                                        true);
    //            cg.AddRunner(normAttRunner);

    //            status.MatchCandidate = candidateActionRunner.Output;
    //            status.MatchCandidateProb = normAttRunner.Output;
    //            //it will cause un-necessary unstable.

    //            BasicPolicyRunner policyRunner = null;

    //            if (Behavior.RunMode == DNNRunMode.Train)
    //            {
    //                if (BuilderParameters.VERSION == 0)
    //                {
    //                    policyRunner = new OldActionSamplingRunner(status, Behavior); // ActionSamplingRunner(status, Behavior);
    //                }
    //                else
    //                {
    //                    policyRunner = new ActionSamplingRunner(status, Behavior);
    //                }
    //            }
    //            else
    //            {
    //                policyRunner = new BeamSearchActionRunner(status, BuilderParameters.BeamSize, Behavior);
    //            }
    //            cg.AddRunner(policyRunner);

    //            #region version 2 of action selection.
    //            MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
    //            cg.AddRunner(srcExpRunner);

    //            MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, policyRunner.MatchPath, 2, Behavior);
    //            cg.AddRunner(tgtExpRunner);

    //            //StatusEmbedRunner actionEmbedRunner = new StatusEmbedRunner(actionRunner.SelectAction, status.MaxBatchSize, model.CNodeEmbed, model.CRelEmbed, Behavior);
    //            //cg.AddRunner(actionEmbedRunner);
    //            GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
    //            cg.AddRunner(stateRunner);
    //            #endregion.

    //            #region version 1 of action selection.
    //            //StatusEmbedRunner actionEmbedRunner = new StatusEmbedRunner(actionRunner.Action, status.MaxBatchSize, model.CNodeEmbed, model.CRelEmbed, Behavior);
    //            //cg.AddRunner(actionEmbedRunner);
    //            //GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, status.StateEmbed, actionEmbedRunner.Output, Behavior);
    //            //cg.AddRunner(stateRunner);
    //            #endregion.

    //            StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.LogProb,
    //                policyRunner.PreSelActIndex, policyRunner.PreStatusIndex, policyRunner.StatusKey, stateRunner.Output, Behavior.Device);
    //            //StatusData nextStatus = new StatusData(status.GraphQuery, actionRunner.NodeIndex, actionRunner.GroupIndex, actionRunner.LogProb, actionRunner.NodeAction, stateRunner.Output, Behavior.Device);

    //            DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, nextStatus.StateEmbed, Behavior);
    //            cg.AddRunner(termRunner);
    //            nextStatus.Term = termRunner.Output;
    //            status = nextStatus;
    //        }
    //        #endregion.

    //        if (Behavior.RunMode == DNNRunMode.Train)
    //        {
    //            if (BuilderParameters.VERSION == 0)
    //            {
    //                OldRewardRunner rewardRunner = new OldRewardRunner(interface_data, Behavior);
    //                cg.AddObjective(rewardRunner);
    //            }
    //            else
    //            {
    //                RewardRunner rewardRunner = new RewardRunner(interface_data, Behavior);
    //                cg.AddObjective(rewardRunner);
    //            }
    //        }
    //        else
    //        {
    //            TopKPredictionRunner predRunner = new TopKPredictionRunner(interface_data, Behavior);
    //            cg.AddRunner(predRunner);
    //        }
    //        cg.SetDelegateModel(model);
    //        return cg;
    //    }


    //    public override void Rock()
    //    {
    //        Logger.OpenLog(BuilderParameters.LogFile);

    //        DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
    //        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

    //        Logger.WriteLog("Loading Training/Validation/Test Data.");
    //        DataPanel.Init();
    //        Logger.WriteLog("Load Data Finished.");


    //        NeuralWalkerModel model =
    //            BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
    //            new NeuralWalkerModel(BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, BuilderParameters.POS_EMBED_DIM, device) :
    //            new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);


    //        model.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
    //        switch (BuilderParameters.RunMode)
    //        {
    //            case DNNRunMode.Train:
    //                if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

    //                ComputationGraph trainCG = BuildComputationGraph(
    //                    BuilderParameters.TRAIN_DEBUG == 0 ? DataPanel.knowledgeGraph.Valid : DataPanel.knowledgeGraph.Train,
    //                    BuilderParameters.MiniBatchSize, model, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

    //                ComputationGraph predCG = BuildComputationGraph(DataPanel.knowledgeGraph.Test, BuilderParameters.TestMiniBatchSize, model,
    //                                   new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

    //                for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
    //                {
    //                    //ActionSampler = new Random(10);
    //                    double loss = trainCG.Execute();
    //                    Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

    //                    if ((iter + 1) % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
    //                    {
    //                        Logger.WriteLog("Evaluation at Iteration {0}", iter);

    //                        predCG.Execute();
    //                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "model." + iter.ToString(), FileMode.Create, FileAccess.Write)))
    //                        {
    //                            model.Serialize(writer);
    //                        }
    //                    }
    //                }
    //                break;
    //            case DNNRunMode.Predict:
    //                ComputationGraph testCG = BuildComputationGraph(DataPanel.knowledgeGraph.Test, BuilderParameters.TestMiniBatchSize, model,
    //                                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
    //                testCG.Execute();
    //                break;

    //        }
    //        Logger.CloseLog();
    //    }

    //    /// <summary>
    //    /// load data.
    //    /// </summary>
    //    public class DataPanel
    //    {
    //        public static RelationGraphData knowledgeGraph;

    //        public static int EntityNum { get { return knowledgeGraph.EntityNum; } }
    //        public static int RelationNum { get { return knowledgeGraph.RelationNum; } }

    //        public static int MaxNeighborCount { get { return knowledgeGraph.TrainNeighborLink.Select(i => i.Value.Count).Max(); } }

    //        public static void Init()
    //        {
    //            knowledgeGraph = new RelationGraphData();
    //            knowledgeGraph.EntityNum = BuilderParameters.EntityNum;
    //            knowledgeGraph.RelationNum = BuilderParameters.RelationNum;

    //            knowledgeGraph.Train = knowledgeGraph.LoadGraphV3(BuilderParameters.TrainData, 0);
    //            knowledgeGraph.Valid = knowledgeGraph.LoadGraphV3(BuilderParameters.ValidData, 1);
    //            knowledgeGraph.Test = knowledgeGraph.LoadGraphV3(BuilderParameters.TestData, 2);

    //            Console.WriteLine("Entity Number {0}, Relation Number {1}", knowledgeGraph.EntityNum, knowledgeGraph.RelationNum);
    //        }
    //    }
    //}
}
