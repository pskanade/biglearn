﻿//using ScopeML;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//using BigLearn;
//namespace BigLearn.DeepNet
//{
//    public class AppKBCReinforceWalkV2_Builder : Builder
//    {
//        /// <summary>
//        /// 
//        /// </summary>
//        public class BuilderParameters : BaseModelArgument<BuilderParameters>
//        {
//            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
//            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

//            #region Input Data Argument.
//            public static string Entity2Id { get { return Argument["ENTITY2ID"].Value; } }
//            public static int EntityNum { get { return int.Parse(Argument["ENTITYNUM"].Value); } }

//            public static string Relation2Id { get { return Argument["RELATION2ID"].Value; } }
//            public static int RelationNum { get { return int.Parse(Argument["RELATIONNUM"].Value); } }

//            public static string TrainData { get { return Argument["TRAIN"].Value; } }
//            public static string ValidData { get { return Argument["VALID"].Value; } }
//            public static string TestData { get { return Argument["TEST"].Value; } }
//            public static bool IsTrainFile { get { return !(TrainData.Equals(string.Empty)); } }
//            public static bool IsValidFile { get { return !(ValidData.Equals(string.Empty)); } }
//            public static bool IsTestFile { get { return !(TestData.Equals(string.Empty)); } }
//            public static string ValidPathFolder { get { return Argument["VALID-PATH"].Value; } }
//            #endregion.

//            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }
//            public static int TrainBeamSize { get { return int.Parse(Argument["TRAIN-BEAM-SIZE"].Value); } }
//            public static int TrainSetting { get { return int.Parse(Argument["TRAIN-SETTING"].Value); } }

//            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
//            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

//            public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
//            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
//            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            
//            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

//            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
//            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

//            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }

//            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
//            public static float T_MAX_CLIP { get { return float.Parse(Argument["T-MAX-CLIP"].Value); } }
//            public static float T_MIN_CLIP { get { return float.Parse(Argument["T-MIN-CLIP"].Value); } }

//            public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
//            public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

//            public static float GUID_RATE { get { return float.Parse(Argument["GUID-RATE"].Value); } }
//            public static float RAND_RATE { get { return float.Parse(Argument["RAND-RATE"].Value); } }

//            public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
//            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
//            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }

//            public static float NORM_REWARD { get { return float.Parse(Argument["NORM-REWARD"].Value); } }

//            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

//            public static int TRAIN_DEBUG { get { return int.Parse(Argument["TRAIN-DEBUG"].Value); } }


//            static BuilderParameters()
//            {
//                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
//                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

//                #region Input Data Argument.
//                Argument.Add("ENTITYNUM", new ParameterArgument("0", "Entity Number."));
//                Argument.Add("RELATIONNUM", new ParameterArgument("0", "Relation Number."));

//                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
//                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
//                Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));
//                #endregion.

//                Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
//                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
//                Argument.Add("DNN-DIMS", new ParameterArgument("100,100","DNN Map Dimensions."));

//                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));
//                Argument.Add("TRAIN-BEAM-SIZE", new ParameterArgument("10", "beam size."));

//                Argument.Add("TRAIN-SETTING", new ParameterArgument("0", "0:supervised  1:reinforcement"));

//                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
//                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

//                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
//                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));

//                Argument.Add("GUID-RATE", new ParameterArgument("1", "guided path rate."));
//                Argument.Add("RAND-RATE", new ParameterArgument("10", "rand path rate."));

//                Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
//                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
//                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

//                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.9", "Reward discount"));

//                Argument.Add("T-MAX-CLIP", new ParameterArgument("0", "Maximum T Clip"));
//                Argument.Add("T-MIN-CLIP", new ParameterArgument("0", "Minimum T Clip"));

//                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

//                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

//                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score File."));
//                Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));

//                Argument.Add("NORM-REWARD", new ParameterArgument("0", "normalize reward."));
//                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));

//                Argument.Add("TRAIN-DEBUG", new ParameterArgument("0", "0:use dev set as train; 1: use train set"));
//            }
//        }

//        public override BuilderType Type { get { return BuilderType.APP_KBC_REINFORCE_WALK_V2; } }

//        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

//        public class GraphQueryData : BatchData
//        {
//            public List<int> RawSource = new List<int>();
//            public List<Tuple<int, int>> RawQuery = new List<Tuple<int, int>>();
//            public List<int> RawTarget = new List<int>();
//            public List<HashSet<int>> BlackTargets = new List<HashSet<int>>();

//            public List<int> PathIdx = new List<int>();

//            public int BatchSize { get { return RawQuery.Count; } }
//            public int MaxBatchSize { get { return MaxGroupNum * GroupSize; } }
//            public int GroupNum { get { return BatchSize / GroupSize; } }
//            public int MaxGroupNum { get; set; }
//            public int GroupSize { get; set; }
//            public List<StatusData> StatusPath { get; set; }

//            public List<Tuple<int, int>> Results = new List<Tuple<int, int>>();


//            /// <summary>
//            /// max group number, and group size;
//            /// </summary>
//            /// <param name="maxGroupNum"></param>
//            /// <param name="groupSize"></param>
//            /// <param name="device"></param>
//            public GraphQueryData(int maxGroupNum, int groupSize, DeviceType device)
//            {
//                MaxGroupNum = maxGroupNum;
//                GroupSize = groupSize;
//                StatusPath = new List<StatusData>();
//            }

//            public List<int> GetBatchIdxs(int b, int step)
//            {
//                List<int> r = new List<int>();
//                for (int i = 0; i < StatusPath[step].BatchSize; i++)
//                {
//                    if (StatusPath[step].GetOriginalStatsIndex(i) == b)
//                        r.Add(i);
//                }
//                return r;
//            }
//        }


//        /// <summary>
//        /// Sample Graph. 
//        /// </summary>
//        class SampleRunner : StructRunner
//        {
//            List<Tuple<int, int, int>> Graph { get; set; }
//            public new GraphQueryData Output { get; set; }
//            int MaxGroupNum { get; set; }
//            int GroupSize { get; set; }
//            DataRandomShuffling Shuffle { get; set; }
//            Random random = new Random(13);

//            public SampleRunner(List<Tuple<int, int, int>> graph,
//                                int groupNum, int groupSize, RunnerBehavior behavior) : base(Structure.Empty, behavior)
//            {
//                Graph = graph;
//                MaxGroupNum = groupNum;
//                GroupSize = groupSize;

//                Shuffle = new DataRandomShuffling(Graph.Count * 2, random);
//                Output = new GraphQueryData(MaxGroupNum, groupSize, behavior.Device);
//            }

//            public override void Init()
//            {
//                IsTerminate = false;
//                IsContinue = true;
//                Shuffle.Init();
//            }

//            public override void Forward()
//            {
//                Output.RawQuery.Clear();
//                Output.RawSource.Clear();
//                Output.RawTarget.Clear();
//                Output.BlackTargets.Clear();
//                Output.PathIdx.Clear();

//                int groupIdx = 0;

//                while (groupIdx < MaxGroupNum)
//                {
//                    int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext();
//                    if (idx <= -1) { break; }
//                    int srcId = idx < Graph.Count ? Graph[idx].Item1 : Graph[idx - Graph.Count].Item2;
//                    int tgtId = idx < Graph.Count ? Graph[idx].Item2 : Graph[idx - Graph.Count].Item1;
//                    int linkId = idx < Graph.Count ? Graph[idx].Item3 : (Graph[idx - Graph.Count].Item3 + DataPanel.RelationNum);

//                    int guidRange = (int)BuilderParameters.GUID_RATE;
//                    int randRange = (int)(BuilderParameters.GUID_RATE + BuilderParameters.RAND_RATE);

//                    for (int b = 0; b < GroupSize; b++)
//                    {
//                        if (Behavior.RunMode == DNNRunMode.Train)
//                        {
//                            if (b < randRange)
//                            {
//                                //random draw path.
//                                Output.PathIdx.Add(-2);
//                            }
//                            else
//                            {
//                                //sample path.
//                                Output.PathIdx.Add(-1);
//                            }
//                        }
//                        else
//                        {
//                            Output.PathIdx.Add(-1);
//                        }
//                        Output.RawQuery.Add(new Tuple<int, int>(srcId, linkId));
//                        Output.RawSource.Add(srcId);
//                        Output.RawTarget.Add(tgtId);

//                        HashSet<int> blackTarget = null;

//                        if (Behavior.RunMode == DNNRunMode.Train)
//                        {
//                            blackTarget = DataPanel.knowledgeGraph.TrainNeighborHash[srcId].ContainsKey(linkId) ?
//                                                    DataPanel.knowledgeGraph.TrainNeighborHash[srcId][linkId] : new HashSet<int>();
//                        }
//                        else if (Behavior.RunMode == DNNRunMode.Predict)
//                        {
//                            blackTarget = DataPanel.knowledgeGraph.AllNeighborHash[srcId].ContainsKey(linkId) ?
//                                                    DataPanel.knowledgeGraph.AllNeighborHash[srcId][linkId] : new HashSet<int>();
//                        }
//                        Output.BlackTargets.Add(blackTarget);
//                    }
//                    groupIdx += 1;
//                }
//                if (groupIdx == 0) { IsTerminate = true; return; }
//            }
//        }


//        public class StatusData : BatchData
//        {
//            /// <summary>
//            /// Raw Query.
//            /// </summary>
//            public GraphQueryData GraphQuery { get; set; }
//            /// <summary>
//            /// Node ID.
//            /// </summary>
//            public List<int> NodeID { get; set; }
//            /// <summary>
//            /// Termination Probability.
//            /// </summary>
//            public HiddenBatchData Term = null;
//            /// <summary>
//            /// LogProbability to this node.
//            /// </summary>
//            List<float> LogProb { get; set; }
//            /// <summary>
//            /// MatchCandidate and MatchCandidateProb.
//            /// </summary>
//            public List<Tuple<int, int>> MatchCandidate = null;
//            public SeqVectorData MatchCandidateProb = null;


//            public List<int> PreSelActIndex = null;
//            public List<int> PreStatusIndex = null;
//            public int GetPreStatusIndex(int batchIdx)
//            {
//                if (Step == 0) { return batchIdx; }
//                else return PreStatusIndex[batchIdx];
//            }
//            public int GetOriginalStatsIndex(int batchIdx)
//            {
//                if (Step == 0) { return batchIdx; }
//                else
//                {
//                    int b = GetPreStatusIndex(batchIdx);
//                    return GraphQuery.StatusPath[Step - 1].GetOriginalStatsIndex(b);
//                }
//            }


//            public List<string> StatusKey = null;
//            public string GetStatusKey(int b)
//            {
//                if (Step == 0) return string.Format("N:{0}-R:{1}", GraphQuery.RawQuery[b].Item1, GraphQuery.RawQuery[b].Item2);
//                else return StatusKey[b];
//            }

//            /// <summary>
//            /// QueryIdx.
//            /// </summary>
//            /// List<int> QueryIndex { get; set; }

//            /// <summary>
//            /// previous batchIdx and actionIdx.
//            /// </summary>
//            public List<Tuple<int, int>> SelectedAction { get; set; }

//            /// <summary>
//            /// constrastive reward.
//            /// </summary>
//            public List<float> CR { get; set; }
            
//            public int MaxBatchSize { get { return StateEmbed.MAX_BATCHSIZE; } }
//            public int BatchSize { get { return StateEmbed.BatchSize; } }

//            /// <summary>
//            /// foreach query, obtain the batch indexes.
//            /// </summary>
//            /// <param name="queryId"></param>
//            /// <returns></returns>
//            //public List<int> GetBatchIdxs(int queryId)
//            //{
//            //    if(QueryIndex == null)
//            //    {
//            //        return Enumerable.Range(queryId * GraphQuery.GroupSize, GraphQuery.GroupSize).ToList();
//            //    }
//            //    else
//            //    {
//            //        List<int> r = new List<int>();
//            //        for(int i=0;i<QueryIndex.Count; i++)
//            //        {
//            //            if (QueryIndex[i] == queryId) r.Add(i);
//            //        }
//            //        return r;
//            //    }
//            //}

//            /// <summary>
//            /// logprobability of the node.
//            /// </summary>
//            /// <param name="batchIdx"></param>
//            /// <returns></returns>
//            public float GetLogProb(int batchIdx)
//            {
//                if (LogProb == null) { return 0; }
//                else { return LogProb[batchIdx]; }
//            }

//            public bool IsTermable(int b)
//            {
//                if (Term == null) { return false; }
//                return true;
//            }

//            /// <summary>
//            /// Log Termination Probability.
//            /// </summary>
//            /// <param name="b"></param>
//            /// <param name="isT"></param>
//            /// <returns></returns>
//            public float LogPTerm(int b, bool isT)
//            {
//                if (isT) { return (float)Util.LogLogistial(Term.Output.Data.MemPtr[b]); }
//                else { return (float)Util.LogNLogistial(Term.Output.Data.MemPtr[b]); }
//            }

//            /// <summary>
//            /// QueryIndex.
//            /// </summary>
//            /// <param name="batchId"></param>
//            /// <returns></returns>
//            //public int GetQueryIndex(int batchId)
//            //{
//            //    if (QueryIndex != null) { return QueryIndex[batchId]; }
//            //    else { return batchId; }
//            //}

//            //public bool IsBlack(int b)
//            //{
//            //    int queryIdx = GetQueryIndex(b);
//            //    if (GraphQuery.BlackTargets[queryIdx].Contains(NodeID[b])) { return true; }
//            //    else return false;
//            //}

//            /// <summary>
//            /// Embedding of the State.
//            /// </summary>
//            public HiddenBatchData StateEmbed { get; set; }

//            public int Step;

//            //public StatusData(GraphQueryData interData, List<int> nodeIndex, HiddenBatchData stateEmbed, DeviceType device) :
//            //    this(interData, nodeIndex, null, null, null, stateEmbed, device)
//            //{ }

//            //public StatusData(GraphQueryData interData, 
//            //                  List<int> nodeIndex, List<int> queryIdx, List<float> logProb, List<Tuple<int, int>> selectedAction, 
//            //                  HiddenBatchData stateEmbed, DeviceType device)
//            //{
//            //    GraphQuery = interData;
//            //    GraphQuery.StatusPath.Add(this);

//            //    NodeID = nodeIndex;
//            //    StateEmbed = stateEmbed;

//            //    QueryIndex = queryIdx;
//            //    LogProb = logProb;
//            //    SelectedAction = selectedAction;

//            //    CR = new List<float>();

//            //    Step = GraphQuery.StatusPath.Count - 1;
//            //}

//            public StatusData(GraphQueryData interData, List<int> nodeIndex, HiddenBatchData stateEmbed, DeviceType device) :
//                this(interData, nodeIndex, null, null, null, null, stateEmbed, device)
//            { }

//            public StatusData(GraphQueryData interData,
//                              List<int> nodeIndex, List<float> logProb, List<int> selectedAction, List<int> preStatusIndex, List<string> statusKey,
//                              HiddenBatchData stateEmbed, DeviceType device)
//            {
//                GraphQuery = interData;
//                GraphQuery.StatusPath.Add(this);

//                NodeID = nodeIndex;
//                StateEmbed = stateEmbed;

//                LogProb = logProb;
//                PreSelActIndex = selectedAction;
//                PreStatusIndex = preStatusIndex;
//                StatusKey = statusKey;
//                //SelectedAction = selectedAction;

//                CR = new List<float>();

//                Step = GraphQuery.StatusPath.Count - 1;
//            }
//        }

//        class StatusEmbedRunner : CompositeNetRunner
//        {
//            public new List< Tuple<int, int>> Input { get; set; }

//            public new HiddenBatchData Output { get; set; }

//            EmbedStructure InNodeEmbed { get; set; }
//            EmbedStructure LinkEmbed { get; set; }

//            public StatusEmbedRunner(List<Tuple<int,int>> input, int maxBatchSize, EmbedStructure inNodeEmbed, EmbedStructure linkEmbed, RunnerBehavior behavior) : base(behavior)
//            {
//                Input = input;
//                InNodeEmbed = inNodeEmbed;
//                LinkEmbed = linkEmbed;
//                // concate of node embedding and relation embedding.
//                Output = new HiddenBatchData(maxBatchSize, inNodeEmbed.Dim + linkEmbed.Dim, DNNRunMode.Train, behavior.Device);
//            }

//            public override void Forward()
//            {
//                InNodeEmbed.Embedding.SyncToCPU();
//                LinkEmbed.Embedding.SyncToCPU();

//                //BatchLinks.Clear();
//                int batchSize = 0;
//                while (batchSize < Input.Count)
//                {
//                    int srcId = Input[batchSize].Item1;
//                    int linkId = Input[batchSize].Item2;

//                    int bidx = batchSize;

//                    if (BuilderParameters.N_EmbedDim > 0)
//                    {
//                        FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim,
//                                                InNodeEmbed.Embedding.MemPtr, srcId * InNodeEmbed.Dim, InNodeEmbed.Dim, 0, 1);
//                    }
//                    FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim + InNodeEmbed.Dim,
//                                            LinkEmbed.Embedding.MemPtr, linkId * LinkEmbed.Dim, LinkEmbed.Dim, 0, 1);
//                    batchSize += 1;
//                }
//                Output.BatchSize = batchSize;
//                Output.Output.Data.SyncFromCPU(Output.BatchSize * Output.Dim);
//            }

//            public override void CleanDeriv()
//            {
//                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
//            }

//            public override void Update()
//            {
//                Output.Deriv.Data.SyncToCPU(Output.Dim * Output.BatchSize);

//                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();
//                LinkEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();

//                for (int b = 0; b < Output.BatchSize; b++)
//                {
//                    int srcIdx = Input[b].Item1;
//                    int linkIdx = Input[b].Item2;

//                    if (BuilderParameters.N_EmbedDim > 0)
//                    {
//                        FastVector.Add_Vector(InNodeEmbed.EmbeddingOptimizer.Gradient.MemPtr, srcIdx * InNodeEmbed.Dim,
//                                        Output.Deriv.Data.MemPtr, b * Output.Dim, InNodeEmbed.Dim,
//                                        1, InNodeEmbed.EmbeddingOptimizer.GradientStep);
//                    }

//                    FastVector.Add_Vector(LinkEmbed.EmbeddingOptimizer.Gradient.MemPtr, linkIdx * LinkEmbed.Dim,
//                                    Output.Deriv.Data.MemPtr, b * Output.Dim + InNodeEmbed.Dim, LinkEmbed.Dim,
//                                    1, LinkEmbed.EmbeddingOptimizer.GradientStep);
//                }

//                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
//                LinkEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();

//            }
//        }

//        class CandidateActionRunner : CompositeNetRunner
//        {
//            public new StatusData Input;

//            public new List<Tuple<int, int>> Output = new List<Tuple<int, int>>();
//            public BiMatchBatchData Match;

//            /// <summary>
//            /// Input : State.
//            /// node and rel embed.
//            /// </summary>
//            /// <param name="input"></param>
//            /// <param name="nodeEmbed"></param>
//            /// <param name="relEmbed"></param>
//            /// <param name="behavior"></param>
//            public CandidateActionRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
//            {
//                Input = input;

//                Match = new BiMatchBatchData(new BiMatchBatchDataStat() {
//                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
//                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
//                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount }, behavior.Device);
//            }

//            Random random = new Random(15);
//            public override void Forward()
//            {
//                Output.Clear();

//                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
//                int cursor = 0;
//                for (int b = 0; b < Input.BatchSize; b++)
//                {
//                    int seedNode = Input.NodeID[b];

//                    int qid = Input.GetOriginalStatsIndex(b);
//                    int rawSrc = Input.GraphQuery.RawQuery[qid].Item1;
//                    int rawR = Input.GraphQuery.RawQuery[qid].Item2;
//                    int rawTgt = Input.GraphQuery.RawTarget[qid];

//                    int candidateNum = 0;
//                    for (int nei = 0; nei < DataPanel.knowledgeGraph.TrainNeighborLink[seedNode].Count; nei++)
//                    {
//                        int lid = DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item1 > 0 ?
//                            DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 :
//                            DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 + DataPanel.RelationNum;

//                        int relid = DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item1 > 0 ?
//                            DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 + DataPanel.RelationNum :
//                            DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 ;

//                        int tgtNode = DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item3;
//                        if (seedNode == rawSrc && lid == rawR && tgtNode == rawTgt) { continue; }
//                        if (seedNode == rawTgt && relid == rawR && tgtNode == rawSrc) { continue; } 

//                        Output.Add(new Tuple<int, int>(tgtNode, lid));
//                        match.Add(new Tuple<int, int, float>(b, cursor, 1));
//                        cursor += 1;
//                        candidateNum += 1;
//                     }
                     
//                     if(candidateNum == 0)
//                     {
//                        Output.Add(new Tuple<int, int>(seedNode, random.Next(DataPanel.RelationNum * 2)));
//                        match.Add(new Tuple<int, int, float>(b, cursor, 1));
//                        cursor += 1;
//                     }
//                }
//                Match.SetMatch(match);
//            }
//        }

//        static Random SampleRandom = new Random(10);
        
//        public class BasicPolicyRunner : CompositeNetRunner
//        {
//            public new StatusData Input { get; set; }

//            public BiMatchBatchData MatchPath = null;
//            /// <summary>
//            /// new Node Index.
//            /// </summary>
//            public List<int> NodeIndex { get; set; }

//            /// <summary>
//            /// next step log probability.
//            /// </summary>
//            public List<float> LogProb { get; set; }

//            /// <summary>
//            /// action index.
//            /// </summary>
//            public List<int> PreSelActIndex { get; set; }
//            public List<int> PreStatusIndex { get; set; }
//            public List<string> StatusKey { get; set; }


//            /// <summary>
//            /// batchIdx and actionIdx.
//            /// </summary>
//            public List<Tuple<int, int>> NodeAction { get; set; }


//            /// <summary>
//            /// Group index.
//            /// </summary>
//            /// public List<int> GroupIndex = null;

//            public BasicPolicyRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
//            {
//                Input = input;

//                NodeIndex = new List<int>();
//                LogProb = new List<float>();
//                NodeAction = new List<Tuple<int, int>>();

//                PreSelActIndex = new List<int>();
//                PreStatusIndex = new List<int>();
//                StatusKey = new List<string>();

//            }
//        }

//        class ActionSamplingRunner : BasicPolicyRunner
//        {
//            int lineIdx = 0;
//            public override void Init()
//            {
//                lineIdx = 0;
//            }
//            /// <summary>
//            /// match candidate, and match probability.
//            /// </summary>
//            /// <param name="input"></param>
//            /// <param name="behavior"></param>
//            public ActionSamplingRunner(StatusData input, RunnerBehavior behavior) : base(input, behavior)
//            {
//                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
//                {
//                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
//                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
//                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
//                }, behavior.Device);
//            }

//            public override void Forward()
//            {
//                NodeIndex.Clear();
//                LogProb.Clear();
//                NodeAction.Clear();

//                PreSelActIndex.Clear();
//                PreStatusIndex.Clear();

//                Input.MatchCandidateProb.Output.SyncToCPU();
//                if (Input.Term != null) Input.Term.Output.SyncToCPU();

//                int currentStep = Input.Step;
//                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

//                for (int i = 0; i < Input.MatchCandidateProb.Segment; i++)
//                {
//                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
//                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

//                    int selectIdx = -1;

//                    // draw sample actions.
//                    if (Input.GraphQuery.PathIdx[i] == -1)
//                    {
//                        int idx = Util.Sample(Input.MatchCandidateProb.Output.MemPtr, s, e - s, SampleRandom);
//                        selectIdx = s + idx;
//                    }
//                    // draw random actions.
//                    else
//                    {
//                        selectIdx = SampleRandom.Next(s, e);
//                    }

//                    NodeIndex.Add(Input.MatchCandidate[selectIdx].Item1);
//                    NodeAction.Add(new Tuple<int, int>(i, selectIdx));

//                    float nonLogTerm = 0;
//                    if (Input.IsTermable(i))
//                    {
//                        nonLogTerm = Input.LogPTerm(i, false);
//                    }
//                    float prob = Input.GetLogProb(i) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
//                    LogProb.Add(prob);

//                    match.Add(new Tuple<int, int, float>(i, selectIdx, 1));
//                }

//                MatchPath.SetMatch(match);

//                lineIdx += Input.MatchCandidateProb.Segment / BuilderParameters.BeamSize;
//            }
//        }


//        class BeamSearchActionRunner : BasicPolicyRunner
//        {
//            int BeamSize = 1;

//            /// <summary>
//            /// group aware beam search.
//            /// </summary>
//            /// <param name="input"></param>
//            /// <param name="behavior"></param>
//            public BeamSearchActionRunner(StatusData input, int beamSize, RunnerBehavior behavior) : base(input, behavior)
//            {
//                BeamSize = beamSize;
//                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
//                {
//                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
//                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
//                    MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
//                }, behavior.Device);
//            }

//            public override void Forward()
//            {
//                if(Input.MatchCandidateProb.Segment != Input.BatchSize)
//                {
//                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
//                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
//                }

//                Input.MatchCandidateProb.Output.SyncToCPU();
//                if(Input.Term != null) Input.Term.Output.SyncToCPU();

//                GroupIndex.Clear();


//                NodeIndex.Clear();
//                LogProb.Clear();
//                NodeAction.Clear();
                
//                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
//                // for each beam.
//                for (int i = 0; i < Input.GraphQuery.BatchSize; i++) //.MatchCandidateProb.Segment; i++)
//                {
//                    List<int> idxs = Input.GetBatchIdxs(i);

//                    MinMaxHeap<Tuple<int, int>> topKheap = new MinMaxHeap<Tuple<int, int>>(BeamSize, 1);

//                    foreach (int b in idxs)
//                    {
//                        int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[b];
//                        int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

//                        float nonLogTerm = 0;
//                        if (Input.IsTermable(b)) { nonLogTerm = Input.LogPTerm(b, false); }

//                        for (int t = s; t < e; t++)
//                        {
//                            float prob = Input.GetLogProb(b) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t]);
//                            topKheap.push_pair(new Tuple<int, int>(b, t), prob);
//                        }
//                    }
                    
//                    while (!topKheap.IsEmpty)
//                    {
//                        KeyValuePair<Tuple<int, int>, float> p = topKheap.PopTop();
//                        match.Add(new Tuple<int, int, float>(p.Key.Item1, p.Key.Item2, p.Value));

//                        NodeIndex.Add(Input.MatchCandidate[p.Key.Item2].Item1);
//                        LogProb.Add(p.Value);
//                        GroupIndex.Add(i);
//                        NodeAction.Add(new Tuple<int, int>(p.Key.Item1, p.Key.Item2));
//                    }
//                }
//                MatchPath.SetMatch(match);
//            }
//        }

//        /// <summary>
//        /// it is a little difficult.
//        /// </summary>
//        class RewardRunner : ObjectiveRunner
//        {
//            Random random = new Random(17);
//            new GraphQueryData Input { get; set; }
//            public RewardRunner(GraphQueryData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
//            {
//                Input = input;
//            }

//            public override void Forward()
//            {
//                // calculate termination probability.
//                for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
//                {
//                    if (Input.StatusPath[t].Term != null)
//                    {
//                        ComputeLib.Logistic(Input.StatusPath[t].Term.Output.Data, 0, Input.StatusPath[t].Term.Output.Data, 0, Input.BatchSize, 1);
//                        ComputeLib.ClipVector(Input.StatusPath[t].Term.Output.Data, Input.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);
//                        Input.StatusPath[t].Term.Output.Data.SyncToCPU();
//                        Array.Clear(Input.StatusPath[t].Term.Deriv.Data.MemPtr, 0, Input.StatusPath[t].BatchSize);
//                    }
//                    if (Input.StatusPath[t].MatchCandidateProb != null)
//                    {
//                        Array.Clear(Input.StatusPath[t].MatchCandidateProb.Deriv.MemPtr, 0, Input.StatusPath[t].MatchCandidateProb.Length);
//                    }
//                }
                
//                // calculate the probability of each stopable sample.
//                List<int> SelectedStep = new List<int>();
//                for (int b = 0; b < Input.BatchSize; b++)
//                {
//                    int selectedStep = Input.StatusPath.Count - 1;

//                    for (int t = 1; t < Input.StatusPath.Count; t++)
//                    {
//                        StatusData st = Input.StatusPath[t];
//                        float tp = st.Term.Output.Data.MemPtr[b];
//                        if (random.NextDouble() <= tp)
//                        {
//                            selectedStep = t;
//                            break;
//                        }
//                    }
//                    SelectedStep.Add(selectedStep);
//                }

//                float avgProb = 0;
//                int avgGroupNum = 0;
//                for (int g = 0; g < Input.GroupNum; g++)
//                {
//                    int targetNum = 0;
//                    int blackNum = 0;
//                    int falseNum = 0;
//                    for (int s = 0; s < Input.GroupSize; s++)
//                    {
//                        int bidx = g * Input.GroupSize + s;
//                        int t = SelectedStep[bidx];

//                        int targetId = Input.StatusPath[t].NodeID[bidx];
//                        if (targetId == Input.RawTarget[bidx])
//                        {
//                            targetNum += 1;
//                        }
//                        else if(Input.BlackTargets[bidx].Contains(targetId))
//                        {
//                            blackNum += 1;
//                        }
//                        else
//                        {
//                            falseNum += 1;
//                        }
//                    }

//                    if(falseNum > 0)
//                    {
//                        avgProb += targetNum * 1.0f / (targetNum + falseNum);
//                        avgGroupNum += 1;
//                    }


//                    #region constrastive reward calculation.
//                    for (int s = 0; s < Input.GroupSize; s++)
//                    {
//                        int bidx = g * Input.GroupSize + s;
//                        int t = SelectedStep[bidx];
//                        int targetId = Input.StatusPath[t].NodeID[bidx];

//                        float cr = 0;
//                        if (BuilderParameters.NORM_REWARD == 0)
//                        {
//                            if (targetId == Input.RawTarget[bidx])
//                            {
//                                cr = BuilderParameters.POS_R;// 1.0f; // 1.0f / targetNum - targetNum * 1.0f / (targetNum + falseNum + float.Epsilon);
//                            }
//                            else if (Input.BlackTargets[bidx].Contains(targetId))
//                            {
//                                //cr = 0;// -1.0f; // -targetNum * 1.0f / (targetNum + falseNum + float.Epsilon);
//                                cr = BuilderParameters.BLACK_R;
//                            }
//                            else
//                            {
//                                cr = BuilderParameters.NEG_R;
//                            }
//                        }
//                        else
//                        {
//                            if (targetId == Input.RawTarget[bidx])
//                            {
//                                cr = 1.0f / targetNum - 1.0f / (targetNum + falseNum + float.Epsilon);
//                            }
//                            else if (Input.BlackTargets[bidx].Contains(targetId))
//                            {
//                                //cr = 0;// -1.0f; // -targetNum * 1.0f / (targetNum + falseNum + float.Epsilon);
//                                cr = 0; // BuilderParameters.BLACK_R;
//                            }
//                            else
//                            {
//                                cr = - 1.0f / (targetNum + falseNum + float.Epsilon);
//                            }
//                        }
//                        StatusData st = Input.StatusPath[t];
//                        st.Term.Deriv.Data.MemPtr[bidx] = cr * (1 - st.Term.Output.Data.MemPtr[bidx]);
//                        for (int pt = t - 1; pt >= 0; pt--)
//                        {
//                            StatusData pst = Input.StatusPath[pt];
//                            if (pst.Term != null)
//                            {
//                                pst.Term.Deriv.Data.MemPtr[bidx] = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt) * cr * (-pst.Term.Output.Data.MemPtr[bidx]);
//                            }
//                        }

//                        for (int pt = t - 1; pt >= 0; pt--)
//                        {
//                            StatusData pst = Input.StatusPath[pt];
//                            int sidx = st.SelectedAction[bidx].Item2;
//                            pst.MatchCandidateProb.Deriv.MemPtr[sidx] = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt) * cr;
//                            st = pst;
//                        }
//                    }
//                    #endregion.
//                }

//                ObjectiveScore = avgProb / (avgGroupNum + float.Epsilon);

//                //average ground truth results.

//                for (int i = Input.StatusPath.Count - 1; i >= 0; i--)
//                {
//                    StatusData st = Input.StatusPath[i];
//                    if (st.Term != null) st.Term.Deriv.Data.SyncFromCPU();
//                    if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
//                }
//            }
//        }
        
//        /// <summary>
//        /// REINFORCE-WALK prediction Runner.
//        /// </summary>
//        class TopKPredictionRunner : StructRunner
//        {
//            int lsum = 0, lsum_filter = 0;
//            int rsum = 0, rsum_filter = 0;

//            float _lsum = 0, _lsum_filter = 0;
//            float _rsum = 0, _rsum_filter = 0;

//            int lp_n = 0, lp_n_filter = 0;
//            int rp_n = 0, rp_n_filter = 0;
//            int HitK = 10;

//            int Iteration = 0;
//            int SmpIdx = 0;
//            GraphQueryData Query { get; set; }

//            public TopKPredictionRunner(GraphQueryData query, RunnerBehavior behavior) : base(Structure.Empty, behavior)
//            {
//                Query = query;
//                Iteration = 0;
//            }

//            public override void Init()
//            {
//                SmpIdx = 0;
//                lsum = 0;
//                lsum_filter = 0;
//                _lsum = 0;
//                _lsum_filter = 0;

//                rsum = 0;
//                rsum_filter = 0;
//                _rsum = 0;
//                _rsum_filter = 0;

//                lp_n = 0;
//                lp_n_filter = 0;
//                rp_n = 0;
//                rp_n_filter = 0;
//            }

//            public void Report()
//            {
//                Logger.WriteLog("Sample Idx {0}", SmpIdx);

//                Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 2.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 2.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 2.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 2.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Left Mean Re Rank {0}", _lsum * 2.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Left Filter Mean Re Rank {0}", _lsum_filter * 2.0 / SmpIdx));

//                Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 2.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 2.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 2.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 2.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Right Mean Re Rank {0}", _rsum * 2.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Right Filter Mean Re Rank {0}", _rsum_filter * 2.0 / SmpIdx));

//                Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 1.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 1.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 1.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 1.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Overall Mean Re Rank {0}", (_lsum + _rsum) * 1.0 / SmpIdx));
//                Logger.WriteLog(string.Format("Overall Filter Mean Re Rank {0}", (_lsum_filter + _rsum_filter) * 1.0 / SmpIdx));

//            }

//            public override void Complete()
//            {
//                Logger.WriteLog("Final Report!");
//                {
//                    Report();
//                }
//                Iteration += 1;
//            }
//            public override void Forward()
//            {
//                for (int t = Query.StatusPath.Count - 1; t >= 0; t--)
//                {
//                    if (Query.StatusPath[t].Term != null)
//                    {
//                        Query.StatusPath[t].Term.Output.Data.SyncToCPU();
//                    }
//                }

//                for (int g = 0; g < Query.BatchSize; g++)
//                {
//                    int relation = Query.RawQuery[g].Item2;
//                    int target = Query.RawTarget[g];
//                    HashSet<int> blackTargets = Query.BlackTargets[g];

//                    Dictionary<int, float> score = new Dictionary<int, float>();

//                    for (int p = 0; p < Query.StatusPath.Count; p++)
//                    {
//                        foreach(int b in Query.GetBatchIdxs(p, g))
//                        {
//                            int nidx = Query.StatusPath[p].NodeID[b];
//                            float logP = Query.StatusPath[p].GetLogProb(b);
//                            if(Query.StatusPath[p].IsTermable(b))
//                            {
//                                float s = logP + Query.StatusPath[p].LogPTerm(b, true);

//                                if (!score.ContainsKey(nidx)) { score[nidx] = float.MinValue; }
//                                score[nidx] = Util.LogAdd(score[nidx], s); 
//                            }
//                        }
//                    }

//                    var sortD = score.OrderByDescending(pair => pair.Value);
                    
//                    int filter = 1;
//                    int nonFilter = 1;
//                    bool isFound = false;
//                    foreach(KeyValuePair<int, float> d in sortD)
//                    {
//                        int ptail = d.Key;

//                        if (ptail == target)
//                        {
//                            isFound = true;
//                            break;
//                        }
//                        nonFilter += 1;
//                        if (!blackTargets.Contains(ptail)) { filter++; }
//                    }
                    
//                    if(!isFound)
//                    {
//                        int tail = (DataPanel.EntityNum - nonFilter);
//                        int topBlack = nonFilter - filter;
//                        int tailBlack = blackTargets.Count - topBlack;
//                        nonFilter = nonFilter + tail / 2;
//                        filter = filter + (tail - tailBlack) / 2;
//                    }
//                    //else
//                    //{
//                    //    Console.WriteLine("Yes, I find it!");
//                    //}
//                    if (relation < DataPanel.RelationNum)
//                    {
//                        Interlocked.Add(ref lsum, nonFilter);
//                        Interlocked.Add(ref lsum_filter, filter);
//                        if (nonFilter <= HitK) Interlocked.Increment(ref lp_n); // lp_n += 1;
//                        if (filter <= HitK) Interlocked.Increment(ref lp_n_filter); // += 1;
//                        _lsum += 1.0f / nonFilter;
//                        _lsum_filter += 1.0f / filter;
//                    }
//                    else
//                    {
//                        Interlocked.Add(ref rsum, nonFilter);
//                        Interlocked.Add(ref rsum_filter, filter);
//                        if (nonFilter <= HitK) Interlocked.Increment(ref rp_n); // lp_n += 1;
//                        if (filter <= HitK) Interlocked.Increment(ref rp_n_filter); // += 1;
//                        _rsum += 1.0f / nonFilter;
//                        _rsum_filter += 1.0f / filter;
//                    }
//                }
//                SmpIdx += Query.BatchSize;
//                //Report();
//            }
//        }

//        public class NeuralWalkerModel : CompositeNNStructure
//        {
//            public EmbedStructure InNodeEmbed { get; set; }
//            public EmbedStructure InRelEmbed { get; set; }

//            public EmbedStructure CNodeEmbed { get; set; }
//            public EmbedStructure CRelEmbed { get; set; }

//            public DNNStructure SrcDNN { get; set; }
//            public DNNStructure TgtDNN { get; set; }

//            public LayerStructure AttEmbed { get; set; }
//            public DNNStructure TermDNN { get; set; }

//            public GRUCell GruCell { get; set; }

//            public NeuralWalkerModel(int nodeDim, int relDim, DeviceType device)
//            {
//                InNodeEmbed = AddLayer(new EmbedStructure(DataPanel.EntityNum, nodeDim, device)); // DeviceType.CPU_FAST_VECTOR));
//                InRelEmbed = AddLayer(new EmbedStructure(DataPanel.RelationNum * 2, relDim, device)); // DeviceType.CPU_FAST_VECTOR));
//                CNodeEmbed = AddLayer(new EmbedStructure(DataPanel.EntityNum, nodeDim, device));// DeviceType.CPU_FAST_VECTOR));
//                CRelEmbed = AddLayer(new EmbedStructure(DataPanel.RelationNum * 2, relDim, device)); // DeviceType.CPU_FAST_VECTOR));

//                SrcDNN = AddLayer(new DNNStructure(nodeDim + relDim, BuilderParameters.DNN_DIMS,
//                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
//                                           BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
//                                           device));

//                TgtDNN = AddLayer(new DNNStructure(nodeDim + relDim, BuilderParameters.DNN_DIMS,
//                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
//                                           BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
//                                           device));

//                AttEmbed = AddLayer(new LayerStructure(BuilderParameters.DNN_DIMS.Last(), 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device));


//                TermDNN = AddLayer(new DNNStructure(nodeDim + relDim, BuilderParameters.T_NET, BuilderParameters.T_AF,
//                                         BuilderParameters.T_NET.Select(i => true).ToArray(), device));

//                GruCell = AddLayer(new GRUCell(nodeDim + relDim, nodeDim + relDim, device));

//            }
//            public NeuralWalkerModel(BinaryReader reader, DeviceType device) 
//            {
//                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

//                InNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
//                InRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);

//                CNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
//                CRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);

//                SrcDNN = (DNNStructure)DeserializeNextModel(reader, device);
//                TgtDNN = (DNNStructure)DeserializeNextModel(reader, device);

//                AttEmbed = (LayerStructure)DeserializeNextModel(reader, device);
//                TermDNN = (DNNStructure)DeserializeNextModel(reader, device);
//                GruCell = (GRUCell)DeserializeNextModel(reader, device);

//            }

//            public void InitOptimization(RunnerBehavior behavior)
//            {
//                //InNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
//                //InRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

//                //CNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
//                //CRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

//                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
//            }
//        }

//        public static ComputationGraph BuildComputationGraph(List<Tuple<int, int, int>> graph, int batchSize, int beamSize,
//                                                             NeuralWalkerModel model, RunnerBehavior Behavior)
//        {

//            ComputationGraph cg = new ComputationGraph();

//            // sample a list of tuplet.
//            SampleRunner SmpRunner = new SampleRunner(graph, batchSize, Behavior.RunMode == DNNRunMode.Train ? beamSize : 1, Behavior);
//            cg.AddDataRunner(SmpRunner);
//            GraphQueryData interface_data = SmpRunner.Output;

//            StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, model.InNodeEmbed, model.InRelEmbed, Behavior);
//            cg.AddRunner(statusEmbedRunner);

//            StatusData status = new StatusData(interface_data, interface_data.RawSource, statusEmbedRunner.Output, Behavior.Device);
//            #region multi-hop expan
//            // travel four steps in the knowledge graph.
//            for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
//            {
//                //given status, obtain the match. miniBatch * maxNeighbor number.
//                CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
//                cg.AddRunner(candidateActionRunner);

//                // miniMatch * maxNeighborNumber.
//                StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, model.CNodeEmbed, model.CRelEmbed, Behavior);
//                cg.AddRunner(candEmbedRunner);

//                DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
//                cg.AddRunner(srcHiddenRunner);

//                DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
//                cg.AddRunner(candHiddenRunner);

//                // alignment miniBatch * miniBatch * neighbor
//                VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
//                                                                            candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
//                cg.AddRunner(attentionRunner);

//                SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
//                                                                            status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
//                                                                            candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
//                                                                            Behavior.Device), 1, Behavior,
//                                                                            true);
//                cg.AddRunner(normAttRunner);

//                status.MatchCandidate = candidateActionRunner.Output;
//                status.MatchCandidateProb = normAttRunner.Output;
//                //it will cause un-necessary unstable.

//                BasicPolicyRunner policyRunner = null;

//                if (Behavior.RunMode == DNNRunMode.Train)
//                {
//                    policyRunner = new ActionSamplingRunner(status, Behavior);
//                }
//                else
//                {
//                    policyRunner = new BeamSearchActionRunner(status, BuilderParameters.BeamSize, Behavior);
//                }
//                cg.AddRunner(policyRunner);


//                #region version 2 of action selection.
//                MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
//                cg.AddRunner(srcExpRunner);

//                MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, policyRunner.MatchPath, 2, Behavior);
//                cg.AddRunner(tgtExpRunner);

//                //StatusEmbedRunner actionEmbedRunner = new StatusEmbedRunner(actionRunner.SelectAction, status.MaxBatchSize, model.CNodeEmbed, model.CRelEmbed, Behavior);
//                //cg.AddRunner(actionEmbedRunner);
//                GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
//                cg.AddRunner(stateRunner);
//                #endregion.

//                #region version 1 of action selection.
//                //StatusEmbedRunner actionEmbedRunner = new StatusEmbedRunner(actionRunner.Action, status.MaxBatchSize, model.CNodeEmbed, model.CRelEmbed, Behavior);
//                //cg.AddRunner(actionEmbedRunner);
//                //GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, status.StateEmbed, actionEmbedRunner.Output, Behavior);
//                //cg.AddRunner(stateRunner);
//                #endregion.

//                StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.GroupIndex, policyRunner.LogProb, policyRunner.NodeAction, stateRunner.Output, Behavior.Device);
//                //StatusData nextStatus = new StatusData(status.GraphQuery, actionRunner.NodeIndex, actionRunner.GroupIndex, actionRunner.LogProb, actionRunner.NodeAction, stateRunner.Output, Behavior.Device);

//                DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, nextStatus.StateEmbed, Behavior);
//                cg.AddRunner(termRunner);
//                nextStatus.Term = termRunner.Output;
//                status = nextStatus;
//            }
//            #endregion.

//            if (Behavior.RunMode == DNNRunMode.Train)
//            {
//                RewardRunner rewardRunner = new RewardRunner(interface_data, Behavior);
//                cg.AddObjective(rewardRunner);
//            }
//            else
//            {
//                TopKPredictionRunner predRunner = new TopKPredictionRunner(interface_data, Behavior);
//                cg.AddRunner(predRunner);
//            }
//            cg.SetDelegateModel(model);
//            return cg;
//        }


//        public override void Rock()
//        {
//            Logger.OpenLog(BuilderParameters.LogFile);

//            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
//            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

//            Logger.WriteLog("Loading Training/Validation/Test Data.");
//            DataPanel.Init();
//            Logger.WriteLog("Load Data Finished.");


//            NeuralWalkerModel model =
//                BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
//                new NeuralWalkerModel(BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, device) :
//                new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);

//            model.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
//            switch (BuilderParameters.RunMode)
//            {
//                case DNNRunMode.Train:
//                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

//                    ComputationGraph trainCG = BuildComputationGraph(
//                        BuilderParameters.TRAIN_DEBUG == 0 ? DataPanel.knowledgeGraph.Valid : DataPanel.knowledgeGraph.Train, 
//                        BuilderParameters.MiniBatchSize, BuilderParameters.TrainBeamSize, model, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    
//                    ComputationGraph predCG = BuildComputationGraph(DataPanel.knowledgeGraph.Test, BuilderParameters.TestMiniBatchSize, BuilderParameters.BeamSize, model,
//                                       new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

//                    //predCG.Execute();

//                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
//                    {
//                        //ActionSampler = new Random(10);
//                        double loss = trainCG.Execute();
//                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

//                        if ( (iter + 1) % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
//                        {
//                            Logger.WriteLog("Evaluation at Iteration {0}", iter);

//                            predCG.Execute();
//                            using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "model." + iter.ToString(), FileMode.Create, FileAccess.Write)))
//                            {
//                                model.Serialize(writer);
//                            }
//                        }
//                    }
//                    break;
//                case DNNRunMode.Predict:
//                    ComputationGraph testCG = BuildComputationGraph(DataPanel.knowledgeGraph.Test, BuilderParameters.TestMiniBatchSize, BuilderParameters.BeamSize, model,
//                                        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
//                    testCG.Execute();
//                    break;
                
//            }
//            Logger.CloseLog();
//        }

//        /// <summary>
//        /// load data.
//        /// </summary>
//        public class DataPanel
//        {
//            public static RelationGraphData knowledgeGraph;

//            public static int EntityNum { get { return knowledgeGraph.EntityNum; } }
//            public static int RelationNum { get { return knowledgeGraph.RelationNum; } }

//            public static int MaxNeighborCount { get { return knowledgeGraph.TrainNeighborLink.Select(i => i.Value.Count).Max(); } }

//            public static void Init()
//            {
//                knowledgeGraph = new RelationGraphData();
//                knowledgeGraph.EntityNum = BuilderParameters.EntityNum;
//                knowledgeGraph.RelationNum = BuilderParameters.RelationNum;

//                knowledgeGraph.Train = knowledgeGraph.LoadGraphV3(BuilderParameters.TrainData, true);
//                knowledgeGraph.Valid = knowledgeGraph.LoadGraphV3(BuilderParameters.ValidData, false);
//                knowledgeGraph.Test = knowledgeGraph.LoadGraphV3(BuilderParameters.TestData, false);

//                Console.WriteLine("Entity Number {0}, Relation Number {1}", knowledgeGraph.EntityNum, knowledgeGraph.RelationNum);
//            }
//        }
//    }
//}
