﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    /// <summary>
    /// Query Side LSTM
    /// Doc Side CNN
    /// </summary>
    public class AppCNNLSTMContextQABuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                // training data folder.
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                Argument.Add("TRAIN-FOLDER", new ParameterArgument(string.Empty, "Training Folder"));
                Argument.Add("TEST-FOLDER", new ParameterArgument(string.Empty, "Testing Folder"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));

                Argument.Add("CON-LAYER-DIM", new ParameterArgument("128,128", "Context Layer Dim"));
                Argument.Add("CON-LAYER-WIN", new ParameterArgument("5,5", "Context Layer WindowSize"));

                Argument.Add("QRY-LSTM-LAYER-DIM", new ParameterArgument("128", "DNN Layer Dim"));

                // Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string Vocab { get { return Argument["VOCAB"].Value == string.Empty ? TrainFolder + ".vocab" : Argument["VOCAB"].Value; } }

            public static string TrainFolder { get { return Argument["TRAIN-FOLDER"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTrainFile { get { return (!TrainFolder.Equals(string.Empty)); } }
            public static string TrainIndexData { get { return TrainFolder + ".idx.tsv"; } }
            public static string TrainContextBinary { get { return TrainFolder + ".cox.bin"; } }
            public static string TrainQueryBinary { get { return TrainFolder + ".qry.bin"; } }
            public static string TrainAnswerBinary { get { return TrainFolder + ".anw.bin"; } }

            public static string TestFolder { get { return Argument["TEST-FOLDER"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTestFile { get { return (!TestFolder.Equals(string.Empty)); } }
            public static string TestIndexData { get { return TestFolder + ".idx.tsv"; } }
            public static string TestContextBinary { get { return TestFolder + ".cox.bin"; } }
            public static string TestQueryBinary { get { return TestFolder + ".qry.bin"; } }
            public static string TestAnswerBinary { get { return TestFolder + ".anw.bin"; } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static int[] CON_LAYER_DIM { get { return Argument["CON-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] CON_LAYER_WIN { get { return Argument["CON-LAYER-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] QUERY_LAYER_DIM { get { return Argument["QRY-LSTM-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            /// <summary>
            /// DNN Run Mode.
            /// </summary>
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
        }

        public override BuilderType Type { get { return BuilderType.APP_CNN_LSTM_CONTEXT_QA; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        class GenerateCandidateRunner : StructRunner
        {
            public new BiMatchBatchData Output { get { return (BiMatchBatchData)base.Output; } set { base.Output = value; } }

            GeneralBatchInputData CandidateIndexData { get; set; }

            SeqSparseBatchData FullContextMem { get; set; }

            public GenerateCandidateRunner(GeneralBatchInputData candidateData, SeqSparseBatchData fullContextMem, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                CandidateIndexData = candidateData;
                FullContextMem = fullContextMem;

                Output = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = CandidateIndexData.Stat.MAX_BATCHSIZE,
                    MAX_TGT_BATCHSIZE = DataPanel.entityFreqDict.ItemDictSize,
                    MAX_MATCH_BATCHSIZE = CandidateIndexData.Stat.MAX_ELEMENTSIZE
                }, Behavior.Device);
            }

            public override void Forward()
            {
                CandidateIndexData.BatchIdx.SyncToCPU(CandidateIndexData.BatchSize);
                CandidateIndexData.FeatureIdx.SyncToCPU(CandidateIndexData.ElementSize);
                CandidateIndexData.FeatureValue.SyncToCPU(CandidateIndexData.ElementSize);

                FullContextMem.SampleIdx.SyncToCPU(FullContextMem.BatchSize);
                FullContextMem.SequenceIdx.SyncToCPU(FullContextMem.SentSize);
                FullContextMem.FeaIdx.SyncToCPU(FullContextMem.ElementSize);

                List<Tuple<int, int, float>> matchList = new List<Tuple<int, int, float>>();
                for (int i = 0; i < CandidateIndexData.BatchSize; i++)
                {
                    int feaBegin = i == 0 ? 0 : CandidateIndexData.BatchIdx.MemPtr[i - 1];
                    int feaEnd = CandidateIndexData.BatchIdx.MemPtr[i];

                    Dictionary<int, float> entityCandidite = new Dictionary<int, float>();

                    int fullMemBegin = i == 0 ? 0 : FullContextMem.SampleIdx.MemPtr[i - 1];
                    for (int f = feaBegin; f < feaEnd; f++)
                    {
                        int candPos = CandidateIndexData.FeatureIdx.MemPtr[f];
                        float candLabel = CandidateIndexData.FeatureValue.MemPtr[f];

                        int wordIdx = FullContextMem.SequenceIdx.MemPtr[fullMemBegin + candPos] - 1;
                        int candIdx = FullContextMem.FeaIdx.MemPtr[wordIdx] - 1;

                        if (entityCandidite.ContainsKey(candIdx))
                        {
                            if (entityCandidite[candIdx] != candLabel) { throw new Exception("Label doesn't match!!"); }
                        }
                        else
                        {
                            entityCandidite.Add(candIdx, candLabel);
                        }
                    }
                     
                    foreach(KeyValuePair<int,float> item in entityCandidite)
                    {
                        matchList.Add(new Tuple<int, int, float>(i, item.Key, item.Value));
                    }
                }
                Output.Clear();
                Output.PushMatch(matchList);
            }
        }

        class GenerateSelectionPredictionRunner : ObjectiveRunner
        {
            BiMatchBatchData MatchData { get; set; }

            CudaPieceFloat CandScoreData { get; set; }

            float Gamma { get; set; }

            int SampleNum = 0;
            int HitNum = 0;
            string OutputPath = "";
            StreamWriter OutputWriter = null;
            public GenerateSelectionPredictionRunner( BiMatchBatchData matchData, CudaPieceFloat candScoreData, float gamma, 
                RunnerBehavior behavior, string outputFile = "") : base(Structure.Empty, behavior)
            {
                MatchData = matchData;
                CandScoreData = candScoreData;
                Gamma = gamma;
                OutputPath = outputFile;
            }

            public override void Init()
            {
                SampleNum = 0;
                HitNum = 0;
                if (!OutputPath.Equals("")) { OutputWriter = new StreamWriter(OutputPath); }
            }
            public override void Forward()
            {
                /*
                CandidateInputData.BatchIdx.SyncToCPU(CandidateInputData.BatchSize);
                CandidateInputData.FeatureIdx.SyncToCPU(CandidateInputData.ElementSize);
                CandidateInputData.FeatureValue.SyncToCPU(CandidateInputData.ElementSize);
                CandScoreData.SyncToCPU(CandidateInputData.ElementSize);

                ContextInputData.SampleIdx.SyncToCPU(ContextInputData.BatchSize);
                ContextInputData.FeaIdx.SyncToCPU(ContextInputData.ElementSize);
                */
                CandScoreData.SyncToCPU(MatchData.MatchSize);
                for (int i = 0; i < MatchData.SrcSize; i++)
                {
                    int smpBgn = i == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[i - 1];
                    int smpEnd = MatchData.Src2MatchIdx.MemPtr[i];

                    Dictionary<int, float> candScoreDict = new Dictionary<int, float>();

                    int truthIdx = -1;
                    float predScore = float.MinValue;
                    int predIdx = -1;
                    for (int s = smpBgn; s < smpEnd; s++)
                    {
                        int e = MatchData.Src2MatchElement.MemPtr[s];
                        float label = MatchData.MatchInfo.MemPtr[e];
                        float score = CandScoreData.MemPtr[e];
                        int entityIdx = MatchData.TgtIdx.MemPtr[e];
                        if (label > 0) truthIdx = entityIdx;
                        if (score > predScore) { predScore = score; predIdx = entityIdx; }

                        candScoreDict[entityIdx] = (float)Math.Exp(Gamma * score);
                    }

                    SampleNum++;
                    if (predIdx == truthIdx) HitNum++;
                    if (truthIdx == -1 || predIdx == -1) { throw new Exception(string.Format("Target id is not correct {0},{1}", truthIdx, predIdx)); }

                    float expSum = candScoreDict.Values.Sum();
                    foreach (int k in new List<int>(candScoreDict.Keys)) candScoreDict[k] = candScoreDict[k] / expSum;

                    OutputWriter.WriteLine("{0}\t{1}\t{2}", truthIdx, TextUtil.Dict2Str(candScoreDict), predIdx);
                }
            }

            public override void Complete()
            {
                Logger.WriteLog("Sample {0}, Hit {1}, Accuracy {2}", SampleNum, HitNum, HitNum * 1.0 / SampleNum);
                ObjectiveScore = HitNum * 1.0 / SampleNum;
                OutputWriter.Close();
            }
        }

        //class GradientOptimizeRunner : StructRunner
        //{
        //    public GradientOptimizer Optimizer;
        //    public GradientOptimizeRunner(GradientOptimizer optimizer, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        //    {
        //        Optimizer = optimizer;
        //    }
        //    public override void Backward(bool cleanDeriv)
        //    {
        //        ComputeLib.Scale_Matrix(Optimizer.Gradient, Optimizer.Parameter.Size, 1, Optimizer.GradientStep);
        //    }
        //}

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> context,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> query,
                                                             IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> answer,
                                                             // context cnn 
                                                             List<LayerStructure> ContextCNN,

                                                             // bi-directional lstm for query.
                                                             LSTMStructure queryD1LSTM, LSTMStructure queryD2LSTM,

                                                             // match between query and passage.
                                                             LayerStructure MatchLayer,

                                                             LayerStructure EntityLayer,
                                                             // model.
                                                             CompositeNNStructure model, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            if(Behavior.RunMode == DNNRunMode.Train) cg.InitOptimizer(model, OptimizerParameters.StructureOptimizer, Behavior);

            /************************************************************ CNN of context data *********/
            SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(context, Behavior));

            SeqDenseBatchData ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(ContextCNN[0], ContextData, Behavior));
            for (int i = 1; i < ContextCNN.Count; i++)
                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(ContextCNN[i], ContextEmbedOutput, Behavior));

            HiddenBatchData ContextOutput = new HiddenBatchData(ContextEmbedOutput.MAX_SENTSIZE, ContextEmbedOutput.Dim, ContextEmbedOutput.SentOutput, ContextEmbedOutput.SentDeriv, Behavior.Device);

            /************************************************************ Bi direction LSTM of query data *********/
            SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));

            // standard order -> lstm embedding. 
            SeqDenseRecursiveData QueryD1LSTMData = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(queryD1LSTM.LSTMCells[0], QueryData, false, Behavior));
            // reverse order -> lstm embedding.
            SeqDenseRecursiveData QueryD2LSTMData = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(queryD2LSTM.LSTMCells[0], QueryData, true, Behavior));
            
            // last state.
            //HiddenBatchData QueryD1Output = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(QueryD1LSTMData, true, 0, QueryD1LSTMData.MapForward, Behavior));
            //HiddenBatchData QueryD2Output = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(QueryD2LSTMData, false, 0, QueryD2LSTMData.MapForward, Behavior));

            // max pooling.
            HiddenBatchData QueryD1Output = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(QueryD1LSTMData, QueryD1LSTMData.MapForward, Behavior));
            HiddenBatchData QueryD2Output = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(QueryD2LSTMData, QueryD2LSTMData.MapForward, Behavior));

            HiddenBatchData QueryEnsembleOutput = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { QueryD1Output, QueryD2Output }, Behavior));
            HiddenBatchData QueryOutput = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(MatchLayer, QueryEnsembleOutput, Behavior));
            
            MemoryQAHelpData ContextMemHelpData = (MemoryQAHelpData)cg.AddRunner(new SequenceMemoryHelpRunner(ContextEmbedOutput, null, Behavior));
            HiddenBatchData MemOutput = (HiddenBatchData)cg.AddRunner(new ExternalMemoryRunner(QueryOutput, ContextMemHelpData, ContextOutput, ContextOutput, Behavior));


            /************************************************************ Candidate and Answer data *********/
            GeneralBatchInputData CandidateAnswerData = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(answer, Behavior));

            // select candidate entities.
            BiMatchBatchData candidateMatchData = (BiMatchBatchData)cg.AddRunner(new GenerateCandidateRunner(CandidateAnswerData, ContextData, Behavior));

            //cg.AddRunner(new GradientOptimizeRunner(EntityLayer.WeightOptimizer, Behavior));

            // similarity between source text and target text.
            HiddenBatchData simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(MemOutput, 
                new HiddenBatchData(EntityLayer.Neural_In, EntityLayer.Neural_Out, EntityLayer.weight, EntityLayer.WeightOptimizer == null ? null : EntityLayer.WeightOptimizer.Gradient, Behavior.Device), 
                candidateMatchData, SimilarityType.InnerProduct, Behavior));

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                        cg.AddRunner(new ModelDiskDumpRunner(model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\cdssm_rank"));
                    cg.AddObjective(new BayesianRatingRunner(simiOutput.Output.Data, simiOutput.Deriv.Data, candidateMatchData.MatchInfo, candidateMatchData, BuilderParameters.Gamma, Behavior));
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new GenerateSelectionPredictionRunner(candidateMatchData, simiOutput.Output.Data, BuilderParameters.Gamma, Behavior, BuilderParameters.ScoreOutputPath));
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");

            CompositeNNStructure modelStructure = new CompositeNNStructure();
            List<LayerStructure> contextLayers = new List<LayerStructure>();
            LSTMStructure queryD1LSTM = null; LSTMStructure queryD2LSTM = null;

            LayerStructure matchLayer = null;
            LayerStructure entityLayer = null;
            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                int inputDim = DataPanel.WordDim;
                for (int i = 0; i < BuilderParameters.CON_LAYER_DIM.Length; i++)
                {
                    contextLayers.Add(new LayerStructure(inputDim, BuilderParameters.CON_LAYER_DIM[i], A_Func.Tanh, N_Type.Convolution_layer, BuilderParameters.CON_LAYER_WIN[i], 0, true));
                    inputDim = BuilderParameters.CON_LAYER_DIM[i];
                }
                queryD1LSTM = new LSTMStructure(DataPanel.WordDim, BuilderParameters.QUERY_LAYER_DIM, device);
                queryD2LSTM = new LSTMStructure(DataPanel.WordDim, BuilderParameters.QUERY_LAYER_DIM, device);

                matchLayer = new LayerStructure(2 * BuilderParameters.QUERY_LAYER_DIM.Last(), BuilderParameters.CON_LAYER_DIM.Last(), A_Func.Linear, N_Type.Fully_Connected, 1, 0, false);
                entityLayer = new LayerStructure(DataPanel.entityFreqDict.ItemDictSize, BuilderParameters.CON_LAYER_DIM.Last(), A_Func.Linear, N_Type.Fully_Connected, 1, 0, true);
                for (int i = 0; i < contextLayers.Count; i++) modelStructure.AddLayer(contextLayers[i]);
                modelStructure.AddLayer(queryD1LSTM);
                modelStructure.AddLayer(queryD2LSTM);
                modelStructure.AddLayer(matchLayer);
                modelStructure.AddLayer(entityLayer);
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, DeviceType.GPU);
                    for (int i = 0; i < BuilderParameters.CON_LAYER_DIM.Length; i++) contextLayers.Add((LayerStructure)modelStructure.CompositeLinks[i]);
                    queryD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[BuilderParameters.CON_LAYER_DIM.Length];
                    queryD2LSTM = (LSTMStructure)modelStructure.CompositeLinks[BuilderParameters.CON_LAYER_DIM.Length + 1];
                    matchLayer = (LayerStructure)modelStructure.CompositeLinks[BuilderParameters.CON_LAYER_DIM.Length + 2];
                    entityLayer = (LayerStructure)modelStructure.CompositeLinks[BuilderParameters.CON_LAYER_DIM.Length + 3];
                }
            }
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainAnswer,
                        contextLayers, queryD1LSTM, queryD2LSTM, matchLayer, entityLayer, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    //trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    ComputationGraph validCG = null;

                    if (BuilderParameters.IsTestFile)
                        validCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                            contextLayers, queryD1LSTM, queryD2LSTM, matchLayer, entityLayer, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                    double bestValidScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            string model_prefix = Path.Combine(BuilderParameters.ModelOutputPath, string.Format("cnnAtt.iter.{0}", iter));
                            modelStructure.Serialize(new BinaryWriter(new FileStream(model_prefix, FileMode.Create, FileAccess.Write)));
                        }

                        if (validCG != null)
                        {
                            double validScore = validCG.Execute();
                            if (validScore > bestValidScore) { bestValidScore = validScore; bestIter = iter; }
                            Logger.WriteLog("Best Valid Score {0}, At iter {1}", bestValidScore, bestIter);
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, "cnnAtt.best.model"), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }

                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                        contextLayers, queryD1LSTM, queryD2LSTM, matchLayer, entityLayer, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
                    predCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQuery = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TrainAnswer = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestQuery = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TestAnswer = null;

            static ItemFreqIndexDictionary wordFreqDict = null;
            public static ItemFreqIndexDictionary entityFreqDict = null;

            public static int WordDim { get { return 1 + entityFreqDict.ItemDictSize + wordFreqDict.ItemDictSize; } }

            const int WordVocabLimited = 50000;

            static void ExtractVocab(string questFolder, string vocab)
            {
                wordFreqDict = new ItemFreqIndexDictionary("WordVocab");
                entityFreqDict = new ItemFreqIndexDictionary("EntityVocab");

                int fileIdx = 0;
                DirectoryInfo directory = new DirectoryInfo(questFolder);
                foreach (FileInfo sampleFile in directory.EnumerateFiles())
                {
                    using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                    {
                        string url = mreader.ReadLine(); mreader.ReadLine();
                        string context = mreader.ReadLine(); mreader.ReadLine();
                        string query = mreader.ReadLine(); mreader.ReadLine();
                        string answer = mreader.ReadLine(); mreader.ReadLine();

                        foreach (string word in context.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }

                        foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else if (word.StartsWith("@placeholder")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }
                    }
                    if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract Vocab from Corpus {0}", fileIdx); }
                }

                /// keep top 50K vocabary.
                wordFreqDict.Filter(0, WordVocabLimited);
                using (StreamWriter mwriter = new StreamWriter(vocab))
                {
                    entityFreqDict.Save(mwriter);
                    wordFreqDict.Save(mwriter);
                }
            }

            /// <summary>
            /// Unknown Index = 0;
            /// </summary>
            static void ExtractCorpusIndex(string questFolder, string questIndexFile)
            {
                using (StreamWriter indexWriter = new StreamWriter(questIndexFile))
                {
                    int fileIdx = 0;
                    DirectoryInfo directory = new DirectoryInfo(questFolder);
                    foreach (FileInfo sampleFile in directory.EnumerateFiles())
                    {
                        using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                        {
                            string url = mreader.ReadLine(); mreader.ReadLine();
                            string context = mreader.ReadLine(); mreader.ReadLine();
                            string query = mreader.ReadLine(); mreader.ReadLine();
                            string answer = mreader.ReadLine(); mreader.ReadLine();

                            int answerIndex = entityFreqDict.IndexItem(answer);

                            Dictionary<int, float> answerLabel = new Dictionary<int, float>();

                            List<int> contextIndex = new List<int>();
                            int pos = 0;
                            foreach (string word in context.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                if (word.StartsWith("@entity"))
                                {
                                    int idx = entityFreqDict.IndexItem(word); contextIndex.Add(idx + 1);
                                    if (answerIndex == idx) { answerLabel.Add(pos, 1); }
                                    else { answerLabel.Add(pos, 0); }
                                }
                                else { int idx = wordFreqDict.IndexItem(word); contextIndex.Add(idx >= 0 ? idx + entityFreqDict.ItemDictSize + 1 : 0); }
                                pos++;
                            }

                            List<int> queryIndex = new List<int>();
                            foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                if (word.StartsWith("@entity")) { int idx = entityFreqDict.IndexItem(word); queryIndex.Add(idx + 1); }
                                else if (word.StartsWith("@placeholder")) { int idx = entityFreqDict.IndexItem(word); queryIndex.Add(idx + 1); }
                                else { int idx = wordFreqDict.IndexItem(word); queryIndex.Add(idx >= 0 ? idx + entityFreqDict.ItemDictSize + 1 : 0); }
                            }

                            indexWriter.WriteLine("{0}\t{1}\t{2}", string.Join(" ", contextIndex), string.Join(" ", queryIndex), TextUtil.Dict2Str(answerLabel));
                        }
                        if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract Index from Corpus {0}", fileIdx); }
                    }
                }
            }

            static void ExtractCorpusBinary(string questIndexFile, string contextBin, string queryBin, string answerBin, int miniBatchSize)
            {
                SeqSparseBatchData context = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData query = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                GeneralBatchInputData answer = new GeneralBatchInputData(new GeneralBatchInputDataStat() { FeatureType = FeatureDataType.SparseFeature, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);

                BinaryWriter contextWriter = FileUtil.CreateBinaryWrite(contextBin);
                BinaryWriter queryWriter = FileUtil.CreateBinaryWrite(queryBin);
                BinaryWriter answerWriter = FileUtil.CreateBinaryWrite(answerBin);

                using (StreamReader indexReader = new StreamReader(questIndexFile))
                {
                    int lineIdx = 0;
                    while (!indexReader.EndOfStream)
                    {
                        string[] items = indexReader.ReadLine().Split('\t');

                        List<Dictionary<int, float>> contextFea = new List<Dictionary<int, float>>();
                        foreach (string term in items[0].Split(' ')) { var tmp = new Dictionary<int, float>(); tmp[int.Parse(term)] = 1; contextFea.Add(tmp); }
                        context.PushSample(contextFea);

                        List<Dictionary<int, float>> queryFea = new List<Dictionary<int, float>>();
                        foreach (string term in items[1].Split(' ')) { var tmp = new Dictionary<int, float>(); tmp[int.Parse(term)] = 1; queryFea.Add(tmp); }
                        query.PushSample(queryFea);

                        Dictionary<int, float> answerFea = TextUtil.Str2Dict(items[2]);
                        answer.PushSample(answerFea);

                        if (context.BatchSize >= miniBatchSize)
                        {
                            context.PopBatchToStat(contextWriter);
                            query.PopBatchToStat(queryWriter);
                            answer.PopBatchToStat(answerWriter);
                        }

                        if (++lineIdx % 1000 == 0) { Console.WriteLine("Extract Binary from Corpus {0}", lineIdx); }
                    }
                    context.PopBatchCompleteStat(contextWriter);
                    query.PopBatchCompleteStat(queryWriter);
                    answer.PopBatchCompleteStat(answerWriter);
    
                }
            }

            public static void Init()
            {
                #region Preprocess Data.
                /// Step 1 : Extract Vocab from training Corpus.
                if (!File.Exists(BuilderParameters.Vocab)) ExtractVocab(BuilderParameters.TrainFolder, BuilderParameters.Vocab);
                else
                {
                    using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab))
                    {
                        entityFreqDict = new ItemFreqIndexDictionary(mreader);
                        wordFreqDict = new ItemFreqIndexDictionary(mreader);
                    }
                }

                /// Step 2 : Corpus 2 Index.
                if (BuilderParameters.IsTrainFile && !File.Exists(BuilderParameters.TrainIndexData))
                {
                    ExtractCorpusIndex(BuilderParameters.TrainFolder, BuilderParameters.TrainIndexData);
                }
                if (BuilderParameters.IsTestFile && !File.Exists(BuilderParameters.TestIndexData))
                {
                    ExtractCorpusIndex(BuilderParameters.TestFolder, BuilderParameters.TestIndexData);
                }

                /// Step 3 : Index 2 Binary Data.
                if (BuilderParameters.IsTrainFile &&
                    (!File.Exists(BuilderParameters.TrainContextBinary) || !File.Exists(BuilderParameters.TrainQueryBinary) || !File.Exists(BuilderParameters.TrainAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.TrainIndexData, BuilderParameters.TrainContextBinary, BuilderParameters.TrainQueryBinary, BuilderParameters.TrainAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                if (BuilderParameters.IsTestFile &&
                    (!File.Exists(BuilderParameters.TestContextBinary) || !File.Exists(BuilderParameters.TestQueryBinary) || !File.Exists(BuilderParameters.TestAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.TestIndexData, BuilderParameters.TestContextBinary, BuilderParameters.TestQueryBinary, BuilderParameters.TestAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                #endregion.

                if (BuilderParameters.IsTrainFile)
                {
                    TrainContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBinary, BuilderParameters.TrainContextBinary+".cursor");
                    TrainQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBinary, BuilderParameters.TrainQueryBinary + ".cursor");
                    TrainAnswer = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TrainAnswerBinary, BuilderParameters.TrainAnswerBinary + ".cursor");

                    TrainContext.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQuery.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainAnswer.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);

                    Console.WriteLine("Context Stat {0}", TrainContext.Stat.ToString());
                    Console.WriteLine("Query Stat {0}", TrainQuery.Stat.ToString());
                    Console.WriteLine("Answer Stat {0}", TrainAnswer.Stat.ToString());
                }

                if (BuilderParameters.IsTestFile)
                {
                    TestContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestContextBinary);
                    TestQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestQueryBinary);
                    TestAnswer = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TestAnswerBinary);

                    TestContext.InitThreadSafePipelineCashier(64, false);
                    TestQuery.InitThreadSafePipelineCashier(64, false);
                    TestAnswer.InitThreadSafePipelineCashier(64, false);

                    Console.WriteLine("Context Stat {0}", TestContext.Stat.ToString());
                    Console.WriteLine("Query Stat {0}", TestQuery.Stat.ToString());
                    Console.WriteLine("Answer Stat {0}", TestAnswer.Stat.ToString());
                }
            }
        }
    }
}