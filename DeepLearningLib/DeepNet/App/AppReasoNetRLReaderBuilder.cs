﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class AppReasoNetRLReaderBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));
                Argument.Add("VOCAB-SIZE", new ParameterArgument("50000", "Vocab Size"));

                Argument.Add("TRAIN-FOLDER", new ParameterArgument(string.Empty, "Training Folder"));
                Argument.Add("TEST-FOLDER", new ParameterArgument(string.Empty, "Testing Folder"));
                Argument.Add("VALID-FOLDER", new ParameterArgument(string.Empty, "Validation Folder"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));
                
                Argument.Add("EMBED-LAYER-DIM", new ParameterArgument("300,300", "DNN Layer Dim"));
                Argument.Add("EMBED-LAYER-WIN", new ParameterArgument("5,5", "DNN Layer WindowSize"));
                Argument.Add("EMBED-LAYER-AF", new ParameterArgument("1,1", "DNN Layer AF" + ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("EMBED-LAYER-DROPOUT", new ParameterArgument("0,0", "DNN Layer Dropout."));
                Argument.Add("WORD-EMBEDDING", new ParameterArgument(string.Empty, "Word Embedding File"));

                Argument.Add("CON-LSTM-DIM", new ParameterArgument("300", "Lstm Dimension"));
                Argument.Add("QUERY-LSTM-DIM", new ParameterArgument("300", "Lstm Dimension"));
                //Argument.Add("IS-ENTITY-LSTM", new ParameterArgument("1", "Con LSTM on  0 : word; 1 : entity"));
                //Argument.Add("IS-ENTITY-SHUFFLE", new ParameterArgument("1", "0 : nonShuffleEntity; 1 : ShuffleEntity."));
                //Argument.Add("RECURRENT-GATE-TYPE", new ParameterArgument(((int)Recurrent_Unit.LSTM).ToString(), ParameterUtil.EnumValues(typeof(Recurrent_Unit))));

                Argument.Add("ATT-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ATT-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

                Argument.Add("ANS-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ANS-TYPE", new ParameterArgument(((int)CrossSimType.Cosine).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("3", "Recurrent Steps"));
                Argument.Add("RECURRENT-POOL", new ParameterArgument((((int)PoolingType.AVG)).ToString(), "Recurrent Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));

                // Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            /// <summary>
            /// DNN Run Mode.
            /// </summary>
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string Vocab { get { return Argument["VOCAB"].Value == string.Empty ? string.Format("{0}.{1}.vocab", TrainFolder, VocabSize) : Argument["VOCAB"].Value; } }
            public static int VocabSize { get { return int.Parse(Argument["VOCAB-SIZE"].Value); } }

            #region train, test, validation dataset.
            public static string TrainFolder { get { return Argument["TRAIN-FOLDER"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTrainFile { get { return (!TrainFolder.Equals(string.Empty)); } }
            public static string TrainIndexData { get { return string.Format("{0}.{1}.idx.tsv", TrainFolder, VocabSize); } }
            public static string TrainContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", TrainFolder, VocabSize, MiniBatchSize); } }
            public static string TrainQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", TrainFolder, VocabSize, MiniBatchSize); } }
            public static string TrainAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", TrainFolder, VocabSize, MiniBatchSize); } }

            public static string TestFolder { get { return Argument["TEST-FOLDER"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTestFile { get { return (!TestFolder.Equals(string.Empty)); } }
            public static string TestIndexData { get { return string.Format("{0}.{1}.idx.tsv", TestFolder, VocabSize); } }
            public static string TestContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", TestFolder, VocabSize, MiniBatchSize); } }
            public static string TestQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", TestFolder, VocabSize, MiniBatchSize); } }
            public static string TestAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", TestFolder, VocabSize, MiniBatchSize); } }

            public static string ValidFolder { get { return Argument["VALID-FOLDER"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsValidFile { get { return (!ValidFolder.Equals(string.Empty)); } }
            public static string ValidIndexData { get { return string.Format("{0}.{1}.idx.tsv", ValidFolder, VocabSize); } }
            public static string ValidContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            public static string ValidQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            public static string ValidAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            #endregion.

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static int[] EMBED_LAYER_DIM { get { return Argument["EMBED-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] EMBED_ACTIVATION { get { return Argument["EMBED-LAYER-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] EMBED_LAYER_WIN { get { return Argument["EMBED-LAYER-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] EMBED_DROPOUT 
            {
                get
                {
                    if (Argument["EMBED-LAYER-DROPOUT"].Value.Equals(string.Empty)) return EMBED_LAYER_DIM.Select(i => 0.0f).ToArray();
                    else return Argument["EMBED-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray();
                }
            }
            public static string InitWordEmbedding { get { return Argument["WORD-EMBEDDING"].Value; } }

            public static int QUERY_LSTM_DIM { get { return int.Parse(Argument["QUERY-LSTM-DIM"].Value); } }
            public static int CON_LSTM_DIM { get { return int.Parse(Argument["CON-LSTM-DIM"].Value); } }
            //public static bool IS_ENTITY_SHUFFLE { get { return int.Parse(Argument["IS-ENTITY-SHUFFLE"].Value) > 0; } }
            //public static bool IS_ENTITY_LSTM { get { return int.Parse(Argument["IS-ENTITY-LSTM"].Value) > 0; } }
            //public static Recurrent_Unit Recurrent_Gate_Type { get { return (Recurrent_Unit)int.Parse(Argument["RECURRENT-GATE-TYPE"].Value); } }

            public static int ATT_HID_DIM { get { return int.Parse(Argument["ATT-HID"].Value); } }
            public static CrossSimType AttType { get { return (CrossSimType)int.Parse(Argument["ATT-TYPE"].Value); } }

            public static int ANS_HID_DIM { get { return int.Parse(Argument["ANS-HID"].Value); } }
            public static CrossSimType AnsType { get { return (CrossSimType)int.Parse(Argument["ANS-TYPE"].Value); } }

            public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            public static PoolingType RECURRENT_POOL { get { return (PoolingType)int.Parse(Argument["RECURRENT-POOL"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
        }

        public override BuilderType Type { get { return BuilderType.APP_REASONET_RL_READER; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        enum CrossSimType { Addition = 0, Product = 1, Cosine = 2 };
        class SoftLinearSimRunner : StructRunner
        {
            HiddenBatchData Status { get; set; }

            SeqDenseBatchData Mem { get; set; }

            BiMatchBatchData MatchData { get; set; }

            HiddenBatchData HiddenStatus = null;
            HiddenBatchData HiddenMatch = null;
            CudaPieceFloat HiddenMatchTmp = null;

            MLPAttentionStructure AttStruct;

            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            CrossSimType AttType = CrossSimType.Addition;

            public SoftLinearSimRunner(HiddenBatchData status, SeqDenseBatchData mem, BiMatchBatchData matchData, CrossSimType attType,
                MLPAttentionStructure attStruct, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Status = status;
                Mem = mem;
                MatchData = matchData;

                AttType = attType;
                AttStruct = attStruct;

                HiddenStatus = new HiddenBatchData(status.MAX_BATCHSIZE, AttStruct.HiddenDim, Behavior.RunMode, Behavior.Device);
                HiddenMatch = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, AttStruct.HiddenDim, Behavior.RunMode, Behavior.Device);

                Output = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);

                if (AttType == CrossSimType.Product)
                {
                    HiddenMatchTmp = new CudaPieceFloat(MatchData.Stat.MAX_MATCH_BATCHSIZE * AttStruct.HiddenDim, true, Behavior.Device == DeviceType.GPU);
                }
            }

            public override void Forward()
            {
                ComputeLib.Sgemm(Status.Output.Data, 0, AttStruct.Wi, 0, HiddenStatus.Output.Data, 0, Status.BatchSize, AttStruct.InputDim, AttStruct.HiddenDim, 0, 1, false, false);

                if (AttType == CrossSimType.Addition)
                {
                    // hiddenMatch = hiddenStatus + Mem
                    ComputeLib.Matrix_AdditionMask(HiddenStatus.Output.Data, 0, MatchData.SrcIdx, 0,
                                                   Mem.SentOutput, 0, MatchData.TgtIdx, 0,
                                                   HiddenMatch.Output.Data, 0, CudaPieceInt.Empty, 0,
                                                   AttStruct.HiddenDim, MatchData.MatchSize, 1, 1, 0);
                }
                else if (AttType == CrossSimType.Product)
                {
                    ComputeLib.ElementwiseProductMask(HiddenStatus.Output.Data, 0,
                                                      Mem.SentOutput, 0,
                                                      HiddenMatch.Output.Data, 0,
                                                      MatchData.SrcIdx, 0, MatchData.TgtIdx, 0, CudaPieceInt.Empty, 0,
                                                      MatchData.MatchSize, AttStruct.HiddenDim, 0, 1);
                }
                // hiddenMatch = tanh(hiddenMatch)
                ComputeLib.Tanh(HiddenMatch.Output.Data, 0, HiddenMatch.Output.Data, 0, AttStruct.HiddenDim * MatchData.MatchSize);

                Output.BatchSize = MatchData.MatchSize;

                // attention = address * Wa.
                ComputeLib.Sgemv(HiddenMatch.Output.Data, AttStruct.Wa, MatchData.MatchSize, HiddenMatch.Dim, Output.Output.Data, false, 0, 1);

                ComputeLib.Tanh(Output.Output.Data, 0, Output.Output.Data, 0, Output.BatchSize);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.DerivTanh(Output.Output.Data, 0, Output.Deriv.Data, 0, Output.Deriv.Data, 0, Output.BatchSize, 0, 1);

                // HiddenOutput = attentionDeriv * Wa'.
                ComputeLib.Sgemm(Output.Deriv.Data, 0, AttStruct.Wa, 0, HiddenMatch.Deriv.Data, 0, MatchData.MatchSize, 1, HiddenMatch.Dim, 0, 1, false, false);

                // HiddenOutput = HiddenOutput * tanh(address)'
                ComputeLib.DerivTanh(HiddenMatch.Output.Data, 0, HiddenMatch.Deriv.Data, 0, HiddenMatch.Deriv.Data, 0, MatchData.MatchSize * HiddenMatch.Dim, 0, 1);

                if (AttType == CrossSimType.Addition)
                {
                    // hiddenDeriv = sum(addressDeriv)
                    ComputeLib.Matrix_AdditionMask(HiddenMatch.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                   Mem.SentDeriv, 0, MatchData.TgtIdx, 0,
                                                   Mem.SentDeriv, 0, MatchData.TgtIdx, 0,
                                                   Mem.Dim, MatchData.MatchSize, 1, 1, 0);

                    //HiddenStatus.Deriv.Data.SyncToCPU();
                    ComputeLib.ColumnWiseSumMask(HiddenMatch.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                          CudaPieceFloat.Empty, MatchData.Src2MatchIdx, MatchData.SrcSize,
                                                          HiddenStatus.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                          MatchData.MatchSize, HiddenStatus.Dim, 0, 1);
                }
                else if (AttType == CrossSimType.Product)
                {
                    ComputeLib.ElementwiseProductMask(HiddenMatch.Deriv.Data, 0,
                                                      HiddenStatus.Output.Data, 0,
                                                      Mem.SentDeriv, 0,
                                                      CudaPieceInt.Empty, 0, MatchData.SrcIdx, 0, MatchData.TgtIdx, 0,
                                                      MatchData.MatchSize, Mem.Dim, 1, 1);

                    ComputeLib.ElementwiseProductMask(HiddenMatch.Deriv.Data, 0,
                                                      Mem.SentOutput, 0,
                                                      HiddenMatchTmp, 0,
                                                      CudaPieceInt.Empty, 0, MatchData.TgtIdx, 0, CudaPieceInt.Empty, 0,
                                                      MatchData.MatchSize, Mem.Dim, 0, 1);

                    ComputeLib.ColumnWiseSumMask(HiddenMatchTmp, 0, CudaPieceInt.Empty, 0,
                                                          CudaPieceFloat.Empty, MatchData.Src2MatchIdx, MatchData.SrcSize,
                                                          HiddenStatus.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                          MatchData.MatchSize, HiddenStatus.Dim, 0, 1);
                }
                //float hiddenStatusDerivNorm = ComputeLib.L2Norm(HiddenStatus.Deriv.Data, HiddenStatus.Dim * MatchData.SrcSize);
                //Console.WriteLine("hidden status deriv norm {0}, len {1}, avg {2}", hiddenStatusDerivNorm, HiddenStatus.Dim * MatchData.SrcSize,
                //    hiddenStatusDerivNorm / Math.Sqrt(HiddenStatus.Dim * MatchData.SrcSize));
                //HiddenStatus.Deriv.Data.SyncToCPU();

                // inputDeriv += hiddenDeriv * wi'
                ComputeLib.Sgemm(HiddenStatus.Deriv.Data, 0,
                                          AttStruct.Wi, 0,
                                          Status.Deriv.Data, 0,
                                          Status.BatchSize, AttStruct.HiddenDim, AttStruct.InputDim, 1, 1, false, true);

                //Status.Deriv.Data.SyncToCPU();
                //Mem.SentDeriv.SyncToCPU();
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    AttStruct.WiMatrixOptimizer.BeforeGradient();
                    AttStruct.WaMatrixOptimizer.BeforeGradient();
                }
                //  Wa  +=  attentionDeriv * address.
                ComputeLib.Sgemm(Output.Deriv.Data, 0, HiddenMatch.Output.Data, 0, AttStruct.WaMatrixOptimizer.Gradient, 0,
                                          1, MatchData.MatchSize, HiddenMatch.Dim, 1, AttStruct.WaMatrixOptimizer.GradientStep, false, false);

                ComputeLib.Sgemm(Status.Output.Data, 0,
                                          HiddenStatus.Deriv.Data, 0,
                                          AttStruct.WiMatrixOptimizer.Gradient, 0,
                                          Status.BatchSize, AttStruct.InputDim, AttStruct.HiddenDim,
                                          1, AttStruct.WiMatrixOptimizer.GradientStep, true, false);
                if (!IsDelegateOptimizer)
                {
                    AttStruct.WiMatrixOptimizer.AfterGradient();
                    AttStruct.WaMatrixOptimizer.AfterGradient();
                }
            }
        }

        class SoftCosineSimRunner : StructRunner
        {
            HiddenBatchData Status { get; set; }

            SeqDenseBatchData Mem { get; set; }

            BiMatchBatchData MatchData { get; set; }

            SimilarityRunner SimRunner = null;

            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            public SoftCosineSimRunner(HiddenBatchData status, SeqDenseBatchData mem, BiMatchBatchData matchData, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Status = status;
                Mem = mem;
                MatchData = matchData;

                SimRunner = new SimilarityRunner(status, new HiddenBatchData(mem.MAX_SENTSIZE, mem.Dim, mem.SentOutput, mem.SentDeriv, Behavior.Device),
                    matchData, SimilarityType.CosineSimilarity, Behavior);

                Output = SimRunner.Output;
            }

            public override void Forward()
            {
                SimRunner.Forward();
            }

            public override void CleanDeriv()
            {
                SimRunner.CleanDeriv();
            }

            public override void Backward(bool cleanDeriv)
            {
                SimRunner.Backward(cleanDeriv);
            }

            public override void Update()
            {
                SimRunner.IsDelegateOptimizer = IsDelegateOptimizer;
                SimRunner.Update();
            }
        }

        /// <summary>
        /// Memory Retrieval Runner.
        /// </summary>
        class MemoryRetrievalRunner : StructRunner
        {
            HiddenBatchData Address { get; set; }
            HiddenBatchData SoftmaxAddress { get; set; }

            SeqDenseBatchData Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }
            float Gamma { get; set; }
            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            public MemoryRetrievalRunner(HiddenBatchData address, SeqDenseBatchData memory, BiMatchBatchData matchData, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Address = address;
                Memory = memory;
                MatchData = matchData;
                Gamma = gamma;
                SoftmaxAddress = new HiddenBatchData(Address.MAX_BATCHSIZE, Address.Dim, Behavior.RunMode, Behavior.Device);
                Output = new HiddenBatchData(MatchData.Stat.MAX_SRC_BATCHSIZE, Memory.Dim, Behavior.RunMode, Behavior.Device);
            }

            public override void Forward()
            {
                Output.BatchSize = MatchData.SrcSize;

                // softmax 
                // softmax of attention
                ComputeLib.SparseSoftmax(MatchData.Src2MatchIdx, Address.Output.Data, SoftmaxAddress.Output.Data, Gamma, MatchData.SrcSize);

                ComputeLib.ColumnWiseSumMask(Memory.SentOutput, 0, MatchData.TgtIdx, 0,
                    SoftmaxAddress.Output.Data, MatchData.Src2MatchIdx, MatchData.SrcSize,
                    Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                    Memory.SentSize, Memory.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                //float oDeriv_norm2 = ComputeLib.L2Norm(Output.Deriv.Data, Output.Dim * Output.BatchSize);

                // outputDeriv -> memoryContentDeriv.
                ComputeLib.Scale_MatrixMask(Output.Deriv.Data, 0, MatchData.SrcIdx, 0,
                                            Memory.SentDeriv, 0, MatchData.TgtIdx, 0,
                                            Memory.Dim, MatchData.MatchSize, SoftmaxAddress.Output.Data, 1);

                //float aDeriv_norm2 = ComputeLib.L2Norm(SoftmaxAddress.Output.Data, SoftmaxAddress.Dim * Memory.SentSize);
                //float mDeriv_norm2 = ComputeLib.L2Norm(Memory.SentDeriv, Memory.Dim * Memory.SentSize);
                //Memory.SentDeriv.SyncToCPU();
                //SoftmaxAddress.Output.Data.SyncToCPU();
                //Output.Deriv.Data.SyncToCPU();

                // outputDeriv -> AddressDeriv.
                ComputeLib.Inner_Product_Matching(Output.Deriv.Data, 0, Memory.SentOutput, 0, SoftmaxAddress.Deriv.Data, 0,
                    MatchData.SrcIdx, MatchData.TgtIdx, Output.BatchSize, Memory.SentSize, MatchData.MatchSize,
                    Memory.Dim, Util.GPUEpsilon);

                ComputeLib.DerivSparseMultiClassSoftmax(MatchData.Src2MatchIdx,
                    SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, Gamma, MatchData.SrcSize);

                ComputeLib.Add_Vector(Address.Deriv.Data, SoftmaxAddress.Deriv.Data, MatchData.MatchSize, 1, 1);
            }
        }

        /// <summary>
        /// GRU Query Runner.
        /// </summary>
        public class GRUQueryRunner : StructRunner<GRUCell, HiddenBatchData>
        {
            public HiddenBatchData Query { get; set; }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            HiddenBatchData GateR; //reset gate
            HiddenBatchData GateZ; //update gate
            HiddenBatchData HHat; //new memory
            HiddenBatchData RestH; //new memory

            CudaPieceFloat Ones;
            CudaPieceFloat Tmp;
            public GRUQueryRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
            {
                Query = query;

                Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

                GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

                Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
                Ones.Init(1);

                Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
            }

            public override void Forward()
            {
                //SubQuery.BatchSize = 1;
                /*Wr X -> GateR*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateR*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Query.BatchSize, Model.HiddenStateDim);

                /*Wz X -> GateZ*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateZ*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Query.BatchSize, Model.HiddenStateDim);

                /*Wh X -> HHat*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Query.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Query.BatchSize, Model.HiddenStateDim);

                Output.BatchSize = Query.BatchSize;

                //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
                // tmp = (1 - GateZ(t))
                ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
                                               GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

                // NewQuery = GateZ(t) * HHat
                ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);

                // NewQuery += tmp * h_t-1
                ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Query.BatchSize, Model.HiddenStateDim, 1);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
                ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Query.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

                ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);

                ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
                ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);

                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.BeforeGradient();
                    Model.WrMatrixOptimizer.BeforeGradient();
                    Model.WzMatrixOptimizer.BeforeGradient();

                    Model.UhMatrixOptimizer.BeforeGradient();
                    Model.UrMatrixOptimizer.BeforeGradient();
                    Model.UzMatrixOptimizer.BeforeGradient();

                    Model.BhMatrixOptimizer.BeforeGradient();
                    Model.BrMatrixOptimizer.BeforeGradient();
                    Model.BzMatrixOptimizer.BeforeGradient();

                }
                ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);

                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.AfterGradient();
                    Model.WrMatrixOptimizer.AfterGradient();
                    Model.WzMatrixOptimizer.AfterGradient();

                    Model.UhMatrixOptimizer.AfterGradient();
                    Model.UrMatrixOptimizer.AfterGradient();
                    Model.UzMatrixOptimizer.AfterGradient();

                    Model.BhMatrixOptimizer.AfterGradient();
                    Model.BrMatrixOptimizer.AfterGradient();
                    Model.BzMatrixOptimizer.AfterGradient();
                }
            }
        }

        /// <summary>
        /// Fix Iteration, SoftAttention, Supervised Training.
        /// </summary>
        class ReasonNetRunner : StructRunner
        {
            /// <summary>
            /// Input Query;
            /// </summary>
            HiddenBatchData InitStatus { get; set; }

            /// <summary>
            /// Memory.
            /// </summary>
            SeqDenseBatchData Memory { get; set; }

            /// <summary>
            /// Status 2 Memory Match Data.
            /// </summary>
            BiMatchBatchData MatchData { get; set; }

            PoolingType ReasonPool = BuilderParameters.RECURRENT_POOL;

            public ReasonNetRunner(
                HiddenBatchData initStatus, SeqDenseBatchData mem, BiMatchBatchData matchData, int maxIterateNum,
                GRUCell gruCell, MLPAttentionStructure attStruct, MLPAttentionStructure ansStruct, LayerStructure termStruct, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                InitStatus = initStatus;
                Memory = mem;
                MatchData = matchData;
                MaxIterationNum = maxIterateNum;

                GruCell = gruCell;
                AttStruct = attStruct;
                AnsStruct = ansStruct;
                TermStruct = termStruct;

                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product)
                {
                    AttHidden = new SeqDenseBatchData( new SequenceDataStat()
                        {
                            MAX_BATCHSIZE = Memory.Stat.MAX_BATCHSIZE,
                            MAX_SEQUENCESIZE = Memory.Stat.MAX_SEQUENCESIZE,
                            FEATURE_DIM = attStruct.HiddenDim
                        }, Memory.SampleIdx, Memory.SentMargin, Behavior.Device);

                    SoftLinearSimRunner initAttRunner = new SoftLinearSimRunner(InitStatus, AttHidden, MatchData, BuilderParameters.AttType, AttStruct, Behavior);
                    AttRunner.Add(initAttRunner);
                }
                else if (BuilderParameters.AttType == CrossSimType.Cosine)
                {
                    SoftCosineSimRunner initAttRunner = new SoftCosineSimRunner(InitStatus, Memory, MatchData, Behavior);
                    AttRunner.Add(initAttRunner);
                }

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product)
                {
                    AnsHidden = new SeqDenseBatchData(new SequenceDataStat()
                        {
                            MAX_BATCHSIZE = Memory.Stat.MAX_BATCHSIZE,
                            MAX_SEQUENCESIZE = Memory.Stat.MAX_SEQUENCESIZE,
                            FEATURE_DIM = ansStruct.HiddenDim
                        }, Memory.SampleIdx, Memory.SentMargin, Behavior.Device);

                    SoftLinearSimRunner initAnsRunner = new SoftLinearSimRunner(InitStatus, AnsHidden, MatchData, BuilderParameters.AnsType, AnsStruct, Behavior);
                    AnsRunner.Add(initAnsRunner);
                }
                else if (BuilderParameters.AnsType == CrossSimType.Cosine)
                {
                    SoftCosineSimRunner initAnsRunner = new SoftCosineSimRunner(InitStatus, Memory, MatchData, Behavior);
                    AnsRunner.Add(initAnsRunner);
                }

                HiddenBatchData lastStatus = InitStatus;
                TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(termStruct, lastStatus, Behavior));


                HiddenBatchData lastAtt = (HiddenBatchData)AttRunner[0].Output;

                for (int i = 0; i < maxIterateNum - 1; i++)
                {
                    MemoryRetrievalRunner xRunner = new MemoryRetrievalRunner(lastAtt, Memory, MatchData, gamma, Behavior);
                    MemRetrievalRunner.Add(xRunner);

                    GRUQueryRunner yRunner = new GRUQueryRunner(gruCell, lastStatus, xRunner.Output, Behavior);
                    StatusRunner.Add(yRunner);
                    lastStatus = yRunner.Output;

                    if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product)
                    {
                        SoftLinearSimRunner attRunner = new SoftLinearSimRunner(lastStatus, AttHidden, MatchData, BuilderParameters.AttType, AttStruct, Behavior);
                        AttRunner.Add(attRunner);
                        lastAtt = attRunner.Output;
                    }
                    else if (BuilderParameters.AttType == CrossSimType.Cosine)
                    {
                        SoftCosineSimRunner attRunner = new SoftCosineSimRunner(lastStatus, Memory, MatchData, Behavior);
                        AttRunner.Add(attRunner);
                        lastAtt = attRunner.Output;
                    }

                    if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product)
                    {
                        SoftLinearSimRunner ansRunner = new SoftLinearSimRunner(lastStatus, AnsHidden, MatchData, BuilderParameters.AnsType, AnsStruct, Behavior);
                        AnsRunner.Add(ansRunner);
                    }
                    else if (BuilderParameters.AnsType == CrossSimType.Cosine)
                    {
                        SoftCosineSimRunner ansRunner = new SoftCosineSimRunner(lastStatus, Memory, MatchData, Behavior);
                        AnsRunner.Add(ansRunner);
                    }

                    TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(termStruct, lastStatus, Behavior));
                }

                if (ReasonPool == PoolingType.AVG || ReasonPool == PoolingType.ATT || ReasonPool == PoolingType.TREE)
                {
                    FinalAns = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
                    TerminalProb = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, MaxIterationNum, DNNRunMode.Train, Behavior.Device);
                }
                else if (ReasonPool == PoolingType.LAST)
                {
                    FinalAns = AnsData.Last();
                }
                else if (ReasonPool == PoolingType.RL)
                {
                    AnswerProb = new HiddenBatchData[MaxIterationNum];
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i] = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
                    }
                    FinalAns = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
                }
            }

            /// <summary>
            /// maximum iteration number.
            /// </summary>
            int MaxIterationNum { get; set; }

            GRUCell GruCell = null;
            MLPAttentionStructure AttStruct = null;
            MLPAttentionStructure AnsStruct = null;
            LayerStructure TermStruct = null;

            SeqDenseBatchData AttHidden = null;
            List<StructRunner> AttRunner = new List<StructRunner>();

            SeqDenseBatchData AnsHidden = null;
            List<StructRunner> AnsRunner = new List<StructRunner>();

            List<FullyConnectHiddenRunner<HiddenBatchData>> TerminalRunner = new List<FullyConnectHiddenRunner<HiddenBatchData>>();

            List<MemoryRetrievalRunner> MemRetrievalRunner = new List<MemoryRetrievalRunner>();
            List<GRUQueryRunner> StatusRunner = new List<GRUQueryRunner>();

            public HiddenBatchData[] AnsData { get { return AnsRunner.Select(i => (HiddenBatchData)i.Output).ToArray(); } }
            public HiddenBatchData[] AnswerProb = null;

            public HiddenBatchData TerminalProb = null;
            public HiddenBatchData FinalAns = null;

            public override void Forward()
            {
                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product)
                {
                    AttHidden.BatchSize = Memory.BatchSize;
                    AttHidden.SentSize = Memory.SentSize;
                    ComputeLib.Sgemm(Memory.SentOutput, 0, AttStruct.Wm, 0, AttHidden.SentOutput, 0,
                                     Memory.SentSize, Memory.Dim, AttStruct.HiddenDim, 0, 1, false, false);
                    if (AttStruct.IsBias) ComputeLib.Matrix_Add_Linear(AttHidden.SentOutput, AttStruct.B, Memory.SentSize, AttStruct.HiddenDim);
                }
                AttRunner[0].Forward();

                if(BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product)
                {
                    AnsHidden.BatchSize = Memory.BatchSize;
                    AnsHidden.SentSize = Memory.SentSize;
                    ComputeLib.Sgemm(Memory.SentOutput, 0, AnsStruct.Wm, 0, AnsHidden.SentOutput, 0,
                                     Memory.SentSize, Memory.Dim, AnsStruct.HiddenDim, 0, 1, false, false);
                    if(AnsStruct.IsBias) ComputeLib.Matrix_Add_Linear(AnsHidden.SentOutput, AnsStruct.B, Memory.SentSize, AnsStruct.HiddenDim);
                }
                AnsRunner[0].Forward();

                TerminalRunner[0].Forward();
                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Forward();
                    // update Status.
                    StatusRunner[i].Forward();
                    // take attention
                    AttRunner[i + 1].Forward();
                    // take answer.
                    AnsRunner[i + 1].Forward();
                    // take terminal gate or not.
                    TerminalRunner[i + 1].Forward();
                }

                #region Avg Pooling.
                if (ReasonPool == PoolingType.AVG)
                {
                    FinalAns.BatchSize = AnsData[0].BatchSize;
                    Array.Clear(FinalAns.Output.Data.MemPtr, 0, FinalAns.BatchSize);
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnsData[i].Output.Data.SyncToCPU(AnsData[i].BatchSize);

                        for (int b = 0; b < MatchData.SrcSize; b++)
                        {
                            int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];
                            float p = 1.0f / MaxIterationNum; //TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i];

                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                FinalAns.Output.Data.MemPtr[m] += p * AnsData[i].Output.Data.MemPtr[m];
                            }
                        }
                    }
                    FinalAns.Output.Data.SyncFromCPU(FinalAns.BatchSize);
                }
                #endregion.

                #region Att Pooling.
                else if (ReasonPool == PoolingType.ATT)
                {
                    TerminalProb.BatchSize = TerminalRunner[0].Output.BatchSize;
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        TerminalRunner[i].Output.Output.Data.SyncToCPU(TerminalRunner[i].Output.BatchSize);
                        for (int b = 0; b < TerminalRunner[i].Output.BatchSize; b++)
                        {
                            TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i] = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                        }
                    }
                    TerminalProb.Output.Data.SyncFromCPU(TerminalProb.BatchSize * MaxIterationNum);
                    ComputeLib.SoftMax(TerminalProb.Output.Data, TerminalProb.Output.Data, MaxIterationNum, TerminalProb.BatchSize, 1);
                    TerminalProb.Output.Data.SyncToCPU(TerminalProb.BatchSize * MaxIterationNum);

                    FinalAns.BatchSize = AnsData[0].BatchSize;
                    Array.Clear(FinalAns.Output.Data.MemPtr, 0, FinalAns.BatchSize);
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnsData[i].Output.Data.SyncToCPU(AnsData[i].BatchSize);

                        for (int b = 0; b < MatchData.SrcSize; b++)
                        {
                            int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];
                            float p = TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i];

                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                FinalAns.Output.Data.MemPtr[m] += p * AnsData[i].Output.Data.MemPtr[m];
                            }
                        }
                    }
                    FinalAns.Output.Data.SyncFromCPU(FinalAns.BatchSize);
                }
                #endregion.

                #region RL Pooling.
                else if (ReasonPool == PoolingType.RL)
                {
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        ComputeLib.Logistic(TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.BatchSize, 1);
                        ComputeLib.ClipVector(TerminalRunner[i].Output.Output.Data, TerminalRunner[i].Output.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                        TerminalRunner[i].Output.Output.Data.SyncToCPU(TerminalRunner[i].Output.BatchSize);
                        AnsData[i].Output.Data.SyncToCPU(AnsData[i].BatchSize);
                    }

                    FinalAns.BatchSize = AnsData.Last().BatchSize;
                    for (int b = 0; b < MatchData.SrcSize; b++)
                    {
                        int max_iter = 0;
                        double max_p = 0;

                        float acc_log_t = 0;
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float t_i = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                            float p = acc_log_t + (float)Math.Log(t_i);
                            if (i == MaxIterationNum - 1) p = acc_log_t;

                            AnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
                            acc_log_t += (float)Math.Log(1 - t_i);

                            if (Math.Exp(p) > max_p)
                            {
                                max_p = Math.Exp(p);
                                max_iter = i;
                            }
                        }

                        int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                        int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];

                        for (int m = matchBgn; m < matchEnd; m++)
                        {
                            FinalAns.Output.Data.MemPtr[m] = AnsData[max_iter].Output.Data.MemPtr[m];
                        }
                    }
                    FinalAns.Output.Data.SyncFromCPU(FinalAns.BatchSize);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i].BatchSize = MatchData.SrcSize;
                        AnswerProb[i].Output.Data.SyncFromCPU(AnswerProb[i].BatchSize);
                    }
                }
                #endregion.
                else
                {
                    FinalAns = AnsData.Last();
                }
            }
            public override void CleanDeriv()
            {
                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product)
                {
                    ComputeLib.Zero(AttHidden.SentDeriv, AttHidden.SentSize * AttHidden.Dim);
                }

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product)
                {
                    ComputeLib.Zero(AnsHidden.SentDeriv, AnsHidden.SentSize * AnsHidden.Dim);
                }


                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    TerminalRunner[i + 1].CleanDeriv();
                    // take Attention
                    AttRunner[i + 1].CleanDeriv();

                    // take Answer.
                    AnsRunner[i + 1].CleanDeriv();

                    // update Status.
                    StatusRunner[i].CleanDeriv();

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].CleanDeriv();
                }
                AttRunner[0].CleanDeriv();
                AnsRunner[0].CleanDeriv();
                TerminalRunner[0].CleanDeriv();
                if (ReasonPool == PoolingType.AVG)
                {
                    ComputeLib.Zero(FinalAns.Deriv.Data, FinalAns.BatchSize);
                }
            }

            /// <summary>
            /// Stage 1 : Iterative Attention Model (Supervised Training).
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                if (ReasonPool == PoolingType.AVG)
                {
                    FinalAns.Deriv.Data.SyncToCPU(FinalAns.BatchSize);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(AnsData[i].Deriv.Data.MemPtr, 0, AnsData[i].BatchSize);

                        for (int b = 0; b < MatchData.SrcSize; b++)
                        {
                            int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];

                            float p = 1.0f / MaxIterationNum;
                            // TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i];
                            //float pDeriv = 0;

                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                AnsData[i].Deriv.Data.MemPtr[m] += FinalAns.Deriv.Data.MemPtr[m] * p;
                                //pDeriv += FinalAct.Deriv.Data.MemPtr[m] * ActionAtt[i].Output.Data.MemPtr[m];
                            }
                            //TerminalProb.Deriv.Data.MemPtr[b * MaxIterationNum + i] = pDeriv;
                        }
                        AnsData[i].Deriv.Data.SyncFromCPU(AnsData[i].BatchSize);
                    }
                }
                else if (ReasonPool == PoolingType.ATT)
                {
                    FinalAns.Deriv.Data.SyncToCPU(FinalAns.BatchSize);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(AnsData[i].Deriv.Data.MemPtr, 0, AnsData[i].BatchSize);

                        for (int b = 0; b < MatchData.SrcSize; b++)
                        {
                            int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];

                            float p = TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i];
                            float pDeriv = 0;

                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                AnsData[i].Deriv.Data.MemPtr[m] += FinalAns.Deriv.Data.MemPtr[m] * p;
                                pDeriv += FinalAns.Deriv.Data.MemPtr[m] * AnsData[i].Output.Data.MemPtr[m];
                            }
                            TerminalProb.Deriv.Data.MemPtr[b * MaxIterationNum + i] = pDeriv;
                        }
                        AnsData[i].Deriv.Data.SyncFromCPU(AnsData[i].BatchSize);
                    }

                    TerminalProb.Deriv.Data.SyncFromCPU(MaxIterationNum * TerminalProb.BatchSize);
                    ComputeLib.DerivSoftmax(TerminalProb.Output.Data, TerminalProb.Deriv.Data, TerminalProb.Deriv.Data, MaxIterationNum, TerminalProb.BatchSize, 0);
                    TerminalProb.Deriv.Data.SyncToCPU(MaxIterationNum * TerminalProb.BatchSize);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        for (int b = 0; b < TerminalRunner[i].Output.BatchSize; b++)
                        {
                            TerminalRunner[i].Output.Deriv.Data.MemPtr[b] = TerminalProb.Deriv.Data.MemPtr[b * MaxIterationNum + i];
                        }
                        TerminalRunner[i].Output.Deriv.Data.SyncFromCPU(TerminalRunner[i].Output.BatchSize);
                    }
                }
                else if (ReasonPool == PoolingType.RL)
                {
                    //int BatchSize = AnswerSeqData.Last().BatchSize;

                    /// according to loss, assign reward to each terminal node.
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(TerminalRunner[i].Output.Deriv.Data.MemPtr, 0, MatchData.SrcSize);
                    }

                    for (int b = 0; b < MatchData.SrcSize; b++)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float reward = AnswerProb[i].Deriv.Data.MemPtr[b];

                            TerminalRunner[i].Output.Deriv.Data.MemPtr[b] += reward * (1 - TerminalRunner[i].Output.Output.Data.MemPtr[b]);
                            for (int hp = 0; hp < i; hp++)
                            {
                                TerminalRunner[hp].Output.Deriv.Data.MemPtr[b] += -reward * TerminalRunner[hp].Output.Output.Data.MemPtr[b];
                            }

                            if (TerminalRunner[i].Output.Deriv.Data.MemPtr[b] > Math.Abs(1))
                            {
                                Logger.WriteLog("TerminalRunner Deriv is too large {0}, step {1}", TerminalRunner[i].Output.Deriv.Data.MemPtr[b], i);
                            }
                        }
                    }

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        TerminalRunner[i].Output.Deriv.Data.SyncFromCPU(TerminalRunner[i].Output.BatchSize);
                    }
                }


                for (int i = MaxIterationNum - 2; i >= 0; i--)
                {
                    TerminalRunner[i + 1].Backward(cleanDeriv);

                    AnsRunner[i + 1].Backward(cleanDeriv);
                    // take action. new Attention
                    AttRunner[i + 1].Backward(cleanDeriv);
                    // update Status.
                    StatusRunner[i].Backward(cleanDeriv);
                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Backward(cleanDeriv);
                }
                AttRunner[0].Backward(cleanDeriv);
                AnsRunner[0].Backward(cleanDeriv);
                TerminalRunner[0].Backward(cleanDeriv);

                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product)
                {
                    ComputeLib.Sgemm(AttHidden.SentDeriv, 0,
                                 AttStruct.Wm, 0,
                                 Memory.SentDeriv, 0,
                                 Memory.SentSize, AttStruct.HiddenDim, AttStruct.MemoryDim, 1, 1, false, true);
                }

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product)
                {
                    ComputeLib.Sgemm(AnsHidden.SentDeriv, 0,
                                 AnsStruct.Wm, 0,
                                 Memory.SentDeriv, 0,
                                 Memory.SentSize, AnsStruct.HiddenDim, AnsStruct.MemoryDim, 1, 1, false, true);
                }

            }

            public override void Update()
            {
                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AttStruct.WmMatrixOptimizer.BeforeGradient();
                        if (AttStruct.IsBias) AttStruct.BMatrixOptimizer.BeforeGradient();
                    }

                    if (AttStruct.IsBias)
                    {
                        ComputeLib.ColumnWiseSum(AttHidden.SentDeriv, AttStruct.BMatrixOptimizer.Gradient, Memory.SentSize, AttStruct.HiddenDim, AttStruct.BMatrixOptimizer.GradientStep);
                    }

                    ComputeLib.Sgemm(Memory.SentOutput, 0,
                                     AttHidden.SentDeriv, 0,
                                     AttStruct.WmMatrixOptimizer.Gradient, 0,
                                     Memory.SentSize, AttStruct.MemoryDim, AttStruct.HiddenDim,
                                     1, AttStruct.WmMatrixOptimizer.GradientStep, true, false);
                }

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AnsStruct.WmMatrixOptimizer.BeforeGradient();
                        if (AnsStruct.IsBias) AnsStruct.BMatrixOptimizer.BeforeGradient();
                    }

                    if (AnsStruct.IsBias)
                    {
                        ComputeLib.ColumnWiseSum(AnsHidden.SentDeriv, AnsStruct.BMatrixOptimizer.Gradient, Memory.SentSize, AnsStruct.HiddenDim, AnsStruct.BMatrixOptimizer.GradientStep);
                    }

                    ComputeLib.Sgemm(Memory.SentOutput, 0,
                                     AnsHidden.SentDeriv, 0,
                                     AnsStruct.WmMatrixOptimizer.Gradient, 0,
                                     Memory.SentSize, AnsStruct.MemoryDim, AnsStruct.HiddenDim,
                                     1, AnsStruct.WmMatrixOptimizer.GradientStep, true, false);
                }

                AttRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                AttRunner[0].Update();

                AnsRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                AnsRunner[0].Update();

                TerminalRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                TerminalRunner[0].Update();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    MemRetrievalRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    StatusRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    AttRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    AnsRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    TerminalRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;


                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Update();

                    // update Status.
                    StatusRunner[i].Update();

                    // take action. new Attention
                    AttRunner[i + 1].Update();

                    AnsRunner[i + 1].Update();

                    TerminalRunner[i + 1].Update();
                }
                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AttStruct.WmMatrixOptimizer.AfterGradient();
                        if (AttStruct.IsBias) AttStruct.BMatrixOptimizer.AfterGradient();
                    }
                }

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AnsStruct.WmMatrixOptimizer.AfterGradient();
                        if (AnsStruct.IsBias) AnsStruct.BMatrixOptimizer.AfterGradient();
                    }
                }
            }
        }


        class CandidateSelectionRunner : StructRunner
        {
            public new BiMatchBatchData Output { get { return (BiMatchBatchData)base.Output; } set { base.Output = value; } }

            public SeqDenseBatchData CandidateContextMem = null;

            GeneralBatchInputData CandidateIndexData { get; set; }

            SeqDenseBatchData FullContextMem { get; set; }

            CudaPieceInt SelectIndex { get; set; }

            public CandidateSelectionRunner(GeneralBatchInputData candidateData, SeqDenseBatchData fullContextMem, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                CandidateIndexData = candidateData;
                FullContextMem = fullContextMem;


                SelectIndex = new CudaPieceInt(candidateData.Stat.MAX_ELEMENTSIZE, true, Behavior.Device == DeviceType.GPU);

                Output = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = candidateData.Stat.MAX_BATCHSIZE,
                    MAX_TGT_BATCHSIZE = candidateData.Stat.MAX_ELEMENTSIZE,
                    MAX_MATCH_BATCHSIZE = candidateData.Stat.MAX_ELEMENTSIZE
                }, Behavior.Device);

                /// candidate context mem;
                CandidateContextMem = new SeqDenseBatchData(
                    new SequenceDataStat
                    {
                        MAX_BATCHSIZE = candidateData.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = candidateData.Stat.MAX_ELEMENTSIZE,
                        FEATURE_DIM = fullContextMem.Dim
                    }, Output.Src2MatchIdx, Output.SrcIdx, Behavior.Device);
            }

            public override void Forward()
            {
                CandidateIndexData.BatchIdx.SyncToCPU(CandidateIndexData.BatchSize);
                CandidateIndexData.FeatureIdx.SyncToCPU(CandidateIndexData.ElementSize);
                CandidateIndexData.FeatureValue.SyncToCPU(CandidateIndexData.ElementSize);

                FullContextMem.SampleIdx.SyncToCPU(CandidateIndexData.BatchSize);

                Output.SrcSize = CandidateIndexData.BatchSize;
                Output.TgtSize = CandidateIndexData.ElementSize; //FullContextMem.SampleIdx.MemPtr[CandidateIndexData.BatchSize - 1];
                Output.MatchSize = CandidateIndexData.ElementSize;

                int matchIdx = 0;
                for (int i = 0; i < CandidateIndexData.BatchSize; i++)
                {
                    int feaBegin = i == 0 ? 0 : CandidateIndexData.BatchIdx.MemPtr[i - 1];
                    int feaEnd = CandidateIndexData.BatchIdx.MemPtr[i];

                    int fullMemBegin = i == 0 ? 0 : FullContextMem.SampleIdx.MemPtr[i - 1];
                    for (int f = feaBegin; f < feaEnd; f++)
                    {
                        Output.SrcIdx.MemPtr[matchIdx] = i;
                        //matchIdx; // 
                        //fullMemBegin + CandidateIndexData.FeatureIdx.MemPtr[f];  
                        Output.TgtIdx.MemPtr[matchIdx] = matchIdx;
                        Output.MatchInfo.MemPtr[matchIdx] = CandidateIndexData.FeatureValue.MemPtr[f];

                        SelectIndex.MemPtr[matchIdx] = fullMemBegin + CandidateIndexData.FeatureIdx.MemPtr[f];

                        matchIdx++;
                    }
                }

                SelectIndex.SyncFromCPU(Output.MatchSize);

                Output.SrcIdx.SyncFromCPU(Output.MatchSize);
                Output.TgtIdx.SyncFromCPU(Output.MatchSize);
                Output.MatchInfo.SyncFromCPU(Output.MatchSize);

                Util.InverseMatchIdx(Output.SrcIdx.MemPtr, Output.MatchSize, Output.Src2MatchIdx.MemPtr, Output.Src2MatchElement.MemPtr, Output.SrcSize);
                Util.InverseMatchIdx(Output.TgtIdx.MemPtr, Output.MatchSize, Output.Tgt2MatchIdx.MemPtr, Output.Tgt2MatchElement.MemPtr, Output.TgtSize);

                Output.Src2MatchIdx.SyncFromCPU(Output.SrcSize);
                Output.Src2MatchElement.SyncFromCPU(Output.MatchSize);
                Output.Tgt2MatchIdx.SyncFromCPU(Output.TgtSize);
                Output.Tgt2MatchElement.SyncFromCPU(Output.MatchSize);


                CandidateContextMem.BatchSize = CandidateIndexData.BatchSize;
                CandidateContextMem.SentSize = CandidateIndexData.ElementSize;

                ComputeLib.Matrix_AdditionMask(FullContextMem.SentOutput, 0, SelectIndex, 0,
                                               CandidateContextMem.SentOutput, 0, CudaPieceInt.Empty, 0,
                                               CandidateContextMem.SentOutput, 0, CudaPieceInt.Empty, 0,
                                               FullContextMem.Dim, Output.MatchSize, 1, 0, 0);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(CandidateContextMem.SentDeriv, Output.MatchSize * CandidateContextMem.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.Matrix_AdditionMask(CandidateContextMem.SentDeriv, 0, CudaPieceInt.Empty, 0,
                                               CandidateContextMem.SentDeriv, 0, CudaPieceInt.Empty, 0,
                                               FullContextMem.SentDeriv, 0, SelectIndex, 0,
                                               FullContextMem.Dim, Output.MatchSize, 1, 0, 1);
            }
        }

        class CandidatePredictionRunner : ObjectiveRunner
        {
            GeneralBatchInputData CandidateInputData { get; set; }

            SeqSparseBatchData ContextInputData { get; set; }

            CudaPieceFloat CandScoreData { get; set; }

            float Gamma { get; set; }

            int SampleNum = 0;
            int HitNum = 0;
            int Iteration = 0;
            string OutputPath = "";
            StreamWriter OutputWriter = null;
            public CandidatePredictionRunner(
                GeneralBatchInputData candInputData, SeqSparseBatchData contextInputData, CudaPieceFloat candScoreData, float gamma, RunnerBehavior behavior,
                string outputFile = "") : base(Structure.Empty, behavior)
            {
                CandidateInputData = candInputData;
                ContextInputData = contextInputData;
                Gamma = gamma;
                CandScoreData = candScoreData;

                OutputPath = outputFile;
            }

            public override void Init()
            {
                SampleNum = 0;
                HitNum = 0;

                if (!OutputPath.Equals(""))
                {
                    OutputWriter = new StreamWriter(OutputPath + "." + Iteration);
                }
            }
            public override void Forward()
            {
                CandidateInputData.BatchIdx.SyncToCPU(CandidateInputData.BatchSize);
                CandidateInputData.FeatureIdx.SyncToCPU(CandidateInputData.ElementSize);
                CandidateInputData.FeatureValue.SyncToCPU(CandidateInputData.ElementSize);

                CandScoreData.SyncToCPU(CandidateInputData.ElementSize);

                ContextInputData.SampleIdx.SyncToCPU(ContextInputData.BatchSize);
                ContextInputData.FeaIdx.SyncToCPU(ContextInputData.ElementSize);

                for (int i = 0; i < CandidateInputData.BatchSize; i++)
                {
                    Dictionary<int, float> candScoreDict = new Dictionary<int, float>();
                    int trueCandIdx = -1;

                    int sentBgn = i == 0 ? 0 : ContextInputData.SampleIdx.MemPtr[i - 1];
                    int sentEnd = ContextInputData.SampleIdx.MemPtr[i];

                    int candBgn = i == 0 ? 0 : CandidateInputData.BatchIdx.MemPtr[i - 1];
                    int candEnd = CandidateInputData.BatchIdx.MemPtr[i];

                    for (int c = candBgn; c < candEnd; c++)
                    {
                        int candPos = CandidateInputData.FeatureIdx.MemPtr[c];
                        int candIdx = ContextInputData.FeaIdx.MemPtr[candPos + sentBgn];
                        float candScore = CandScoreData.MemPtr[c];
                        float candLabel = CandidateInputData.FeatureValue.MemPtr[c];

                        if (candLabel > 0 && trueCandIdx >= 0 && trueCandIdx != candIdx) { throw new Exception("Candidate Label Error 1 !!"); }

                        if (candLabel > 0) { trueCandIdx = candIdx; }

                        if (!candScoreDict.ContainsKey(candIdx)) { candScoreDict[candIdx] = 0; }
                        candScoreDict[candIdx] += (float)Math.Exp(Gamma * candScore);
                    }

                    if (trueCandIdx == -1) { throw new Exception("Candidate Label Error 2 !!"); }
                    var predCandIdx = candScoreDict.FirstOrDefault(x => x.Value == candScoreDict.Values.Max()).Key;

                    float expSum = candScoreDict.Values.Sum();

                    foreach (int k in new List<int>(candScoreDict.Keys)) candScoreDict[k] = candScoreDict[k] / expSum;

                    OutputWriter.WriteLine("{0}\t{1}\t{2}", trueCandIdx, TextUtil.Dict2Str(candScoreDict), predCandIdx);
                    SampleNum++;
                    if (predCandIdx == trueCandIdx) HitNum++;
                }
            }

            public override void Complete()
            {
                Logger.WriteLog("Sample {0}, Hit {1}, Accuracy {2}", SampleNum, HitNum, HitNum * 1.0 / SampleNum);
                ObjectiveScore = HitNum * 1.0 / SampleNum;
                OutputWriter.Close();
                Iteration += 1;
            }
        }

        class CandidateMatchRunner : StructRunner
        {
            public new BiMatchBatchData Output { get { return (BiMatchBatchData)base.Output; } set { base.Output = value; } }

            GeneralBatchInputData CandidateIndexData { get; set; }

            SeqDenseBatchData FullContextMem { get; set; }

            public CandidateMatchRunner(GeneralBatchInputData candidateData, SeqDenseBatchData fullContextMem, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                CandidateIndexData = candidateData;
                FullContextMem = fullContextMem;

                Output = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = CandidateIndexData.Stat.MAX_BATCHSIZE,
                    MAX_TGT_BATCHSIZE = fullContextMem.Stat.MAX_SEQUENCESIZE,
                    MAX_MATCH_BATCHSIZE = CandidateIndexData.Stat.MAX_ELEMENTSIZE
                }, Behavior.Device);
            }

            public override void Forward()
            {
                CandidateIndexData.BatchIdx.SyncToCPU(CandidateIndexData.BatchSize);
                CandidateIndexData.FeatureIdx.SyncToCPU(CandidateIndexData.ElementSize);
                CandidateIndexData.FeatureValue.SyncToCPU(CandidateIndexData.ElementSize);

                FullContextMem.SampleIdx.SyncToCPU(FullContextMem.BatchSize);

                List<Tuple<int, int, float>> matchList = new List<Tuple<int, int, float>>();
                for (int i = 0; i < CandidateIndexData.BatchSize; i++)
                {
                    int feaBegin = i == 0 ? 0 : CandidateIndexData.BatchIdx.MemPtr[i - 1];
                    int feaEnd = CandidateIndexData.BatchIdx.MemPtr[i];

                    Dictionary<int, float> entityCandidite = new Dictionary<int, float>();

                    int fullMemBegin = i == 0 ? 0 : FullContextMem.SampleIdx.MemPtr[i - 1];
                    int fullMemEnd = FullContextMem.SampleIdx.MemPtr[i];
                    for (int f = feaBegin; f < feaEnd; f++)
                    {
                        int pos = fullMemBegin + CandidateIndexData.FeatureIdx.MemPtr[f];
                        if (pos >= fullMemEnd) { throw new Exception("Label position should not be over the maximum number of words in passage."); }
                        entityCandidite.Add(pos, CandidateIndexData.FeatureValue.MemPtr[f]);
                    }

                    foreach (KeyValuePair<int, float> item in entityCandidite)
                    {
                        matchList.Add(new Tuple<int, int, float>(i, item.Key, item.Value));
                    }
                }
                Output.Clear();
                Output.PushMatch(matchList);
            }
        }

        // This is the main code.
        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> context,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> query,
                                                             IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> answer,

                                                             // text embedding cnn.
                                                             List<LayerStructure> EmbedCNN,

                                                             // context lstm 
                                                             bool IsContextLSTM, LSTMStructure ContextD1LSTM, LSTMStructure ContextD2LSTM,

                                                             // query lstm
                                                             bool IsQueryLSTM, LSTMStructure QueryD1LSTM, LSTMStructure QueryD2LSTM,

                                                             // reason net;
                                                             GRUCell StateStruct, MLPAttentionStructure AttStruct, MLPAttentionStructure AnsStruct, LayerStructure TerminalStruct,

                                                             // model.
                                                             CompositeNNStructure model, RunnerBehavior Behavior, string resultFile)
        {
            ComputationGraph cg = new ComputationGraph();

            /************************************************************ Query Passage Candidate data *********/
            SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));
            SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(context, Behavior));
            GeneralBatchInputData CandidateAnswerData = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(answer, Behavior));

            /*************Random Shuffle Entity *******/
            //if(BuilderParameters.IS_ENTITY_SHUFFLE)
            //cg.AddRunner(new DataPanel.EntityShuffleRunner(QueryData, ContextData, Behavior));

            /************************************************************ CNN on Query data *********/
            SeqDenseBatchData QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN[0], QueryData, Behavior));
            for (int i = 1; i < EmbedCNN.Count; i++)
                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(EmbedCNN[i], QueryEmbedOutput, Behavior));

            /************************************************************ LSTM on Query data *********/
            HiddenBatchData QueryOutput = null;
            if (IsQueryLSTM)
            {
                SeqDenseRecursiveData QueryD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(QueryEmbedOutput, false, Behavior));
                FastLSTMDenseRunner<SeqDenseRecursiveData> queryD1LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(QueryD1LSTM.LSTMCells[0], QueryD1LstmInput, Behavior);
                SeqDenseRecursiveData QueryD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(queryD1LSTMRunner);

                SeqDenseRecursiveData QueryD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(QueryEmbedOutput, true, Behavior));
                FastLSTMDenseRunner<SeqDenseRecursiveData> queryD2LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(QueryD2LSTM.LSTMCells[0], QueryD2LstmInput, Behavior);
                SeqDenseRecursiveData QueryD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(queryD2LSTMRunner);

                HiddenBatchData QueryD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(QueryD1LstmOutput, true, 0, QueryD1LstmOutput.MapForward, Behavior));
                HiddenBatchData QueryD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(QueryD2LstmOutput, false, 0, QueryD2LstmOutput.MapForward, Behavior));
                HiddenBatchData QueryO = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { QueryD1O, QueryD2O }, Behavior));
                QueryOutput = QueryO;
            }
            else
            {
                QueryOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(QueryEmbedOutput, Behavior));
            }

            /************************************************************ Embedding on Passage data *********/
            SeqDenseBatchData ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN[0], ContextData, Behavior));
            for (int i = 1; i < EmbedCNN.Count; i++)
                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(EmbedCNN[i], ContextEmbedOutput, Behavior));

            SeqDenseBatchData CandidateSeqO = null;
            BiMatchBatchData CandidateMatch = null;

            /************************************************************ LSTM on Passage data *********/

            if (IsContextLSTM)
            {
                //if (BuilderParameters.IS_ENTITY_LSTM)
                //{
                //    /************************************************************ Select Candidate on Entity data *********/
                //    CandidateSelectionRunner CandidateMemRunner = new CandidateSelectionRunner(CandidateAnswerData, ContextEmbedOutput, Behavior);
                //    cg.AddRunner(CandidateMemRunner);
                //    SeqDenseBatchData CandidateEntityMem = CandidateMemRunner.CandidateContextMem;
                //    CandidateMatch = CandidateMemRunner.Output;
                //    /************************************************************ LSTM on Candidate Entity data *********/
                //    SeqDenseRecursiveData CandidateD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(CandidateEntityMem, false, Behavior));
                //    FastLSTMDenseRunner<SeqDenseRecursiveData> candidateD1LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(ContextD1LSTM.LSTMCells[0], CandidateD1LstmInput, Behavior);
                //    SeqDenseRecursiveData CandidateD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(candidateD1LSTMRunner);
                //    SeqDenseRecursiveData CandidateD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(CandidateEntityMem, true, Behavior));
                //    FastLSTMDenseRunner<SeqDenseRecursiveData> candidateD2LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(ContextD2LSTM.LSTMCells[0], CandidateD2LstmInput, Behavior);
                //    SeqDenseRecursiveData CandidateD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(candidateD2LSTMRunner);
                //    SeqDenseBatchData CandidateSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(CandidateD1LstmOutput, Behavior));
                //    SeqDenseBatchData CandidateSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(CandidateD2LstmOutput, Behavior));
                //    CandidateSeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { CandidateSeqD1O, CandidateSeqD2O }, Behavior));
                //}
                //else
                /************************************************************ LSTM on Candidate Word data *********/
                SeqDenseRecursiveData CandidateD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(ContextEmbedOutput, false, Behavior));
                FastLSTMDenseRunner<SeqDenseRecursiveData> candidateD1LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(ContextD1LSTM.LSTMCells[0], CandidateD1LstmInput, Behavior);
                SeqDenseRecursiveData CandidateD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(candidateD1LSTMRunner);

                SeqDenseRecursiveData CandidateD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(ContextEmbedOutput, true, Behavior));
                FastLSTMDenseRunner<SeqDenseRecursiveData> candidateD2LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(ContextD2LSTM.LSTMCells[0], CandidateD2LstmInput, Behavior);
                SeqDenseRecursiveData CandidateD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(candidateD2LSTMRunner);

                SeqDenseBatchData CandidateSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(CandidateD1LstmOutput, Behavior));
                SeqDenseBatchData CandidateSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(CandidateD2LstmOutput, Behavior));
                CandidateSeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { CandidateSeqD1O, CandidateSeqD2O }, Behavior));

                //CandidateMatchRunner CandidateMemRunner = new CandidateMatchRunner(CandidateAnswerData, ContextEmbedOutput, Behavior);
                //cg.AddRunner(CandidateMemRunner);
                //CandidateMatch = CandidateMemRunner.Output;

                CandidateSelectionRunner CandidateMemRunner = new CandidateSelectionRunner(CandidateAnswerData, CandidateSeqO, Behavior);
                cg.AddRunner(CandidateMemRunner);

                CandidateSeqO = CandidateMemRunner.CandidateContextMem;
                CandidateMatch = CandidateMemRunner.Output;
            }
            else
            {
                CandidateMatchRunner CandidateMemRunner = new CandidateMatchRunner(CandidateAnswerData, ContextEmbedOutput, Behavior);
                cg.AddRunner(CandidateMemRunner);
                CandidateMatch = CandidateMemRunner.Output;
                CandidateSeqO = ContextEmbedOutput;
            }

            /* We will always use reasoNet.
            if (BuilderParameters.RECURRENT_STEP == 0)
            {
                // similarity between source text and target text.
                simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(QueryOutput,
                    new HiddenBatchData(CandidateSeqO.MAX_SENTSIZE, CandidateSeqO.Dim, CandidateSeqO.SentOutput, CandidateSeqO.SentDeriv, Behavior.Device),
                     CandidateMatch, BuilderParameters.SimiType, Behavior));
            }
            */

            ReasonNetRunner reasonRunner = new ReasonNetRunner(QueryOutput, CandidateSeqO, CandidateMatch, BuilderParameters.RECURRENT_STEP,
                StateStruct, AttStruct, AnsStruct, TerminalStruct, BuilderParameters.Gamma, Behavior);

            cg.AddRunner(reasonRunner);

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                        cg.AddRunner(new ModelDiskDumpRunner(model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\RecurrentAttention"));
                    cg.AddObjective(new MultiInstanceBayesianRatingRunner(CandidateMatch.MatchInfo, reasonRunner.AnsData, reasonRunner.AnswerProb, CandidateMatch, BuilderParameters.Gamma, Behavior));
                    break;
                case DNNRunMode.Predict:
                    HiddenBatchData AnswerOutput = reasonRunner.FinalAns;
                    cg.AddRunner(new CandidatePredictionRunner(CandidateAnswerData, ContextData, AnswerOutput.Output.Data, BuilderParameters.Gamma, Behavior, resultFile));
                    break;
            }

            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");


            CompositeNNStructure modelStructure = new CompositeNNStructure();

            // word embedding layer.
            List<LayerStructure> embedLayers = new List<LayerStructure>();

            //context lstm layer.
            LSTMStructure contextD1LSTM = null;
            LSTMStructure contextD2LSTM = null;

            //query lstm layer.
            LSTMStructure queryD1LSTM = null;
            LSTMStructure queryD2LSTM = null;

            //LayerStructure matchLayer = null;

            // gru for state transformation.
            GRUCell stateStruct = null;

            // MLP as attention.
            MLPAttentionStructure attStruct = null;

            // cosine similarity as action.
            MLPAttentionStructure ansStruct = null;

            // Terminal gate module. 
            LayerStructure terminalStruct = null;

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                int embedDim = DataPanel.WordDim;
                for (int i = 0; i < BuilderParameters.EMBED_LAYER_DIM.Length; i++)
                {
                    embedLayers.Add((new LayerStructure(embedDim, BuilderParameters.EMBED_LAYER_DIM[i],
                        BuilderParameters.EMBED_ACTIVATION[i], N_Type.Convolution_layer, BuilderParameters.EMBED_LAYER_WIN[i],
                        BuilderParameters.EMBED_DROPOUT[i], false, device)));
                    embedDim = BuilderParameters.EMBED_LAYER_DIM[i];
                }

                #region Glove Work Embedding Initialization.

                if (!BuilderParameters.InitWordEmbedding.Equals(string.Empty))
                {
                    int wordDim = BuilderParameters.EMBED_LAYER_DIM[0];

                    int halfWinStart = BuilderParameters.EMBED_LAYER_WIN[0] / 2; // * DataPanel.WordDim;

                    Logger.WriteLog("Init Glove Word Embedding ...");
                    int hit = 0;
                    using (StreamReader mreader = new StreamReader(BuilderParameters.InitWordEmbedding))
                    {
                        while (!mreader.EndOfStream)
                        {
                            string[] items = mreader.ReadLine().Split(' ');
                            string word = items[0];
                            float[] vec = new float[items.Length - 1];
                            for (int i = 1; i < items.Length; i++)
                            {
                                vec[i - 1] = float.Parse(items[i]);
                            }

                            int wordIdx = DataPanel.wordFreqDict.IndexItem(word);
                            if (wordIdx >= 0)
                            {
                                int feaIdx = DataPanel.entityFreqDict.ItemDictSize + 1 + wordIdx;

                                for (int win = -halfWinStart; win < BuilderParameters.EMBED_LAYER_WIN[0] - halfWinStart; win++)
                                {
                                    for (int i = 0; i < wordDim; i++)
                                    {
                                        embedLayers[0].weight.MemPtr[((win + halfWinStart) * DataPanel.WordDim + feaIdx) * wordDim + i] = vec[i];
                                    }
                                }
                                hit++;
                            }
                        }
                    }
                    embedLayers[0].weight.SyncFromCPU();
                    Logger.WriteLog("Init Glove Word Embedding Done, {0} words initalized in total {1}.", hit, DataPanel.wordFreqDict.ItemDictSize);
                }
                #endregion.

                int conDim = embedDim;
                int queryDim = embedDim;

                if (BuilderParameters.CON_LSTM_DIM > 0)
                {
                    contextD1LSTM = new LSTMStructure(conDim, new int[] { BuilderParameters.CON_LSTM_DIM }, device);
                    contextD2LSTM = new LSTMStructure(conDim, new int[] { BuilderParameters.CON_LSTM_DIM }, device);
                    conDim = 2 * BuilderParameters.CON_LSTM_DIM;
                }

                if (BuilderParameters.QUERY_LSTM_DIM > 0)
                {
                    queryD1LSTM = new LSTMStructure(queryDim, new int[] { BuilderParameters.QUERY_LSTM_DIM }, device);
                    queryD2LSTM = new LSTMStructure(queryDim, new int[] { BuilderParameters.QUERY_LSTM_DIM }, device);
                    queryDim = 2 * BuilderParameters.QUERY_LSTM_DIM;
                }

                stateStruct = new GRUCell(conDim, queryDim, device);
                attStruct = new MLPAttentionStructure(queryDim, conDim, BuilderParameters.ATT_HID_DIM, device);
                ansStruct = new MLPAttentionStructure(queryDim, conDim, BuilderParameters.ANS_HID_DIM, device);
                terminalStruct = new LayerStructure(queryDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false);
                //matchLayer = new LayerStructure(BuilderParameters.QUERY_LAYER_DIM.Last(), BuilderParameters.CON_LAYER_DIM.Last(), A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true);

                for (int i = 0; i < embedLayers.Count; i++) modelStructure.AddLayer(embedLayers[i]);

                modelStructure.AddLayer(stateStruct);
                modelStructure.AddLayer(attStruct);
                modelStructure.AddLayer(ansStruct);
                modelStructure.AddLayer(terminalStruct);

                if (BuilderParameters.CON_LSTM_DIM > 0)
                {
                    modelStructure.AddLayer(contextD1LSTM);
                    modelStructure.AddLayer(contextD2LSTM);
                }

                if (BuilderParameters.QUERY_LSTM_DIM > 0)
                {
                    modelStructure.AddLayer(queryD1LSTM);
                    modelStructure.AddLayer(queryD2LSTM);
                }
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, DeviceType.GPU);
                    int link = 0;
                    for (int i = 0; i < BuilderParameters.EMBED_LAYER_DIM.Length; i++) embedLayers.Add((LayerStructure)modelStructure.CompositeLinks[link++]);

                    stateStruct = (GRUCell)modelStructure.CompositeLinks[link++];
                    attStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    ansStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    terminalStruct = (LayerStructure)modelStructure.CompositeLinks[link++];

                    if (BuilderParameters.CON_LSTM_DIM > 0)
                    {
                        contextD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                        contextD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    }

                    if (BuilderParameters.QUERY_LSTM_DIM > 0)
                    {
                        queryD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                        queryD2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    }
                }
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainAnswer,
                        embedLayers, BuilderParameters.CON_LSTM_DIM > 0, contextD1LSTM, contextD2LSTM,
                        BuilderParameters.QUERY_LSTM_DIM > 0, queryD1LSTM, queryD2LSTM,
                        stateStruct, attStruct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }, string.Empty);
                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    ComputationGraph testCG = null;
                    if (BuilderParameters.IsTestFile)
                        testCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                            embedLayers, BuilderParameters.CON_LSTM_DIM > 0, contextD1LSTM, contextD2LSTM,
                            BuilderParameters.QUERY_LSTM_DIM > 0, queryD1LSTM, queryD2LSTM,
                            stateStruct, attStruct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.ScoreOutputPath + ".Test");

                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraph(DataPanel.ValidContext, DataPanel.ValidQuery, DataPanel.ValidAnswer,
                            embedLayers, BuilderParameters.CON_LSTM_DIM > 0, contextD1LSTM, contextD2LSTM,
                            BuilderParameters.QUERY_LSTM_DIM > 0, queryD1LSTM, queryD2LSTM,
                            stateStruct, attStruct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.ScoreOutputPath + ".Valid");

                    double bestValidScore = double.MinValue;
                    double bestTestScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }

                        double testScore = 0;
                        double validScore = 0;
                        if (testCG != null)
                        {
                            testScore = testCG.Execute();
                            Logger.WriteLog("Test Score {0}, At iter {1}", testScore, bestIter);
                        }
                        if (validCG != null)
                        {
                            validScore = validCG.Execute();
                            Logger.WriteLog("Valid Score {0}, At iter {1}", validScore, bestIter);
                        }

                        if (validScore > bestValidScore)
                        {
                            bestValidScore = validScore; bestTestScore = testScore; bestIter = iter;
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, "RecurrentAttention.best.model"), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }
                        Logger.WriteLog("Best Valid/Test Score {0}, {1}, At iter {1}", bestValidScore, bestTestScore, bestIter);

                        // if (BuilderParameters.IS_ENTITY_SHUFFLE) DataPanel.InitEntityShuffle();
                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                            embedLayers, BuilderParameters.CON_LSTM_DIM > 0, contextD1LSTM, contextD2LSTM,
                            BuilderParameters.QUERY_LSTM_DIM > 0, queryD1LSTM, queryD2LSTM,
                            stateStruct, attStruct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.ScoreOutputPath + ".Test");
                    predCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQuery = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TrainAnswer = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestQuery = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TestAnswer = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQuery = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> ValidAnswer = null;

            public static ItemFreqIndexDictionary wordFreqDict = null;
            public static ItemFreqIndexDictionary entityFreqDict = null;

            public static int WordDim { get { return 1 + entityFreqDict.ItemDictSize + wordFreqDict.ItemDictSize; } }

            static int WordVocabLimited = BuilderParameters.VocabSize;

            static void ExtractVocab(string questFolder, string vocab)
            {
                wordFreqDict = new ItemFreqIndexDictionary("WordVocab");
                entityFreqDict = new ItemFreqIndexDictionary("EntityVocab");

                int fileIdx = 0;
                DirectoryInfo directory = new DirectoryInfo(questFolder);
                foreach (FileInfo sampleFile in directory.EnumerateFiles())
                {
                    using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                    {
                        string url = mreader.ReadLine(); mreader.ReadLine();
                        string context = mreader.ReadLine(); mreader.ReadLine();
                        string query = mreader.ReadLine(); mreader.ReadLine();
                        string answer = mreader.ReadLine(); mreader.ReadLine();

                        foreach (string word in context.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }

                        foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else if (word.StartsWith("@placeholder")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }
                    }
                    if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract Vocab from Corpus {0}", fileIdx); }
                }

                /// keep top 50K vocabary.
                wordFreqDict.Filter(0, WordVocabLimited);
                using (StreamWriter mwriter = new StreamWriter(vocab))
                {
                    entityFreqDict.Save(mwriter);
                    wordFreqDict.Save(mwriter);
                }
            }

            /// <summary>
            /// Unknown Index = 0;
            /// </summary>
            static void ExtractCorpusIndex(string questFolder, string questIndexFile)
            {
                using (StreamWriter indexWriter = new StreamWriter(questIndexFile))
                {
                    int fileIdx = 0;
                    DirectoryInfo directory = new DirectoryInfo(questFolder);
                    foreach (FileInfo sampleFile in directory.EnumerateFiles())
                    {
                        using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                        {
                            string url = mreader.ReadLine(); mreader.ReadLine();
                            string context = mreader.ReadLine(); mreader.ReadLine();
                            string query = mreader.ReadLine(); mreader.ReadLine();
                            string answer = mreader.ReadLine(); mreader.ReadLine();

                            int answerIndex = entityFreqDict.IndexItem(answer);

                            Dictionary<int, float> answerLabel = new Dictionary<int, float>();

                            List<int> contextIndex = new List<int>();
                            int pos = 0;
                            foreach (string word in context.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                if (word.StartsWith("@entity"))
                                {
                                    int idx = entityFreqDict.IndexItem(word); contextIndex.Add(idx + 1);
                                    if (answerIndex == idx) { answerLabel.Add(pos, 1); }
                                    else { answerLabel.Add(pos, 0); }
                                }
                                else { int idx = wordFreqDict.IndexItem(word); contextIndex.Add(idx >= 0 ? idx + entityFreqDict.ItemDictSize + 1 : 0); }
                                pos++;
                            }

                            List<int> queryIndex = new List<int>();
                            foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                if (word.StartsWith("@entity")) { int idx = entityFreqDict.IndexItem(word); queryIndex.Add(idx + 1); }
                                else if (word.StartsWith("@placeholder")) { int idx = entityFreqDict.IndexItem(word); queryIndex.Add(idx + 1); }
                                else { int idx = wordFreqDict.IndexItem(word); queryIndex.Add(idx >= 0 ? idx + entityFreqDict.ItemDictSize + 1 : 0); }
                            }

                            indexWriter.WriteLine("{0}\t{1}\t{2}", string.Join(" ", contextIndex), string.Join(" ", queryIndex), TextUtil.Dict2Str(answerLabel));
                        }
                        if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract Index from Corpus {0}", fileIdx); }
                    }
                }
            }

            #region Entity Shuffle.
            public class EntityShuffleRunner : StructRunner
            {
                SeqSparseBatchData Query { get; set; }
                SeqSparseBatchData Passage { get; set; }

                public EntityShuffleRunner(SeqSparseBatchData query, SeqSparseBatchData passage, RunnerBehavior behavior) : base(Structure.Empty, behavior)
                {
                    Query = query;
                    Passage = passage;
                }

                public override void Forward()
                {
                    if (entityShuffle != null) ShuffleEntity(Query, Passage, entityShuffle);
                }
            }

            static Dictionary<int, int> entityShuffle = null;
            public static void InitEntityShuffle()
            {
                int placeHolderId = entityFreqDict.IndexItem("@placeholder");
                entityShuffle = new Dictionary<int, int>();
                DataRandomShuffling shuffle = new DataRandomShuffling(entityFreqDict.ItemDictSize);
                for (int i = 0; i < entityFreqDict.ItemDictSize; i++)
                {
                    int eid = shuffle.RandomNext();
                    entityShuffle.Add(i, eid);
                }

                for (int i = 0; i < entityFreqDict.ItemDictSize; i++)
                {
                    if (entityShuffle[i] == placeHolderId)
                    {
                        entityShuffle[i] = entityShuffle[placeHolderId];
                        entityShuffle[placeHolderId] = placeHolderId;
                        break;
                    }
                }
            }

            static void ShuffleEntity(SeqSparseBatchData query, SeqSparseBatchData passage, Dictionary<int, int> shuffle)
            {
                query.FeaIdx.SyncToCPU(query.ElementSize);
                for (int e = 0; e < query.ElementSize; e++)
                {
                    if (query.FeaIdx.MemPtr[e] >= 1 && query.FeaIdx.MemPtr[e] <= entityFreqDict.ItemDictSize)
                    {
                        query.FeaIdx.MemPtr[e] = shuffle[query.FeaIdx.MemPtr[e] - 1] + 1;
                    }
                }
                query.FeaIdx.SyncFromCPU(query.ElementSize);


                passage.FeaIdx.SyncToCPU(passage.ElementSize);
                for (int e = 0; e < passage.ElementSize; e++)
                {
                    if (passage.FeaIdx.MemPtr[e] >= 1 && passage.FeaIdx.MemPtr[e] <= entityFreqDict.ItemDictSize)
                    {
                        passage.FeaIdx.MemPtr[e] = shuffle[passage.FeaIdx.MemPtr[e] - 1] + 1;
                    }
                }
                passage.FeaIdx.SyncFromCPU(passage.ElementSize);
            }
            #endregion.

            static void ExtractCorpusBinary(string questIndexFile, string contextBin, string queryBin, string answerBin, int miniBatchSize)
            {
                SeqSparseBatchData context = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData query = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                GeneralBatchInputData answer = new GeneralBatchInputData(new GeneralBatchInputDataStat() { FeatureType = FeatureDataType.SparseFeature, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);

                BinaryWriter contextWriter = FileUtil.CreateBinaryWrite(contextBin);
                BinaryWriter queryWriter = FileUtil.CreateBinaryWrite(queryBin);
                BinaryWriter answerWriter = FileUtil.CreateBinaryWrite(answerBin);

                using (StreamReader indexReader = new StreamReader(questIndexFile))
                {
                    int lineIdx = 0;
                    while (!indexReader.EndOfStream)
                    {
                        string[] items = indexReader.ReadLine().Split('\t');

                        List<Dictionary<int, float>> contextFea = new List<Dictionary<int, float>>();
                        foreach (string term in items[0].Split(' ')) { var tmp = new Dictionary<int, float>(); tmp[int.Parse(term)] = 1; contextFea.Add(tmp); }
                        context.PushSample(contextFea);

                        List<Dictionary<int, float>> queryFea = new List<Dictionary<int, float>>();
                        foreach (string term in items[1].Split(' ')) { var tmp = new Dictionary<int, float>(); tmp[int.Parse(term)] = 1; queryFea.Add(tmp); }
                        query.PushSample(queryFea);

                        Dictionary<int, float> answerFea = TextUtil.Str2Dict(items[2]);
                        answer.PushSample(answerFea);

                        if (context.BatchSize >= miniBatchSize)
                        {
                            context.PopBatchToStat(contextWriter);
                            query.PopBatchToStat(queryWriter);
                            answer.PopBatchToStat(answerWriter);
                        }

                        if (++lineIdx % 1000 == 0) { Console.WriteLine("Extract Binary from Corpus {0}", lineIdx); }
                    }
                    context.PopBatchCompleteStat(contextWriter);
                    query.PopBatchCompleteStat(queryWriter);
                    answer.PopBatchCompleteStat(answerWriter);

                    Console.WriteLine("Context Stat {0}", context.Stat.ToString());
                    Console.WriteLine("Query Stat {0}", query.Stat.ToString());
                    Console.WriteLine("Answer Stat {0}", answer.Stat.ToString());
                }
            }

            public static void Init()
            {
                #region Preprocess Data.
                /// Step 1 : Extract Vocab from training Corpus.
                if (!File.Exists(BuilderParameters.Vocab)) ExtractVocab(BuilderParameters.TrainFolder, BuilderParameters.Vocab);
                else
                {
                    using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab))
                    {
                        entityFreqDict = new ItemFreqIndexDictionary(mreader);
                        wordFreqDict = new ItemFreqIndexDictionary(mreader);
                    }
                }

                /// Step 2 : Corpus 2 Index.
                if (BuilderParameters.IsTrainFile && !File.Exists(BuilderParameters.TrainIndexData))
                {
                    ExtractCorpusIndex(BuilderParameters.TrainFolder, BuilderParameters.TrainIndexData);
                }
                if (BuilderParameters.IsTestFile && !File.Exists(BuilderParameters.TestIndexData))
                {
                    ExtractCorpusIndex(BuilderParameters.TestFolder, BuilderParameters.TestIndexData);
                }

                if (BuilderParameters.IsValidFile && !File.Exists(BuilderParameters.ValidIndexData))
                {
                    ExtractCorpusIndex(BuilderParameters.ValidFolder, BuilderParameters.ValidIndexData);
                }


                /// Step 3 : Index 2 Binary Data.
                if (BuilderParameters.IsTrainFile &&
                    (!File.Exists(BuilderParameters.TrainContextBinary) || !File.Exists(BuilderParameters.TrainQueryBinary) || !File.Exists(BuilderParameters.TrainAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.TrainIndexData, BuilderParameters.TrainContextBinary, BuilderParameters.TrainQueryBinary, BuilderParameters.TrainAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                if (BuilderParameters.IsTestFile &&
                    (!File.Exists(BuilderParameters.TestContextBinary) || !File.Exists(BuilderParameters.TestQueryBinary) || !File.Exists(BuilderParameters.TestAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.TestIndexData, BuilderParameters.TestContextBinary, BuilderParameters.TestQueryBinary, BuilderParameters.TestAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                if (BuilderParameters.IsValidFile &&
                    (!File.Exists(BuilderParameters.ValidContextBinary) || !File.Exists(BuilderParameters.ValidQueryBinary) || !File.Exists(BuilderParameters.ValidAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.ValidIndexData, BuilderParameters.ValidContextBinary, BuilderParameters.ValidQueryBinary, BuilderParameters.ValidAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                #endregion.

                #region Load Data.
                if (BuilderParameters.IsTrainFile)
                {
                    TrainContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBinary, BuilderParameters.TrainContextBinary + ".cursor");
                    TrainQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBinary, BuilderParameters.TrainQueryBinary + ".cursor");
                    TrainAnswer = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TrainAnswerBinary, BuilderParameters.TrainAnswerBinary + ".cursor");

                    TrainContext.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQuery.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainAnswer.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                }

                if (BuilderParameters.IsTestFile)
                {
                    TestContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestContextBinary, BuilderParameters.TestContextBinary + ".cursor");
                    TestQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestQueryBinary, BuilderParameters.TestQueryBinary + ".cursor");
                    TestAnswer = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TestAnswerBinary, BuilderParameters.TestAnswerBinary + ".cursor");

                    TestContext.InitThreadSafePipelineCashier(64, false);
                    TestQuery.InitThreadSafePipelineCashier(64, false);
                    TestAnswer.InitThreadSafePipelineCashier(64, false);
                }

                if (BuilderParameters.IsValidFile)
                {
                    ValidContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBinary, BuilderParameters.ValidContextBinary + ".cursor");
                    ValidQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBinary, BuilderParameters.ValidQueryBinary + ".cursor");
                    ValidAnswer = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.ValidAnswerBinary, BuilderParameters.ValidAnswerBinary + ".cursor");

                    ValidContext.InitThreadSafePipelineCashier(64, false);
                    ValidQuery.InitThreadSafePipelineCashier(64, false);
                    ValidAnswer.InitThreadSafePipelineCashier(64, false);
                }
                #endregion.
            }
        }
    }
}
