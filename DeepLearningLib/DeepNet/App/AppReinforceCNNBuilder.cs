﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    /// <summary>
    /// CNN for Image.
    /// </summary>
    public class AppReinforceCNNBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }

            public static string TrainData { get { return Argument["TRAIN-DATA"].Value; } }
            public static string TrainLabel { get { return Argument["TRAIN-LABEL"].Value; } }

            public static string ValidData { get { return Argument["VALID-DATA"].Value; } }
            public static string ValidLabel { get { return Argument["VALID-LABEL"].Value; } }

            public static int[] SX { get { return Argument["SX"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] Stride { get { return Argument["STRIDE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] O { get { return Argument["O"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] Pad { get { return Argument["PAD"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] Afs { get { return Argument["AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] PoolSX { get { return Argument["POOLSX"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] PoolStride { get { return Argument["POOLSTRIDE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static List<ImageFilterParameter> Filters(int depth)
            {
                List<ImageFilterParameter> mFilters = new List<ImageFilterParameter>();
                int mdepth = depth;
                for (int i = 0; i < SX.Length; i++)
                {
                    mFilters.Add(new ImageFilterParameter(SX[i], Stride[i], mdepth, O[i], Pad[i], Afs[i], PoolSX[i], PoolStride[i]));
                    mdepth = O[i];
                }
                return mFilters;
            }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }


            public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            
            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                Argument.Add("TRAIN-DATA", new ParameterArgument(string.Empty, "Train Image Data"));
                Argument.Add("TRAIN-LABEL", new ParameterArgument(string.Empty, "Train Image Label"));

                Argument.Add("VALID-DATA", new ParameterArgument(string.Empty, "Valid Image Data"));
                Argument.Add("VALID-LABEL", new ParameterArgument(string.Empty, "Valid Image Label"));

                Argument.Add("SX", new ParameterArgument(string.Empty, "Convolutional Filter SX"));
                Argument.Add("STRIDE", new ParameterArgument(string.Empty, "Convolutional Filter Stride"));
                Argument.Add("O", new ParameterArgument(string.Empty, "Convolutional Output Dim"));
                Argument.Add("PAD", new ParameterArgument(string.Empty, "Pad space for image"));
                Argument.Add("AF", new ParameterArgument(string.Empty, "Activation functions"));
                Argument.Add("POOLSX", new ParameterArgument(string.Empty, "Pooling SX"));
                Argument.Add("POOLSTRIDE", new ParameterArgument(string.Empty, "Pooling Stride"));

                Argument.Add("LAYER-DIM", new ParameterArgument(string.Empty, "Layer Dimension"));
                Argument.Add("ACTIVATION", new ParameterArgument(string.Empty, "Activation NN"));
                Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
                Argument.Add("LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));

                Argument.Add("LEARN-RATE", new ParameterArgument("0.01", "Model Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
            
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "DNN Seed Model."));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_REINFORCE_CNN; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
            //Cudalib.CudaInit(BuilderParameters.GPUID);
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<ImageDataSource, ImageDataStat> imgData,
            IDataCashier<DenseBatchData, DenseDataStat> labelData,
            RunnerBehavior Behavior, List<RLImageLinkStructure> ConvStruct, List<RLLayerStructure> LayerStruct)
        {
            ComputationGraph cg = new ComputationGraph();

            /**************** Get Data from DataCashier *********/
            ImageDataSource image = (ImageDataSource)cg.AddDataRunner(new DataRunner<ImageDataSource, ImageDataStat>(imgData, Behavior));
            DenseBatchData label = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(labelData, Behavior));

            RewardData reward = new RewardData();
            foreach (RLImageLinkStructure linkModel in ConvStruct)
            {
                cg.AddRunner(new RLSampleRunner(linkModel.RLFilter, reward, Behavior));
            }
            foreach(RLLayerStructure linkModel in LayerStruct)
            {
                cg.AddRunner(new RLSampleRunner(linkModel.RLWeight, reward, Behavior));
            }

            ImageDataSource layerInput = image;
            foreach (RLImageLinkStructure linkModel in ConvStruct)
            {
                RLImageConvRunner<ImageDataSource> linkRunner = new RLImageConvRunner<ImageDataSource>(linkModel, layerInput, Behavior);
                cg.AddRunner(linkRunner);
                layerInput = linkRunner.Output;
            }

            RLImageFullyConnectRunner<ImageDataSource> fullRunner = new RLImageFullyConnectRunner<ImageDataSource>(LayerStruct[0], layerInput, Behavior);
            cg.AddRunner(fullRunner);
            HiddenBatchData nnLayerInput = fullRunner.Output;
            for (int i = 1; i < LayerStruct.Count; i++)
            {
                RLFullyConnectHiddenRunner<HiddenBatchData> nnRunner = new RLFullyConnectHiddenRunner<HiddenBatchData>(LayerStruct[i], nnLayerInput, Behavior);
                cg.AddRunner(nnRunner);
                nnLayerInput = nnRunner.Output;
            }


            /*************** DNN for Source Input. ********************/
            HiddenBatchData pred = nnLayerInput;

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new RLMultiClassSoftmaxRunner(label.Data, pred.Output, reward, Behavior));
                    break;

                case DNNRunMode.Predict:
                    cg.AddRunner(new AccuracyDiskDumpRunner(label.Data, pred.Output, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath));
                    break;
            }
            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager ComputeLib = MathOperatorManager.CreateInstance(device);
            //EvaluationParameter evalParameter = new EvaluationParameter()
            //{
            //    evalType = ImageCNNBuilderParameters.Evaluation,
            //    InputDataType = DataSourceID.ImageLabelData,
            //    OutputDataType = DataSourceID.HiddenBatchData,
            //    Param = new BasicEvalParameter()
            //    {
            //        Gamma = ImageCNNBuilderParameters.Gamma,
            //        LabelMaxValue = ImageCNNBuilderParameters.LabelMaxValue,
            //        LabelMinValue = ImageCNNBuilderParameters.LabelMinValue,
            //        ScoreFile = ImageCNNBuilderParameters.ScoreOutputPath,
            //        MetricFile = ImageCNNBuilderParameters.MetricOutputPath
            //    }
            //};
            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            List<RLImageLinkStructure> ConvLinkStruct = new List<RLImageLinkStructure>();
            List<RLLayerStructure> FullLinkStruct = new List<RLLayerStructure>();

            //Console.WriteLine(Cudalib.CudaDeviceProperties(0));
            //CompositeNNStructure cnnModel = null;
            //    new ImageInputParameter() { Width = DataPanel.Train.Stat.SrcStat.Width, Height = DataPanel.Train.Stat.SrcStat.Height, Depth = DataPanel.Train.Stat.SrcStat.Depth },
            //    ImageCNNBuilderParameters.Filters(DataPanel.Train.Stat.SrcStat.Depth),
            //    ImageCNNBuilderParameters.LAYER_DIM, ImageCNNBuilderParameters.ACTIVATION, ImageCNNBuilderParameters.LAYER_DROPOUT, ImageCNNBuilderParameters.LAYER_BIAS);
            Logger.WriteLog("Loading CNN Structure.");

            if (BuilderParameters.SEED_MODEL.Equals(string.Empty))
            {
                int outWidth = DataPanel.TrainImage.Stat.Width;
                int outHeight = DataPanel.TrainImage.Stat.Height;
                List<ImageFilterParameter> filters = BuilderParameters.Filters(DataPanel.TrainImage.Stat.Depth);
                foreach (ImageFilterParameter filter in filters)
                {
                    ConvLinkStruct.Add(new RLImageLinkStructure(filter, device));
                    outWidth = filter.OutputWidth(outWidth);
                    outHeight = filter.OutputHeight(outHeight);
                }
                int featureNum = filters.Last().O * outWidth * outHeight;
                for (int i = 0; i < BuilderParameters.LAYER_DIM.Length; i++)
                {
                    RLLayerStructure link = new RLLayerStructure(featureNum, BuilderParameters.LAYER_DIM[i], BuilderParameters.ACTIVATION[i], device);
                    FullLinkStruct.Add(link);
                    featureNum = BuilderParameters.LAYER_DIM[i];
                }
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    ///int[] layerDim, A_Func[] activation, bool[] isbias, N_Type[] arch, int[] wind)

                    ComputationGraph train_cg = BuildComputationGraph(DataPanel.TrainImage, DataPanel.TrainLabel, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = ComputeLib },
                        ConvLinkStruct, FullLinkStruct);

                    ComputationGraph valid_cg = BuildComputationGraph(DataPanel.ValidImage, DataPanel.ValidLabel, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = ComputeLib },
                        ConvLinkStruct, FullLinkStruct);


                    //ImageLabelDataSource trainSet = new ImageLabelDataSource(DataPanel.Train, DataPanel.TrainLabel);
                    //ImageLabelDataSource validSet = new ImageLabelDataSource(DataPanel.Valid, DataPanel.ValidLabel);

                    //LossDerivParameter lossDerivParameter = new LossDerivParameter()
                    //{
                    //    InputDataType = DataSourceID.ImageLabelData,
                    //    OutputDataType = DataSourceID.HiddenBatchData,
                    //    lossType = ImageCNNBuilderParameters.LossFunction,
                    //    Gamma = ImageCNNBuilderParameters.Gamma,
                    //    LabelMinValue = ImageCNNBuilderParameters.LabelMinValue,
                    //    LabelMaxValue = ImageCNNBuilderParameters.LabelMaxValue
                    //};

                    //trainSet , 
                    //ImageCNNRunner<ImageLabelDataSource> trainer = new ImageCNNRunner<ImageLabelDataSource>(cnnModel, new RunnerBehavior() { RunMode = DNNRunMode.Train });
                    //trainer.LossParameter = lossDerivParameter;

                    //if (trainer.GradientCheckRandomVector(DataPanel.Train.GetInstances(false)))
                    //    Logger.WriteLog("Gradient Check Successfully!");
                    //else
                    //    Logger.WriteLog("Gradient Check Failed!");

                    //cnnModel.InitOptimizer(new StructureLearner()
                    //{
                    //    LearnRate = ImageCNNBuilderParameters.LearnRate,
                    //    Optimizer = ImageCNNBuilderParameters.Optimizer,
                    //    AdaBoost = ImageCNNBuilderParameters.AdaBoost,
                    //    BatchNum = DataPanel.Train.Stat.SrcStat.TotalBatchNumber,
                    //    EpochNum = ImageCNNBuilderParameters.Iteration
                    //});


                    //validSet, 
                    //ImageCNNRunner<ImageLabelDataSource> evaler = new ImageCNNRunner<ImageLabelDataSource>(cnnModel, new RunnerBehavior() { RunMode = DNNRunMode.Predict });
                    //evaler.Predict(DataPanel.Valid.GetInstances(false), evalParameter);
                    double score = valid_cg.Execute();
                    Logger.WriteLog("Prediction Accuracy {0}, at Iteration {1}", score, -1);

                    double best_score = 0;
                    double best_iter = -1;
                    for (int iter = 0; iter < BuilderParameters.Iteration; iter++)
                    {
                        double loss = train_cg.Execute();
                        Logger.WriteLog("Train Loss {0}", loss);
                        score = valid_cg.Execute();
                        Logger.WriteLog("Prediction Accuracy {0}, at Iteration {1}", score, iter);

                        if (score > best_score)
                        {
                            best_score = score;
                            best_iter = iter;
                        }
                        Logger.WriteLog("Best Accuracy {0}, at Iteration {1}", best_score, best_iter);
                    }
                    break;
                case DNNRunMode.Predict:

                    //ImageCNNStructure pcnnModel = new ImageCNNStructure(
                    //        new ImageInputParameter() { Width = DataPanel.Valid.Stat.SrcStat.Width, Height = DataPanel.Valid.Stat.SrcStat.Height, Depth = DataPanel.Valid.Stat.SrcStat.Depth },
                    //        ImageCNNBuilderParameters.Filters(DataPanel.Valid.Stat.SrcStat.Depth),
                    //        ImageCNNBuilderParameters.LAYER_DIM, ImageCNNBuilderParameters.ACTIVATION, ImageCNNBuilderParameters.LAYER_DROPOUT, ImageCNNBuilderParameters.LAYER_BIAS);

                    //pcnnModel.Init();
                    //pcnnModel.InitOptimizer(new StructureLearner()
                    //{
                    //    LearnRate = ImageCNNBuilderParameters.LearnRate,
                    //    Optimizer = ImageCNNBuilderParameters.Optimizer,
                    //    AdaBoost = ImageCNNBuilderParameters.AdaBoost,
                    //    BatchNum = DataPanel.Valid.Stat.TotalBatchNumber,
                    //    EpochNum = ImageCNNBuilderParameters.Iteration
                    //});

                    //ImageLabelDataSource pvalidSet = new ImageLabelDataSource(DataPanel.Valid, DataPanel.ValidLabel);

                    //pvalidSet,
                    //ImageCNNRunner<ImageLabelDataSource> pevaler = new ImageCNNRunner<ImageLabelDataSource>(pcnnModel, new RunnerBehavior() { RunMode = DNNRunMode.Predict });
                    //pevaler.Predict(DataPanel.Valid.GetInstances(false), evalParameter);

                    //Logger.WriteLog("Loading Validation Data.");
                    //DataPanel.Init();
                    //Logger.WriteLog("Load Data Finished.");

                    //Logger.WriteLog("Loading DNN Structure.");
                    //DNNStructure pDnnModel = new DNNStructure(CNNBuilderParameters.ModelOutputPath, DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR, DeviceType.GPU);

                    //EvaluationSet pEvaluator = EvaluationSet.CreateEvaluation(CNNBuilderParameters.Evaluation);

                    //DNNRunner<MatchBatchInputData> pEvaler = new DNNRunner<MatchBatchInputData>(pDnnModel, DataPanel.Valid, new RunnerBehavior() { RunMode = DNNRunMode.Predict });
                    //DataProcessor pValidProcessor = new DataProcessor(DataPanel.Valid);
                    //pValidProcessor.Init();
                    //while (pValidProcessor.Next())
                    //{
                    //    pEvaler.Forward();
                    //    pEvaler.Output.Output.Data.CopyOutFromCuda();
                    //    switch (DNNBuilderParameters.Evaluation)
                    //    {
                    //        case EvaluationType.AUC:
                    //            pEvaluator.PushScore(pEvaler.Input.MatchLabelValue.MemPtr, pEvaler.Output.Output.Data.MemPtr, pEvaler.Output.Output.BatchSize);
                    //            break;
                    //        case EvaluationType.MSE:
                    //            pEvaluator.PushScore(pEvaler.Input.MatchLabelValue.MemPtr, pEvaler.Output.Output.Data.MemPtr, pEvaler.Output.Output.BatchSize);
                    //            break;
                    //    }
                    //}
                    //List<string> pEvalInfo = new List<string>();
                    //float pScore = pEvaluator.Evaluation(out pEvalInfo);
                    //Logger.WriteLog("Evaluation :\n{0}", string.Join("\n", pEvalInfo));
                    break;
            }
            Logger.CloseLog();
        }


        public class DataPanel
        {
            public static DataCashier<ImageDataSource, ImageDataStat> TrainImage = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainLabel = null;

            public static DataCashier<ImageDataSource, ImageDataStat> ValidImage = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidLabel = null;

            public static void Init()
            {
                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    TrainImage = new DataCashier<ImageDataSource, ImageDataStat>(BuilderParameters.TrainData);
                    TrainLabel = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainLabel);

                    TrainImage.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainLabel.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                }

                ValidImage = new DataCashier<ImageDataSource, ImageDataStat>(BuilderParameters.ValidData);
                ValidLabel = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidLabel);

                ValidImage.InitThreadSafePipelineCashier(100, false);
                ValidLabel.InitThreadSafePipelineCashier(100, false);
            }
        }

    }
}
