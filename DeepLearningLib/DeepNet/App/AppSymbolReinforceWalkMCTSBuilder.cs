﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    /// <summary>
    /// perform AI search task with Reinforcement Learning Algorithm.
    /// </summary>
    public class AppSymbolReinforceWalkMCTSBuilder : Builder
    {
        /// <summary>
        /// 
        /// </summary>
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.

            /// <summary>
            /// Train Dataset.
            /// </summary>
            public static string TRAIN_DATA { get { return Argument["TRAIN-DATA"].Value; } }

            /// <summary>
            /// Valid Dataset.
            /// </summary>
            public static string VALID_DATA { get { return Argument["VALID-DATA"].Value; } }

            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }
           
            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            
            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static float T_MAX_CLIP { get { return float.Parse(Argument["T-MAX-CLIP"].Value); } }
            public static float T_MIN_CLIP { get { return float.Parse(Argument["T-MIN-CLIP"].Value); } }

            public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static int[] S_NET { get { return Argument["S-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] S_AF { get { return Argument["S-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }


            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }

            public static float NORM_REWARD { get { return float.Parse(Argument["NORM-REWARD"].Value); } }

            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            public static int MCTS_NUM { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

            public static int TEST_MCTS_NUM { get { int t = int.Parse(Argument["TEST-MCTS-NUM"].Value); return t > 0 ? t : MCTS_NUM; } }

            public static float MSE_LAMBDA { get { return float.Parse(Argument["MSE-LAMBDA"].Value); } }
            public static float SCORE_LAMBDA { get { return float.Parse(Argument["SCORE-LAMBDA"].Value); } }

            public static float RAND_TERM { get { return float.Parse(Argument["RAND-TERM"].Value); } }

            public static float UCB1_C { get { return float.Parse(Argument["UCB1-C"].Value); } }
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }

            public static List<Tuple<int, float>> ScheduleRewardDiscount
            {
                get
                {
                    return Argument["R-FB"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static float Epsilon(int step)
            {
                for (int i = 0; i < ScheduleRewardDiscount.Count; i++)
                {
                    if (step < ScheduleRewardDiscount[i].Item1)
                    {
                        float lambda = (step - ScheduleRewardDiscount[i - 1].Item1) * 1.0f / (ScheduleRewardDiscount[i].Item1 - ScheduleRewardDiscount[i - 1].Item1);
                        return lambda * ScheduleRewardDiscount[i].Item2 + (1 - lambda) * ScheduleRewardDiscount[i - 1].Item2;
                    }
                }
                return ScheduleRewardDiscount.Last().Item2;
            }

            public static int EStrategy(int mcts_idx)
            {
                string[] idxs = Argument["MCTS-EXP"].Value.Split(',').ToArray();
                for (int i = 0; i < idxs.Length; i++)
                {
                    if(mcts_idx < int.Parse(idxs[i]))
                    {
                        return i;
                    }
                }
                return idxs.Length - 1;
            }

            public static int MEM_CLEAN { get { return int.Parse(Argument["MEM-CLEAN"].Value); } }

            public static int SCORE_TYPE { get { return int.Parse(Argument["SCORE-TYPE"].Value); } }
            
            public static float UPDATE_R_DISCOUNT { get { return float.Parse(Argument["UPDATE-R-DISCOUNT"].Value); } }

            public static float BASE_LAMBDA { get { return float.Parse(Argument["BASE-LAMBDA"].Value); } }
            public static float PROB_LAMBDA { get { return float.Parse(Argument["PROB-LAMBDA"].Value); } }

            public static float PUCB_C { get { return float.Parse(Argument["PUCB-C"].Value); } }
            public static float PUCB_B { get { return float.Parse(Argument["PUCB-B"].Value); } }
            public static float PUCB_M { get { return float.Parse(Argument["PUCB-M"].Value); } }

            public static int FeaType { get { return int.Parse(Argument["FEA-TYPE"].Value); } }
            public static int CONTAIN_NUM { get { return int.Parse(Argument["CONTAIN-NUM"].Value); } }
            public static int CONTAIN_LIM { get { return int.Parse(Argument["CONTAIN-LIM"].Value); } }

            public static string RESULT_FILE { get { return Argument["RESULT-FILE"].Value; } }

            public static int CRITIC_TYPE { get { return int.Parse(Argument["CRITIC-TYPE"].Value); } }

            public static float BETA { get { return float.Parse(Argument["BETA"].Value); } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                Argument.Add("TRAIN-DATA", new ParameterArgument(string.Empty, "Training Dataset"));
                Argument.Add("VALID-DATA", new ParameterArgument(string.Empty, "Valid Dataset"));

                Argument.Add("DNN-DIMS", new ParameterArgument("100,100","DNN Map Dimensions."));

                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));

                Argument.Add("S-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("S-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));

                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.9", "Reward discount"));

                Argument.Add("T-MAX-CLIP", new ParameterArgument("0", "Maximum T Clip"));
                Argument.Add("T-MIN-CLIP", new ParameterArgument("0", "Minimum T Clip"));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("NORM-REWARD", new ParameterArgument("0", "normalize reward."));
                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));

                Argument.Add("TRAIN-DEBUG", new ParameterArgument("0", "0:use dev set as train; 1: use train set"));

                Argument.Add("MCTS-NUM", new ParameterArgument("64", "Monto Carlo Tree Search Number"));
                Argument.Add("TEST-MCTS-NUM", new ParameterArgument("0", "Monto Carlo Tree Search Number"));
                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));
                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0","Prob lambda in scoring function."));
                Argument.Add("SCORE-LAMBDA", new ParameterArgument("1.0", "score lambda in scoring function."));

                Argument.Add("MCTS-EXP", new ParameterArgument("8,16,24,48,64", "Exploration strategy."));
                Argument.Add("RAND-TERM", new ParameterArgument("0.3", "Random termination gate."));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("MEM-CLEAN", new ParameterArgument("1", "1: clean memory everytime."));
                Argument.Add("SCORE-TYPE", new ParameterArgument("0", "0:probability; 1:pesudo reward;"));
                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1","mcts reward discount."));
                Argument.Add("FEA-TYPE", new ParameterArgument("3", "feature type"));

                Argument.Add("CONTAIN-NUM", new ParameterArgument("3", "Contain Num."));
                Argument.Add("CONTAIN-LIM", new ParameterArgument("20", "Contain Lim."));

                Argument.Add("RESULT-FILE", new ParameterArgument("tmp.result", "result file"));
                Argument.Add("CRITIC-TYPE", new ParameterArgument("0:no critic; 1:has critic;", "Critic type"));

                Argument.Add("BETA", new ParameterArgument("0", "policy entropy."));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_SYMBOL_REINFORCE_WALK_MCTS; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        /// <summary>
        /// episodic memory structure.
        /// </summary>
        //public class EpisodicMemory
        //{
        //    Dictionary<string, Tuple<List<Tuple<float, float>>, int>> Mem = new Dictionary<string, Tuple<List<Tuple<float, float>>, int>>();
        //    int Time { get; set; }
        //    public EpisodicMemory()
        //    {
        //        Time = 0;
        //    }
        //    public void Clear()
        //    {
        //        Mem.Clear();
        //        Time = 0;
        //    }
        //    /// <summary>
        //    /// memory index.
        //    /// </summary>
        //    /// <param name="key"></param>
        //    /// <param name="actionCount"></param>
        //    /// <param name="topK"></param>
        //    /// <returns></returns>
        //    public List<Tuple<float, float>> Search(string key)
        //    {
        //        if (Mem.ContainsKey(key))
        //        {
        //            return Mem[key].Item1;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }

        //    public void Update(string key, int actid, int totalAct, float reward, float v = 1.0f)
        //    {
        //        float new_u = 0;
        //        float new_c = 0;

        //        // update memory.
        //        if (Mem.ContainsKey(key))
        //        {
        //            float u = Mem[key].Item1[actid].Item1;
        //            float c = Mem[key].Item1[actid].Item2;
        //            new_u = c / (c + v) * u + v / (c + v) * reward;
        //            new_c = c + v;
        //        }
        //        else
        //        {
        //            Mem.Add(key, new Tuple<List<Tuple<float, float>>, int>(new List<Tuple<float, float>>(), Time));
        //            for (int a = 0; a < totalAct; a++)
        //            {
        //                Mem[key].Item1.Add(new Tuple<float, float>(0, 0));
        //            }
        //            new_u = reward;
        //            new_c = v;
        //        }
        //        Mem[key].Item1[actid] = new Tuple<float, float>(new_u, new_c);
        //    }

        //    public void UpdateTiming()
        //    {
        //        Time += 1;
        //    }
        //}

        public class EpisodicMemoryV2
        {
            public Dictionary<string, Tuple<float, float>> Mem = new Dictionary<string, Tuple<float, float>>();
            public bool IsGlobal = true;
            public EpisodicMemoryV2(bool isGlobal)
            {
                IsGlobal = isGlobal;
            }
            public void Clear()
            {
                Mem.Clear();
            }
            /// <summary>
            /// memory index.
            /// </summary>
            /// <param name="key"></param>
            /// <param name="actionCount"></param>
            /// <param name="topK"></param>
            /// <returns></returns>
            public Tuple<float, float> Search(string key)
            {
                if (Mem.ContainsKey(key))
                {
                    return Mem[key];
                }
                else
                {
                    return new Tuple<float, float>(0, 0);
                }
            }

            public void Update(string key, float reward, float v = 1.0f)
            {
                float new_u = 0;
                float new_c = 0;

                // update memory.
                if (Mem.ContainsKey(key))
                {
                    float u = Mem[key].Item1;
                    float c = Mem[key].Item2;
                    new_u = c / (c + v) * u + v / (c + v) * reward;
                    new_c = c + v;
                }
                else
                {
                    new_u = reward;
                    new_c = v;
                }
                Mem[key] = new Tuple<float, float>(new_u, new_c);
            }
        }

        public class BanditAlg
        {
            /// <summary>
            /// Exp 0.
            /// </summary>
            /// <param name="prior"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int UniformRandomStrategy0(int dim, Random random)
            {
                return random.Next(dim);
            }

            /// <summary>
            /// Exp 1.
            /// </summary>
            /// <param name="actdim"></param>
            /// <param name="rand_term"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int CascadeRandomStrategy1(int actdim, float rand_term, Random random)
            {
                if (random.NextDouble() > rand_term)
                {
                    return actdim;
                }
                return random.Next(actdim);
            }

            /// <summary>
            /// Exp 2.
            /// </summary>
            /// <param name="prior"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int ThompasSampling(float[] prior, Random random)
            {
                int idx = Util.Sample(prior.ToArray(), random);
                return idx;
            }

            public static int UCBBandit(List<Tuple<float, float>> arms, float c, Random random)
            {
                float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);
                List<float> v = new List<float>();
                foreach (Tuple<float, float> arm in arms)
                {
                    v.Add(arm.Item1 / (arm.Item2 + 0.1f) + c * (float)Math.Sqrt(log_total / (arm.Item2 + 0.1f)) + (float)random.NextDouble() * 0.0001f);
                }
                int idx = Util.MaximumValue(v.ToArray());
                return idx;
            }

            /// <summary>
            /// Exp 3. Standard UCB Bandit.
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int UCB1Bandit(List<Tuple<float, float>> arms, int dim, float c, Random random)
            {
                if (arms == null)
                {
                    return UniformRandomStrategy0(dim, random);
                }
                float log_total = (float)Math.Log(arms.Take(dim).Select(i => i.Item2).Sum() + 1.0f);
                List<float> v = new List<float>();
                foreach (Tuple<float, float> arm in arms.Take(dim))
                {
                    v.Add(arm.Item1 / (arm.Item2 + 0.1f) + c * (float)Math.Sqrt(log_total / (arm.Item2 + 0.1f)) + (float)random.NextDouble() * 0.0001f);
                }
                int idx = Util.MaximumValue(v.ToArray());
                
                return idx;
            }

            public static int PUCB(List<Tuple<float, float>> arms, List<float> prior, float mb, float c, float m, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    throw new NotImplementedException("PUCB arms is null");
                    // select the max prior value.
                    //for (int i = 0; i < prior.Count; i++)
                    //{
                    //    float s = prior[i] + (float)random.NextDouble() * 0.001f;
                    //    if (s > maxV)
                    //    {
                    //        maxV = s;
                    //        maxI = i;
                    //    }
                    //}
                }
                else
                {
                    float sum = prior.Sum() + mb * prior.Count;

                    float t = (float)arms.Select(i => i.Item2).Sum();

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float si = arms[i].Item1;
                        float xi = (arms[i].Item2 == 0 ? 1 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        float ci = si == 0 ? 0 : c * (float)Math.Sqrt(Math.Log(t) / si);
                        float mi = (t == 0 ? 1 : (float)Math.Sqrt(Math.Log(t) / t)) * m * sum / (prior[i] + mb);
                        float s = xi + ci - mi;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }


            /// <summary>
            /// Exp 4. PUCT.
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int AlphaGoZeroBandit(List<Tuple<float, float>> arms, List<float> prior, int dim, float c, float d, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;
                {
                    float total = (float)arms.Take(dim).Select(i => i.Item2).Sum();

                    for (int i = 0; i < dim; i++)
                    {

                        if(double.IsNaN(prior[i]) || double.IsNaN(prior[i]))
                        {
                            throw new Exception("Wrong on prior probability!");
                        }

                        float s = c * (float)Math.Pow(prior[i], d) * (float)Math.Sqrt(Math.Log(total + 1.1f) / (arms[i].Item2 + 0.1f)) +
                                                       (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2)) + (float)random.NextDouble() * 0.0001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }


            public static int AlphaGoZeroBanditV2(List<Tuple<float, float>> arms, List<float> prior, float c, float d, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                {
                    float total = (float)arms.Take(prior.Count).Select(i => i.Item2).Sum() + 1;

                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i],d) * (float)Math.Sqrt(total) / (arms[i].Item2 + 1.0f) +
                                                       (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2)) + (float)random.NextDouble() * 0.0001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }

            public static int AlphaGoZeroBanditV3(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float total = (float)arms.Select(i => i.Item2).Sum();

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float s = c * prior[i] * (float)Math.Sqrt(total) / (arms[i].Item2 + 1.0f) +
                                                       (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }

            /// <summary>
            /// mb = 0.00001;
            /// c = exploration.
            /// m = 
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="mb"></param>
            /// <param name="c"></param>
            /// <param name="m"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int PUCB2(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() );
                    List<float> v = new List<float>();
                    foreach (Tuple<float, float> arm in arms)
                    {
                        v.Add(arm.Item1 / (arm.Item2 + 1.0f) + c * (float)Math.Sqrt(log_total / (arm.Item2)) + (float)random.NextDouble() * 0.0001f);
                    }
                    int idx = Util.MaximumValue(v.ToArray());
                }
                return maxI;
            }


        }

        public class GraphQueryData : BatchData
        {
            public List<SymbolicState> RawQuery = new List<SymbolicState>();
            
            public List<int> RawIndex = new List<int>();

            public int BatchSize { get { return RawQuery.Count; } }
            public int MaxBatchSize { get; set; }

            public List<StatusData> StatusPath = new List<StatusData>();
            public List<Tuple<int, int>> Results = new List<Tuple<int, int>>();
            public List<int> VisitNum = new List<int>();
            /// <summary>
            /// max group number, and group size;
            /// </summary>
            /// <param name="maxGroupNum"></param>
            /// <param name="groupSize"></param>
            /// <param name="device"></param>
            public GraphQueryData(int maxBatchSize, DeviceType device)
            {
                MaxBatchSize = maxBatchSize;
            }

            public GraphQueryData(GraphQueryData data)
            {
                RawQuery = data.RawQuery;
                RawIndex = data.RawIndex;

                MaxBatchSize = data.MaxBatchSize;
            }

            public List<int> GetBatchIdxs(int b, int step)
            {
                List<int> r = new List<int>();
                for (int i = 0; i < StatusPath[step].BatchSize; i++)
                {
                    if (StatusPath[step].GetOriginalStatsIndex(i) == b)
                        r.Add(i);
                }
                return r;
            }
        }

        public class StatusData : BatchData
        {
            /// <summary>
            /// Raw Query.
            /// </summary>
            public GraphQueryData GraphQuery { get; set; }

            /// <summary>
            /// Node ID.
            /// </summary>
            public List<SymbolicState> NodeID { get; set; }

            /// <summary>
            /// Termination Probability.
            /// </summary>
            public HiddenBatchData Term = null;

            /// <summary>
            /// Score for each instance.
            /// </summary>
            public HiddenBatchData Score = null;

            /// <summary>
            /// LogProbability to this node.
            /// </summary>
            List<float> LogProb { get; set; }

            /// <summary>
            /// MatchCandidate and MatchCandidateProb.
            /// </summary>
            public List<SymbolicState> MatchCandidate = null;
            public SeqVectorData MatchCandidateProb = null;
            
            public List<int> PreSelActIndex = null;
            public List<int> PreStatusIndex = null;
            public int GetPreStatusIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else return PreStatusIndex[batchIdx];
            }
            public int GetOriginalStatsIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else
                {
                    int b = GetPreStatusIndex(batchIdx);
                    return GraphQuery.StatusPath[Step - 1].GetOriginalStatsIndex(b);
                }
            }

            //public List<string> StatusKey = null;
            public string GetStatusKey(int b)
            {
                return NodeID[b].StateKey; // GraphQuery.RawQuery[b]
                //if (Step == 0) return string.Format("{0}", GraphQuery.RawQuery[b].StateKey);
                //else return StatusKey[b];
            }

            /// <summary>
            /// constrastive reward.
            /// </summary>
            public List<float> CR { get; set; }
            
            public int MaxBatchSize { get { return StateEmbed.MAX_BATCHSIZE; } }
            public int BatchSize { get { return StateEmbed.BatchSize; } }

            /// <summary>
            /// logprobability of the node.
            /// </summary>
            /// <param name="batchIdx"></param>
            /// <returns></returns>
            public float GetLogProb(int batchIdx)
            {
                if (Step == 0) { return 0; }
                else { return LogProb[batchIdx]; }
            }

            /// <summary>
            /// Log Termination Probability.
            /// </summary>
            /// <param name="b"></param>
            /// <param name="isT"></param>
            /// <returns></returns>
            public float LogPTerm(int b, bool isT)
            {
                if (isT) { return (float)Util.LogLogistial(Term.Output.Data.MemPtr[b]); }
                else { return (float)Util.LogNLogistial(Term.Output.Data.MemPtr[b]); }
            }

            
            public int GetActionDim(int b)
            {
                return GetActionEndIndex(b) - GetActionStartIndex(b);
            }

            public int GetActionStartIndex(int b)
            {
                return b == 0 ? 0 : MatchCandidateProb.SegmentIdx.MemPtr[b - 1];
            }
            public int GetActionEndIndex(int b)
            {
                return MatchCandidateProb.SegmentIdx.MemPtr[b];
            }

            /// <summary>
            /// Embedding of the State.
            /// </summary>
            public HiddenBatchData StateEmbed { get; set; }

            public int Step;

            public StatusData(GraphQueryData interData, List<SymbolicState> nodeIndex, HiddenBatchData stateEmbed, DeviceType device) :
                this(interData, nodeIndex, null, null, null, null, stateEmbed, device)
            { }

            public StatusData(GraphQueryData interData, 
                              List<SymbolicState> nodeIndex, List<float> logProb, List<int> selectedAction, List<int> preStatusIndex, List<string> statusKey,
                              HiddenBatchData stateEmbed, DeviceType device)
            {
                GraphQuery = interData;
                GraphQuery.StatusPath.Add(this);

                NodeID = nodeIndex;
                StateEmbed = stateEmbed;

                LogProb = logProb;
                PreSelActIndex = selectedAction;
                PreStatusIndex = preStatusIndex;
                //StatusKey = statusKey;
                //SelectedAction = selectedAction;

                CR = new List<float>();

                Step = GraphQuery.StatusPath.Count - 1;
            }
        }

        /// <summary>
        /// Sample Graph. 
        /// </summary>
        class SampleRunner : StructRunner
        {
            List<SymbolicState> Graph { get; set; }
            public new GraphQueryData Output { get; set; }
            int MaxBatchSize { get; set; }
            DataRandomShuffling Shuffle { get; set; }
            Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1);

            public SampleRunner(List<SymbolicState> graph, int maxBatchSize, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Graph = graph;
                MaxBatchSize = maxBatchSize;
                Shuffle = new DataRandomShuffling(Graph.Count, random);
                Output = new GraphQueryData(maxBatchSize, behavior.Device);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
            }

            public override void Forward()
            {
                Output.RawQuery.Clear();
                Output.RawIndex.Clear();

                int groupIdx = 0;

                while (groupIdx < MaxBatchSize)
                {
                    int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext();
                    if (idx <= -1) { break; }

                    Output.RawQuery.Add(Graph[idx]);
                    Output.RawIndex.Add(idx);
                    groupIdx += 1;
                }
                if (groupIdx == 0) { IsTerminate = true; return; }
            }
        }
        
        class StatusEmbedRunner : CompositeNetRunner
        {
            public new List<SymbolicState> Input { get; set; }

            public new HiddenBatchData Output { get; set; }
            int Type = 0;

            /// <summary>
            /// Type 0: Query Feature; Type 1: State Feature.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="maxBatchSize"></param>
            /// <param name="type"></param>
            /// <param name="behavior"></param>
            public StatusEmbedRunner(List<SymbolicState> input, int maxBatchSize, int type, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;
                Type = type;
                // concate of node embedding and relation embedding.
                Output = new HiddenBatchData(maxBatchSize, SymbolicState.CurrentFeaDim, DNNRunMode.Train, behavior.Device);
            }

            public override void Forward()
            {
                //BatchLinks.Clear();
                int batchSize = 0;
                while (batchSize < Input.Count)
                {
                    if (Type == 0)
                        Array.Copy(Input[batchSize].C_QFea, 0, Output.Output.Data.MemPtr, batchSize * SymbolicState.CurrentFeaDim, SymbolicState.CurrentFeaDim);
                    else if (Type == 1)
                        Array.Copy(Input[batchSize].C_SFea, 0, Output.Output.Data.MemPtr, batchSize * SymbolicState.CurrentFeaDim, SymbolicState.CurrentFeaDim);
                    batchSize += 1;
                }
                Output.BatchSize = batchSize;
                Output.Output.Data.SyncFromCPU(Output.BatchSize * Output.Dim);
            }
        }

        class CandidateActionRunner : CompositeNetRunner
        {
            public new StatusData Input;

            public new List<SymbolicState> Output = new List<SymbolicState>();
            public BiMatchBatchData Match;

            /// <summary>
            /// Input : State.
            /// node and rel embed.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="nodeEmbed"></param>
            /// <param name="relEmbed"></param>
            /// <param name="behavior"></param>
            public CandidateActionRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;

                Match = new BiMatchBatchData(new BiMatchBatchDataStat() {
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize * SymbolicState.MaxNeighborNum,
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * SymbolicState.MaxNeighborNum }, behavior.Device);
            }

            //Random random = new Random();
            public override void Forward()
            {
                Output.Clear();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
                int cursor = 0;
                for (int b = 0; b < Input.BatchSize; b++)
                {
                    SymbolicState seedNode = Input.NodeID[b];

                    List<SymbolicState> neis = seedNode.NeighborStates();
                    for (int nei = 0; nei < neis.Count; nei++)
                    {
                        Output.Add(neis[nei]);
                        match.Add(new Tuple<int, int, float>(b, cursor, 1));
                        cursor += 1;
                     }
                }
                Match.SetMatch(match);
            }
        }

        static Random SampleRandom = new Random(DeepNet.BuilderParameters.RandomSeed);
        
        public class BasicPolicyRunner : CompositeNetRunner
        {
            public new StatusData Input { get; set; }

            /// <summary>
            /// MatchData.
            /// </summary>
            public BiMatchBatchData MatchPath = null;

            /// <summary>
            /// new Node Index.
            /// </summary>
            public List<SymbolicState> NodeIndex { get; set; }

            /// <summary>
            /// next step log probability.
            /// </summary>
            public List<float> LogProb { get; set; }
            
            /// <summary>
            /// pre sel action and pre status. 
            /// </summary>
            public List<int> PreSelActIndex { get; set; }
            public List<int> PreStatusIndex { get; set; }

            public List<string> StatusKey { get; set; }

            public BasicPolicyRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;

                NodeIndex = new List<SymbolicState>();
                LogProb = new List<float>();
                PreSelActIndex = new List<int>();
                PreStatusIndex = new List<int>();
                StatusKey = new List<string>();
            }
        }

        class BeamSearchActionRunner : BasicPolicyRunner
        {
            int BeamSize = 1;
            bool IsLast = false;
            /// <summary>
            /// group aware beam search.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public BeamSearchActionRunner(StatusData input, int beamSize, RunnerBehavior behavior, bool isLast) : base(input, behavior)
            {
                BeamSize = beamSize;
                IsLast = isLast;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * SymbolicState.MaxNeighborNum,
                    MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
                }, behavior.Device);
                //GroupIndex = new List<int>();
            }

            public override void Forward()
            {
                if(Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }

                Input.MatchCandidateProb.Output.SyncToCPU();
                if(Input.Term != null) Input.Term.Output.SyncToCPU();

                NodeIndex.Clear();
                LogProb.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                StatusKey.Clear();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
                // for each beam.
                for (int i = 0; i < Input.GraphQuery.BatchSize; i++) //.MatchCandidateProb.Segment; i++)
                {
                    List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);

                    MinMaxHeap<Tuple<int, int>> topKheap = new MinMaxHeap<Tuple<int, int>>(BeamSize, 1);

                    foreach (int b in idxs)
                    {
                        int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[b];
                        int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

                        float nonLogTerm = Input.LogPTerm(b, false);
                        //if (Input.Step > 0)
                        {
                            float tprob = Input.GetLogProb(b) + Input.LogPTerm(b, true);
                            topKheap.push_pair(new Tuple<int, int>(b, -1), tprob);
                        }
                        if (!IsLast)
                        {
                            for (int t = s; t < e; t++)
                            {
                                float prob = Input.GetLogProb(b) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t]);
                                topKheap.push_pair(new Tuple<int, int>(b, t), prob);
                            }
                        }
                    }

                    while (!topKheap.IsEmpty)
                    {
                        KeyValuePair<Tuple<int, int>, float> p = topKheap.PopTop();

                        if(p.Key.Item2 == -1)
                        {
                            Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, p.Key.Item1));
                            continue;
                        }

                        match.Add(new Tuple<int, int, float>(p.Key.Item1, p.Key.Item2, p.Value));

                        NodeIndex.Add(Input.MatchCandidate[p.Key.Item2]);
                        LogProb.Add(p.Value);

                        PreStatusIndex.Add(p.Key.Item1);
                        PreSelActIndex.Add(p.Key.Item2);

                        string statusQ = string.Format("{0}-{1}", Input.GetStatusKey(p.Key.Item1), Input.MatchCandidate[p.Key.Item2].StateKey);
                        
                        StatusKey.Add(statusQ);
                    }
                }
                MatchPath.SetMatch(match);
            }
        }

        class MCTSActionSamplingRunner : BasicPolicyRunner
        {
            int lineIdx = 0;
            List<EpisodicMemoryV2> Memory { get; set; }
            //Random random = new Random(21);
            public int MCTSIdx { get; set; }
            bool IsLast = false;
            public override void Init()
            {
                lineIdx = 0;
            }
            /// <summary>
            /// match candidate, and match probability.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public MCTSActionSamplingRunner(StatusData input, int mctsIdx, List<EpisodicMemoryV2> memory, RunnerBehavior behavior, bool isLast) : base(input, behavior)
            {
                Memory = memory;
                MCTSIdx = mctsIdx;
                IsLast = isLast;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * SymbolicState.MaxNeighborNum,
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                }, behavior.Device);
            }

            public override void Forward()
            {
                NodeIndex.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                LogProb.Clear();
                StatusKey.Clear();

                Input.MatchCandidateProb.Output.SyncToCPU();
                if (Input.Term != null) Input.Term.Output.SyncToCPU();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

                int currentStep = Input.Step;
                for (int i = 0; i < Input.MatchCandidateProb.Segment; i++)
                {
                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

                    int selectIdx = s;

                    List<Tuple<float, float>> m = new List<Tuple<float, float>>();
                    for (int n = s; n < e; n++)
                    {
                        m.Add(Memory[0].Search(Input.MatchCandidate[n].StateKey));
                    }

                    if(Input.Term!= null) { m.Add(Memory[0].Search(Input.NodeID[i].TerminateKey)); }
                    

                    float[] prior_r = null;

                    int actionDim = e - s;
                    if (Input.Term != null)
                    {
                        prior_r = new float[actionDim + 1];
                        Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);

                        float t = Util.Logistic(Input.Term.Output.Data.MemPtr[i]);
                        if (t >= BuilderParameters.T_MAX_CLIP) { t = BuilderParameters.T_MAX_CLIP; }
                        if (t <= BuilderParameters.T_MIN_CLIP) { t = BuilderParameters.T_MIN_CLIP; }

                        prior_r[actionDim] = t;
                        for (int a = 0; a < actionDim; a++) { prior_r[a] = prior_r[a] * (1 - t); }
                    }
                    else
                    {
                        throw new NotImplementedException("Term should not be null.");
                        //prior_r = new float[actionDim];
                        //Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);
                    }

                    int idx = 0;
                    int strategy = BuilderParameters.EStrategy(MCTSIdx);
                    //Console.WriteLine("MCTS Index {0}, strategy {1}", MCTSIdx, strategy);
                    switch(strategy)
                    {
                        case 0: idx = BanditAlg.UniformRandomStrategy0(prior_r.Length, SampleRandom); break;
                        case 1:
                            {
                                idx = BanditAlg.CascadeRandomStrategy1(actionDim, BuilderParameters.RAND_TERM, SampleRandom);
                            }
                            break;
                        case 2: idx = BanditAlg.ThompasSampling(prior_r, SampleRandom); break;
                        case 3: idx = BanditAlg.UCB1Bandit(m, prior_r.Length, BuilderParameters.UCB1_C, SampleRandom); break;
                        case 4: idx = BanditAlg.AlphaGoZeroBandit(m, prior_r.ToList(), prior_r.Length, BuilderParameters.PUCT_C, BuilderParameters.PUCT_D, SampleRandom); break;
                        case 5: idx = BanditAlg.AlphaGoZeroBanditV2(m, prior_r.ToList(), BuilderParameters.PUCT_C, BuilderParameters.PUCT_D, SampleRandom); break;
                        case 6: idx = BanditAlg.PUCB(m, prior_r.ToList(), BuilderParameters.PUCB_B, BuilderParameters.PUCB_C, BuilderParameters.PUCB_M, SampleRandom); break;
                    }

                    if(IsLast)
                    {
                        //select the termination.
                        idx = actionDim;
                    }
                    //int idx = BanditAlg.ThompasSampling(m, prior_r.ToList(), 2, random);

                    // sample to termination.
                    if (idx == actionDim)
                    {
                        //if(Input.Step == 0)
                        //{
                        //    Console.WriteLine("Bug!!");
                        //}
                        Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, i));
                    }
                    else
                    {
                        selectIdx = s + idx;

                        PreSelActIndex.Add(selectIdx);
                        PreStatusIndex.Add(i);

                        //string statusQ = string.Format("{0}-{1}", Qkey, Input.MatchCandidate[selectIdx].StateKey);
                        //StatusKey.Add(statusQ);

                        NodeIndex.Add(Input.MatchCandidate[selectIdx]);

                        float nonLogTerm = 0;
                        if (Input.Term != null) { nonLogTerm = Input.LogPTerm(i, false); }

                        float prob = Input.GetLogProb(i) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);

                        if(float.IsInfinity(prob) || float.IsNegativeInfinity(prob))
                        {
                            Console.WriteLine("fsffsdf");
                        }
                        LogProb.Add(prob);

                        match.Add(new Tuple<int, int, float>(i, selectIdx, 1));
                    }
                }
                MatchPath.SetMatch(match);
                lineIdx += Input.MatchCandidateProb.Segment;
            }
        }

        /// <summary>
        /// it is a little difficult.
        /// </summary>
        class RewardRunner : ObjectiveRunner
        {
            //Random random = new Random();
            new List<GraphQueryData> Input { get; set; }
            List<EpisodicMemoryV2> Memory { get; set; }

            Dictionary<int, float> Success = new Dictionary<int, float>();
            Dictionary<int, float> StepSuccess = new Dictionary<int, float>();
            int Epoch = 0;
            public override void Init()
            {
                Success.Clear();
                StepSuccess.Clear();
            }

            public override void Complete()
            {
                Epoch += 1;
                //foreach (KeyValuePair<int, float> i in StepSuccess)
                //{
                //    Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
                //}
                int sc = Success.Where(i => i.Value > 0).Count();
                int t = Success.Count;
                Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
            }

            public RewardRunner(List<GraphQueryData> input, List<EpisodicMemoryV2> memory,  RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Memory = memory;
            }

            public override void Forward()
            {
                float trueReward = 0;
                int sampleNum = 0;
                float pos_mseloss = 0;
                float neg_mseloss = 0;
                int pos_smp = 0;
                int neg_smp = 0;
                ObjectiveDict.Clear();
                
                //for (int m = 0; m < Input.Count; m++)
                //{
                //    for (int i = 0; i < Input[m].Results.Count; i++)
                //    {
                //        int t = Input[m].Results[i].Item1;
                //        int b = Input[m].Results[i].Item2;
                //        int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);
                //        SymbolicState predId = Input[m].StatusPath[t].NodeID[b];
                //    }
                //}

                // mcts sampling.
                for (int m = 0; m < Input.Count; m++)
                {
                    for (int i = 0; i < Input[m].Results.Count; i++)
                    {
                        int t = Input[m].Results[i].Item1;
                        int b = Input[m].Results[i].Item2;
                        int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);
                        SymbolicState predId = Input[m].StatusPath[t].NodeID[b];

                        //Dictionary<int, float> targetId = Input[m].RawTarget[origialB];
                        int rawIdx = Input[m].RawIndex[origialB];

                        //float estimate_v = Math.Max(0, Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);
                        //if (Input[m].BlackTargets[origialB].Contains(predId)) { estimate_v = 0; }

                        string mkey = string.Format("Term {0}", t);
                        if (!ObjectiveDict.ContainsKey(mkey)) { ObjectiveDict[mkey] = 0; }
                        ObjectiveDict[mkey] += 1;

                        //float true_v = BuilderParameters.NEG_R ;
                        //if (targetId == predId) true_v = BuilderParameters.POS_R;
                        //else if
                        //float update_v = 0;
                        int sss = 0;
                        float true_v = 0;
                        if (predId.IsSuccess)
                        {
                            true_v = BuilderParameters.POS_R;
                            //update_v = true_v;
                            //if (BuilderParameters.NORM_REWARD > 0) update_v = true_v * 1.0f / (ReNorm[origialB][0] + 0.0000001f);
                            sss = 1;
                        }
                        else
                        {
                            true_v = BuilderParameters.NEG_R;
                            //update_v = true_v;
                            //if (BuilderParameters.NORM_REWARD > 0) update_v = true_v * 1.0f / (ReNorm[origialB][2] + 0.0000001f);
                            sss = 0;
                        }
                        float estimate_v = Util.Logistic(Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);
                            //Math.Max(0, Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);
                        //if (estimate_v > 1) estimate_v = 1;

                        //if(BuilderParameters.MSE_LAMBDA <= 0.0000001) { estimate_v = 0; }

                        if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
                        Success[rawIdx] += sss;

                        if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = 0;
                        StepSuccess[t] += sss;

                        //if (targetId == predId || !Input[m].BlackTargets[origialB].Contains(predId))
                        {
                            trueReward += true_v;
                            sampleNum += 1;

                            // MSE error for reward estimation.
                            //Input[m].StatusPath[t].Score.Deriv.Data.MemPtr[b] = BuilderParameters.MSE_LAMBDA * (true_v - estimate_v);
                            if (true_v > 0)
                            {
                                pos_mseloss += Math.Abs(true_v - estimate_v);
                                pos_smp += 1;
                            }
                            else
                            {
                                neg_mseloss += Math.Abs(true_v - estimate_v);
                                neg_smp += 1;
                            }
                            // Policy gradient for path finding.
                            StatusData st = Input[m].StatusPath[t];
                            
                            //if (t != BuilderParameters.MAX_HOP)
                            //st.Term.Deriv.Data.MemPtr[b] = Math.Max(0, (true_v - estimate_v)) * (1 - st.Term.Output.Data.MemPtr[b]);

                            float adv = true_v;
                            if (BuilderParameters.CRITIC_TYPE == 1)
                            {
                                adv = true_v - estimate_v;
                            }
                            //else if (BuilderParameters.CRITIC_TYPE == 2)
                            //{
                            //    adv = true_v - estimate_v;
                            //}

                            //if (t != BuilderParameters.MAX_HOP)
                            st.Term.Deriv.Data.MemPtr[b] =  adv * (1 - st.Term.Output.Data.MemPtr[b]);

                            if(BuilderParameters.BETA > 0)
                            {

                            }

                            // MSE error for reward estimation.
                            st.Score.Deriv.Data.MemPtr[b] = BuilderParameters.MSE_LAMBDA * (true_v - estimate_v);


                            for (int pt = t - 1; pt >= 0; pt--)
                            {
                                StatusData pst = Input[m].StatusPath[pt];
                                int pb = st.GetPreStatusIndex(b);
                                int sidx = st.PreSelActIndex[b];// - pst.GetActionStartIndex(pb);

                                float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt);

                                if (BuilderParameters.CRITIC_TYPE == 0)
                                {
                                    if (pst.Term != null)
                                    {
                                        pst.Term.Deriv.Data.MemPtr[pb] = discount * true_v * (-pst.Term.Output.Data.MemPtr[pb]);
                                    }
                                    pst.MatchCandidateProb.Deriv.MemPtr[sidx] = discount * true_v;
                                }
                                else if (BuilderParameters.CRITIC_TYPE == 1)
                                {
                                    float p_estimate_v = Util.Logistic(pst.Score.Output.Data.MemPtr[pb]);
                                    if (pst.Term != null)
                                    {
                                        pst.Term.Deriv.Data.MemPtr[pb] = (discount * true_v - p_estimate_v) * (-pst.Term.Output.Data.MemPtr[pb]);
                                    }
                                    pst.MatchCandidateProb.Deriv.MemPtr[sidx] = (discount * true_v - p_estimate_v);
                                    pst.Score.Deriv.Data.MemPtr[pb] = BuilderParameters.MSE_LAMBDA * (discount * true_v - p_estimate_v);
                                }
                                //else if(BuilderParameters.CRITIC_TYPE == 2)
                                //{
                                //    float p_estimate_v = Util.Logistic(pst.Score.Output.Data.MemPtr[pb]);
                                //    if (pst.Term != null)
                                //    {
                                //        pst.Term.Deriv.Data.MemPtr[pb] = (discount * true_v - p_estimate_v) * (-pst.Term.Output.Data.MemPtr[pb]);
                                //    }
                                //    pst.MatchCandidateProb.Deriv.MemPtr[sidx] = (discount * true_v - p_estimate_v);
                                //    //pst.Score.Deriv.Data.MemPtr[pb] = BuilderParameters.MSE_LAMBDA * (discount * true_v - p_estimate_v);
                                //}
                                //if (pst.Term != null)
                                //{
                                //    pst.Term.Deriv.Data.MemPtr[pb] = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt) * Math.Max(0, (true_v - estimate_v)) * (-pst.Term.Output.Data.MemPtr[pb]);
                                //}
                                //pst.MatchCandidateProb.Deriv.MemPtr[sidx] = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt) * Math.Max(0, (true_v - estimate_v));

                                st = pst;
                                b = pb;
                            }
                        }
                    }
                }

                //ObjectiveDict["AVG-TRUE-PATH"] = trueReward / (sampleNum + float.Epsilon);
                ObjectiveDict["TERM-POS-MSE-LOSS"] = pos_mseloss / (pos_smp + 1);
                ObjectiveDict["TERM-NEG-MSE-LOSS"] = neg_mseloss / (neg_smp + 1);

                ObjectiveDict["TERM-POS-NUM"] = pos_smp;
                ObjectiveDict["TERM-NEG-NUM"] = neg_smp;


                ObjectiveScore = trueReward / (sampleNum + float.Epsilon);

                //if(BuilderParameters.MEM_CLEAN > 0 && Epoch % BuilderParameters.MEM_CLEAN == 0) Memory.Clear();
                //average ground truth results.
                for (int p = 0; p < Input.Count; p++)
                {
                    for (int i = Input[p].StatusPath.Count - 1; i >= 0; i--)
                    {
                        StatusData st = Input[p].StatusPath[i];
                        if (st.Term != null) st.Term.Deriv.Data.SyncFromCPU();
                        if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
                        if (st.Score != null) st.Score.Deriv.SyncFromCPU();
                    }
                    Input[p].Results.Clear();
                }
                
            }
        }

        // update reward feedback 
        class RewardFeedbackRunner : StructRunner
        {
            new GraphQueryData Input { get; set; }
            List<EpisodicMemoryV2> Memory { get; set; }

            float PosMean = 0;
            float NegMean = 0;
            int PosNum = 0;
            int NegNum = 0;

            int Epoch = 0;
            public override void Init()
            {
                Epoch = 0;
                PosMean = 0;
                PosNum = 0;

                NegMean = 0;
                NegNum = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
                Logger.WriteLog("Pos Num {0}, Pos Mean {1}", PosNum, PosMean / (PosNum + 0.000001f));
                Logger.WriteLog("Neg Num {0}, Neg Mean {1}", NegNum, NegMean / (NegNum + 0.000001f));

            }

            public RewardFeedbackRunner(GraphQueryData input, List<EpisodicMemoryV2> memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Memory = memory;
            }

            public override void Forward()
            {
                // calculate termination probability.
                for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
                {
                    if (Input.StatusPath[t].Term != null)
                    {
                        ComputeLib.Logistic(Input.StatusPath[t].Term.Output.Data, 0, Input.StatusPath[t].Term.Output.Data, 0, Input.BatchSize, 1);
                        ComputeLib.ClipVector(Input.StatusPath[t].Term.Output.Data, Input.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);
                        Input.StatusPath[t].Term.Output.Data.SyncToCPU();
                        Array.Clear(Input.StatusPath[t].Term.Deriv.Data.MemPtr, 0, Input.StatusPath[t].BatchSize);
                    }
                    if (Input.StatusPath[t].MatchCandidateProb != null)
                    {
                        Array.Clear(Input.StatusPath[t].MatchCandidateProb.Deriv.MemPtr, 0, Input.StatusPath[t].MatchCandidateProb.Length);
                    }
                    if (Input.StatusPath[t].Score != null)
                    {
                        Input.StatusPath[t].Score.Output.Data.SyncToCPU();
                        Array.Clear(Input.StatusPath[t].Score.Deriv.Data.MemPtr, 0, Input.StatusPath[t].Score.Output.Data.EffectiveSize);
                    }
                }

                // add last step states.
                //if (Input.StatusPath.Count == BuilderParameters.MAX_HOP + 1)
                //{
                //for (int b = 0; b < Input.StatusPath[BuilderParameters.MAX_HOP].BatchSize; b++)
                //{
                //    Input.Results.Add(new Tuple<int, int>(BuilderParameters.MAX_HOP, b));
                //}
                //}
                Input.VisitNum.Clear();
                for (int i = 0; i < Input.Results.Count; i++)
                {
                    int t = Input.Results[i].Item1;
                    int b = Input.Results[i].Item2;
                    int origialB = Input.StatusPath[t].GetOriginalStatsIndex(b);
                    SymbolicState predId = Input.StatusPath[t].NodeID[b];

                    float feedback_v = 0;

                    float estimate_v = Util.Logistic(Input.StatusPath[t].Score.Output.Data.MemPtr[b]); // Math.Max(0, Input.StatusPath[t].Score.Output.Data.MemPtr[b]);
                    //if (estimate_v > 1) estimate_v = 1;
                    
                    float true_v = 0;
                    if (predId.IsSuccess)
                    {

                        true_v = 1;
                    }
                    // use true feed back or pesdo reward.
                    if (Memory[0].IsGlobal) { feedback_v = true_v; }
                    else { feedback_v = estimate_v; }

                    feedback_v = feedback_v * BuilderParameters.UPDATE_R_DISCOUNT;
                    float v = 1 * BuilderParameters.UPDATE_R_DISCOUNT;

                    if (true_v == 0)
                    {
                        NegMean += estimate_v;
                        NegNum += 1;
                    }
                    else
                    {
                        PosMean += estimate_v;
                        PosNum += 1;
                    }

                    StatusData st = Input.StatusPath[t];

                    //if (t != BuilderParameters.MAX_HOP)
                    {
                        //if (t == 0) {
                        //    Console.WriteLine("BUG!!!"); }
                        Memory[0].Update(st.NodeID[b].TerminateKey, feedback_v, v);
                    }

                    for (int pt = t ; pt >= 0; pt--)
                    {
                        st = Input.StatusPath[pt];
                        
                        //string pkey = st.GetStatusKey(pb);

                        //int sidx = st.PreSelActIndex[b] - pst.GetActionStartIndex(pb);

                        //if (pt == 0 && pst.Term != null) {
                        //    Console.WriteLine("Bug !!!"); }
                        // pst.Term == null ? pst.GetActionDim(pb) :
                        Memory[0].Update(st.NodeID[b].StateKey, feedback_v, v);

                        //st = pst;
                        //b = pb;
                        b = st.GetPreStatusIndex(b);
                    }

                    Input.VisitNum.Add(Memory[0].Mem.Count);

                    //for (int pt = t - 1; pt >= 0; pt--)
                    //{
                    //    StatusData pst = Input.StatusPath[pt];
                    //    int pb = st.GetPreStatusIndex(b);
                    //    string pkey = pst.GetStatusKey(pb);

                    //    int sidx = st.PreSelActIndex[b] - pst.GetActionStartIndex(pb);

                    //    //if (pt == 0 && pst.Term != null) {
                    //    //    Console.WriteLine("Bug !!!"); }
                    //    // pst.Term == null ? pst.GetActionDim(pb) :
                    //    Memory.Update(pst.NodeID[pb].StateKey, pkey, sidx, pst.GetActionDim(pb) + 1, feedback_v, v);

                    //    st = pst;
                    //    b = pb;
                    //}
                }

                //
            }
        }

        /// <summary>
        /// REINFORCE-WALK prediction Runner.
        /// </summary>
        //class MAPPredictionRunner : StructRunner
        //{
        //    int Iteration = 0;
        //    int SmpIdx = 0;
        //    GraphQueryData Query { get; set; }

        //    Dictionary<int, float> Success = new Dictionary<int, float>();
        //    Dictionary<int, float> StepSuccess = new Dictionary<int, float>();
        //    int SuccessNum = 0;

        //    StreamWriter resultWriter = null;

        //    public MAPPredictionRunner(GraphQueryData query, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        //    {
        //        Query = query;
        //        Iteration = 0;

                
        //    }

        //    public override void Init()
        //    {
        //        Success.Clear();
        //        StepSuccess.Clear();

        //        SmpIdx = 0;
        //        SuccessNum = 0;
        //        resultWriter = new StreamWriter(BuilderParameters.RESULT_FILE);
        //    }

        //    public void Report()
        //    {
        //        Logger.WriteLog("Sample Idx {0}", SmpIdx);

        //        Logger.WriteLog(string.Format("Success ratio {0}", SuccessNum * 1.0f / SmpIdx));
        //    }

        //    public override void Complete()
        //    {
        //        Logger.WriteLog("Final Report!");
        //        {
        //            Report();
        //        }
        //        //foreach (KeyValuePair<int, float> i in StepSuccess)
        //        //{
        //        //    Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
        //        //}
        //        //int sc = Success.Where(i => i.Value > 0).Count();
        //        //int t = Success.Count;
        //        //Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
        //        Iteration += 1;

        //        resultWriter.Close();
        //    }
        //    public override void Forward()
        //    {
        //        Dictionary<string, float>[] scoreDict = new Dictionary<string, float>[Query.BatchSize];
        //        for (int i = 0; i < Query.BatchSize; i++)
        //        {
        //            scoreDict[i] = new Dictionary<string, float>();
        //        }

        //        // add last step nodes.
        //        if (Query.StatusPath.Count == BuilderParameters.MAX_HOP + 1)
        //        {
        //            for (int b = 0; b < Query.StatusPath[BuilderParameters.MAX_HOP].BatchSize; b++)
        //            {
        //                Query.Results.Add(new Tuple<int, int>(BuilderParameters.MAX_HOP, b));
        //            }
        //        }

        //        for (int t = Query.StatusPath.Count - 1; t >= 0; t--)
        //        {
        //            if (Query.StatusPath[t].Term != null) { Query.StatusPath[t].Term.Output.Data.SyncToCPU(); }
        //        }

        //        Dictionary<int, List<Tuple<string, float, float>>> resultPath = new Dictionary<int, List<Tuple<string, float, float>>>();

        //        for (int i = 0; i < Query.Results.Count; i++)
        //        {
        //            int t = Query.Results[i].Item1;
        //            int b = Query.Results[i].Item2;
        //            int origialB = Query.StatusPath[t].GetOriginalStatsIndex(b);
        //            SymbolicState predId = Query.StatusPath[t].NodeID[b];

        //            //if (t != BuilderParameters.MAX_HOP)
        //            float v = (Query.StatusPath[t].GetLogProb(b) + (float)Query.StatusPath[t].LogPTerm(b, true));

        //            if (!scoreDict[origialB].ContainsKey(predId.StateKey))
        //            {
        //                scoreDict[origialB][predId.StateKey] = v;
        //            }
        //            else
        //            {
        //                scoreDict[origialB][predId.StateKey] = Util.LogAdd(scoreDict[origialB][predId.StateKey], v);
        //            }

        //            float true_v = 0;
        //            if (predId.IsSuccess) true_v = 1;

        //            int rawIdx = Query.RawIndex[origialB];
        //            if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
        //            Success[rawIdx] += true_v;

        //            if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = 0;
        //            StepSuccess[t] += true_v;

        //            if (!resultPath.ContainsKey(origialB))
        //            {
        //                resultPath.Add(origialB, new List<Tuple<string, float, float>>());
        //            }

        //            List<SymbolicState> path = new List<SymbolicState>();
        //            path.Add(predId);
        //            int pb = b;
        //            for (int pt = t - 1; pt >= 0; pt--)
        //            {
        //                pb = Query.StatusPath[pt + 1].GetPreStatusIndex(pb);
        //                SymbolicState mtID = Query.StatusPath[pt].NodeID[pb];
        //                path.Add(mtID);
        //            }

        //            resultPath[origialB].Add(new Tuple<string, float, float>(string.Join("->", path.Select(p => p.StateKey).Reverse()), v, predId.IsSuccess ? 1 : 0));
        //        }

        //        //using (StreamWriter mwriter = new StreamWriter(BuilderParameters.RESULT_FILE))
        //        {
        //            for (int g = 0; g < Query.BatchSize; g++)
        //            {
        //                var rs = resultPath[g].OrderByDescending(pair => pair.Item2);
        //                resultWriter.WriteLine(string.Join("\t", rs.Take(10).Select(gs => string.Format("{0}->{1}->{2}", gs.Item1, gs.Item2, gs.Item3))));
        //            }
        //        }

        //        for (int g = 0; g < Query.BatchSize; g++)
        //        {
        //            Dictionary<string, float> score = scoreDict[g];

        //            var sortD = score.OrderByDescending(pair => pair.Value);

        //            bool isSuccess = false;
        //            foreach (KeyValuePair<string, float> mk in sortD)
        //            {
        //                SymbolicState s = new SymbolicState(mk.Key, 1);
        //                isSuccess = s.IsSuccess;
        //                break;
        //            }
        //            if (isSuccess)
        //                SuccessNum += 1;
        //        }
        //        SmpIdx += Query.BatchSize;
                
        //        Query.Results.Clear();
        //        //Report();
        //    }
        //}

        class MAPPredictionV2Runner : StructRunner
        {
            float SuccessNum = 0;
            int Iteration = 0;
            int SmpIdx = 0;
            GraphQueryData Query { get; set; }

            //Dictionary<int, float> Success = new Dictionary<int, float>();
            //Dictionary<int, float> StepSuccess = new Dictionary<int, float>();
            StreamWriter resultWriter = null;

            public MAPPredictionV2Runner(GraphQueryData query, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Query = query;
                Iteration = 0;

                
            }

            public override void Init()
            {
                //Success.Clear();
                //StepSuccess.Clear();

                SmpIdx = 0;
                SuccessNum = 0;
                resultWriter = new StreamWriter(BuilderParameters.RESULT_FILE);
            }

            public void Report()
            {
                Logger.WriteLog("Sample Idx {0}", SmpIdx);
                Logger.WriteLog(string.Format("Success ratio {0}", SuccessNum * 1.0f / SmpIdx));
            }

            public override void Complete()
            {
                Logger.WriteLog("Final Report!");
                {
                    Report();
                }

                //foreach (KeyValuePair<int, float> i in StepSuccess)
                //{
                //    Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
                //}
                //int sc = Success.Where(i => i.Value > 0).Count();
                //int t = Success.Count;
                //Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);

                Iteration += 1;

                resultWriter.Close();
            }
            public override void Forward()
            {
                Dictionary<string, float>[] scoreDict = new Dictionary<string, float>[Query.BatchSize];
                for (int i = 0; i < Query.BatchSize; i++)
                {
                    scoreDict[i] = new Dictionary<string, float>();
                }

                // add last step nodes.
                //if (Query.StatusPath.Count == BuilderParameters.MAX_HOP + 1)
                //{
                //    for (int b = 0; b < Query.StatusPath[BuilderParameters.MAX_HOP].BatchSize; b++)
                //    {
                //        Query.Results.Add(new Tuple<int, int>(BuilderParameters.MAX_HOP, b));
                //    }
                //}

                for (int t = Query.StatusPath.Count - 1; t >= 0; t--)
                {
                    if (Query.StatusPath[t].Term != null) { Query.StatusPath[t].Term.Output.Data.SyncToCPU(); }
                    if (Query.StatusPath[t].Score != null) { Query.StatusPath[t].Score.Output.Data.SyncToCPU(); }
                }

                Dictionary<int, List<Tuple<string, float, float>>> resultPath = new Dictionary<int, List<Tuple<string, float, float>>>();

                Dictionary<int, Tuple<string, float, float>> selectPath = new Dictionary<int, Tuple<string, float, float>>();

                // add results.
                for (int i = 0; i < Query.Results.Count; i++)
                {
                    int t = Query.Results[i].Item1;
                    int b = Query.Results[i].Item2;
                    int origialB = Query.StatusPath[t].GetOriginalStatsIndex(b);
                    SymbolicState predId = Query.StatusPath[t].NodeID[b];

                    float v = BuilderParameters.PROB_LAMBDA * (Query.StatusPath[t].GetLogProb(b) + (float)Query.StatusPath[t].LogPTerm(b, true));

                    float score = Util.Logistic(Query.StatusPath[t].Score.Output.Data.MemPtr[b]);
                    //if (score >= 1) score = 1;
                    //else if (score <= 0) score = 0;

                    float s = (float)Math.Log(score + BuilderParameters.BASE_LAMBDA);
                    v = v + s * BuilderParameters.SCORE_LAMBDA;

                   


                    //float true_v = 0;
                    //if (predId.IsSuccess) true_v = 1;

                    //int rawIdx = Query.RawIndex[origialB];
                    //if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
                    //Success[rawIdx] += true_v;

                    //if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = 0;
                    //StepSuccess[t] += true_v;


                    //if (!resultPath.ContainsKey(origialB))
                    //{
                    //    resultPath.Add(origialB, new List<Tuple<string, float, float>>());
                    //}
                    //resultPath[origialB].Add(new Tuple<string, float, float>(string.Join("->", path.Select(p => p.StateKey).Reverse()), v, predId.IsSuccess ? 1 : 0));

                    if (!resultPath.ContainsKey(origialB))
                    {
                        resultPath.Add(origialB, new List<Tuple<string, float, float>>());
                        selectPath.Add(origialB, null);
                    }

                    List<SymbolicState> path = new List<SymbolicState>();
                    path.Add(predId);
                    int pb = b;
                    for (int pt = t - 1; pt >= 0; pt--)
                    {
                        pb = Query.StatusPath[pt + 1].GetPreStatusIndex(pb);
                        SymbolicState mtID = Query.StatusPath[pt].NodeID[pb];
                        path.Add(mtID);
                    }

                    Tuple<string, float, float> currentPath = new Tuple<string, float, float>(string.Join("->", path.Select(p => p.StateKey).Reverse()), v, predId.IsSuccess ? 1 : 0);
                    resultPath[origialB].Add(currentPath);

                    if (!scoreDict[origialB].ContainsKey(predId.StateKey) || scoreDict[origialB][predId.StateKey] < v)
                    {
                        scoreDict[origialB][predId.StateKey] = v;
                        //selectPath[origialB] = currentPath;
                    }
                    //else if(scoreDict[origialB][predId.StateKey] < v)
                    //{
                    //    scoreDict[origialB][predId.StateKey] = v;
                        //selectPath[origialB] = currentPath;
                        //scoreDict[origialB][predId.StateKey] = Util.LogAdd(scoreDict[origialB][predId.StateKey], v);
                    //}

                    if(selectPath[origialB] == null || selectPath[origialB].Item2 < v)
                    {
                        selectPath[origialB] = currentPath;
                    }

                }

                {
                    for (int g = 0; g < Query.BatchSize; g++)
                    {
                        int gidx = g;
                        var rs = resultPath[gidx].OrderByDescending(pair => pair.Item2);
                        resultWriter.WriteLine(string.Format("{0}->{1}->{2}", selectPath[gidx].Item1, selectPath[gidx].Item2, selectPath[gidx].Item3));
                        resultWriter.WriteLine(string.Join("\t", rs.Select(gs => string.Format("{0}->{1}->{2}", gs.Item1, gs.Item2, gs.Item3))));
                        resultWriter.WriteLine();
                    }
                }


                for (int g = 0; g < Query.BatchSize; g++)
                {
                    //int relation = Query.RawQuery[g].Item2;
                    //Dictionary<int, float> target = Query.RawTarget[g];
                    //HashSet<int> blackTargets = Query.BlackTargets[g];

                    Dictionary<string, float> score = scoreDict[g];

                    //Dictionary<int, Tuple<float, float>> combine = new Dictionary<int, Tuple<float, float>>();
                    //foreach (int mk in target.Keys)
                    //{
                    //    float s = float.MinValue;
                    //    if (score.ContainsKey(mk)) s = score[mk];
                    //    combine.Add(mk, new Tuple<float, float>(target[mk], s));
                    //}
                    var sortD = score.OrderByDescending(pair => pair.Value);

                    bool isSuccess = false;
                    foreach (KeyValuePair<string, float> mk in sortD)
                    {
                        SymbolicState s = new SymbolicState(mk.Key, 1);
                        isSuccess = s.IsSuccess;
                        break;
                    }
                    if (isSuccess) SuccessNum += 1;
                }

                SmpIdx += Query.BatchSize;
                Query.Results.Clear();
                //Report();
            }
        }

        public class MCTSResult
        {
            public List<int> OriginalBatchIndex = new List<int>();
            public List<SymbolicState> PredIndex = new List<SymbolicState>();
            public List<float> Score = new List<float>();
            public List<int> RawIndex = new List<int>();
            public List<int> TermStep = new List<int>();
            public List<int> BatchIndex = new List<int>();
            public List<int> VisitNum = new List<int>();

            public List<List<SymbolicState>> PredPath = new List<List<SymbolicState>>();
            public void Clear()
            {
                OriginalBatchIndex.Clear();
                PredIndex.Clear();
                Score.Clear();
                RawIndex.Clear();
                TermStep.Clear();
                BatchIndex.Clear();
                PredPath.Clear();
                VisitNum.Clear();
            }

            public MCTSResult(GraphQueryData data)
            {
                for (int i = 0; i < data.Results.Count; i++)
                {
                    int t = data.Results[i].Item1;
                    int b = data.Results[i].Item2;
                    int origialB = data.StatusPath[t].GetOriginalStatsIndex(b);
                    SymbolicState predId = data.StatusPath[t].NodeID[b];

                    float v = BuilderParameters.PROB_LAMBDA * (data.StatusPath[t].GetLogProb(b) + (float)Math.Log(data.StatusPath[t].Term.Output.Data.MemPtr[b]));
                    float estimate_v = Util.Logistic(data.StatusPath[t].Score.Output.Data.MemPtr[b]);
                    float s = (float)Math.Log(estimate_v + BuilderParameters.BASE_LAMBDA);
                    v = v + s * BuilderParameters.SCORE_LAMBDA;

                    string mkey = string.Format("Term {0}", t);
                    int rawIdx = data.RawIndex[origialB];

                    OriginalBatchIndex.Add(origialB);
                    PredIndex.Add(predId);
                    RawIndex.Add(rawIdx);

                    Score.Add(v);
                    TermStep.Add(t);

                    BatchIndex.Add(b);

                    //if(float.IsNaN(v))
                    //{
                    //    Console.WriteLine("degfefe");
                    //}

                    List<SymbolicState> path = new List<SymbolicState>();
                    path.Add(predId);
                    int pb = b;
                    for (int pt = t - 1; pt >= 0; pt--)
                    {
                        pb = data.StatusPath[pt + 1].GetPreStatusIndex(pb);
                        SymbolicState mtID = data.StatusPath[pt].NodeID[pb];
                        path.Add(mtID);
                    }
                    PredPath.Add(path);

                    VisitNum.Add(data.VisitNum[i]);
                }
                data.Results.Clear();
            }
        }

        class MAPPredictionV4Runner : StructRunner
        {
            float SuccessNum = 0;
            int Iteration = 0;
            int SmpIdx = 0;
            int TotalVisitNum = 0;
            int MaxVisitNum = 0;
            List<MCTSResult> Results { get; set; }
            GraphQueryData Query { get; set; }
            EpisodicMemoryV2 Memory { get; set; }

            StreamWriter resultWriter = null;

            public MAPPredictionV4Runner(List<MCTSResult> results, GraphQueryData query, EpisodicMemoryV2 memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Query = query;
                Results = results;
                Memory = memory;

                Iteration = 0;
                
            }

            Dictionary<int, float> Success = new Dictionary<int, float>();
            Dictionary<int, float> StepSuccess = new Dictionary<int, float>();

            public override void Init() 
            {
                Success.Clear();
                StepSuccess.Clear();

                SmpIdx = 0;
                SuccessNum = 0;
                TotalVisitNum = 0;
                MaxVisitNum = 0;
                resultWriter = new StreamWriter(BuilderParameters.RESULT_FILE);
            }
            public void Report() 
            {
                Logger.WriteLog("Sample Idx {0}", SmpIdx);
                Logger.WriteLog(string.Format("Success Ratio {0}", SuccessNum * 1.0f / SmpIdx));
                Logger.WriteLog(string.Format("Avg Access Node Ratio {0}", TotalVisitNum * 1.0f / SmpIdx));
                Logger.WriteLog(string.Format("Max Access Node {0}", MaxVisitNum));

            }

            public override void Complete()
            {
                Logger.WriteLog("Final Report!");
                {
                    Report();
                }
                foreach (KeyValuePair<int, float> i in StepSuccess)
                {
                    Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
                }
                int sc = Success.Where(i => i.Value > 0).Count();
                int t = Success.Count;
                Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
                Iteration += 1;

                resultWriter.Close();
            }
            public override void Forward()
            {
                Dictionary<string, float>[] scoreDict = new Dictionary<string, float>[Query.BatchSize];
                for (int i = 0; i < Query.BatchSize; i++)
                {
                    scoreDict[i] = new Dictionary<string, float>();
                }

                Dictionary<int, List<Tuple<string, float, float>>> resultPath = new Dictionary<int, List<Tuple<string, float, float>>>();
                Dictionary<int, Tuple<string, float, float>> selectPath = new Dictionary<int, Tuple<string, float, float>>();

                Dictionary<int, int> MiniVisitNum = new Dictionary<int, int>();

                // it is not sorted by probability.
                for (int m = 0; m < Results.Count; m++)
                {
                    for (int i = 0; i < Results[m].OriginalBatchIndex.Count; i++)
                    {
                        //int t = Results[m].Results[i].Item1;
                        //int b = Results[m].Results[i].Item2;
                        int origialB = Results[m].OriginalBatchIndex[i]; // StatusPath[t].GetOriginalStatsIndex(b);
                        SymbolicState predId = Results[m].PredIndex[i];

                        //float v = Results[m].StatusPath[t].GetLogProb(b);
                        //if (t != BuilderParameters.MAX_HOP)
                        //{ v = v + (float)Math.Log(Query[m].StatusPath[t].Term.Output.Data.MemPtr[b]); }// LogPTerm(b, true); }
                        //float s = (float)Math.Log(Query[m].StatusPath[t].Score.Output.Data.MemPtr[b] + BuilderParameters.BASE_LAMBDA);
                        //v = v + s * BuilderParameters.MSE_LAMBDA;

                        float v = Results[m].Score[i];

                        if (!scoreDict[origialB].ContainsKey(predId.StateKey) || scoreDict[origialB][predId.StateKey] < v)
                        {
                            scoreDict[origialB][predId.StateKey] = v;
                        }
                        //else
                        //{
                        //    scoreDict[origialB][predId.StateKey] = Util.LogAdd(scoreDict[origialB][predId.StateKey], v);
                        //}
                        
                        float true_v = 0;
                        if (predId.IsSuccess)
                            true_v = 1;

                        int rawIdx = Results[m].RawIndex[i];
                        if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
                        Success[rawIdx] += true_v;

                        int t = Results[m].TermStep[i];

                        if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = 0;
                        StepSuccess[t] += true_v;


                        if (!resultPath.ContainsKey(origialB))
                        {
                            resultPath.Add(origialB, new List<Tuple<string, float, float>>());
                            selectPath.Add(origialB, null);
                        }

                        int b = Results[m].BatchIndex[i];

                        List<SymbolicState> path = Results[m].PredPath[i];

                        Tuple<string, float, float> currentPath = new Tuple<string, float, float>(string.Join("->", path.Select(p => p.StateKey).Reverse()), v, predId.IsSuccess ? 1 : 0);

                        resultPath[origialB].Add(currentPath);

                        if (selectPath[origialB] == null || selectPath[origialB].Item2 < v)
                        {
                            selectPath[origialB] = currentPath;
                        }

                        int vs = Results[m].VisitNum[i];

                        if (predId.IsSuccess)
                        {
                            if (!MiniVisitNum.ContainsKey(origialB))
                            {
                                MiniVisitNum.Add(origialB, vs);
                            }
                            else if (MiniVisitNum[origialB] > vs)
                            {
                                MiniVisitNum[origialB] = vs;
                            }
                        }
                    }
                }

                {
                    for (int g = 0; g < Query.BatchSize; g++)
                    {
                        int gidx = g;

                        resultWriter.WriteLine(string.Format("{0}->{1}->{2}", selectPath[gidx].Item1, selectPath[gidx].Item2, selectPath[gidx].Item3));                        
                        //var rs = resultPath[gidx].OrderByDescending(pair => pair.Item2);
                        resultWriter.WriteLine(string.Join("\t", resultPath[gidx].Select(gs => string.Format("{0}->{1}->{2}", gs.Item1, gs.Item2, gs.Item3))));
                        resultWriter.WriteLine();

                    }
                }

                int nnn = 0;
                for (int g = 0; g < Query.BatchSize; g++)
                {
                    //int relation = Query.RawQuery[g].Item2;
                    //Dictionary<int, float> target = Query.RawTarget[g];
                    //HashSet<int> blackTargets = Query.BlackTargets[g];

                    Dictionary<string, float> score = scoreDict[g];
                    //Dictionary<int, Tuple<float, float>> combine = new Dictionary<int, Tuple<float, float>>();
                    var sortD = score.OrderByDescending(pair => pair.Value);
                    bool isSuccess = false;
                    foreach (KeyValuePair<string, float> mk in sortD)
                    {
                        SymbolicState s = new SymbolicState(mk.Key, 1);
                        isSuccess = s.IsSuccess;
                        break;
                    }
                    if (isSuccess) {
                        SuccessNum += 1;
                        nnn += 1;
                    }
                }
                //if (nnn > 0)
                //{
                //    Console.WriteLine("jihhghyg");
                //}

                TotalVisitNum += MiniVisitNum.Count == 0 ? 0 : MiniVisitNum.Select(i => i.Value).Sum();
                MaxVisitNum = MiniVisitNum.Count == 0 ? MaxVisitNum : Math.Max(MaxVisitNum, MiniVisitNum.Select(i => i.Value).Max());
                SmpIdx += Query.BatchSize;
                Memory.Clear();
            }
        }

        public class NeuralWalkerModel : CompositeNNStructure
        {
            public DNNStructure SrcDNN { get; set; }
            public DNNStructure TgtDNN { get; set; }

            public LayerStructure AttEmbed { get; set; }
            public DNNStructure TermDNN { get; set; }
            public DNNStructure ScoreDNN { get; set; }

            public GRUCell GruCell { get; set; }

            public NeuralWalkerModel(int sDim, DeviceType device)
            {
                int statusDim = SymbolicState.CurrentFeaDim;

                SrcDNN = AddLayer(new DNNStructure(statusDim, BuilderParameters.DNN_DIMS,
                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
                                           device));

                TgtDNN = AddLayer(new DNNStructure(statusDim, BuilderParameters.DNN_DIMS,
                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
                                           device));

                AttEmbed = AddLayer(new LayerStructure(BuilderParameters.DNN_DIMS.Last(), 1, A_Func.Linear, 
                                            N_Type.Fully_Connected, 1, 0, false, device));

                TermDNN = AddLayer(new DNNStructure(statusDim, BuilderParameters.T_NET, BuilderParameters.T_AF,
                                         BuilderParameters.T_NET.Select(i => true).ToArray(), device));

                ScoreDNN = AddLayer(new DNNStructure(statusDim, BuilderParameters.S_NET, BuilderParameters.S_AF,
                                         BuilderParameters.S_NET.Select(i => true).ToArray(), device));

                GruCell = AddLayer(new GRUCell(statusDim, statusDim, device));
            }
            public NeuralWalkerModel(BinaryReader reader, DeviceType device)
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

                SrcDNN = (DNNStructure)DeserializeNextModel(reader, device);
                TgtDNN = (DNNStructure)DeserializeNextModel(reader, device);

                AttEmbed = (LayerStructure)DeserializeNextModel(reader, device);
                TermDNN = (DNNStructure)DeserializeNextModel(reader, device);

                ScoreDNN = (DNNStructure)DeserializeNextModel(reader, device);
                GruCell = (GRUCell)DeserializeNextModel(reader, device);
            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                //InNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //InRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                //CNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //CRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        class MemoryScheduleRunner : StructRunner
        {
            EpisodicMemoryV2 GMemory { get; set; }
            EpisodicMemoryV2 LMemory { get; set; }
            public new List<EpisodicMemoryV2> Output { get; set; }
            public MemoryScheduleRunner(EpisodicMemoryV2 globalMemory, EpisodicMemoryV2 localMemory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                GMemory = globalMemory;
                LMemory = localMemory;
                Output = new List<EpisodicMemoryV2>();
            }
            int Epoch = 0;
            public override void Init()
            {
                if(BuilderParameters.MEM_CLEAN > 0 && Epoch % BuilderParameters.MEM_CLEAN == 0) GMemory.Clear();
            }
            public override void Complete()
            {
                Epoch += 1;
            }
            public override void Forward()
            {
                Output.Clear();
                if (Behavior.RunMode == DNNRunMode.Train && SampleRandom.NextDouble() < BuilderParameters.Epsilon(Epoch)) { Output.Add(GMemory); }
                else { LMemory.Clear(); Output.Add(LMemory); }
                //Epoch += 1;
            }
        }

        public static ComputationGraph BuildComputationGraph(List<SymbolicState> graph, int batchSize, 
                                                             NeuralWalkerModel model, EpisodicMemoryV2 gmemory, EpisodicMemoryV2 tmpMemory,  RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            // sample a list of tuplet from the graph.
            SampleRunner SmpRunner = new SampleRunner(graph, batchSize, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;

            MemoryScheduleRunner memRunner = new MemoryScheduleRunner(gmemory, tmpMemory, Behavior);
            cg.AddRunner(memRunner);
            List<EpisodicMemoryV2> aMem = memRunner.Output;

            // get the embedding from the query data.
            StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, 0, Behavior);
            cg.AddRunner(statusEmbedRunner);

            List<GraphQueryData> Queries = new List<GraphQueryData>();

            // MCTS path number.
            for (int j = 0; j < (Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.MCTS_NUM : 1); j++)
            {
                GraphQueryData newQuery = new GraphQueryData(interface_data);
                StatusData status = new StatusData(newQuery, newQuery.RawQuery, statusEmbedRunner.Output, Behavior.Device);

                #region multi-hop expan
                // travel four steps in the knowledge graph.
                for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
                {
                    // given status, obtain the match. miniBatch * maxNeighbor number.
                    CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
                    cg.AddRunner(candidateActionRunner);

                    // miniMatch * maxNeighborNumber.
                    StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior);
                    cg.AddRunner(candEmbedRunner);

                    DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
                    cg.AddRunner(srcHiddenRunner);

                    DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
                    cg.AddRunner(candHiddenRunner);

                    // alignment miniBatch * miniBatch * neighbor.
                    VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
                                                                                candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
                    cg.AddRunner(attentionRunner);

                    SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
                                                                                status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
                                                                                candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
                                                                                Behavior.Device), 1, Behavior, true);
                    cg.AddRunner(normAttRunner);

                    status.MatchCandidate = candidateActionRunner.Output;
                    status.MatchCandidateProb = normAttRunner.Output;
                    // it will cause un-necessary unstable.

                    DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, status.StateEmbed, Behavior);
                    cg.AddRunner(termRunner);
                    status.Term = termRunner.Output;

                    DNNRunner<HiddenBatchData> scoreRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, status.StateEmbed, Behavior);
                    cg.AddRunner(scoreRunner);
                    status.Score = scoreRunner.Output;


                    BasicPolicyRunner policyRunner = null;

                    if (Behavior.RunMode == DNNRunMode.Train)
                    {
                        policyRunner = new MCTSActionSamplingRunner(status, j, aMem, Behavior, i == BuilderParameters.MAX_HOP - 1);
                    }
                    else
                    {
                        policyRunner = new BeamSearchActionRunner(status, BuilderParameters.BeamSize, Behavior, i == BuilderParameters.MAX_HOP - 1);
                    }
                    cg.AddRunner(policyRunner);

                    if (i < BuilderParameters.MAX_HOP - 1)
                    {
                        #region version 2 of action selection.
                        MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
                        cg.AddRunner(srcExpRunner);

                        MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, policyRunner.MatchPath, 2, Behavior);
                        cg.AddRunner(tgtExpRunner);

                        GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
                        cg.AddRunner(stateRunner);
                        #endregion.

                        StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.LogProb,
                            policyRunner.PreSelActIndex, policyRunner.PreStatusIndex, policyRunner.StatusKey, stateRunner.Output, Behavior.Device);

                        status = nextStatus;
                    }
                }
                #endregion.

                //reward feedback runner.
                if (Behavior.RunMode == DNNRunMode.Train)
                {
                    RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(newQuery, aMem, Behavior);
                    cg.AddRunner(rewardFeedRunner);
                }
                Queries.Add(newQuery);
            }

            if (Behavior.RunMode == DNNRunMode.Train)
            {
                RewardRunner rewardRunner = new RewardRunner(Queries, aMem, Behavior);
                //ClassicalRewardRunner rewardRunner = new ClassicalRewardRunner(Queries, Behavior);
                cg.AddObjective(rewardRunner);
            }
            else
            {
                //if (BuilderParameters.SCORE_TYPE == 0)
                //{
                //    MAPPredictionRunner predRunner = new MAPPredictionRunner(Queries[0], Behavior);
                //    cg.AddRunner(predRunner);
                //}
                //else if(BuilderParameters.SCORE_TYPE == 1)
                {
                    MAPPredictionV2Runner predV2Runner = new MAPPredictionV2Runner(Queries[0], Behavior);
                    cg.AddRunner(predV2Runner);
                }
            }
            cg.SetDelegateModel(model);
            return cg;
        }

        public class MCTSPredRunner : CompositeNetRunner
        {
            int MCTS { get; set; }
            GraphQueryData RawInput { get; set; }
            HiddenBatchData Embed { get; set; }
            EpisodicMemoryV2 Memory { get; set; }
            new NeuralWalkerModel Model { get; set; }
            List<MCTSActionSamplingRunner> PolicyRunner = new List<MCTSActionSamplingRunner>();
            public List<MCTSResult> Results = new List<MCTSResult>();
            public MCTSPredRunner(GraphQueryData interface_data, HiddenBatchData embed, EpisodicMemoryV2 memory, NeuralWalkerModel model, int mctsNum, RunnerBehavior behavior) : base(behavior)
            {
                MCTS = mctsNum; // BuilderParameters.MCTS_NUM;
                List<EpisodicMemoryV2> Mems = new List<EpisodicMemoryV2>() { memory };
                RawInput = interface_data;
                Embed = embed;
                Memory = memory;

                Model = model;

                //GraphQueryData newQuery = new GraphQueryData(interface_data);
                StatusData status = new StatusData(RawInput, RawInput.RawQuery, embed, Behavior.Device);

                #region multi-hop expan
                // travel four steps in the knowledge graph.
                for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
                {
                    // given status, obtain the match. miniBatch * maxNeighbor number.
                    CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
                    LinkRunners.Add(candidateActionRunner);

                    // miniMatch * maxNeighborNumber.
                    StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior);
                    LinkRunners.Add(candEmbedRunner);

                    DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
                    LinkRunners.Add(srcHiddenRunner);

                    DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
                    LinkRunners.Add(candHiddenRunner);

                    // alignment miniBatch * miniBatch * neighbor.
                    VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
                                                                                candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
                    LinkRunners.Add(attentionRunner);

                    SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
                                                                                status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
                                                                                candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
                                                                                Behavior.Device), 1, Behavior, true);
                    LinkRunners.Add(normAttRunner);

                    status.MatchCandidate = candidateActionRunner.Output;
                    status.MatchCandidateProb = normAttRunner.Output;
                    // it will cause un-necessary unstable.

                    //BasicPolicyRunner PolicyRunner = null;
                    DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, status.StateEmbed, Behavior);
                    LinkRunners.Add(termRunner);
                    status.Term = termRunner.Output;

                    DNNRunner<HiddenBatchData> scoreRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, status.StateEmbed, Behavior);
                    LinkRunners.Add(scoreRunner);
                    status.Score = scoreRunner.Output;


                    // select the most promising state.
                    MCTSActionSamplingRunner tmpPolicyRunner = new MCTSActionSamplingRunner(status, -1, Mems, Behavior, i == BuilderParameters.MAX_HOP - 1);
                    LinkRunners.Add(tmpPolicyRunner);

                    PolicyRunner.Add(tmpPolicyRunner);
                    if (i < BuilderParameters.MAX_HOP - 1)
                    {
                        #region version 2 of action selection.
                        MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, tmpPolicyRunner.MatchPath, 1, Behavior);
                        LinkRunners.Add(srcExpRunner);

                        MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, tmpPolicyRunner.MatchPath, 2, Behavior);
                        LinkRunners.Add(tgtExpRunner);

                        GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
                        LinkRunners.Add(stateRunner);
                        #endregion.

                        StatusData nextStatus = new StatusData(status.GraphQuery, tmpPolicyRunner.NodeIndex, tmpPolicyRunner.LogProb,
                            tmpPolicyRunner.PreSelActIndex, tmpPolicyRunner.PreStatusIndex, tmpPolicyRunner.StatusKey, stateRunner.Output, Behavior.Device);

                        status = nextStatus;
                    }
                }
                RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(RawInput, Mems, Behavior);
                LinkRunners.Add(rewardFeedRunner);
                #endregion.   
            }

            // only works for prediction. in the prediction, things are changed.
            public override void Forward()
            {
                Results.Clear();
                for (int i = 0; i < MCTS; i++)
                {
                    foreach(MCTSActionSamplingRunner runner in PolicyRunner)
                        runner.MCTSIdx = i;
                    base.Forward();
                    Results.Add(new MCTSResult(RawInput));
                }
            }
        }

        //public class MCTSDynamicRunner : CompositeNetRunner
        //{

        //    public MCTSDynamicRunner(List<MCTSPredRunner> runnerList, RunnerBehavior behavior) : base(behavior)
        //    {
        //    }
        //}
        //public static ComputationGraph BuildDynamicComputationGraph(List<SymbolicState> graph, int batchSize,
        //                                                     NeuralWalkerModel model, EpisodicMemory memory, RunnerBehavior Behavior)
        //{

        //    ComputationGraph cg = new ComputationGraph();

        //    // sample a list of tuplet from the graph.
        //    SampleRunner SmpRunner = new SampleRunner(graph, batchSize, Behavior);
        //    cg.AddDataRunner(SmpRunner);
        //    GraphQueryData interface_data = SmpRunner.Output;

        //    // get the embedding from the query data.
        //    StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, 0, Behavior);
        //    cg.AddRunner(statusEmbedRunner);

        //    List<GraphQueryData> Queries = new List<GraphQueryData>();

        //    // MCTS path number.
        //    for (int j = 0; j < (Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.MCTS_NUM : 1); j++)
        //    {
        //        GraphQueryData newQuery = new GraphQueryData(interface_data);
        //        StatusData status = new StatusData(newQuery, newQuery.RawQuery, statusEmbedRunner.Output, Behavior.Device);

        //        #region multi-hop expan
        //        // travel four steps in the knowledge graph.
        //        for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
        //        {
        //            // given status, obtain the match. miniBatch * maxNeighbor number.
        //            CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
        //            cg.AddRunner(candidateActionRunner);

        //            // miniMatch * maxNeighborNumber.
        //            StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior);
        //            cg.AddRunner(candEmbedRunner);

        //            DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
        //            cg.AddRunner(srcHiddenRunner);

        //            DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
        //            cg.AddRunner(candHiddenRunner);

        //            // alignment miniBatch * miniBatch * neighbor.
        //            VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
        //                                                                        candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
        //            cg.AddRunner(attentionRunner);

        //            SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
        //                                                                        status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
        //                                                                        candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
        //                                                                        Behavior.Device), 1, Behavior, true);
        //            cg.AddRunner(normAttRunner);

        //            status.MatchCandidate = candidateActionRunner.Output;
        //            status.MatchCandidateProb = normAttRunner.Output;
        //            // it will cause un-necessary unstable.

        //            BasicPolicyRunner policyRunner = null;

        //            if (Behavior.RunMode == DNNRunMode.Train)
        //            {
        //                policyRunner = new MCTSActionSamplingRunner(status, j, memory, Behavior);
        //            }
        //            else
        //            {
        //                policyRunner = new BeamSearchActionRunner(status, BuilderParameters.BeamSize, Behavior);
        //            }
        //            cg.AddRunner(policyRunner);

        //            #region version 2 of action selection.
        //            MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
        //            cg.AddRunner(srcExpRunner);

        //            MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, policyRunner.MatchPath, 2, Behavior);
        //            cg.AddRunner(tgtExpRunner);

        //            GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
        //            cg.AddRunner(stateRunner);
        //            #endregion.

        //            StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.LogProb,
        //                policyRunner.PreSelActIndex, policyRunner.PreStatusIndex, policyRunner.StatusKey, stateRunner.Output, Behavior.Device);

        //            DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, nextStatus.StateEmbed, Behavior);
        //            cg.AddRunner(termRunner);
        //            nextStatus.Term = termRunner.Output;

        //            DNNRunner<HiddenBatchData> scoreRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, nextStatus.StateEmbed, Behavior);
        //            cg.AddRunner(scoreRunner);
        //            nextStatus.Score = scoreRunner.Output;

        //            status = nextStatus;
        //        }
        //        #endregion.

        //        //reward feedback runner.
        //        if (Behavior.RunMode == DNNRunMode.Train)
        //        {
        //            RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(newQuery, memory, Behavior);
        //            cg.AddRunner(rewardFeedRunner);
        //        }
        //        Queries.Add(newQuery);
        //    }

        //    if (Behavior.RunMode == DNNRunMode.Train)
        //    {
        //        RewardRunner rewardRunner = new RewardRunner(Queries, memory, Behavior);
        //        //ClassicalRewardRunner rewardRunner = new ClassicalRewardRunner(Queries, Behavior);
        //        cg.AddObjective(rewardRunner);
        //    }
        //    else
        //    {
        //        if (BuilderParameters.SCORE_TYPE == 0)
        //        {
        //            MAPPredictionRunner predRunner = new MAPPredictionRunner(Queries[0], Behavior);
        //            cg.AddRunner(predRunner);
        //        }
        //        else if (BuilderParameters.SCORE_TYPE == 1)
        //        {
        //            MAPPredictionV2Runner predV2Runner = new MAPPredictionV2Runner(Queries[0], Behavior);
        //            cg.AddRunner(predV2Runner);
        //        }
        //    }
        //    cg.SetDelegateModel(model);
        //    return cg;
        //}

        public static ComputationGraph BuildPredComputationGraph(List<SymbolicState> graph, int batchSize,
                                                             NeuralWalkerModel model, EpisodicMemoryV2 memory, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            // sample a list of tuplet from the graph.
            SampleRunner SmpRunner = new SampleRunner(graph, batchSize, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;

            // get the embedding from the query data.
            StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, 0, Behavior);
            cg.AddRunner(statusEmbedRunner);
            
            MCTSPredRunner predRunner = new MCTSPredRunner(interface_data, statusEmbedRunner.Output, memory, model, BuilderParameters.TEST_MCTS_NUM, Behavior);
            cg.AddRunner(predRunner);

            MAPPredictionV4Runner predV4Runner = new MAPPredictionV4Runner(predRunner.Results, interface_data, memory, Behavior);
            cg.AddRunner(predV4Runner);
            cg.SetDelegateModel(model);
            return cg;
        }


        public override void Rock()
        {

            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            NeuralWalkerModel model =
                BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
                new NeuralWalkerModel(SymbolicState.CurrentFeaDim, device) :
                new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);

            model.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

            EpisodicMemoryV2 Memory = new EpisodicMemoryV2(true);
            EpisodicMemoryV2 predMemory = new EpisodicMemoryV2(false);

            ComputationGraph predCG = null;
            if (BuilderParameters.SCORE_TYPE == 0 || BuilderParameters.SCORE_TYPE == 1)
            {
                predCG = BuildComputationGraph(DataPanel.TestQuery, BuilderParameters.TestMiniBatchSize, model, Memory, predMemory, 
                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
            }
            else if (BuilderParameters.SCORE_TYPE == 2)
            {
                predCG = BuildPredComputationGraph(DataPanel.TestQuery, BuilderParameters.TestMiniBatchSize, model, predMemory,
                                new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph( DataPanel.TrainQuery, BuilderParameters.MiniBatchSize,
                        model, Memory, predMemory, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        //ActionSampler = new Random(10);
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        if ((iter + 1) % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            Logger.WriteLog("Evaluation at Iteration {0}", iter);
                            predCG.Execute();
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "model." + iter.ToString(), FileMode.Create, FileAccess.Write)))
                            {
                                model.Serialize(writer);
                            }
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    //ComputationGraph testCG = null;
                    //EpisodicMemoryV2 testMemory = new EpisodicMemoryV2();
                    //if (BuilderParameters.SCORE_TYPE == 0 || BuilderParameters.SCORE_TYPE == 1)
                    //{
                    //    testCG = BuildComputationGraph(DataPanel.TestQuery, BuilderParameters.TestMiniBatchSize, model, testMemory,
                    //                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
                    //}
                    //else if (BuilderParameters.SCORE_TYPE == 2)
                    //{
                    //    testCG = BuildPredComputationGraph(DataPanel.TestQuery, BuilderParameters.TestMiniBatchSize, model, testMemory,
                    //                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
                    //}
                    predCG.Execute();
                    break;
                
            }
            Logger.CloseLog();
        }


        public class SymbolicState
        {
            public int ContainNum { get; set; }
            public int[] ContainLim { get; set; }
            public int[] ContainStatus { get; set; }
            public int Q;

            public int Step = 0;
            public int BFS_TIMES = 0;
            public int BFS_DEPTH = 0;

            public int DFS_TIMES = 0;
            public int DFS_DEPTH = 0;

            public int MC_TIMES = 0;
            public int MC_DEPTH = 0;

            public string SaveKey { get { return string.Format("{0}#{1}#{2},{3}#{4},{5}", Q, string.Join(",", ContainLim), MC_DEPTH, MC_TIMES, BFS_DEPTH, BFS_TIMES); } }

            public string QueryKey { get { return string.Format("{0}#{1}", Q, string.Join(",", ContainLim)); } }

            public string StateKey { get { return string.Format("{0}#{1}#{2}", Q, string.Join(",", ContainLim), string.Join(",", ContainStatus)); } }

            public string TerminateKey { get { return string.Format("{0}#{1}#{2}#END", Q, string.Join(",", ContainLim), string.Join(",", ContainStatus)); } }
            public static int MaxNeighborNum { get { return 2 * BuilderParameters.CONTAIN_NUM + BuilderParameters.CONTAIN_NUM * (BuilderParameters.CONTAIN_NUM - 1); } }

            public static int Fea5Dim = (BuilderParameters.CONTAIN_NUM + 1) * BuilderParameters.CONTAIN_LIM + BuilderParameters.CONTAIN_NUM;
            public float[] Fea5
            {
                get
                {
                    float[] x = new float[Fea5Dim];
                    x[Q] = 1;
                    for (int i = 0; i < ContainNum; i++)
                    {
                        x[BuilderParameters.CONTAIN_LIM * (i + 1) + ContainStatus[i]] = 1;
                        x[BuilderParameters.CONTAIN_LIM * (1 + ContainNum) + i] = ContainStatus[i] == Q ? 1 : 0;
                    }
                    return x;
                }
            }

            static int CType = BuilderParameters.FeaType;
            public static int CurrentFeaDim
            {
                get
                {
                    switch (CType)
                    {
                        case 5: return Fea5Dim;
                    }
                    throw new NotImplementedException("Feature Type  is not supported");
                }
            }
            public float[] C_QFea
            {
                get
                {
                    switch (CType)
                    {
                        case 5: return Fea5;
                    }
                    throw new NotImplementedException("Feature Type  is not supported");
                }
            }
            public float[] C_SFea
            {
                get
                {
                    switch (CType)
                    {
                        case 5: return Fea5;
                    }
                    throw new NotImplementedException("Feature Type  is not supported");
                }
            }

            public SymbolicState() { }

            /// <summary>
            /// type = 0 : saveKey
            /// type = 1 : stateKey
            /// </summary>
            /// <param name="key"></param>
            /// <param name="type"></param>
            public SymbolicState(string key, int type ) //= 0)
            {
                if (type == 0)
                {
                    string[] items = key.Split('#');
                    Q = int.Parse(items[0]);
                    ContainLim = items[1].Split(',').Select(i => int.Parse(i)).ToArray();
                    ContainNum = ContainLim.Length;
                    ContainStatus = new int[ContainNum];

                    MC_DEPTH = int.Parse(items[2].Split(',')[0]);
                    MC_TIMES = int.Parse(items[2].Split(',')[1]);

                    BFS_DEPTH = int.Parse(items[3].Split(',')[0]);
                    BFS_TIMES = int.Parse(items[3].Split(',')[1]);
                }
                else if(type == 1)
                {
                    //public string StateKey { get { return string.Format("{0}#{1}#{2}", Q, string.Join(",", ContainLim), string.Join(",", ContainStatus)); } }
                    string[] items = key.Split('#');
                    Q = int.Parse(items[0]);
                    ContainLim = items[1].Split(',').Select(i => int.Parse(i)).ToArray();
                    ContainStatus = items[2].Split(',').Select(i => int.Parse(i)).ToArray();
                }
            }

            //public SymbolicState(Random random, HashSet<string> keys)
            //{
            //    while (true)
            //    {
            //        Q = random.Next(1, BuilderParameters.CONTAIN_LIM);
            //        ContainNum = BuilderParameters.CONTAIN_NUM;
            //        ContainStatus = new int[ContainNum];
            //        ContainLim = new int[ContainNum];

            //        for (int i = 0; i < ContainNum; i++)
            //        {
            //            ContainLim[i] = random.Next(1, BuilderParameters.CONTAIN_LIM);
            //        }

            //        if (keys.Contains(this.QueryKey)) continue;

            //        if (ContainLim.Where(i => i <= Q).Count() > 0) continue;

            //        MonteCarloActor actor = new MonteCarloActor();
            //        Tuple<int, List<SymbolicState>> r = actor.Search(this, 1000, BuilderParameters.MAX_SEARCH_LEN);
            //        MC_TIMES = r.Item1;
            //        if (MC_TIMES <= 0) continue;

            //        MC_DEPTH = r.Item2.Count;



            //        // BFS search Step;
            //        int t = 0;
            //        bool isFound = false;
            //        HashSet<string> Checked = new HashSet<string>();

            //        Queue<SymbolicState> queue = new Queue<SymbolicState>();
            //        queue.Enqueue(this);
            //        Checked.Add(this.StateKey);
            //        while (queue.Count > 0)
            //        {
            //            SymbolicState s = queue.Dequeue();
            //            if (s.IsSuccess)
            //            {
            //                this.BFS_DEPTH = s.Step; this.BFS_TIMES = t; isFound = true; break;
            //            }

            //            if (s.Step < BuilderParameters.MAX_SEARCH_LEN)
            //            {
            //                List<SymbolicState> neighbors = s.NeighborStates();
            //                foreach (SymbolicState ns in neighbors)
            //                {
            //                    if (Checked.Contains(ns.StateKey)) continue;
            //                    Checked.Add(ns.StateKey);
            //                    queue.Enqueue(ns);

            //                }
            //            }
            //            t = t + 1;

            //            if (t >= 1000000) { break; }
            //        }

            //        if (!isFound)
            //        {
            //            Console.WriteLine("Wireld things happen...");
            //        }

            //        break;
            //        //isFound = false;

            //        ////DFS search Step;
            //        //t = 0;
            //        //Stack<SymbolicState> stack = new Stack<SymbolicState>();
            //        //stack.Push(this);
            //        //while (stack.Count > 0)
            //        //{
            //        //    SymbolicState s = stack.Pop();
            //        //    if (s.IsSuccess) { this.DFS_DEPTH = s.Step; this.DFS_TIMES = t; isFound = true; break; }

            //        //    if (s.Step < BuilderParameters.MAX_SEARCH_LEN)
            //        //    {
            //        //        List<SymbolicState> neighbors = s.NeighborStates();
            //        //        foreach (SymbolicState ns in neighbors) stack.Push(ns);
            //        //    }
            //        //    t = t + 1;
            //        //    if (t >= 100000) { break; }
            //        //}

            //        //if (!isFound) continue;

            //        //return;
            //    }
            //}

            public SymbolicState Transform(int[] s, int step)
            {
                return new SymbolicState() { ContainLim = this.ContainLim, ContainStatus = s, ContainNum = this.ContainNum, Q = Q, Step = step + 1 };
            }

            public bool IsSuccess { get { if (ContainStatus.Where(i => i == Q).Count() > 0) return true; return false; } }

            public List<SymbolicState> NeighborStates()
            {
                List<SymbolicState> results = new List<SymbolicState>();

                for (int c = 0; c < ContainNum; c++)
                {
                    // A -> Full;
                    if (ContainStatus[c] < ContainLim[c])
                    {
                        int[] news = new int[ContainNum];
                        Array.Copy(ContainStatus, news, ContainNum);
                        news[c] = ContainLim[c];

                        results.Add(Transform(news, Step));
                    }

                    // A -> empty;
                    if (ContainStatus[c] > 0)
                    {
                        int[] news = new int[ContainNum];
                        Array.Copy(ContainStatus, news, ContainNum);
                        news[c] = 0;
                        results.Add(Transform(news, Step));
                    }
                    // A -> B; A -> C;
                    for (int anoC = 0; anoC < ContainNum; anoC++)
                    {
                        if (c == anoC) continue;

                        if (ContainStatus[c] > 0 && ContainStatus[anoC] < ContainLim[anoC])
                        {
                            int[] news = new int[ContainNum];
                            Array.Copy(ContainStatus, news, ContainNum);

                            if (ContainStatus[c] + ContainStatus[anoC] >= ContainLim[anoC])
                            {
                                news[c] = ContainStatus[c] + ContainStatus[anoC] - ContainLim[anoC];
                                news[anoC] = ContainLim[anoC];
                            }
                            else
                            {
                                news[c] = 0;
                                news[anoC] = ContainStatus[c] + ContainStatus[anoC];
                            }
                            results.Add(Transform(news, Step));
                        }

                    }
                }

                return results;
            }
        }

        /// <summary>
        /// load data.
        /// </summary>
        public class DataPanel
        {
            public static List<SymbolicState> TrainQuery = new List<SymbolicState>();
            public static List<SymbolicState> TestQuery = new List<SymbolicState>();
            
            public static List<SymbolicState> LoadQuery(string fileName)
            {
                List<SymbolicState> results = new List<SymbolicState>();
                using (StreamReader mreader = new StreamReader(fileName))
                {
                    while (!mreader.EndOfStream)
                    {
                        SymbolicState s = new SymbolicState(mreader.ReadLine().Trim(), 0);
                        results.Add(s);
                    }
                }
                return results;
            }

            public static void Init()
            {
                TrainQuery = LoadQuery(BuilderParameters.TRAIN_DATA);
                TestQuery = LoadQuery(BuilderParameters.VALID_DATA);
            }
        }
    }
}
