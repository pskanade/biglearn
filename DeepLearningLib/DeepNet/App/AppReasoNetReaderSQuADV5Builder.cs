﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class AppReasoNetReaderSQuADV5Builder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Training Folder"));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Testing Folder"));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Validation Folder"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));

                Argument.Add("IS-EMBED-UPDATE", new ParameterArgument("0", "0 : Fix embedding; 1 : Update Embedding."));
                Argument.Add("EMBED-LAYER-DIM", new ParameterArgument("300,300", "DNN Layer Dim"));
                Argument.Add("EMBED-LAYER-WIN", new ParameterArgument("5,5", "DNN Layer WindowSize"));
                Argument.Add("EMBED-LAYER-AF", new ParameterArgument("1,1", "DNN Layer AF" + ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("EMBED-LAYER-DROPOUT", new ParameterArgument("0,0", "DNN Layer Dropout."));
                Argument.Add("WORD-EMBEDDING", new ParameterArgument(string.Empty, "Word Embedding File"));
                Argument.Add("UNK-INIT", new ParameterArgument("0", "0 : zero initialize; 1 : random initialize"));

                Argument.Add("L3G-EMBED-DIM", new ParameterArgument("0", "L3G Embedding Dim."));
                Argument.Add("L3G-EMBED-WIN", new ParameterArgument("3", "L3G Embedding Win."));

                Argument.Add("CHAR-EMBED-DIM", new ParameterArgument("0", "Char Embedding Dim."));
                Argument.Add("CHAR-EMBED-WIN", new ParameterArgument("5", "Char Embedding Win."));

                Argument.Add("HIGHWAY-LAYER", new ParameterArgument("2", "Highway Layers."));
                Argument.Add("HIGHWAY-SHARE", new ParameterArgument("0", "0: Nonshare highway; 1 : share highway;"));

                Argument.Add("GRU-DIM", new ParameterArgument("300", "GRU Dimension."));
                Argument.Add("CNN-DIM", new ParameterArgument("0", "CNN Dimension."));
                Argument.Add("CNN-WIN", new ParameterArgument("5", "CNN Window Size."));
                Argument.Add("IS-SHARE-RNN", new ParameterArgument("0", "0 : non-share; 1 : share"));
                Argument.Add("MEMORY-ENSEMBLE", new ParameterArgument("2", "2 : two-way ensemble; 3 : three-way ensemble;"));

                //public static float Soft1Dropout { get { return int.Parse(Argument["SOFT1-DROPOUT"].Value); } }
                //public static float Soft2Dropout { get { return int.Parse(Argument["SOFT2-DROPOUT"].Value); } }
                Argument.Add("SOFT1-DROPOUT", new ParameterArgument("0", "Soft 1 Dropout;"));
                Argument.Add("SOFT2-DROPOUT", new ParameterArgument("0", "Soft 2 Dropout;"));
                Argument.Add("LSTM-DROPOUT", new ParameterArgument("0", "LSTM Embed Dropout;"));
                Argument.Add("EMBED-DROPOUT", new ParameterArgument("0", "Embeding Dropout;"));
                Argument.Add("COATT-DROPOUT", new ParameterArgument("0", "CoATT Dropout;"));

                Argument.Add("ATT-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ATT-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));
                Argument.Add("ATT-MEM", new ParameterArgument("0", "0 : LSTM; 1 : CoAtt LSTM; 2 : LSTM + CoAtt LSTM; 3 : CoAttOAtt LSTM; 4 : LSTM + CoAttOAtt"));

                Argument.Add("ATT2-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ATT2-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));
                Argument.Add("ATT2-MEM", new ParameterArgument("0", "0 : LSTM; 1 : CoAtt LSTM; 2 : LSTM + CoAtt LSTM;"));

                Argument.Add("ANS-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ANS-TYPE", new ParameterArgument(((int)CrossSimType.Cosine).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));
                Argument.Add("ANS-MEM", new ParameterArgument("0", "0 : LSTM; 1 : CoAtt LSTM; 2 : LSTM + CoAtt LSTM; 3 : CoAttOAtt LSTM; 4 : LSTM + CoAttOAtt"));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("3", "Recurrent Steps"));
                Argument.Add("RECURRENT-POOL", new ParameterArgument((((int)PoolingType.RL)).ToString(), "Recurrent Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));
                Argument.Add("QUERY-POOL", new ParameterArgument((((int)PoolingType.LAST)).ToString(), "Query Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));

                // Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

                Argument.Add("DEBUG-FILE", new ParameterArgument(string.Empty, "DEBUG File."));
                Argument.Add("R-INIT", new ParameterArgument(((int)RndRecurrentInit.RndNorm).ToString(), "Recurrent Weight Init."));

                Argument.Add("RL-ALL", new ParameterArgument("0", "RL All"));
                Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));
                Argument.Add("PRED-RL", new ParameterArgument((((int)PredType.RL_MAXITER)).ToString(), "RL Pred Type : " + ParameterUtil.EnumValues(typeof(PredType))));
                Argument.Add("IS-NORM-REWARD", new ParameterArgument("0", "0 : No Norm Reward; 1 : Norm Reward."));

                Argument.Add("SPAN-LENGTH", new ParameterArgument("0", "Span length"));
                Argument.Add("DEBUG-MODE", new ParameterArgument("0", "0: exact compute Model; 1: atomicAdd Model; "));

                Argument.Add("ENDPOINT-CONFIG", new ParameterArgument("0", "0: old M2; 1: new M2;"));
            }

            /// <summary>
            /// DNN Run Mode.
            /// </summary>
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string Vocab { get { return Argument["VOCAB"].Value; } }

            #region train, test, validation dataset.
            public static string Train { get { return Argument["TRAIN"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTrainFile { get { return (!Train.Equals(string.Empty)); } }
            public static string TrainContext { get { return string.Format("{0}.para", Train); } }
            public static string TrainQuery { get { return string.Format("{0}.ques", Train); } }
            public static string TrainPos { get { return string.Format("{0}.pos", Train); } }

            public static string TrainContextBin { get { return string.Format("{0}.{1}.bin", TrainContext, MiniBatchSize); } }
            public static string TrainQueryBin { get { return string.Format("{0}.{1}.bin", TrainQuery, MiniBatchSize); } }
            public static string TrainPosBin { get { return string.Format("{0}.{1}.bin", TrainPos, MiniBatchSize); } }

            public static string TestFolder { get { return Argument["TEST"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTestFile { get { return (!TestFolder.Equals(string.Empty)); } }

            //public static string TestContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", TestFolder, VocabSize, MiniBatchSize); } }
            //public static string TestQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", TestFolder, VocabSize, MiniBatchSize); } }
            //public static string TestAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", TestFolder, VocabSize, MiniBatchSize); } }

            public static string Valid { get { return Argument["VALID"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsValidFile { get { return (!Valid.Equals(string.Empty)); } }
            public static string ValidContext { get { return string.Format("{0}.para", Valid); } }
            public static string ValidQuery { get { return string.Format("{0}.ques", Valid); } }
            public static string ValidPos { get { return string.Format("{0}.pos", Valid); } }
            public static string ValidIds { get { return string.Format("{0}.ids", Valid); } }
            public static string ValidSpan { get { return string.Format("{0}.spans", Valid); } }


            public static string ValidContextBin { get { return string.Format("{0}.{1}.bin", ValidContext, MiniBatchSize); } }
            public static string ValidQueryBin { get { return string.Format("{0}.{1}.bin", ValidQuery, MiniBatchSize); } }
            public static string ValidPosBin { get { return string.Format("{0}.{1}.bin", ValidPos, MiniBatchSize); } }

            //public static string ValidIndexData { get { return string.Format("{0}.{1}.idx.tsv", ValidFolder, VocabSize); } }
            //public static string ValidContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            //public static string ValidQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            //public static string ValidAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            #endregion.

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static bool IS_EMBED_UPDATE { get { return int.Parse(Argument["IS-EMBED-UPDATE"].Value) > 0; } }
            public static int[] EMBED_LAYER_DIM { get { return Argument["EMBED-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] EMBED_ACTIVATION { get { return Argument["EMBED-LAYER-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] EMBED_LAYER_WIN { get { return Argument["EMBED-LAYER-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] EMBED_DROPOUT
            {
                get
                {
                    if (Argument["EMBED-LAYER-DROPOUT"].Value.Equals(string.Empty)) return EMBED_LAYER_DIM.Select(i => 0.0f).ToArray();
                    else return Argument["EMBED-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray();
                }
            }
            public static string InitWordEmbedding { get { return Argument["WORD-EMBEDDING"].Value; } }
            public static int UNK_INIT { get { return int.Parse(Argument["UNK-INIT"].Value); } }

            public static int L3G_EMBED_DIM { get { return int.Parse(Argument["L3G-EMBED-DIM"].Value); } }
            public static int L3G_EMBED_WIN { get { return int.Parse(Argument["L3G-EMBED-WIN"].Value); } }

            public static int[] CHAR_EMBED_DIM { get { return Argument["CHAR-EMBED-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] CHAR_EMBED_WIN { get { return Argument["CHAR-EMBED-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int HighWay_Layer { get { return int.Parse(Argument["HIGHWAY-LAYER"].Value); } }
            public static bool HighWay_Share { get { return int.Parse(Argument["HIGHWAY-SHARE"].Value) > 0; } }

            public static int GRU_DIM { get { return int.Parse(Argument["GRU-DIM"].Value); } }
            public static int CNN_DIM { get { return int.Parse(Argument["CNN-DIM"].Value); } }
            public static int CNN_WIN { get { return int.Parse(Argument["CNN-WIN"].Value); } }
            public static bool IS_SHARE_RNN { get { return int.Parse(Argument["IS-SHARE-RNN"].Value) > 0; } }
            public static int MEMORY_ENSEMBLE { get { return int.Parse(Argument["MEMORY-ENSEMBLE"].Value); } }

            public static float Soft1Dropout { get { return float.Parse(Argument["SOFT1-DROPOUT"].Value); } }
            public static float Soft2Dropout { get { return float.Parse(Argument["SOFT2-DROPOUT"].Value); } }
            public static float LSTMDropout { get { return float.Parse(Argument["LSTM-DROPOUT"].Value); } }
            public static float CoAttDropout { get { return float.Parse(Argument["COATT-DROPOUT"].Value); } }
            public static float EMBEDDropout { get { return float.Parse(Argument["EMBED-DROPOUT"].Value); } }

            //public static bool IS_ENTITY_LSTM { get { return int.Parse(Argument["IS-ENTITY-LSTM"].Value) > 0; } }
            public static Recurrent_Unit Recurrent_Gate_Type { get { return (Recurrent_Unit)int.Parse(Argument["RECURRENT-GATE-TYPE"].Value); } }

            public static int ATT_HID_DIM { get { return int.Parse(Argument["ATT-HID"].Value); } }
            public static CrossSimType AttType { get { return (CrossSimType)int.Parse(Argument["ATT-TYPE"].Value); } }
            public static int ATT_MEM { get { return int.Parse(Argument["ATT-MEM"].Value); } }

            public static int ATT2_HID_DIM { get { return int.Parse(Argument["ATT2-HID"].Value); } }
            public static CrossSimType Att2Type { get { return (CrossSimType)int.Parse(Argument["ATT2-TYPE"].Value); } }
            public static int ATT2_MEM { get { return int.Parse(Argument["ATT2-MEM"].Value); } }

            public static int ANS_HID_DIM { get { return int.Parse(Argument["ANS-HID"].Value); } }
            public static CrossSimType AnsType { get { return (CrossSimType)int.Parse(Argument["ANS-TYPE"].Value); } }
            public static int ANS_MEM { get { return int.Parse(Argument["ANS-MEM"].Value); } }

            public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            public static PoolingType RECURRENT_POOL { get { return (PoolingType)int.Parse(Argument["RECURRENT-POOL"].Value); } }
            public static PoolingType QUERY_POOL { get { return (PoolingType)int.Parse(Argument["QUERY-POOL"].Value); } }
            public static bool RL_ALL { get { return int.Parse(Argument["RL-ALL"].Value) > 0; } }
            public static float RL_DISCOUNT { get { return float.Parse(Argument["RL-DISCOUNT"].Value); } }

            public static PredType PRED_RL { get { return (PredType)int.Parse(Argument["PRED-RL"].Value); } }
            public static bool IS_NORM_REWARD { get { return int.Parse(Argument["IS-NORM-REWARD"].Value) > 0; } }
            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

            public static string DebugFile { get { return Argument["DEBUG-FILE"].Value; } }
            public static RndRecurrentInit RndInit { get { return (RndRecurrentInit)int.Parse(Argument["R-INIT"].Value); } }

            public static int SPAN_LENGTH { get { return int.Parse(Argument["SPAN-LENGTH"].Value); } }
            public static int DEBUG_MODE { get { return int.Parse(Argument["DEBUG-MODE"].Value); } }

            public static int ENDPOINT_CONFIG { get { return int.Parse(Argument["ENDPOINT-CONFIG"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.APP_REASONET_SQUAD_READER_V5; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }
        enum PredType { RL_MAXITER, RL_AVGSIM, RL_AVGPROB }

        /// <summary>
        /// Memory Retrieval Runner.
        /// </summary>
        class MemoryRetrievalRunner : StructRunner
        {
            HiddenBatchData Address { get; set; }
            public HiddenBatchData SoftmaxAddress { get; set; }

            SeqDenseBatchData Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }
            float Gamma { get; set; }
            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
            CudaPieceInt Tgt2Src;
            CudaPieceFloat Tgt2SrcSoftmax;

            //CudaPieceFloat tmpMem;
            //CudaPieceFloat tmpMem2;
            public MemoryRetrievalRunner(HiddenBatchData address, SeqDenseBatchData memory, BiMatchBatchData matchData, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Address = address;
                Memory = memory;
                MatchData = matchData;
                Gamma = gamma;
                SoftmaxAddress = new HiddenBatchData(Address.MAX_BATCHSIZE, Address.Dim, Behavior.RunMode, Behavior.Device);
                Output = new HiddenBatchData(MatchData.Stat.MAX_SRC_BATCHSIZE, Memory.Dim, Behavior.RunMode, Behavior.Device);
                Tgt2Src = new CudaPieceInt(MatchData.Stat.MAX_MATCH_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                Tgt2SrcSoftmax = new CudaPieceFloat(MatchData.Stat.MAX_MATCH_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);


                //tmpMem = new CudaPieceFloat(Memory.SentDeriv.Size, true, true);
                //tmpMem2 = new CudaPieceFloat(Memory.SentDeriv.Size, true, true);
            }

            public override void Forward()
            {
                Output.BatchSize = MatchData.SrcSize;

                // softmax 
                // softmax of attention
                ComputeLib.SparseSoftmax(MatchData.Src2MatchIdx, Address.Output.Data, SoftmaxAddress.Output.Data, Gamma, MatchData.SrcSize);

                ComputeLib.ColumnWiseSumMask(Memory.SentOutput, 0, MatchData.TgtIdx, 0,
                    SoftmaxAddress.Output.Data, MatchData.Src2MatchIdx, MatchData.SrcSize,
                    Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                    Memory.SentSize, Memory.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                
                //tmpMem.CopyFrom(Memory.SentDeriv);
                //tmpMem.Zero();
                

                // outputDeriv -> memoryContentDeriv.
                
                //tmpMem2.CopyFrom(Memory.SentDeriv);
                if(BuilderParameters.DEBUG_MODE == 0)
                {
                    SoftmaxAddress.Output.Data.SyncToCPU(MatchData.MatchSize);
                    for (int i = 0; i < MatchData.MatchSize; i++)
                    {
                        int matchIdx = MatchData.Tgt2MatchElement.MemPtr[i];
                        Tgt2Src.MemPtr[i] = MatchData.SrcIdx.MemPtr[matchIdx];
                        Tgt2SrcSoftmax.MemPtr[i] = SoftmaxAddress.Output.Data.MemPtr[matchIdx];
                    }
                    Tgt2Src.SyncFromCPU(MatchData.MatchSize);
                    Tgt2SrcSoftmax.SyncFromCPU(MatchData.MatchSize);
                    ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, Tgt2Src, 0, Tgt2SrcSoftmax, // SoftmaxAddress.Output.Data,
                                                 MatchData.Tgt2MatchIdx, MatchData.TgtSize, Memory.SentDeriv /*Memory.SentDeriv*/, 0, CudaPieceInt.Empty, 0,
                                                 MatchData.MatchSize, Memory.Dim, 1, 1);
                    //ComputeLib.Matrix_Add(Memory.SentDeriv, tmpMem, Memory.SentSize, Memory.Dim, 1);
                }
                else if(BuilderParameters.DEBUG_MODE == 1)
                {
                    //tmpMem2.Zero();
                    ComputeLib.AccurateScale_Matrix(Output.Deriv.Data, 0, MatchData.SrcIdx, 0,
                                                Memory.SentDeriv, 0, MatchData.TgtIdx, 0,
                                                Memory.Dim, MatchData.MatchSize, SoftmaxAddress.Output.Data);
                    //ComputeLib.Matrix_Add(Memory.SentDeriv, tmpMem2, Memory.SentSize, Memory.Dim, 1);
                }

                //tmpMem.SyncToCPU(Memory.SentSize * Memory.Dim);
                //tmpMem2.SyncToCPU(Memory.SentSize * Memory.Dim);
                //for (int i = 0; i < Memory.SentSize * Memory.Dim; i++)
                //{
                //    if (Math.Abs(tmpMem2.MemPtr[i] - tmpMem.MemPtr[i]) >= 0.000000001)
                //    {
                //        Console.WriteLine("Adopt Debug Model {3},  Error! {0}, {1}, {2}", tmpMem.MemPtr[i], tmpMem2.MemPtr[i], i, BuilderParameters.DEBUG_MODE);
                //    }
                //}
                //ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, )

                // outputDeriv -> AddressDeriv.
                ComputeLib.Inner_Product_Matching(Output.Deriv.Data, 0, Memory.SentOutput, 0, SoftmaxAddress.Deriv.Data, 0,
                    MatchData.SrcIdx, MatchData.TgtIdx, Output.BatchSize, Memory.SentSize, MatchData.MatchSize,
                    Memory.Dim, Util.GPUEpsilon);

                ComputeLib.DerivSparseMultiClassSoftmax(MatchData.Src2MatchIdx,
                    SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, Gamma, MatchData.SrcSize);

                ComputeLib.Add_Vector(Address.Deriv.Data, SoftmaxAddress.Deriv.Data, MatchData.MatchSize, 1, 1);
            }
        }

        /// <summary>
        /// GRU Query Runner.
        /// </summary>
        public class GRUQueryRunner : StructRunner<GRUCell, HiddenBatchData>
        {
            public HiddenBatchData Query { get; set; }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            HiddenBatchData GateR; //reset gate
            HiddenBatchData GateZ; //update gate
            HiddenBatchData HHat; //new memory
            HiddenBatchData RestH; //new memory

            CudaPieceFloat Ones;
            CudaPieceFloat Tmp;
            public GRUQueryRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
            {
                Query = query;

                Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

                GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

                Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
                Ones.Init(1);

                Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
            }

            public override void Forward()
            {
                //SubQuery.BatchSize = 1;
                /*Wr X -> GateR*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateR*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Query.BatchSize, Model.HiddenStateDim);

                /*Wz X -> GateZ*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateZ*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Query.BatchSize, Model.HiddenStateDim);

                /*Wh X -> HHat*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Query.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Query.BatchSize, Model.HiddenStateDim);

                Output.BatchSize = Query.BatchSize;

                //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
                // tmp = (1 - GateZ(t))
                ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
                                               GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

                // NewQuery = GateZ(t) * HHat
                ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);

                // NewQuery += tmp * h_t-1
                ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Query.BatchSize, Model.HiddenStateDim, 1);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
                ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Query.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

                ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);

                ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
                ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);

                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.BeforeGradient();
                    Model.WrMatrixOptimizer.BeforeGradient();
                    Model.WzMatrixOptimizer.BeforeGradient();

                    Model.UhMatrixOptimizer.BeforeGradient();
                    Model.UrMatrixOptimizer.BeforeGradient();
                    Model.UzMatrixOptimizer.BeforeGradient();

                    Model.BhMatrixOptimizer.BeforeGradient();
                    Model.BrMatrixOptimizer.BeforeGradient();
                    Model.BzMatrixOptimizer.BeforeGradient();

                }
                ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);

                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.AfterGradient();
                    Model.WrMatrixOptimizer.AfterGradient();
                    Model.WzMatrixOptimizer.AfterGradient();

                    Model.UhMatrixOptimizer.AfterGradient();
                    Model.UrMatrixOptimizer.AfterGradient();
                    Model.UzMatrixOptimizer.AfterGradient();

                    Model.BhMatrixOptimizer.AfterGradient();
                    Model.BrMatrixOptimizer.AfterGradient();
                    Model.BzMatrixOptimizer.AfterGradient();
                }
            }
        }

        /// <summary>
        /// Fix Iteration, SoftAttention, Supervised Training.
        /// </summary>
        class ReasonNetRunner : StructRunner
        {
            /// <summary>
            /// Input Query;
            /// </summary>
            HiddenBatchData InitStatus { get; set; }

            SeqDenseBatchData Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }

            SeqDenseBatchData Memory2 { get; set; }
            BiMatchBatchData MatchData2 { get; set; }

            SeqDenseBatchData AnsMem { get; set; }
            BiMatchBatchData AnsMatchData { get; set; }

            PoolingType ReasonPool = BuilderParameters.RECURRENT_POOL;

            string DebugFile { get; set; }
            //StreamWriter DebugWriter = null;
            //int DebugSample = 0;
            //int DebugIter = 0;

            /// <summary>
            /// maximum iteration number.
            /// </summary>
            int MaxIterationNum { get; set; }
            public ReasonNetRunner(
                HiddenBatchData initStatus, int maxIterateNum,
                SeqDenseBatchData mem, BiMatchBatchData matchData, MLPAttentionStructure attStruct,
                SeqDenseBatchData mem2, BiMatchBatchData matchData2, MLPAttentionStructure att2Struct,
                SeqDenseBatchData ansMem, BiMatchBatchData ansMatchData, MLPAttentionStructure ansStartStruct, MLPAttentionStructure ansEndStruct,
                GRUCell gruCell, LayerStructure termStruct, float gamma, RunnerBehavior behavior, string debugFile = "") : base(Structure.Empty, behavior)
            {
                InitStatus = initStatus;
                Memory = mem;
                MatchData = matchData;
                AttStruct = attStruct;

                Memory2 = mem2;
                MatchData2 = matchData2;
                Att2Struct = att2Struct;

                AnsMem = ansMem;
                AnsMatchData = ansMatchData;
                AnsStartStruct = ansStartStruct;
                AnsEndStruct = ansEndStruct;


                GruCell = gruCell;
                TermStruct = termStruct;

                MaxIterationNum = maxIterateNum;
                DebugFile = debugFile;

                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    AttHidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = Memory.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = Memory.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = attStruct.HiddenDim
                    }, Memory.SampleIdx, Memory.SentMargin, Behavior.Device);
                }
                //else if (BuilderParameters.AttType == CrossSimType.Cosine)
                //{
                //    SoftCosineSimRunner initAttRunner = new SoftCosineSimRunner(InitStatus, Memory, MatchData, Behavior);
                //    AttRunner.Add(initAttRunner);
                //}

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    Att2Hidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = Memory2.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = Memory2.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = att2Struct.HiddenDim
                    }, Memory2.SampleIdx, Memory2.SentMargin, Behavior.Device);
                }
                //else if (BuilderParameters.Att2Type == CrossSimType.Cosine)
                //{
                //    SoftCosineSimRunner initAtt2Runner = new SoftCosineSimRunner(InitStatus, Memory2, MatchData2, Behavior);
                //    Att2Runner.Add(initAtt2Runner);
                //}

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    AnsStartHidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = AnsMem.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = AnsMem.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = AnsStartStruct.HiddenDim
                    }, AnsMem.SampleIdx, AnsMem.SentMargin, Behavior.Device);

                    AnsEndHidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = AnsMem.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = AnsMem.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = AnsEndStruct.HiddenDim
                    }, AnsMem.SampleIdx, AnsMem.SentMargin, Behavior.Device);
                }
                //else if (BuilderParameters.AnsType == CrossSimType.Cosine)
                //{
                //    SoftCosineSimRunner initAnsStartRunner = new SoftCosineSimRunner(InitStatus, AnsMem, AnsMatchData, Behavior);
                //    AnsStartRunner.Add(initAnsStartRunner);

                //    SoftCosineSimRunner initAnsEndRunner = new SoftCosineSimRunner(InitStatus, AnsMem, AnsMatchData, Behavior);
                //    AnsEndRunner.Add(initAnsEndRunner);
                //}


                AnsStartRunner.Add(new SoftLinearSimRunner(InitStatus, AnsStartHidden, AnsMatchData, BuilderParameters.AnsType, AnsStartStruct, Behavior));
                AnsEndRunner.Add(new SoftLinearSimRunner(InitStatus, AnsEndHidden, AnsMatchData, BuilderParameters.AnsType, AnsEndStruct, Behavior));
                TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(termStruct, InitStatus, Behavior));

                HiddenBatchData lastStatus = InitStatus;
                HiddenBatchData lastAtt = null; // (HiddenBatchData)AttRunner[0].Output;
                HiddenBatchData lastAtt2 = null; // (HiddenBatchData)Att2Runner[0].Output;

                for (int i = 0; i < maxIterateNum - 1; i++)
                {
                    if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                    {
                        SoftLinearSimRunner attRunner = new SoftLinearSimRunner(lastStatus, AttHidden, MatchData, BuilderParameters.AttType, AttStruct, Behavior);
                        AttRunner.Add(attRunner);
                        lastAtt = attRunner.Output;
                    }
                    //else if (BuilderParameters.AttType == CrossSimType.Cosine)
                    //{
                    //    SoftCosineSimRunner attRunner = new SoftCosineSimRunner(lastStatus, Memory, MatchData, Behavior);
                    //    AttRunner.Add(attRunner);
                    //    lastAtt = attRunner.Output;
                    //}

                    if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                    {
                        SoftLinearSimRunner att2Runner = new SoftLinearSimRunner(lastStatus, Att2Hidden, MatchData2, BuilderParameters.Att2Type, Att2Struct, Behavior);
                        Att2Runner.Add(att2Runner);
                        lastAtt2 = att2Runner.Output;
                    }
                    //else if (BuilderParameters.Att2Type == CrossSimType.Cosine)
                    //{
                    //    SoftCosineSimRunner att2Runner = new SoftCosineSimRunner(lastStatus, Memory2, MatchData2, Behavior);
                    //    Att2Runner.Add(att2Runner);
                    //    lastAtt2 = att2Runner.Output;
                    //}

                    MemoryRetrievalRunner xRunner = new MemoryRetrievalRunner(lastAtt, Memory, MatchData, gamma, Behavior);
                    MemRetrievalRunner.Add(xRunner);

                    MemoryRetrievalRunner x2Runner = new MemoryRetrievalRunner(lastAtt2, Memory2, MatchData2, gamma, Behavior);
                    MemRetrieval2Runner.Add(x2Runner);

                    EnsembleMatrixRunner eRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { xRunner.Output, x2Runner.Output }, Behavior);
                    RetrievalEnsembleRunner.Add(eRunner);

                    GRUQueryRunner yRunner = new GRUQueryRunner(gruCell, lastStatus, eRunner.Output, Behavior);
                    StatusRunner.Add(yRunner);
                    lastStatus = yRunner.Output;

                    if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                    {
                        SoftLinearSimRunner ansStartRunner = new SoftLinearSimRunner(lastStatus, AnsStartHidden, AnsMatchData, BuilderParameters.AnsType, AnsStartStruct, Behavior);
                        AnsStartRunner.Add(ansStartRunner);

                        SoftLinearSimRunner ansEndRunner = new SoftLinearSimRunner(lastStatus, AnsEndHidden, AnsMatchData, BuilderParameters.AnsType, AnsEndStruct, Behavior);
                        AnsEndRunner.Add(ansEndRunner);
                    }
                    //else if (BuilderParameters.AnsType == CrossSimType.Cosine)
                    //{
                    //    SoftCosineSimRunner ansStartRunner = new SoftCosineSimRunner(lastStatus, AnsMem, AnsMatchData, Behavior);
                    //    AnsStartRunner.Add(ansStartRunner);

                    //    SoftCosineSimRunner ansEndRunner = new SoftCosineSimRunner(lastStatus, AnsMem, AnsMatchData, Behavior);
                    //    AnsEndRunner.Add(ansEndRunner);
                    //}

                    TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(termStruct, lastStatus, Behavior));
                }

                {
                    AnswerProb = new HiddenBatchData[MaxIterationNum];
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i] = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
                    }
                    FinalStartAns = new HiddenBatchData(AnsMatchData.Stat.MAX_MATCH_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
                    FinalEndAns = new HiddenBatchData(AnsMatchData.Stat.MAX_MATCH_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
                }
            }

            GRUCell GruCell = null;
            MLPAttentionStructure AttStruct = null;
            MLPAttentionStructure Att2Struct = null;
            MLPAttentionStructure AnsStartStruct = null;
            MLPAttentionStructure AnsEndStruct = null;

            LayerStructure TermStruct = null;

            SeqDenseBatchData Att2Hidden = null;
            List<StructRunner> Att2Runner = new List<StructRunner>();

            SeqDenseBatchData AttHidden = null;
            List<StructRunner> AttRunner = new List<StructRunner>();

            SeqDenseBatchData AnsStartHidden = null;
            SeqDenseBatchData AnsEndHidden = null;

            List<StructRunner> AnsStartRunner = new List<StructRunner>();
            List<StructRunner> AnsEndRunner = new List<StructRunner>();

            List<FullyConnectHiddenRunner<HiddenBatchData>> TerminalRunner = new List<FullyConnectHiddenRunner<HiddenBatchData>>();

            List<MemoryRetrievalRunner> MemRetrievalRunner = new List<MemoryRetrievalRunner>();
            List<MemoryRetrievalRunner> MemRetrieval2Runner = new List<MemoryRetrievalRunner>();
            List<EnsembleMatrixRunner> RetrievalEnsembleRunner = new List<EnsembleMatrixRunner>();

            List<GRUQueryRunner> StatusRunner = new List<GRUQueryRunner>();

            public HiddenBatchData[] AnsStartData { get { return AnsStartRunner.Select(i => (HiddenBatchData)i.Output).ToArray(); } }
            public HiddenBatchData[] AnsEndData { get { return AnsEndRunner.Select(i => (HiddenBatchData)i.Output).ToArray(); } }

            public HiddenBatchData[] AnswerProb = null;

            public HiddenBatchData TerminalProb = null;
            public HiddenBatchData FinalStartAns = null;
            public HiddenBatchData FinalEndAns = null;

            /// <summary>
            /// Output Debug File.
            /// </summary>
            public override void Init()
            {
                if (DebugFile != "" && !DebugFile.Equals(string.Empty))
                {
                    //DebugWriter = new StreamWriter(DebugFile + DateTime.Now.ToFileTime().ToString() + "." + DebugIter.ToString() + ".iter");
                    //DebugSample = 0;
                    //DebugIter += 1;
                }
            }

            /// <summary>
            /// Save Debug File.
            /// </summary>
            public override void Complete()
            {
                //if (DebugWriter != null)
                //{
                //    DebugWriter.Close();
                //}
            }

            public override void Forward()
            {
                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    AttHidden.BatchSize = Memory.BatchSize;
                    AttHidden.SentSize = Memory.SentSize;
                    ComputeLib.Sgemm(Memory.SentOutput, 0, AttStruct.Wm, 0, AttHidden.SentOutput, 0,
                                     Memory.SentSize, Memory.Dim, AttStruct.HiddenDim, 0, 1, false, false);
                    if (AttStruct.IsBias) ComputeLib.Matrix_Add_Linear(AttHidden.SentOutput, AttStruct.B, Memory.SentSize, AttStruct.HiddenDim);
                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    Att2Hidden.BatchSize = Memory2.BatchSize;
                    Att2Hidden.SentSize = Memory2.SentSize;
                    ComputeLib.Sgemm(Memory2.SentOutput, 0, Att2Struct.Wm, 0, Att2Hidden.SentOutput, 0,
                                     Memory2.SentSize, Memory2.Dim, Att2Struct.HiddenDim, 0, 1, false, false);
                    if (Att2Struct.IsBias) ComputeLib.Matrix_Add_Linear(Att2Hidden.SentOutput, Att2Struct.B, Memory2.SentSize, Att2Struct.HiddenDim);
                }

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    AnsStartHidden.BatchSize = AnsMem.BatchSize;
                    AnsStartHidden.SentSize = AnsMem.SentSize;
                    ComputeLib.Sgemm(AnsMem.SentOutput, 0, AnsStartStruct.Wm, 0, AnsStartHidden.SentOutput, 0,
                                     AnsMem.SentSize, AnsMem.Dim, AnsStartStruct.HiddenDim, 0, 1, false, false);
                    if (AnsStartStruct.IsBias) ComputeLib.Matrix_Add_Linear(AnsStartHidden.SentOutput, AnsStartStruct.B, AnsMem.SentSize, AnsStartStruct.HiddenDim);

                    AnsEndHidden.BatchSize = AnsMem.BatchSize;
                    AnsEndHidden.SentSize = AnsMem.SentSize;
                    ComputeLib.Sgemm(AnsMem.SentOutput, 0, AnsEndStruct.Wm, 0, AnsEndHidden.SentOutput, 0,
                                     AnsMem.SentSize, AnsMem.Dim, AnsEndStruct.HiddenDim, 0, 1, false, false);
                    if (AnsEndStruct.IsBias) ComputeLib.Matrix_Add_Linear(AnsEndHidden.SentOutput, AnsEndStruct.B, AnsMem.SentSize, AnsEndStruct.HiddenDim);
                }
                AnsStartRunner[0].Forward();
                AnsEndRunner[0].Forward();
                TerminalRunner[0].Forward();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    // take attention
                    AttRunner[i].Forward();
                    Att2Runner[i].Forward();

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Forward();
                    MemRetrieval2Runner[i].Forward();
                    RetrievalEnsembleRunner[i].Forward();

                    // update Status.
                    StatusRunner[i].Forward();

                    AnsStartRunner[i + 1].Forward();
                    AnsEndRunner[i + 1].Forward();

                    // take terminal gate or not.
                    TerminalRunner[i + 1].Forward();
                }

                #region RL Pooling.
                //else if (ReasonPool == PoolingType.RL)
                {
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        ComputeLib.Logistic(TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.BatchSize, 1);
                        ComputeLib.ClipVector(TerminalRunner[i].Output.Output.Data, TerminalRunner[i].Output.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                        TerminalRunner[i].Output.Output.Data.SyncToCPU(TerminalRunner[i].Output.BatchSize);

                        AnsStartData[i].Output.Data.SyncToCPU(AnsStartData[i].BatchSize);
                        AnsEndData[i].Output.Data.SyncToCPU(AnsEndData[i].BatchSize);
                    }

                    FinalStartAns.BatchSize = AnsStartData.Last().BatchSize;
                    Array.Clear(FinalStartAns.Output.Data.MemPtr, 0, FinalStartAns.BatchSize);

                    FinalEndAns.BatchSize = AnsEndData.Last().BatchSize;
                    Array.Clear(FinalEndAns.Output.Data.MemPtr, 0, FinalEndAns.BatchSize);

                    for (int b = 0; b < MatchData.SrcSize; b++)
                    {
                        int max_iter = 0;
                        double max_p = 0;

                        float acc_log_t = 0;
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float t_i = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                            float p = acc_log_t + (float)Math.Log(t_i);
                            if (i == MaxIterationNum - 1) p = acc_log_t;

                            AnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
                            acc_log_t += (float)Math.Log(1 - t_i);

                            if (Math.Exp(p) > max_p)
                            {
                                max_p = Math.Exp(p);
                                max_iter = i;
                            }

                            if (BuilderParameters.PRED_RL == PredType.RL_AVGSIM)
                            {
                                int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                                int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];
                                for (int m = matchBgn; m < matchEnd; m++)
                                {
                                    FinalStartAns.Output.Data.MemPtr[m] += (float)Math.Exp(p) * AnsStartData[i].Output.Data.MemPtr[m];
                                    FinalEndAns.Output.Data.MemPtr[m] += (float)Math.Exp(p) * AnsEndData[i].Output.Data.MemPtr[m];
                                }
                            }
                            if (BuilderParameters.PRED_RL == PredType.RL_AVGPROB)
                            {
                                int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                                int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];
                                double expSum_start = 0;
                                double expSum_end = 0;
                                for (int m = matchBgn; m < matchEnd; m++)
                                {
                                    expSum_start += Math.Exp(BuilderParameters.Gamma * AnsStartData[i].Output.Data.MemPtr[m]);
                                    expSum_end += Math.Exp(BuilderParameters.Gamma * AnsEndData[i].Output.Data.MemPtr[m]);
                                }

                                for (int m = matchBgn; m < matchEnd; m++)
                                {
                                    FinalStartAns.Output.Data.MemPtr[m] += (float)(Math.Exp(p) * Math.Exp(BuilderParameters.Gamma * AnsStartData[i].Output.Data.MemPtr[m]) / expSum_start);
                                    FinalEndAns.Output.Data.MemPtr[m] += (float)(Math.Exp(p) * Math.Exp(BuilderParameters.Gamma * AnsEndData[i].Output.Data.MemPtr[m]) / expSum_end);
                                }
                            }
                        }
                        if (BuilderParameters.PRED_RL == PredType.RL_MAXITER)
                        {
                            int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];

                            double expSum_start = 0;
                            double expSum_end = 0;
                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                expSum_start += Math.Exp(BuilderParameters.Gamma * AnsStartData[max_iter].Output.Data.MemPtr[m]);
                                expSum_end += Math.Exp(BuilderParameters.Gamma * AnsEndData[max_iter].Output.Data.MemPtr[m]);
                            }

                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                FinalStartAns.Output.Data.MemPtr[m] = (float)(Math.Exp(BuilderParameters.Gamma * AnsStartData[max_iter].Output.Data.MemPtr[m]) / expSum_start);  //AnsStartData[max_iter].Output.Data.MemPtr[m];
                                FinalEndAns.Output.Data.MemPtr[m] = (float)(Math.Exp(BuilderParameters.Gamma * AnsEndData[max_iter].Output.Data.MemPtr[m]) / expSum_end); // AnsEndData[max_iter].Output.Data.MemPtr[m];
                            }
                        }
                    }
                    FinalStartAns.Output.Data.SyncFromCPU(FinalStartAns.BatchSize);
                    FinalEndAns.Output.Data.SyncFromCPU(FinalEndAns.BatchSize);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i].BatchSize = MatchData.SrcSize;
                        AnswerProb[i].Output.Data.SyncFromCPU(AnswerProb[i].BatchSize);
                    }

                    // write the reasonet status.
                    //if (DebugWriter != null)
                    //{
                    //    for (int i = 0; i < MaxIterationNum - 1; i++)
                    //    {
                    //        MemRetrievalRunner[i].SoftmaxAddress.Output.Data.SyncToCPU();
                    //        MemRetrieval2Runner[i].SoftmaxAddress.Output.Data.SyncToCPU();
                    //    }

                    //    for (int b = 0; b < MatchData.SrcSize; b++)
                    //    {
                    //        int att1Bgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                    //        int att1End = MatchData.Src2MatchIdx.MemPtr[b];

                    //        int att2Bgn = b == 0 ? 0 : MatchData2.Src2MatchIdx.MemPtr[b - 1];
                    //        int att2End = MatchData2.Src2MatchIdx.MemPtr[b];

                    //        int ansBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                    //        int ansEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];

                    //        DebugWriter.WriteLine("Sample {0}.............", DebugSample + b);

                    //        for (int i = 0; i < MaxIterationNum; i++)
                    //        {
                    //            string att1Str = "";
                    //            string att2Str = "";
                    //            string ansStr = "";

                    //            if (i > 0)
                    //            {
                    //                for (int m = att1Bgn; m < att1End; m++)
                    //                {
                    //                    att1Str = att1Str + MemRetrievalRunner[i - 1].SoftmaxAddress.Output.Data.MemPtr[m].ToString() + " ";
                    //                }

                    //                for (int m = att2Bgn; m < att2End; m++)
                    //                {
                    //                    att2Str = att2Str + MemRetrieval2Runner[i - 1].SoftmaxAddress.Output.Data.MemPtr[m].ToString() + " ";
                    //                }
                    //            }

                    //            float aSum = 0;
                    //            for (int m = ansBgn; m < ansEnd; m++)
                    //            {
                    //                aSum += (float)Math.Exp(BuilderParameters.Gamma * AnsData[i].Output.Data.MemPtr[m]);
                    //            }

                    //            for (int m = ansBgn; m < ansEnd; m++)
                    //            {
                    //                ansStr = ansStr + (Math.Exp(BuilderParameters.Gamma * AnsData[i].Output.Data.MemPtr[m]) / aSum).ToString() + " ";
                    //            }

                    //            float termProb = TerminalRunner[i].Output.Output.Data.MemPtr[b];

                    //            DebugWriter.WriteLine("T {0}\t{1}\t{2}\t{3}\t{4}", i, ansStr, termProb, att1Str, att2Str);
                    //        }

                    //        string finalAnsStr = "";

                    //        for (int m = ansBgn; m < ansEnd; m++)
                    //        {
                    //            finalAnsStr = finalAnsStr + FinalAns.Output.Data.MemPtr[m].ToString() + " ";
                    //        }
                    //        DebugWriter.WriteLine("Sample {0}\t{1}", DebugSample + b, finalAnsStr);
                    //        DebugWriter.WriteLine();
                    //    }
                    //    DebugSample += MatchData.SrcSize;
                    //}
                }
                #endregion.
            }
            public override void CleanDeriv()
            {
                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    TerminalRunner[i + 1].CleanDeriv();
                    // take Answer.
                    AnsEndRunner[i + 1].CleanDeriv();
                    AnsStartRunner[i + 1].CleanDeriv();

                    // update Status.
                    StatusRunner[i].CleanDeriv();

                    RetrievalEnsembleRunner[i].CleanDeriv();

                    MemRetrieval2Runner[i].CleanDeriv();
                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].CleanDeriv();

                    Att2Runner[i].CleanDeriv();
                    // take Attention
                    AttRunner[i].CleanDeriv();
                }
                TerminalRunner[0].CleanDeriv();
                AnsEndRunner[0].CleanDeriv();
                AnsStartRunner[0].CleanDeriv();

                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(AttHidden.SentDeriv, AttHidden.SentSize * AttHidden.Dim);
                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(Att2Hidden.SentDeriv, Att2Hidden.SentSize * Att2Hidden.Dim);
                }

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(AnsStartHidden.SentDeriv, AnsStartHidden.SentSize * AnsStartHidden.Dim);
                    ComputeLib.Zero(AnsEndHidden.SentDeriv, AnsEndHidden.SentSize * AnsEndHidden.Dim);
                }

            }

            /// <summary>
            /// Stage 1 : Iterative Attention Model (Supervised Training).
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                #region RL Pool.
                //else if (ReasonPool == PoolingType.RL)
                {
                    //int BatchSize = AnswerSeqData.Last().BatchSize;

                    /// according to loss, assign reward to each terminal node.
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(TerminalRunner[i].Output.Deriv.Data.MemPtr, 0, AnsMatchData.SrcSize);
                    }

                    for (int b = 0; b < AnsMatchData.SrcSize; b++)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float reward = AnswerProb[i].Deriv.Data.MemPtr[b];

                            if (BuilderParameters.IS_NORM_REWARD) reward = reward / (i + 1);

                            TerminalRunner[i].Output.Deriv.Data.MemPtr[b] += reward * (1 - TerminalRunner[i].Output.Output.Data.MemPtr[b]);
                            for (int hp = 0; hp < i; hp++)
                            {
                                TerminalRunner[hp].Output.Deriv.Data.MemPtr[b] += -(float)Math.Pow(BuilderParameters.RL_DISCOUNT, i - hp) * reward * TerminalRunner[hp].Output.Output.Data.MemPtr[b];
                            }

                            if (TerminalRunner[i].Output.Deriv.Data.MemPtr[b] > Math.Abs(1))
                            {
                                Logger.WriteLog("TerminalRunner Deriv is too large {0}, step {1}", TerminalRunner[i].Output.Deriv.Data.MemPtr[b], i);
                            }
                        }
                    }

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        TerminalRunner[i].Output.Deriv.Data.SyncFromCPU(TerminalRunner[i].Output.BatchSize);
                    }
                }
                #endregion.

                for (int i = MaxIterationNum - 2; i >= 0; i--)
                {
                    TerminalRunner[i + 1].Backward(cleanDeriv);
                    AnsEndRunner[i + 1].Backward(cleanDeriv);
                    AnsStartRunner[i + 1].Backward(cleanDeriv);

                    StatusRunner[i].Backward(cleanDeriv);

                    RetrievalEnsembleRunner[i].Backward(cleanDeriv);

                    MemRetrieval2Runner[i].Backward(cleanDeriv);

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Backward(cleanDeriv);

                    Att2Runner[i].Backward(cleanDeriv);
                    // take action. new Attention
                    AttRunner[i].Backward(cleanDeriv);
                    // update Status.
                }
                TerminalRunner[0].Backward(cleanDeriv);
                AnsEndRunner[0].Backward(cleanDeriv);
                AnsStartRunner[0].Backward(cleanDeriv);

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    ComputeLib.Sgemm(AnsStartHidden.SentDeriv, 0,
                                 AnsStartStruct.Wm, 0,
                                 AnsMem.SentDeriv, 0,
                                 AnsMem.SentSize, AnsStartStruct.HiddenDim, AnsStartStruct.MemoryDim, 1, 1, false, true);

                    ComputeLib.Sgemm(AnsEndHidden.SentDeriv, 0,
                                 AnsEndStruct.Wm, 0,
                                 AnsMem.SentDeriv, 0,
                                 AnsMem.SentSize, AnsEndStruct.HiddenDim, AnsEndStruct.MemoryDim, 1, 1, false, true);

                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    ComputeLib.Sgemm(Att2Hidden.SentDeriv, 0,
                                 Att2Struct.Wm, 0,
                                 Memory2.SentDeriv, 0,
                                 Memory2.SentSize, Att2Struct.HiddenDim, Att2Struct.MemoryDim, 1, 1, false, true);
                }

                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    ComputeLib.Sgemm(AttHidden.SentDeriv, 0,
                                 AttStruct.Wm, 0,
                                 Memory.SentDeriv, 0,
                                 Memory.SentSize, AttStruct.HiddenDim, AttStruct.MemoryDim, 1, 1, false, true);
                }
            }

            public override void Update()
            {
                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AttStruct.WmMatrixOptimizer.BeforeGradient();
                        if (AttStruct.IsBias) AttStruct.BMatrixOptimizer.BeforeGradient();
                    }

                    if (AttStruct.IsBias)
                        ComputeLib.ColumnWiseSum(AttHidden.SentDeriv, AttStruct.BMatrixOptimizer.Gradient, Memory.SentSize, AttStruct.HiddenDim, AttStruct.BMatrixOptimizer.GradientStep);

                    ComputeLib.Sgemm(Memory.SentOutput, 0,
                                     AttHidden.SentDeriv, 0,
                                     AttStruct.WmMatrixOptimizer.Gradient, 0,
                                     Memory.SentSize, AttStruct.MemoryDim, AttStruct.HiddenDim,
                                     1, AttStruct.WmMatrixOptimizer.GradientStep, true, false);

                    if (!IsDelegateOptimizer)
                    {
                        AttStruct.WmMatrixOptimizer.AfterGradient();
                        if (AttStruct.IsBias) AttStruct.BMatrixOptimizer.AfterGradient();
                    }
                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        Att2Struct.WmMatrixOptimizer.BeforeGradient();
                        if (Att2Struct.IsBias) Att2Struct.BMatrixOptimizer.BeforeGradient();
                    }

                    if (Att2Struct.IsBias)
                        ComputeLib.ColumnWiseSum(Att2Hidden.SentDeriv, Att2Struct.BMatrixOptimizer.Gradient, Memory2.SentSize, Att2Struct.HiddenDim, Att2Struct.BMatrixOptimizer.GradientStep);

                    ComputeLib.Sgemm(Memory2.SentOutput, 0,
                                     Att2Hidden.SentDeriv, 0,
                                     Att2Struct.WmMatrixOptimizer.Gradient, 0,
                                     Memory2.SentSize, Att2Struct.MemoryDim, Att2Struct.HiddenDim,
                                     1, Att2Struct.WmMatrixOptimizer.GradientStep, true, false);

                    if (!IsDelegateOptimizer)
                    {
                        Att2Struct.WmMatrixOptimizer.AfterGradient();
                        if (Att2Struct.IsBias) Att2Struct.BMatrixOptimizer.AfterGradient();
                    }
                }


                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AnsStartStruct.WmMatrixOptimizer.BeforeGradient();
                        if (AnsStartStruct.IsBias) AnsStartStruct.BMatrixOptimizer.BeforeGradient();

                        AnsEndStruct.WmMatrixOptimizer.BeforeGradient();
                        if (AnsEndStruct.IsBias) AnsEndStruct.BMatrixOptimizer.BeforeGradient();
                    }

                    if (AnsStartStruct.IsBias)
                        ComputeLib.ColumnWiseSum(AnsStartHidden.SentDeriv, AnsStartStruct.BMatrixOptimizer.Gradient, AnsMem.SentSize, AnsStartStruct.HiddenDim, AnsStartStruct.BMatrixOptimizer.GradientStep);

                    if (AnsEndStruct.IsBias)
                        ComputeLib.ColumnWiseSum(AnsEndHidden.SentDeriv, AnsEndStruct.BMatrixOptimizer.Gradient, AnsMem.SentSize, AnsEndStruct.HiddenDim, AnsEndStruct.BMatrixOptimizer.GradientStep);

                    ComputeLib.Sgemm(AnsMem.SentOutput, 0,
                                     AnsStartHidden.SentDeriv, 0,
                                     AnsStartStruct.WmMatrixOptimizer.Gradient, 0,
                                     AnsMem.SentSize, AnsStartStruct.MemoryDim, AnsStartStruct.HiddenDim,
                                     1, AnsStartStruct.WmMatrixOptimizer.GradientStep, true, false);

                    ComputeLib.Sgemm(AnsMem.SentOutput, 0,
                                     AnsEndHidden.SentDeriv, 0,
                                     AnsEndStruct.WmMatrixOptimizer.Gradient, 0,
                                     AnsMem.SentSize, AnsEndStruct.MemoryDim, AnsEndStruct.HiddenDim,
                                     1, AnsEndStruct.WmMatrixOptimizer.GradientStep, true, false);

                    if (!IsDelegateOptimizer)
                    {
                        AnsStartStruct.WmMatrixOptimizer.AfterGradient();
                        if (AnsStartStruct.IsBias) AnsStartStruct.BMatrixOptimizer.AfterGradient();

                        AnsEndStruct.WmMatrixOptimizer.AfterGradient();
                        if (AnsEndStruct.IsBias) AnsEndStruct.BMatrixOptimizer.AfterGradient();
                    }

                }

                //AttRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                //AttRunner[0].Update();

                //Att2Runner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                //Att2Runner[0].Update();

                AnsStartRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                AnsEndRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                TerminalRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;

                AnsStartRunner[0].Update();
                AnsEndRunner[0].Update();
                TerminalRunner[0].Update();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    AttRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    Att2Runner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    MemRetrievalRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    MemRetrieval2Runner[i].IsDelegateOptimizer = IsDelegateOptimizer;

                    StatusRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;

                    AnsEndRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    AnsStartRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    TerminalRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;

                    // take action. new Attention
                    AttRunner[i].Update();
                    Att2Runner[i].Update();

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Update();
                    MemRetrieval2Runner[i].Update();

                    // update Status.
                    StatusRunner[i].Update();

                    AnsStartRunner[i + 1].Update();
                    AnsEndRunner[i + 1].Update();
                    TerminalRunner[i + 1].Update();
                }

                //if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                //{
                //}

                //if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                //{
                //}

                //if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                //{
                //}
            }
        }

        class ExactMatchPredictionRunner : ObjectiveRunner
        {
            HiddenBatchData StartPosData;
            HiddenBatchData EndPosData;
            List<HashSet<string>> PosTrueData;
            BiMatchBatchData AnsMatch;

            
            int SampleNum = 0;
            int HitNum = 0;
            int Iteration = 0;

            //List<Dictionary<string, float>> Result = null;
            //List<List<Dictionary<string, float>>> HisResults = new List<List<Dictionary<string, float>>>();

            public ExactMatchPredictionRunner(
                HiddenBatchData startPosData, HiddenBatchData endPosData, BiMatchBatchData ansMatch,
                List<HashSet<string>> posTrueData,
                RunnerBehavior behavior, string outputFile = "") : base(Structure.Empty, behavior)
            {
                StartPosData = startPosData;
                EndPosData = endPosData;
                PosTrueData = posTrueData;
                AnsMatch = ansMatch;

                OutputPath = outputFile;
            }
            string OutputPath = "";
            string resultFile = string.Empty;
            StreamWriter OutputWriter = null;

            string dumpFile = string.Empty;
            StreamWriter DumpWriter = null;
            public override void Init()
            {
                SampleNum = 0;
                HitNum = 0;
                if (!OutputPath.Equals(""))
                {
                    resultFile = OutputPath + "." + Iteration.ToString() + ".result";
                    dumpFile = OutputPath + "." + Iteration.ToString() + ".dump";
                    OutputWriter = new StreamWriter(resultFile);
                    DumpWriter = new StreamWriter(dumpFile);
                }
            }

            public override void Complete()
            {
                Logger.WriteLog("Sample {0}, Hit {1}, Accuracy {2}", SampleNum, HitNum, HitNum * 1.0 / SampleNum);
                if (OutputWriter != null)
                {
                    OutputWriter.Close();
                    DumpWriter.Close();

                    //restore.py --span_path dev.nltk.spans --raw_path dev.doc --ids_path dev.ids --fin score.4.result --fout dump.json
                    using (Process callProcess = new Process()
                    {
                        StartInfo = new ProcessStartInfo()
                        {
                            FileName = @"C:\cygwin64\bin\python2.7.exe", // executiveFile,
                            Arguments = string.Format("\"{0}\" --span_path \"{1}\" --raw_path \"{2}\" --ids_path \"{3}\" --fin \"{4}\" --fout {5}",
                            @"\PublicDataSource\SQuADV3\restore.py",
                            BuilderParameters.ValidSpan,
                            @"\PublicDataSource\SQuADV3\dev.doc",
                            @"\PublicDataSource\SQuADV3\dev.ids",
                            resultFile,
                            resultFile + ".json"
                            ),
                            CreateNoWindow = true,
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                        }
                    })
                    {
                        callProcess.Start();
                        callProcess.WaitForExit();
                    }

                    string resultsOutput = "";
                    using (Process callProcess = new Process()
                    {
                        StartInfo = new ProcessStartInfo()
                        {
                            FileName = @"C:\cygwin64\bin\python2.7.exe", // executiveFile,
                            Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\"",
                            @"\PublicDataSource\SQuADV3\evaluate-v1.1.py",
                            @"\PublicDataSource\SQuADV3\dev-v1.1.json",
                            resultFile + ".json"
                            ),
                            CreateNoWindow = true,
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                        }
                    })
                    {
                        callProcess.Start();
                        resultsOutput = callProcess.StandardOutput.ReadToEnd();
                        callProcess.WaitForExit();
                    }

                    Logger.WriteLog("Let see results here {0}", resultsOutput);
                }
                ObjectiveScore = HitNum * 1.0 / SampleNum;
                Iteration += 1;
            }

            public override void Forward()
            {
                ComputeLib.SparseSoftmax(AnsMatch.Src2MatchIdx, StartPosData.Output.Data, StartPosData.Output.Data, 1, AnsMatch.SrcSize);
                ComputeLib.SparseSoftmax(AnsMatch.Src2MatchIdx, EndPosData.Output.Data, EndPosData.Output.Data, 1, AnsMatch.SrcSize);

                StartPosData.Output.Data.SyncToCPU(StartPosData.BatchSize);
                EndPosData.Output.Data.SyncToCPU(EndPosData.BatchSize);
                AnsMatch.Src2MatchIdx.SyncToCPU(AnsMatch.BatchSize);

                for (int i = 0; i < AnsMatch.SrcSize; i++)
                {
                    Dictionary<string, float> candScoreDict = new Dictionary<string, float>();
                    string trueCandIdx = string.Empty;

                    int candBgn = i == 0 ? 0 : AnsMatch.Src2MatchIdx.MemPtr[i - 1];
                    int candEnd = AnsMatch.Src2MatchIdx.MemPtr[i];

                    List<string> prob = new List<string>();
                    for (int c = candBgn; c < candEnd; c++)
                    {
                        float candScoreStart = StartPosData.Output.Data.MemPtr[c]; // Util.Logistic();
                        float candScoreEnd = EndPosData.Output.Data.MemPtr[c]; // Util.Logistic(EndPosData.Output.Data.MemPtr[c2]);
                        int index = c - candBgn;
                        prob.Add(string.Format("{0}#{1}#{2}", index, candScoreStart, candScoreEnd));
                    }

                    if (DumpWriter != null) { DumpWriter.WriteLine(string.Join(" ", prob)); }

                    float maxP = 0;
                    float sP = 0;
                    float eP = 0;
                    int maxStart = -1;
                    int maxEnd = -1;
                    for (int c = candBgn; c < candEnd; c++)
                    {
                        float candScoreStart = StartPosData.Output.Data.MemPtr[c]; // Util.Logistic();
                        for (int c2 = c; c2 < candEnd; c2++)
                        {
                            float candScoreEnd = EndPosData.Output.Data.MemPtr[c2]; // Util.Logistic(EndPosData.Output.Data.MemPtr[c2]);

                            if (BuilderParameters.SPAN_LENGTH > 0 && c2 - c + 1 > BuilderParameters.SPAN_LENGTH) continue;
                            if (candScoreStart * candScoreEnd > maxP)
                            {
                                maxP = candScoreStart * candScoreEnd;
                                sP = candScoreStart;
                                eP = candScoreEnd;
                                maxStart = c - candBgn;
                                maxEnd = c2 - candBgn;
                            }
                        }
                    }
                    if (OutputWriter != null) { OutputWriter.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", maxStart, maxEnd, sP, eP, maxP); }
                    if (PosTrueData[SampleNum + i].Contains(string.Format("{0}#{1}", maxStart, maxEnd))) { HitNum += 1; }
                }
                SampleNum += AnsMatch.SrcSize;
            }

        }

        class TwoWayEnsembleRunner : StructRunner
        {
            public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
            //CudaPieceInt OutputMask;
            SeqDenseBatchData AData { get; set; }
            SeqDenseBatchData BData { get; set; }
            CudaPieceInt EnsembleMask;

            public TwoWayEnsembleRunner(SeqDenseBatchData aData, SeqDenseBatchData bData, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
            {
                AData = aData;
                BData = bData;

                Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 3, Behavior.Device);

                EnsembleMask = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask.MemPtr[s] = 2 + (s * 3);
                EnsembleMask.SyncFromCPU(AData.MAX_SENTSIZE);
            }

            public override void Forward()
            {
                Output.BatchSize = AData.BatchSize;
                Output.SentSize = AData.SentSize;

                ComputeLib.Matrix_AdditionEx(AData.SentOutput, 0, AData.Dim,
                                             AData.SentOutput, 0, AData.Dim,
                                             Output.SentOutput, 0, Output.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 0);

                ComputeLib.Matrix_AdditionEx(BData.SentOutput, 0, BData.Dim,
                                             BData.SentOutput, 0, BData.Dim,
                                             Output.SentOutput, AData.Dim, Output.Dim,
                                             BData.Dim, BData.SentSize, 1, 0, 0);

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, BData.SentOutput, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, EnsembleMask, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, BData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              AData.SentSize, AData.Dim, 1, 1);

                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, BData.SentOutput, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  BData.SentSize, BData.Dim, 1, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, AData.Dim, Output.Dim,
                                             BData.SentDeriv, 0, BData.Dim,
                                             BData.SentDeriv, 0, BData.Dim,
                                             BData.Dim, BData.SentSize, 1, 0, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, 0, Output.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 1);

            }
        }

        class ThreeWayEnsembleRunner : StructRunner
        {
            public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
            //CudaPieceInt OutputMask;
            SeqDenseBatchData AData { get; set; }
            SeqDenseBatchData BData { get; set; }

            HiddenBatchData CData { get; set; }

            CudaPieceInt EnsembleMask1;
            CudaPieceInt EnsembleMask2;

            CudaPieceFloat tmpOutput;
            public ThreeWayEnsembleRunner(SeqDenseBatchData aData, SeqDenseBatchData bData, HiddenBatchData cData, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
            {
                AData = aData;
                BData = bData;
                CData = cData;

                Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 4, Behavior.Device);
                tmpOutput = new CudaPieceFloat(AData.MAX_SENTSIZE * AData.Dim, true, Behavior.Device == DeviceType.GPU);
                EnsembleMask1 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask1.MemPtr[s] = 2 + (s * 4);
                EnsembleMask1.SyncFromCPU(AData.MAX_SENTSIZE);

                EnsembleMask2 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask2.MemPtr[s] = 3 + (s * 4);
                EnsembleMask2.SyncFromCPU(AData.MAX_SENTSIZE);
            }

            public override void Forward()
            {
                Output.BatchSize = AData.BatchSize;
                Output.SentSize = AData.SentSize;

                ComputeLib.Matrix_AdditionEx(AData.SentOutput, 0, AData.Dim,
                                             AData.SentOutput, 0, AData.Dim,
                                             Output.SentOutput, 0, Output.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 0);

                ComputeLib.Matrix_AdditionEx(BData.SentOutput, 0, BData.Dim,
                                             BData.SentOutput, 0, BData.Dim,
                                             Output.SentOutput, AData.Dim, Output.Dim,
                                             BData.Dim, BData.SentSize, 1, 0, 0);

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, BData.SentOutput, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, EnsembleMask1, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, CData.Output.Data, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, Output.SentMargin, 0, EnsembleMask2, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, CData.Output.Data, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask2, 0, Output.SentMargin, 0, CudaPieceInt.Empty, 0,
                                              Output.SentSize, AData.Dim, 1, 1);


                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, tmpOutput, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  Output.SentSize, AData.Dim, 0, 1);

                ComputeLib.ColumnWiseSumMask(tmpOutput, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
                                            Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);                
                //ComputeLib.AccurateElementwiseProduct(Output.SentDeriv, 0, AData.SentOutput, 0, CData.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
                //                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, Output.SentMargin, 0,
                //                                  Output.SentSize, AData.Dim, 1);


                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, BData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask1, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              AData.SentSize, AData.Dim, 1, 1);

                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, BData.SentOutput, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask1, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  BData.SentSize, BData.Dim, 1, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, AData.Dim, Output.Dim,
                                             BData.SentDeriv, 0, BData.Dim,
                                             BData.SentDeriv, 0, BData.Dim,
                                             BData.Dim, BData.SentSize, 1, 0, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, 0, Output.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 1);

            }
        }


        class TwoAdvWayEnsembleRunner : StructRunner
        {
            public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
            //CudaPieceInt OutputMask;
            SeqDenseBatchData AData { get; set; }

            HiddenBatchData CData { get; set; }

            CudaPieceInt EnsembleMask1;
            CudaPieceInt EnsembleMask2;

            CudaPieceFloat tmpOutput;
            public TwoAdvWayEnsembleRunner(SeqDenseBatchData aData, HiddenBatchData cData, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
            {
                AData = aData;

                CData = cData;

                Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 2, Behavior.Device);

                tmpOutput = new CudaPieceFloat(AData.MAX_SENTSIZE * AData.Dim, true, Behavior.Device == DeviceType.GPU);

                EnsembleMask1 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask1.MemPtr[s] = 0 + (s * 2);
                EnsembleMask1.SyncFromCPU(AData.MAX_SENTSIZE);

                EnsembleMask2 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask2.MemPtr[s] = 1 + (s * 2);
                EnsembleMask2.SyncFromCPU(AData.MAX_SENTSIZE);
            }

            public override void Forward()
            {
                Output.BatchSize = AData.BatchSize;
                Output.SentSize = AData.SentSize;


                ComputeLib.Matrix_AdditionMask(CData.Output.Data, 0, AData.SentMargin, 0,
                                               CData.Output.Data, 0, AData.SentMargin, 0,
                                               Output.SentOutput, 0, EnsembleMask1, 0,
                                               CData.Dim, AData.SentSize, 1, 0, 0);

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, CData.Output.Data, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, Output.SentMargin, 0, EnsembleMask2, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);

            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, CData.Output.Data, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask2, 0, Output.SentMargin, 0, CudaPieceInt.Empty, 0,
                                              Output.SentSize, AData.Dim, 1, 1);


                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, tmpOutput, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  Output.SentSize, AData.Dim, 0, 1);

                ComputeLib.ColumnWiseSumMask(tmpOutput, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
                                            Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
                //ComputeLib.AccurateElementwiseProduct(Output.SentDeriv, 0, AData.SentOutput, 0, CData.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
                //                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, Output.SentMargin, 0,
                //                                  Output.SentSize, AData.Dim, 1);

                ComputeLib.ColumnWiseSumMask(Output.SentDeriv, 0, EnsembleMask1, 0, CudaPieceFloat.Empty,
                                             Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
            }
        }



        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> context,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> query,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> contextl3g,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> queryl3g,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> contextchar,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> querychar,
                                                             DataCashier<DenseBatchData, DenseDataStat> startingPos,
                                                             DataCashier<DenseBatchData, DenseDataStat> endPos,
                                                             //IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> answer,

                                                             // text embedding cnn.
                                                             List<LayerStructure> EmbedCNN,
                                                             LayerStructure L3GEmbedCNN,
                                                             List<LayerStructure> CharEmbedCNN,

                                                             List<LayerStructure> HighwayGate,
                                                             List<LayerStructure> HighwayConnect,

                                                             // context lstm 
                                                             LSTMStructure ContextD1LSTM, LSTMStructure ContextD2LSTM,

                                                             LayerStructure ContextCNN,

                                                             // query lstm
                                                             LSTMStructure QueryD1LSTM, LSTMStructure QueryD2LSTM,

                                                             LayerStructure QueryCNN,

                                                             LayerStructure CoAttLayer,

                                                             LSTMStructure M1D1LSTM, LSTMStructure M1D2LSTM,
                                                             LSTMStructure M2D1LSTM, LSTMStructure M2D2LSTM,

                                                             LayerStructure AnsStartStruct, LayerStructure AnsEndStruct,
                                                             // model.
                                                             CompositeNNStructure model, RunnerBehavior Behavior, string resultFile)
        {
            ComputationGraph cg = new ComputationGraph();

            /************************************************************ Query Passage Candidate data *********/
            SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));
            SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(context, Behavior));
            SeqSparseBatchData Queryl3gData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(queryl3g, Behavior));
            SeqSparseBatchData Contextl3gData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(contextl3g, Behavior));
            SeqSparseBatchData QuerycharData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(querychar, Behavior));
            SeqSparseBatchData ContextcharData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(contextchar, Behavior));

            DenseBatchData StartPos = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(startingPos, Behavior));
            DenseBatchData EndPos = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(endPos, Behavior));

            /************************************************************ CNN on Query data *********/
            SeqDenseBatchData QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN[0], QueryData, Behavior) { IsBackProp = BuilderParameters.IS_EMBED_UPDATE, IsUpdate = BuilderParameters.IS_EMBED_UPDATE });
            for (int i = 1; i < EmbedCNN.Count; i++)
            {
                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new SeqDenseConvRunner<SeqDenseBatchData>(EmbedCNN[i], QueryEmbedOutput, Behavior) { IsBackProp = BuilderParameters.IS_EMBED_UPDATE, IsUpdate = BuilderParameters.IS_EMBED_UPDATE });
            }

            /************************************************************ Embedding on Passage data *********/
            SeqDenseBatchData ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN[0], ContextData, Behavior) { IsBackProp = BuilderParameters.IS_EMBED_UPDATE, IsUpdate = BuilderParameters.IS_EMBED_UPDATE });
            for (int i = 1; i < EmbedCNN.Count; i++)
            {
                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new SeqDenseConvRunner<SeqDenseBatchData>(EmbedCNN[i], ContextEmbedOutput, Behavior) {IsBackProp = BuilderParameters.IS_EMBED_UPDATE, IsUpdate = BuilderParameters.IS_EMBED_UPDATE });
            }

            if(L3GEmbedCNN != null)
            {
                SeqDenseBatchData l3gQueryEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
                    L3GEmbedCNN, Queryl3gData, Behavior));

                SeqDenseBatchData l3gContextEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
                    L3GEmbedCNN, Contextl3gData, Behavior));

                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QueryEmbedOutput, l3gQueryEmbed }, Behavior));

                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { ContextEmbedOutput, l3gContextEmbed }, Behavior));
            }

            if(CharEmbedCNN.Count > 0)
            {
                SeqDenseBatchData letterQueryEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
                    CharEmbedCNN[0], QuerycharData, Behavior));
                for (int i = 1; i < CharEmbedCNN.Count; i++)
                {
                    letterQueryEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(
                        CharEmbedCNN[i], letterQueryEmbed, Behavior));
                }
                HiddenBatchData letterQueryMaxpool = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(letterQueryEmbed, Behavior));
                SeqDenseBatchData charQueryEmbed = new SeqDenseBatchData(
                    new SequenceDataStat(QueryEmbedOutput.MAX_BATCHSIZE, QueryEmbedOutput.MAX_SENTSIZE, letterQueryMaxpool.Dim), 
                    QueryEmbedOutput.SampleIdx, QueryEmbedOutput.SentMargin,
                    letterQueryMaxpool.Output.Data, letterQueryMaxpool.Deriv.Data, Behavior.Device);
                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QueryEmbedOutput, charQueryEmbed }, Behavior));

                SeqDenseBatchData letterContextEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
                    CharEmbedCNN[0], ContextcharData, Behavior));
                for (int i = 1; i < CharEmbedCNN.Count; i++)
                {
                    letterContextEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(
                        CharEmbedCNN[i], letterContextEmbed, Behavior));
                }
                HiddenBatchData letterContextMaxpool = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(letterContextEmbed, Behavior));
                SeqDenseBatchData charContextEmbed = new SeqDenseBatchData(
                    //ContextEmbedOutput.Stat, 
                    new SequenceDataStat(ContextEmbedOutput.MAX_BATCHSIZE, ContextEmbedOutput.MAX_SENTSIZE, letterContextMaxpool.Dim),
                    ContextEmbedOutput.SampleIdx, ContextEmbedOutput.SentMargin,
                    letterContextMaxpool.Output.Data, letterContextMaxpool.Deriv.Data, Behavior.Device);
                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { ContextEmbedOutput, charContextEmbed }, Behavior));
            }
            /************************************************************ Highway net on Query and Passage Data *********/
            //SeqDenseBatchData Dropout_QueryEmbedOutput = QueryEmbedOutput;
            //SeqDenseBatchData Dropout_ContextEmbedOutput = ContextEmbedOutput;

            int highWayLayer = BuilderParameters.HighWay_Layer;

            if (highWayLayer > 0)
            {
                HiddenBatchData qtmpembed = new HiddenBatchData(QueryEmbedOutput.MAX_SENTSIZE, QueryEmbedOutput.Dim,
                                                                QueryEmbedOutput.SentOutput, QueryEmbedOutput.SentDeriv, Behavior.Device);

                HiddenBatchData dtmpembed = new HiddenBatchData(ContextEmbedOutput.MAX_SENTSIZE, ContextEmbedOutput.Dim,
                                                                ContextEmbedOutput.SentOutput, ContextEmbedOutput.SentDeriv, Behavior.Device);

                qtmpembed = (HiddenBatchData)cg.AddRunner(new HighwayNetRunner(HighwayConnect, HighwayGate, qtmpembed, Behavior));
                dtmpembed = (HiddenBatchData)cg.AddRunner(new HighwayNetRunner(HighwayConnect, HighwayGate, dtmpembed, Behavior));

                //Dropout_QueryEmbedOutput = QueryEmbedOutput;
                //Dropout_ContextEmbedOutput = ContextEmbedOutput;

                if (BuilderParameters.EMBEDDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
                {
                    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(qtmpembed, BuilderParameters.EMBEDDropout, Behavior, false));
                    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(dtmpembed, BuilderParameters.EMBEDDropout, Behavior, false));

                    //HiddenBatchData dropout_qembed = (HiddenBatchData)cg.AddRunner(new DropOutRunner<HiddenBatchData>(qtmpembed, BuilderParameters.EMBEDDropout, Behavior));
                    //HiddenBatchData dropout_dembed = (HiddenBatchData)cg.AddRunner(new DropOutRunner<HiddenBatchData>(dtmpembed, BuilderParameters.EMBEDDropout, Behavior));

                    //Dropout_QueryEmbedOutput = new SeqDenseBatchData(new SequenceDataStat(QueryEmbedOutput.MAX_BATCHSIZE, QueryEmbedOutput.MAX_SENTSIZE, QueryEmbedOutput.Dim, true),
                    //                                                QueryEmbedOutput.SampleIdx, QueryEmbedOutput.SentMargin,
                    //                                                dropout_qembed.Output.Data, dropout_qembed.Deriv.Data, Behavior.Device);

                    //Dropout_ContextEmbedOutput = new SeqDenseBatchData(new SequenceDataStat(ContextEmbedOutput.MAX_BATCHSIZE, ContextEmbedOutput.MAX_SENTSIZE, ContextEmbedOutput.Dim, true),
                    //                                                ContextEmbedOutput.SampleIdx, ContextEmbedOutput.SentMargin,
                    //                                                dropout_dembed.Output.Data, dropout_dembed.Deriv.Data, Behavior.Device);
                }

                QueryEmbedOutput = new SeqDenseBatchData(new SequenceDataStat(QueryEmbedOutput.MAX_BATCHSIZE, QueryEmbedOutput.MAX_SENTSIZE, QueryEmbedOutput.Dim),
                                                                    QueryEmbedOutput.SampleIdx, QueryEmbedOutput.SentMargin,
                                                                    qtmpembed.Output.Data, qtmpembed.Deriv.Data, Behavior.Device);

                ContextEmbedOutput = new SeqDenseBatchData(new SequenceDataStat(ContextEmbedOutput.MAX_BATCHSIZE, ContextEmbedOutput.MAX_SENTSIZE, ContextEmbedOutput.Dim),
                                                                    ContextEmbedOutput.SampleIdx, ContextEmbedOutput.SentMargin,
                                                                    dtmpembed.Output.Data, dtmpembed.Deriv.Data, Behavior.Device);

                /*
                for (int i = 0; i< highWayLayer; i++)
                {
                    HiddenBatchData qGate = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(HighwayGate[i], qtmpembed, Behavior));
                    HiddenBatchData qConnect = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(HighwayConnect[i], qtmpembed, Behavior));
                    qtmpembed = (HiddenBatchData)cg.AddRunner(new HighwayEnsembleRunner(qtmpembed, qConnect, qGate, Behavior)); 
                }
                */
                /*
                for (int i = 0; i < highWayLayer; i++)
                {
                    HiddenBatchData dGate = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(HighwayGate[i], dtmpembed, Behavior));
                    HiddenBatchData dConnect = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(HighwayConnect[i], dtmpembed, Behavior));
                    dtmpembed = (HiddenBatchData)cg.AddRunner(new HighwayEnsembleRunner(dtmpembed, dConnect, dGate, Behavior));
                }*/
            }



            /************************************************************ LSTM on Query data *********/
            //HiddenBatchData QueryOutput = null;
            SeqDenseBatchData QuerySeqO = null;
            BiMatchBatchData QueryMatch = null;
            {
                /*
                SeqDenseRecursiveData QueryD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(
                    new Cook_Seq2TransposeSeqRunner(DropoutQueryEmbedOutput, false, Behavior) { IsUpdate = true });
                SeqDenseRecursiveData QueryD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(
                    new FastLSTMDenseRunner<SeqDenseRecursiveData>(QueryD1LSTM.LSTMCells[0], QueryD1LstmInput, Behavior) { IsUpdate = true });
                SeqDenseRecursiveData QueryD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(
                    new Cook_Seq2TransposeSeqRunner(DropoutQueryEmbedOutput, true, Behavior) { IsUpdate = true });
                SeqDenseRecursiveData QueryD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(
                    new FastLSTMDenseRunner<SeqDenseRecursiveData>(QueryD2LSTM.LSTMCells[0], QueryD2LstmInput, Behavior) { IsUpdate = true });

                SeqDenseBatchData QuerySeqD1O = (SeqDenseBatchData)cg.AddRunner(
                    new Cook_TransposeSeq2SeqRunner(QueryD1LstmOutput, Behavior) { IsUpdate = true });
                SeqDenseBatchData QuerySeqD2O = (SeqDenseBatchData)cg.AddRunner(
                    new Cook_TransposeSeq2SeqRunner(QueryD2LstmOutput, Behavior) { IsUpdate = true });
                QuerySeqO = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QuerySeqD1O, QuerySeqD2O }, Behavior) { IsUpdate = true });
                QueryMatch = (BiMatchBatchData)cg.AddRunner(
                    new SeqBiMatchRunner(QuerySeqO, Behavior) { IsUpdate = true });
                */
                QuerySeqO = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(QueryD1LSTM, QueryD2LSTM, QueryEmbedOutput, Behavior));
                QueryMatch = (BiMatchBatchData)cg.AddRunner(new SeqBiMatchRunner(QuerySeqO, Behavior) { IsUpdate = true });
            }

            if(QueryCNN != null)
            {
                SeqDenseBatchData QueryCNNEmbed = (SeqDenseBatchData)cg.AddRunner(
                    new SeqDenseConvRunner<SeqDenseBatchData>(QueryCNN, QueryEmbedOutput, Behavior));
                QuerySeqO = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QuerySeqO, QueryCNNEmbed }, Behavior));
            }


            SeqDenseBatchData MemorySeqO = null;
            BiMatchBatchData MemoryMatch = null;
            /************************************************************ LSTM on Passage data *********/
            {
                /*
                SeqDenseRecursiveData CandidateD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(
                    new Cook_Seq2TransposeSeqRunner(DropoutContextEmbedOutput, false, Behavior) { IsUpdate = true });
                SeqDenseRecursiveData CandidateD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(
                    new FastLSTMDenseRunner<SeqDenseRecursiveData>(ContextD1LSTM.LSTMCells[0], CandidateD1LstmInput, Behavior) { IsUpdate = true });

                SeqDenseRecursiveData CandidateD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(
                    new Cook_Seq2TransposeSeqRunner(DropoutContextEmbedOutput, true, Behavior) { IsUpdate = true });
                SeqDenseRecursiveData CandidateD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(
                    new FastLSTMDenseRunner<SeqDenseRecursiveData>(ContextD2LSTM.LSTMCells[0], CandidateD2LstmInput, Behavior) { IsUpdate = true });

                SeqDenseBatchData CandidateSeqD1O = (SeqDenseBatchData)cg.AddRunner(
                    new Cook_TransposeSeq2SeqRunner(CandidateD1LstmOutput, Behavior) { IsUpdate = true });
                SeqDenseBatchData CandidateSeqD2O = (SeqDenseBatchData)cg.AddRunner(
                    new Cook_TransposeSeq2SeqRunner(CandidateD2LstmOutput, Behavior) { IsUpdate = true });
                MemorySeqO = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { CandidateSeqD1O, CandidateSeqD2O }, Behavior) { IsUpdate = true });
                MemoryMatch = (BiMatchBatchData)cg.AddRunner(new SeqBiMatchRunner(MemorySeqO, Behavior) { IsUpdate = true });
                */
                MemorySeqO = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(ContextD1LSTM, ContextD2LSTM, ContextEmbedOutput, Behavior));
                MemoryMatch = (BiMatchBatchData)cg.AddRunner(new SeqBiMatchRunner(MemorySeqO, Behavior) { IsUpdate = true });
            }

            if (ContextCNN != null)
            {
                SeqDenseBatchData ContextCNNEmbed = (SeqDenseBatchData)cg.AddRunner(
                    new SeqDenseConvRunner<SeqDenseBatchData>(ContextCNN, ContextEmbedOutput, Behavior));
                MemorySeqO = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { MemorySeqO, ContextCNNEmbed }, Behavior));
            }


            //SeqDenseBatchData DropoutMemorySeqO = MemorySeqO;
            //SeqDenseBatchData DropoutQuerySeqO = QuerySeqO;
            //if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            //{
            //    HiddenBatchData qembed = new HiddenBatchData(QuerySeqO.MAX_SENTSIZE, QuerySeqO.Dim, QuerySeqO.SentOutput, QuerySeqO.SentDeriv, Behavior.Device);
            //    HiddenBatchData dembed = new HiddenBatchData(MemorySeqO.MAX_SENTSIZE, MemorySeqO.Dim, MemorySeqO.SentOutput, MemorySeqO.SentDeriv, Behavior.Device);
            //    HiddenBatchData DropoutQembed = (HiddenBatchData) cg.AddRunner(new DropOutRunner<HiddenBatchData>(qembed, BuilderParameters.LSTMDropout, Behavior) { IsUpdate = true });
            //    HiddenBatchData DropoutDembed = (HiddenBatchData) cg.AddRunner(new DropOutRunner<HiddenBatchData>(dembed, BuilderParameters.LSTMDropout, Behavior) { IsUpdate = true });

            //    DropoutQuerySeqO = new SeqDenseBatchData(new SequenceDataStat(QuerySeqO.MAX_BATCHSIZE, QuerySeqO.MAX_SENTSIZE, QuerySeqO.Dim, true),
            //                                                    QuerySeqO.SampleIdx, QuerySeqO.SentMargin, 
            //                                                    DropoutQembed.Output.Data, DropoutQembed.Deriv.Data, Behavior.Device);


            //    DropoutMemorySeqO = new SeqDenseBatchData(new SequenceDataStat(MemorySeqO.MAX_BATCHSIZE, MemorySeqO.MAX_SENTSIZE, MemorySeqO.Dim, true),
            //                                                    MemorySeqO.SampleIdx, MemorySeqO.SentMargin,
            //                                                    DropoutDembed.Output.Data, DropoutDembed.Deriv.Data, Behavior.Device);
            //}

            if (BuilderParameters.CoAttDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                //HiddenBatchData qembed = new HiddenBatchData(QuerySeqO.MAX_SENTSIZE, QuerySeqO.Dim, QuerySeqO.SentOutput, QuerySeqO.SentDeriv, Behavior.Device);
                //HiddenBatchData dembed = new HiddenBatchData(MemorySeqO.MAX_SENTSIZE, MemorySeqO.Dim, MemorySeqO.SentOutput, MemorySeqO.SentDeriv, Behavior.Device);
                //HiddenBatchData DropoutQembed = (HiddenBatchData)cg.AddRunner(new DropOutRunner<HiddenBatchData>(qembed, BuilderParameters.LSTMDropout, Behavior) { IsUpdate = true });
                //HiddenBatchData DropoutDembed = (HiddenBatchData)cg.AddRunner(new DropOutRunner<HiddenBatchData>(dembed, BuilderParameters.LSTMDropout, Behavior) { IsUpdate = true });

                //DropoutQuerySeqO = new SeqDenseBatchData(new SequenceDataStat(QuerySeqO.MAX_BATCHSIZE, QuerySeqO.MAX_SENTSIZE, QuerySeqO.Dim, true),
                //                                                QuerySeqO.SampleIdx, QuerySeqO.SentMargin,
                //                                                DropoutQembed.Output.Data, DropoutQembed.Deriv.Data, Behavior.Device);


                //DropoutMemorySeqO = new SeqDenseBatchData(new SequenceDataStat(MemorySeqO.MAX_BATCHSIZE, MemorySeqO.MAX_SENTSIZE, MemorySeqO.Dim, true),
                //                                                MemorySeqO.SampleIdx, MemorySeqO.SentMargin,
                //                                                DropoutDembed.Output.Data, DropoutDembed.Deriv.Data, Behavior.Device);

                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(QuerySeqO.MAX_SENTSIZE, QuerySeqO.Dim, QuerySeqO.SentOutput, QuerySeqO.SentDeriv, Behavior.Device), 
                    BuilderParameters.CoAttDropout, Behavior, false) { IsUpdate = true });

                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(MemorySeqO.MAX_SENTSIZE, MemorySeqO.Dim, MemorySeqO.SentOutput, MemorySeqO.SentDeriv, Behavior.Device), 
                    BuilderParameters.CoAttDropout, Behavior, false) { IsUpdate = true });
            }

            //BiMatchBatchData d2qMatchData = (BiMatchBatchData)cg.AddRunner(new CrossSeqBiMatchRunner(MemorySeqO, QuerySeqO, DataPanel.MaxCrossQDLen, Behavior) { IsUpdate = true });

            SoftSeqLinearSimRunner coattRunner = new SoftSeqLinearSimRunner(MemorySeqO, QuerySeqO, DataPanel.MaxCrossQDLen, CoAttLayer, Behavior) { IsUpdate = true };
            BiMatchBatchData d2qMatchData = coattRunner.MatchData;
            HiddenBatchData CoAttL = (HiddenBatchData)cg.AddRunner(coattRunner);

            HiddenBatchData d2qData = (HiddenBatchData)cg.AddRunner(new MemoryRetrievalRunner(CoAttL, QuerySeqO, d2qMatchData, 1, Behavior) { IsUpdate = true });
            SeqDenseBatchData MemorySeqO2 = new SeqDenseBatchData(new SequenceDataStat(MemorySeqO.MAX_BATCHSIZE, MemorySeqO.MAX_SENTSIZE, d2qData.Dim),
                                                                MemorySeqO.SampleIdx, MemorySeqO.SentMargin, 
                                                                d2qData.Output.Data, d2qData.Deriv.Data, Behavior.Device);

            SeqDenseBatchData DocG = null;

            if (BuilderParameters.MEMORY_ENSEMBLE == 2)
            {
                DocG = (SeqDenseBatchData)cg.AddRunner(new TwoWayEnsembleRunner(MemorySeqO, MemorySeqO2, Behavior) { IsUpdate = true });
            }
            else if (BuilderParameters.MEMORY_ENSEMBLE == 3)
            {
                HiddenBatchData maxCoeff = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(
                                new SeqDenseBatchData(new SequenceDataStat()
                                {
                                    MAX_BATCHSIZE = d2qMatchData.Stat.MAX_SRC_BATCHSIZE,
                                    MAX_SEQUENCESIZE = d2qMatchData.Stat.MAX_MATCH_BATCHSIZE,
                                    FEATURE_DIM = CoAttL.Dim
                                }, d2qMatchData.Src2MatchIdx, d2qMatchData.SrcIdx, 
                                CoAttL.Output.Data, CoAttL.Deriv.Data, Behavior.Device), Behavior) { IsUpdate = true });
                HiddenBatchData d2qMaxData = (HiddenBatchData)cg.AddRunner(new MemoryRetrievalRunner(maxCoeff, MemorySeqO, MemoryMatch, 1, Behavior) { IsUpdate = true });
                DocG = (SeqDenseBatchData)cg.AddRunner(new ThreeWayEnsembleRunner(MemorySeqO, MemorySeqO2, d2qMaxData, Behavior) { IsUpdate = true });
            }

            //if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            //{
            //    HiddenBatchData docG = new HiddenBatchData(DocG.MAX_SENTSIZE, DocG.Dim, DocG.SentOutput, DocG.SentDeriv, Behavior.Device);
            //    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(docG, BuilderParameters.LSTMDropout, Behavior, false) { IsUpdate = true });
            //}

            //SeqDenseBatchData Dropout_DocG = DocG;
            if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                //HiddenBatchData dropoutdocG = (HiddenBatchData)cg.AddRunner(new DropOutRunner<HiddenBatchData>(
                //    new HiddenBatchData(DocG.MAX_SENTSIZE, DocG.Dim, DocG.SentOutput, DocG.SentDeriv, Behavior.Device), 
                //    BuilderParameters.LSTMDropout, Behavior) { IsUpdate = true });
                //Dropout_DocG = new SeqDenseBatchData(new SequenceDataStat(DocG.MAX_BATCHSIZE, DocG.MAX_SENTSIZE, DocG.Dim, true),
                //                                                    DocG.SampleIdx, DocG.SentMargin,
                //                                                    dropoutdocG.Output.Data, dropoutdocG.Deriv.Data, Behavior.Device);
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(
                        new HiddenBatchData(DocG.MAX_SENTSIZE, DocG.Dim, DocG.SentOutput, DocG.SentDeriv, Behavior.Device),  BuilderParameters.LSTMDropout, Behavior, false) { IsUpdate = true });
            }

            /*
            SeqDenseRecursiveData GD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(DropoutDocG, false, Behavior) { IsUpdate = true });
            for (int i = 0; i < M1D1LSTM.LSTMCells.Count; i++)
            {
                GD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(M1D1LSTM.LSTMCells[i], GD1LstmInput, Behavior) {IsUpdate = true });
            }
            SeqDenseBatchData GD1LstmOutput = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(GD1LstmInput, Behavior) { IsUpdate = true });
            SeqDenseRecursiveData GD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(DropoutDocG, true, Behavior) { IsUpdate = true });
            for (int i = 0; i < M1D2LSTM.LSTMCells.Count; i++)
            {
                GD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(M1D2LSTM.LSTMCells[i], GD2LstmInput, Behavior) { IsUpdate = true });
            }
            SeqDenseBatchData GD2LstmOutput = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(GD2LstmInput, Behavior) { IsUpdate = true });
            //g1 = 2h
            SeqDenseBatchData DocM1 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { GD1LstmOutput, GD2LstmOutput }, Behavior) { IsUpdate = true });
            */
            SeqDenseBatchData DocM1 = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(M1D1LSTM, M1D2LSTM, DocG, Behavior));

            //[p0, g1] = 4d + 2h
            SeqDenseBatchData DocGM1 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocG, DocM1 }, Behavior) { IsUpdate = true });
            HiddenBatchData startInput = new HiddenBatchData(DocGM1.MAX_SENTSIZE, DocGM1.Dim, DocGM1.SentOutput, DocGM1.SentDeriv, Behavior.Device);
            //HiddenBatchData Dropout_StartInput = startInput;
            if (BuilderParameters.Soft1Dropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                //Dropout_StartInput = (HiddenBatchData)cg.AddRunner(new DropOutRunner<HiddenBatchData>(startInput, BuilderParameters.Soft1Dropout, Behavior) { IsUpdate = true });
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(startInput, BuilderParameters.Soft1Dropout, Behavior, false) { IsUpdate = true });
            }
            HiddenBatchData StartP = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStartStruct, startInput, Behavior) { IsUpdate = true });

            /************start point memory*****/
            // ali = 2h
            HiddenBatchData a1i = (HiddenBatchData)cg.AddRunner(new MemoryRetrievalRunner(StartP, DocM1, MemoryMatch, 1, Behavior) { IsUpdate = true });
            //[ali, g1 * ali] = 4h
            SeqDenseBatchData DocM1_Adv = (SeqDenseBatchData)cg.AddRunner(new TwoAdvWayEnsembleRunner(DocM1, a1i, Behavior) { IsUpdate = true });

            //[p0, g1, ali, g1 * ali] = 4d + 6h
            SeqDenseBatchData DocM1_New = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocGM1, DocM1_Adv }, Behavior) { IsUpdate = true });

            int endPointModel = BuilderParameters.ENDPOINT_CONFIG;
            if (endPointModel == 1) { DocM1 = DocM1_New; }

            //SeqDenseBatchData Dropout_DocM1 = DocM1;
            if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                //HiddenBatchData dropoutdocM1 = (HiddenBatchData)cg.AddRunner(new DropOutRunner<HiddenBatchData>(
                //    new HiddenBatchData(DocM1.MAX_SENTSIZE, DocM1.Dim, DocM1.SentOutput, DocM1.SentDeriv, Behavior.Device),
                //    BuilderParameters.LSTMDropout, Behavior) { IsUpdate = true });
                //Dropout_DocM1 = new SeqDenseBatchData(new SequenceDataStat(DocM1.MAX_BATCHSIZE, DocM1.MAX_SENTSIZE, DocM1.Dim, true),
                //                                                    DocM1.SampleIdx, DocM1.SentMargin,
                //                                                    dropoutdocM1.Output.Data, dropoutdocM1.Deriv.Data, Behavior.Device);
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(
                    new HiddenBatchData(DocM1.MAX_SENTSIZE, DocM1.Dim, DocM1.SentOutput, DocM1.SentDeriv, Behavior.Device),
                    BuilderParameters.LSTMDropout, Behavior, false) { IsUpdate = true });
            }

            /*
            SeqDenseRecursiveData M1D1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(DropoutDocM1, false, Behavior) { IsUpdate = true });
            for (int i = 0; i < M2D1LSTM.LSTMCells.Count; i++)
            {
                M1D1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(M2D1LSTM.LSTMCells[i], M1D1LstmInput, Behavior) { IsUpdate = true });
            }
            SeqDenseBatchData M1D1LstmOutput = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(M1D1LstmInput, Behavior) { IsUpdate = true });
            SeqDenseRecursiveData M1D2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(DropoutDocM1, true, Behavior) { IsUpdate = true });
            for (int i = 0; i < M2D2LSTM.LSTMCells.Count; i++)
            {
                M1D2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(M2D2LSTM.LSTMCells[i], M1D2LstmInput, Behavior) { IsUpdate = true });
            }
            SeqDenseBatchData M1D2LstmOutput = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(M1D2LstmInput, Behavior) { IsUpdate = true });
            SeqDenseBatchData DocM2 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { M1D1LstmOutput, M1D2LstmOutput }, Behavior) { IsUpdate = true });
            */
            SeqDenseBatchData DocM2 = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(M2D1LSTM, M2D2LSTM, DocM1, Behavior));


            SeqDenseBatchData DocGM2 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocG, DocM2 }, Behavior) { IsUpdate = true });
            HiddenBatchData endInput = new HiddenBatchData(DocGM2.MAX_SENTSIZE, DocGM2.Dim, DocGM2.SentOutput, DocGM2.SentDeriv, Behavior.Device);

            //HiddenBatchData Dropout_EndInput = endInput;
            if (BuilderParameters.Soft2Dropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                //Dropout_EndInput = (HiddenBatchData)cg.AddRunner(new DropOutRunner<HiddenBatchData>(endInput, BuilderParameters.Soft2Dropout, Behavior) { IsUpdate = true });
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(endInput, BuilderParameters.Soft2Dropout, Behavior, false) { IsUpdate = true });
            }
            HiddenBatchData EndP = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsEndStruct, endInput, Behavior) { IsUpdate = true });

            //if (Behavior.RunMode == DNNRunMode.Train)
            //{
            //    if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
            //        cg.AddRunner(new ModelDiskDumpRunner(model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\RecurrentAttention"));
            //}

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new BayesianPairRunner(StartPos.Data, EndPos.Data, StartP, EndP, MemoryMatch, 1, Behavior));
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new ExactMatchPredictionRunner(StartP, EndP, MemoryMatch, DataPanel.ValidResults, Behavior, BuilderParameters.ScoreOutputPath));
                    break;
            }

            return cg;
        }

        private int InitEmbedLayers(List<LayerStructure> embedLayers, DeviceType device)
        {
            int embedDim = DataPanel.WordSize;
            for (int i = 0; i < BuilderParameters.EMBED_LAYER_DIM.Length; i++)
            {
                embedLayers.Add(new LayerStructure(embedDim, BuilderParameters.EMBED_LAYER_DIM[i],
                    BuilderParameters.EMBED_ACTIVATION[i], N_Type.Convolution_layer, BuilderParameters.EMBED_LAYER_WIN[i],
                    BuilderParameters.EMBED_DROPOUT[i], false, device));
                embedDim = BuilderParameters.EMBED_LAYER_DIM[i];
            }

            #region Glove Work Embedding Initialization.

            if (!BuilderParameters.InitWordEmbedding.Equals(string.Empty))
            {
                int wordDim = BuilderParameters.EMBED_LAYER_DIM[0];

                int halfWinStart = BuilderParameters.EMBED_LAYER_WIN[0] / 2; // * DataPanel.WordDim;

                Logger.WriteLog("Init Glove Word Embedding ...");
                int hit = 0;
                using (StreamReader mreader = new StreamReader(BuilderParameters.InitWordEmbedding))
                {
                    int[] HitWords = new int[DataPanel.wordFreqDict.ItemDictSize]; 
                    while (!mreader.EndOfStream)
                    {
                        string[] items = mreader.ReadLine().Split(' ');
                        string word = items[0];
                        float[] vec = new float[items.Length - 1];
                        for (int i = 1; i < items.Length; i++)
                        {
                            vec[i - 1] = float.Parse(items[i]);
                        }

                        int wordIdx = DataPanel.wordFreqDict.IndexItem(word);
                        
                        if (wordIdx >= 0)
                        {
                            HitWords[wordIdx] = 1;
                            int feaIdx = wordIdx;

                            for (int win = -halfWinStart; win < BuilderParameters.EMBED_LAYER_WIN[0] - halfWinStart; win++)
                            {
                                for (int i = 0; i < wordDim; i++)
                                {
                                    embedLayers[0].weight.MemPtr[((win + halfWinStart) * DataPanel.WordSize + feaIdx) * wordDim + i] = vec[i];
                                }
                            }
                            hit++;
                        }
                        else
                        {
                            //....
                        }
                    }
                    if (BuilderParameters.UNK_INIT == 0)
                    {
                        Logger.WriteLog("UNK is initalized to zero!");
                        for (int u = 0; u < DataPanel.wordFreqDict.ItemDictSize; u++)
                        {
                            if (HitWords[u] == 0)
                            {
                                int unkIdx = u;
                                for (int win = -halfWinStart; win < BuilderParameters.EMBED_LAYER_WIN[0] - halfWinStart; win++)
                                {
                                    for (int i = 0; i < wordDim; i++)
                                    {
                                        embedLayers[0].weight.MemPtr[((win + halfWinStart) * DataPanel.WordSize + unkIdx) * wordDim + i] = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                embedLayers[0].weight.SyncFromCPU();
                Logger.WriteLog("Init Glove Word Embedding Done, {0} words initalized in total {1}.", hit, DataPanel.wordFreqDict.ItemDictSize);
            }
            #endregion.

            return embedDim;
        }
        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");

            CompositeNNStructure modelStructure = new CompositeNNStructure();

            // word embedding layer.
            List<LayerStructure> embedLayers = new List<LayerStructure>();

            // l3g embedding layer.
            LayerStructure l3gembedLayer = null;

            // char embedding layer.
            List<LayerStructure> charembedLayer = new List<LayerStructure>();

            List<LayerStructure> highwayConnect = new List<LayerStructure>();
            List<LayerStructure> highwayGate = new List<LayerStructure>();

            LayerStructure CNNembed = null;

            //context lstm layer.
            LSTMStructure contextD1LSTM = null;
            LSTMStructure contextD2LSTM = null;

            //query lstm layer.
            LSTMStructure queryD1LSTM = null;
            LSTMStructure queryD2LSTM = null;


            LayerStructure CoAttLayer = null;

            LSTMStructure M1D1LSTM = null;
            LSTMStructure M1D2LSTM = null;
            LSTMStructure M2D1LSTM = null;
            LSTMStructure M2D2LSTM = null;

            LayerStructure AnsStartStruct = null;
            LayerStructure AnsEndStruct = null;

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                int embedDim = InitEmbedLayers(embedLayers, device);
                int l3gembedDim = BuilderParameters.L3G_EMBED_DIM;
                if (l3gembedDim > 0)
                {
                    l3gembedLayer = new LayerStructure(DataPanel.L3gSize, BuilderParameters.L3G_EMBED_DIM,
                        A_Func.Tanh, N_Type.Convolution_layer, BuilderParameters.L3G_EMBED_WIN, 0, false, device);
                    modelStructure.AddLayer(l3gembedLayer);
                }

                int charembedDim = BuilderParameters.CHAR_EMBED_DIM.Last();
                if (charembedDim > 0)
                {
                    int inputDim = DataPanel.CharSize;
                    for (int i = 0; i < BuilderParameters.CHAR_EMBED_DIM.Length; i++)
                    {
                        LayerStructure charEmbed = (new LayerStructure(inputDim, BuilderParameters.CHAR_EMBED_DIM[i],
                            A_Func.Tanh, N_Type.Convolution_layer, BuilderParameters.CHAR_EMBED_WIN[i], 0, false, device));
                        inputDim = BuilderParameters.CHAR_EMBED_DIM[i];
                        charembedLayer.Add(charEmbed);
                        modelStructure.AddLayer(charEmbed);
                    }
                }

                embedDim = embedDim + l3gembedDim + charembedDim;

                for (int i = 0; i < BuilderParameters.HighWay_Layer; i++)
                {
                    if (i == 0 || !BuilderParameters.HighWay_Share)
                    {
                        highwayConnect.Add(new LayerStructure(embedDim, embedDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true, device));
                        highwayGate.Add(new LayerStructure(embedDim, embedDim, A_Func.Sigmoid, N_Type.Fully_Connected, 1, 0, true, device));

                        modelStructure.AddLayer(highwayConnect[i]);
                        modelStructure.AddLayer(highwayGate[i]);
                    }
                    else
                    {
                        highwayConnect.Add(highwayConnect[0]);
                        highwayGate.Add(highwayGate[0]);
                    }
                }
                {
                    contextD1LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    contextD2LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                }

                if (!BuilderParameters.IS_SHARE_RNN)
                {
                    queryD1LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    queryD2LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                }
                else
                {
                    queryD1LSTM = contextD1LSTM;
                    queryD2LSTM = contextD2LSTM;
                }

                if(BuilderParameters.CNN_DIM > 0)
                {
                    CNNembed = (new LayerStructure(embedDim, BuilderParameters.CNN_DIM, A_Func.Tanh, N_Type.Convolution_layer, 
                        BuilderParameters.CNN_WIN, 0, true, device));
                }

                int D = BuilderParameters.GRU_DIM * 2 + BuilderParameters.CNN_DIM;
                int H = BuilderParameters.GRU_DIM;

                CoAttLayer = new LayerStructure(3 * D, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);

                if (BuilderParameters.MEMORY_ENSEMBLE == 2)
                {
                    M1D1LSTM = new LSTMStructure(3 * D, new int[] { H }, device, BuilderParameters.RndInit);
                    M1D2LSTM = new LSTMStructure(3 * D, new int[] { H }, device, BuilderParameters.RndInit);
                    
                    AnsStartStruct = new LayerStructure(3 * D + 2 * H, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                    AnsEndStruct = new LayerStructure(3 * D + 2 * H, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                }
                else
                {
                    M1D1LSTM = new LSTMStructure(4 * D, new int[] { H }, device, BuilderParameters.RndInit);
                    M1D2LSTM = new LSTMStructure(4 * D, new int[] { H }, device, BuilderParameters.RndInit);

                    AnsStartStruct = new LayerStructure(4 * D + 2 * H, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                    AnsEndStruct = new LayerStructure(4 * D + 2 * H, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                }

                if (BuilderParameters.ENDPOINT_CONFIG == 0)
                {
                    M2D1LSTM = new LSTMStructure(2 * H, new int[] { H }, device, BuilderParameters.RndInit);
                    M2D2LSTM = new LSTMStructure(2 * H, new int[] { H }, device, BuilderParameters.RndInit);
                }
                else
                {
                    M2D1LSTM = new LSTMStructure(4 * D + 6 * H, new int[] { H }, device, BuilderParameters.RndInit);
                    M2D2LSTM = new LSTMStructure(4 * D + 6 * H, new int[] { H }, device, BuilderParameters.RndInit);
                }
                if (BuilderParameters.IS_EMBED_UPDATE)
                {
                    for (int i = 0; i < embedLayers.Count; i++)
                        modelStructure.AddLayer(embedLayers[i]);
                }
                else
                {
                    for (int i = 0; i < embedLayers.Count; i++)
                        embedLayers[i].StructureOptimizer.Clear();
                }

                modelStructure.AddLayer(CoAttLayer);
                modelStructure.AddLayer(M1D1LSTM);
                modelStructure.AddLayer(M1D2LSTM);
                modelStructure.AddLayer(M2D1LSTM);
                modelStructure.AddLayer(M2D2LSTM);
                modelStructure.AddLayer(AnsStartStruct);
                modelStructure.AddLayer(AnsEndStruct);
                {
                    modelStructure.AddLayer(contextD1LSTM);
                    modelStructure.AddLayer(contextD2LSTM);
                }
                if (!BuilderParameters.IS_SHARE_RNN)
                {
                    modelStructure.AddLayer(queryD1LSTM);
                    modelStructure.AddLayer(queryD2LSTM);
                }

                if(BuilderParameters.CNN_DIM > 0) { modelStructure.AddLayer(CNNembed); }
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, device);
                    int link = 0;

                    if(BuilderParameters.L3G_EMBED_DIM > 0)
                    {
                        l3gembedLayer = (LayerStructure)modelStructure.CompositeLinks[link++];
                    }

                    if (BuilderParameters.CHAR_EMBED_DIM.Last() > 0)
                    {
                        for (int i = 0; i < BuilderParameters.CHAR_EMBED_DIM.Length; i++)
                        {
                            charembedLayer.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                        }
                    }

                    for (int i = 0; i < BuilderParameters.HighWay_Layer; i++)
                    {
                        if (i == 0 || !BuilderParameters.HighWay_Share)
                        {
                            highwayConnect.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                            highwayGate.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                        }
                        else
                        {
                            highwayConnect.Add(highwayConnect[0]);
                            highwayGate.Add(highwayGate[0]);
                        }
                    }

                    if (BuilderParameters.IS_EMBED_UPDATE)
                    {
                        for (int i = 0; i < BuilderParameters.EMBED_LAYER_DIM.Length; i++)
                            embedLayers.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                    }
                    else
                    {
                        InitEmbedLayers(embedLayers, device);
                    }
                    CoAttLayer = (LayerStructure)modelStructure.CompositeLinks[link++];
                    M1D1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    M1D2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    M2D1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    M2D2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    AnsStartStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                    AnsEndStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                    {
                        contextD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                        contextD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    }
                    if (!BuilderParameters.IS_SHARE_RNN)
                    {
                        queryD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                        queryD2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    }
                    else
                    {
                        queryD1LSTM = contextD1LSTM;
                        queryD2LSTM = contextD2LSTM;
                    }
                    if (BuilderParameters.CNN_DIM > 0) { CNNembed = (LayerStructure) modelStructure.CompositeLinks[link++]; }
                }
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainContextL3G, DataPanel.TrainQueryL3G, DataPanel.TrainContextChar, DataPanel.TrainQueryChar, DataPanel.TrainStartPos, DataPanel.TrainEndPos,
                        embedLayers, l3gembedLayer, charembedLayer, highwayGate, highwayConnect, contextD1LSTM, contextD2LSTM, CNNembed, queryD1LSTM, queryD2LSTM, CNNembed,
                        CoAttLayer, M1D1LSTM, M1D2LSTM, M2D1LSTM, M2D2LSTM, AnsStartStruct, AnsEndStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }, string.Empty);
                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    //ComputationGraph testCG = null;
                    //if (BuilderParameters.IsTestFile)
                    //    testCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                    //        embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                    //        stateStruct, attStruct, att2Struct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.ScoreOutputPath + ".Test");

                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraph(DataPanel.ValidContext, DataPanel.ValidQuery, DataPanel.ValidContextL3G, DataPanel.ValidQueryL3G, DataPanel.ValidContextChar, DataPanel.ValidQueryChar, DataPanel.ValidStartPos, DataPanel.ValidEndPos,
                            embedLayers, l3gembedLayer, charembedLayer, highwayGate, highwayConnect, contextD1LSTM, contextD2LSTM, CNNembed, queryD1LSTM, queryD2LSTM, CNNembed,
                            CoAttLayer, M1D1LSTM, M1D2LSTM, M2D1LSTM, M2D2LSTM, AnsStartStruct, AnsEndStruct, modelStructure,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, string.Empty);

                    double bestValidScore = double.MinValue; // validCG.Execute(); //double.MinValue;
                    //double bestTestScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        //modelStructure.ApplyShadowParameter();
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }

                        //double testScore = 0;
                        double validScore = 0;
                        //if (testCG != null)
                        //{
                        //    testScore = testCG.Execute();
                        //    Logger.WriteLog("Test Score {0}, At iter {1}", testScore, bestIter);
                        //}
                        if (validCG != null)
                        {
                            validScore = validCG.Execute();
                            Logger.WriteLog("Valid Score {0}, At iter {1}", validScore, iter);
                        }

                        if (validScore > bestValidScore)
                        {
                            bestValidScore = validScore; bestIter = iter;
                        }
                        Logger.WriteLog("Best Valid Score {0},  At iter {1}", bestValidScore, bestIter);

                        //modelStructure.ApplyTrueParameter();
                    }
                    break;
                case DNNRunMode.Predict:
                    //ComputationGraph predCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                    //        embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                    //        stateStruct, attStruct, att2Struct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.ScoreOutputPath + ".Test");
                    //predCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQuery = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContextL3G = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQueryL3G = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContextChar = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQueryChar = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainStartPos = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainEndPos = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQuery = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContextL3G = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQueryL3G = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContextChar = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQueryChar = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidStartPos = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidEndPos = null;
            public static string[] ValidIds = null;
            public static List<HashSet<string>> ValidResults = new List<HashSet<string>>();

            public static ItemFreqIndexDictionary wordFreqDict = null;
            public static ItemFreqIndexDictionary l3gFreqDict = null;
            public static ItemFreqIndexDictionary charFreqDict = null;
            static Dictionary<int, List<int>> word2l3g = new Dictionary<int, List<int>>();
            static Dictionary<int, List<int>> word2char = new Dictionary<int, List<int>>();
            public static int L3gSize { get { return l3gFreqDict.ItemDictSize; } }
            public static int WordSize { get { return wordFreqDict.ItemDictSize; } }
            public static int CharSize { get { return charFreqDict.ItemDictSize; } }

            public static int MaxQueryLen = 0;
            public static int MaxDocLen = 0;
            public static int MaxCrossQDLen = 0;
            static IEnumerable<Tuple<string, string, string>> ExtractQAS(string paraFile, string queryFile, string ansFile)
            {
                using (StreamReader paraReader = new StreamReader(paraFile))
                using (StreamReader queryReader = new StreamReader(queryFile))
                using (StreamReader ansReader = new StreamReader(ansFile))
                {
                    int lineIdx = 0;
                    while (!ansReader.EndOfStream)
                    {
                        string p = paraReader.ReadLine();
                        string q = queryReader.ReadLine();
                        string a = ansReader.ReadLine();
                        //Console.WriteLine("{0},{1}", p, q);
                        yield return new Tuple<string, string, string>(p, q, a);
                        lineIdx += 1;
                    }
                    Console.WriteLine("Total Number of answers {0}", lineIdx);
                }
            }

            static Tuple<int, int> GetSpan(List<string[]> sentenceIdxs, int startSent, int startIdx, int endSent, int endIdx)
            {
                if (startSent == -1 || startIdx == -1 || endSent == -1 || endIdx == -1)
                {
                    return new Tuple<int, int>(-1, -1);
                }
                if (sentenceIdxs.Count <= startSent || sentenceIdxs.Count <= endSent)
                {
                    Console.WriteLine("Sentence number is not enough! {0}, start {1}, end {2}", sentenceIdxs.Count, startSent, endSent);
                    Console.ReadLine();
                }

                if (startSent != endSent)
                {
                    Console.WriteLine("Sentence start is not equals to Sentence end! {0} and {1}", startSent, endSent);
                }

                int answerSentStart = sentenceIdxs.Take(startSent).Sum(i => i.Length) + startIdx;
                int answerSentEnd = sentenceIdxs.Take(endSent).Sum(i => i.Length) + endIdx;

                return new Tuple<int, int>(answerSentStart, answerSentEnd);
            }

            static void ExtractCorpusBinary(string paraFile, string queryFile, string ansFile, string paraFileBin, string queryFileBin, string ansFileBin, bool isShuffle, int miniBatchSize,
                int spanSize, List<HashSet<string>> resultSave)
            {
                SeqSparseBatchData context = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData query = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData contextL3g = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = L3gSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData queryL3g = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = L3gSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData contextChar = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = CharSize }, DeviceType.CPU);
                SeqSparseBatchData queryChar = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = CharSize }, DeviceType.CPU);

                DenseBatchData ansStart = new DenseBatchData(new DenseDataStat() { Dim = 1 }, DeviceType.CPU);
                DenseBatchData ansEnd = new DenseBatchData(new DenseDataStat() { Dim = 1 }, DeviceType.CPU);

                BinaryWriter contextWriter = FileUtil.CreateBinaryWrite(paraFileBin);
                BinaryWriter queryWriter = FileUtil.CreateBinaryWrite(queryFileBin);
                BinaryWriter contextL3GWriter = FileUtil.CreateBinaryWrite(paraFileBin + ".l3g");
                BinaryWriter queryL3GWriter = FileUtil.CreateBinaryWrite(queryFileBin + ".l3g");
                BinaryWriter contextCharWriter = FileUtil.CreateBinaryWrite(paraFileBin + ".char");
                BinaryWriter queryCharWriter = FileUtil.CreateBinaryWrite(queryFileBin + ".char");
                BinaryWriter ansStartWriter = FileUtil.CreateBinaryWrite(ansFileBin + ".1");
                BinaryWriter ansEndWriter = FileUtil.CreateBinaryWrite(ansFileBin + ".2");

                IEnumerable<Tuple<string, string, string>> QAS = ExtractQAS(paraFile, queryFile, ansFile);

                IEnumerable<Tuple<string, string, string>> Input = QAS;
                if (isShuffle) Input = CommonExtractor.RandomShuffle<Tuple<string, string, string>>(QAS, 1000000, DeepNet.BuilderParameters.RandomSeed);

                int crossLen = 0;
                int lineIdx = 0;
                foreach (Tuple<string, string, string> smp in Input)
                {
                    string p = smp.Item1;
                    string q = smp.Item2;
                    string a = smp.Item3;

                    List<string[]> sentenceIdxs = p.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries).
                        Select(i => i.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)).ToList();
                    string[] queryIdxs = q.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    List<Dictionary<int, float>> contextFea = new List<Dictionary<int, float>>();
                    List<Dictionary<int, float>> contextL3GFea = new List<Dictionary<int, float>>();
                    foreach (string[] term in sentenceIdxs)
                    {
                        foreach (string id in term)
                        {
                            int termIdx = int.Parse(id);
                            var tmp = new Dictionary<int, float>();
                            tmp[termIdx] = 1;
                            contextFea.Add(tmp);
                            var l3gtmp = new Dictionary<int, float>();
                            foreach (int l3gid in word2l3g[termIdx])
                            {
                                if (l3gtmp.ContainsKey(l3gid))
                                {
                                    l3gtmp[l3gid] += 1;
                                }
                                else
                                {
                                    l3gtmp.Add(l3gid, 1);
                                }
                            }
                            contextL3GFea.Add(l3gtmp);

                            List<Dictionary<int, float>> contextCharFea = new List<Dictionary<int, float>>();
                            foreach(int cid in word2char[termIdx])
                            {
                                var chartmp = new Dictionary<int, float>();
                                chartmp[cid] = 1;
                                contextCharFea.Add(chartmp);
                            }
                            contextChar.PushSample(contextCharFea);
                        }
                    }

                    List<Dictionary<int, float>> queryFea = new List<Dictionary<int, float>>();
                    List<Dictionary<int, float>> queryL3GFea = new List<Dictionary<int, float>>();
                    foreach (string term in queryIdxs)
                    {
                        int termIdx = int.Parse(term);
                        var tmp = new Dictionary<int, float>();
                        tmp[termIdx] = 1;
                        queryFea.Add(tmp);
                        var l3gtmp = new Dictionary<int, float>();
                        foreach (int l3gid in word2l3g[termIdx])
                        {
                            if (l3gtmp.ContainsKey(l3gid))
                            {
                                l3gtmp[l3gid] += 1;
                            }
                            else
                            {
                                l3gtmp.Add(l3gid, 1);
                            }
                        }
                        queryL3GFea.Add(l3gtmp);

                        List<Dictionary<int, float>> queryCharFea = new List<Dictionary<int, float>>();
                        foreach (int cid in word2char[termIdx])
                        {
                            var chartmp = new Dictionary<int, float>();
                            chartmp[cid] = 1;
                            queryCharFea.Add(chartmp);
                        }
                        queryChar.PushSample(queryCharFea);
                    }
                    
                    string[] poses = a.Split(new char[] { ',', ')', '(', '#' }, StringSplitOptions.RemoveEmptyEntries);

                    int answerSentStart = -1;
                    int answerSentEnd = -1;
                    if (resultSave != null) { resultSave.Add(new HashSet<string>()); }
                    for (int m = 0; m < poses.Length / 4; m++)
                    {
                        int startSent = int.Parse(poses[4 * m + 0]);
                        int startIdx = int.Parse(poses[4 * m + 1]);
                        int endSent = int.Parse(poses[4 * m + 2]);
                        int endIdx = int.Parse(poses[4 * m + 3]);
                        Tuple<int, int> r = GetSpan(sentenceIdxs, startSent, startIdx, endSent, endIdx);
                        answerSentStart = r.Item1;
                        answerSentEnd = r.Item2;

                        if (resultSave != null) { resultSave[lineIdx].Add(string.Format("{0}#{1}", answerSentStart, answerSentEnd)); }
                    }

                    if (spanSize > 0 && answerSentEnd - answerSentStart + 1 > spanSize) continue;
                    if (resultSave == null && (answerSentEnd == -1 || answerSentStart == -1)) continue;

                    float[] tmpAnsStart = new float[contextFea.Count];
                    float[] tmpAnsEnd = new float[contextFea.Count];
                    for (int i = 0; i < contextFea.Count; i++)
                    {
                        tmpAnsStart[i] = 0;
                        tmpAnsEnd[i] = 0;
                        if (i == answerSentStart) { tmpAnsStart[i] = 1; }
                        if (i == answerSentEnd) { tmpAnsEnd[i] = 1; }
                    }

                    if (contextFea.Count > MaxDocLen) { MaxDocLen = contextFea.Count; }
                    if (queryFea.Count > MaxQueryLen) { MaxQueryLen = queryFea.Count; }
                    crossLen += contextFea.Count * queryFea.Count;
                    if (crossLen > MaxCrossQDLen) { MaxCrossQDLen = crossLen; }

                    context.PushSample(contextFea);
                    query.PushSample(queryFea);
                    contextL3g.PushSample(contextL3GFea);
                    queryL3g.PushSample(queryL3GFea);
                    ansStart.PushSample(tmpAnsStart, contextFea.Count);
                    ansEnd.PushSample(tmpAnsEnd, contextFea.Count);
                    if (answerSentEnd < answerSentStart)
                    {
                        Console.WriteLine("answer sent end smaller than answer sent start {0} {1} {2} ### {3}", lineIdx, answerSentEnd, answerSentStart, string.Join("##", poses));
                        Console.ReadLine();
                    }
                    if (context.BatchSize >= miniBatchSize)
                    {
                        context.PopBatchToStat(contextWriter);
                        query.PopBatchToStat(queryWriter);
                        contextL3g.PopBatchToStat(contextL3GWriter);
                        queryL3g.PopBatchToStat(queryL3GWriter);
                        contextChar.PopBatchToStat(contextCharWriter);
                        queryChar.PopBatchToStat(queryCharWriter);
                        ansStart.PopBatchToStat(ansStartWriter);
                        ansEnd.PopBatchToStat(ansEndWriter);
                        crossLen = 0;
                    }

                    if (++lineIdx % 1000 == 0) { Console.WriteLine("Extract Binary from Corpus {0}", lineIdx); }
                }
                context.PopBatchCompleteStat(contextWriter);
                query.PopBatchCompleteStat(queryWriter);
                contextL3g.PopBatchCompleteStat(contextL3GWriter);
                queryL3g.PopBatchCompleteStat(queryL3GWriter);
                contextChar.PopBatchCompleteStat(contextCharWriter);
                queryChar.PopBatchCompleteStat(queryCharWriter);
                ansStart.PopBatchCompleteStat(ansStartWriter);
                ansEnd.PopBatchCompleteStat(ansEndWriter);

                if (resultSave != null)
                {
                    using (StreamWriter ansWriter = new StreamWriter(ansFile + ".index"))
                    {
                        foreach (HashSet<string> results in resultSave)
                        {
                            ansWriter.WriteLine(string.Join("\t", results.ToArray()));
                        }
                    }
                }
                Console.WriteLine("Context Stat {0}", context.Stat.ToString());
                Console.WriteLine("Query Stat {0}", query.Stat.ToString());
                Console.WriteLine("Context l3g Stat {0}", contextL3g.Stat.ToString());
                Console.WriteLine("Query l3g Stat {0}", queryL3g.Stat.ToString());
                Console.WriteLine("Context char Stat {0}", contextChar.Stat.ToString());
                Console.WriteLine("Query char Stat {0}", queryChar.Stat.ToString());
                Console.WriteLine("Answer Start Stat {0}", ansStart.Stat.ToString());
                Console.WriteLine("Answer End Stat {0}", ansEnd.Stat.ToString());
                Console.WriteLine("Max Q Len {0}, Max D Len {1}, Max QD Cross Len {2}", MaxQueryLen, MaxDocLen, MaxCrossQDLen);
            }



            public static void Init()
            {
                #region Preprocess Data.
                // Step 1 : Load Word Vocab .
                {
                    using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab))
                    {
                        wordFreqDict = new ItemFreqIndexDictionary(mreader, false);
                    }
                }

                SimpleTextTokenizer textTokenize = new SimpleTextTokenizer();
                
                // Step 2 : Extract L3G vocab from word vocab.
                l3gFreqDict = new ItemFreqIndexDictionary("l3g dictionary");
                foreach (KeyValuePair<string, int> wordItem in wordFreqDict.ItemIndex.NameDict)
                {
                    int wordIdx = wordItem.Value;
                    string wordStr = wordItem.Key;
                    word2l3g.Add(wordIdx, new List<int>());
                    IEnumerable<string> strList = textTokenize.LnGTokenize(wordStr, 3);
                    foreach (string ngram in strList)
                    {
                        int ngramIdx = l3gFreqDict.ItemIndex.Index(ngram, true);
                        word2l3g[wordIdx].Add(ngramIdx);
                    }
                }
                Console.WriteLine("l3g vocab size {0}", l3gFreqDict.ItemDictSize);

                // Step 3 : Extract char vocab from word vocab.
                charFreqDict = new ItemFreqIndexDictionary("char dictionary");
                foreach (KeyValuePair<string, int> wordItem in wordFreqDict.ItemIndex.NameDict)
                {
                    int wordIdx = wordItem.Value;
                    string wordStr = wordItem.Key;
                    word2char.Add(wordIdx, new List<int>());
                    IEnumerable<string> strList = textTokenize.LetterTokenize(wordStr);
                    foreach (string c in strList)
                    {
                        int cIdx = charFreqDict.ItemIndex.Index(c, true);
                        word2char[wordIdx].Add(cIdx);
                    }
                }
                Console.WriteLine("char vocab size {0}", charFreqDict.ItemDictSize);
                //Console.ReadLine();

                // Step 4 : Index 2 Binary Data.
                if (BuilderParameters.IsTrainFile)
                //&& 
                //!FileUtil.IsFileinUse(new FileInfo(BuilderParameters.TrainQueryBin)) &&
                //!FileUtil.IsFileinUse(new FileInfo(BuilderParameters.TrainContextBin)))
                {
                    ExtractCorpusBinary(
                        BuilderParameters.TrainContext, BuilderParameters.TrainQuery, BuilderParameters.TrainPos,
                        BuilderParameters.TrainContextBin, BuilderParameters.TrainQueryBin, BuilderParameters.TrainPosBin,
                        true, BuilderParameters.MiniBatchSize, BuilderParameters.SPAN_LENGTH, null);
                }

                if (BuilderParameters.IsValidFile)
                //&&
                //!FileUtil.IsFileinUse(new FileInfo(BuilderParameters.ValidQueryBin)) &&
                //!FileUtil.IsFileinUse(new FileInfo(BuilderParameters.ValidContextBin)))
                {
                    ExtractCorpusBinary(
                        BuilderParameters.ValidContext, BuilderParameters.ValidQuery, BuilderParameters.ValidPos,
                        BuilderParameters.ValidContextBin, BuilderParameters.ValidQueryBin, BuilderParameters.ValidPosBin,
                        false, BuilderParameters.MiniBatchSize, 0, ValidResults);
                }
                else
                {
                    using (StreamReader ansReader = new StreamReader(BuilderParameters.ValidPos + ".index"))
                    {
                        while (!ansReader.EndOfStream)
                        {
                            string[] items = ansReader.ReadLine().Split('\t');
                            ValidResults.Add(new HashSet<string>(items));
                        }
                    }
                }
                ValidIds = File.ReadAllLines(BuilderParameters.ValidIds);
                #endregion.

                #region Load Data.
                if (BuilderParameters.IsTrainFile)
                {
                    TrainContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin);
                    TrainQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin);
                    TrainStartPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainPosBin + ".1");
                    TrainEndPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainPosBin + ".2");
                    TrainContextL3G = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin + ".l3g");
                    TrainQueryL3G = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin + ".l3g");
                    TrainContextChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin + ".char");
                    TrainQueryChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin + ".char");

                    TrainContext.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQuery.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainContextL3G.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQueryL3G.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainContextChar.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQueryChar.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainStartPos.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainEndPos.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                }

                if (BuilderParameters.IsValidFile)
                {
                    ValidContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin);
                    ValidQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin);
                    ValidStartPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidPosBin + ".1");
                    ValidEndPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidPosBin + ".2");
                    ValidContextL3G = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin + ".l3g");
                    ValidQueryL3G = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin + ".l3g");
                    ValidContextChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin + ".char");
                    ValidQueryChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin + ".char");

                    ValidContext.InitThreadSafePipelineCashier(64, false);
                    ValidQuery.InitThreadSafePipelineCashier(64, false);
                    ValidContextL3G.InitThreadSafePipelineCashier(64, false);
                    ValidQueryL3G.InitThreadSafePipelineCashier(64, false);
                    ValidContextChar.InitThreadSafePipelineCashier(64, false);
                    ValidQueryChar.InitThreadSafePipelineCashier(64, false);
                    ValidStartPos.InitThreadSafePipelineCashier(64, false);
                    ValidEndPos.InitThreadSafePipelineCashier(64, false);
                }
                #endregion.
            }
        }
    }
}
