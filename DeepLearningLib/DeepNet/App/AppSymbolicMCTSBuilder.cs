﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;

namespace BigLearn.DeepNet
{

    public class AppSymbolicMCTSBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID."));

                Argument.Add("QUERY-SET", new ParameterArgument("100000", "Query Set."));
                Argument.Add("CONTAIN-LIM", new ParameterArgument("20", "Contain Lim."));
                Argument.Add("MIN-CONTAIN-LIM", new ParameterArgument("1", "Contain Lim."));

                Argument.Add("CONTAIN-NUM", new ParameterArgument("3", "Contain Num."));
                Argument.Add("MAX-SEARCH-LEN", new ParameterArgument("10", "Contain Lim."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Mini Batch Size"));

                Argument.Add("SCHEDULE-EPSILON", new ParameterArgument("0:1,3000000:0.1,10000000:0.001,50000000:0.000001", "Epsilon Schedule"));

                Argument.Add("RECURRENT-T", new ParameterArgument("10,10,1", "Recurrent Terminate Net."));

                Argument.Add("EMBED-DIM", new ParameterArgument("32", "memory size"));
                Argument.Add("STATUS-DIM", new ParameterArgument("128", "memory size"));

                Argument.Add("MEM-ATT-GAMMA", new ParameterArgument("10", " attention gamma"));
                Argument.Add("MEM-ATT-HIDDEN", new ParameterArgument("128", " attention hidden size."));
                Argument.Add("MEM-ATT-TYPE", new ParameterArgument("1", "0:inner product; 1: weighted inner product;"));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("10", "Reasoning Steps."));

                Argument.Add("INIT-STATUS", new ParameterArgument("0", "0:Fix Init Status; 1: Random Init Status;"));

                //Argument.Add("ENSEMBLE-RL", new ParameterArgument("0", "0:max terminate prob; 1:average terminate prob."));
                Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                //Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

                Argument.Add("MIN-BFS-DEPTH", new ParameterArgument("0", "Minimum BFS depth."));
                Argument.Add("MAX-BFS-SEARCH", new ParameterArgument("1000000", "Minimum BFS depth."));
                Argument.Add("MAX-MC-SEARCH", new ParameterArgument("1000", "Minimum BFS depth."));

                Argument.Add("C-QUERY", new ParameterArgument(string.Empty, "check query path"));
            }

            public static int QUERY_SET { get { return int.Parse(Argument["QUERY-SET"].Value); } }
            public static int CONTAIN_LIM { get { return int.Parse(Argument["CONTAIN-LIM"].Value); } }
            public static int CONTAIN_NUM { get { return int.Parse(Argument["CONTAIN-NUM"].Value); } }
            public static int MAX_SEARCH_LEN { get { return int.Parse(Argument["MAX-SEARCH-LEN"].Value); } }

            public static string C_QUERY { get { return Argument["C-QUERY"].Value; } }

            public static string QUERY_PATH { get { return string.Format("Q.{0}.{1}.{2}.{3}.set", QUERY_SET, CONTAIN_LIM, MAX_SEARCH_LEN, CONTAIN_NUM); } }

            public static List<Tuple<int, float>> ScheduleEpsilon
            {
                get
                {
                    return Argument["SCHEDULE-EPSILON"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }
            public static float Epsilon(int step)
            {
                for (int i = 0; i < ScheduleEpsilon.Count; i++)
                {
                    if (step < ScheduleEpsilon[i].Item1)
                    {
                        float lambda = (step - ScheduleEpsilon[i - 1].Item1) * 1.0f / (ScheduleEpsilon[i].Item1 - ScheduleEpsilon[i - 1].Item1);
                        return lambda * ScheduleEpsilon[i].Item2 + (1 - lambda) * ScheduleEpsilon[i - 1].Item2;
                    }
                }
                return ScheduleEpsilon.Last().Item2;
            }



            public static int Recurrent_Step { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }

            public static int MIN_BFS_DEPTH { get { return int.Parse(Argument["MIN-BFS-DEPTH"].Value); } }
            public static int MAX_BFS_SEARCH { get { return int.Parse(Argument["MAX-BFS-SEARCH"].Value); } }
            public static int MAX_MC_SEARCH { get { return int.Parse(Argument["MAX-MC-SEARCH"].Value); } }
            public static int MIN_CONTAIN_LIM { get { return int.Parse(Argument["MIN-CONTAIN-LIM"].Value); } }

        }

        public override BuilderType Type { get { return BuilderType.APP_SYMBOLIC_MCTS; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }


        //public static ComputationGraph BuildActorGraph(string queryPath, DNNStructure stateDNN, DNNStructure tgtDNN, DNNStructure scoreDNN,
        //    GRUCell gru, DNNStructure vDNN, RunnerBehavior behavior)
        //{
        //    ComputationGraph cg = new ComputationGraph();

        //    SymbolicDataRunner dataRunner = new SymbolicDataRunner(BuilderParameters.BatchSize, queryPath, true, behavior) { IsBackProp = false };
        //    cg.AddDataRunner(dataRunner);

        //    List<List<SymbolicState>> query_seq = new List<List<SymbolicState>>();
        //    List<HiddenBatchData> state_seq = new List<HiddenBatchData>();
        //    List<HiddenBatchData> v_seq = new List<HiddenBatchData>();

        //    // Init query and state.
        //    List<SymbolicState> query = dataRunner.OutQueries;
        //    HiddenBatchData state = (HiddenBatchData)cg.AddRunner(new SymbolicStateRunner(query, BuilderParameters.BatchSize, stateDNN, behavior));
        //    HiddenBatchData v = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(vDNN, state, behavior));

        //    query_seq.Add(query);
        //    state_seq.Add(state);
        //    v_seq.Add(v);

        //    for (int i = 0; i < BuilderParameters.Recurrent_Step; i++)
        //    {
        //        StateActorRunner actorRunner = new StateActorRunner(query, state, tgtDNN, scoreDNN, behavior);
        //        cg.AddRunner(actorRunner);
        //        List<SymbolicState> answer = actorRunner.Answer;

        //        HiddenBatchData x = (HiddenBatchData)cg.AddRunner(new SymbolicStateRunner(answer, BuilderParameters.BatchSize, stateDNN, behavior));

        //        GRUQueryRunner queryRewriteRunner = new GRUQueryRunner(gru, state, x, behavior);
        //        cg.AddRunner(queryRewriteRunner);
        //        HiddenBatchData newState = queryRewriteRunner.Output;

        //        HiddenBatchData newV = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(vDNN, newState, behavior));

        //        state = newState;
        //        query = answer;
        //        v = newV;

        //        query_seq.Add(query);
        //        state_seq.Add(state);
        //        v_seq.Add(v);
        //    }

        //    //float lambda = BuilderParameters.RL_DISCOUNT;
        //    //for (int i = BuilderParameters.Recurrent_Step - 1; i >= 0; i--)
        //    //{
        //    //    rewards_seq[i]
        //    //}

        //    return cg;
        //}

        class RewardVDerivRunner : StructRunner
        {
            List<List<SymbolicState>> QuerySeq;
            List<HiddenBatchData> StateSeq;
            List<HiddenBatchData> VSeq;

            public RewardVDerivRunner(List<List<SymbolicState>> query_seq, List<HiddenBatchData> state_seq, List<HiddenBatchData> v_seq, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                QuerySeq = query_seq;
                StateSeq = state_seq;
                VSeq = v_seq;
            }

            //unsafe public override void Forward()
            //{
            //    Output.BatchSize = Queries.Count;
            //    for (int i = 0; i < Queries.Count; i++)
            //    {
            //        Array.Copy(Queries[i].Fea, 0, XVec.Data.MemPtr, i * SymbolicState.StateDim, SymbolicState.StateDim);
            //    }
            //    XVec.Data.SyncFromCPU();
            //    Runner.Forward();
            //}

            //public override void Backward(bool cleanDeriv)
            //{
            //    Runner.Backward(cleanDeriv);
            //}

            //public override void CleanDeriv()
            //{
            //    Runner.CleanDeriv();
            //}

            //public override void Update()
            //{
            //    Runner.Update();
            //}
        }

        public class Actor
        {
            //return null, indicates termination.
            public virtual SymbolicState Action(SymbolicState s)
            {
                throw new NotImplementedException();
            }

            public virtual void Feedback(List<SymbolicState> path, float reward)
            {
                throw new NotImplementedException();
            }
        }

        public class RandomActor : Actor
        {
            Random random = new Random();
            public RandomActor() { }
            public override SymbolicState Action(SymbolicState s)
            {
                List<SymbolicState> results = s.NeighborStates();
                int idx = random.Next(results.Count);
                return results[idx];
            }
            public override void Feedback(List<SymbolicState> path, float reward)
            { }
        }

        public static int UCBBandit(List<Tuple<float, float>> arms, float c, Random random)
        {
            float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);
            List<float> v = new List<float>();
            foreach (Tuple<float, float> arm in arms)
            {
                v.Add(arm.Item1 / (arm.Item2 + 0.1f) + c * (float)Math.Sqrt(log_total / (arm.Item2 + 0.1f)) + (float)random.NextDouble() * 0.0001f);
            }
            int idx = Util.MaximumValue(v.ToArray());
            return idx;
        }

        /// <summary>
        /// Monte Carlo Tree Search.
        /// </summary>
        public class MonteCarloActor : Actor
        {
            public Dictionary<string, Tuple<float, float>> Mem = new Dictionary<string, Tuple<float, float>>();
            Random random = new Random();
            public float c = 1.0f;

            public override SymbolicState Action(SymbolicState s)
            {
                List<SymbolicState> neighbors = s.NeighborStates();
                List<Tuple<float, float>> arms = new List<Tuple<float, float>>();
                int nIdx = 0;
                foreach (SymbolicState n in neighbors)
                {
                    arms.Add(LookUp(n.StateKey));
                    nIdx += 1;
                }
                arms.Add(LookUp(s.StateKey + ":T"));

                int idx = UCBBandit(arms, c, random);
                if (idx >= neighbors.Count)
                    return null;
                return neighbors[idx];
            }

            public Tuple<float, float> LookUp(string key)
            {
                if (!Mem.ContainsKey(key))
                {
                    Mem.Add(key, new Tuple<float, float>(0, 0));
                }
                return Mem[key];
            }

            public void Write(string key, float reward)
            {
                Mem[key] = new Tuple<float, float>(Mem[key].Item1 + reward, Mem[key].Item2 + 1);
            }

            public override void Feedback(List<SymbolicState> path, float reward)
            {
                int num = path.Count;
                if (path[num - 1] == path[num - 2])
                {
                    Write(path[num - 1].StateKey + ":T", reward);
                    num = num - 1;
                }

                for (int i = 1; i < num; i++)
                {
                    Write(path[i].StateKey, reward);
                }
            }

            public Tuple<int, List<SymbolicState>> Search(SymbolicState q, int maxTimes, int searchDepth)
            {
                //Tuple<int, int> result = new Tuple<int, int>();
                bool IsSuccess = false;
                int successDepth = -1;
                int epoch = 0;
                for (epoch = 0; epoch < maxTimes; epoch++)
                {
                    SymbolicState state = q;
                    List<SymbolicState> path = new List<SymbolicState>();
                    
                    for (int i = 0; i < searchDepth; i++)
                    {
                        state = Action(state);
                        path.Add(state);
                        IsSuccess = state.IsSuccess;
                        if (IsSuccess) { break; }
                    }

                    int reward = 0;
                    if (IsSuccess) { reward = 1; }

                    Feedback(path, reward);
                    if (IsSuccess)
                    {
                        successDepth = path.Count;
                        return new Tuple<int, List<SymbolicState>>(epoch, path);
                    }
                }
                return new Tuple<int, List<SymbolicState>>(-1, null);
            }
        }

        /// <summary>
        /// Differentiable neural dictionary.
        /// </summary>
        public class DND
        {
            int KeyDim { get; set; }
            int TopK = 50;
            public DND(int keyDim)
            {
                KeyDim = keyDim;
            }

            List<Tuple<float[], float>> NeuralDictionary = new List<Tuple<float[], float>>();

            float GaussianKernel(float[] key1, float[] key2)
            {
                float d = 0;
                for (int i = 0; i < key1.Length; i++)
                {
                    d = (key1[i] - key2[i]) * (key1[i] - key2[i]);
                }
                return 1.0f / (d + 0.001f);
            }

            public float Lookup(float[] key)
            {
                MinMaxHeap<int> heap = new MinMaxHeap<int>(TopK, 1);
                for (int i = 0; i < NeuralDictionary.Count; i++)
                {
                    float v = GaussianKernel(key, NeuralDictionary[i].Item1);
                    heap.push_pair(i, v);
                }
                float sum_k = heap.topK.Select(i => i.Value).Sum();
                float sum_v = 0;
                foreach (KeyValuePair<int, float> item in heap.topK)
                {
                    sum_v += item.Value / sum_k * NeuralDictionary[item.Key].Item2;
                }
                return sum_v;
            }

            public float Write(float[] key, float v)
            {
                return 0;
            }

            //public void Lookup()
        }

        /// <summary>
        /// Neural Episodic Control.
        /// </summary>
        public class NeuralEpisodicActor : Actor
        {
            Dictionary<string, Tuple<float, float>> Mem = new Dictionary<string, Tuple<float, float>>();
            Random random = new Random();
            float c = 1.0f;
            float lr = 0.5f;
            int poscorrect = 0;
            int negcorrect = 0;
            int poswrong = 0;
            int negwrong = 0;
            bool CanSimulate = false;
            // Tomson Sampling.
            // Q_t = \lambda Q_{t+1} + r_t
            // 
            // variance = sqrt( (x-v)^2 ) 

            RunnerBehavior Behavior = null;
            DNNStructure Model = null;
            DNNRunner<HiddenBatchData> Runner = null;
            HiddenBatchData Input = null;

            ComputationGraph TrainCG = null;
            HiddenBatchData TrainInput = null;
            HiddenBatchData TrainOutput = null;
            DenseBatchData TrainLabel = null;
            int BatchSize = 1024;

            public NeuralEpisodicActor(RunnerBehavior rb)
            {
                Behavior = rb;
                Model = new DNNStructure(SymbolicState.Fea2Dim, new int[] { 2 },
                    new A_Func[] { A_Func.Linear }, new bool[] { true }, Behavior.Device);
                Model.Init(0.02f, -0.01f);
                //Model.neurallinks[0].weight.MemPtr[0] = -100;
                //Model.neurallinks[0].weight.MemPtr[1] = 100;

                //Model.neurallinks[0].weight.MemPtr[2] = -100;
                //Model.neurallinks[0].weight.MemPtr[3] = 100;

                //Model.neurallinks[0].weight.MemPtr[4] = -100;
                //Model.neurallinks[0].weight.MemPtr[5] = 100;

                //Model.neurallinks[0].weight.SyncFromCPU();

                //Model.neurallinks[0].bias.MemPtr[0] = 100;
                //Model.neurallinks[0].bias.MemPtr[1] = 0;
                //Model.neurallinks[0].bias.SyncFromCPU();



                Input = new HiddenBatchData(1, SymbolicState.Fea2Dim, new CudaPieceFloat(SymbolicState.Fea2Dim, Behavior.Device), null, Behavior.Device);
                Runner = new DNNRunner<HiddenBatchData>(Model, Input, Behavior);

                TrainInput = new HiddenBatchData(BatchSize, SymbolicState.Fea2Dim, new CudaPieceFloat(SymbolicState.Fea2Dim * BatchSize, Behavior.Device), null, Behavior.Device);
                TrainLabel = new DenseBatchData(BatchSize, 1, Behavior.Device);
                Model.InitOptimizer(OptimizerParameters.StructureOptimizer, Behavior);

                TrainCG = new ComputationGraph();
                /*************** DNN for Source Input. ********************/
                TrainOutput = (HiddenBatchData)TrainCG.AddRunner(new DNNRunner<GeneralBatchInputData>(Model, TrainInput, Behavior));
                //TrainCG.AddObjective(new MSERunner(TrainOutput.Output, TrainLabel, CudaPieceFloat.Empty, TrainOutput.Deriv, Behavior));
                //TrainCG.AddObjective(new CrossEntropyRunner(TrainLabel.Data, TrainOutput.Output, TrainOutput.Deriv, 1, Behavior));
                TrainCG.AddObjective(new MultiClassSoftmaxRunner(TrainLabel.Data, TrainOutput.Output, TrainOutput.Deriv, 1, Behavior));

                TrainCG.SetDelegateModel(Model);

            }

            bool RandomT(float t)
            {
                if (t > 0.5f) { return true; }
                else return false;

                //if (random.NextDouble() < t) { return true; }
                //return false;
            }

            public float ComputeTQ(SymbolicState s)
            {
                float[] fea = s.Fea2;
                for (int i = 0; i < fea.Length; i++)
                {
                    Input.Output.Data.MemPtr[i] = fea[i];
                }
                Input.BatchSize = 1;
                Input.Output.Data.SyncFromCPU();
                Runner.Forward();
                Runner.Output.Output.Data.SyncToCPU();
                float[] h = Runner.Output.Output.Data.MemPtr;
                h = Util.Softmax(h, 1);
                return h[1]; // Util.Logistic(h[0]);
            }

            int simulate = 50;
            int depth = 12;

            public SymbolicState Next(SymbolicState s)
            {
                List<SymbolicState> neighbors = s.NeighborStates();
                List<Tuple<float, float>> arms = new List<Tuple<float, float>>();
                int nIdx = 0;
                foreach (SymbolicState n in neighbors)
                {
                    arms.Add(LookUp(n.StateKey));
                    nIdx += 1;
                }

                int idx = UCBBandit(arms, c, random);
                return neighbors[idx];
            }

            public override SymbolicState Action(SymbolicState s)
            {
                // simulation.
                if (CanSimulate)
                {
                    for (int e = 0; e < simulate; e++)
                    {
                        List<SymbolicState> path = new List<SymbolicState>();

                        SymbolicState q = s;
                        float log_p = 0;
                        float reward = 0;
                        for (int d = 0; d < depth; d++)
                        {
                            path.Add(q);
                            float r = ComputeTQ(q);
                            if (RandomT(r)) { reward = 1; log_p += (float)Math.Log(poscorrect / (poscorrect + negwrong + 0.001f) + 0.0001f); break; }
                            log_p += (float)Math.Log(negcorrect / (negcorrect + poswrong + 0.001f) + 0.0001f);
                            q = Next(q);
                        }

                        //logP record the variance of the reward.

                        Feedback(path, reward * (float)Math.Exp(log_p) * lr);
                    }
                }

                return Next(s);
            }

            public Tuple<float, float> LookUp(string key)
            {
                if (!Mem.ContainsKey(key))
                {
                    Mem.Add(key, new Tuple<float, float>(0, 0));
                }
                return Mem[key];
            }

            public void Write(string key, float reward)
            {
                Mem[key] = new Tuple<float, float>(Mem[key].Item1 + reward, Mem[key].Item2 + 1);
            }

            public override void Feedback(List<SymbolicState> path, float reward)
            {
                foreach (SymbolicState s in path)
                {
                    Write(s.StateKey, reward);
                }

                //PushReplay(path, reward);
            }

            Queue<Tuple<SymbolicState, float>> ReplayMem = new Queue<Tuple<SymbolicState, float>>();

            public void PushReplay(List<SymbolicState> path, float feedback)
            {
                for (int i = 0; i < path.Count - 1; i++)
                {
                    ReplayMem.Enqueue(new Tuple<SymbolicState, float>(path[i], 0));
                }
                ReplayMem.Enqueue(new Tuple<SymbolicState, float>(path.Last(), feedback));

                if (ReplayMem.Count >= 5 * BatchSize)
                {
                    Model.neurallinks[0].weight.SyncToCPU();
                    Model.neurallinks[0].bias.SyncToCPU();
                    for (int i = 0; i < BatchSize; i++)
                    {
                        int idx = random.Next(ReplayMem.Count);

                        float[] fea = ReplayMem.ElementAt(idx).Item1.Fea2;
                        for (int f = 0; f < SymbolicState.Fea2Dim; f++)
                        {
                            TrainInput.Output.Data.MemPtr[i * SymbolicState.Fea2Dim + f] = fea[f];
                        }

                        TrainLabel.Data.MemPtr[i] = ReplayMem.ElementAt(idx).Item2;

                        //if(TrainLabel.Data.MemPtr[i] > 0)
                        //{
                        //    Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {0}", i);
                        //}
                    }
                    TrainInput.BatchSize = BatchSize;
                    TrainLabel.BatchSize = BatchSize;
                    TrainInput.Output.Data.SyncFromCPU();
                    TrainLabel.Data.SyncFromCPU();
                    TrainCG.Step();
                    double loss = TrainCG.StepLoss;
                    TrainOutput.Output.Data.SyncToCPU();

                    for (int i = 0; i < BatchSize; i++)
                    {
                        float r = Util.Softmax(new float[] { TrainOutput.Output.Data.MemPtr[i * 2 + 0], TrainOutput.Output.Data.MemPtr[i * 2 + 1] }, 1)[1];
                        float reward = RandomT(r) ? 1 : 0;

                        if (TrainLabel.Data.MemPtr[i] > 0)
                        {
                            if (reward > 0) { poscorrect += 1; }
                            else { poswrong += 1; }
                        }
                        else
                        {
                            if (reward <= 0) { negcorrect += 1; }
                            else { negwrong += 1; }
                        }
                    }
                    Console.WriteLine("Pos_Correct {0}, Neg_Correct {1}, Pos_Wrong {2}, Neg_Wrong {3}, Loss {4}", poscorrect, negcorrect, poswrong, negwrong, loss);

                    if (poscorrect / (poscorrect + negwrong + 0.001f) > 0.75f && negcorrect / (negcorrect + poswrong + 0.001f) > 0.75f) { CanSimulate = true; }
                    else { CanSimulate = false; }
                }

                while (ReplayMem.Count >= 100000)
                {
                    ReplayMem.Dequeue();
                }
                /// train model.
            }
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation Data.");

            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            return;
            //Console.SetWindowSize(
            //    Math.Min(150, Console.LargestWindowWidth),
            //    Math.Min(60, Console.LargestWindowHeight));

            //DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            //IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            //RandomActor actor = new RandomActor();
            //Actor actor = new NeuralEpisodicActor(new RunnerBehavior()
            //{ RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }); //MonteCarloActor(); //RandomActor(); // 
            //int monte_carlo_search = 100;
        }

        public void Text1()
        { 
            MonteCarloActor actor = new MonteCarloActor();
            actor.c = 10f ;
            //for (int iter = 0; iter < 10; iter++)
            //{
            //    Console.WriteLine("Iteration {0}", iter);
            List<int> avgMonteCarlo = new List<int>();
            List<int> avgSearchDepth = new List<int>();
            List<int> avgBFSSearch = new List<int>();
            List<int> avgBFSSearchDepth = new List<int>();
            using (StreamWriter mwriter = new StreamWriter(BuilderParameters.C_QUERY + ".success"))
            {
                foreach (SymbolicState q in DataPanel.Query)
                {
                    // maximum recurrent step;
                    bool IsSuccess = false;
                    int epoch = 0;
                    int successDepth = -1;
                    for (epoch = 0; epoch < BuilderParameters.MAX_MC_SEARCH; epoch++)
                    {
                        SymbolicState state = q;
                        List<SymbolicState> path = new List<SymbolicState>();
                        successDepth = -1;
                        IsSuccess = false;

                        path.Add(state);
                        // max hop;
                        for (int i = 0; i < BuilderParameters.Recurrent_Step; i++)
                        {
                            SymbolicState nextState = actor.Action(state);
                            if (nextState != null)
                            {
                                path.Add(nextState);
                            }
                            else
                            {
                                path.Add(state);
                                break;
                            }

                            state = nextState;
                            //IsSuccess = state.IsSuccess;
                            //if (IsSuccess) { break; }
                        }

                        int reward = 0;
                        if (path.Last().IsSuccess) {
                            Console.WriteLine(string.Join("\n", path.Select(i => i.StateKey)));
                            reward = 1; }

                        actor.Feedback(path, reward);
                        //((NeuralEpisodicActor)actor).PushReplay(path, reward);
                        if (path.Last().IsSuccess)
                        {
                            successDepth = path.Count;
                            IsSuccess = true;
                            break;
                        }
                    }
                    actor.Mem.Clear();

                    DataPanel.BFSSearch(q);

                    avgBFSSearch.Add(q.BFS_TIMES);
                    avgBFSSearchDepth.Add(q.BFS_DEPTH);

                    if (!IsSuccess) avgMonteCarlo.Add(-1);
                    else avgMonteCarlo.Add(epoch);

                    if (!IsSuccess) avgSearchDepth.Add(-1);
                    else avgSearchDepth.Add(successDepth);

                    if(IsSuccess)
                    {
                        mwriter.WriteLine(q.SaveKey);
                    }

                    int notComplete = avgMonteCarlo.Select(i => i == -1 ? 1 : 0).Sum();
                    int Complete = avgMonteCarlo.Select(i => i > -1 ? 1 : 0).Sum();
                    int totalLen = avgMonteCarlo.Select(i => i > -1 ? i : 0).Sum();

                    int totalSearchDepth = avgSearchDepth.Select(i => i > -1 ? i : 0).Sum();


                    if (avgBFSSearch.Count % 10 == 0)
                    {
                        Console.WriteLine(
                            "{0} Not Completed, {1} Completed, {2} AvgSearchLen, {3} AvgSearchDepth, {4} Avg BFSSearchLen, {5} Avg BFS SearchDepth, {6} Max MC SearchLen, {7} Max BFS SearchLen {8} Max BFSSearchDpeth",
                            notComplete, Complete, totalLen / (Complete + 0.0001f), totalSearchDepth / (Complete + 0.0001f),
                            avgBFSSearch.Average(), avgBFSSearchDepth.Average(), avgMonteCarlo.Max(),
                            avgBFSSearch.Max(), avgBFSSearchDepth.Max());
                    }
                }
            }
            Logger.CloseLog();
        }

        public class SymbolicState
        {
            public int ContainNum { get; set; }
            public int[] ContainLim { get; set; }
            public int[] ContainStatus { get; set; }
            public int Q;

            public int Step = 0;
            public int BFS_TIMES = 0;
            public int BFS_DEPTH = 0;

            public int DFS_TIMES = 0;
            public int DFS_DEPTH = 0;

            public int MC_TIMES = 0;
            public int MC_DEPTH = 0;


            public string SaveKey { get { return string.Format("{0}#{1}#{2},{3}#{4},{5}", Q, string.Join(",", ContainLim), MC_DEPTH, MC_TIMES, BFS_DEPTH, BFS_TIMES); } }

            public string QueryKey { get { return string.Format("{0}#{1}", Q, string.Join(",", ContainLim)); } }

            public string StateKey { get { return string.Format("{0}#{1}#{2}", Q, string.Join(",", ContainLim), string.Join(",", ContainStatus)); } }

            public static int MaxNeighborNum { get { return 2 * BuilderParameters.CONTAIN_NUM + BuilderParameters.CONTAIN_NUM * (BuilderParameters.CONTAIN_NUM - 1); } }

            public static int FeaDim = BuilderParameters.CONTAIN_NUM * 2 + 1;
            public float[] Fea
            {
                get
                {
                    float[] m = new float[FeaDim];
                    m[0] = Q;
                    Array.Copy(ContainLim, 0, m, 1, ContainNum);
                    Array.Copy(ContainStatus, 0, m, 1 + ContainNum, ContainNum);
                    return m;
                }
            }

            public static int Fea2Dim = BuilderParameters.CONTAIN_NUM;
            public float[] Fea2 { get { return ContainStatus.Select(i => i == Q ? 1.0f : 0.0f).ToArray(); } }

            public SymbolicState() { }

            public SymbolicState(string savekey)
            {
                string[] items = savekey.Split('#');
                Q = int.Parse(items[0]);
                ContainLim = items[1].Split(',').Select(i => int.Parse(i)).ToArray();
                ContainNum = ContainLim.Length;
                ContainStatus = new int[ContainNum];

                MC_DEPTH = int.Parse(items[2].Split(',')[0]);
                MC_TIMES = int.Parse(items[2].Split(',')[1]);

                BFS_DEPTH = int.Parse(items[3].Split(',')[0]);
                BFS_TIMES = int.Parse(items[3].Split(',')[1]);
            }

            public SymbolicState(Random random, HashSet<string> keys)
            {
                while (true)
                {
                    Q = random.Next(1, BuilderParameters.CONTAIN_LIM);
                    ContainNum = BuilderParameters.CONTAIN_NUM;
                    ContainStatus = new int[ContainNum];
                    ContainLim = new int[ContainNum];

                    for (int i = 0; i < ContainNum; i++)
                    {
                        ContainLim[i] = random.Next(1, BuilderParameters.CONTAIN_LIM);
                    }

                    if (keys.Contains(this.QueryKey)) continue;
                    keys.Add(this.QueryKey);

                    if (ContainLim.Where(i => i <= Q).Count() > 0) continue;


                    // BFS search Step;
                    int t = 0;
                    bool isFound = false;
                    HashSet<string> Checked = new HashSet<string>();

                    Queue<SymbolicState> queue = new Queue<SymbolicState>();
                    queue.Enqueue(this);
                    Checked.Add(this.StateKey);
                    while (queue.Count > 0)
                    {
                        SymbolicState s = queue.Dequeue();
                        if (s.IsSuccess )
                        {
                            if (s.Step < BuilderParameters.MIN_BFS_DEPTH)
                                break;
                            this.BFS_DEPTH = s.Step; this.BFS_TIMES = t; isFound = true; break;
                        }

                        if (s.Step < BuilderParameters.MAX_SEARCH_LEN)
                        {
                            List<SymbolicState> neighbors = s.NeighborStates();
                            foreach (SymbolicState ns in neighbors)
                            {
                                if (Checked.Contains(ns.StateKey)) continue;
                                Checked.Add(ns.StateKey);
                                queue.Enqueue(ns);
                            }
                        }
                        t = t + 1;
                        if (t >= BuilderParameters.MAX_BFS_SEARCH) { break; }
                    }

                    if (!isFound) continue;

                    MonteCarloActor actor = new MonteCarloActor();
                    Tuple<int, List<SymbolicState>> r = actor.Search(this, BuilderParameters.MAX_MC_SEARCH, BuilderParameters.MAX_SEARCH_LEN);
                    MC_TIMES = r.Item1;
                    if (MC_TIMES <= 0) continue;
                    MC_DEPTH = r.Item2.Count;

                    break;
                    //isFound = false;

                    ////DFS search Step;
                    //t = 0;
                    //Stack<SymbolicState> stack = new Stack<SymbolicState>();
                    //stack.Push(this);
                    //while (stack.Count > 0)
                    //{
                    //    SymbolicState s = stack.Pop();
                    //    if (s.IsSuccess) { this.DFS_DEPTH = s.Step; this.DFS_TIMES = t; isFound = true; break; }

                    //    if (s.Step < BuilderParameters.MAX_SEARCH_LEN)
                    //    {
                    //        List<SymbolicState> neighbors = s.NeighborStates();
                    //        foreach (SymbolicState ns in neighbors) stack.Push(ns);
                    //    }
                    //    t = t + 1;
                    //    if (t >= 100000) { break; }
                    //}

                    //if (!isFound) continue;

                    //return;
                }
            }

            

            public SymbolicState Transform(int[] s, int step)
            {
                return new SymbolicState() { ContainLim = this.ContainLim, ContainStatus = s, ContainNum = this.ContainNum, Q = Q, Step = step + 1 };
            }

            public bool IsSuccess { get { if (ContainStatus.Where(i => i == Q).Count() > 0) return true; return false; } }

            public List<SymbolicState> NeighborStates()
            {
                List<SymbolicState> results = new List<SymbolicState>();

                for (int c = 0; c < ContainNum; c++)
                {
                    // A -> Full;
                    if (ContainStatus[c] < ContainLim[c])
                    {
                        int[] news = new int[ContainNum];
                        Array.Copy(ContainStatus, news, ContainNum);
                        news[c] = ContainLim[c];

                        results.Add(Transform(news, Step)); 
                    }

                    // A -> empty;
                    if (ContainStatus[c] > 0)
                    {
                        int[] news = new int[ContainNum];
                        Array.Copy(ContainStatus, news, ContainNum);
                        news[c] = 0;
                        results.Add(Transform(news, Step));
                    }
                    // A -> B; A -> C;
                    for (int anoC = 0; anoC < ContainNum; anoC++)
                    {
                        if (c == anoC) continue;

                        if (ContainStatus[c] > 0 && ContainStatus[anoC] < ContainLim[anoC])
                        {
                            int[] news = new int[ContainNum];
                            Array.Copy(ContainStatus, news, ContainNum);

                            if (ContainStatus[c] + ContainStatus[anoC] >= ContainLim[anoC])
                            {
                                news[c] = ContainStatus[c] + ContainStatus[anoC] - ContainLim[anoC];
                                news[anoC] = ContainLim[anoC];
                            }
                            else
                            {
                                news[c] = 0;
                                news[anoC] = ContainStatus[c] + ContainStatus[anoC];
                            }
                            results.Add(Transform(news, Step));
                        }

                    }
                }

                return results;
            }
        }

        public class SymbolicStateRunner : StructRunner
        {
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } protected set { base.Output = value; } }
            List<SymbolicState> Queries = null;
            DNNStructure DNN;
            DenseBatchData XVec;
            DNNRunner<HiddenBatchData> Runner = null;
            public SymbolicStateRunner(List<SymbolicState> input, int maxBatchSize, DNNStructure dnn, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Queries = input;
                XVec = new DenseBatchData(maxBatchSize, SymbolicState.FeaDim, behavior.Device);
                DNN = dnn;
                Runner = new DNNRunner<HiddenBatchData>(DNN, new HiddenBatchData(maxBatchSize, XVec.Stat.Dim, XVec.Data, null, Behavior.Device), Behavior);
                Output = Runner.Output;
            }

            unsafe public override void Forward()
            {
                Output.BatchSize = Queries.Count;
                for (int i = 0; i < Queries.Count; i++)
                {
                    Array.Copy(Queries[i].Fea, 0, XVec.Data.MemPtr, i * SymbolicState.FeaDim, SymbolicState.FeaDim);
                }
                XVec.Data.SyncFromCPU();
                Runner.Forward();
            }

            public override void Backward(bool cleanDeriv)
            {
                Runner.Backward(cleanDeriv);
            }

            public override void CleanDeriv()
            {
                Runner.CleanDeriv();
            }

            public override void Update()
            {
                Runner.Update();
            }
        }


        

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static List<SymbolicState> Query = new List<SymbolicState>();
            public static void RandomQuery()
            {
                Random random = new Random();
                HashSet<string> keys = new HashSet<string>();
                using (StreamWriter mwriter = new StreamWriter(BuilderParameters.QUERY_PATH))
                {
                    int BFS_SearchDepth = 0;
                    int BFS_SearchTimes = 0;
                    int DFS_SearchDepth = 0;
                    int DFS_SearchTimes = 0;
                    int MAX_BFS_TIMES = 0;
                    int MAX_DFS_TIMES = 0;
                    int MAX_MC_TIMES = 0;

                    int MC_SearchDepth = 0;
                    int MC_SearchTimes = 0;

                    for (int i = 0; i < BuilderParameters.QUERY_SET; i++)
                    {
                        SymbolicState s = new SymbolicState(random, keys);
                        Query.Add(s);
                        if (i % 10 == 0) { Console.WriteLine(string.Format("Search Examples {0}", i)); }
                        keys.Add(s.QueryKey);

                        BFS_SearchDepth += s.BFS_DEPTH;
                        BFS_SearchTimes += s.BFS_TIMES;
                        DFS_SearchDepth += s.DFS_DEPTH;
                        DFS_SearchTimes += s.DFS_TIMES;

                        MC_SearchDepth += s.MC_DEPTH;
                        MC_SearchTimes += s.MC_TIMES;

                        mwriter.WriteLine(s.SaveKey);
                        if (s.BFS_TIMES > MAX_BFS_TIMES) MAX_BFS_TIMES = s.BFS_TIMES;
                        if (s.DFS_TIMES > MAX_DFS_TIMES) MAX_DFS_TIMES = s.DFS_TIMES;
                        if (s.MC_TIMES > MAX_MC_TIMES) MAX_MC_TIMES = s.MC_TIMES;

                    }
                    mwriter.Close();
                    Logger.WriteLog("Avg BFS Depth {0}", BFS_SearchDepth * 1.0f / BuilderParameters.QUERY_SET);
                    Logger.WriteLog("Avg DFS Depth {0}", DFS_SearchDepth * 1.0f / BuilderParameters.QUERY_SET);
                    Logger.WriteLog("Avg MC Depth {0}", MC_SearchDepth * 1.0f / BuilderParameters.QUERY_SET);

                    Logger.WriteLog("Avg BFS Times {0}", BFS_SearchTimes * 1.0f / BuilderParameters.QUERY_SET);
                    Logger.WriteLog("Avg DFS Times {0}", DFS_SearchTimes * 1.0f / BuilderParameters.QUERY_SET);
                    Logger.WriteLog("Avg MC Times {0}", MC_SearchTimes * 1.0f / BuilderParameters.QUERY_SET);

                    Logger.WriteLog("Max BFS Times {0}", MAX_BFS_TIMES);
                    Logger.WriteLog("Max DFS Times {0}", MAX_DFS_TIMES);
                    Logger.WriteLog("Max MC Times {0}", MAX_MC_TIMES);
                }
            }

            static List<int> conLim = new List<int>();
            static int Q = 0;

            public static List<SymbolicState> GetArrivState()
            {
                int t = 0;

                List<SymbolicState> rs = new List<SymbolicState>();
                SymbolicState init = new SymbolicState();

                init.ContainLim = conLim.ToArray();
                init.ContainNum = conLim.Count;
                init.ContainStatus = new int[init.ContainNum];
                init.Step = 0;

                HashSet<string> Checked = new HashSet<string>();
                Dictionary<int, int> Availables = new Dictionary<int, int>();

                Queue<SymbolicState> queue = new Queue<SymbolicState>();
                queue.Enqueue(init);
                Checked.Add(init.StateKey);
                while (queue.Count > 0)
                {
                    SymbolicState s = queue.Dequeue();

                    foreach (int c in s.ContainStatus)
                    {
                        if (!Availables.ContainsKey(c))
                        {
                            if (s.Step >= BuilderParameters.MIN_BFS_DEPTH)
                            {
                                rs.Add(new SymbolicState() { ContainLim = init.ContainLim, ContainNum = init.ContainNum, ContainStatus = init.ContainStatus, Q = c });
                            }
                            Availables[c] = s.Step;
                        }
                    }

                    if (Availables.Count >= conLim.Last())
                        break;
                    if (s.Step < BuilderParameters.MAX_SEARCH_LEN)
                    {
                        List<SymbolicState> neighbors = s.NeighborStates();
                        foreach (SymbolicState ns in neighbors)
                        {
                            if (Checked.Contains(ns.StateKey)) continue;
                            Checked.Add(ns.StateKey);
                            queue.Enqueue(ns);
                        }
                    }
                    t = t + 1;
                    if (BuilderParameters.MAX_BFS_SEARCH > 0 && t >= BuilderParameters.MAX_BFS_SEARCH) { break; }
                }

                return rs;
            }

            public static SymbolicState CheckSuccess()
            {
                int t = 0;
                bool isFound = false;
                SymbolicState init = new SymbolicState();
                init.Q = Q;
                init.ContainLim = conLim.ToArray();
                init.ContainNum = conLim.Count;
                init.ContainStatus = new int[init.ContainNum];


                MonteCarloActor actor = new MonteCarloActor();
                Tuple<int, List<SymbolicState>> r = actor.Search(init, BuilderParameters.MAX_MC_SEARCH, BuilderParameters.MAX_SEARCH_LEN);
                init.MC_TIMES = r.Item1;
                if (init.MC_TIMES <= 0) return null;
                init.MC_DEPTH = r.Item2.Count;

                HashSet<string> Checked = new HashSet<string>();
                    
                Queue<SymbolicState> queue = new Queue<SymbolicState>();
                queue.Enqueue(init);
                Checked.Add(init.StateKey);
                while (queue.Count > 0)
                {
                    SymbolicState s = queue.Dequeue();
                    if (s.IsSuccess)
                    {
                        if (s.Step < BuilderParameters.MIN_BFS_DEPTH)
                            break;
                        init.BFS_DEPTH = s.Step; init.BFS_TIMES = t; isFound = true; break;
                    }

                    if (s.Step < BuilderParameters.MAX_SEARCH_LEN)
                    {
                        List<SymbolicState> neighbors = s.NeighborStates();
                        foreach (SymbolicState ns in neighbors)
                        {
                            if (Checked.Contains(ns.StateKey)) continue;
                            Checked.Add(ns.StateKey);
                            queue.Enqueue(ns);
                        }
                    }
                    t = t + 1;
                    if (BuilderParameters.MAX_BFS_SEARCH > 0 && t >= BuilderParameters.MAX_BFS_SEARCH) { break; }
                }

                if (!isFound) return null;

                return init;
            }

            public static int BFSSearch(SymbolicState init)
            {
                HashSet<string> Checked = new HashSet<string>();
                bool isFound = false;
                int t = 1;
                Queue<SymbolicState> queue = new Queue<SymbolicState>();
                queue.Enqueue(init);
                Checked.Add(init.StateKey);
                while (queue.Count > 0)
                {
                    SymbolicState s = queue.Dequeue();
                    if (s.IsSuccess)
                    {
                        init.BFS_DEPTH = s.Step; init.BFS_TIMES = t; isFound = true; break;
                    }

                    if (s.Step < BuilderParameters.MAX_SEARCH_LEN)
                    {
                        List<SymbolicState> neighbors = s.NeighborStates();
                        foreach (SymbolicState ns in neighbors)
                        {
                            if (Checked.Contains(ns.StateKey)) continue;
                            Checked.Add(ns.StateKey);
                            queue.Enqueue(ns);
                        }
                    }
                    t = t + 1;
                }

                if (!isFound) {
                    Console.WriteLine("BUGGGG"); };
                return Checked.Count; // t;
            }

            public static int DFSSearch(SymbolicState init)
            {
                HashSet<string> Checked = new HashSet<string>();
                bool isFound = false;
                int t = 1;
                Stack<SymbolicState> queue = new Stack<SymbolicState>();
                queue.Push(init);
                Checked.Add(init.StateKey);
                while (queue.Count > 0)
                {
                    SymbolicState s = queue.Pop();
                    if (s.IsSuccess)
                    {
                        init.DFS_DEPTH = s.Step; init.DFS_TIMES = t; isFound = true; break;
                    }
                    if (s.Step < BuilderParameters.MAX_SEARCH_LEN)
                    {
                        List<SymbolicState> neighbors = s.NeighborStates();
                        foreach (SymbolicState ns in neighbors)
                        {
                            if (Checked.Contains(ns.StateKey)) continue;
                            Checked.Add(ns.StateKey);
                            queue.Push(ns);
                        }
                    }
                    t = t + 1;
                }
                if (!isFound)
                {
                    Console.WriteLine("BUGGGG");
                };
                return Checked.Count; // t;
            }

            static List<SymbolicState> Qs = new List<SymbolicState>();
            static bool Find(int miniC)
            {
                if(conLim.Count == BuilderParameters.CONTAIN_NUM)
                {
                    List<SymbolicState> ms = GetArrivState();

                    int abs = Qs.Count;
                    Qs.AddRange(ms);
                    int bbs = Qs.Count;
                    if(bbs / 10 > abs / 10 )
                    {
                        Console.WriteLine(string.Format("Search Examples {0}", Qs.Count));
                    }
                    if (Qs.Count >= BuilderParameters.QUERY_SET) return true;

                    //for (int q = 1; q < conLim.Last(); q++)
                    //{
                    //    if (conLim.IndexOf(q) >= 0)
                    //        continue;
                    //    Q = q;
                    //    SymbolicState s = CheckSuccess();
                    //    if(s!= null)
                    //    {
                    //        Qs.Add(s);
                    //        if (Qs.Count % 10 == 0) { Console.WriteLine(string.Format("Search Examples {0}", Qs.Count)); }
                    //        if (Qs.Count >= BuilderParameters.QUERY_SET)
                    //            return true;
                    //    }
                    //}
                }
                else
                {
                    for (int c = miniC; c < BuilderParameters.CONTAIN_LIM; c++)
                    {
                        conLim.Add(c);
                        if (Find(c))
                            return true;
                        else
                            conLim.RemoveAt(conLim.Count - 1);
                    }
                }
                return false;
            }
            public static void RandomQuery2(string writePath)
            {
                Random random = new Random();
                HashSet<string> keys = new HashSet<string>();
                using (StreamWriter mwriter = new StreamWriter(writePath))
                {
                    int BFS_SearchDepth = 0;
                    int BFS_SearchTimes = 0;
                    int DFS_SearchDepth = 0;
                    int DFS_SearchTimes = 0;
                    int MAX_BFS_TIMES = 0;
                    int MAX_DFS_TIMES = 0;
                    int MAX_MC_TIMES = 0;

                    int MC_SearchDepth = 0;
                    int MC_SearchTimes = 0;

                    Find(BuilderParameters.MIN_CONTAIN_LIM);

                    for (int i = 0; i < Qs.Count; i++)
                    {
                        SymbolicState s = Qs[i];
                        
                        BFS_SearchDepth += s.BFS_DEPTH;
                        BFS_SearchTimes += s.BFS_TIMES;
                        DFS_SearchDepth += s.DFS_DEPTH;
                        DFS_SearchTimes += s.DFS_TIMES;

                        MC_SearchDepth += s.MC_DEPTH;
                        MC_SearchTimes += s.MC_TIMES;

                        mwriter.WriteLine(s.SaveKey);
                        if (s.BFS_TIMES > MAX_BFS_TIMES) MAX_BFS_TIMES = s.BFS_TIMES;
                        if (s.DFS_TIMES > MAX_DFS_TIMES) MAX_DFS_TIMES = s.DFS_TIMES;
                        if (s.MC_TIMES > MAX_MC_TIMES) MAX_MC_TIMES = s.MC_TIMES;

                    }
                    mwriter.Close();
                    Logger.WriteLog("Avg BFS Depth {0}", BFS_SearchDepth * 1.0f / BuilderParameters.QUERY_SET);
                    Logger.WriteLog("Avg DFS Depth {0}", DFS_SearchDepth * 1.0f / BuilderParameters.QUERY_SET);
                    Logger.WriteLog("Avg MC Depth {0}", MC_SearchDepth * 1.0f / BuilderParameters.QUERY_SET);

                    Logger.WriteLog("Avg BFS Times {0}", BFS_SearchTimes * 1.0f / BuilderParameters.QUERY_SET);
                    Logger.WriteLog("Avg DFS Times {0}", DFS_SearchTimes * 1.0f / BuilderParameters.QUERY_SET);
                    Logger.WriteLog("Avg MC Times {0}", MC_SearchTimes * 1.0f / BuilderParameters.QUERY_SET);

                    Logger.WriteLog("Max BFS Times {0}", MAX_BFS_TIMES);
                    Logger.WriteLog("Max DFS Times {0}", MAX_DFS_TIMES);
                    Logger.WriteLog("Max MC Times {0}", MAX_MC_TIMES);
                }
            }

            public static void LoadQuery(string path)
            {
                using (StreamReader mreader = new StreamReader(path))
                {
                    while (!mreader.EndOfStream)
                    {
                        SymbolicState s = new SymbolicState(mreader.ReadLine().Trim());
                        Query.Add(s);
                    }
                }
            }

            public static void Init()
            {
                if (!File.Exists(BuilderParameters.C_QUERY))
                {
                    RandomQuery2(BuilderParameters.QUERY_PATH);

                    LoadQuery(BuilderParameters.QUERY_PATH);
                }
                else
                {
                    LoadQuery(BuilderParameters.C_QUERY);

                    foreach(SymbolicState s in Query )
                    {
                        BFSSearch(s);
                        DFSSearch(s);
                    }

                    Console.WriteLine("Acg BFS Node Ratio : {0} ", Query.Select(i => i.BFS_TIMES).Sum() * 1.0 / Query.Count);
                    Console.WriteLine("Max BFS Node Ratio : {0} ", Query.Select(i => i.BFS_TIMES).Max());

                    Console.WriteLine("Avg DFS Node Ratio : {0} ", Query.Select(i => i.DFS_TIMES).Sum() * 1.0 / Query.Count);
                    Console.WriteLine("Max DFS Node Ratio : {0} ", Query.Select(i => i.DFS_TIMES).Max());
                }
            }
        }
    }
}
