﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BigLearn;

namespace BigLearn.DeepNet
{
    //public class AppMCTSGraphWalkerExecutorBuilder : Builder
    //{
    //    /// <summary>
    //    /// Builder Parameters.
    //    /// </summary>
    //    public class BuilderParameters : BaseModelArgument<BuilderParameters>
    //    {
    //        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
    //        public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

    //        #region Input Data Argument.
    //        public static string Entity2Id { get { return Argument["ENTITY2ID"].Value; } }
    //        public static int EntityNum { get { return int.Parse(Argument["ENTITYNUM"].Value); } }

    //        public static string Relation2Id { get { return Argument["RELATION2ID"].Value; } }
    //        public static int RelationNum { get { return int.Parse(Argument["RELATIONNUM"].Value); } }

    //        public static string TrainData { get { return Argument["TRAIN"].Value; } }
    //        public static string ValidData { get { return Argument["VALID"].Value; } }
    //        public static string TestData { get { return Argument["TEST"].Value; } }
    //        public static bool IsTrainFile { get { return !(TrainData.Equals(string.Empty)); } }
    //        public static bool IsValidFile { get { return !(ValidData.Equals(string.Empty)); } }
    //        public static bool IsTestFile { get { return !(TestData.Equals(string.Empty)); } }
    //        public static string ValidPathFolder { get { return Argument["VALID-PATH"].Value; } }
    //        #endregion.

    //        public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }
    //        public static int TrainBeamSize { get { return int.Parse(Argument["TRAIN-BEAM-SIZE"].Value); } }
    //        public static int TrainSetting { get { return int.Parse(Argument["TRAIN-SETTING"].Value); } }

    //        public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
    //        public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

    //        public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
    //        public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
    //        public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

    //        public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

    //        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
    //        public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

    //        public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }

    //        public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
    //        public static float T_MAX_CLIP { get { return float.Parse(Argument["T-MAX-CLIP"].Value); } }
    //        public static float T_MIN_CLIP { get { return float.Parse(Argument["T-MIN-CLIP"].Value); } }

    //        public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

    //        public static int[] S_NET { get { return Argument["S-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static A_Func[] S_AF { get { return Argument["S-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }


    //        public static float GUID_RATE { get { return float.Parse(Argument["GUID-RATE"].Value); } }
    //        public static float RAND_RATE { get { return float.Parse(Argument["RAND-RATE"].Value); } }

    //        public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
    //        public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
    //        public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }

    //        public static float NORM_REWARD { get { return float.Parse(Argument["NORM-REWARD"].Value); } }

    //        public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

    //        /// <summary>
    //        /// Monto Carlo Search Number.
    //        /// </summary>
    //        public static int MCTS_NUM { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

    //        static BuilderParameters()
    //        {
    //            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
    //            Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

    //            #region Input Data Argument.
    //            Argument.Add("ENTITY2ID", new ParameterArgument(string.Empty, "Entity 2 ID."));
    //            Argument.Add("ENTITYNUM", new ParameterArgument("0", "Entity Number."));

    //            Argument.Add("RELATION2ID", new ParameterArgument(string.Empty, "Relation 2 ID."));
    //            Argument.Add("RELATIONNUM", new ParameterArgument("0", "Relation Number."));

    //            Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
    //            Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
    //            Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));
    //            Argument.Add("VALID-PATH", new ParameterArgument(string.Empty, "Valid Path Data."));
    //            #endregion.

    //            Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
    //            Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
    //            Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));

    //            Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));
    //            Argument.Add("TRAIN-BEAM-SIZE", new ParameterArgument("10", "beam size."));

    //            Argument.Add("TRAIN-SETTING", new ParameterArgument("0", "0:supervised  1:reinforcement"));

    //            Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
    //            Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

    //            Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
    //            Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));

    //            Argument.Add("S-NET", new ParameterArgument("1", "Scoring Network."));
    //            Argument.Add("S-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));

    //            Argument.Add("GUID-RATE", new ParameterArgument("1", "guided path rate."));
    //            Argument.Add("RAND-RATE", new ParameterArgument("10", "rand path rate."));

    //            Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
    //            Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
    //            Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

    //            Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.9", "Reward discount"));

    //            Argument.Add("T-MAX-CLIP", new ParameterArgument("0", "Maximum T Clip"));
    //            Argument.Add("T-MIN-CLIP", new ParameterArgument("0", "Minimum T Clip"));

    //            Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

    //            Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

    //            Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score File."));
    //            Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));

    //            Argument.Add("NORM-REWARD", new ParameterArgument("0", "normalize reward."));
    //            Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));
    //            Argument.Add("MCTS-NUM", new ParameterArgument("10", "Monto Carlo Tree Search Number"));
    //        }
    //    }

    //    public override BuilderType Type { get { return BuilderType.APP_MCTS_GRAPH_WALKER_EXECUTOR; } }

    //    public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

    //    /// <summary>
    //    /// episodic memory structure.
    //    /// </summary>
    //    public class EpisodicMemory
    //    {
    //        Dictionary<string, Tuple<List<Tuple<float, float>>, int>> Mem = new Dictionary<string, Tuple<List<Tuple<float, float>>, int>>();
    //        int Time { get; set; }
    //        public EpisodicMemory()
    //        {
    //            Time = 0;
    //        }
    //        public void Clear()
    //        {
    //            Mem.Clear();
    //            Time = 0;
    //        }

    //        /// <summary>
    //        /// memory index.
    //        /// </summary>
    //        /// <param name="key"></param>
    //        /// <param name="actionCount"></param>
    //        /// <param name="topK"></param>
    //        /// <returns></returns>
    //        public List<Tuple<float, float>> Search(string key)
    //        {
    //            if(Mem.ContainsKey(key))
    //            {
    //                return Mem[key].Item1;
    //            }
    //            else
    //            {
    //                return null;
    //            }
    //        }

    //        public void Update(string key, int actid, int totalAct, float reward)
    //        {
    //            float new_u = 0;
    //            float new_c = 0;

    //            // update memory.
    //            if (Mem.ContainsKey(key))
    //            {
    //                float u = Mem[key].Item1[actid].Item1;
    //                float c = Mem[key].Item1[actid].Item2; 
    //                new_u = c / (c + 1.0f) * u + 1.0f / (c + 1.0f) * reward;
    //                new_c = c + 1;
    //            }
    //            else
    //            {
    //                Mem.Add(key, new Tuple<List<Tuple<float, float>>, int>(new List<Tuple<float, float>>(), Time));
                    
    //                for (int a = 0; a < totalAct; a++)
    //                {
    //                    Mem[key].Item1.Add(new Tuple<float, float>(0, 0));
    //                }
    //                new_u = reward;
    //                new_c = 1;
    //            }
    //            Mem[key].Item1[actid] = new Tuple<float, float>(new_u, new_c);
    //            //Mem[key].Item2 = Time;
    //        }

    //        public void UpdateTiming()
    //        {
    //            Time += 1;
    //        }
    //    }

    //    /// <summary>
    //    /// Graph Query Data.
    //    /// </summary>
    //    class GraphQueryData : BatchData
    //    {
    //        public List<int> RawSource = new List<int>();
    //        public List<Tuple<int, int>> RawQuery = new List<Tuple<int, int>>();
    //        public List<int> RawTarget = new List<int>();
    //        public List<HashSet<int>> BlackTargets = new List<HashSet<int>>();

    //        public List<StatusData> StatusPath = new List<StatusData>();
    //        public List<Tuple<int, int>> Results = new List<Tuple<int, int>>();

    //        public int BatchSize { get { return RawQuery.Count; } }
    //        public int MaxBatchSize { get; set; }

    //        /// <summary>
    //        /// max group number, and group size;
    //        /// </summary>
    //        /// <param name="maxGroupNum"></param>
    //        /// <param name="groupSize"></param>
    //        /// <param name="device"></param>
    //        public GraphQueryData(int maxBatchSize, DeviceType device)
    //        {
    //            MaxBatchSize = maxBatchSize;
    //        }

    //        public GraphQueryData(GraphQueryData data)
    //        {
    //            RawSource = data.RawSource;
    //            RawQuery = data.RawQuery;
    //            RawTarget = data.RawTarget;
    //            BlackTargets = data.BlackTargets;

    //            MaxBatchSize = data.MaxBatchSize;
    //        }
    //    }

    //    /// <summary>
    //    /// Sample Graph. 
    //    /// </summary>
    //    class SampleRunner : StructRunner
    //    {
    //        List<Tuple<int, int, int>> Graph { get; set; }
    //        public new GraphQueryData Output { get; set; }
    //        int MaxBatchSize { get; set; }
    //        int BatchSize { get; set; }
    //        DataRandomShuffling Shuffle { get; set; }
    //        Random random = new Random();
    //        public SampleRunner(List<Tuple<int, int, int>> graph,
    //                            int maxBatchSize,
    //                            RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Graph = graph;
    //            MaxBatchSize = maxBatchSize;
    //            Shuffle = new DataRandomShuffling(Graph.Count * 2, random);
    //            Output = new GraphQueryData(MaxBatchSize, behavior.Device);
    //        }
    //        public override void Init()
    //        {
    //            IsTerminate = false;
    //            IsContinue = true;
    //            Shuffle.Init();
    //        }
    //        public override void Forward()
    //        {
    //            Output.RawQuery.Clear();
    //            Output.RawSource.Clear();
    //            Output.RawTarget.Clear();
    //            Output.BlackTargets.Clear();

    //            int gIdx = 0;

    //            while (gIdx < MaxBatchSize)
    //            {
    //                int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext();
    //                if (idx <= -1) { break; }
    //                int srcId = idx < Graph.Count ? Graph[idx].Item1 : Graph[idx - Graph.Count].Item2;
    //                int tgtId = idx < Graph.Count ? Graph[idx].Item2 : Graph[idx - Graph.Count].Item1;
    //                int linkId = idx < Graph.Count ? Graph[idx].Item3 : (Graph[idx - Graph.Count].Item3 + DataPanel.RelationNum);

    //                Output.RawQuery.Add(new Tuple<int, int>(srcId, linkId));
    //                Output.RawSource.Add(srcId);
    //                Output.RawTarget.Add(tgtId);

    //                HashSet<int> blackTarget = DataPanel.knowledgeGraph.NeighborHash[srcId].ContainsKey(linkId) ?
    //                                            DataPanel.knowledgeGraph.NeighborHash[srcId][linkId] : new HashSet<int>();
    //                Output.BlackTargets.Add(blackTarget);
    //                gIdx += 1;
    //            }
    //            if (gIdx == 0) { IsTerminate = true; return; }
    //        }
    //    }


    //    class StatusData : BatchData
    //    {
    //        /// <summary>
    //        /// Raw Query.
    //        /// </summary>
    //        public GraphQueryData GraphQuery { get; set; }

    //        /// <summary>
    //        /// Node ID.
    //        /// </summary>
    //        public List<int> NodeID { get; set; }

    //        /// <summary>
    //        /// Embedding of the State.
    //        /// </summary>
    //        public HiddenBatchData StateEmbed { get; set; }


    //        /// <summary>
    //        /// LogProbability to this node.
    //        /// </summary>
    //        List<float> LogProb = null;
    //        public float GetLogProb(int batchIdx)
    //        {
    //            if (LogProb == null) { return 0; }
    //            else { return LogProb[batchIdx]; }
    //        }

    //        /// <summary>
    //        /// MatchCandidate tgtID and relationID. and MatchCandidateProb. 
    //        /// </summary>
    //        public List<Tuple<int, int>> MatchCandidate = null;
    //        public SeqVectorData MatchCandidateProb = null;
    //        public int GetActionDim(int b)
    //        {
    //            return GetActionEndIndex(b) - GetActionStartIndex(b);
    //        }

    //        public int GetActionStartIndex(int b)
    //        {
    //            return b == 0 ? 0 : MatchCandidateProb.SegmentIdx.MemPtr[b - 1];
    //        }
    //        public int GetActionEndIndex(int b)
    //        {
    //            return MatchCandidateProb.SegmentIdx.MemPtr[b];
    //        }

    //        public List<int> PreSelActIndex = null;

    //        public List<int> PreStatusIndex = null;
    //        public int GetPreStatusIndex(int batchIdx)
    //        {
    //            if (PreStatusIndex == null) { return batchIdx; }
    //            else return PreStatusIndex[batchIdx];
    //        }
    //        public int GetOriginalStatsIndex(int batchIdx)
    //        {
    //            if (Step == 0)
    //            {
    //                return batchIdx;
    //            }
    //            else
    //            {
    //                int b = GetPreStatusIndex(batchIdx);
    //                return GraphQuery.StatusPath[Step - 1].GetOriginalStatsIndex(b);
    //            }
    //        }


    //        public List<string> StatusKey = null;
    //        public string GetStatusKey(int b)
    //        {
    //            if (StatusKey == null)
    //            {
    //                return string.Format("N:{0}-R:{1}", GraphQuery.RawQuery[b].Item1, GraphQuery.RawQuery[b].Item2);
    //            }
    //            else
    //            {
    //                return StatusKey[b];
    //            }
    //        }

    //        /// <summary>
    //        /// Termination Probability.
    //        /// </summary>
    //        public HiddenBatchData Term = null;
    //        public float GetTerm(int batchIdx)
    //        {
    //            if(Term == null) { return 0; }
    //            else { return (float)Util.Logistic(Term.Output.Data.MemPtr[batchIdx]); }
    //            //if (isScaled) { return (float)Util.LogLogistial(Term.Output.Data.MemPtr[b]); }
    //            //else { return (float)Util.LogNLogistial(Term.Output.Data.MemPtr[b]); }
    //        }
            
    //        public bool IsBlack(int b)
    //        {
    //            int originb = GetOriginalStatsIndex(b);
    //            if (GraphQuery.BlackTargets[originb].Contains(NodeID[b])) { return true; }
    //            else return false;
    //        }

    //        /// <summary>
    //        /// Estimated Reward. 
    //        /// </summary>
    //        public HiddenBatchData Score = null;

    //        public int MaxBatchSize { get { return StateEmbed.MAX_BATCHSIZE; } }

    //        public int BatchSize { get { return StateEmbed.BatchSize; } }
           
    //        /// <summary>
    //        /// step of current data.
    //        /// </summary>
    //        public int Step;


    //        /// <summary>
    //        /// Initial StatusData Construction Function.
    //        /// </summary>
    //        /// <param name="interData"></param>
    //        /// <param name="nodeIndex"></param>
    //        /// <param name="stateEmbed"></param>
    //        /// <param name="device"></param>
    //        public StatusData(GraphQueryData interData, HiddenBatchData stateEmbed, DeviceType device) :
    //            this(interData, interData.RawSource, null, null, null, null, stateEmbed, device)
    //        { }

    //        public StatusData(GraphQueryData interData, List<int> nodeIndex, List<float> logProb, List<int> selectedAction, List<int> preStatusIndex, List<string> statusKey,
    //                          HiddenBatchData stateEmbed, DeviceType device)
    //        {
    //            GraphQuery = interData;
    //            GraphQuery.StatusPath.Add(this);

    //            NodeID = nodeIndex;
    //            LogProb = logProb;
    //            PreSelActIndex = selectedAction;
    //            PreStatusIndex = preStatusIndex;
    //            StatusKey = statusKey;

    //            StateEmbed = stateEmbed;                
    //            Step = GraphQuery.StatusPath.Count - 1;
    //        }
    //    }


    //    class StatusEmbedRunner : CompositeNetRunner
    //    {
    //        public new List<Tuple<int, int>> Input { get; set; }

    //        public new HiddenBatchData Output { get; set; }

    //        EmbedStructure InNodeEmbed { get; set; }
    //        EmbedStructure LinkEmbed { get; set; }

    //        public StatusEmbedRunner(List<Tuple<int, int>> input, int maxBatchSize, EmbedStructure inNodeEmbed, EmbedStructure linkEmbed, RunnerBehavior behavior) : base(behavior)
    //        {
    //            Input = input;
    //            InNodeEmbed = inNodeEmbed;
    //            LinkEmbed = linkEmbed;
    //            // concate of node embedding and relation embedding.
    //            Output = new HiddenBatchData(maxBatchSize, inNodeEmbed.Dim + linkEmbed.Dim, DNNRunMode.Train, behavior.Device);
    //        }

    //        public override void Forward()
    //        {
    //            InNodeEmbed.Embedding.SyncToCPU();
    //            LinkEmbed.Embedding.SyncToCPU();

    //            //BatchLinks.Clear();
    //            int batchSize = 0;
    //            while (batchSize < Input.Count)
    //            {
    //                int srcId = Input[batchSize].Item1;
    //                int linkId = Input[batchSize].Item2;

    //                int bidx = batchSize;

    //                //Output.NodeIndex[bidx] = srcId;

    //                if (BuilderParameters.N_EmbedDim > 0)
    //                {
    //                    FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim,
    //                                            InNodeEmbed.Embedding.MemPtr, srcId * InNodeEmbed.Dim, InNodeEmbed.Dim, 0, 1);
    //                }
    //                FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim + InNodeEmbed.Dim,
    //                                        LinkEmbed.Embedding.MemPtr, linkId * LinkEmbed.Dim, LinkEmbed.Dim, 0, 1);
    //                batchSize += 1;
    //            }
    //            Output.BatchSize = batchSize;
    //            Output.Output.Data.SyncFromCPU(Output.BatchSize * Output.Dim);
    //        }

    //        public override void CleanDeriv()
    //        {
    //            ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
    //        }

    //        public override void Update()
    //        {
    //            Output.Deriv.Data.SyncToCPU(Output.Dim * Output.BatchSize);

    //            InNodeEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();
    //            LinkEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();

    //            for (int b = 0; b < Output.BatchSize; b++)
    //            {
    //                int srcIdx = Input[b].Item1;
    //                int linkIdx = Input[b].Item2;

    //                //InNodeEmbed.EmbeddingOptimizer.PushSparseGradient(srcIdx * InNodeEmbed.Dim, InNodeEmbed.Dim);
    //                //LinkEmbed.EmbeddingOptimizer.PushSparseGradient(linkIdx * LinkEmbed.Dim, LinkEmbed.Dim);

    //                if (BuilderParameters.N_EmbedDim > 0)
    //                {
    //                    FastVector.Add_Vector(InNodeEmbed.EmbeddingOptimizer.Gradient.MemPtr, srcIdx * InNodeEmbed.Dim,
    //                                    Output.Deriv.Data.MemPtr, b * Output.Dim, InNodeEmbed.Dim,
    //                                    1, InNodeEmbed.EmbeddingOptimizer.GradientStep);
    //                }

    //                FastVector.Add_Vector(LinkEmbed.EmbeddingOptimizer.Gradient.MemPtr, linkIdx * LinkEmbed.Dim,
    //                                Output.Deriv.Data.MemPtr, b * Output.Dim + InNodeEmbed.Dim, LinkEmbed.Dim,
    //                                1, LinkEmbed.EmbeddingOptimizer.GradientStep);
    //            }

    //            InNodeEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
    //            LinkEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();

    //        }
    //    }

    //    /// <summary>
    //    /// candidate selection.
    //    /// </summary>
    //    class CandidateActionRunner : CompositeNetRunner
    //    {
    //        public new StatusData Input;

    //        /// <summary>
    //        /// target node and relation.
    //        /// </summary>
    //        public new List<Tuple<int, int>> Output = new List<Tuple<int, int>>();

    //        /// <summary>
    //        /// (src, tgt, label)
    //        /// </summary>
    //        public BiMatchBatchData Match;

    //        /// <summary>
    //        /// Input : State.
    //        /// node and rel embed.
    //        /// </summary>
    //        /// <param name="input"></param>
    //        /// <param name="nodeEmbed"></param>
    //        /// <param name="relEmbed"></param>
    //        /// <param name="behavior"></param>
    //        public CandidateActionRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
    //        {
    //            Input = input;

    //            Match = new BiMatchBatchData(new BiMatchBatchDataStat()
    //            {
    //                MAX_MATCH_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
    //                MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
    //                MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount
    //            }, behavior.Device);
    //        }

    //        Random random = new Random();
    //        public override void Forward()
    //        {
    //            Output.Clear();

    //            List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
    //            int cursor = 0;
    //            for (int b = 0; b < Input.BatchSize; b++)
    //            {
    //                int seedNode = Input.NodeID[b];

    //                int rawSrc = Input.GraphQuery.RawQuery[b].Item1;
    //                int rawR = Input.GraphQuery.RawQuery[b].Item2;
    //                int rawTgt = Input.GraphQuery.RawTarget[b];

    //                int candidateNum = 0;
    //                for (int nei = 0; nei < DataPanel.knowledgeGraph.NeighborLink[seedNode].Count; nei++)
    //                {
    //                    int lid = DataPanel.knowledgeGraph.NeighborLink[seedNode][nei].Item1 > 0 ?
    //                        DataPanel.knowledgeGraph.NeighborLink[seedNode][nei].Item2 :
    //                        DataPanel.knowledgeGraph.NeighborLink[seedNode][nei].Item2 + DataPanel.RelationNum;

    //                    int relid = DataPanel.knowledgeGraph.NeighborLink[seedNode][nei].Item1 > 0 ?
    //                        DataPanel.knowledgeGraph.NeighborLink[seedNode][nei].Item2 + DataPanel.RelationNum :
    //                        DataPanel.knowledgeGraph.NeighborLink[seedNode][nei].Item2;

    //                    int tgtNode = DataPanel.knowledgeGraph.NeighborLink[seedNode][nei].Item3;
    //                    if (seedNode == rawSrc && lid == rawR && tgtNode == rawTgt) { continue; }
    //                    if (seedNode == rawTgt && relid == rawR && tgtNode == rawSrc) { continue; }

    //                    Output.Add(new Tuple<int, int>(tgtNode, lid));
    //                    match.Add(new Tuple<int, int, float>(b, cursor, 1));
    //                    cursor += 1;
    //                    candidateNum += 1;
    //                }

    //                if (candidateNum == 0)
    //                {
    //                    //random action.
    //                    Output.Add(new Tuple<int, int>(seedNode, random.Next(DataPanel.RelationNum * 2)));
    //                    match.Add(new Tuple<int, int, float>(b, cursor, 1));
    //                    cursor += 1;
    //                }
    //            }
    //            Match.SetMatch(match);
    //        }
    //    }

    //    static Random SampleRandom = new Random(10);

    //    class BanditAlg
    //    {
    //        /// <summary>
    //        /// standford UCB.
    //        /// </summary>
    //        /// <param name="arms"></param>
    //        /// <param name="c"></param>
    //        /// <param name="random"></param>
    //        /// <returns></returns>
    //        public static int UCBBandit(List<Tuple<float, float>> arms, float c, Random random)
    //        {
    //            float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);
    //            List<float> v = new List<float>();
    //            foreach (Tuple<float, float> arm in arms)
    //            {
    //                v.Add(arm.Item1 / (arm.Item2 + 0.1f) + c * (float)Math.Sqrt(log_total / (arm.Item2 + 0.1f)) + (float)random.NextDouble() * 0.0001f);
    //            }
    //            int idx = Util.MaximumValue(v.ToArray());
    //            return idx;
    //        }

    //        public static int UCBBandit_Sample(List<Tuple<float, float>> arms, float[] prior, float c, Random random)
    //        {
    //            if (arms == null)
    //            {
    //                int idx = Util.Sample(prior, random);
    //                return idx;
    //            }
    //            else
    //            {
    //                float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);

    //                List<float> v = new List<float>();
    //                int aidx = 0;
    //                foreach (Tuple<float, float> arm in arms)
    //                {
    //                    v.Add(prior[aidx] / (1.0f + arms[aidx].Item2) + arms[aidx].Item1  / ( 1.0f + arms[aidx].Item2) +
    //                            c * (float)Math.Sqrt(log_total / (arm.Item2 + 0.1f)) + (float)random.NextDouble() * 0.001f);
    //                    aidx += 1;
    //                }
    //                int idx = Util.MaximumValue(v.ToArray());
    //                return idx;
    //            }
    //        }

    //        public static int UCBBandit(List<Tuple<float, float>> arms, float[] prior, float c, Random random)
    //        {
    //            if (arms == null)
    //            {
    //                int idx = Util.MaximumValue(prior);
    //                return idx;
    //            }
    //            else
    //            {
    //                float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);

    //                List<float> v = new List<float>();
    //                int aidx = 0;
    //                foreach (Tuple<float, float> arm in arms)
    //                {
    //                    v.Add(prior[aidx] / (1.0f + arms[aidx].Item2) + arms[aidx].Item1 * 1.0f / arms[aidx].Item2 +
    //                            c * (float)Math.Sqrt(log_total / (arm.Item2 + 0.1f)) + (float)random.NextDouble() * 0.001f);
    //                    aidx += 1;
    //                }
    //                int idx = Util.MaximumValue(v.ToArray());
    //                return idx;
    //            }
    //        }


    //        public static int AlphaGoBandit(List<Tuple<float, float>> arms, List<float> prior, Random random)
    //        {
    //            int maxI = 0;
    //            float maxV = float.MinValue;

    //            if (arms == null)
    //            {
    //                for (int i = 0; i < prior.Count; i++)
    //                {
    //                    float s = prior[i] + (float)random.NextDouble() * 0.1f;
    //                    if (s > maxV)
    //                    {
    //                        maxV = s;
    //                        maxI = i;
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                for (int i = 0; i < arms.Count; i++)
    //                {
    //                    float s = prior[i] / (1.0f + arms[i].Item2) + arms[i].Item1 * 1.0f / arms[i].Item2 + (float)random.NextDouble() * 0.1f;
    //                    if (s > maxV)
    //                    {
    //                        maxV = s;
    //                        maxI = i;
    //                    }
    //                }
    //            }
    //            return maxI;
    //        }
    //    }

    //    class MCTSActionSamplingRunner : CompositeNetRunner
    //    {
    //        new StatusData Input { get; set; }

    //        public BiMatchBatchData MatchPath = null;
    //        /// <summary>
    //        /// new Node Index.
    //        /// </summary>
    //        public List<int> NodeIndex { get; set; }

    //        /// <summary>
    //        /// next step log probability.
    //        /// </summary>
    //        public List<float> LogProb { get; set; }

    //        /// <summary>
    //        /// target id and relation.
    //        /// </summary>
    //        public List<Tuple<int, int>> Action { get; set; }

    //        /// <summary>
    //        /// action index.
    //        /// </summary>
    //        public List<int> PreSelActIndex { get; set; }
    //        public List<int> PreStatusIndex { get; set; }
    //        public List<string> StautsKey { get; set; }

    //        int lineIdx = 0;
    //        EpisodicMemory Memory { get; set; }
    //        Random random = new Random();
    //        public override void Init()
    //        {
    //            lineIdx = 0;
    //        }
    //        /// <summary>
    //        /// match candidate, and match probability.
    //        /// </summary>
    //        /// <param name="input"></param>
    //        /// <param name="behavior"></param>
    //        public MCTSActionSamplingRunner(StatusData input, EpisodicMemory memory, RunnerBehavior behavior) : base(behavior)
    //        {
    //            Input = input;
    //            Memory = memory;

    //            NodeIndex = new List<int>();
    //            LogProb = new List<float>();
    //            PreSelActIndex = new List<int>();
    //            PreStatusIndex = new List<int>();
    //            Action = new List<Tuple<int, int>>();
    //            StautsKey = new List<string>();

    //            MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
    //            {
    //                MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
    //                MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
    //                MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
    //            }, behavior.Device);
    //        }

    //        public override void Forward()
    //        {
    //            NodeIndex.Clear();
    //            PreSelActIndex.Clear();
    //            PreStatusIndex.Clear();
    //            LogProb.Clear();
    //            Action.Clear();
    //            StautsKey.Clear();

    //            Input.MatchCandidateProb.Output.SyncToCPU();
    //            if (Input.Term != null) Input.Term.Output.SyncToCPU();

    //            List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

    //            int currentStep = Input.Step;
    //            for (int i = 0; i < Input.MatchCandidateProb.Segment; i++)
    //            {
    //                int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
    //                int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

    //                int selectIdx = -1;

    //                string Qkey = Input.GetStatusKey(i);

    //                //key = key + "-" + 
    //                List<Tuple<float, float>> m = Memory.Search(Qkey);
    //                float[] prior_r = null;

    //                int actionDim = e - s;
    //                if (Input.Term != null)
    //                {
    //                    actionDim = actionDim + 1;
    //                    prior_r = new float[actionDim];
    //                    Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim - 1);

    //                    float t = Util.Logistic(Input.Term.Output.Data.MemPtr[i]);
    //                    prior_r[actionDim - 1] = t;
    //                    for (int a = 0; a < actionDim - 1; a++)
    //                    {
    //                        prior_r[a] = prior_r[a] * (1 - t);
    //                    }
    //                }
    //                else
    //                {
    //                    prior_r = new float[actionDim];
    //                    Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);
    //                }

    //                if (m != null && actionDim != m.Count) { throw new Exception(string.Format("the dimension doesn't match {0}, {1}", actionDim, m.Count)); }

    //                int idx = BanditAlg.UCBBandit_Sample(m, prior_r, 2, random);

    //                // sample to termination.
    //                if (Input.Term != null && idx == actionDim - 1)
    //                {
    //                    //Tuple<int, int> randAct = new Tuple<int, int>(Input.NodeID[i], random.Next(DataPanel.RelationNum * 2));
    //                    //NodeAction.Add(-1);
    //                    //Action.Add(randAct);
    //                    //NodeIndex.Add(Input.NodeID[i]);
    //                    //string statusQ = string.Format("H:{0}-N:{1}-R:{2}", Qkey, randAct.Item1, randAct.Item2);
    //                    //StautsKey.Add(statusQ);

    //                    Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, i));
    //                }
    //                else
    //                {
    //                    selectIdx = s + idx;
    //                    PreSelActIndex.Add(selectIdx);
    //                    PreStatusIndex.Add(i);

    //                    string statusQ = string.Format("H:{0}-N:{1}-R:{2}", Qkey, Input.MatchCandidate[selectIdx].Item1, Input.MatchCandidate[selectIdx].Item2);
    //                    NodeIndex.Add(Input.MatchCandidate[selectIdx].Item1);
    //                    Action.Add(Input.MatchCandidate[selectIdx]);
    //                    StautsKey.Add(statusQ);
    //                    float prob = Input.GetLogProb(i) + (float)Math.Log(prior_r[idx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
    //                    LogProb.Add(prob);

    //                    match.Add(new Tuple<int, int, float>(i, selectIdx, 1));
    //                }
    //            }
    //            MatchPath.SetMatch(match);
    //            lineIdx += Input.MatchCandidateProb.Segment ;
    //        }
    //    }

    //    //class BeamSearchActionRunner : CompositeNetRunner
    //    //{
    //    //    new StatusData Input { get; set; }
    //    //    int BeamSize = 1;

    //    //    public BiMatchBatchData MatchPath { get; set; }

    //    //    /// <summary>
    //    //    /// Transist Node Index.
    //    //    /// </summary>
    //    //    public List<int> NodeIndex { get; set; }

    //    //    /// <summary>
    //    //    /// Group index.
    //    //    /// </summary>
    //    //    public List<int> GroupIndex { get; set; }

    //    //    /// <summary>
    //    //    /// Transist Node Probability.
    //    //    /// </summary>
    //    //    public List<float> NodeProb { get; set; }


    //    //    /// <summary>
    //    //    /// batchIdx and actionIdx.
    //    //    /// </summary>
    //    //    public List<Tuple<int, int>> NodeAction { get; set; }

    //    //    /// <summary>
    //    //    /// group aware beam search.
    //    //    /// </summary>
    //    //    /// <param name="input"></param>
    //    //    /// <param name="behavior"></param>
    //    //    public BeamSearchActionRunner(StatusData input, int beamSize, RunnerBehavior behavior) : base(behavior)
    //    //    {
    //    //        Input = input;
    //    //        BeamSize = beamSize;
    //    //        MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
    //    //        {
    //    //            MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
    //    //            MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
    //    //            MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
    //    //        }, behavior.Device);

    //    //        NodeIndex = new List<int>();
    //    //        GroupIndex = new List<int>();
    //    //        NodeProb = new List<float>();
    //    //        NodeAction = new List<Tuple<int, int>>();
    //    //    }

    //    //    public override void Forward()
    //    //    {
    //    //        if (Input.MatchCandidateProb.Segment != Input.BatchSize)
    //    //        {
    //    //            throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
    //    //                            Input.MatchCandidateProb.Segment, Input.BatchSize));
    //    //        }

    //    //        Input.MatchCandidateProb.Output.SyncToCPU();
    //    //        if (Input.Term != null) Input.Term.Output.SyncToCPU();

    //    //        NodeIndex.Clear();
    //    //        NodeProb.Clear();
    //    //        NodeAction.Clear();
    //    //        GroupIndex.Clear();

    //    //        List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
    //    //        // for each beam.
    //    //        for (int i = 0; i < Input.GraphQuery.BatchSize; i++) //.MatchCandidateProb.Segment; i++)
    //    //        {
    //    //            List<int> idxs = Input.GetBatchIdxs(i);

    //    //            MinMaxHeap<Tuple<int, int>> topKheap = new MinMaxHeap<Tuple<int, int>>(BeamSize, 1);

    //    //            foreach (int b in idxs)
    //    //            {
    //    //                int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[b];
    //    //                int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

    //    //                float nonLogTerm = 0;
    //    //                if (Input.IsTermable(b))
    //    //                {
    //    //                    nonLogTerm = Input.LogPTerm(b, false);
    //    //                }

    //    //                for (int t = s; t < e; t++)
    //    //                {
    //    //                    float prob = Input.GetLogProb(b) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t]);
    //    //                    topKheap.push_pair(new Tuple<int, int>(b, t), prob);
    //    //                }
    //    //            }

    //    //            while (!topKheap.IsEmpty)
    //    //            {
    //    //                KeyValuePair<Tuple<int, int>, float> p = topKheap.PopTop();
    //    //                match.Add(new Tuple<int, int, float>(p.Key.Item1, p.Key.Item2, p.Value));

    //    //                NodeIndex.Add(Input.MatchCandidate[p.Key.Item2].Item1);
    //    //                NodeProb.Add(p.Value);
    //    //                GroupIndex.Add(i);
    //    //                NodeAction.Add(new Tuple<int, int>(p.Key.Item1, p.Key.Item2));
    //    //            }
    //    //        }
    //    //        MatchPath.SetMatch(match);
    //    //    }
    //    //}

    //    class RewardFeedbackRunner : StructRunner
    //    {
    //        Random random = new Random();
    //        new GraphQueryData Input { get; set; }
    //        EpisodicMemory Memory { get; set; }
    //        public RewardFeedbackRunner(GraphQueryData input, EpisodicMemory memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Input = input;
    //            Memory = memory;
    //        }

    //        public override void Forward()
    //        {
    //            if (Input.StatusPath.Count == BuilderParameters.MAX_HOP + 1)
    //            {
    //                for (int b = 0; b < Input.StatusPath[BuilderParameters.MAX_HOP].BatchSize; b++)
    //                {
    //                    Input.Results.Add(new Tuple<int, int>(BuilderParameters.MAX_HOP, b));
    //                }
    //            }

    //            // calculate termination probability.
    //            for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
    //            {
    //                if (Input.StatusPath[t].Term != null)
    //                {
    //                    ComputeLib.Logistic(Input.StatusPath[t].Term.Output.Data, 0, Input.StatusPath[t].Term.Output.Data, 0, Input.BatchSize, 1);
    //                    ComputeLib.ClipVector(Input.StatusPath[t].Term.Output.Data, Input.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);
    //                    Input.StatusPath[t].Term.Output.Data.SyncToCPU();
    //                    Array.Clear(Input.StatusPath[t].Term.Deriv.Data.MemPtr, 0, Input.StatusPath[t].BatchSize);
    //                }
    //                if (Input.StatusPath[t].MatchCandidateProb != null)
    //                {
    //                    Array.Clear(Input.StatusPath[t].MatchCandidateProb.Deriv.MemPtr, 0, Input.StatusPath[t].MatchCandidateProb.Length);
    //                }
    //                if(Input.StatusPath[t].Score != null)
    //                {
    //                    Input.StatusPath[t].Score.Output.Data.SyncToCPU();
    //                    Array.Clear(Input.StatusPath[t].Score.Deriv.Data.MemPtr, 0, Input.StatusPath[t].Score.Output.Data.EffectiveSize);
    //                }
    //            }

    //            for (int i = 0; i < Input.Results.Count; i++)
    //            {
    //                int t = Input.Results[i].Item1;
    //                int b = Input.Results[i].Item2;
    //                int origialB = Input.StatusPath[t].GetOriginalStatsIndex(b);
    //                int predId = Input.StatusPath[t].NodeID[b];

    //                int targetId = Input.RawTarget[origialB];

    //                float v = Math.Max(0, Input.StatusPath[t].Score.Output.Data.MemPtr[b]) * 0.1f;
    //                if (Input.BlackTargets[origialB].Contains(predId)) { v = 0; }


    //                StatusData st = Input.StatusPath[t];
    //                string key = st.GetStatusKey(b);
    //                if (t != BuilderParameters.MAX_HOP) { Memory.Update(key, st.GetActionDim(b), st.GetActionDim(b) + 1, v); }

    //                for (int pt = t - 1; pt >= 0; pt--)
    //                {
    //                    StatusData pst = Input.StatusPath[pt];
    //                    int pb = st.GetPreStatusIndex(b);
    //                    string pkey = pst.GetStatusKey(pb);

    //                    int sidx = st.PreSelActIndex[b] - pst.GetActionStartIndex(pb);

    //                    Memory.Update(pkey, sidx, pst.Term == null ? pst.GetActionDim(pb) : pst.GetActionDim(pb) + 1, v);

    //                    st = pst;
    //                    b = pb;
    //                }
    //            }
    //            //Input.TerminateIndex.Clear();
    //            //Input.EstimatedRewards.Clear();
    //            //Input.TrueRewards.Clear();
    //            //Input.IsTrainRewards.Clear();

    //            //// calculate the probability of each stopable sample.
    //            //for (int b = 0; b < Input.BatchSize; b++)
    //            //{
    //            //    int selectedStep = Input.StatusPath.Count - 1;
    //            //    for (int t = 2; t < Input.StatusPath.Count; t++)
    //            //    {
    //            //        StatusData st = Input.StatusPath[t];
    //            //        if(st.PreSelActIndex[b] == -1)
    //            //        {
    //            //            selectedStep = t - 1;
    //            //            break;
    //            //        }
    //            //    }
    //            //    Input.TerminateIndex.Add(selectedStep);
    //            //}

    //            //for (int b = 0; b < Input.BatchSize; b++)
    //            //{
    //            //    int t = Input.TerminateIndex[b];
    //            //    float v = Math.Max(0, Input.StatusPath[t].Score.Output.Data.MemPtr[b]);

    //            //    if (Input.BlackTargets[b].Contains(targetId))
    //            //    {
    //            //        v = 0;
    //            //        Input.IsTrainRewards.Add(false);
    //            //    }
    //            //    else
    //            //    {
    //            //        Input.IsTrainRewards.Add(true);
    //            //    }

    //            //    if (targetId == Input.RawTarget[b])
    //            //    {
    //            //        Input.TrueRewards.Add(1);
    //            //    }
    //            //    else
    //            //    {
    //            //        Input.TrueRewards.Add(0);
    //            //    }

    //            //    Input.EstimatedRewards.Add(v);

    //            //    StatusData st = Input.StatusPath[t];
    //            //    for (int pt = t - 1; pt >= 0; pt--)
    //            //    {
    //            //        int sidx = st.PreSelActIndex[b];
                        
    //            //        StatusData pst = Input.StatusPath[pt];
    //            //        string key = pst.GetStatusKey(b);

    //            //        int e = pst.MatchCandidateProb.SegmentIdx.MemPtr[b];
    //            //        int s = b == 0 ? 0 : pst.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

    //            //        Memory.Update(key, sidx, sidx - s, e - s, v * 0.2f);
    //            //        st = pst;
    //            //    }
    //            //}
    //            Memory.UpdateTiming();
                
    //        }
    //    }

    //    /// <summary>
    //    /// it is a little difficult.
    //    /// </summary>
    //    class RewardUpdateRunner : ObjectiveRunner
    //    {
    //        Random random = new Random();
    //        new List<GraphQueryData> Input { get; set; }
    //        EpisodicMemory Memory { get; set; }
    //        public RewardUpdateRunner(List<GraphQueryData> input, EpisodicMemory memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Input = input;
    //            Memory = memory;
    //        }

    //        public override void Forward()
    //        {
    //            float trueReward = 0;
    //            int sampleNum = 0;
    //            float mseloss = 0;
    //            for (int m = 0; m < Input.Count; m++)
    //            {
    //                for (int i = 0; i < Input[m].Results.Count; i++)
    //                {
    //                    int t = Input[m].Results[i].Item1;
    //                    int b = Input[m].Results[i].Item2;
    //                    int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);
    //                    int predId = Input[m].StatusPath[t].NodeID[b];

    //                    int targetId = Input[m].RawTarget[origialB];

    //                    float v = Input[m].StatusPath[t].Score.Output.Data.MemPtr[b];
    //                    if (Input[m].BlackTargets[origialB].Contains(predId)) { continue; }

    //                    float cr = 0;
    //                    if (predId == targetId) cr = 1;

    //                    trueReward += cr;
    //                    sampleNum += 1;

    //                    // MSE error for reward estimation.
    //                    Input[m].StatusPath[t].Score.Deriv.Data.MemPtr[b] = (cr - Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);

    //                    mseloss += 0.5f * (cr - Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]) * (cr - Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);

    //                    // Policy gradient for path finding.
    //                    StatusData st = Input[m].StatusPath[t];

    //                    if (t != BuilderParameters.MAX_HOP) st.Term.Deriv.Data.MemPtr[b] = cr * (1 - st.Term.Output.Data.MemPtr[b]);

    //                    for (int pt = t - 1; pt >= 0; pt--)
    //                    {
    //                        StatusData pst = Input[m].StatusPath[pt];
    //                        int pb = st.GetPreStatusIndex(b);


    //                        int sidx = st.PreSelActIndex[b];// - pst.GetActionStartIndex(pb);

    //                        if (pst.Term != null)
    //                        {
    //                            pst.Term.Deriv.Data.MemPtr[b] = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt) * cr * (-pst.Term.Output.Data.MemPtr[b]);
    //                        }
    //                        pst.MatchCandidateProb.Deriv.MemPtr[sidx] = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt) * cr;

    //                        st = pst;
    //                        b = pb;
    //                    }
    //                }
    //            }

    //            ObjectiveDict["AVG-TRUE-PATH"] = trueReward / (sampleNum + float.Epsilon);
    //            ObjectiveDict["REWARD-MSE-LOSS"] = mseloss / (sampleNum + float.Epsilon);


    //            ObjectiveScore = trueReward / (sampleNum + float.Epsilon);
    //            Memory.Clear();
    //            //average ground truth results.
    //            for (int p = 0; p < Input.Count; p++)
    //            {
    //                for (int i = Input[p].StatusPath.Count - 1; i >= 0; i--)
    //                {
    //                    StatusData st = Input[p].StatusPath[i];
    //                    if (st.Term != null) st.Term.Deriv.Data.SyncFromCPU();
    //                    if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
    //                    if (st.Score != null) st.Score.Deriv.SyncFromCPU();
    //                }
    //                Input[p].Results.Clear();
    //            }
    //        }
    //    }


    //    /// <summary>
    //    /// REINFORCE-WALK prediction Runner.
    //    /// </summary>
    //    class TopKPredictionRunner : StructRunner
    //    {
    //        int lsum = 0, lsum_filter = 0;
    //        int rsum = 0, rsum_filter = 0;
    //        int lp_n = 0, lp_n_filter = 0;
    //        int rp_n = 0, rp_n_filter = 0;
    //        int HitK = 10;

    //        int Iteration = 0;
    //        int SmpIdx = 0;
    //        List<GraphQueryData> Query { get; set; }
    //        EpisodicMemory Memory { get; set; }
    //        public TopKPredictionRunner(List<GraphQueryData> input, EpisodicMemory memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Query = input;
    //            Memory = memory;
    //            Iteration = 0;
    //        }

    //        public override void Init()
    //        {
    //            SmpIdx = 0;
    //            lsum = 0;
    //            lsum_filter = 0;
    //            rsum = 0;
    //            rsum_filter = 0;
    //            lp_n = 0;
    //            lp_n_filter = 0;
    //            rp_n = 0;
    //            rp_n_filter = 0;
    //        }

    //        public void Report()
    //        {
    //            Logger.WriteLog("Sample Idx {0}", SmpIdx);

    //            Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 2.0 / SmpIdx));

    //            Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 2.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 2.0 / SmpIdx));

    //            Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 1.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 1.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 1.0 / SmpIdx));
    //            Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 1.0 / SmpIdx));
    //        }

    //        public override void Complete()
    //        {
    //            Logger.WriteLog("Final Report!");
    //            {
    //                Report();
    //            }
    //            Iteration += 1;
    //        }
    //        public override void Forward()
    //        {
    //            Dictionary<int, float>[] scoreDict = new Dictionary<int, float>[Query[0].BatchSize];

    //            for (int i = 0; i < Query[0].BatchSize; i++)
    //            {
    //                scoreDict[i] = new Dictionary<int, float>();
    //            }

    //            for (int m = 0; m < Query.Count; m++)
    //            {
    //                for (int i = 0; i < Query[m].Results.Count; i++)
    //                {
    //                    int t = Query[m].Results[i].Item1;
    //                    int b = Query[m].Results[i].Item2;
    //                    int origialB = Query[m].StatusPath[t].GetOriginalStatsIndex(b);
    //                    int predId = Query[m].StatusPath[t].NodeID[b];

    //                    int targetId = Query[m].RawTarget[origialB];

    //                    float v = Query[m].StatusPath[t].Score.Output.Data.MemPtr[b];
    //                    //if (Query[m].BlackTargets[origialB].Contains(predId)) { continue; }

    //                    if (!scoreDict[origialB].ContainsKey(predId))
    //                    {
    //                        scoreDict[origialB][predId] = v;
    //                    }
    //                    else
    //                    {
    //                        scoreDict[origialB][predId] += v;
    //                    }
    //                }
    //            }


    //            for (int g = 0; g < Query[0].BatchSize; g++)
    //            {
    //                int relation = Query[0].RawQuery[g].Item2;
    //                int target = Query[0].RawTarget[g];
    //                HashSet<int> blackTargets = Query[0].BlackTargets[g];

    //                Dictionary<int, float> score = scoreDict[g];

    //                //for (int p = 0; p < Query.StatusPath.Count; p++)
    //                //{
    //                //    foreach (int b in Query.StatusPath[p].GetBatchIdxs(g))
    //                //    {
    //                //        int nidx = Query.StatusPath[p].NodeID[b];
    //                //        float logP = Query.StatusPath[p].GetLogProb(b);
    //                //        if (Query.StatusPath[p].IsTermable(b))
    //                //        {
    //                //            float s = logP + Query.StatusPath[p].LogPTerm(b, true);

    //                //            if (!score.ContainsKey(nidx)) { score[nidx] = float.MinValue; }
    //                //            score[nidx] = Util.LogAdd(score[nidx], s);
    //                //        }
    //                //    }
    //                //}
    //                var sortD = score.OrderByDescending(pair => pair.Value);
    //                int filter = 1;
    //                int nonFilter = 1;
    //                bool isFound = false;
    //                foreach (KeyValuePair<int, float> d in sortD)
    //                {
    //                    int ptail = d.Key;
    //                    if (ptail == target)
    //                    {
    //                        isFound = true;
    //                        break;
    //                    }
    //                    nonFilter += 1;
    //                    if (!blackTargets.Contains(ptail)) { filter++; }
    //                }

    //                if (!isFound)
    //                {
    //                    int tail = (DataPanel.EntityNum - nonFilter);
    //                    int topBlack = nonFilter - filter;
    //                    int tailBlack = blackTargets.Count - topBlack;
    //                    nonFilter = nonFilter + tail / 2;
    //                    filter = filter + (tail - tailBlack) / 2;
    //                }
    //                //else
    //                //{
    //                //    Console.WriteLine("Yes, I find it!");
    //                //}
    //                if (relation < DataPanel.RelationNum)
    //                {
    //                    Interlocked.Add(ref lsum, nonFilter);
    //                    Interlocked.Add(ref lsum_filter, filter);
    //                    if (nonFilter <= HitK) Interlocked.Increment(ref lp_n); // lp_n += 1;
    //                    if (filter <= HitK) Interlocked.Increment(ref lp_n_filter); // += 1;
    //                }
    //                else
    //                {
    //                    Interlocked.Add(ref rsum, nonFilter);
    //                    Interlocked.Add(ref rsum_filter, filter);
    //                    if (nonFilter <= HitK) Interlocked.Increment(ref rp_n); // lp_n += 1;
    //                    if (filter <= HitK) Interlocked.Increment(ref rp_n_filter); // += 1;
    //                }
    //            }
    //            Memory.Clear();
    //            //average ground truth results.
    //            for (int p = 0; p < Query.Count; p++)
    //            {
    //                Query[p].Results.Clear();
    //            }

    //            SmpIdx += Query[0].BatchSize;
    //        }
    //    }



    //    /// <summary>
    //    /// REINFORCE-WALK prediction Runner.
    //    /// </summary>
    //    //class TopKPredictionRunner : StructRunner
    //    //{
    //    //    int lsum = 0, lsum_filter = 0;
    //    //    int rsum = 0, rsum_filter = 0;
    //    //    int lp_n = 0, lp_n_filter = 0;
    //    //    int rp_n = 0, rp_n_filter = 0;
    //    //    int HitK = 10;

    //    //    int Iteration = 0;
    //    //    int SmpIdx = 0;
    //    //    GraphQueryData Query { get; set; }

    //    //    public TopKPredictionRunner(GraphQueryData query, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //    //    {
    //    //        Query = query;
    //    //        Iteration = 0;
    //    //    }

    //    //    public override void Init()
    //    //    {
    //    //        SmpIdx = 0;
    //    //        lsum = 0;
    //    //        lsum_filter = 0;
    //    //        rsum = 0;
    //    //        rsum_filter = 0;
    //    //        lp_n = 0;
    //    //        lp_n_filter = 0;
    //    //        rp_n = 0;
    //    //        rp_n_filter = 0;
    //    //    }

    //    //    public void Report()
    //    //    {
    //    //        Logger.WriteLog("Sample Idx {0}", SmpIdx);

    //    //        Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 2.0 / SmpIdx));
    //    //        Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 2.0 / SmpIdx));
    //    //        Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 2.0 / SmpIdx));
    //    //        Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 2.0 / SmpIdx));

    //    //        Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 2.0 / SmpIdx));
    //    //        Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 2.0 / SmpIdx));
    //    //        Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 2.0 / SmpIdx));
    //    //        Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 2.0 / SmpIdx));

    //    //        Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 1.0 / SmpIdx));
    //    //        Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 1.0 / SmpIdx));
    //    //        Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 1.0 / SmpIdx));
    //    //        Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 1.0 / SmpIdx));
    //    //    }

    //    //    public override void Complete()
    //    //    {
    //    //        Logger.WriteLog("Final Report!");
    //    //        {
    //    //            Report();
    //    //        }
    //    //        Iteration += 1;
    //    //    }
    //    //    public override void Forward()
    //    //    {
    //    //        for (int t = Query.StatusPath.Count - 1; t >= 0; t--)
    //    //        {
    //    //            if (Query.StatusPath[t].Term != null)
    //    //            {
    //    //                Query.StatusPath[t].Term.Output.Data.SyncToCPU();
    //    //            }
    //    //        }

    //    //        for (int g = 0; g < Query.BatchSize; g++)
    //    //        {
    //    //            int relation = Query.RawQuery[g].Item2;
    //    //            int target = Query.RawTarget[g];
    //    //            HashSet<int> blackTargets = Query.BlackTargets[g];

    //    //            Dictionary<int, float> score = new Dictionary<int, float>();

    //    //            for (int p = 0; p < Query.StatusPath.Count; p++)
    //    //            {
    //    //                foreach (int b in Query.StatusPath[p].GetBatchIdxs(g))
    //    //                {
    //    //                    int nidx = Query.StatusPath[p].NodeID[b];
    //    //                    float logP = Query.StatusPath[p].GetLogProb(b);
    //    //                    if (Query.StatusPath[p].IsTermable(b))
    //    //                    {
    //    //                        float s = logP + Query.StatusPath[p].LogPTerm(b, true);

    //    //                        if (!score.ContainsKey(nidx)) { score[nidx] = float.MinValue; }
    //    //                        score[nidx] = Util.LogAdd(score[nidx], s);
    //    //                    }
    //    //                }
    //    //            }
    //    //            var sortD = score.OrderByDescending(pair => pair.Value);
    //    //            int filter = 1;
    //    //            int nonFilter = 1;
    //    //            bool isFound = false;
    //    //            foreach (KeyValuePair<int, float> d in sortD)
    //    //            {
    //    //                int ptail = d.Key;

    //    //                if (ptail == target)
    //    //                {
    //    //                    isFound = true;
    //    //                    break;
    //    //                }
    //    //                nonFilter += 1;
    //    //                if (!blackTargets.Contains(ptail)) { filter++; }
    //    //            }

    //    //            if (!isFound)
    //    //            {
    //    //                int tail = (DataPanel.EntityNum - nonFilter);
    //    //                int topBlack = nonFilter - filter;
    //    //                int tailBlack = blackTargets.Count - topBlack;
    //    //                nonFilter = nonFilter + tail / 2;
    //    //                filter = filter + (tail - tailBlack) / 2;
    //    //            }
    //    //            //else
    //    //            //{
    //    //            //    Console.WriteLine("Yes, I find it!");
    //    //            //}
    //    //            if (relation < DataPanel.RelationNum)
    //    //            {
    //    //                Interlocked.Add(ref lsum, nonFilter);
    //    //                Interlocked.Add(ref lsum_filter, filter);
    //    //                if (nonFilter <= HitK) Interlocked.Increment(ref lp_n); // lp_n += 1;
    //    //                if (filter <= HitK) Interlocked.Increment(ref lp_n_filter); // += 1;
    //    //            }
    //    //            else
    //    //            {
    //    //                Interlocked.Add(ref rsum, nonFilter);
    //    //                Interlocked.Add(ref rsum_filter, filter);
    //    //                if (nonFilter <= HitK) Interlocked.Increment(ref rp_n); // lp_n += 1;
    //    //                if (filter <= HitK) Interlocked.Increment(ref rp_n_filter); // += 1;
    //    //            }
    //    //        }
    //    //        SmpIdx += Query.BatchSize;
    //    //    }
    //    //}

    //    public class NeuralWalkerModel : CompositeNNStructure
    //    {
    //        public EmbedStructure InNodeEmbed { get; set; }
    //        public EmbedStructure InRelEmbed { get; set; }

    //        public EmbedStructure CNodeEmbed { get; set; }
    //        public EmbedStructure CRelEmbed { get; set; }

    //        public DNNStructure SrcDNN { get; set; }
    //        public DNNStructure TgtDNN { get; set; }

    //        public LayerStructure AttEmbed { get; set; }
    //        public DNNStructure TermDNN { get; set; }

    //        public DNNStructure ScoreDNN { get; set; }
    //        public GRUCell GruCell { get; set; }

    //        public NeuralWalkerModel(int nodeDim, int relDim, DeviceType device)
    //        {
    //            InNodeEmbed = AddLayer(new EmbedStructure(DataPanel.EntityNum, nodeDim, device)); // DeviceType.CPU_FAST_VECTOR));
    //            InRelEmbed = AddLayer(new EmbedStructure(DataPanel.RelationNum * 2, relDim, device)); // DeviceType.CPU_FAST_VECTOR));
    //            CNodeEmbed = AddLayer(new EmbedStructure(DataPanel.EntityNum, nodeDim, device));// DeviceType.CPU_FAST_VECTOR));
    //            CRelEmbed = AddLayer(new EmbedStructure(DataPanel.RelationNum * 2, relDim, device)); // DeviceType.CPU_FAST_VECTOR));
    //            SrcDNN = AddLayer(new DNNStructure(nodeDim + relDim, BuilderParameters.DNN_DIMS,
    //                                       BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
    //                                       BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
    //                                       device));
    //            TgtDNN = AddLayer(new DNNStructure(nodeDim + relDim, BuilderParameters.DNN_DIMS,
    //                                       BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
    //                                       BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
    //                                       device));
    //            AttEmbed = AddLayer(new LayerStructure(BuilderParameters.DNN_DIMS.Last(), 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device));
    //            TermDNN = AddLayer(new DNNStructure(nodeDim + relDim, BuilderParameters.T_NET, BuilderParameters.T_AF,
    //                                     BuilderParameters.T_NET.Select(i => true).ToArray(), device));
    //            ScoreDNN = AddLayer(new DNNStructure(nodeDim + relDim, BuilderParameters.S_NET, BuilderParameters.S_AF,
    //                                     BuilderParameters.S_NET.Select(i => true).ToArray(), device));
    //            GruCell = AddLayer(new GRUCell(nodeDim + relDim, nodeDim + relDim, device));
    //        }
    //        public NeuralWalkerModel(BinaryReader reader, DeviceType device)
    //        {
    //            int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
    //            InNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
    //            InRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
    //            CNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
    //            CRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
    //            SrcDNN = (DNNStructure)DeserializeNextModel(reader, device);
    //            TgtDNN = (DNNStructure)DeserializeNextModel(reader, device);
    //            AttEmbed = (LayerStructure)DeserializeNextModel(reader, device);
    //            TermDNN = (DNNStructure)DeserializeNextModel(reader, device);
    //            ScoreDNN = (DNNStructure)DeserializeNextModel(reader, device);
    //            GruCell = (GRUCell)DeserializeNextModel(reader, device);
    //        }

    //        public void InitOptimization(RunnerBehavior behavior)
    //        {
    //            //InNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
    //            //InRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
    //            //CNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
    //            //CRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
    //            InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
    //        }
    //    }

    //    /// <summary>
    //    /// Build MCTS Computation Graph.
    //    /// </summary>
    //    /// <param name="graph"></param>
    //    /// <param name="batchSize"></param>
    //    /// <param name="beamSize"></param>
    //    /// <param name="model"></param>
    //    /// <param name="memory"></param>
    //    /// <param name="Behavior"></param>
    //    /// <returns></returns>
    //    public static ComputationGraph BuildComputationGraph(List<Tuple<int, int, int>> graph,
    //                                                         //List<List<Tuple<int, List<Tuple<int, int, int>>>>> path,
    //                                                         int batchSize, int mcts_num,
    //                                                         NeuralWalkerModel model, EpisodicMemory memory, RunnerBehavior Behavior)
    //    {

    //        ComputationGraph cg = new ComputationGraph();

    //        // sample a list of tuplet.
    //        // group of beamsize.
    //        SampleRunner SmpRunner = new SampleRunner(graph, batchSize, Behavior);
    //        cg.AddDataRunner(SmpRunner);
    //        GraphQueryData interface_data = SmpRunner.Output;

    //        // get the status embedding.
    //        StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, model.InNodeEmbed, model.InRelEmbed, Behavior);
    //        cg.AddRunner(statusEmbedRunner);

    //        List<GraphQueryData> Queries = new List<GraphQueryData>();
    //        for (int j = 0; j < mcts_num; j++)
    //        {
    //            GraphQueryData newQuery = new GraphQueryData(interface_data);
                
    //            // initial status information.
    //            StatusData status = new StatusData(newQuery, statusEmbedRunner.Output, Behavior.Device);
                
    //            // travel four steps in the knowledge graph.
    //            for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
    //            {
    //                #region get candidate actions and its probability. All the operatons are reflected to the StatusData.
    //                CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
    //                cg.AddRunner(candidateActionRunner);
    //                //return candidateActionRunner.Output : neighbors and BiMatchData.

    //                StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, model.CNodeEmbed, model.CRelEmbed, Behavior);
    //                cg.AddRunner(candEmbedRunner);

    //                DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
    //                cg.AddRunner(srcHiddenRunner);

    //                DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
    //                cg.AddRunner(candHiddenRunner);

    //                VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
    //                                                                            candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
    //                cg.AddRunner(attentionRunner);

    //                SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
    //                                                                            status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
    //                                                                            candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
    //                                                                            Behavior.Device), 1, Behavior,
    //                                                                            true);
    //                cg.AddRunner(normAttRunner);

    //                status.MatchCandidate = candidateActionRunner.Output;
    //                status.MatchCandidateProb = normAttRunner.Output;
    //                #endregion.

    //                #region Assign selected Action.
    //                MCTSActionSamplingRunner actionRunner = new MCTSActionSamplingRunner(status, memory, Behavior);
    //                cg.AddRunner(actionRunner);
    //                #endregion.

    //                MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, actionRunner.MatchPath, 1, Behavior);
    //                cg.AddRunner(srcExpRunner);

    //                MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, actionRunner.MatchPath, 2, Behavior);
    //                cg.AddRunner(tgtExpRunner);

    //                GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
    //                cg.AddRunner(stateRunner);

    //                StatusData nextStatus = new StatusData(status.GraphQuery,
    //                    actionRunner.NodeIndex, actionRunner.LogProb, actionRunner.PreSelActIndex, actionRunner.PreStatusIndex, actionRunner.StautsKey,
    //                    stateRunner.Output, Behavior.Device);

    //                DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, nextStatus.StateEmbed, Behavior);
    //                cg.AddRunner(termRunner);

    //                DNNRunner<HiddenBatchData> scoreRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, nextStatus.StateEmbed, Behavior);
    //                cg.AddRunner(scoreRunner);

    //                nextStatus.Term = termRunner.Output;
    //                nextStatus.Score = scoreRunner.Output;

    //                status = nextStatus;
    //            }

    //            RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(newQuery, memory, Behavior);
    //            cg.AddRunner(rewardFeedRunner);
    //            Queries.Add(newQuery);
    //        }

    //        if (Behavior.RunMode == DNNRunMode.Train)
    //        {
    //            RewardUpdateRunner rewardUpdateRunner = new RewardUpdateRunner(Queries, memory, Behavior);
    //            cg.AddObjective(rewardUpdateRunner);
    //        }
    //        else
    //        {
    //            TopKPredictionRunner predRunner = new TopKPredictionRunner(Queries, memory, Behavior);
    //            cg.AddRunner(predRunner);
    //        }
    //        cg.SetDelegateModel(model);
    //        return cg;
    //    }

    //    //public static ComputationGraph BuildPredComputationGraph(List<Tuple<int, int, int>> graph,
    //    //                                                    int batchSize, int beamSize,
    //    //                                                    NeuralWalkerModel model, RunnerBehavior Behavior)
    //    //{

    //    //    ComputationGraph cg = new ComputationGraph();

    //    //    // sample a list of tuplet.
    //    //    SampleRunner SmpRunner = new SampleRunner(graph, null, batchSize, 1, Behavior);
    //    //    cg.AddDataRunner(SmpRunner);
    //    //    GraphQueryData interface_data = SmpRunner.Output;

    //    //    // 16 maxbatchsize. 
    //    //    StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, model.InNodeEmbed, model.InRelEmbed, Behavior);
    //    //    cg.AddRunner(statusEmbedRunner);

    //    //    // 16 minibatches.
    //    //    StatusData status = new StatusData(interface_data, interface_data.RawSource, statusEmbedRunner.Output, Behavior.Device);

    //    //    // travel four steps in the knowledge graph.
    //    //    for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
    //    //    {
    //    //        //given status, obtain the match. miniBatch * maxNeighbor number.
    //    //        CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
    //    //        cg.AddRunner(candidateActionRunner);

    //    //        // miniMatch * maxNeighborNumber.
    //    //        StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, model.CNodeEmbed, model.CRelEmbed, Behavior);
    //    //        cg.AddRunner(candEmbedRunner);

    //    //        DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
    //    //        cg.AddRunner(srcHiddenRunner);

    //    //        DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
    //    //        cg.AddRunner(candHiddenRunner);

    //    //        // alignment miniBatch * miniBatch * neighbor
    //    //        VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
    //    //                                                                    candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified)
    //    //        { name = "align" + i.ToString() };
    //    //        cg.AddRunner(attentionRunner);

    //    //        SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
    //    //                                                                    status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
    //    //                                                                    candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
    //    //                                                                    Behavior.Device), 1, Behavior,
    //    //                                                                    true);
    //    //        cg.AddRunner(normAttRunner);

    //    //        status.MatchCandidate = candidateActionRunner.Output;
    //    //        status.MatchCandidateProb = normAttRunner.Output;
    //    //        //it will cause un-necessary unstable.

    //    //        //status.CandidateProb = normAttRunner.Output;
    //    //        BeamSearchActionRunner actionRunner = new BeamSearchActionRunner(status, BuilderParameters.BeamSize, Behavior);
    //    //        cg.AddRunner(actionRunner);

    //    //        // return:
    //    //        // MatchPath { get; set; }
    //    //        // List<int> NodeIndex { get; set; }
    //    //        // List<float> NodeProb { get; set; }
    //    //        // List<Tuple<int, int>> NodeAction { get; set; }

    //    //        MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, actionRunner.MatchPath, 1, Behavior);
    //    //        cg.AddRunner(srcExpRunner);

    //    //        MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, actionRunner.MatchPath, 2, Behavior);
    //    //        cg.AddRunner(tgtExpRunner);

    //    //        //StatusEmbedRunner actionEmbedRunner = new StatusEmbedRunner(actionRunner.SelectAction, status.MaxBatchSize, model.CNodeEmbed, model.CRelEmbed, Behavior);
    //    //        //cg.AddRunner(actionEmbedRunner);
    //    //        GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
    //    //        cg.AddRunner(stateRunner);

    //    //        StatusData nextStatus = new StatusData(status.GraphQuery,
    //    //            actionRunner.NodeIndex, actionRunner.GroupIndex, actionRunner.NodeProb, actionRunner.NodeAction,
    //    //            stateRunner.Output, Behavior.Device);

    //    //        DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, nextStatus.StateEmbed, Behavior);
    //    //        cg.AddRunner(termRunner);
    //    //        nextStatus.Term = termRunner.Output;
    //    //        status = nextStatus;
    //    //    }

    //    //    TopKPredictionRunner predRunner = new TopKPredictionRunner(interface_data, Behavior);
    //    //    cg.AddRunner(predRunner);

    //    //    cg.SetDelegateModel(model);
    //    //    return cg;
    //    //}

    //    public override void Rock()
    //    {
    //        Logger.OpenLog(BuilderParameters.LogFile);

    //        DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
    //        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

    //        Logger.WriteLog("Loading Training/Validation/Test Data.");
    //        DataPanel.Init();
    //        Logger.WriteLog("Load Data Finished.");

    //        NeuralWalkerModel model =
    //            BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
    //            new NeuralWalkerModel(BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, device) :
    //            new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
    //        model.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

    //        switch (BuilderParameters.RunMode)
    //        {
    //            case DNNRunMode.Train:
    //                if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);
    //                EpisodicMemory memory = new EpisodicMemory();
    //                ComputationGraph trainCG = BuildComputationGraph(DataPanel.knowledgeGraph.Valid, BuilderParameters.MiniBatchSize,
    //                    BuilderParameters.MCTS_NUM, model, memory, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    
    //                ComputationGraph predCG = BuildComputationGraph(DataPanel.knowledgeGraph.Test, BuilderParameters.TestMiniBatchSize,
    //                    BuilderParameters.BeamSize, model, memory, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
    //                //predCG.Execute();
    //                for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
    //                {
    //                    //ActionSampler = new Random(10);
    //                    double loss = trainCG.Execute();
    //                    Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
    //                    if ((iter + 1) % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
    //                    {
    //                        Logger.WriteLog("Evaluation at Iteration {0}", iter);
    //                        predCG.Execute();
    //                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "model." + iter.ToString(), FileMode.Create, FileAccess.Write)))
    //                        {
    //                            model.Serialize(writer);
    //                        }
    //                    }
    //                }
    //                break;
    //            case DNNRunMode.Predict:
    //                //ComputationGraph predCG = BuildComputationGraph(DataPanel.knowledgeGraph.Test, null, BuilderParameters.MiniBatchSize,
    //                //    BuilderParameters.BeamSize, model, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
    //                break;

    //        }
    //        Logger.CloseLog();
    //    }

    //    /// <summary>
    //    /// Suppose no path is needed.
    //    /// </summary>
    //    public class DataPanel
    //    {
    //        public static List<List<string>> TrainQ = new List<List<string>>();
    //        public static List<List<string>> TrainInput = new List<List<string>>();
    //        public static List<List<string>> TrainOutput = new List<List<string>>();



    //        public static List<Tuple<int, int, int>> Test
    //        {
    //            get { return knowledgeGraph.Test; }
    //        }
    //        public static List<Tuple<int, int, int>> Valid
    //        {
    //            get { return knowledgeGraph.Valid; }
    //        }
    //        /// <summary>
    //        /// start end relation
    //        /// </summary>
    //        public static List<List<Tuple<int, List<Tuple<int, int, int>>>>> ValidPath = new List<List<Tuple<int, List<Tuple<int, int, int>>>>>();
    //        public static int ReverseRelation(int relation)
    //        {
    //            if (relation >= DataPanel.RelationNum) { return relation - DataPanel.RelationNum; }
    //            else { return relation + DataPanel.RelationNum; }
    //        }
    //        public static List<Tuple<int, int, int>> ReversePath(List<Tuple<int, int, int>> path)
    //        {
    //            List<Tuple<int, int, int>> result = new List<Tuple<int, int, int>>();

    //            for (int i = path.Count - 1; i >= 0; i--)
    //            {
    //                result.Add(new Tuple<int, int, int>(path[i].Item2, path[i].Item1, ReverseRelation(path[i].Item3)));

    //                int n1 = path[i].Item2;
    //                int n2 = path[i].Item1;
    //                int r = ReverseRelation(path[i].Item3);
    //                if (n1 == 10565 && r == 10565 && n2 == 4187)
    //                {
    //                    Console.WriteLine("Find the issue in reverse path ");
    //                    throw new Exception("Error!!");
    //                }
    //            }
    //            return result;
    //        }
    //        static void LoadSamplePath(string pathfolder)
    //        {
    //            for (int i = 0; i < Valid.Count; i++)
    //            {
    //                ValidPath.Add(new List<Tuple<int, List<Tuple<int, int, int>>>>());
    //            }

    //            string[] fileEntries = Directory.GetFiles(pathfolder);
    //            foreach (string mpathfile in fileEntries)
    //            {
    //                int pathIdx = int.Parse(mpathfile.Substring(mpathfile.LastIndexOf('.') + 1));

    //                using (StreamReader mreader = new StreamReader(mpathfile))
    //                {
    //                    int line = 0;

    //                    while (!mreader.EndOfStream)
    //                    {
    //                        string[] stops = mreader.ReadLine().Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries);
    //                        if (stops.Length >= 1)
    //                        {
    //                            List<Tuple<int, int, int>> path = new List<Tuple<int, int, int>>();
    //                            foreach (string stop in stops)
    //                            {
    //                                string[] tuple = stop.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
    //                                int n1 = int.Parse(tuple[0]);
    //                                int r = int.Parse(tuple[1]);
    //                                int n2 = int.Parse(tuple[2]);
    //                                path.Add(new Tuple<int, int, int>(n1, n2, r));

    //                                if (n1 == 10565 && r == 10565 && n2 == 4187)
    //                                {
    //                                    Console.WriteLine("Find the issue {0}, {1}", mpathfile, line);
    //                                    throw new Exception("Error!!");
    //                                }
    //                            }
    //                            ValidPath[line].Add(new Tuple<int, List<Tuple<int, int, int>>>(pathIdx, path));
    //                        }
    //                        line = line + 1;
    //                    }
    //                }
    //                pathIdx += 1;
    //            }
    //        }
    //        public static double[] HeadProb { get { return knowledgeGraph.HeadProb; } }
    //        public static double[] TailProb { get { return knowledgeGraph.TailProb; } }
    //        public static int MaxNeighborCount { get { return knowledgeGraph.NeighborLink.Select(i => i.Value.Count).Max(); } }
           
    //        public static void Init()
    //        {
    //            knowledgeGraph = new RelationGraphData();
    //            if (!BuilderParameters.Entity2Id.Equals(string.Empty)) knowledgeGraph.EntityId = RelationGraphData.LoadMapId(BuilderParameters.Entity2Id);
    //            else knowledgeGraph.EntityNum = BuilderParameters.EntityNum;
    //            if (!BuilderParameters.Relation2Id.Equals(string.Empty)) knowledgeGraph.RelationId = RelationGraphData.LoadMapId(BuilderParameters.Relation2Id);
    //            else knowledgeGraph.RelationNum = BuilderParameters.RelationNum;

    //            knowledgeGraph.Train = knowledgeGraph.LoadGraphV2(BuilderParameters.TrainData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, true);
    //            if (BuilderParameters.IsValidFile) knowledgeGraph.Valid = knowledgeGraph.LoadGraphV2(BuilderParameters.ValidData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);
    //            if (BuilderParameters.IsTestFile) knowledgeGraph.Test = knowledgeGraph.LoadGraphV2(BuilderParameters.TestData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);

    //            LoadSamplePath(BuilderParameters.ValidPathFolder);
    //            int emptyPath = ValidPath.Select(i => i.Count == 0 ? 1 : 0).Sum();

    //            Console.WriteLine("Entity Number {0}, Relation Number {1}", knowledgeGraph.EntityNum, knowledgeGraph.RelationNum);
    //            Console.WriteLine("Valid Empty Path {0}, Valid Total Path {1}", emptyPath, ValidPath.Count);
    //        }
    //    }
    //}
}
