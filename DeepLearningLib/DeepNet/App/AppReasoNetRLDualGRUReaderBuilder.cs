﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class AppReasoNetRLDualGRUReaderBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));
                Argument.Add("VOCAB-SIZE", new ParameterArgument("50000", "Vocab Size"));

                Argument.Add("TRAIN-FOLDER", new ParameterArgument(string.Empty, "Training Folder"));
                Argument.Add("TEST-FOLDER", new ParameterArgument(string.Empty, "Testing Folder"));
                Argument.Add("VALID-FOLDER", new ParameterArgument(string.Empty, "Validation Folder"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));

                Argument.Add("EMBED-LAYER-DIM", new ParameterArgument("300,300", "DNN Layer Dim"));
                Argument.Add("EMBED-LAYER-WIN", new ParameterArgument("5,5", "DNN Layer WindowSize"));
                Argument.Add("EMBED-LAYER-AF", new ParameterArgument("1,1", "DNN Layer AF" + ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("EMBED-LAYER-DROPOUT", new ParameterArgument("0,0", "DNN Layer Dropout."));
                Argument.Add("WORD-EMBEDDING", new ParameterArgument(string.Empty, "Word Embedding File"));

                Argument.Add("GRU-DIM", new ParameterArgument("300", "GRU Dimension"));
                Argument.Add("IS-SHARE-RNN", new ParameterArgument("0", "0 : non-share; 1 : share"));
                Argument.Add("IS-ENTITY-SHUFFLE", new ParameterArgument("0", "0 : nonShuffleEntity; 1 : ShuffleEntity."));

                Argument.Add("ATT-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ATT-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));
                Argument.Add("IS-ENTITY-ATT", new ParameterArgument("1", "1 : Entity Memory;  0 : Full Memory;"));

                Argument.Add("ATT2-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ATT2-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

                Argument.Add("ANS-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ANS-TYPE", new ParameterArgument(((int)CrossSimType.Cosine).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("3", "Recurrent Steps"));
                Argument.Add("RECURRENT-POOL", new ParameterArgument((((int)PoolingType.RL)).ToString(), "Recurrent Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));
                Argument.Add("QUERY-POOL", new ParameterArgument((((int)PoolingType.LAST)).ToString(), "Query Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));

                // Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("K-SHUFFLE", new ParameterArgument("1", " Maintain latest K-Shuffle."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

                Argument.Add("DEBUG-FILE", new ParameterArgument(string.Empty, "DEBUG File."));
                Argument.Add("R-INIT", new ParameterArgument(((int)RndRecurrentInit.RndNorm).ToString(), "Recurrent Weight Init."));

                Argument.Add("RL-ALL", new ParameterArgument("0", "RL All"));
                Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));
                Argument.Add("PRED-RL", new ParameterArgument((((int)PredType.RL_MAXITER)).ToString(), "RL Pred Type : " + ParameterUtil.EnumValues(typeof(PredType))));
                Argument.Add("IS-NORM-REWARD", new ParameterArgument("0", "0 : No Norm Reward; 1 : Norm Reward."));
            }

            /// <summary>
            /// DNN Run Mode.
            /// </summary>
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string Vocab { get { return Argument["VOCAB"].Value == string.Empty ? string.Format("{0}.{1}.vocab", TrainFolder, VocabSize) : Argument["VOCAB"].Value; } }
            public static int VocabSize { get { return int.Parse(Argument["VOCAB-SIZE"].Value); } }

            #region train, test, validation dataset.
            public static string TrainFolder { get { return Argument["TRAIN-FOLDER"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTrainFile { get { return (!TrainFolder.Equals(string.Empty)); } }
            public static string TrainIndexData { get { return string.Format("{0}.{1}.idx.tsv", TrainFolder, VocabSize); } }
            public static string TrainContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", TrainFolder, VocabSize, MiniBatchSize); } }
            public static string TrainQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", TrainFolder, VocabSize, MiniBatchSize); } }
            public static string TrainAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", TrainFolder, VocabSize, MiniBatchSize); } }

            public static string TestFolder { get { return Argument["TEST-FOLDER"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTestFile { get { return (!TestFolder.Equals(string.Empty)); } }
            public static string TestIndexData { get { return string.Format("{0}.{1}.idx.tsv", TestFolder, VocabSize); } }
            public static string TestContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", TestFolder, VocabSize, MiniBatchSize); } }
            public static string TestQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", TestFolder, VocabSize, MiniBatchSize); } }
            public static string TestAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", TestFolder, VocabSize, MiniBatchSize); } }

            public static string ValidFolder { get { return Argument["VALID-FOLDER"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsValidFile { get { return (!ValidFolder.Equals(string.Empty)); } }
            public static string ValidIndexData { get { return string.Format("{0}.{1}.idx.tsv", ValidFolder, VocabSize); } }
            public static string ValidContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            public static string ValidQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            public static string ValidAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            #endregion.

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static int[] EMBED_LAYER_DIM { get { return Argument["EMBED-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] EMBED_ACTIVATION { get { return Argument["EMBED-LAYER-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] EMBED_LAYER_WIN { get { return Argument["EMBED-LAYER-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] EMBED_DROPOUT
            {
                get
                {
                    if (Argument["EMBED-LAYER-DROPOUT"].Value.Equals(string.Empty)) return EMBED_LAYER_DIM.Select(i => 0.0f).ToArray();
                    else return Argument["EMBED-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray();
                }
            }
            public static string InitWordEmbedding { get { return Argument["WORD-EMBEDDING"].Value; } }

            public static int GRU_DIM { get { return int.Parse(Argument["GRU-DIM"].Value); } }
            public static bool IS_SHARE_RNN { get { return int.Parse(Argument["IS-SHARE-RNN"].Value) > 0; } }

            public static bool IS_ENTITY_SHUFFLE { get { return int.Parse(Argument["IS-ENTITY-SHUFFLE"].Value) > 0; } }
            //public static bool IS_ENTITY_LSTM { get { return int.Parse(Argument["IS-ENTITY-LSTM"].Value) > 0; } }
            public static Recurrent_Unit Recurrent_Gate_Type { get { return (Recurrent_Unit)int.Parse(Argument["RECURRENT-GATE-TYPE"].Value); } }

            public static int ATT_HID_DIM { get { return int.Parse(Argument["ATT-HID"].Value); } }
            public static CrossSimType AttType { get { return (CrossSimType)int.Parse(Argument["ATT-TYPE"].Value); } }
            public static bool IS_ENTITY_ATT { get { return int.Parse(Argument["IS-ENTITY-ATT"].Value) > 0; } }

            public static int ATT2_HID_DIM { get { return int.Parse(Argument["ATT2-HID"].Value); } }
            public static CrossSimType Att2Type { get { return (CrossSimType)int.Parse(Argument["ATT2-TYPE"].Value); } }

            public static int ANS_HID_DIM { get { return int.Parse(Argument["ANS-HID"].Value); } }
            public static CrossSimType AnsType { get { return (CrossSimType)int.Parse(Argument["ANS-TYPE"].Value); } }

            public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            public static PoolingType RECURRENT_POOL { get { return (PoolingType)int.Parse(Argument["RECURRENT-POOL"].Value); } }
            public static PoolingType QUERY_POOL { get { return (PoolingType)int.Parse(Argument["QUERY-POOL"].Value); } }
            public static bool RL_ALL { get { return int.Parse(Argument["RL-ALL"].Value) > 0; } }
            public static float RL_DISCOUNT { get { return float.Parse(Argument["RL-DISCOUNT"].Value); } }

            public static PredType PRED_RL { get { return (PredType)int.Parse(Argument["PRED-RL"].Value); } }
            public static bool IS_NORM_REWARD { get { return int.Parse(Argument["IS-NORM-REWARD"].Value) > 0; } }
            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static int K_SHUFFLE { get { return int.Parse(Argument["K-SHUFFLE"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

            public static string DebugFile { get { return Argument["DEBUG-FILE"].Value; } }
            public static RndRecurrentInit RndInit { get { return (RndRecurrentInit)int.Parse(Argument["R-INIT"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.APP_REASONET_RL_GRU_DUAL_READER; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }
        enum PredType { RL_MAXITER, RL_AVGSIM, RL_AVGPROB }
        
        /// <summary>
        /// Memory Retrieval Runner.
        /// </summary>
        class MemoryRetrievalRunner : StructRunner
        {
            HiddenBatchData Address { get; set; }
            public HiddenBatchData SoftmaxAddress { get; set; }

            SeqDenseBatchData Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }
            float Gamma { get; set; }
            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            public MemoryRetrievalRunner(HiddenBatchData address, SeqDenseBatchData memory, BiMatchBatchData matchData, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Address = address;
                Memory = memory;
                MatchData = matchData;
                Gamma = gamma;
                SoftmaxAddress = new HiddenBatchData(Address.MAX_BATCHSIZE, Address.Dim, Behavior.RunMode, Behavior.Device);
                Output = new HiddenBatchData(MatchData.Stat.MAX_SRC_BATCHSIZE, Memory.Dim, Behavior.RunMode, Behavior.Device);
            }

            public override void Forward()
            {
                Output.BatchSize = MatchData.SrcSize;

                // softmax 
                // softmax of attention
                ComputeLib.SparseSoftmax(MatchData.Src2MatchIdx, Address.Output.Data, SoftmaxAddress.Output.Data, Gamma, MatchData.SrcSize);

                ComputeLib.ColumnWiseSumMask(Memory.SentOutput, 0, MatchData.TgtIdx, 0,
                    SoftmaxAddress.Output.Data, MatchData.Src2MatchIdx, MatchData.SrcSize,
                    Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                    Memory.SentSize, Memory.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                //float oDeriv_norm2 = ComputeLib.L2Norm(Output.Deriv.Data, Output.Dim * Output.BatchSize);

                // outputDeriv -> memoryContentDeriv.
                ComputeLib.Scale_MatrixMask(Output.Deriv.Data, 0, MatchData.SrcIdx, 0,
                                            Memory.SentDeriv, 0, MatchData.TgtIdx, 0,
                                            Memory.Dim, MatchData.MatchSize, SoftmaxAddress.Output.Data, 1);

                //float aDeriv_norm2 = ComputeLib.L2Norm(SoftmaxAddress.Output.Data, SoftmaxAddress.Dim * Memory.SentSize);
                //float mDeriv_norm2 = ComputeLib.L2Norm(Memory.SentDeriv, Memory.Dim * Memory.SentSize);
                //Memory.SentDeriv.SyncToCPU();
                //SoftmaxAddress.Output.Data.SyncToCPU();
                //Output.Deriv.Data.SyncToCPU();

                // outputDeriv -> AddressDeriv.
                ComputeLib.Inner_Product_Matching(Output.Deriv.Data, 0, Memory.SentOutput, 0, SoftmaxAddress.Deriv.Data, 0,
                    MatchData.SrcIdx, MatchData.TgtIdx, Output.BatchSize, Memory.SentSize, MatchData.MatchSize,
                    Memory.Dim, Util.GPUEpsilon);

                ComputeLib.DerivSparseMultiClassSoftmax(MatchData.Src2MatchIdx,
                    SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, Gamma, MatchData.SrcSize);

                ComputeLib.Add_Vector(Address.Deriv.Data, SoftmaxAddress.Deriv.Data, MatchData.MatchSize, 1, 1);
            }
        }

        /// <summary>
        /// GRU Query Runner.
        /// </summary>
        public class GRUQueryRunner : StructRunner<GRUCell, HiddenBatchData>
        {
            public HiddenBatchData Query { get; set; }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            HiddenBatchData GateR; //reset gate
            HiddenBatchData GateZ; //update gate
            HiddenBatchData HHat; //new memory
            HiddenBatchData RestH; //new memory

            CudaPieceFloat Ones;
            CudaPieceFloat Tmp;
            public GRUQueryRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
            {
                Query = query;

                Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

                GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

                Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
                Ones.Init(1);

                Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
            }

            public override void Forward()
            {
                //SubQuery.BatchSize = 1;
                /*Wr X -> GateR*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateR*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Query.BatchSize, Model.HiddenStateDim);

                /*Wz X -> GateZ*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateZ*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Query.BatchSize, Model.HiddenStateDim);

                /*Wh X -> HHat*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Query.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Query.BatchSize, Model.HiddenStateDim);

                Output.BatchSize = Query.BatchSize;

                //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
                // tmp = (1 - GateZ(t))
                ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
                                               GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

                // NewQuery = GateZ(t) * HHat
                ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);

                // NewQuery += tmp * h_t-1
                ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Query.BatchSize, Model.HiddenStateDim, 1);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
                ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Query.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

                ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);

                ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
                ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);

                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.BeforeGradient();
                    Model.WrMatrixOptimizer.BeforeGradient();
                    Model.WzMatrixOptimizer.BeforeGradient();

                    Model.UhMatrixOptimizer.BeforeGradient();
                    Model.UrMatrixOptimizer.BeforeGradient();
                    Model.UzMatrixOptimizer.BeforeGradient();

                    Model.BhMatrixOptimizer.BeforeGradient();
                    Model.BrMatrixOptimizer.BeforeGradient();
                    Model.BzMatrixOptimizer.BeforeGradient();

                }
                ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);

                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.AfterGradient();
                    Model.WrMatrixOptimizer.AfterGradient();
                    Model.WzMatrixOptimizer.AfterGradient();

                    Model.UhMatrixOptimizer.AfterGradient();
                    Model.UrMatrixOptimizer.AfterGradient();
                    Model.UzMatrixOptimizer.AfterGradient();

                    Model.BhMatrixOptimizer.AfterGradient();
                    Model.BrMatrixOptimizer.AfterGradient();
                    Model.BzMatrixOptimizer.AfterGradient();
                }
            }
        }

        /// <summary>
        /// Fix Iteration, SoftAttention, Supervised Training.
        /// </summary>
        class ReasonNetRunner : StructRunner
        {
            /// <summary>
            /// Input Query;
            /// </summary>
            HiddenBatchData InitStatus { get; set; }

            SeqDenseBatchData Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }

            SeqDenseBatchData Memory2 { get; set; }
            BiMatchBatchData MatchData2 { get; set; }

            SeqDenseBatchData AnsMem { get; set; }
            BiMatchBatchData AnsMatchData { get; set; }

            PoolingType ReasonPool = BuilderParameters.RECURRENT_POOL;

            string DebugFile { get; set; }
            StreamWriter DebugWriter = null;
            int DebugSample = 0;
            int DebugIter = 0;

            /// <summary>
            /// maximum iteration number.
            /// </summary>
            int MaxIterationNum { get; set; }
            public ReasonNetRunner(
                HiddenBatchData initStatus, int maxIterateNum,
                SeqDenseBatchData mem, BiMatchBatchData matchData, MLPAttentionStructure attStruct,
                SeqDenseBatchData mem2, BiMatchBatchData matchData2, MLPAttentionStructure att2Struct,
                SeqDenseBatchData ansMem, BiMatchBatchData ansMatchData, MLPAttentionStructure ansStruct,
                GRUCell gruCell, LayerStructure termStruct, float gamma, RunnerBehavior behavior, string debugFile = "") : base(Structure.Empty, behavior)
            {
                InitStatus = initStatus;
                Memory = mem;
                MatchData = matchData;
                Memory2 = mem2;
                MatchData2 = matchData2;
                AnsMem = ansMem;
                AnsMatchData = ansMatchData;
                MaxIterationNum = maxIterateNum;

                GruCell = gruCell;
                AttStruct = attStruct;
                Att2Struct = att2Struct;
                AnsStruct = ansStruct;
                TermStruct = termStruct;

                DebugFile = debugFile;

                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    AttHidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = Memory.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = Memory.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = attStruct.HiddenDim
                    }, Memory.SampleIdx, Memory.SentMargin, Behavior.Device);

                    SoftLinearSimRunner initAttRunner = new SoftLinearSimRunner(InitStatus, AttHidden, MatchData, BuilderParameters.AttType, AttStruct, Behavior);
                    AttRunner.Add(initAttRunner);
                }
                else if (BuilderParameters.AttType == CrossSimType.Cosine)
                {
                    CosineSimilarityRunner initAttRunner = new CosineSimilarityRunner(InitStatus, Memory, MatchData, Behavior);
                    AttRunner.Add(initAttRunner);
                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    Att2Hidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = Memory2.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = Memory2.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = att2Struct.HiddenDim
                    }, Memory2.SampleIdx, Memory2.SentMargin, Behavior.Device);

                    SoftLinearSimRunner initAtt2Runner = new SoftLinearSimRunner(InitStatus, Att2Hidden, MatchData2, BuilderParameters.Att2Type, Att2Struct, Behavior);
                    Att2Runner.Add(initAtt2Runner);
                }
                else if (BuilderParameters.Att2Type == CrossSimType.Cosine)
                {
                    CosineSimilarityRunner initAtt2Runner = new CosineSimilarityRunner(InitStatus, Memory2, MatchData2, Behavior);
                    Att2Runner.Add(initAtt2Runner);
                }

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    AnsHidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = AnsMem.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = AnsMem.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = ansStruct.HiddenDim
                    }, AnsMem.SampleIdx, AnsMem.SentMargin, Behavior.Device);

                    SoftLinearSimRunner initAnsRunner = new SoftLinearSimRunner(InitStatus, AnsHidden, AnsMatchData, BuilderParameters.AnsType, AnsStruct, Behavior);
                    AnsRunner.Add(initAnsRunner);
                }
                else if (BuilderParameters.AnsType == CrossSimType.Cosine)
                {
                    CosineSimilarityRunner initAnsRunner = new CosineSimilarityRunner(InitStatus, AnsMem, AnsMatchData, Behavior);
                    AnsRunner.Add(initAnsRunner);
                }

                HiddenBatchData lastStatus = InitStatus;
                TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(termStruct, lastStatus, Behavior));


                HiddenBatchData lastAtt = (HiddenBatchData)AttRunner[0].Output;
                HiddenBatchData lastAtt2 = (HiddenBatchData)Att2Runner[0].Output;

                for (int i = 0; i < maxIterateNum - 1; i++)
                {
                    MemoryRetrievalRunner xRunner = new MemoryRetrievalRunner(lastAtt, Memory, MatchData, gamma, Behavior);
                    MemRetrievalRunner.Add(xRunner);

                    MemoryRetrievalRunner x2Runner = new MemoryRetrievalRunner(lastAtt2, Memory2, MatchData2, gamma, Behavior);
                    MemRetrieval2Runner.Add(x2Runner);

                    EnsembleMatrixRunner eRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { xRunner.Output, x2Runner.Output }, Behavior);
                    RetrievalEnsembleRunner.Add(eRunner);

                    GRUQueryRunner yRunner = new GRUQueryRunner(gruCell, lastStatus, eRunner.Output, Behavior);
                    StatusRunner.Add(yRunner);
                    lastStatus = yRunner.Output;

                    if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                    {
                        SoftLinearSimRunner attRunner = new SoftLinearSimRunner(lastStatus, AttHidden, MatchData, BuilderParameters.AttType, AttStruct, Behavior);
                        AttRunner.Add(attRunner);
                        lastAtt = attRunner.Output;
                    }
                    else if (BuilderParameters.AttType == CrossSimType.Cosine)
                    {
                        CosineSimilarityRunner attRunner = new CosineSimilarityRunner(lastStatus, Memory, MatchData, Behavior);
                        AttRunner.Add(attRunner);
                        lastAtt = attRunner.Output;
                    }

                    if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                    {
                        SoftLinearSimRunner att2Runner = new SoftLinearSimRunner(lastStatus, Att2Hidden, MatchData2, BuilderParameters.Att2Type, Att2Struct, Behavior);
                        Att2Runner.Add(att2Runner);
                        lastAtt2 = att2Runner.Output;
                    }
                    else if (BuilderParameters.Att2Type == CrossSimType.Cosine)
                    {
                        CosineSimilarityRunner att2Runner = new CosineSimilarityRunner(lastStatus, Memory2, MatchData2, Behavior);
                        Att2Runner.Add(att2Runner);
                        lastAtt2 = att2Runner.Output;
                    }

                    if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                    {
                        SoftLinearSimRunner ansRunner = new SoftLinearSimRunner(lastStatus, AnsHidden, AnsMatchData, BuilderParameters.AnsType, AnsStruct, Behavior);
                        AnsRunner.Add(ansRunner);
                    }
                    else if (BuilderParameters.AnsType == CrossSimType.Cosine)
                    {
                        CosineSimilarityRunner ansRunner = new CosineSimilarityRunner(lastStatus, AnsMem, AnsMatchData, Behavior);
                        AnsRunner.Add(ansRunner);
                    }

                    TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(termStruct, lastStatus, Behavior));
                }

                if (ReasonPool == PoolingType.AVG || ReasonPool == PoolingType.ATT || ReasonPool == PoolingType.TREE)
                {
                    FinalAns = new HiddenBatchData(AnsMatchData.Stat.MAX_MATCH_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
                    TerminalProb = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, MaxIterationNum, DNNRunMode.Train, Behavior.Device);
                }
                else if (ReasonPool == PoolingType.LAST)
                {
                    FinalAns = AnsData.Last();
                }
                else if (ReasonPool == PoolingType.RL)
                {
                    AnswerProb = new HiddenBatchData[MaxIterationNum];
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i] = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
                    }
                    FinalAns = new HiddenBatchData(AnsMatchData.Stat.MAX_MATCH_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
                }
            }

            GRUCell GruCell = null;
            MLPAttentionStructure AttStruct = null;
            MLPAttentionStructure Att2Struct = null;
            MLPAttentionStructure AnsStruct = null;
            LayerStructure TermStruct = null;

            SeqDenseBatchData Att2Hidden = null;
            List<StructRunner> Att2Runner = new List<StructRunner>();

            SeqDenseBatchData AttHidden = null;
            List<StructRunner> AttRunner = new List<StructRunner>();

            SeqDenseBatchData AnsHidden = null;
            List<StructRunner> AnsRunner = new List<StructRunner>();

            List<FullyConnectHiddenRunner<HiddenBatchData>> TerminalRunner = new List<FullyConnectHiddenRunner<HiddenBatchData>>();

            List<MemoryRetrievalRunner> MemRetrievalRunner = new List<MemoryRetrievalRunner>();
            List<MemoryRetrievalRunner> MemRetrieval2Runner = new List<MemoryRetrievalRunner>();
            List<EnsembleMatrixRunner> RetrievalEnsembleRunner = new List<EnsembleMatrixRunner>();

            List<GRUQueryRunner> StatusRunner = new List<GRUQueryRunner>();

            public HiddenBatchData[] AnsData { get { return AnsRunner.Select(i => (HiddenBatchData)i.Output).ToArray(); } }
            public HiddenBatchData[] AnswerProb = null;

            public HiddenBatchData TerminalProb = null;
            public HiddenBatchData FinalAns = null;

            /// <summary>
            /// Output Debug File.
            /// </summary>
            public override void Init()
            {
                if (DebugFile != "" && !DebugFile.Equals(string.Empty))
                {
                    DebugWriter = new StreamWriter(DebugFile + DateTime.Now.ToFileTime().ToString() + "." + DebugIter.ToString() + ".iter");
                    DebugSample = 0;
                    DebugIter += 1;
                }
            }

            /// <summary>
            /// Save Debug File.
            /// </summary>
            public override void Complete()
            {
                if (DebugWriter != null)
                {
                    DebugWriter.Close();
                }
            }

            public override void Forward()
            {
                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    AttHidden.BatchSize = Memory.BatchSize;
                    AttHidden.SentSize = Memory.SentSize;
                    ComputeLib.Sgemm(Memory.SentOutput, 0, AttStruct.Wm, 0, AttHidden.SentOutput, 0,
                                     Memory.SentSize, Memory.Dim, AttStruct.HiddenDim, 0, 1, false, false);
                    if (AttStruct.IsBias) ComputeLib.Matrix_Add_Linear(AttHidden.SentOutput, AttStruct.B, Memory.SentSize, AttStruct.HiddenDim);
                }
                AttRunner[0].Forward();

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    Att2Hidden.BatchSize = Memory2.BatchSize;
                    Att2Hidden.SentSize = Memory2.SentSize;
                    ComputeLib.Sgemm(Memory2.SentOutput, 0, Att2Struct.Wm, 0, Att2Hidden.SentOutput, 0,
                                     Memory2.SentSize, Memory2.Dim, Att2Struct.HiddenDim, 0, 1, false, false);
                    if (Att2Struct.IsBias) ComputeLib.Matrix_Add_Linear(Att2Hidden.SentOutput, Att2Struct.B, Memory2.SentSize, Att2Struct.HiddenDim);
                }
                Att2Runner[0].Forward();

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    AnsHidden.BatchSize = AnsMem.BatchSize;
                    AnsHidden.SentSize = AnsMem.SentSize;
                    ComputeLib.Sgemm(AnsMem.SentOutput, 0, AnsStruct.Wm, 0, AnsHidden.SentOutput, 0,
                                     AnsMem.SentSize, AnsMem.Dim, AnsStruct.HiddenDim, 0, 1, false, false);
                    if (AnsStruct.IsBias) ComputeLib.Matrix_Add_Linear(AnsHidden.SentOutput, AnsStruct.B, AnsMem.SentSize, AnsStruct.HiddenDim);
                }
                AnsRunner[0].Forward();

                TerminalRunner[0].Forward();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Forward();

                    MemRetrieval2Runner[i].Forward();

                    RetrievalEnsembleRunner[i].Forward();

                    // update Status.
                    StatusRunner[i].Forward();
                    // take attention
                    AttRunner[i + 1].Forward();
                    // take attention 2.
                    Att2Runner[i + 1].Forward();
                    // take answer.
                    AnsRunner[i + 1].Forward();
                    // take terminal gate or not.
                    TerminalRunner[i + 1].Forward();
                }

                #region Avg Pooling.
                if (ReasonPool == PoolingType.AVG)
                {
                    FinalAns.BatchSize = AnsData[0].BatchSize;
                    Array.Clear(FinalAns.Output.Data.MemPtr, 0, FinalAns.BatchSize);
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnsData[i].Output.Data.SyncToCPU(AnsData[i].BatchSize);

                        for (int b = 0; b < AnsMatchData.SrcSize; b++)
                        {
                            int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];
                            float p = 1.0f / MaxIterationNum; //TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i];

                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                FinalAns.Output.Data.MemPtr[m] += p * AnsData[i].Output.Data.MemPtr[m];
                            }
                        }
                    }
                    FinalAns.Output.Data.SyncFromCPU(FinalAns.BatchSize);
                }
                #endregion.

                #region Att Pooling.
                else if (ReasonPool == PoolingType.ATT)
                {
                    TerminalProb.BatchSize = TerminalRunner[0].Output.BatchSize;
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        TerminalRunner[i].Output.Output.Data.SyncToCPU(TerminalRunner[i].Output.BatchSize);
                        for (int b = 0; b < TerminalRunner[i].Output.BatchSize; b++)
                        {
                            TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i] = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                        }
                    }
                    TerminalProb.Output.Data.SyncFromCPU(TerminalProb.BatchSize * MaxIterationNum);
                    ComputeLib.SoftMax(TerminalProb.Output.Data, TerminalProb.Output.Data, MaxIterationNum, TerminalProb.BatchSize, 1);
                    TerminalProb.Output.Data.SyncToCPU(TerminalProb.BatchSize * MaxIterationNum);

                    FinalAns.BatchSize = AnsData[0].BatchSize;
                    Array.Clear(FinalAns.Output.Data.MemPtr, 0, FinalAns.BatchSize);
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnsData[i].Output.Data.SyncToCPU(AnsData[i].BatchSize);

                        for (int b = 0; b < AnsMatchData.SrcSize; b++)
                        {
                            int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];
                            float p = TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i];

                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                FinalAns.Output.Data.MemPtr[m] += p * AnsData[i].Output.Data.MemPtr[m];
                            }
                        }
                    }
                    FinalAns.Output.Data.SyncFromCPU(FinalAns.BatchSize);
                }
                #endregion.

                #region RL Pooling.
                else if (ReasonPool == PoolingType.RL)
                {
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        ComputeLib.Logistic(TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.BatchSize, 1);
                        ComputeLib.ClipVector(TerminalRunner[i].Output.Output.Data, TerminalRunner[i].Output.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                        TerminalRunner[i].Output.Output.Data.SyncToCPU(TerminalRunner[i].Output.BatchSize);
                        AnsData[i].Output.Data.SyncToCPU(AnsData[i].BatchSize);
                    }

                    FinalAns.BatchSize = AnsData.Last().BatchSize;
                    Array.Clear(FinalAns.Output.Data.MemPtr, 0, FinalAns.BatchSize);

                    for (int b = 0; b < MatchData.SrcSize; b++)
                    {
                        int max_iter = 0;
                        double max_p = 0;

                        float acc_log_t = 0;
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float t_i = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                            float p = acc_log_t + (float)Math.Log(t_i);
                            if (i == MaxIterationNum - 1) p = acc_log_t;

                            AnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
                            acc_log_t += (float)Math.Log(1 - t_i);

                            if (Math.Exp(p) > max_p)
                            {
                                max_p = Math.Exp(p);
                                max_iter = i;
                            }

                            if (BuilderParameters.PRED_RL == PredType.RL_AVGSIM)
                            {
                                int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                                int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];
                                for (int m = matchBgn; m < matchEnd; m++)
                                {
                                    FinalAns.Output.Data.MemPtr[m] += (float)Math.Exp(p) * AnsData[i].Output.Data.MemPtr[m];
                                }
                            }
                            if (BuilderParameters.PRED_RL == PredType.RL_AVGPROB)
                            {
                                int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                                int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];
                                double expSum = 0;
                                for (int m = matchBgn; m < matchEnd; m++)
                                {
                                    expSum += Math.Exp(BuilderParameters.Gamma * AnsData[i].Output.Data.MemPtr[m]);
                                }

                                for (int m = matchBgn; m < matchEnd; m++)
                                {
                                    FinalAns.Output.Data.MemPtr[m] += (float)(Math.Exp(p) * Math.Exp(BuilderParameters.Gamma * AnsData[i].Output.Data.MemPtr[m]) / expSum);
                                }
                            }
                        }
                        if (BuilderParameters.PRED_RL == PredType.RL_MAXITER)
                        {
                            int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];
                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                FinalAns.Output.Data.MemPtr[m] = AnsData[max_iter].Output.Data.MemPtr[m];
                            }
                        }
                    }
                    FinalAns.Output.Data.SyncFromCPU(FinalAns.BatchSize);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i].BatchSize = MatchData.SrcSize;
                        AnswerProb[i].Output.Data.SyncFromCPU(AnswerProb[i].BatchSize);
                    }


                    // write the reasonet status.
                    if (DebugWriter != null)
                    {
                        for (int i = 0; i < MaxIterationNum - 1; i++)
                        {
                            MemRetrievalRunner[i].SoftmaxAddress.Output.Data.SyncToCPU();
                            MemRetrieval2Runner[i].SoftmaxAddress.Output.Data.SyncToCPU();
                        }

                        for (int b = 0; b < MatchData.SrcSize; b++)
                        {
                            int att1Bgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                            int att1End = MatchData.Src2MatchIdx.MemPtr[b];

                            int att2Bgn = b == 0 ? 0 : MatchData2.Src2MatchIdx.MemPtr[b - 1];
                            int att2End = MatchData2.Src2MatchIdx.MemPtr[b];

                            int ansBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                            int ansEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];

                            DebugWriter.WriteLine("Sample {0}.............", DebugSample + b);

                            for (int i = 0; i < MaxIterationNum; i++)
                            {
                                string att1Str = "";
                                string att2Str = "";
                                string ansStr = "";

                                if (i > 0)
                                {
                                    for (int m = att1Bgn; m < att1End; m++)
                                    {
                                        att1Str = att1Str + MemRetrievalRunner[i - 1].SoftmaxAddress.Output.Data.MemPtr[m].ToString() + " ";
                                    }

                                    for (int m = att2Bgn; m < att2End; m++)
                                    {
                                        att2Str = att2Str + MemRetrieval2Runner[i - 1].SoftmaxAddress.Output.Data.MemPtr[m].ToString() + " ";
                                    }
                                }

                                float aSum = 0;
                                for (int m = ansBgn; m < ansEnd; m++)
                                {
                                    aSum += (float)Math.Exp(BuilderParameters.Gamma * AnsData[i].Output.Data.MemPtr[m]);
                                }

                                for (int m = ansBgn; m < ansEnd; m++)
                                {
                                    ansStr = ansStr + (Math.Exp(BuilderParameters.Gamma * AnsData[i].Output.Data.MemPtr[m]) / aSum).ToString() + " ";
                                }

                                float termProb = TerminalRunner[i].Output.Output.Data.MemPtr[b];

                                DebugWriter.WriteLine("T {0}\t{1}\t{2}\t{3}\t{4}", i, ansStr, termProb, att1Str, att2Str);
                            }

                            string finalAnsStr = "";

                            for (int m = ansBgn; m < ansEnd; m++)
                            {
                                finalAnsStr = finalAnsStr + FinalAns.Output.Data.MemPtr[m].ToString() + " ";
                            }
                            DebugWriter.WriteLine("Sample {0}\t{1}", DebugSample + b, finalAnsStr);
                            DebugWriter.WriteLine();
                        }
                        DebugSample += MatchData.SrcSize;
                    }
                }
                #endregion.
                else
                {
                    FinalAns = AnsData.Last();
                }
            }
            public override void CleanDeriv()
            {
                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(AttHidden.SentDeriv, AttHidden.SentSize * AttHidden.Dim);
                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(Att2Hidden.SentDeriv, Att2Hidden.SentSize * Att2Hidden.Dim);
                }

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(AnsHidden.SentDeriv, AnsHidden.SentSize * AnsHidden.Dim);
                }


                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    TerminalRunner[i + 1].CleanDeriv();
                    // take Answer.
                    AnsRunner[i + 1].CleanDeriv();
                    Att2Runner[i + 1].CleanDeriv();
                    // take Attention
                    AttRunner[i + 1].CleanDeriv();

                    // update Status.
                    StatusRunner[i].CleanDeriv();

                    RetrievalEnsembleRunner[i].CleanDeriv();

                    MemRetrieval2Runner[i].CleanDeriv();

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].CleanDeriv();
                }
                TerminalRunner[0].CleanDeriv();
                AnsRunner[0].CleanDeriv();
                Att2Runner[0].CleanDeriv();
                AttRunner[0].CleanDeriv();

                if (ReasonPool == PoolingType.AVG)
                {
                    ComputeLib.Zero(FinalAns.Deriv.Data, FinalAns.BatchSize);
                }
            }

            /// <summary>
            /// Stage 1 : Iterative Attention Model (Supervised Training).
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                #region Avg pool.
                if (ReasonPool == PoolingType.AVG)
                {
                    FinalAns.Deriv.Data.SyncToCPU(FinalAns.BatchSize);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(AnsData[i].Deriv.Data.MemPtr, 0, AnsData[i].BatchSize);

                        for (int b = 0; b < AnsMatchData.SrcSize; b++)
                        {
                            int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];

                            float p = 1.0f / MaxIterationNum;
                            // TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i];
                            //float pDeriv = 0;

                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                AnsData[i].Deriv.Data.MemPtr[m] += FinalAns.Deriv.Data.MemPtr[m] * p;
                                //pDeriv += FinalAct.Deriv.Data.MemPtr[m] * ActionAtt[i].Output.Data.MemPtr[m];
                            }
                            //TerminalProb.Deriv.Data.MemPtr[b * MaxIterationNum + i] = pDeriv;
                        }
                        AnsData[i].Deriv.Data.SyncFromCPU(AnsData[i].BatchSize);
                    }
                }
                #endregion.

                #region Att pool.
                else if (ReasonPool == PoolingType.ATT)
                {
                    FinalAns.Deriv.Data.SyncToCPU(FinalAns.BatchSize);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(AnsData[i].Deriv.Data.MemPtr, 0, AnsData[i].BatchSize);

                        for (int b = 0; b < AnsMatchData.SrcSize; b++)
                        {
                            int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];

                            float p = TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i];
                            float pDeriv = 0;

                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                AnsData[i].Deriv.Data.MemPtr[m] += FinalAns.Deriv.Data.MemPtr[m] * p;
                                pDeriv += FinalAns.Deriv.Data.MemPtr[m] * AnsData[i].Output.Data.MemPtr[m];
                            }
                            TerminalProb.Deriv.Data.MemPtr[b * MaxIterationNum + i] = pDeriv;
                        }
                        AnsData[i].Deriv.Data.SyncFromCPU(AnsData[i].BatchSize);
                    }

                    TerminalProb.Deriv.Data.SyncFromCPU(MaxIterationNum * TerminalProb.BatchSize);
                    ComputeLib.DerivSoftmax(TerminalProb.Output.Data, TerminalProb.Deriv.Data, TerminalProb.Deriv.Data, MaxIterationNum, TerminalProb.BatchSize, 0);
                    TerminalProb.Deriv.Data.SyncToCPU(MaxIterationNum * TerminalProb.BatchSize);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        for (int b = 0; b < TerminalRunner[i].Output.BatchSize; b++)
                        {
                            TerminalRunner[i].Output.Deriv.Data.MemPtr[b] = TerminalProb.Deriv.Data.MemPtr[b * MaxIterationNum + i];
                        }
                        TerminalRunner[i].Output.Deriv.Data.SyncFromCPU(TerminalRunner[i].Output.BatchSize);
                    }
                }
                #endregion.

                #region RL Pool.
                else if (ReasonPool == PoolingType.RL)
                {
                    //int BatchSize = AnswerSeqData.Last().BatchSize;

                    /// according to loss, assign reward to each terminal node.
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(TerminalRunner[i].Output.Deriv.Data.MemPtr, 0, AnsMatchData.SrcSize);
                    }

                    for (int b = 0; b < AnsMatchData.SrcSize; b++)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float reward = AnswerProb[i].Deriv.Data.MemPtr[b];

                            if (BuilderParameters.IS_NORM_REWARD) reward = reward / (i + 1);

                            TerminalRunner[i].Output.Deriv.Data.MemPtr[b] += reward * (1 - TerminalRunner[i].Output.Output.Data.MemPtr[b]);
                            for (int hp = 0; hp < i; hp++)
                            {
                                TerminalRunner[hp].Output.Deriv.Data.MemPtr[b] += -(float)Math.Pow(BuilderParameters.RL_DISCOUNT, i - hp) * reward * TerminalRunner[hp].Output.Output.Data.MemPtr[b];
                            }

                            if (TerminalRunner[i].Output.Deriv.Data.MemPtr[b] > Math.Abs(1))
                            {
                                Logger.WriteLog("TerminalRunner Deriv is too large {0}, step {1}", TerminalRunner[i].Output.Deriv.Data.MemPtr[b], i);
                            }
                        }
                    }

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        TerminalRunner[i].Output.Deriv.Data.SyncFromCPU(TerminalRunner[i].Output.BatchSize);
                    }
                }
                #endregion.

                for (int i = MaxIterationNum - 2; i >= 0; i--)
                {
                    TerminalRunner[i + 1].Backward(cleanDeriv);

                    AnsRunner[i + 1].Backward(cleanDeriv);

                    Att2Runner[i + 1].Backward(cleanDeriv);
                    // take action. new Attention
                    AttRunner[i + 1].Backward(cleanDeriv);
                    // update Status.
                    StatusRunner[i].Backward(cleanDeriv);

                    RetrievalEnsembleRunner[i].Backward(cleanDeriv);

                    MemRetrieval2Runner[i].Backward(cleanDeriv);

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Backward(cleanDeriv);
                }
                TerminalRunner[0].Backward(cleanDeriv);
                AnsRunner[0].Backward(cleanDeriv);
                Att2Runner[0].Backward(cleanDeriv);
                AttRunner[0].Backward(cleanDeriv);

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    ComputeLib.Sgemm(AnsHidden.SentDeriv, 0,
                                 AnsStruct.Wm, 0,
                                 AnsMem.SentDeriv, 0,
                                 AnsMem.SentSize, AnsStruct.HiddenDim, AnsStruct.MemoryDim, 1, 1, false, true);
                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    ComputeLib.Sgemm(Att2Hidden.SentDeriv, 0,
                                 Att2Struct.Wm, 0,
                                 Memory2.SentDeriv, 0,
                                 Memory2.SentSize, Att2Struct.HiddenDim, Att2Struct.MemoryDim, 1, 1, false, true);
                }

                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    ComputeLib.Sgemm(AttHidden.SentDeriv, 0,
                                 AttStruct.Wm, 0,
                                 Memory.SentDeriv, 0,
                                 Memory.SentSize, AttStruct.HiddenDim, AttStruct.MemoryDim, 1, 1, false, true);
                }
            }

            public override void Update()
            {
                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AttStruct.WmMatrixOptimizer.BeforeGradient();
                        if (AttStruct.IsBias) AttStruct.BMatrixOptimizer.BeforeGradient();
                    }

                    if (AttStruct.IsBias)
                    {
                        ComputeLib.ColumnWiseSum(AttHidden.SentDeriv, AttStruct.BMatrixOptimizer.Gradient, Memory.SentSize, AttStruct.HiddenDim, AttStruct.BMatrixOptimizer.GradientStep);
                    }

                    ComputeLib.Sgemm(Memory.SentOutput, 0,
                                     AttHidden.SentDeriv, 0,
                                     AttStruct.WmMatrixOptimizer.Gradient, 0,
                                     Memory.SentSize, AttStruct.MemoryDim, AttStruct.HiddenDim,
                                     1, AttStruct.WmMatrixOptimizer.GradientStep, true, false);
                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        Att2Struct.WmMatrixOptimizer.BeforeGradient();
                        if (Att2Struct.IsBias) Att2Struct.BMatrixOptimizer.BeforeGradient();
                    }

                    if (Att2Struct.IsBias)
                    {
                        ComputeLib.ColumnWiseSum(Att2Hidden.SentDeriv, Att2Struct.BMatrixOptimizer.Gradient, Memory2.SentSize, Att2Struct.HiddenDim, Att2Struct.BMatrixOptimizer.GradientStep);
                    }

                    ComputeLib.Sgemm(Memory2.SentOutput, 0,
                                     Att2Hidden.SentDeriv, 0,
                                     Att2Struct.WmMatrixOptimizer.Gradient, 0,
                                     Memory2.SentSize, Att2Struct.MemoryDim, Att2Struct.HiddenDim,
                                     1, Att2Struct.WmMatrixOptimizer.GradientStep, true, false);
                }


                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AnsStruct.WmMatrixOptimizer.BeforeGradient();
                        if (AnsStruct.IsBias) AnsStruct.BMatrixOptimizer.BeforeGradient();
                    }

                    if (AnsStruct.IsBias)
                    {
                        ComputeLib.ColumnWiseSum(AnsHidden.SentDeriv, AnsStruct.BMatrixOptimizer.Gradient, AnsMem.SentSize, AnsStruct.HiddenDim, AnsStruct.BMatrixOptimizer.GradientStep);
                    }

                    ComputeLib.Sgemm(AnsMem.SentOutput, 0,
                                     AnsHidden.SentDeriv, 0,
                                     AnsStruct.WmMatrixOptimizer.Gradient, 0,
                                     AnsMem.SentSize, AnsStruct.MemoryDim, AnsStruct.HiddenDim,
                                     1, AnsStruct.WmMatrixOptimizer.GradientStep, true, false);
                }

                AttRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                AttRunner[0].Update();

                Att2Runner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                Att2Runner[0].Update();

                AnsRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                AnsRunner[0].Update();

                TerminalRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                TerminalRunner[0].Update();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    MemRetrievalRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    MemRetrieval2Runner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    StatusRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    AttRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    Att2Runner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    AnsRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    TerminalRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;


                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Update();

                    MemRetrieval2Runner[i].Update();

                    // update Status.
                    StatusRunner[i].Update();

                    // take action. new Attention
                    AttRunner[i + 1].Update();

                    Att2Runner[i + 1].Update();

                    AnsRunner[i + 1].Update();

                    TerminalRunner[i + 1].Update();
                }
                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AttStruct.WmMatrixOptimizer.AfterGradient();
                        if (AttStruct.IsBias) AttStruct.BMatrixOptimizer.AfterGradient();
                    }
                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        Att2Struct.WmMatrixOptimizer.AfterGradient();
                        if (Att2Struct.IsBias) Att2Struct.BMatrixOptimizer.AfterGradient();
                    }
                }

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AnsStruct.WmMatrixOptimizer.AfterGradient();
                        if (AnsStruct.IsBias) AnsStruct.BMatrixOptimizer.AfterGradient();
                    }
                }
            }
        }


        class CandidateSelectionRunner : StructRunner
        {
            public new BiMatchBatchData Output { get { return (BiMatchBatchData)base.Output; } set { base.Output = value; } }

            public SeqDenseBatchData CandidateContextMem = null;

            GeneralBatchInputData CandidateIndexData { get; set; }

            SeqDenseBatchData FullContextMem { get; set; }

            CudaPieceInt SelectIndex { get; set; }

            public CandidateSelectionRunner(GeneralBatchInputData candidateData, SeqDenseBatchData fullContextMem, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                CandidateIndexData = candidateData;
                FullContextMem = fullContextMem;


                SelectIndex = new CudaPieceInt(candidateData.Stat.MAX_ELEMENTSIZE, true, Behavior.Device == DeviceType.GPU);

                Output = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = candidateData.Stat.MAX_BATCHSIZE,
                    MAX_TGT_BATCHSIZE = candidateData.Stat.MAX_ELEMENTSIZE,
                    MAX_MATCH_BATCHSIZE = candidateData.Stat.MAX_ELEMENTSIZE
                }, Behavior.Device);

                /// candidate context mem;
                CandidateContextMem = new SeqDenseBatchData(
                    new SequenceDataStat
                    {
                        MAX_BATCHSIZE = candidateData.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = candidateData.Stat.MAX_ELEMENTSIZE,
                        FEATURE_DIM = fullContextMem.Dim
                    }, Output.Src2MatchIdx, Output.SrcIdx, Behavior.Device);
            }

            public override void Forward()
            {
                CandidateIndexData.BatchIdx.SyncToCPU(CandidateIndexData.BatchSize);
                CandidateIndexData.FeatureIdx.SyncToCPU(CandidateIndexData.ElementSize);
                CandidateIndexData.FeatureValue.SyncToCPU(CandidateIndexData.ElementSize);

                FullContextMem.SampleIdx.SyncToCPU(CandidateIndexData.BatchSize);

                Output.SrcSize = CandidateIndexData.BatchSize;
                Output.TgtSize = CandidateIndexData.ElementSize; //FullContextMem.SampleIdx.MemPtr[CandidateIndexData.BatchSize - 1];
                Output.MatchSize = CandidateIndexData.ElementSize;

                int matchIdx = 0;
                for (int i = 0; i < CandidateIndexData.BatchSize; i++)
                {
                    int feaBegin = i == 0 ? 0 : CandidateIndexData.BatchIdx.MemPtr[i - 1];
                    int feaEnd = CandidateIndexData.BatchIdx.MemPtr[i];

                    int fullMemBegin = i == 0 ? 0 : FullContextMem.SampleIdx.MemPtr[i - 1];
                    for (int f = feaBegin; f < feaEnd; f++)
                    {
                        Output.SrcIdx.MemPtr[matchIdx] = i;
                        //matchIdx; // 
                        //fullMemBegin + CandidateIndexData.FeatureIdx.MemPtr[f];  
                        Output.TgtIdx.MemPtr[matchIdx] = matchIdx;
                        Output.MatchInfo.MemPtr[matchIdx] = CandidateIndexData.FeatureValue.MemPtr[f];

                        SelectIndex.MemPtr[matchIdx] = fullMemBegin + CandidateIndexData.FeatureIdx.MemPtr[f];

                        matchIdx++;
                    }
                }

                SelectIndex.SyncFromCPU(Output.MatchSize);

                Output.SrcIdx.SyncFromCPU(Output.MatchSize);
                Output.TgtIdx.SyncFromCPU(Output.MatchSize);
                Output.MatchInfo.SyncFromCPU(Output.MatchSize);

                Util.InverseMatchIdx(Output.SrcIdx.MemPtr, Output.MatchSize, Output.Src2MatchIdx.MemPtr, Output.Src2MatchElement.MemPtr, Output.SrcSize);
                Util.InverseMatchIdx(Output.TgtIdx.MemPtr, Output.MatchSize, Output.Tgt2MatchIdx.MemPtr, Output.Tgt2MatchElement.MemPtr, Output.TgtSize);

                Output.Src2MatchIdx.SyncFromCPU(Output.SrcSize);
                Output.Src2MatchElement.SyncFromCPU(Output.MatchSize);
                Output.Tgt2MatchIdx.SyncFromCPU(Output.TgtSize);
                Output.Tgt2MatchElement.SyncFromCPU(Output.MatchSize);


                CandidateContextMem.BatchSize = CandidateIndexData.BatchSize;
                CandidateContextMem.SentSize = CandidateIndexData.ElementSize;

                ComputeLib.Matrix_AdditionMask(FullContextMem.SentOutput, 0, SelectIndex, 0,
                                               CandidateContextMem.SentOutput, 0, CudaPieceInt.Empty, 0,
                                               CandidateContextMem.SentOutput, 0, CudaPieceInt.Empty, 0,
                                               FullContextMem.Dim, Output.MatchSize, 1, 0, 0);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(CandidateContextMem.SentDeriv, CandidateContextMem.SentSize * CandidateContextMem.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.Matrix_AdditionMask(CandidateContextMem.SentDeriv, 0, CudaPieceInt.Empty, 0,
                                               CandidateContextMem.SentDeriv, 0, CudaPieceInt.Empty, 0,
                                               FullContextMem.SentDeriv, 0, SelectIndex, 0,
                                               FullContextMem.Dim, Output.MatchSize, 1, 0, 1);
            }
        }

        class CandidatePredictionRunner : ObjectiveRunner
        {
            GeneralBatchInputData CandidateInputData { get; set; }

            SeqSparseBatchData ContextInputData { get; set; }

            CudaPieceFloat CandScoreData { get; set; }

            float Gamma { get; set; }

            int SampleNum = 0;
            int[] HitNum = null;

            int Iteration = 0;
            string OutputPath = "";
            StreamWriter[] OutputWriter = null;
            bool IsExp = true;

            List<Dictionary<string, float>> Result = null;
            List<List<Dictionary<string, float>>> HisResults = new List<List<Dictionary<string, float>>>();

            public CandidatePredictionRunner(
                GeneralBatchInputData candInputData, SeqSparseBatchData contextInputData, CudaPieceFloat candScoreData, float gamma, RunnerBehavior behavior,
                string outputFile = "", bool isExp = true) : base(Structure.Empty, behavior)
            {
                CandidateInputData = candInputData;
                ContextInputData = contextInputData;
                Gamma = gamma;
                CandScoreData = candScoreData;

                OutputPath = outputFile;
                IsExp = isExp;

                HitNum = new int[BuilderParameters.K_SHUFFLE];
                OutputWriter = new StreamWriter[BuilderParameters.K_SHUFFLE];
            }

            public override void Init()
            {
                SampleNum = 0;
                for (int i = 0; i < BuilderParameters.K_SHUFFLE; i++)
                {
                    HitNum[i] = 0;
                    if (!OutputPath.Equals(""))
                    {
                        OutputWriter[i] = new StreamWriter(OutputPath + "." + Iteration.ToString() + ".Shuffle" + i.ToString());
                    }
                }
                Result = new List<Dictionary<string, float>>();

            }
            public override void Forward()
            {
                CandidateInputData.BatchIdx.SyncToCPU(CandidateInputData.BatchSize);
                CandidateInputData.FeatureIdx.SyncToCPU(CandidateInputData.ElementSize);
                CandidateInputData.FeatureValue.SyncToCPU(CandidateInputData.ElementSize);

                CandScoreData.SyncToCPU(CandidateInputData.ElementSize);

                ContextInputData.SampleIdx.SyncToCPU(ContextInputData.BatchSize);
                ContextInputData.FeaIdx.SyncToCPU(ContextInputData.ElementSize);

                for (int i = 0; i < CandidateInputData.BatchSize; i++)
                {
                    Dictionary<string, float> candScoreDict = new Dictionary<string, float>();
                    string trueCandIdx = string.Empty;

                    int sentBgn = i == 0 ? 0 : ContextInputData.SampleIdx.MemPtr[i - 1];
                    int sentEnd = ContextInputData.SampleIdx.MemPtr[i];

                    int candBgn = i == 0 ? 0 : CandidateInputData.BatchIdx.MemPtr[i - 1];
                    int candEnd = CandidateInputData.BatchIdx.MemPtr[i];

                    for (int c = candBgn; c < candEnd; c++)
                    {
                        int candPos = CandidateInputData.FeatureIdx.MemPtr[c];
                        int candIdx = ContextInputData.FeaIdx.MemPtr[candPos + sentBgn];
                        float candScore = CandScoreData.MemPtr[c];
                        float candLabel = CandidateInputData.FeatureValue.MemPtr[c];
                        string candName = DataPanel.EntityName(candIdx);

                        if (candLabel > 0 && !trueCandIdx.Equals(string.Empty) && !trueCandIdx.Equals(candName)) { throw new Exception("Candidate Label Error 1 !!"); }

                        if (candLabel > 0) { trueCandIdx = candName; }

                        if (!candScoreDict.ContainsKey(candName)) { candScoreDict[candName] = 0; }
                        candScoreDict[candName] += IsExp ? (float)Math.Exp(Gamma * candScore) : candScore;
                    }

                    if (trueCandIdx.Equals(string.Empty)) { throw new Exception("Candidate Label Error 2 !!"); }

                    float expSum = candScoreDict.Values.Sum();
                    foreach (string k in new List<string>(candScoreDict.Keys)) candScoreDict[k] = candScoreDict[k] / expSum;
                    Result.Add(candScoreDict);

                    for (int s = 0; s < BuilderParameters.K_SHUFFLE; s++)
                    {
                        Dictionary<string, float> kScoreDict = new Dictionary<string, float>();
                        int uptoK = Math.Min(s, HisResults.Count);

                        foreach (string e in new List<string>(candScoreDict.Keys))
                        {
                            kScoreDict.Add(e, candScoreDict[e] / (uptoK + 1));
                            for (int k = 0; k < uptoK; k++)
                            {
                                kScoreDict[e] += HisResults[HisResults.Count - 1 - k][SampleNum][e] / (uptoK + 1);
                            }
                        }
                        var predCandIdx = kScoreDict.FirstOrDefault(x => x.Value == kScoreDict.Values.Max()).Key;
                        OutputWriter[s].WriteLine("{0}\t{1}\t{2}", trueCandIdx, TextUtil.Dict2Str(kScoreDict), predCandIdx);
                        if (predCandIdx.Equals(trueCandIdx)) HitNum[s]++;
                    }
                    SampleNum++;
                    
                }
            }

            public override void Complete()
            {
                HisResults.Add(Result);

                if (HisResults.Count >= BuilderParameters.K_SHUFFLE) { HisResults.RemoveAt(0); }

                for (int k = 0; k < BuilderParameters.K_SHUFFLE; k++)
                {
                    Logger.WriteLog("Sample {0}, {1}-Shuffle, Hit {2}, Accuracy {3}", SampleNum, k, HitNum[k], HitNum[k] * 1.0 / SampleNum);
                    OutputWriter[k].Close();
                }
                ObjectiveScore = HitNum.Max() * 1.0 / SampleNum;
                
                Iteration += 1;
            }
        }
        
        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> context,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> query,
                                                             IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> answer,

                                                             // text embedding cnn.
                                                             List<LayerStructure> EmbedCNN,

                                                             // context lstm 
                                                             GRUStructure ContextD1LSTM, GRUStructure ContextD2LSTM,

                                                             // query lstm
                                                             GRUStructure QueryD1LSTM, GRUStructure QueryD2LSTM,

                                                             // reason net;
                                                             GRUCell StateStruct, MLPAttentionStructure AttStruct, MLPAttentionStructure Att2Struct,

                                                             MLPAttentionStructure AnsStruct, LayerStructure TerminalStruct,

                                                             // model.
                                                             CompositeNNStructure model, RunnerBehavior Behavior, string resultFile)
        {
            ComputationGraph cg = new ComputationGraph();

            /************************************************************ Query Passage Candidate data *********/
            SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));
            SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(context, Behavior));
            GeneralBatchInputData CandidateAnswerData = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(answer, Behavior));

            /*************Random Shuffle Entity *******/
            if(BuilderParameters.IS_ENTITY_SHUFFLE) cg.AddRunner(new DataPanel.EntityShuffleRunner(QueryData, ContextData, Behavior));

            /************************************************************ CNN on Query data *********/
            SeqDenseBatchData QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN[0], QueryData, Behavior));
            for (int i = 1; i < EmbedCNN.Count; i++)
                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(EmbedCNN[i], QueryEmbedOutput, Behavior));

            /************************************************************ LSTM on Query data *********/
            HiddenBatchData QueryOutput = null;
            SeqDenseBatchData QuerySeqOutput = null;
            BiMatchBatchData QueryMatch = null;
            { 
                SeqDenseRecursiveData QueryD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(QueryEmbedOutput, false, Behavior));
                FastGRUDenseRunner<SeqDenseRecursiveData> queryD1LSTMRunner = new FastGRUDenseRunner<SeqDenseRecursiveData>(QueryD1LSTM.GRUCells[0], QueryD1LstmInput, Behavior);
                SeqDenseRecursiveData QueryD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(queryD1LSTMRunner);

                SeqDenseRecursiveData QueryD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(QueryEmbedOutput, true, Behavior));
                FastGRUDenseRunner<SeqDenseRecursiveData> queryD2LSTMRunner = new FastGRUDenseRunner<SeqDenseRecursiveData>(QueryD2LSTM.GRUCells[0], QueryD2LstmInput, Behavior);
                SeqDenseRecursiveData QueryD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(queryD2LSTMRunner);

                SeqDenseBatchData QuerySeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(QueryD1LstmOutput, Behavior));
                SeqDenseBatchData QuerySeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(QueryD2LstmOutput, Behavior));
                QuerySeqOutput = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QuerySeqD1O, QuerySeqD2O }, Behavior));
                QueryMatch = (BiMatchBatchData)cg.AddRunner(new SeqBiMatchRunner(QuerySeqOutput, Behavior));

                if (BuilderParameters.QUERY_POOL == PoolingType.LAST)
                {
                    HiddenBatchData QueryD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(QueryD1LstmOutput, true, 0, QueryD1LstmOutput.MapForward, Behavior));
                    HiddenBatchData QueryD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(QueryD2LstmOutput, false, 0, QueryD2LstmOutput.MapForward, Behavior));
                    QueryOutput = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { QueryD1O, QueryD2O }, Behavior));
                }
                else if (BuilderParameters.QUERY_POOL == PoolingType.MAX)
                {
                    QueryOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(QuerySeqOutput, Behavior));
                }
            }
            //else
            //{
            //    QueryOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(QueryEmbedOutput, Behavior));
            //}

            /************************************************************ Embedding on Passage data *********/
            SeqDenseBatchData ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN[0], ContextData, Behavior));
            for (int i = 1; i < EmbedCNN.Count; i++)
                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(EmbedCNN[i], ContextEmbedOutput, Behavior));

            SeqDenseBatchData CandidateSeqO = null;
            BiMatchBatchData CandidateMatch = null;

            SeqDenseBatchData MemorySeqO = null;
            BiMatchBatchData MemoryMatch = null;
            /************************************************************ LSTM on Passage data *********/
            
            {
                //if (BuilderParameters.IS_ENTITY_LSTM)
                //{
                //    /************************************************************ Select Candidate on Entity data *********/
                //    CandidateSelectionRunner CandidateMemRunner = new CandidateSelectionRunner(CandidateAnswerData, ContextEmbedOutput, Behavior);
                //    cg.AddRunner(CandidateMemRunner);
                //    SeqDenseBatchData CandidateEntityMem = CandidateMemRunner.CandidateContextMem;
                //    CandidateMatch = CandidateMemRunner.Output;
                //    /************************************************************ LSTM on Candidate Entity data *********/
                //    SeqDenseRecursiveData CandidateD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(CandidateEntityMem, false, Behavior));
                //    FastLSTMDenseRunner<SeqDenseRecursiveData> candidateD1LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(ContextD1LSTM.LSTMCells[0], CandidateD1LstmInput, Behavior);
                //    SeqDenseRecursiveData CandidateD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(candidateD1LSTMRunner);
                //    SeqDenseRecursiveData CandidateD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(CandidateEntityMem, true, Behavior));
                //    FastLSTMDenseRunner<SeqDenseRecursiveData> candidateD2LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(ContextD2LSTM.LSTMCells[0], CandidateD2LstmInput, Behavior);
                //    SeqDenseRecursiveData CandidateD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(candidateD2LSTMRunner);
                //    SeqDenseBatchData CandidateSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(CandidateD1LstmOutput, Behavior));
                //    SeqDenseBatchData CandidateSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(CandidateD2LstmOutput, Behavior));
                //    CandidateSeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { CandidateSeqD1O, CandidateSeqD2O }, Behavior));
                //}
                //else
                /************************************************************ LSTM on Candidate Word data *********/
                SeqDenseRecursiveData CandidateD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(ContextEmbedOutput, false, Behavior));
                FastGRUDenseRunner<SeqDenseRecursiveData> candidateD1LSTMRunner = new FastGRUDenseRunner<SeqDenseRecursiveData>(ContextD1LSTM.GRUCells[0], CandidateD1LstmInput, Behavior);
                SeqDenseRecursiveData CandidateD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(candidateD1LSTMRunner);

                SeqDenseRecursiveData CandidateD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(ContextEmbedOutput, true, Behavior));
                FastGRUDenseRunner<SeqDenseRecursiveData> candidateD2LSTMRunner = new FastGRUDenseRunner<SeqDenseRecursiveData>(ContextD2LSTM.GRUCells[0], CandidateD2LstmInput, Behavior);
                SeqDenseRecursiveData CandidateD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(candidateD2LSTMRunner);

                SeqDenseBatchData CandidateSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(CandidateD1LstmOutput, Behavior));
                SeqDenseBatchData CandidateSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(CandidateD2LstmOutput, Behavior));
                MemorySeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { CandidateSeqD1O, CandidateSeqD2O }, Behavior));

                if (!BuilderParameters.IS_ENTITY_ATT) MemoryMatch = (BiMatchBatchData)cg.AddRunner(new SeqBiMatchRunner(MemorySeqO, Behavior));

                CandidateSelectionRunner CandidateMemRunner = new CandidateSelectionRunner(CandidateAnswerData, MemorySeqO, Behavior);
                cg.AddRunner(CandidateMemRunner);
                CandidateSeqO = CandidateMemRunner.CandidateContextMem;
                CandidateMatch = CandidateMemRunner.Output;
            }
            //else
            //{
            //    CandidateMatchRunner CandidateMemRunner = new CandidateMatchRunner(CandidateAnswerData, ContextEmbedOutput, Behavior);
            //    cg.AddRunner(CandidateMemRunner);
            //    CandidateMatch = CandidateMemRunner.Output;
            //    CandidateSeqO = ContextEmbedOutput;
            //}

            /* We will always use reasoNet.
            if (BuilderParameters.RECURRENT_STEP == 0)
            {
                // similarity between source text and target text.
                simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(QueryOutput,
                    new HiddenBatchData(CandidateSeqO.MAX_SENTSIZE, CandidateSeqO.Dim, CandidateSeqO.SentOutput, CandidateSeqO.SentDeriv, Behavior.Device),
                     CandidateMatch, BuilderParameters.SimiType, Behavior));
            }
            */

            if (Behavior.RunMode == DNNRunMode.Train)
            {
                if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                    cg.AddRunner(new ModelDiskDumpRunner(model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\RecurrentAttention"));
            }

            // We will always use reasoNet.
            if (BuilderParameters.RECURRENT_STEP == 0)
            {
                // similarity between source text and target text.
                HiddenBatchData simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(QueryOutput,
                    new HiddenBatchData(CandidateSeqO.MAX_SENTSIZE, CandidateSeqO.Dim, CandidateSeqO.SentOutput, CandidateSeqO.SentDeriv, Behavior.Device),
                     CandidateMatch, SimilarityType.CosineSimilarity, Behavior));

                switch (Behavior.RunMode)
                {
                    case DNNRunMode.Train:
                        cg.AddObjective(new BayesianRatingRunner(simiOutput.Output.Data, simiOutput.Deriv.Data, CandidateMatch.MatchInfo, CandidateMatch, BuilderParameters.Gamma, Behavior));
                        break;
                    case DNNRunMode.Predict:
                        cg.AddRunner(new CandidatePredictionRunner(CandidateAnswerData, ContextData, simiOutput.Output.Data, BuilderParameters.Gamma, Behavior, resultFile));
                        break;
                }
            }
            else
            {
                ReasonNetRunner reasonRunner = new ReasonNetRunner(QueryOutput, BuilderParameters.RECURRENT_STEP,
                   BuilderParameters.IS_ENTITY_ATT ? CandidateSeqO : MemorySeqO, BuilderParameters.IS_ENTITY_ATT ? CandidateMatch : MemoryMatch, AttStruct,
                    QuerySeqOutput, QueryMatch, Att2Struct,
                    CandidateSeqO, CandidateMatch, AnsStruct,
                    StateStruct, TerminalStruct, BuilderParameters.Gamma, Behavior, Behavior.RunMode == DNNRunMode.Train ? string.Empty : BuilderParameters.DebugFile);
                cg.AddRunner(reasonRunner);

                switch (Behavior.RunMode)
                {
                    case DNNRunMode.Train:

                        if (BuilderParameters.RL_ALL)
                            cg.AddObjective(new BayesianContrastiveRewardRunner(CandidateMatch.MatchInfo, reasonRunner.AnsData, reasonRunner.AnswerProb, CandidateMatch, BuilderParameters.Gamma, Behavior));
                        else
                            cg.AddObjective(new MultiInstanceBayesianRatingRunner(CandidateMatch.MatchInfo, reasonRunner.AnsData, reasonRunner.AnswerProb, CandidateMatch, BuilderParameters.Gamma, Behavior));

                        //
                        break;
                    case DNNRunMode.Predict:
                        HiddenBatchData AnswerOutput = reasonRunner.FinalAns;
                        cg.AddRunner(new CandidatePredictionRunner(CandidateAnswerData, ContextData, AnswerOutput.Output.Data, BuilderParameters.Gamma, Behavior, resultFile, BuilderParameters.PRED_RL == PredType.RL_AVGPROB ? false : true));
                        break;
                }
            }

            //ReasonNetRunner reasonRunner = new ReasonNetRunner(QueryOutput, BuilderParameters.RECURRENT_STEP,
            //   BuilderParameters.IS_ENTITY_ATT ? CandidateSeqO : MemorySeqO, BuilderParameters.IS_ENTITY_ATT ? CandidateMatch : MemoryMatch, AttStruct,
            //    QuerySeqOutput, QueryMatch, Att2Struct,
            //    CandidateSeqO, CandidateMatch, AnsStruct,
            //    StateStruct, TerminalStruct, BuilderParameters.Gamma, Behavior);
            //cg.AddRunner(reasonRunner);
            //switch (Behavior.RunMode)
            //{
            //    case DNNRunMode.Train:
            //        if (DSMlib.BuilderParameters.ModelUpdatePerSave > 0)
            //            cg.AddRunner(new ModelDiskDumpRunner(model, DSMlib.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\RecurrentAttention"));
            //        cg.AddObjective(new MultiInstanceBayesianRatingRunner(CandidateMatch.MatchInfo, reasonRunner.AnsData, reasonRunner.AnswerProb, CandidateMatch, BuilderParameters.Gamma, Behavior));
            //        break;
            //    case DNNRunMode.Predict:
            //        HiddenBatchData AnswerOutput = reasonRunner.FinalAns;
            //        cg.AddRunner(new CandidatePredictionRunner(CandidateAnswerData, ContextData, AnswerOutput.Output.Data, BuilderParameters.Gamma, Behavior, resultFile));
            //        break;
            //}

            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");


            CompositeNNStructure modelStructure = new CompositeNNStructure();

            // word embedding layer.
            List<LayerStructure> embedLayers = new List<LayerStructure>();

            //context lstm layer.
            GRUStructure contextD1LSTM = null;
            GRUStructure contextD2LSTM = null;

            //query lstm layer.
            GRUStructure queryD1LSTM = null;
            GRUStructure queryD2LSTM = null;

            //LayerStructure matchLayer = null;

            // gru for state transformation.
            GRUCell stateStruct = null;

            // MLP as attention.
            MLPAttentionStructure attStruct = null;

            // MLP as attention.
            MLPAttentionStructure att2Struct = null;

            // cosine similarity as action.
            MLPAttentionStructure ansStruct = null;

            // Terminal gate module. 
            LayerStructure terminalStruct = null;

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                int embedDim = DataPanel.WordDim;
                for (int i = 0; i < BuilderParameters.EMBED_LAYER_DIM.Length; i++)
                {
                    embedLayers.Add((new LayerStructure(embedDim, BuilderParameters.EMBED_LAYER_DIM[i],
                        BuilderParameters.EMBED_ACTIVATION[i], N_Type.Convolution_layer, BuilderParameters.EMBED_LAYER_WIN[i],
                        BuilderParameters.EMBED_DROPOUT[i], false, device)));
                    embedDim = BuilderParameters.EMBED_LAYER_DIM[i];
                }

                #region Glove Work Embedding Initialization.

                if (!BuilderParameters.InitWordEmbedding.Equals(string.Empty))
                {
                    int wordDim = BuilderParameters.EMBED_LAYER_DIM[0];

                    int halfWinStart = BuilderParameters.EMBED_LAYER_WIN[0] / 2; // * DataPanel.WordDim;

                    Logger.WriteLog("Init Glove Word Embedding ...");
                    int hit = 0;
                    using (StreamReader mreader = new StreamReader(BuilderParameters.InitWordEmbedding))
                    {
                        while (!mreader.EndOfStream)
                        {
                            string[] items = mreader.ReadLine().Split(' ');
                            string word = items[0];
                            float[] vec = new float[items.Length - 1];
                            for (int i = 1; i < items.Length; i++)
                            {
                                vec[i - 1] = float.Parse(items[i]);
                            }

                            int wordIdx = DataPanel.wordFreqDict.IndexItem(word);
                            if (wordIdx >= 0)
                            {
                                int feaIdx = DataPanel.entityFreqDict.ItemDictSize + 1 + wordIdx;

                                for (int win = -halfWinStart; win < BuilderParameters.EMBED_LAYER_WIN[0] - halfWinStart; win++)
                                {
                                    for (int i = 0; i < wordDim; i++)
                                    {
                                        embedLayers[0].weight.MemPtr[((win + halfWinStart) * DataPanel.WordDim + feaIdx) * wordDim + i] = vec[i];
                                    }
                                }
                                hit++;
                            }
                        }
                    }
                    embedLayers[0].weight.SyncFromCPU();
                    Logger.WriteLog("Init Glove Word Embedding Done, {0} words initalized in total {1}.", hit, DataPanel.wordFreqDict.ItemDictSize);
                }
                #endregion.

                int conDim = embedDim;
                int queryDim = embedDim;
                { 
                    contextD1LSTM = new GRUStructure(conDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    contextD2LSTM = new GRUStructure(conDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    conDim = 2 * BuilderParameters.GRU_DIM;
                }
                if(!BuilderParameters.IS_SHARE_RNN)
                {
                    queryD1LSTM = new GRUStructure(queryDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    queryD2LSTM = new GRUStructure(queryDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    queryDim = 2 * BuilderParameters.GRU_DIM;
                }
                else
                {
                    queryD1LSTM = contextD1LSTM;
                    queryD2LSTM = contextD2LSTM;
                    queryDim = conDim;
                }
                attStruct = new MLPAttentionStructure(queryDim, conDim, BuilderParameters.ATT_HID_DIM, device);
                att2Struct = new MLPAttentionStructure(queryDim, queryDim, BuilderParameters.ATT2_HID_DIM, device);
                stateStruct = new GRUCell(queryDim + conDim, queryDim, device, BuilderParameters.RndInit);

                ansStruct = new MLPAttentionStructure(queryDim, conDim, BuilderParameters.ANS_HID_DIM, device);
                terminalStruct = new LayerStructure(queryDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false);
                //matchLayer = new LayerStructure(BuilderParameters.QUERY_LAYER_DIM.Last(), BuilderParameters.CON_LAYER_DIM.Last(), A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true);

                for (int i = 0; i < embedLayers.Count; i++) modelStructure.AddLayer(embedLayers[i]);

                modelStructure.AddLayer(stateStruct);
                modelStructure.AddLayer(attStruct);
                modelStructure.AddLayer(att2Struct);
                modelStructure.AddLayer(ansStruct);
                modelStructure.AddLayer(terminalStruct);

                {
                    modelStructure.AddLayer(contextD1LSTM);
                    modelStructure.AddLayer(contextD2LSTM);
                }
                if (!BuilderParameters.IS_SHARE_RNN)
                {
                    modelStructure.AddLayer(queryD1LSTM);
                    modelStructure.AddLayer(queryD2LSTM);
                }
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, DeviceType.GPU);
                    int link = 0;
                    for (int i = 0; i < BuilderParameters.EMBED_LAYER_DIM.Length; i++) embedLayers.Add((LayerStructure)modelStructure.CompositeLinks[link++]);

                    stateStruct = (GRUCell)modelStructure.CompositeLinks[link++];
                    attStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    att2Struct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    ansStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    terminalStruct = (LayerStructure)modelStructure.CompositeLinks[link++];

                    {
                        contextD1LSTM = (GRUStructure)modelStructure.CompositeLinks[link++];
                        contextD1LSTM = (GRUStructure)modelStructure.CompositeLinks[link++];
                    }
                    if (!BuilderParameters.IS_SHARE_RNN)
                    {
                        queryD1LSTM = (GRUStructure)modelStructure.CompositeLinks[link++];
                        queryD2LSTM = (GRUStructure)modelStructure.CompositeLinks[link++];
                    }
                    else
                    {
                        queryD1LSTM = contextD1LSTM;
                        queryD2LSTM = contextD2LSTM;
                    }
                }
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainAnswer,
                        embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                        stateStruct, attStruct, att2Struct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }, string.Empty);
                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    ComputationGraph testCG = null;
                    if (BuilderParameters.IsTestFile)
                        testCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                            embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                            stateStruct, attStruct, att2Struct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.ScoreOutputPath + ".Test");

                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraph(DataPanel.ValidContext, DataPanel.ValidQuery, DataPanel.ValidAnswer,
                            embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                            stateStruct, attStruct, att2Struct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.ScoreOutputPath + ".Valid");

                    double bestValidScore = double.MinValue;
                    double bestTestScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }

                        double testScore = 0;
                        double validScore = 0;
                        if (testCG != null)
                        {
                            testScore = testCG.Execute();
                            Logger.WriteLog("Test Score {0}, At iter {1}", testScore, bestIter);
                        }
                        if (validCG != null)
                        {
                            validScore = validCG.Execute();
                            Logger.WriteLog("Valid Score {0}, At iter {1}", validScore, bestIter);
                        }

                        if (validScore > bestValidScore)
                        {
                            bestValidScore = validScore; bestTestScore = testScore; bestIter = iter;
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, "RecurrentAttention.best.model"), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }
                        Logger.WriteLog("Best Valid/Test Score {0}, {1}, At iter {2}", bestValidScore, bestTestScore, bestIter);

                        if (BuilderParameters.IS_ENTITY_SHUFFLE) DataPanel.InitEntityShuffle();
                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                            embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                            stateStruct, attStruct, att2Struct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.ScoreOutputPath + ".Test");
                    predCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQuery = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TrainAnswer = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestQuery = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TestAnswer = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQuery = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> ValidAnswer = null;

            public static ItemFreqIndexDictionary wordFreqDict = null;
            public static ItemFreqIndexDictionary entityFreqDict = null;

            public static int WordDim { get { return 1 + entityFreqDict.ItemDictSize + wordFreqDict.ItemDictSize; } }

            static int WordVocabLimited = BuilderParameters.VocabSize;

            static void ExtractVocab(string questFolder, string vocab)
            {
                wordFreqDict = new ItemFreqIndexDictionary("WordVocab");
                entityFreqDict = new ItemFreqIndexDictionary("EntityVocab");

                int fileIdx = 0;
                DirectoryInfo directory = new DirectoryInfo(questFolder);
                foreach (FileInfo sampleFile in directory.EnumerateFiles())
                {
                    using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                    {
                        string url = mreader.ReadLine(); mreader.ReadLine();
                        string context = mreader.ReadLine(); mreader.ReadLine();
                        string query = mreader.ReadLine(); mreader.ReadLine();
                        string answer = mreader.ReadLine(); mreader.ReadLine();

                        foreach (string word in context.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }

                        foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else if (word.StartsWith("@placeholder")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }
                    }
                    if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract Vocab from Corpus {0}", fileIdx); }
                }

                /// keep top 50K vocabary.
                wordFreqDict.Filter(0, WordVocabLimited);
                using (StreamWriter mwriter = new StreamWriter(vocab))
                {
                    entityFreqDict.Save(mwriter);
                    wordFreqDict.Save(mwriter);
                }
            }

            static void ExtractVocab(string trainFolder, string testFolder, string validFolder, string vocab)
            {
                wordFreqDict = new ItemFreqIndexDictionary("WordVocab");
                entityFreqDict = new ItemFreqIndexDictionary("EntityVocab");

                int fileIdx = 0;
                DirectoryInfo directory = new DirectoryInfo(trainFolder);
                foreach (FileInfo sampleFile in directory.EnumerateFiles())
                {
                    using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                    {
                        string url = mreader.ReadLine(); mreader.ReadLine();
                        string context = mreader.ReadLine(); mreader.ReadLine();
                        string query = mreader.ReadLine(); mreader.ReadLine();
                        string answer = mreader.ReadLine(); mreader.ReadLine();

                        foreach (string word in context.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }

                        foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else if (word.StartsWith("@placeholder")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }
                    }
                    if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract Vocab from Train Corpus {0}", fileIdx); }
                }

                DirectoryInfo testdirectory = new DirectoryInfo(testFolder);
                foreach (FileInfo sampleFile in testdirectory.EnumerateFiles())
                {
                    using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                    {
                        string url = mreader.ReadLine(); mreader.ReadLine();
                        string context = mreader.ReadLine(); mreader.ReadLine();
                        string query = mreader.ReadLine(); mreader.ReadLine();
                        string answer = mreader.ReadLine(); mreader.ReadLine();

                        foreach (string word in context.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }

                        foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else if (word.StartsWith("@placeholder")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }
                    }
                    if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract Vocab from Test Corpus {0}", fileIdx); }
                }

                DirectoryInfo validdirectory = new DirectoryInfo(validFolder);
                foreach (FileInfo sampleFile in validdirectory.EnumerateFiles())
                {
                    using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                    {
                        string url = mreader.ReadLine(); mreader.ReadLine();
                        string context = mreader.ReadLine(); mreader.ReadLine();
                        string query = mreader.ReadLine(); mreader.ReadLine();
                        string answer = mreader.ReadLine(); mreader.ReadLine();

                        foreach (string word in context.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }

                        foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (word.StartsWith("@entity")) entityFreqDict.PushItem(word);
                            else if (word.StartsWith("@placeholder")) entityFreqDict.PushItem(word);
                            else wordFreqDict.PushItem(word);
                        }
                    }
                    if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract Vocab from Valid Corpus {0}", fileIdx); }
                }

                /// keep top 50K vocabary.
                if(WordVocabLimited > 0) wordFreqDict.Filter(0, WordVocabLimited);
                using (StreamWriter mwriter = new StreamWriter(vocab))
                {
                    entityFreqDict.Save(mwriter);
                    wordFreqDict.Save(mwriter);
                }
            }

            static void ExtractToDebugFile(string questFolder, string debugFile)
            {
                using (StreamWriter indexWriter = new StreamWriter(debugFile))
                {
                    int fileIdx = 0;
                    DirectoryInfo directory = new DirectoryInfo(questFolder);
                    foreach (FileInfo sampleFile in directory.EnumerateFiles())
                    {
                        using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                        {
                            string url = mreader.ReadLine(); mreader.ReadLine();
                            string context = mreader.ReadLine(); mreader.ReadLine();
                            string query = mreader.ReadLine(); mreader.ReadLine();
                            string answer = mreader.ReadLine(); mreader.ReadLine();

                            indexWriter.WriteLine("{0}\t{1}\t{2}", context, query, answer);
                        }
                        if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract String from Corpus {0}", fileIdx); }
                    }
                }
            }

            /// <summary>
            /// Unknown Index = 0;
            /// </summary>
            static void ExtractCorpusIndex(string questFolder, string questIndexFile)
            {
                using (StreamWriter indexWriter = new StreamWriter(questIndexFile))
                {
                    int fileIdx = 0;
                    DirectoryInfo directory = new DirectoryInfo(questFolder);
                    foreach (FileInfo sampleFile in directory.EnumerateFiles())
                    {
                        using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                        {
                            string url = mreader.ReadLine(); mreader.ReadLine();
                            string context = mreader.ReadLine(); mreader.ReadLine();
                            string query = mreader.ReadLine(); mreader.ReadLine();
                            string answer = mreader.ReadLine(); mreader.ReadLine();

                            int answerIndex = entityFreqDict.IndexItem(answer);

                            Dictionary<int, float> answerLabel = new Dictionary<int, float>();

                            List<int> contextIndex = new List<int>();
                            int pos = 0;
                            foreach (string word in context.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                if (word.StartsWith("@entity"))
                                {
                                    int idx = entityFreqDict.IndexItem(word); contextIndex.Add(idx + 1);
                                    if (answerIndex == idx) { answerLabel.Add(pos, 1); }
                                    else { answerLabel.Add(pos, 0); }
                                }
                                else { int idx = wordFreqDict.IndexItem(word); contextIndex.Add(idx >= 0 ? idx + entityFreqDict.ItemDictSize + 1 : 0); }
                                pos++;
                            }

                            List<int> queryIndex = new List<int>();
                            foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                if (word.StartsWith("@entity")) { int idx = entityFreqDict.IndexItem(word); queryIndex.Add(idx + 1); }
                                else if (word.StartsWith("@placeholder")) { int idx = entityFreqDict.IndexItem(word); queryIndex.Add(idx + 1); }
                                else { int idx = wordFreqDict.IndexItem(word); queryIndex.Add(idx >= 0 ? idx + entityFreqDict.ItemDictSize + 1 : 0); }
                            }

                            indexWriter.WriteLine("{0}\t{1}\t{2}", string.Join(" ", contextIndex), string.Join(" ", queryIndex), TextUtil.Dict2Str(answerLabel));
                        }
                        if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract Index from Corpus {0}", fileIdx); }
                    }
                }
            }

            #region Entity Shuffle.
            public class EntityShuffleRunner : StructRunner
            {
                SeqSparseBatchData Query { get; set; }
                SeqSparseBatchData Passage { get; set; }

                public EntityShuffleRunner(SeqSparseBatchData query, SeqSparseBatchData passage, RunnerBehavior behavior) : base(Structure.Empty, behavior)
                {
                    Query = query;
                    Passage = passage;
                }

                public override void Forward()
                {
                    if (entityShuffle != null) ShuffleEntity(Query, Passage, entityShuffle);
                }
            }

            public static string EntityName(int id)
            {
                int nid = reverseShuffle == null ? id - 1 : reverseShuffle[id - 1];
                return entityFreqDict.ItemIndex.GetName(nid);
            }

            static List<Dictionary<int, int>> entityShuffleList = new List<Dictionary<int, int>>();

            static Dictionary<int, int> entityShuffle = null;
            static Dictionary<int, int> reverseShuffle = null;
            public static void InitEntityShuffle()
            {
                int placeHolderId = entityFreqDict.IndexItem("@placeholder");
                entityShuffle = new Dictionary<int, int>();
                DataRandomShuffling shuffle = new DataRandomShuffling(entityFreqDict.ItemDictSize);
                for (int i = 0; i < entityFreqDict.ItemDictSize; i++)
                {
                    int eid = shuffle.RandomNext();
                    entityShuffle.Add(i, eid);
                }

                for (int i = 0; i < entityFreqDict.ItemDictSize; i++)
                {
                    if (entityShuffle[i] == placeHolderId)
                    {
                        entityShuffle[i] = entityShuffle[placeHolderId];
                        entityShuffle[placeHolderId] = placeHolderId;
                        break;
                    }
                }
                reverseShuffle = new Dictionary<int, int>();
                foreach (KeyValuePair<int,int> item in entityShuffle)
                {
                    reverseShuffle.Add(item.Value, item.Key);
                }

                entityShuffleList.Add(entityShuffle);
            }

            static void ShuffleEntity(SeqSparseBatchData query, SeqSparseBatchData passage, Dictionary<int, int> shuffle)
            {
                query.FeaIdx.SyncToCPU(query.ElementSize);
                for (int e = 0; e < query.ElementSize; e++)
                {
                    if (query.FeaIdx.MemPtr[e] >= 1 && query.FeaIdx.MemPtr[e] <= entityFreqDict.ItemDictSize)
                    {
                        query.FeaIdx.MemPtr[e] = shuffle[query.FeaIdx.MemPtr[e] - 1] + 1;
                    }
                }
                query.FeaIdx.SyncFromCPU(query.ElementSize);


                passage.FeaIdx.SyncToCPU(passage.ElementSize);
                for (int e = 0; e < passage.ElementSize; e++)
                {
                    if (passage.FeaIdx.MemPtr[e] >= 1 && passage.FeaIdx.MemPtr[e] <= entityFreqDict.ItemDictSize)
                    {
                        passage.FeaIdx.MemPtr[e] = shuffle[passage.FeaIdx.MemPtr[e] - 1] + 1;
                    }
                }
                passage.FeaIdx.SyncFromCPU(passage.ElementSize);
            }
            #endregion.

            static void ExtractCorpusBinary(string questIndexFile, string contextBin, string queryBin, string answerBin, int miniBatchSize)
            {
                SeqSparseBatchData context = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData query = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                GeneralBatchInputData answer = new GeneralBatchInputData(new GeneralBatchInputDataStat() { FeatureType = FeatureDataType.SparseFeature, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);

                BinaryWriter contextWriter = FileUtil.CreateBinaryWrite(contextBin);
                BinaryWriter queryWriter = FileUtil.CreateBinaryWrite(queryBin);
                BinaryWriter answerWriter = FileUtil.CreateBinaryWrite(answerBin);

                using (StreamReader indexReader = new StreamReader(questIndexFile))
                {
                    int lineIdx = 0;
                    while (!indexReader.EndOfStream)
                    {
                        string[] items = indexReader.ReadLine().Split('\t');

                        List<Dictionary<int, float>> contextFea = new List<Dictionary<int, float>>();
                        foreach (string term in items[0].Split(' ')) { var tmp = new Dictionary<int, float>(); tmp[int.Parse(term)] = 1; contextFea.Add(tmp); }
                        context.PushSample(contextFea);

                        List<Dictionary<int, float>> queryFea = new List<Dictionary<int, float>>();
                        foreach (string term in items[1].Split(' ')) { var tmp = new Dictionary<int, float>(); tmp[int.Parse(term)] = 1; queryFea.Add(tmp); }
                        query.PushSample(queryFea);

                        Dictionary<int, float> answerFea = TextUtil.Str2Dict(items[2]);
                        answer.PushSample(answerFea);

                        if (context.BatchSize >= miniBatchSize)
                        {
                            context.PopBatchToStat(contextWriter);
                            query.PopBatchToStat(queryWriter);
                            answer.PopBatchToStat(answerWriter);
                        }

                        if (++lineIdx % 1000 == 0) { Console.WriteLine("Extract Binary from Corpus {0}", lineIdx); }
                    }
                    context.PopBatchCompleteStat(contextWriter);
                    query.PopBatchCompleteStat(queryWriter);
                    answer.PopBatchCompleteStat(answerWriter);

                    Console.WriteLine("Context Stat {0}", context.Stat.ToString());
                    Console.WriteLine("Query Stat {0}", query.Stat.ToString());
                    Console.WriteLine("Answer Stat {0}", answer.Stat.ToString());
                }
            }

            public static void Init()
            {
                #region Preprocess Data.
                /// Step 1 : Extract Vocab from training Corpus.
                if (!File.Exists(BuilderParameters.Vocab)) ExtractVocab(BuilderParameters.TrainFolder, BuilderParameters.TestFolder, BuilderParameters.ValidFolder, BuilderParameters.Vocab);
                else
                {
                    using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab))
                    {
                        entityFreqDict = new ItemFreqIndexDictionary(mreader);
                        wordFreqDict = new ItemFreqIndexDictionary(mreader);
                    }
                }

                /// Step 2 : Corpus 2 Index.
                if (BuilderParameters.IsTrainFile && !File.Exists(BuilderParameters.TrainIndexData))
                {
                    ExtractCorpusIndex(BuilderParameters.TrainFolder, BuilderParameters.TrainIndexData);
                }
                if (BuilderParameters.IsTestFile && !File.Exists(BuilderParameters.TestIndexData))
                {
                    ExtractCorpusIndex(BuilderParameters.TestFolder, BuilderParameters.TestIndexData);
                }

                if (BuilderParameters.IsValidFile && !File.Exists(BuilderParameters.ValidIndexData))
                {
                    ExtractCorpusIndex(BuilderParameters.ValidFolder, BuilderParameters.ValidIndexData);
                }

                if(BuilderParameters.DebugFile != string.Empty && BuilderParameters.DebugFile != "" && !File.Exists(BuilderParameters.DebugFile+".test.txt"))
                {
                    ExtractToDebugFile(BuilderParameters.TestFolder, BuilderParameters.DebugFile + ".test.txt");
                }

                if (BuilderParameters.DebugFile != string.Empty && BuilderParameters.DebugFile != "" && !File.Exists(BuilderParameters.DebugFile + ".valid.txt"))
                {
                    ExtractToDebugFile(BuilderParameters.ValidFolder, BuilderParameters.DebugFile + ".valid.txt");
                }


                /// Step 3 : Index 2 Binary Data.
                if (BuilderParameters.IsTrainFile &&
                    (!File.Exists(BuilderParameters.TrainContextBinary) || !File.Exists(BuilderParameters.TrainQueryBinary) || !File.Exists(BuilderParameters.TrainAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.TrainIndexData, BuilderParameters.TrainContextBinary, BuilderParameters.TrainQueryBinary, BuilderParameters.TrainAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                if (BuilderParameters.IsTestFile &&
                    (!File.Exists(BuilderParameters.TestContextBinary) || !File.Exists(BuilderParameters.TestQueryBinary) || !File.Exists(BuilderParameters.TestAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.TestIndexData, BuilderParameters.TestContextBinary, BuilderParameters.TestQueryBinary, BuilderParameters.TestAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                if (BuilderParameters.IsValidFile &&
                    (!File.Exists(BuilderParameters.ValidContextBinary) || !File.Exists(BuilderParameters.ValidQueryBinary) || !File.Exists(BuilderParameters.ValidAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.ValidIndexData, BuilderParameters.ValidContextBinary, BuilderParameters.ValidQueryBinary, BuilderParameters.ValidAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                #endregion.

                #region Load Data.
                if (BuilderParameters.IsTrainFile)
                {
                    TrainContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBinary, BuilderParameters.TrainContextBinary + ".cursor");
                    TrainQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBinary, BuilderParameters.TrainQueryBinary + ".cursor");
                    TrainAnswer = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TrainAnswerBinary, BuilderParameters.TrainAnswerBinary + ".cursor");

                    TrainContext.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQuery.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainAnswer.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                }

                if (BuilderParameters.IsTestFile)
                {
                    TestContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestContextBinary, BuilderParameters.TestContextBinary + ".cursor");
                    TestQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestQueryBinary, BuilderParameters.TestQueryBinary + ".cursor");
                    TestAnswer = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TestAnswerBinary, BuilderParameters.TestAnswerBinary + ".cursor");

                    TestContext.InitThreadSafePipelineCashier(64, false);
                    TestQuery.InitThreadSafePipelineCashier(64, false);
                    TestAnswer.InitThreadSafePipelineCashier(64, false);
                }

                if (BuilderParameters.IsValidFile)
                {
                    ValidContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBinary, BuilderParameters.ValidContextBinary + ".cursor");
                    ValidQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBinary, BuilderParameters.ValidQueryBinary + ".cursor");
                    ValidAnswer = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.ValidAnswerBinary, BuilderParameters.ValidAnswerBinary + ".cursor");

                    ValidContext.InitThreadSafePipelineCashier(64, false);
                    ValidQuery.InitThreadSafePipelineCashier(64, false);
                    ValidAnswer.InitThreadSafePipelineCashier(64, false);
                }
                #endregion.
            }
        }
    }
}
