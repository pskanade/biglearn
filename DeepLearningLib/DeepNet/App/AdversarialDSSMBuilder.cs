﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BigLearn.DeepNet
{
    //public class AdversarialDSSMBuilder : Builder
    //{
    //    public class BuilderParameters : BaseModelArgument<BuilderParameters>
    //    {
    //        public static string DataStream { get { return Argument["DATA"].Value; } }
    //        public static string L3GVocab { get { return Argument["L3G-VOCAB"].Value; } }
    //        public static int FeaDim { get { return int.Parse(Argument["FEA-DIM"].Value); } }
    //        public static string TrainSrcBin { get { return string.Format("{0}.src.bin", DataStream); } }
    //        public static string TrainTgtBin { get { return string.Format("{0}.tgt.bin", DataStream); } }
    //        public static string TrainMatchBin { get { return string.Format("{0}.match.bin", DataStream); } }

    //        public static string ValidDataStream { get { return Argument["VALID-DATA"].Value; } }
    //        public static string ValidSrcBin { get { return string.Format("{0}.src.bin", ValidDataStream); } }
    //        public static string ValidTgtBin { get { return string.Format("{0}.tgt.bin", ValidDataStream); } }
    //        public static string ValidMatchBin { get { return string.Format("{0}.match.bin", ValidDataStream); } }

    //        public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

    //        public static int[] SRC_LAYER_DIM { get { return Argument["SRC-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static A_Func[] SRC_ACTIVATION { get { return Argument["SRC-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
    //        public static bool[] SRC_LAYER_BIAS { get { return Argument["SRC-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
    //        public static N_Type[] SRC_ARCH { get { return Argument["SRC-ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
    //        public static int[] SRC_WINDOW { get { return Argument["SRC-WINDOW"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static bool IS_SRC_SEED_UPDATE { get { return int.Parse(Argument["SRC-SEED-UPDATE"].Value) > 0; } }
    //        public static float[] SRC_DROPOUT { get { if (Argument["SRC-DROPOUT"].Value.Equals(string.Empty)) return SRC_LAYER_DIM.Select(i => 0.0f).ToArray(); else return Argument["SRC-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

    //        public static int[] TGT_LAYER_DIM { get { return Argument["TGT-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static A_Func[] TGT_ACTIVATION { get { return Argument["TGT-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
    //        public static bool[] TGT_LAYER_BIAS { get { return Argument["TGT-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
    //        public static N_Type[] TGT_ARCH { get { return Argument["TGT-ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
    //        public static int[] TGT_WINDOW { get { return Argument["TGT-WINDOW"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static bool IS_TGT_SEED_UPDATE { get { return int.Parse(Argument["TGT-SEED-UPDATE"].Value) > 0; } }
    //        public static float[] TGT_DROPOUT { get { if (Argument["TGT-DROPOUT"].Value.Equals(string.Empty)) return TGT_LAYER_DIM.Select(i => 0.0f).ToArray(); else return Argument["TGT-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

    //        public static bool IsShareModel { get { return int.Parse(Argument["SHARE-MODEL"].Value) > 0; } }
    //        public static SimilarityType SimiType { get { return (SimilarityType)(int.Parse(Argument["SIM-TYPE"].Value)); } }

    //        public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
    //        public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

    //        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
    //        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
    //        public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

    //        public static string ScoreOutputPath
    //        {
    //            get
    //            {
    //                return Argument["SCORE-PATH"].Value.Equals(string.Empty) ? LogFile + ".tmp.score" : Argument["SCORE-PATH"].Value;
    //            }
    //        }

    //        public static string SRC_EMBED_FILE { get { return Argument["SRC-EMBED-FILE"].Value; } }
    //        public static string TGT_EMBED_FILE { get { return Argument["TGT-EMBED-FILE"].Value; } }

    //        public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
    //        public static DeviceType Device { get { return MathOperatorManager.Device(GPUID); } }


    //        static BuilderParameters()
    //        {
    //            Argument.Add("DATA", new ParameterArgument(string.Empty, "Train Data."));
    //            Argument.Add("SRC", new ParameterArgument(string.Empty, "Src Train Data."));
    //            Argument.Add("TGT", new ParameterArgument(string.Empty, "Tgt Train Data."));
    //            Argument.Add("MATCH", new ParameterArgument(string.Empty, "Match Train Data."));
    //            Argument.Add("VALID-DATA", new ParameterArgument(string.Empty, "Valid Data."));

    //            Argument.Add("L3G-VOCAB", new ParameterArgument(string.Empty, "Vocab File"));
    //            Argument.Add("FEA-DIM", new ParameterArgument(string.Empty, "Input Feature Dim"));

    //            Argument.Add("MINI-BATCH", new ParameterArgument(string.Empty, "Mini Batch Size."));


    //            Argument.Add("SRC-LAYER-DIM", new ParameterArgument("300,300", "Source Layer Dim"));
    //            Argument.Add("SRC-ACTIVATION", new ParameterArgument("1,1", ParameterUtil.EnumValues(typeof(A_Func))));
    //            Argument.Add("SRC-LAYER-BIAS", new ParameterArgument("0,0", "Source Layer Bias"));
    //            Argument.Add("SRC-ARCH", new ParameterArgument("1,0", ParameterUtil.EnumValues(typeof(N_Type))));
    //            Argument.Add("SRC-WINDOW", new ParameterArgument("3,1", "Source DNN Window Size"));
    //            Argument.Add("SRC-DROPOUT", new ParameterArgument(string.Empty, "Source DNN Dropout"));

    //            Argument.Add("TGT-LAYER-DIM", new ParameterArgument("300,300", "Target Layer Dim"));
    //            Argument.Add("TGT-ACTIVATION", new ParameterArgument("1,1", ParameterUtil.EnumValues(typeof(A_Func))));
    //            Argument.Add("TGT-LAYER-BIAS", new ParameterArgument("0,0", "Target Layer Bias"));
    //            Argument.Add("TGT-ARCH", new ParameterArgument("1,0", ParameterUtil.EnumValues(typeof(N_Type))));
    //            Argument.Add("TGT-WINDOW", new ParameterArgument("3,1", "Target DNN Window Size"));
    //            Argument.Add("TGT-DROPOUT", new ParameterArgument(string.Empty, "Target DNN Dropout"));

    //            Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

    //            Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
    //            Argument.Add("SHARE-MODEL", new ParameterArgument("0", "Share Source and Target Model; 0 : no share; 1 : share;"));
    //            Argument.Add("SIM-TYPE", new ParameterArgument(((int)SimilarityType.CosineSimilarity).ToString(), ParameterUtil.EnumValues(typeof(SimilarityType))));

    //            Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.LogBayesianRating).ToString(), string.Format("{0},{1}", LossFunctionType.LogBayesianRating, LossFunctionType.BayesianRating)));
    //            Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));

    //            Argument.Add("GPUID", new ParameterArgument("1", "GPU Device ID"));
    //            Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score Path"));
    //            Argument.Add("METRIC-PATH", new ParameterArgument(string.Empty, "Output Metric Path"));
    //            Argument.Add("SRC-EMBED-FILE", new ParameterArgument(string.Empty, "Indicates whether src embedding is outputted"));
    //            Argument.Add("TGT-EMBED-FILE", new ParameterArgument(string.Empty, "Indicates whether tgt embedding is outputted"));

    //            Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));
    //        }
    //    }

    //    public override BuilderType Type { get { return BuilderType.ADVERSARIAL_DSSM; } }

    //    public override void InitStartup(string fileName)
    //    {
    //        BuilderParameters.Parse(fileName);
    //        //EvaluationParameter
    //    }

    //    public class VecNormRunner : StructRunner
    //    {
    //        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
    //        new HiddenBatchData Input { get; set; }

    //        CudaPieceFloat tmp = null;
    //        public VecNormRunner(HiddenBatchData data, RunnerBehavior behavior)
    //       : base(Structure.Empty, behavior)
    //        {
    //            Input = data;
    //            Output = data;
    //            tmp = new CudaPieceFloat(Output.MAX_BATCHSIZE, behavior.Device);
    //        }

    //        public override void Forward()
    //        {
    //            ComputeLib.MatrixL2Norm(Input.Output.Data, Input.Dim, Input.BatchSize, tmp);

    //            tmp.SyncToCPU(Output.BatchSize);
    //            for (int i = 0; i < Output.BatchSize; i++)
    //            {
    //                tmp.MemPtr[i] = 1.0f / (tmp.MemPtr[i] + Util.GPUEpsilon);
    //            }
    //            tmp.SyncFromCPU(Output.BatchSize);

    //            ComputeLib.Scale_MatrixMask(Input.Output.Data, 0, CudaPieceInt.Empty, 0,
    //                    Output.Output.Data, 0, CudaPieceInt.Empty, 0, Output.Dim, Output.BatchSize, tmp, 0);
    //        }

    //        public override void Backward(bool cleanDeriv)
    //        {
    //            throw new NotImplementedException();
    //        }
    //    }

    //    public static ComputationGraph BuildEmbeddingCG( IDataCashier<SeqSparseBatchData, SequenceDataStat> data, List<float[]> embed, DNNStructure dnn, RunnerBehavior rb)
    //    {
    //        ComputationGraph cg = new ComputationGraph();
    //        SeqSparseBatchData input = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(data, rb));
    //        HiddenBatchData srcOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(dnn, input, rb));
    //        cg.AddRunner(new VecNormRunner(srcOutput, rb));
    //        cg.AddRunner(new VectorDumpV2Runner(srcOutput.Output, embed));
    //        return cg;
    //    }

    //    public static ComputationGraph BuildComputationGraph(
    //                IDataCashier<SeqSparseBatchData, SequenceDataStat> srcData,
    //                IDataCashier<SeqSparseBatchData, SequenceDataStat> tgtData,
    //                IDataCashier<BiMatchBatchData, BiMatchBatchDataStat> matchData,
    //                DSSMStructure dssmModel, RunnerBehavior rb)
    //    {
    //        ComputationGraph cg = new ComputationGraph();

    //        //RunnerBehavior rb = new RunnerBehavior() { RunMode = mode, Device = BuilderParameters.Device };

    //        SeqSparseBatchData srcInput = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(srcData, rb));
    //        SeqSparseBatchData tgtInput = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(tgtData, rb));
    //        BiMatchBatchData matchInput = (BiMatchBatchData)cg.AddDataRunner(new DataRunner<BiMatchBatchData, BiMatchBatchDataStat>(matchData, rb));

    //        /// embedding for source text.
    //        HiddenBatchData srcOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(dssmModel.SrcDnn, srcInput, rb));
    //        /// embedding for target text.
    //        HiddenBatchData tgtOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(dssmModel.TgtDnn, tgtInput, rb));

    //        /// similarity between source text and target text.
    //        HiddenBatchData simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(srcOutput, tgtOutput, matchInput, BuilderParameters.SimiType, rb));

    //        Cudalib.PairwiseRankingLoss

    //        switch (rb.RunMode)
    //        {
    //            case DNNRunMode.Train:
    //                switch (BuilderParameters.LossFunction)
    //                {
    //                    case LossFunctionType.LogBayesianRating:
    //                        cg.AddObjective(new BayesianRatingRunner(simiOutput.Output.Data, simiOutput.Deriv.Data, matchInput.MatchInfo, matchInput, BuilderParameters.Gamma, rb));
    //                        break;
    //                    case LossFunctionType.BayesianRating:
    //                        cg.AddObjective(new BayesianRatingRunner(simiOutput.Output.Data, simiOutput.Deriv.Data, matchInput.MatchInfo, matchInput, BuilderParameters.Gamma, rb, false));
    //                        break;
    //                }
    //                break;
    //            case DNNRunMode.Predict:
    //                if (!BuilderParameters.SRC_EMBED_FILE.Equals(string.Empty))
    //                    cg.AddRunner(new VectorDumpRunner(srcOutput.Output, BuilderParameters.SRC_EMBED_FILE, true));

    //                if (!BuilderParameters.TGT_EMBED_FILE.Equals(string.Empty))
    //                    cg.AddRunner(new VectorDumpRunner(tgtOutput.Output, BuilderParameters.TGT_EMBED_FILE, true));

    //                cg.AddRunner(new VectorDumpRunner(simiOutput.Output, BuilderParameters.ScoreOutputPath, true));
    //                cg.AddRunner(new TopKHitRunner(simiOutput.Output.Data, matchInput.MatchInfo, matchInput, BuilderParameters.Gamma, rb));
    //                /*
    //                switch (BuilderParameters.Evaluation)
    //                {
    //                    case EvaluationType.NDCG:
    //                        cg.AddRunner(new NDCGDiskDumpRunner(simiOutput.Output, BuilderParameters.MappingValidData, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
    //                        break;
    //                    case EvaluationType.AUC:
    //                        cg.AddRunner(new AUCDiskDumpRunner(dssmData.ScoreInput.Data, simiOutput.Output, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
    //                        break;
    //                    case EvaluationType.MSE:
    //                        cg.AddRunner(new RMSEDiskDumpRunner(dssmData.ScoreInput.Data, simiOutput.Output, BuilderParameters.Gamma, true, BuilderParameters.ScoreOutputPath));
    //                        break;
    //                }
    //                */
    //                break;
    //        }

    //        return cg;
    //    }
    //    public override void Rock()
    //    {
    //        Logger.OpenLog(BuilderParameters.LogFile);

    //        Logger.WriteLog("Loading Training/Validation Data.");
    //        DataPanel.Init();
    //        Logger.WriteLog("Load Data Finished.");

    //        DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
    //        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

    //        DSSMStructure gDSSM = null;
    //        DSSMStructure dDSSM = null;

    //        gDSSM = new DSSMStructure(device,
    //                    BuilderParameters.SimiType, BuilderParameters.IsShareModel,
    //                    DataPanel.TrainSrcBin.Stat.FEATURE_DIM, BuilderParameters.SRC_LAYER_DIM, BuilderParameters.SRC_ACTIVATION,
    //                    BuilderParameters.SRC_LAYER_BIAS, BuilderParameters.SRC_ARCH, BuilderParameters.SRC_WINDOW, BuilderParameters.SRC_DROPOUT,
    //                    DataPanel.TrainTgtBin.Stat.FEATURE_DIM, BuilderParameters.TGT_LAYER_DIM, BuilderParameters.TGT_ACTIVATION,
    //                    BuilderParameters.TGT_LAYER_BIAS, BuilderParameters.TGT_ARCH, BuilderParameters.TGT_WINDOW, BuilderParameters.TGT_DROPOUT);

    //        dDSSM = new DSSMStructure(device,
    //                BuilderParameters.SimiType, BuilderParameters.IsShareModel,
    //                DataPanel.TrainSrcBin.Stat.FEATURE_DIM, BuilderParameters.SRC_LAYER_DIM, BuilderParameters.SRC_ACTIVATION,
    //                BuilderParameters.SRC_LAYER_BIAS, BuilderParameters.SRC_ARCH, BuilderParameters.SRC_WINDOW, BuilderParameters.SRC_DROPOUT,
    //                DataPanel.TrainTgtBin.Stat.FEATURE_DIM, BuilderParameters.TGT_LAYER_DIM, BuilderParameters.TGT_ACTIVATION,
    //                BuilderParameters.TGT_LAYER_BIAS, BuilderParameters.TGT_ARCH, BuilderParameters.TGT_WINDOW, BuilderParameters.TGT_DROPOUT);


    //        Logger.WriteLog("Loading DSSM Structure.");
    //        Logger.WriteLog(gDSSM.SrcDnn.DNN_Descr());
    //        Logger.WriteLog(gDSSM.TgtDnn.DNN_Descr());
    //        Logger.WriteLog("Load DSSM Structure Finished.");

    //        switch (BuilderParameters.RunMode)
    //        {
    //            case DNNRunMode.Train:
    //                if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

    //                ComputationGraph SrcEmbedingCG = BuildEmbeddingCG(DataPanel.TrainSrcBin, DataPanel.TrainSrcEmbed, gDSSM.SrcDnn,
    //                    new RunnerBehavior() { Device = device, Computelib = computeLib, RunMode = DNNRunMode.Predict });

    //                ComputationGraph TgtEmbedingCG = BuildEmbeddingCG(DataPanel.TrainTgtBin, DataPanel.TrainTgtEmbed, gDSSM.TgtDnn,
    //                    new RunnerBehavior() { Device = device, Computelib = computeLib, RunMode = DNNRunMode.Predict });

    //                for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
    //                {
    //                    DataPanel.TrainSrcEmbed.Clear();
    //                    DataPanel.TrainTgtEmbed.Clear();
    //                    SrcEmbedingCG.Execute();
    //                    TgtEmbedingCG.Execute();

    //                    DataPanel.NegativSampling(10, Math.Max(2 + iter * 0.1f, 10));

    //                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainMTgtBin, DataPanel.TrainMatchBin, dDSSM,
    //                        new RunnerBehavior() { Device = device, Computelib = computeLib, RunMode = DNNRunMode.Train });
    //                    trainCG.Execute();


    //                }





    //                ComputationGraph predCG = null;
    //                if (!BuilderParameters.ValidDataStream.Equals(string.Empty))
    //                {
    //                    predCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin, DataPanel.ValidMatchBin, dssmModel,
    //                        new RunnerBehavior() { Device = device, Computelib = computeLib, RunMode = DNNRunMode.Predict });
    //                }

    //                trainCG.InitOptimizer(dssmModel, OptimizerParameters.StructureOptimizer,
    //                    new RunnerBehavior() { Device = device, Computelib = computeLib, RunMode = DNNRunMode.Train });

    //                for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
    //                {
    //                    SrcEmbedingCG.Execute();
    //                    TgtEmbedingCG.Execute();
    //                    /// generate negative samples.





    //                    double loss = trainCG.Execute();

    //                    Logger.WriteLog("Iteration {0}, Total MiniBatch {1}, Avg Loss {2}", iter, DataPanel.TrainSrcBin.Stat.TotalBatchNumber, loss);

    //                    if (predCG != null) predCG.Execute();

    //                    if (DeepNet.BuilderParameters.ModelSavePerIteration)
    //                    {
    //                        using (BinaryWriter writer = new BinaryWriter(new FileStream(string.Format(@"{0}\\DSSM.{1}.model", BuilderParameters.ModelOutputPath, iter.ToString()), FileMode.Create, FileAccess.Write)))
    //                        {
    //                            dssmModel.Serialize(writer);
    //                        }
    //                    }
    //                }
    //                break;
    //            case DNNRunMode.Predict:
    //                Logger.WriteLog("Loading DSSM Structure.");
    //                ComputationGraph cg = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin, DataPanel.TrainMatchBin, dssmModel,
    //                    new RunnerBehavior() { Device = device, Computelib = computeLib, RunMode = DNNRunMode.Predict });
    //                cg.Execute();
    //                break;
    //        }
    //        Logger.CloseLog();
    //    }

    //    public class DataPanel
    //    {
    //        public static List<List<Dictionary<int, float>>> TrainSrcFeas = new List<List<Dictionary<int, float>>>();
    //        public static List<float[]> TrainSrcEmbed = new List<float[]>();
    //        public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainSrcBin = null;

    //        public static List<List<Dictionary<int, float>>> TrainTgtFeas = new List<List<Dictionary<int, float>>>();
    //        public static List<float[]> TrainTgtEmbed = new List<float[]>();
    //        public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainTgtBin = null;

    //        public static List<int[]> NegativeSample = new List<int[]>();

    //        public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainMTgtBin = null;
    //        public static DataCashier<BiMatchBatchData, BiMatchBatchDataStat> TrainMatchBin = null;

    //        static L3GTextHash wordHash = null;

    //        public static void NegativSampling(int K, float temp)
    //        {
    //            for (int i = 0; i < TrainSrcEmbed.Count; i++)
    //            {
    //                float[] weis = new float[TrainTgtEmbed.Count];
    //                for (int j = 0; j < TrainTgtEmbed.Count; j++)
    //                {
    //                    float score = temp * SseUtils.DotProductDense(TrainSrcEmbed[i], TrainTgtEmbed[j]);
    //                    if (j == i) score = -100 * temp;
    //                    weis[j] = (float) Math.Exp(score);
    //                }

    //                float sum = weis.Sum();
    //                int[] idx = new int[K];

    //                for (int k = 0; k < K; k++)
    //                {
    //                    idx[k] = Util.RandomSampling(weis, sum);
    //                }
    //                NegativeSample.Add(idx);
    //            }

    //            GenerateMatch();
    //        }


    //        public static void GenerateMatch()
    //        {


    //            BinaryWriter tgtDataWriter = new BinaryWriter(new FileStream("tmp.tgt.bin", FileMode.Create, FileAccess.Write));
    //            SeqSparseBatchData tgtBinary = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = wordHash.VocabSize }, DeviceType.CPU);

    //            BinaryWriter matchDataWriter = new BinaryWriter(new FileStream("tmp.match.bin", FileMode.Create, FileAccess.Write));
    //            BiMatchBatchData matchBinary = new BiMatchBatchData(new BiMatchBatchDataStat() { }, DeviceType.CPU);
    //            List<Tuple<int, int, float>> matchList = new List<Tuple<int, int, float>>();


    //            for (int i = 0; i < NegativeSample.Count; i++)
    //            {
    //                tgtBinary.PushSample(TrainTgtFeas[i]);
    //                matchList.Add(new Tuple<int, int, float>(i, tgtBinary.BatchSize - 1, 1));

    //                for (int k = 0; k < NegativeSample[i].Length; k++)
    //                {
    //                    int didx = NegativeSample[i][k];
    //                    tgtBinary.PushSample(TrainTgtFeas[didx]);
    //                    matchList.Add(new Tuple<int, int, float>(i, tgtBinary.BatchSize - 1, 0));
    //                }

    //                if ((i + 1) % BuilderParameters.MiniBatchSize == 0)
    //                {
    //                    tgtBinary.PopBatchToStat(tgtDataWriter);

    //                    matchBinary.SetMatch(matchList);
    //                    matchBinary.PopBatchToStat(matchDataWriter);
    //                    matchList.Clear();
    //                }

    //                if ( (i+1) % 100000 == 0) Console.WriteLine("Extract Row {0} into binary", (i+1));
    //            }

    //            tgtBinary.PopBatchCompleteStat(tgtDataWriter);
    //            Console.WriteLine("Target Data Stat {0}", tgtBinary.Stat.ToString());

    //            if (matchList.Count > 0) matchBinary.SetMatch(matchList);
    //            matchBinary.PopBatchCompleteStat(matchDataWriter);
    //            Console.WriteLine("Match Data Stat {0}", matchBinary.Stat.ToString());


    //            TrainMTgtBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>("tmp.tgt.bin");
    //            TrainMatchBin = new DataCashier<BiMatchBatchData, BiMatchBatchDataStat>("tmp.match.bin");

    //            if (BuilderParameters.RunMode == DNNRunMode.Train)
    //            {
    //                TrainMTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
    //                TrainMatchBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
    //            }
    //            else
    //            {
    //                TrainMTgtBin.InitThreadSafePipelineCashier(100, false);
    //                TrainMatchBin.InitThreadSafePipelineCashier(100, false);
    //            }
    //        }


    //        public static void GeneratePairBin(string dataStream, string srcbin, string tgtbin, int feaDim, int batchSize, L3GTextHash wordHash)
    //        {
    //            if (File.Exists(srcbin) == false || File.Exists(tgtbin) == false )
    //            {
    //                BinaryWriter srcDataWriter = new BinaryWriter(new FileStream(srcbin, FileMode.Create, FileAccess.Write));
    //                SeqSparseBatchData srcBinary = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = feaDim }, DeviceType.CPU);

    //                BinaryWriter tgtDataWriter = new BinaryWriter(new FileStream(tgtbin, FileMode.Create, FileAccess.Write));
    //                SeqSparseBatchData tgtBinary = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = feaDim }, DeviceType.CPU);

    //                using (StreamReader reader = new StreamReader(dataStream))
    //                {
    //                    int rowIdx = 0;
    //                    while (!reader.EndOfStream)
    //                    {
    //                        string[] items = reader.ReadLine().Split('\t');
    //                        string src = items[1].Trim().ToLower();
    //                        string[] tgts = items[4].Trim().ToLower().Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
    //                        string[] labels = items[3].Trim().ToLower().Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

    //                        List<Dictionary<int, float>> src_fea = wordHash == null ? TextUtil.Str2Dicts(src) : wordHash.SeqFeatureExtract(src);
    //                        if (src_fea.Count == 0)
    //                        {
    //                            src_fea.Add(new Dictionary<int, float>());
    //                            src_fea[0].Add(0, 0);
    //                        }
    //                        srcBinary.PushSample(src_fea);
    //                        TrainSrcFeas.Add(src_fea);

    //                        List<Dictionary<int, float>> tgt_fea = wordHash == null ? TextUtil.Str2Dicts(tgts[t]) : wordHash.SeqFeatureExtract(tgts[t]);

    //                        if (tgt_fea.Count == 0)
    //                        {
    //                            tgt_fea.Add(new Dictionary<int, float>());
    //                            tgt_fea[0].Add(0, 0);
    //                        }
    //                        tgtBinary.PushSample(tgt_fea);
    //                        TrainTgtFeas.Add(tgt_fea);

    //                        if (srcBinary.BatchSize >= batchSize)
    //                        {
    //                            srcBinary.PopBatchToStat(srcDataWriter);
    //                            tgtBinary.PopBatchToStat(tgtDataWriter);
    //                        }

    //                        if (++rowIdx % 100000 == 0) Console.WriteLine("Extract Row {0} into binary", (rowIdx));
    //                    }
    //                    srcBinary.PopBatchCompleteStat(srcDataWriter);
    //                    Console.WriteLine("Source Data Stat {0}", srcBinary.Stat.ToString());

    //                    tgtBinary.PopBatchCompleteStat(tgtDataWriter);
    //                    Console.WriteLine("Target Data Stat {0}", tgtBinary.Stat.ToString());

    //                }
    //            }
    //            else
    //            {
    //                Logger.WriteLog("bin file already exist. Skip Extracting Binary File {0}, {1}", srcbin, tgtbin);
    //            }
    //        }

    //        public static void Init()
    //        {

    //            if (!BuilderParameters.L3GVocab.Equals(string.Empty))
    //            {
    //                Console.WriteLine("Load L3G Vocab File.....");
    //                wordHash = new L3GTextHash(BuilderParameters.L3GVocab, new SimpleTextTokenizer(TextTokenizeParameters.WordTokens, TextTokenizeParameters.SentenceTokens, 512, 1024, 512));
    //            }
    //            else
    //            {
    //                Console.WriteLine("Please specify the l3g vocab.");
    //                throw new Exception();
    //            }

    //            int feaDim = wordHash == null ? BuilderParameters.FeaDim : wordHash.VocabSize;

    //            if (!BuilderParameters.DataStream.Equals(string.Empty))
    //            {
    //                GeneratePairBin(BuilderParameters.DataStream, BuilderParameters.TrainSrcBin, BuilderParameters.TrainTgtBin, feaDim, BuilderParameters.MiniBatchSize, wordHash);
    //                TrainSrcBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainSrcBin);
    //                TrainTgtBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainTgtBin);

    //                if (BuilderParameters.RunMode == DNNRunMode.Train)
    //                {
    //                    TrainSrcBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
    //                    TrainTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
    //                }
    //                else
    //                {
    //                    TrainSrcBin.InitThreadSafePipelineCashier(100, false);
    //                    TrainTgtBin.InitThreadSafePipelineCashier(100, false);
    //                }
    //            }

    //        }
    //    }
    //}


}
