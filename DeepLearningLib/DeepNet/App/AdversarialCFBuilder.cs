﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn.DeepNet
{
    public class AdversarialCFBuilder : Builder
    {
        public override BuilderType Type { get { return BuilderType.ADVERSARIAL_CF; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        const int EMB_DIM = 5;
        const int USER_NUM = 943;
        const int ITEM_NUM = 1683;
        const int BATCH_SIZE = 16;
        const float INIT_DELTA = 0.05f;
        public const string workdir = @"\\toronto-g1\PublicDataSource\Adversial_DSSM\";


        class PositiveMaskProcessor : StructRunner<Structure, BatchData>
        {
            public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

            new MatrixData Input { get; set; }
            GeneralBatchInputData u { get; set; }
            
            public PositiveMaskProcessor(MatrixData input, GeneralBatchInputData u_data, RunnerBehavior behavior)
                : base(Structure.Empty, behavior)
            {
                Input = input;
                u = u_data;
                Output = input;
            }

            public override void Forward()
            {
                u.FeatureIdx.SyncToCPU();
                Input.Output.SyncToCPU();
                for (int i = 0; i < u.ElementSize; i++)
                {
                    int uidx = u.FeatureIdx.MemPtr[i];
                    foreach(int iid in DataPanel.user_pos_train[uidx])
                    {
                        Input.Output.MemPtr[i * Input.Column + iid] = -10000;
                    }
                }
                Input.Output.SyncFromCPU();
            }
        }


        class GenerateNegRunner : StructRunner<Structure, BatchData>
        {
            public new GeneralBatchInputData Output { get { return (GeneralBatchInputData)base.Output; } set { base.Output = value; } }

            public BiMatchBatchData MatchInput { get; set; }

            public float[] Reward { get; set; }

            MatrixData Prob { get; set; }

            GeneralBatchInputData I_data { get; set; }

            int K { get; set; }
            public GenerateNegRunner(MatrixData prob, GeneralBatchInputData i_data, int k, RunnerBehavior behavior)
                : base(Structure.Empty, behavior)
            {
                Prob = prob;
                I_data = i_data;
                K = k;

                Output = new GeneralBatchInputData(new GeneralBatchInputDataStat()
                {
                    MAX_FEATUREDIM = ITEM_NUM,
                    MAX_BATCHSIZE = i_data.Stat.MAX_BATCHSIZE * k,
                    MAX_ELEMENTSIZE = i_data.Stat.MAX_BATCHSIZE * k,
                    FeatureType = FeatureDataType.SparseFeature
                }, behavior.Device);

                MatchInput = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = i_data.Stat.MAX_BATCHSIZE,
                    MAX_TGT_BATCHSIZE = i_data.Stat.MAX_BATCHSIZE * k,
                    MAX_MATCH_BATCHSIZE = i_data.Stat.MAX_BATCHSIZE * k,
                }, Behavior.Device);

                Output.BatchSize = i_data.Stat.MAX_BATCHSIZE * K;
                Output.ElementSize = i_data.Stat.MAX_BATCHSIZE * K;

                List<Tuple<int, int, float>> matchData = new List<Tuple<int, int, float>>();
                for (int i = 0; i < i_data.Stat.MAX_BATCHSIZE; i++)
                {
                    for (int t = 0; t < K; t++)
                    {
                        matchData.Add(new Tuple<int, int, float>(i, i * K + t, t == 0 ? 1 : 0));

                        Output.BatchIdx.MemPtr[i * K + t] = i * K + t + 1;
                        Output.FeatureValue.MemPtr[i * K + t] = 1;
                    }
                }
                MatchInput.SetMatch(matchData);
                Output.BatchIdx.SyncFromCPU();
                Output.FeatureValue.SyncFromCPU();

                Reward = new float[i_data.Stat.MAX_BATCHSIZE * (k - 1)];

            }

            public override void Forward()
            {
                MatchInput.SrcSize = I_data.BatchSize;
                MatchInput.TgtSize = I_data.BatchSize * K;
                MatchInput.MatchSize = I_data.BatchSize * K;

                
                Prob.Output.SyncToCPU();

                I_data.FeatureIdx.SyncToCPU();
                Output.BatchSize = Prob.Row * K;
                Output.ElementSize = Prob.Row * K;
                for (int i = 0; i < Prob.Row; i++)
                {
                    for (int t = 0; t < K; t++)
                    {
                        int idx = t == 0 ? I_data.FeatureIdx.MemPtr[i] : Util.Sample(Prob.Output.MemPtr, i * Prob.Column, Prob.Column, ParameterSetting.Random);
                        Output.FeatureIdx.MemPtr[i * K + t] = idx;
                    }
                }
                Output.FeatureIdx.SyncFromCPU();
                
            }

            public override void Backward(bool cleanDeriv)
            {
                Prob.Deriv.SyncToCPU();

                int ridx = 0;
                for (int i = 0; i < Prob.Row; i++)
                {
                    for (int t = 1; t < K; t++)
                    {
                        int fIdx = Output.FeatureIdx.MemPtr[i * K + t];

                        float p = Prob.Output.MemPtr[i * Prob.Column + fIdx];

                        Prob.Deriv.MemPtr[i * Prob.Column + fIdx] = 1.0f / (p + 0.00000001f) * Reward[ridx];
                        ridx += 1;
                    }
                }

                Prob.Deriv.SyncFromCPU();
            }
        }

        class DisRunner : CompositeNetRunner
        {
            GeneralBatchInputData UD;
            GeneralBatchInputData ID;

            EmbedStructure ULookup;
            EmbedStructure ILookup;
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            public DisRunner(GeneralBatchInputData uData, GeneralBatchInputData iData, BiMatchBatchData match,
                EmbedStructure uLookup, EmbedStructure iLookup, RunnerBehavior behavior) : base(behavior)
            {
                UD = uData;
                ID = iData;

                ULookup = uLookup;
                ILookup = iLookup;

                SparseMultiplicationRunner uRunner =  new SparseMultiplicationRunner(
                    new SparseMatrixData(uData.Stat.MAX_FEATUREDIM, uData.Stat.MAX_BATCHSIZE, uData.Stat.MAX_ELEMENTSIZE, uData.BatchIdx, uData.FeatureIdx, uData.FeatureValue, null, behavior.Device),
                    new MatrixData(uLookup.Dim, uLookup.VocabSize, uLookup.Embedding, uLookup.EmbeddingGrad, behavior.Device), behavior);
                LinkRunners.Add(uRunner);

                SparseMultiplicationRunner vRunner = new SparseMultiplicationRunner(
                    new SparseMatrixData(iData.Stat.MAX_FEATUREDIM, iData.Stat.MAX_BATCHSIZE, iData.Stat.MAX_ELEMENTSIZE, iData.BatchIdx, iData.FeatureIdx, iData.FeatureValue, null, behavior.Device),
                    new MatrixData(iLookup.Dim, iLookup.VocabSize, iLookup.Embedding, iLookup.EmbeddingGrad, behavior.Device), behavior);
                LinkRunners.Add(vRunner);

                MatrixInnerProductRunner simRunner = new MatrixInnerProductRunner(uRunner.Output, vRunner.Output, match, behavior);
                LinkRunners.Add(simRunner);

                SparseMultiplicationRunner biasRunner = new SparseMultiplicationRunner(
                    new SparseMatrixData(ID.Stat.MAX_FEATUREDIM, ID.Stat.MAX_BATCHSIZE, ID.Stat.MAX_BATCHSIZE, ID.BatchIdx, ID.FeatureIdx, ID.FeatureValue, null, behavior.Device),
                    new MatrixData(1, iLookup.VocabSize, iLookup.Bias, iLookup.BiasGrad, behavior.Device), behavior);
                LinkRunners.Add(biasRunner);

                AdditionRunner sRunner = new AdditionRunner(new HiddenBatchData(ID.Stat.MAX_BATCHSIZE, 1, simRunner.Output.Output, simRunner.Output.Deriv, behavior.Device),
                                                new HiddenBatchData(ID.Stat.MAX_BATCHSIZE, 1, biasRunner.Output.Output, biasRunner.Output.Deriv, behavior.Device), behavior);
                LinkRunners.Add(sRunner);

                Output = sRunner.Output;
            }

            public override void Backward(bool cleanDeriv)
            {
                base.Backward(cleanDeriv);
                ILookup.Bias.SyncToCPU();
            }
        }

        class GanRunner : CompositeNetRunner
        {
            GeneralBatchInputData UD;

            EmbedStructure ULookup;
            EmbedStructure ILookup;
            public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

            public GanRunner(GeneralBatchInputData uData, 
                EmbedStructure uLookup, EmbedStructure iLookup, float gamma, RunnerBehavior behavior) : base(behavior)
            {
                UD = uData;

                ULookup = uLookup;
                ILookup = iLookup;

                SparseMultiplicationRunner uRunner = new SparseMultiplicationRunner(
                    new SparseMatrixData(uData.Stat.MAX_FEATUREDIM, uData.Stat.MAX_BATCHSIZE, uData.Stat.MAX_ELEMENTSIZE, uData.BatchIdx, uData.FeatureIdx, uData.FeatureValue, null, behavior.Device),
                    new MatrixData(uLookup.Dim, uLookup.VocabSize, uLookup.Embedding, uLookup.EmbeddingGrad, behavior.Device), behavior);
                LinkRunners.Add(uRunner);

                MatrixProductRunner vRunner = new MatrixProductRunner(uRunner.Output,
                    new MatrixData(iLookup.Dim, iLookup.VocabSize, iLookup.Embedding, iLookup.EmbeddingGrad, behavior.Device), behavior);
                LinkRunners.Add(vRunner);

                LinkRunners.Add(new BiasAdditionProcessor(vRunner.Output, new VectorData(iLookup.VocabSize, iLookup.Bias, iLookup.BiasGrad, behavior.Device), behavior));
                LinkRunners.Add(new PositiveMaskProcessor(vRunner.Output, uData, behavior));
                
                ScaleRunner<MatrixData> vScaleRunner = new ScaleRunner<MatrixData>(vRunner.Output, gamma, behavior);
                LinkRunners.Add(vScaleRunner);
                //MatrixData vScaleData = vScaleRunner.Output;
                
                LinkRunners.Add(new MatrixSoftmaxProcessor(vScaleRunner.Output, 1, behavior));

                Output = vRunner.Output;
            }

            public override void Backward(bool cleanDeriv)
            {
                base.Backward(cleanDeriv);
                ILookup.Bias.SyncToCPU();
            }
        }


        class GanRewardRunner : ObjectiveRunner
        {
            new HiddenBatchData Input { get; set; }
            BiMatchBatchData Match { get; set; }
            float[] Reward { get; set; }

            public GanRewardRunner(HiddenBatchData input, float[] reward, BiMatchBatchData match, RunnerBehavior behavior)
                : base(Structure.Empty, behavior)
            {
                Input = input;
                Match = match;
                Reward = reward;
            }

            public override void Forward()
            {
                Input.Output.SyncToCPU();
                ObjectiveScore = 0;
                int ridx = 0;
                for (int i = 0; i < Match.SrcSize; i++)
                {
                    int sidx = i == 0 ? 0 : Match.Src2MatchIdx.MemPtr[i - 1];
                    int eidx = Match.Src2MatchIdx.MemPtr[i];

                    int pidx = Match.Src2MatchElement.MemPtr[sidx];
                    float posScore = Input.Output.Data.MemPtr[pidx];

                    float[] r = new float[eidx - sidx - 1];
                    for (int e = sidx + 1; e < eidx; e++)
                    {
                        int nidx = Match.TgtIdx.MemPtr[Match.Src2MatchElement.MemPtr[e]];
                        float negScore = Input.Output.Data.MemPtr[nidx];

                        r[e - sidx - 1] = Util.Logistic(negScore - posScore);

                    }
                    float b = r.Sum() / (eidx - sidx - 1);
                    ObjectiveScore += b;

                    for (int e = sidx + 1; e < eidx; e++)
                    {
                        // / (b + 0.000001f) - 1)
                        Reward[ridx] = (r[e - sidx - 1] - b) / Match.SrcSize / (eidx - sidx - 1);
                        ridx += 1;
                    }
                }
                ObjectiveScore = ObjectiveScore / Match.SrcSize;
            }
        }

        class DisRewardRunner : ObjectiveRunner
        {
            new HiddenBatchData Input { get; set; }
            BiMatchBatchData Match { get; set; }

            public DisRewardRunner(HiddenBatchData input, BiMatchBatchData match, RunnerBehavior behavior)
                : base(Structure.Empty, behavior)
            {
                Input = input;
                Match = match;
            }

            public override void Forward()
            {
                Input.Output.SyncToCPU();
                Input.Deriv.SyncToCPU();
                ObjectiveScore = 0;
                for (int i = 0; i < Match.SrcSize; i++)
                {
                    int sidx = i == 0 ? 0 : Match.Src2MatchIdx.MemPtr[i - 1];
                    int eidx = Match.Src2MatchIdx.MemPtr[i];

                    int pidx = Match.Src2MatchElement.MemPtr[sidx];
                    float posScore = Input.Output.Data.MemPtr[pidx];

                    float[] r = new float[eidx - sidx - 1];
                    for (int e = sidx + 1; e < eidx; e++)
                    {
                        int nidx = Match.Src2MatchElement.MemPtr[e];
                        float negScore = Input.Output.Data.MemPtr[nidx];

                        r[e - sidx - 1] = Util.Logistic(posScore - negScore);

                    }
                    float b = r.Sum() / (eidx - sidx - 1);
                    ObjectiveScore += b;
                    
                    for (int e = sidx + 1; e < eidx; e++)
                    {
                        Input.Deriv.Data.MemPtr[pidx] += (1 - r[e - sidx - 1]) / Match.SrcSize / (eidx - sidx - 1);
                        int nidx = Match.Src2MatchElement.MemPtr[e];
                        Input.Deriv.Data.MemPtr[nidx] += (r[e - sidx - 1] - 1) / Match.SrcSize / (eidx - sidx - 1);
                    }
                }
                Input.Deriv.SyncFromCPU();
                ObjectiveScore = ObjectiveScore / Match.SrcSize;
            }
        }

        class EvalRunner : ObjectiveRunner
        {
            float p3 = 0;
            float p5 = 0;
            float p10 = 0;
            float ndcg_3 = 0;
            float ndcg_5 = 0;
            float ndcg_10 = 0;
            int P = 0;

            GeneralBatchInputData U { get; set; }
            MatrixData Prob { get; set; }
            public EvalRunner(GeneralBatchInputData u, MatrixData prob, RunnerBehavior rb) : base(Structure.Empty, rb)
            {
                U = u;
                Prob = prob;

            }

            public override void Init()
            {
                P = 0;
                p3 = 0;
                p5 = 0;
                p10 = 0;
                ndcg_3 = 0;
                ndcg_5 = 0;
                ndcg_10 = 0;
            }

            public override void Forward()
            {
                U.FeatureIdx.SyncToCPU();
                Prob.Output.SyncToCPU();
                P += U.BatchSize;

                for (int u = 0; u < U.BatchSize; u++)
                {
                    int uid = U.FeatureIdx.MemPtr[u];
                    MinMaxHeap<int> heap = new MinMaxHeap<int>(10, 1);

                    for (int m = 0; m < Prob.Column; m++)
                    {
                        heap.push_pair(m, Prob.Output.MemPtr[u * Prob.Column + m]);
                    }

                    float[] r = new float[10];

                    for (int c = 0; c < 10; c++)
                    {
                        int ridx = heap.PopTop().Key;
                        if (DataPanel.user_pos_test[uid].Contains(ridx)) r[10 - c - 1] = 1;
                        else r[10 - c - 1] = 0;
                    }

                    p3 += r.Take(3).Average();
                    p5 += r.Take(5).Average();
                    p10 += r.Take(10).Average();

                    ndcg_3 += DataPanel.ndcg_at_k(r, 3, DataPanel.user_pos_test[uid].Count);
                    ndcg_5 += DataPanel.ndcg_at_k(r, 5, DataPanel.user_pos_test[uid].Count);
                    ndcg_10 += DataPanel.ndcg_at_k(r, 10, DataPanel.user_pos_test[uid].Count);

                }
            }

            public override void Complete()
            {
                Logger.WriteLog("P3: {0}, p5 : {1}, p10 : {2}, ndcg3 : {3}, ndcg5 : {4}, ndcg10: {5} ",
                    (p3 / P), p5 / P, p10 / P, ndcg_3 / P, ndcg_5 / P, ndcg_10 / P);

            }

        }

        public static ComputationGraph TrainDisGraph(
                    IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> u_data,
                    IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> i_data,
                    int K,
                    float Gamma,
                    EmbedStructure uLookup,
                    EmbedStructure iLookup,
                    EmbedStructure uLookupDis,
                    EmbedStructure iLookupDis,

                    RunnerBehavior rb)
        {
            ComputationGraph cg = new ComputationGraph();

            GeneralBatchInputData user = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(u_data, rb));
            GeneralBatchInputData item = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(i_data, rb));

            GanRunner gan = new GanRunner(user, uLookup, iLookup, Gamma, rb) { IsUpdate = false, IsBackProp = false };
            cg.AddRunner(gan);

            GenerateNegRunner negRunner = new GenerateNegRunner(gan.Output, item, K, rb) { IsUpdate = false, IsBackProp = false };
            cg.AddRunner(negRunner);

            DisRunner sRunner = new DisRunner(user, negRunner.Output, negRunner.MatchInput, uLookupDis, iLookupDis, rb);
            cg.AddRunner(sRunner);

            cg.AddObjective(new DisRewardRunner(sRunner.Output, negRunner.MatchInput, rb));

            return cg;
        }

        public static ComputationGraph PredGraph(
                        IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> u_data,
                        EmbedStructure uLookup,
                        EmbedStructure iLookup,
                        RunnerBehavior rb)
        {
            ComputationGraph cg = new ComputationGraph();

            GeneralBatchInputData user = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(u_data, rb));

            GanRunner gan = new GanRunner(user, uLookup, iLookup, 1, rb) { IsUpdate = false, IsBackProp = false };
            cg.AddRunner(gan);

            cg.AddRunner(new EvalRunner(user, gan.Output, rb));
            return cg;

        }

        public static ComputationGraph TrainGanGraph(
                    IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> u_data,
                    IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> i_data,
                    int K,
                    float Gamma,
                    EmbedStructure uLookup,
                    EmbedStructure iLookup,
                    EmbedStructure uLookupDis,
                    EmbedStructure iLookupDis,
                    RunnerBehavior rb)
        {
            ComputationGraph cg = new ComputationGraph();

            GeneralBatchInputData user = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(u_data, rb));
            GeneralBatchInputData item = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(i_data, rb));

            GanRunner gan = new GanRunner(user, uLookup, iLookup, Gamma, rb);
            cg.AddRunner(gan);

            GenerateNegRunner negRunner = new GenerateNegRunner(gan.Output, item, K, rb);
            cg.AddRunner(negRunner);

            DisRunner sRunner = new DisRunner(user, negRunner.Output, negRunner.MatchInput, uLookupDis, iLookupDis, rb) { IsBackProp = false, IsUpdate = false };
            cg.AddRunner(sRunner);

            cg.AddObjective(new GanRewardRunner(sRunner.Output, negRunner.Reward, negRunner.MatchInput, rb));            
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");


            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            CompositeNNStructure DeepNet = new CompositeNNStructure();

            EmbedStructure UserEmbedGen = new EmbedStructure(USER_NUM, EMB_DIM, device);
            EmbedStructure ItemEmbedGen = new EmbedStructure(ITEM_NUM, EMB_DIM, device);
            UserEmbedGen.Init(-INIT_DELTA, INIT_DELTA);
            ItemEmbedGen.IsBias = true;
            ItemEmbedGen.Init(-INIT_DELTA, INIT_DELTA);

            EmbedStructure UserEmbedDis = new EmbedStructure(USER_NUM, EMB_DIM, device);
            EmbedStructure ItemEmbedDis = new EmbedStructure(ITEM_NUM, EMB_DIM, device);
            UserEmbedDis.Init(-INIT_DELTA, INIT_DELTA);
            ItemEmbedDis.IsBias = true;
            ItemEmbedDis.Init(-INIT_DELTA, INIT_DELTA);


            DeepNet.AddLayer(UserEmbedGen);
            DeepNet.AddLayer(ItemEmbedGen);
            DeepNet.AddLayer(UserEmbedDis);
            DeepNet.AddLayer(ItemEmbedDis);

            OptimizerParameters.StructureOptimizer.ScheduleLR.Add(new Tuple<int, float>(0, 1));
            DeepNet.InitOptimizer(OptimizerParameters.StructureOptimizer, new RunnerBehavior() { Device = device, Computelib = computeLib, RunMode = DNNRunMode.Train });

            ComputationGraph ganTrainCG = TrainGanGraph(DataPanel.User_Train, DataPanel.Item_Train, 10, 0.5f, UserEmbedGen, ItemEmbedGen, UserEmbedDis, ItemEmbedDis,
                new RunnerBehavior() { Device = device, Computelib = computeLib, RunMode = DNNRunMode.Train });
            ganTrainCG.SetDelegateModel(DeepNet);

            ComputationGraph disTrainCG = TrainDisGraph(DataPanel.User_Train, DataPanel.Item_Train, 10, 0.1f, UserEmbedGen, ItemEmbedGen, UserEmbedDis, ItemEmbedDis,
                new RunnerBehavior() { Device = device, Computelib = computeLib, RunMode = DNNRunMode.Train });
            disTrainCG.SetDelegateModel(DeepNet);

            ComputationGraph predgan = PredGraph(DataPanel.User_Test, UserEmbedGen, ItemEmbedGen, new RunnerBehavior() { Device = device, Computelib = computeLib, RunMode = DNNRunMode.Predict });
            ComputationGraph preddis = PredGraph(DataPanel.User_Test, UserEmbedDis, ItemEmbedDis, new RunnerBehavior() { Device = device, Computelib = computeLib, RunMode = DNNRunMode.Predict });

            for (int m = 0; m < 50; m++)
            {
                double rewardDis = disTrainCG.Execute();
                Logger.WriteLog("Dis avg reward {0}", rewardDis);
                preddis.Execute();
            }


            //UserEmbedGen.Embedding.CopyFrom(UserEmbedDis.Embedding);
            //ItemEmbedGen.Embedding.CopyFrom(ItemEmbedDis.Embedding);
            //ItemEmbedGen.Bias.CopyFrom(ItemEmbedDis.Bias);
            //predgan.Execute();
            OptimizerParameters.StructureOptimizer.ScheduleLR[0] = new Tuple<int, float>(0, 0.05f);

            for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
            {
               
                for (int m = 0; m < 5; m++)
                {
                    double rewardGan = ganTrainCG.Execute();
                    Logger.WriteLog("Gan avg reward {0}", rewardGan);
                    predgan.Execute();
                }

                for (int m = 0; m < 5; m++)
                {
                    double rewardDis = disTrainCG.Execute();
                    Logger.WriteLog("Dis avg reward {0}", rewardDis);
                    preddis.Execute();
                }



                //for (int m = 0; m < 10; m++)
                //{
                //    double rewardGan = ganTrainCG.Execute();
                //    Logger.WriteLog("Gan avg reward {0}", rewardGan);
                //}

            }

            Logger.CloseLog();
        }

        public class DataPanel
        {
            public static Dictionary<int, HashSet<int>> user_pos_train = new Dictionary<int, HashSet<int>>();
            public static Dictionary<int, HashSet<int>> user_pos_test = new Dictionary<int, HashSet<int>>();

            static List<int> all_users = new List<int>();

            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> User_Train = null;

            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Item_Train = null;

            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> User_Test = null;

            public static float dcg_at_k(float[] r, int k)
            {
                float s = 0;
                for (int i = 1; i <= k; i++)
                {
                    s += r[i - 1] / (float)Math.Log(i + 1, 2);
                }
                return s;
            }

            public static float ndcg_at_k(float[] r, int k, int mK)
            {                
                float dcg_max = 0;
                for (int i = 1; i <= Math.Min(k, mK); i++)
                {
                    dcg_max += 1.0f / (float)Math.Log(i + 1, 2);
                }

                if (dcg_max == 0) return 0;

                float dcg = dcg_at_k(r, k);
                return dcg / dcg_max;
            }

            static void LoadTrain()
            {
                List<Tuple<int, int>> Pair = new List<Tuple<int, int>>();
                using (StreamReader reader = new StreamReader(workdir + "movielens-100k-train.txt"))
                {
                    while (!reader.EndOfStream)
                    {
                        string[] items = reader.ReadLine().Split('\t');
                        int uid = int.Parse(items[0]);
                        int iid = int.Parse(items[1]);
                        float r = float.Parse(items[2]);
                        if (r > 3.99)
                        {
                            if (!user_pos_train.ContainsKey(uid))
                                user_pos_train[uid] = new HashSet<int>();
                            user_pos_train[uid].Add(iid);

                            Pair.Add(new Tuple<int, int>(uid, iid));
                        }
                    }
                }


                BinaryWriter uWriter = new BinaryWriter(new FileStream(workdir + "user.bin", FileMode.Create, FileAccess.Write));
                GeneralBatchInputData u = new GeneralBatchInputData(new GeneralBatchInputDataStat()
                {
                    MAX_FEATUREDIM = USER_NUM,
                    MAX_BATCHSIZE = BATCH_SIZE,
                    FeatureType = FeatureDataType.SparseFeature
                }, DeviceType.CPU);


                BinaryWriter iWriter = new BinaryWriter(new FileStream(workdir + "item.bin", FileMode.Create, FileAccess.Write));
                GeneralBatchInputData i = new GeneralBatchInputData(new GeneralBatchInputDataStat()
                {
                    MAX_FEATUREDIM = ITEM_NUM,
                    MAX_BATCHSIZE = BATCH_SIZE,
                    FeatureType = FeatureDataType.SparseFeature
                }, DeviceType.CPU);


                foreach (Tuple<int,int> kv in CommonExtractor.RandomShuffle(Pair, 100000, 10))
                {
                    Dictionary<int, float> ufea = new Dictionary<int, float>();
                    Dictionary<int, float> ifea = new Dictionary<int, float>();

                    ufea.Add(kv.Item1, 1);
                    ifea.Add(kv.Item2, 1);

                    u.PushSample(ufea, 0);
                    i.PushSample(ifea, 0);

                    if (u.BatchSize >= BATCH_SIZE)
                    {
                        u.PopBatchToStat(uWriter);
                        i.PopBatchToStat(iWriter);
                    }
                }

                u.PopBatchCompleteStat(uWriter);
                i.PopBatchCompleteStat(iWriter);
                Console.WriteLine("User Stat {0}", u.Stat.ToString());
                Console.WriteLine("Item Stat {0}", i.Stat.ToString());
            }

            static void LoadTest()
            {
                using (StreamReader reader = new StreamReader(workdir + "movielens-100k-test.txt"))
                {
                    while (!reader.EndOfStream)
                    {
                        string[] items = reader.ReadLine().Split('\t');
                        int uid = int.Parse(items[0]);
                        int iid = int.Parse(items[1]);
                        float r = float.Parse(items[2]);
                        if (r > 3.99)
                        {
                            if (!user_pos_test.ContainsKey(uid))
                                user_pos_test[uid] = new HashSet<int>();
                            user_pos_test[uid].Add(iid);
                        }
                    }
                }

                BinaryWriter uWriter = new BinaryWriter(new FileStream(workdir + "user.test.bin", FileMode.Create, FileAccess.Write));
                GeneralBatchInputData u = new GeneralBatchInputData(new GeneralBatchInputDataStat()
                {
                    MAX_FEATUREDIM = USER_NUM,
                    MAX_BATCHSIZE = BATCH_SIZE,
                    FeatureType = FeatureDataType.SparseFeature
                }, DeviceType.CPU);

                foreach (int uid in user_pos_test.Keys)
                {
                    Dictionary<int, float> ufea = new Dictionary<int, float>();
                    ufea.Add(uid, 1);
                    u.PushSample(ufea, 0);

                    if (u.BatchSize >= BATCH_SIZE) { u.PopBatchToStat(uWriter); }
                }

                u.PopBatchCompleteStat(uWriter);
                Console.WriteLine("User Stat {0}", u.Stat.ToString());
            }
            public static void Init()
            {
                LoadTrain();
                LoadTest();
                all_users = user_pos_train.Keys.ToList();


                User_Train = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(workdir + "user.bin");
                User_Train.InitThreadSafePipelineCashier(128, ParameterSetting.RANDOM_SEED);


                Item_Train = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(workdir + "item.bin");
                Item_Train.InitThreadSafePipelineCashier(128, ParameterSetting.RANDOM_SEED);

                User_Test = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(workdir + "user.test.bin");
                User_Test.InitThreadSafePipelineCashier(10, false);

            }
        }
    }
}
