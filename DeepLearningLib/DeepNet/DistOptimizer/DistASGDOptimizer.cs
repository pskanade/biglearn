﻿//using CommLib;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BigLearn;

namespace BigLearn.DeepNet
{
    public class DistASGDOptimizer : GradientOptimizer
    {

        public DistASGDOptimizer(CudaPieceFloat weight, float learnRate, RunnerBehavior behavior, IGradientAggregatorFactory aggFactory = null) : base(behavior)
        {
            //Init(weight, new StructureLearner() { LearnRate = learnRate, device = behavior.Device, GradientAggFactory = aggFactory });
            SetupConfig(weight, new CudaPieceFloat(weight.Size, behavior.Device), new StructureLearner() { LearnRate = learnRate, device = behavior.Device, GradientAggFactory = aggFactory });
        }
        
        //public override void Init(CudaPieceFloat weight, StructureLearner learner)
        //{
        //    Aggregator = learner.GradientAggFactory == null ? null : learner.GradientAggFactory.CreateAggregator(weight.Size);

        //    if (Aggregator == null)
        //    {
        //        Gradient = weight;
        //        GradientStep = learner.LearnRate;
        //    }
        //    else
        //    {
        //        GradientStep = 1;
        //        UpdateRate = learner.LearnRate;
        //        Parameter = weight;
        //        Gradient = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    }
        //}

        public override void BeforeGradient()
        {
        }

        public override void AfterGradient()
        {
        }
    }

    // public unsafe class DistASGDOptimizerFlow : OptimizerFlow
    // {
    //     public List<ModelOptimizer> variables;
    //     IGradientAggregator Aggregator;
    //     RunnerBehavior behavior;
    //     float*[] paramPtrs;
    //     float*[] gradPtrs;
    //     GCHandle[] paramHandles;
    //     GCHandle[] gradHandles;
    //     ulong* keys;
    //     ulong* lengths;
    //     float** vals;
    //     AutoResetEvent pushEvent;
    //     ParameterServerBase.PsPushCallback pushCallback;
    //     GCHandle cbHandle;
    //     public IMathOperationManager ComputeLib
    //     {
    //         get
    //         {
    //             if (behavior.Computelib == null)
    //             {
    //                 behavior.Computelib = MathOperatorManager.CreateInstance(behavior.Device);
    //             }
    //             return behavior.Computelib;
    //         }
    //     }

    //     public override void Init(Structure model, RunnerBehavior behavior, ComputationGraph cg, StructureLearner learner)
    //     {
    //         Model = model;
    //         CG = cg;
    //         Device = behavior.Device;
    //         this.behavior = behavior;
    //         this.variables = new List<ModelOptimizer>();
    //         Aggregator = learner.GradientAggFactory == null ? null : learner.GradientAggFactory.CreateAggregator(0);
    //         pushEvent = new AutoResetEvent(false);
    //         /// **************** Init Model-Weight Optimizer; ***************** ///
    //         foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
    //         {
    //             var sgdOpt = new DistASGDOptimizer(optimizer.Parameter, learner.LearnRate, behavior, learner.GradientAggFactory);
    //             optimizer.Optimizer = sgdOpt;
    //             this.variables.Add(optimizer);
    //         }

    //         this.variables = this.variables.OrderBy(v => v.Parameter.Size).ToList();
    //         SetupPS();
    //     }

    //     public override double Run()
    //     {
    //         double Loss = 0;
    //         int batchIdx = 0;
    //         CG.Init();
    //         while (CG.FetchData())
    //         {
    //             if (!CG.IsContinue) continue;
    //             //if (AsyncCommunication.GetMyRank() == 0)
    //             {
    //                 this.PullWeights();
    //             }

    //             Loss = batchIdx * 1.0f / (batchIdx + 1) * Loss + 1.0f / (batchIdx + 1) * CG.ForwardLoss();

    //             if (++batchIdx % 50 == 0)
    //             {
    //                 Logger.WriteLog("Train Batch Num {0}, AvgLoss {1}", batchIdx, Loss);
    //             }

    //             CG.BackwardGrad();

    //             //if (AsyncCommunication.GetMyRank() == 0)
    //             {
    //                 this.ApplyGrad();
    //             }
    //         }

    //         CG.Complete();
    //         return Loss;
    //     }

    //     public virtual void ApplyGrad()
    //     {
    //         throw new Exception("Comment the following lines here, need to design better framework!");
    //         //PerfCounter.Manager.Instance["PushPS"].CountOperation(() =>
    //         //{
    //         //    int keyCnt = 0;
    //         //    for (ulong i = 0; i < (ulong)this.variables.Count; i++)
    //         //    {
    //         //        int[] fids;
    //         //        int[] fidLen;
    //         //        if (this.variables[(int)i].GetActiveFids(out fids, out fidLen))
    //         //        {
    //         //            for (ulong k = 0; k < (ulong)fids.Length; k++)
    //         //            {
    //         //                ulong bk = (ulong)i;
    //         //                ulong pk = (ulong)fids[k];
    //         //                this.keys[keyCnt] = (bk << 32) | (pk / 300);
    //         //                this.lengths[keyCnt] = (ulong)fidLen[k]*4;
    //         //                this.vals[keyCnt] = &this.gradPtrs[i][pk];
    //         //                keyCnt++;
    //         //            }
    //         //        }
    //         //        else
    //         //        {
    //         //            this.keys[keyCnt] = i;
    //         //            this.lengths[keyCnt] = (ulong)this.variables[(int)i].Optimizer.Gradient.MemPtr.Length * 4;
    //         //            this.vals[keyCnt] = this.gradPtrs[i];
    //         //            keyCnt++;
    //         //        }
    //         //    }

    //         //    if (Aggregator != null)
    //         //    {
    //         //        ChaNaParameterClient.Instance.PushSync(keyCnt, this.keys, (void**)this.vals, this.lengths);
    //         //    }
    //         //});
    //     }

    //     private void PullWeights()
    //     {
    //         PerfCounter.Manager.Instance["PullPS"].CountOperation(() =>
    //         {
    //             int keyCnt = 0;
    //             for (ulong i = 0; i < (ulong)this.variables.Count; i++)
    //             {
                    
    //                 if (i >= 0) throw new Exception("Get ActiveFids is not allowed here.");
    //                 //int[] fids;
    //                 //int[] fidLen;

    //                 //if (this.variables[(int)i].GetActiveFids(out fids, out fidLen))
    //                 //{
    //                 //    for (ulong k = 0; k < (ulong)fids.Length; k++)
    //                 //    {
    //                 //        ulong bk = (ulong)i;
    //                 //        ulong pk = (ulong)fids[k];
    //                 //        this.keys[keyCnt] = (bk << 32) | (pk / 300);
    //                 //        this.lengths[keyCnt] = (ulong)fidLen[k] * 4;
    //                 //        this.vals[keyCnt] = &this.paramPtrs[i][pk];

    //                 //        //zero sparse gds
    //                 //        for (int j = 0; j < fidLen[k]; j++)
    //                 //        {
    //                 //            this.gradPtrs[i][(int)pk+j] = 0;
    //                 //        }

    //                 //        keyCnt++;
    //                 //    }
    //                 //}
    //                 //else
    //                 //{
    //                 //    this.keys[keyCnt] = i;
    //                 //    this.lengths[keyCnt] = (ulong)this.variables[(int)i].Optimizer.Parameter.MemPtr.Length * 4;
    //                 //    this.vals[keyCnt] = this.paramPtrs[i];
    //                 //    keyCnt++;

    //                 //    ComputeLib.Zero(this.variables[(int)i].Optimizer.Gradient, this.variables[(int)i].Optimizer.Gradient.Size);
    //                 //}
    //             }

    //             if (Aggregator != null)
    //             {
    //                 ChaNaParameterClient.Instance.PullSync(keyCnt, this.keys, (void**)(this.vals), this.lengths);
    //             }
    //         });
    //     }

    //     public void PushCallback(ulong cnt, ulong* keys, void** vals, ulong* lengths, void* args)
    //     {
    //         pushEvent.Set();
    //     }

    //     private void SetupPS()
    //     {
    //         CG.Init();

    //         paramPtrs = new float*[variables.Count];
    //         gradPtrs = new float*[variables.Count];
    //         paramHandles = new GCHandle[variables.Count];
    //         gradHandles = new GCHandle[variables.Count];
    //         int[] bufSizes = new int[this.variables.Count];
    //         bool[] sparseFlags = new bool[this.variables.Count];
    //         float** bufsPtr = (float**)Marshal.AllocHGlobal(sizeof(float*) * this.variables.Count);
    //         int elements = 0;
    //         for (int i = 0; i < variables.Count; i++)
    //         {
    //             Console.WriteLine("Key: {0}:{1}", i, variables[i].Parameter.Size);
    //             if(i >= 0) throw new Exception("Model is not allowed to use Runner");
    //             //if (variables[i].Model.Runner.Input is SeqSparseBatchData)
    //             //{
    //             //    sparseFlags[i] = true;
    //             //    elements += variables[i].Parameter.MemPtr.Length;
    //             //}
    //             //else
    //             //{
    //             //    sparseFlags[i] = false;
    //             //    elements++;
    //             //}               

    //             //bufSizes[i] = variables[i].Parameter.MemPtr.Length * sizeof(float);
    //             //bufsPtr[i] = (float*)Marshal.AllocHGlobal(bufSizes[i]);
    //             //Marshal.Copy(variables[i].Parameter.MemPtr, 0, new IntPtr(bufsPtr[i]), bufSizes[i] >> 2);
    //             //gradHandles[i] = GCHandle.Alloc(variables[i].Optimizer.Gradient.MemPtr, GCHandleType.Pinned);
    //             //paramHandles[i] = GCHandle.Alloc(variables[i].Optimizer.Parameter.MemPtr, GCHandleType.Pinned);
    //             //paramPtrs[i] = (float*)paramHandles[i].AddrOfPinnedObject().ToPointer();
    //             //gradPtrs[i] = (float*)gradHandles[i].AddrOfPinnedObject().ToPointer();
    //         }

    //         keys = (ulong*)Marshal.AllocHGlobal(elements * sizeof(ulong));
    //         lengths = (ulong*)Marshal.AllocHGlobal(elements * sizeof(ulong));
    //         vals = (float**)Marshal.AllocHGlobal(elements * sizeof(float*));

    //         pushCallback = new ParameterServerBase.PsPushCallback(PushCallback);
    //         cbHandle = GCHandle.Alloc(pushCallback);
    //         ChaNaParameterServer.Instance.Init(bufSizes, bufsPtr, sparseFlags);
    //         AsyncCommunication.Barrier();
    //     }
    // }
}
