﻿//using CommLib;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;
using BigLearn.Common;
namespace BigLearn.DeepNet
{
    // public unsafe class BlockAdamOptimizerFlow : OptimizerFlow
    // {
    //     System.Action DoBM;
    //     private List<ModelOptimizer> localOpts;
    //     private IGradientAggregator Aggregator;
    //     private RunnerBehavior behavior;
    //     private DNNOptimizerType localOptimizer;
    //     private float[] globalWeight;
    //     private float[] globalMomemtum;
    //     private float[] globalV;

    //     private float beta2 = 0.99f;
    //     private float beta1 = 0.8f;
    //     private float alpha = 1.4f;
    //     private double gama = 1e-4;
    //     private int blockSize = 1000;
    //     private float localLearningRate = 0.4f;
    //     private bool resetLocalOpt = true;
    //     int iter = 0;

    //     public IMathOperationManager ComputeLib
    //     {
    //         get
    //         {
    //             if (behavior.Computelib == null)
    //             {
    //                 behavior.Computelib = MathOperatorManager.CreateInstance(behavior.Device);
    //             }

    //             return behavior.Computelib;
    //         }
    //     }

    //     public override double Run()
    //     {
    //         double Loss = 0;
    //         double batchLoss = 0;
    //         int batchIdx = 0;
    //         CG.Init();
    //         int blockCnt = 0;
    //         while (CG.FetchData())
    //         {
    //             if (!CG.IsContinue) continue;
    //             batchLoss = CG.ForwardLoss();
    //             Loss += batchLoss;

    //             if (++batchIdx % 100 == 0)
    //             {
    //                 Logger.WriteLog("Train Batch Num {0}, AvgLoss {1}, BatchLoss: {2}", batchIdx, Loss / batchIdx, batchLoss);
    //             }

    //             CG.BackwardGrad();

    //             blockCnt++;
    //             if (blockCnt % blockSize == 0)
    //             {
    //                 DoBM();
    //                 blockCnt = 0;
    //             }
    //         }

    //         if (blockCnt > 0)
    //         {
    //             DoBM();
    //         }

    //         CG.Complete();
    //         return Loss / batchIdx;
    //     }

    //     public override void Init(Structure model, RunnerBehavior behavior, ComputationGraph cg, StructureLearner learner)
    //     {
    //         Model = model;
    //         CG = cg;
    //         Device = behavior.Device;
    //         this.behavior = behavior;
    //         this.localOpts = new List<ModelOptimizer>();
    //         Aggregator = learner.GradientAggFactory == null ? null : learner.GradientAggFactory.CreateAggregator(0);
    //         this.localLearningRate = learner.LearnRate;
    //         /// **************** Init Model-Weight Optimizer; ***************** ///
    //         foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
    //         {
    //             this.localOpts.Add(optimizer);
    //         }

    //         int elements = this.localOpts.Sum(v => v.Parameter.EffectiveSize);

    //         this.globalWeight = new float[elements];
    //         this.globalMomemtum = new float[elements];
    //         this.globalV = new float[elements];

    //         this.SetArgs(learner.OptArgs);
    //         CG.Init();

    //         int offset = 0;
    //         StructureLearner subLearner = new StructureLearner
    //         {
    //             LearnRate = this.localLearningRate,
    //             Optimizer = this.localOptimizer,
    //             device = this.behavior.Device
    //         };

    //         for (int i = 0; i < this.localOpts.Count; i++)
    //         {
    //             this.localOpts[i].Optimizer = GradientOptimizer.CreateLocalOptimizer(this.localOpts[i].Parameter, subLearner, this.behavior);

    //             Array.Copy(this.localOpts[i].Parameter.MemPtr, 0, this.globalWeight, offset, this.localOpts[i].Parameter.EffectiveSize);
    //             offset += this.localOpts[i].Parameter.EffectiveSize;
    //         }

    //         this.DoBM = BlockAdam;
    //         Console.WriteLine("Set BM with Adam, ResetLocalOpt: {0}.", this.resetLocalOpt);
    //     }

    //     private void SetArgs(string args)
    //     {
    //         args = args ?? string.Empty;
    //         string[] argPairs = args.Split(',');
    //         NamedArgsParser parser = new NamedArgsParser(argPairs);
    //         this.beta1 = parser.Get("BETA1", 0.9f);
    //         this.beta2 = parser.Get("BETA2", 0.999f);

    //         this.alpha = parser.Get("ALPHA", 0.02f);
    //         this.gama = parser.Get("GAMA", 1e-6);
    //         this.blockSize = parser.Get("BLOCK", 100);
    //         string localOptStr = parser.Get("LOCAL-OPTIMIZER", DNNOptimizerType.ClipSGD.ToString());
    //         this.localOptimizer = (DNNOptimizerType)Enum.Parse(typeof(DNNOptimizerType), localOptStr);
    //         this.resetLocalOpt = parser.Get("RESET-LOCAL", true);
    //     }

    //     private void BlockAdam()
    //     {
    //         iter++;

    //         var perfC = PerfCounter.Manager.Instance["AllReduce"].Begin();
    //         int workers = AsyncCommunication.GetMachineCount();
    //         float avg = 1.0f / workers;
    //         float updateRate = (float)(alpha * Math.Sqrt(1 - Math.Pow(beta2, iter)) / (1 - Math.Pow(beta1, iter)));
    //         int[] chunks = new int[this.localOpts.Count];
    //         for (int i = 1; i < chunks.Length; i++)
    //         {
    //             chunks[i] = chunks[i - 1] + this.localOpts[i - 1].Parameter.EffectiveSize;
    //         }

    //         for (int i = 0; i < chunks.Length; i++)
    //         {
    //             float[] param = this.localOpts[i].Parameter.MemPtr;
    //             int off = chunks[i];
    //             Parallel.For(0, this.localOpts[i].Parameter.EffectiveSize, j =>
    //                {
    //                    float delta = param[j] - this.globalWeight[off + j];
    //                    param[j] = this.globalWeight[off + j];
    //                    this.globalWeight[off + j] = delta;
    //                });
    //         };

    //         DistComputationUtils.Sum(this.globalWeight, globalWeight.Length, 120 * 1000);

    //         PerfCounter.Manager.Instance["BlockMomentum"].CountOperation(() =>
    //         {
    //             for (int i = 0; i < chunks.Length; i++)
    //             {
    //                 float[] subGlobalWeights = this.localOpts[i].Parameter.MemPtr;
    //                 int off = chunks[i];

    //                 Parallel.For(0, this.localOpts[i].Parameter.EffectiveSize, j =>
    //                  {
    //                      float gradient = this.globalWeight[off + j];
    //                      this.globalMomemtum[off + j] = beta1 * this.globalMomemtum[off + j] + (1 - beta1) * (gradient);
    //                      this.globalV[off + j] = beta2 * this.globalV[off + j] + (1 - beta2) * (gradient) * (gradient);
    //                      this.globalWeight[off + j] = subGlobalWeights[j] +
    //                          (float)(updateRate * this.globalMomemtum[off + j] / Math.Sqrt(this.globalV[off + j] + 1e-6))
    //                          - (float)(gama * subGlobalWeights[j]);

    //                      if (float.IsNaN(this.globalWeight[off + j]))
    //                      {
    //                          Console.WriteLine("Invlaide weight. Nan, {0}, {1}, {2}, {3}", this.globalMomemtum[off + j], this.globalV[off + j], gradient, updateRate);
    //                          throw new InvalidDataException();
    //                      }

    //                      subGlobalWeights[j] = this.globalWeight[off + j];
    //                  });

    //                 if (this.resetLocalOpt)
    //                 {
    //                     this.localOpts[i].Optimizer.Reset();
    //                 }
    //             }
    //         });

    //         PerfCounter.Manager.Instance["AllReduce"].TakeCount(perfC);
    //     }

    //     public override void Save(BinaryWriter writer)
    //     {
    //         writer.Write(iter);
    //         int size = this.globalMomemtum.Length * sizeof(float);
    //         writer.Write(size);
    //         byte[] tempBuf = new byte[this.globalMomemtum.Length * sizeof(float)];
    //         Buffer.BlockCopy(this.globalMomemtum, 0, tempBuf, 0, tempBuf.Length);
    //         writer.Write(tempBuf, 0, tempBuf.Length);
    //         Buffer.BlockCopy(this.globalV, 0, tempBuf, 0, tempBuf.Length);
    //         writer.Write(tempBuf, 0, tempBuf.Length);
    //     }

    //     public override void Load(BinaryReader reader)
    //     {
    //         this.iter = reader.ReadInt32();
    //         int size = reader.ReadInt32();
    //         byte[] tempBuf = new byte[size];
    //         reader.Read(tempBuf, 0, size);
    //         Buffer.BlockCopy(tempBuf, 0, this.globalMomemtum, 0, tempBuf.Length);
    //         reader.Read(tempBuf, 0, size);
    //         Buffer.BlockCopy(tempBuf, 0, this.globalV, 0, tempBuf.Length);
    //     }
    // }
}
