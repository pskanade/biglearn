﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using CommLib;
//using ScopeML;
using BigLearn;
using BigLearn.Common;
namespace BigLearn.DeepNet
{
    // public unsafe class DistMAOptimizerFlow : OptimizerFlow
    // {
    //     public List<ModelOptimizer> variables;
    //     IGradientAggregator Aggregator;
    //     RunnerBehavior behavior;
    //     int blockSize = 1000;

    //     public IMathOperationManager ComputeLib
    //     {
    //         get
    //         {
    //             if (behavior.Computelib == null)
    //             {
    //                 behavior.Computelib = MathOperatorManager.CreateInstance(behavior.Device);
    //             }
    //             return behavior.Computelib;
    //         }
    //     }

    //     public override void Init(Structure model, RunnerBehavior behavior, ComputationGraph cg, StructureLearner learner)
    //     {
    //         Model = model;
    //         CG = cg;
    //         Device = behavior.Device;
    //         this.behavior = behavior;
    //         this.variables = new List<ModelOptimizer>();
    //         Aggregator = learner.GradientAggFactory == null ? null : learner.GradientAggFactory.CreateAggregator(0);
    //         /// **************** Init Model-Weight Optimizer; ***************** ///
    //         int elements = 0;
    //         foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
    //         {

    //             var sgdOpt = GradientOptimizer.CreateLocalOptimizer(optimizer.Parameter, learner, behavior);
    //             //new SGDOptimizer(optimizer.Parameter, learner.LearnRate, behavior, null);

    //             optimizer.Optimizer = sgdOpt;
    //             this.variables.Add(optimizer);
    //             elements += optimizer.Parameter.EffectiveSize;
    //         }

    //         this.variables = this.variables.OrderBy(v => v.Parameter.Size).ToList();
    //         /*
    //         this.globalWeight = new float[elements];
    //         int offset = 0;
    //         for (int i = 0; i < this.variables.Count; i++)
    //         {
    //             Array.Copy(this.variables[i].Parameter.MemPtr, 0, this.globalWeight, offset, this.variables[i].Parameter.EffectiveSize);
    //         }*/
    //         SetupPS();
    //     }

    //     public override double Run()
    //     {
    //         double Loss = 0;
    //         int batchIdx = 0;
    //         CG.Init();
    //         int blockCnt = 0;
    //         while (CG.FetchData())
    //         {
    //             if (!CG.IsContinue) continue;

    //             Loss = batchIdx * 1.0f / (batchIdx + 1) * Loss + 1.0f / (batchIdx + 1) * CG.ForwardLoss();

    //             if (++batchIdx % 100 == 0)
    //             {
    //                 Logger.WriteLog("Train Batch Num {0}, AvgLoss {1}", batchIdx, Loss);
    //             }

    //             CG.BackwardGrad();

    //             blockCnt++;
    //             if (blockCnt % blockSize == 0)
    //             {
    //                 DoModelAverage();
    //                 blockCnt = 0;
    //             }
    //         }

    //         if (blockCnt > 0)
    //         {
    //             DoModelAverage();
    //         }
    //         CG.Complete();
    //         return Loss;
    //     }

    //     private void DoModelAverage()
    //     {
    //         for (int i = 0; i < this.variables.Count; i++)
    //         {
    //             var cnt = PerfCounter.Manager.Instance["AllReduce"].Begin();
    //             DistComputationUtils.Sum(this.variables[i].Parameter, this.variables[i].Parameter.EffectiveSize, 60 * 1000);
    //             int n = AsyncCommunication.GetMachineCount();
    //             for (int j = 0; j < this.variables[i].Parameter.EffectiveSize; j++)
    //             {
    //                 this.variables[i].Parameter.MemPtr[j] /= n;
    //             }
    //             PerfCounter.Manager.Instance["AllReduce"].TakeCount(cnt);
    //         }
    //     }

    //     private void SetArgs(string args)
    //     {
    //         args = args ?? string.Empty;

    //         string[] argPairs = args.Split(',');
    //         NamedArgsParser parser = new NamedArgsParser(argPairs);
    //         this.blockSize = parser.Get("BLOCK", 1000);
    //     }

    //     private void SetupPS()
    //     {
    //         CG.Init();
    //     }
    // }
}
