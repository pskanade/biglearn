﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace BigLearn.DeepNet
//{
//    class SeqMatchSeqNLIBuilder : Builder
//    {
//        class BuilderParameters : BaseModelArgument<BuilderParameters>
//        {
//            static BuilderParameters()
//            {
//                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

//                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
//                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
//                Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));
//                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Word Dict."));

//                Argument.Add("MINI-BATCH", new ParameterArgument("64", "Mini Batch Size"));

//                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

//                Argument.Add("INIT-EMD", new ParameterArgument(string.Empty, "vocab embeddings."));
//                Argument.Add("EMD-DIM", new ParameterArgument("300", "vocab embeddings."));
//                Argument.Add("UNK-INIT", new ParameterArgument("0", "0:zero initialize; 1:random initialize."));
//                Argument.Add("LAYER-DIM", new ParameterArgument("1", "LSTM encoder Dim"));
//                Argument.Add("MATCH-DIM", new ParameterArgument("128", "match lstm dim"));

//                ///Softmax Randomup.
//                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
//                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
//                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));

//                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
//                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
//                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
//                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

//                Argument.Add("IS-SHARE-EMD", new ParameterArgument("1", "is share embedding vec."));

//            }

//            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
//            public static string Train { get { return Argument["TRAIN"].Value; } }
//            public static string Valid { get { return Argument["VALID"].Value; } }
//            public static string Test { get { return Argument["TEST"].Value; } }

//            public static string VOCAB { get { return Argument["VOCAB"].Value; } }

//            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }

//            public static string INIT_EMD { get { return (Argument["INIT-EMD"].Value); } }
//            public static int EMD_DIM { get { return int.Parse(Argument["EMD-DIM"].Value); } }
//            public static int UNK_INIT { get { return int.Parse(Argument["UNK-INIT"].Value); } }
//            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
//            /// <summary>
//            /// match lstm dim.
//            /// </summary>
//            public static int MATCH_DIM { get { return int.Parse(Argument["MATCH-DIM"].Value); } }

//            public static int Mini_Batch { get { return int.Parse(Argument["MINI-BATCH"].Value); } }




//            //public static Vocab2Freq mVocabDict = null;
//            //public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Tgt_Vocab); return mVocabDict; } }
//            //public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
//            //public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

//            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
//            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }

//            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
//            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
//            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
//            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
//            public static bool Is_Share_Emd { get { return int.Parse(Argument["IS-SHARE-EMD"].Value) > 0; } }
//        }

//        public override BuilderType Type { get { return BuilderType.APP_SEQ_MATCH_SEQ_NLI; } }

//        public override void InitStartup(string fileName, string[] additonalArg)
//        {
//            BuilderParameters.Parse(fileName);
//            BuilderParameters.Parse(additonalArg);
//        }

//        public override void InitStartup(string fileName)
//        {
//            BuilderParameters.Parse(fileName);
//        }

//        public class SeqMatchRunner : CompositeNetRunner
//        {
//            int MaxLength { get; set; }
//            SeqDenseBatchData Memory { get; set; }
//            new SeqDenseBatchData Input { get; set; }
//            public SeqMatchRunner(SeqDenseBatchData memory, SeqDenseBatchData input, int maxLength, RunnerBehavior behavior) : base(behavior)
//            {
//                MaxLength = maxLength;
//                Input = input;
//                Memory = memory;

//                for (int i = 0; i < MaxLength; i++)
//                {
//                    HiddenBatchData x = new SeqOrderMatrixRunner()
//                }
//            }
//        }

//        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> src,
//                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> tgt,
//                                                             IDataCashier<DenseBatchData, DenseDataStat> label,
//                                                             EpisodicLSTMModel model,
//                                                             RunnerBehavior Behavior)
//        {
//            ComputationGraph cg = new ComputationGraph();

//            SeqSparseBatchData srcData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(src, Behavior));
//            SeqSparseBatchData tgtData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(tgt, Behavior));
//            DenseBatchData labelData = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(label, Behavior));

//            SeqDenseBatchData srcEmd = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(model.EmbedLayer, srcData, Behavior)
//            { IsBackProp = false, IsUpdate = false });
//            SeqDenseBatchData tgtEmd = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(model.EmbedLayer, tgtData, Behavior)
//            { IsBackProp = false, IsUpdate = false });

//            SeqDenseBatchData srcConEmd = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(model.BiLSTMLayer.Item1, model.BiLSTMLayer.Item2, srcEmd, Behavior));
//            SeqDenseBatchData tgtConEmd = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(model.BiLSTMLayer.Item1, model.BiLSTMLayer.Item2, tgtEmd, Behavior));

//            FastLSTMDenseRunner<S>
//            // sample a list of tuplet from the graph.
//            SampleRunner SmpRunner = new SampleRunner(graph, batchSize, Behavior);
//            cg.AddDataRunner(SmpRunner);
//            GraphQueryData interface_data = SmpRunner.Output;

//            MemoryScheduleRunner memRunner = new MemoryScheduleRunner(gmemory, tmpMemory, Behavior);
//            cg.AddRunner(memRunner);
//            List<EpisodicMemoryV2> aMem = memRunner.Output;

//            // get the embedding from the query data.
//            StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, 0, Behavior);
//            cg.AddRunner(statusEmbedRunner);

//            List<GraphQueryData> Queries = new List<GraphQueryData>();

//            // MCTS path number.
//            for (int j = 0; j < (Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.MCTS_NUM : 1); j++)
//            {
//                GraphQueryData newQuery = new GraphQueryData(interface_data);
//                StatusData status = new StatusData(newQuery, newQuery.RawQuery, statusEmbedRunner.Output, Behavior.Device);

//                #region multi-hop expan
//                // travel four steps in the knowledge graph.
//                for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
//                {
//                    // given status, obtain the match. miniBatch * maxNeighbor number.
//                    CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
//                    cg.AddRunner(candidateActionRunner);

//                    StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior);
//                    cg.AddRunner(candEmbedRunner);

//                    DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
//                    cg.AddRunner(srcHiddenRunner);

//                    DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
//                    cg.AddRunner(candHiddenRunner);

//                    // alignment miniBatch * miniBatch * neighbor.
//                    VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
//                                                                                candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
//                    cg.AddRunner(attentionRunner);

//                    SeqVectorData Q = new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
//                                                        status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
//                                                        candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
//                                                        Behavior.Device);
//                    SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(Q, BuilderParameters.BETA, Behavior, true);
//                    cg.AddRunner(normAttRunner);

//                    status.MatchCandidate = candidateActionRunner.Output;
//                    status.MatchCandidateQ = Q;
//                    status.MatchCandidateProb = normAttRunner.Output;

//                    DNNRunner<HiddenBatchData> scoreRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, status.StateEmbed, Behavior);
//                    cg.AddRunner(scoreRunner);
//                    status.Score = scoreRunner.Output;

//                    BasicPolicyRunner policyRunner = null;

//                    if (Behavior.RunMode == DNNRunMode.Train)
//                    {
//                        policyRunner = new MCTSActionSamplingRunner(status, j, aMem, Behavior, i == BuilderParameters.MAX_HOP - 1);
//                    }
//                    else
//                    {
//                        policyRunner = new BeamSearchActionRunner(status, BuilderParameters.BeamSize, Behavior, i == BuilderParameters.MAX_HOP - 1);
//                    }
//                    cg.AddRunner(policyRunner);

//                    status.MaxMatchQ = policyRunner.MaxQ;

//                    if (i < BuilderParameters.MAX_HOP - 1)
//                    {
//                        #region version 2 of action selection.
//                        MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
//                        cg.AddRunner(srcExpRunner);

//                        MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, policyRunner.MatchPath, 2, Behavior);
//                        cg.AddRunner(tgtExpRunner);

//                        GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
//                        cg.AddRunner(stateRunner);
//                        #endregion.

//                        StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.LogProb,
//                            policyRunner.PreSelActIndex, policyRunner.PreStatusIndex, stateRunner.Output, Behavior.Device);

//                        status = nextStatus;
//                    }
//                }
//                #endregion.

//                //reward feedback runner.
//                RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(newQuery, aMem, Behavior);
//                cg.AddRunner(rewardFeedRunner);
//                Queries.Add(newQuery);
//            }

//            if (Behavior.RunMode == DNNRunMode.Train)
//            {
//                if (BuilderParameters.TRAIN_TYPE == 0)
//                {
//                    RewardRunner rewardRunner = new RewardRunner(Queries, aMem, Behavior);
//                    //ClassicalRewardRunner rewardRunner = new ClassicalRewardRunner(Queries, Behavior);
//                    cg.AddObjective(rewardRunner);
//                }
//                else if (BuilderParameters.TRAIN_TYPE == 1)
//                {
//                    RewardRunner qrewardRunner = new RewardRunner(Queries, aMem, Behavior);
//                    cg.AddObjective(qrewardRunner);
//                }
//            }
//            else
//            {
//                {
//                    MAPPredictionV2Runner predV2Runner = new MAPPredictionV2Runner(Queries[0], Behavior);
//                    cg.AddRunner(predV2Runner);
//                }
//            }
//            cg.SetDelegateModel(model);
//            return cg;
//        }

//        public class EpisodicLSTMModel : CompositeNNStructure
//        {
//            public LayerStructure EmbedLayer { get; set; }

//            public Tuple<LSTMStructure, LSTMStructure> BiLSTMLayer { get; set; }

//            public LayerStructure AttEmbed { get; set; }
//            public LSTMCell MatchLayer { get; set; }

//            public DNNStructure OutputDNN { get; set; }

//            public EpisodicLSTMModel(int sDim, DeviceType device)
//            {
//                EmbedLayer = DataPanel.InitWordEmbedLayers(device);

//                int inputDim = BuilderParameters.EMD_DIM;
//                LSTMStructure fw = AddLayer(new LSTMStructure(inputDim, BuilderParameters.LAYER_DIM, device));
//                LSTMStructure bw = AddLayer(new LSTMStructure(inputDim, BuilderParameters.LAYER_DIM, device));

//                BiLSTMLayer = new Tuple<LSTMStructure, LSTMStructure>(fw, bw);

//                int MemDim = BuilderParameters.LAYER_DIM.Last();
//                int InputDim = BuilderParameters.LAYER_DIM.Last();

//                AttEmbed = AddLayer(new LayerStructure())

//                int MatchDim = BuilderParameters.MATCH_DIM;



//                OutputDNN = AddLayer(new DNNStructure()
//            }

//            public void InitOptimization(RunnerBehavior behavior)
//            {
//                //InNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
//                //InRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

//                //CNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
//                //CRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
//                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
//            }
//        }


//        public override void Rock()
//        {
//            Logger.OpenLog(BuilderParameters.LogFile, !DeepNet.BuilderParameters.IsLogFile);

//            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
//            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
//            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

//            Logger.WriteLog("Loading Training/Test Data.");
//            DataPanel.Init();
//            Logger.WriteLog("Load Training/Test Data Finished.");


//        }

//        public class DataPanel
//        {
//            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainSrcBin = null;
//            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainTgtBin = null;
//            public static DataCashier<DenseBatchData, DenseDataStat> TrainLabel = null;

//            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidSrcBin = null;
//            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidTgtBin = null;
//            public static DataCashier<DenseBatchData, DenseDataStat> ValidLabel = null;

//            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestSrcBin = null;
//            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestTgtBin = null;
//            public static DataCashier<DenseBatchData, DenseDataStat> TestLabel = null;

//            public static ItemFreqIndexDictionary WordVocab = null;

//            public static LayerStructure InitWordEmbedLayers(DeviceType device)
//            {
//                LayerStructure wordEmbedding = null;
//                wordEmbedding = new LayerStructure(WordVocab.ItemDictSize, BuilderParameters.EMD_DIM,
//                        A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device, false);

//                #region Glove Work Embedding Initialization.
//                if (!BuilderParameters.INIT_EMD.Equals(string.Empty))
//                {
//                    int wordDim = BuilderParameters.EMD_DIM;

//                    Logger.WriteLog("Loading Glove Word Embedding ...");
//                    HashSet<int> HitWord = new HashSet<int>();
//                    //Dictionary<string, float[]> initEmbedding = new Dictionary<string, float[]>();
//                    using (StreamReader mreader = new StreamReader(BuilderParameters.INIT_EMD, Encoding.UTF8))
//                    {
//                        while (!mreader.EndOfStream)
//                        {
//                            string[] items = mreader.ReadLine().Split(' ');
//                            string word = items[0];
//                            float[] vec = new float[items.Length - 1];
//                            for (int i = 1; i < items.Length; i++)
//                            {
//                                vec[i - 1] = float.Parse(items[i]);
//                            }
//                            if (vec.Length != wordDim) { throw new Exception(string.Format("word {0} embedding {1} does not match with the dim {2}", word, vec.Length, wordDim)); }

//                            int wIdx = WordVocab.IndexItem(word);
//                            if (wIdx >= 0)
//                            {
//                                for (int i = 0; i < wordDim; i++) { wordEmbedding.weight.MemPtr[wIdx * wordDim + i] = vec[i]; }
//                                HitWord.Add(wIdx);
//                            }
//                        }
//                    }

//                    Logger.WriteLog("Init Glove Word Embedding ...");

//                    Logger.WriteLog("Vocab Size {0}, Glove Initialzie {1}!", WordVocab.ItemDictSize, HitWord.Count);

//                    if (BuilderParameters.UNK_INIT == 0)
//                    {
//                        Logger.WriteLog("UNK is initalized to zero!");
//                    }
//                    else if (BuilderParameters.UNK_INIT == 1)
//                    {
//                        Logger.WriteLog("UNK is initalized to random vector!");
//                    }

//                    foreach (KeyValuePair<string, int> item in WordVocab.ItemIndex.NameDict)
//                    {
//                        int wIdx = item.Value;
//                        string word = item.Key;

//                        if (!HitWord.Contains(wIdx))
//                        {
//                            if (BuilderParameters.UNK_INIT == 0)
//                            {
//                                for (int i = 0; i < wordDim; i++) { wordEmbedding.weight.MemPtr[wIdx * wordDim + i] = 0; }
//                            }
//                        }
//                    }
//                    wordEmbedding.weight.SyncFromCPU();
//                    Logger.WriteLog("Init Glove Word Embedding Done, {0} words initalized in total {1}.", HitWord.Count, WordVocab.ItemDictSize);
//                }
//                #endregion.

//                return wordEmbedding;
//            }

//            static List<Tuple<int, string[], string[]>> LoadData(string fileName)
//            {
//                List<Tuple<int, string[], string[]>> data = new List<Tuple<int, string[], string[]>>();
//                using (StreamReader mreader = new StreamReader(fileName))
//                {
//                    while (!mreader.EndOfStream)
//                    {
//                        string[] items = mreader.ReadLine().Split('\t');
//                        int label = int.Parse(items[0]);
//                        string[] tokens1 = items[1].Split(' ');
//                        string[] tokens2 = items[2].Split(' ');
//                        data.Add(new Tuple<int, string[], string[]>(label, tokens1, tokens2));
//                    }
//                }
//                return data;
//            }

//            static void Data2Bin(List<Tuple<int, string[], string[]>> data, string filePath)
//            {
//                IEnumerable<IEnumerable<int>> data_srcIdx = data.Select(i => i.Item2.Select(j => WordVocab.IndexItem(j)));
//                IEnumerable<IEnumerable<int>> data_tgtIdx = data.Select(i => i.Item2.Select(j => WordVocab.IndexItem(j)));
//                IEnumerable<float> label = data.Select(i => i.Item1 + 0.0f);

//                using (BinaryWriter writer = new BinaryWriter(new FileStream(filePath + ".src.bin", FileMode.Create, FileAccess.Write)))
//                {
//                    CommonExtractor.ExtractSeqSparseDataBinary(data_srcIdx, WordVocab.ItemDictSize, BuilderParameters.Mini_Batch, writer);
//                }

//                using (BinaryWriter writer = new BinaryWriter(new FileStream(filePath + ".tgt.bin", FileMode.Create, FileAccess.Write)))
//                {
//                    CommonExtractor.ExtractSeqSparseDataBinary(data_tgtIdx, WordVocab.ItemDictSize, BuilderParameters.Mini_Batch, writer);
//                }

//                using (BinaryWriter writer = new BinaryWriter(new FileStream(filePath + ".label.bin", FileMode.Create, FileAccess.Write)))
//                {
//                    CommonExtractor.ExtractLabelDataBinary(label, BuilderParameters.Mini_Batch, writer);
//                }
//            }

//            public static void Init()
//            {
//                List<Tuple<int, string[], string[]>> train_data = LoadData(BuilderParameters.Train);
//                List<Tuple<int, string[], string[]>> test_data = LoadData(BuilderParameters.Test);
//                List<Tuple<int, string[], string[]>> valid_data = LoadData(BuilderParameters.Valid);

//                if (!File.Exists(BuilderParameters.VOCAB))
//                {
//                    WordVocab = new ItemFreqIndexDictionary("wordDict");
//                    IEnumerable<string[]> strings = train_data.Select(i => i.Item2).Concat(train_data.Select(i => i.Item3)).
//                                                    Concat(test_data.Select(i => i.Item2)).Concat(test_data.Select(i => i.Item3)).
//                                                    Concat(valid_data.Select(i => i.Item2)).Concat(valid_data.Select(i => i.Item3));
//                    WordVocab.BuildIndex(strings);
//                    WordVocab.Save(new StreamWriter(BuilderParameters.VOCAB), true);
//                }
//                else
//                {
//                    WordVocab.Load(new StreamReader(BuilderParameters.VOCAB), true);
//                }

//                if (BuilderParameters.RunMode == DNNRunMode.Train)
//                {
//                    Data2Bin(train_data, BuilderParameters.Train);

//                    TrainSrcBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
//                    TrainTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
//                    TrainLabel.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
//                }

//                Data2Bin(valid_data, BuilderParameters.Valid);
//                ValidSrcBin.InitThreadSafePipelineCashier(100, false);
//                ValidTgtBin.InitThreadSafePipelineCashier(100, false);
//                ValidLabel.InitThreadSafePipelineCashier(100, false);

//                Data2Bin(test_data, BuilderParameters.Test);
//                TestSrcBin.InitThreadSafePipelineCashier(100, false);
//                TestTgtBin.InitThreadSafePipelineCashier(100, false);
//                TestLabel.InitThreadSafePipelineCashier(100, false);
//            }
//        }
//    }
//}
