﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn.Common
{
    public abstract class ArgumentsBase
    {
        protected ArgumentsBase()
        {
            Type type = this.GetType();
            var argProperties = this.GetType().GetProperties().Where(p => p.IsDefined(typeof(ArgumentAttribute), true));
            foreach (var prop in type.GetProperties())
            {
                ArgumentAttribute argAttr = (ArgumentAttribute)prop.GetCustomAttribute(typeof(ArgumentAttribute), true);
                if (argAttr != null)
                {
                    if (argAttr.Default != null)
                    {
                        if (argAttr.Default.GetType() != typeof(Type) && prop.PropertyType != typeof(Type))
                        {
                            prop.SetValue(this, argAttr.Default);
                        }
                        else
                        {
                            prop.SetValue(this, Activator.CreateInstance(argAttr.Default as Type));
                        }
                    }
                }
            }
        }

        public virtual void Usage(string appName = null)
        {
        }
    }

    public abstract class CmdArgumentsBase : ArgumentsBase
    {
        protected CmdArgumentsBase()
        {
        }

        [Argument("-h|-help", "Show help information.", IsPair = false)]
        public bool NeedHelp { get; set; }

        public Dictionary<int, string> UnrecognizedArgs { get; set; }

        public override void Usage(string appName = null)
        {
            var argProperties = this.GetType().GetProperties().Where(p => p.IsDefined(typeof(ArgumentAttribute), true));
            StringBuilder sb = new StringBuilder();
            StringBuilder cmd = new StringBuilder();
            
            appName = appName ?? Process.GetCurrentProcess().ProcessName;
            cmd.AppendFormat("{0} ", appName);

            foreach (var prop in argProperties)
            {
                ArgumentAttribute argFieldDef = (ArgumentAttribute)prop.GetCustomAttributes(typeof(ArgumentAttribute), true)[0];
                string switchStr = string.IsNullOrEmpty(argFieldDef.Switches) ? ("-" + prop.Name) : argFieldDef.Switches;
                if (argFieldDef.IsPair)
                {
                    Type propType = prop.PropertyType;
                    string typeName = propType.Name;
                    if (propType.IsGenericType)
                    {
                        if (propType.GetGenericTypeDefinition() == typeof(System.Nullable<>))
                        {
                            typeName = propType.GenericTypeArguments[0].Name;

                            if (IsNullableEnum(propType))
                            {
                                Type u = Nullable.GetUnderlyingType(propType);
                                typeName = string.Join("|", u.GetEnumNames());
                            }
                        }
                    }
                    else if (propType.IsEnum)
                    {
                        typeName = string.Join("|", propType.GetEnumNames());
                    }

                    sb.AppendFormat("{0}  [{2}]  {{{1}}} #{3}\r\n", switchStr, typeName, argFieldDef.Optional ? "Optional" : "Required", argFieldDef.Comments);

                    if (argFieldDef.Optional)
                    {
                        cmd.AppendFormat(" [{0} {{{1}}}]", switchStr, typeName);
                    }
                    else
                    {
                        cmd.AppendFormat(" {0} {{{1}}}", switchStr, typeName);
                    }
                }
                else
                {
                    sb.AppendFormat("{0}  [{1}]  {{Flag}} #{2}\r\n", switchStr, argFieldDef.Optional ? "Optional" : "Required", argFieldDef.Comments);
                    if (argFieldDef.Optional)
                    {
                        cmd.AppendFormat(" [{0}]", switchStr);
                    }
                    else
                    {
                        cmd.AppendFormat(" {0}", switchStr);
                    }
                }
            }

            Console.WriteLine(cmd);
            Console.WriteLine();
            Console.WriteLine(sb);
        }

        public static bool IsNullableEnum(Type t)
        {
            Type u = Nullable.GetUnderlyingType(t);
            return (u != null) && u.IsEnum;
        }
    }
}
