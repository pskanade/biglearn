﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn.Common
{
    public class NamedArgsParser
    {
        private Dictionary<string, NamedArgument> namedArgs;

        public NamedArgsParser(string[] args)
        {
            this.Parse(args);
        }

        public static NamedArgsParser Create(string[] args)
        {
            return new NamedArgsParser(args);
        }

        public bool IsContain(string name)
        {
            return this.Get(name) == null ? false : true;
        }

        public NamedArgument Get(string name)
        {
            if (this.namedArgs.ContainsKey(name))
            {
                return this.namedArgs[name];
            }
            else
            {
                return null;
            }
        }

        public int Get(string name, int defaultValue)
        {
            return this.Get(name) == null ? defaultValue : this.namedArgs[name].Integer;
        }

        public bool Get(string name, bool defaultValue)
        {
            return this.Get(name) == null ? defaultValue : this.namedArgs[name].Boolean;
        }

        public float Get(string name, float defaultValue)
        {
            return this.Get(name) == null ? defaultValue : this.namedArgs[name].Float;
        }

        public double Get(string name, double defaultValue)
        {
            return this.Get(name) == null ? defaultValue : this.namedArgs[name].Double;
        }

        public long Get(string name, long defaultValue)
        {
            return this.Get(name) == null ? defaultValue : this.namedArgs[name].Long;
        }

        public string Get(string name, string defaultValue)
        {
            return this.Get(name) == null ? defaultValue : this.namedArgs[name].Value;
        }

        public Dictionary<string, NamedArgument> Parse(string[] args)
        {
            this.namedArgs = new Dictionary<string, NamedArgument>(StringComparer.OrdinalIgnoreCase);
            foreach (var a in args)
            {
                int sep = a.IndexOf("=");
                if (sep > 0)
                {
                    string name = a.Substring(0, sep).Trim(' ', '\t');
                    string val = a.Substring(sep + 1).Trim(' ', '\t');
                    namedArgs[name] = new NamedArgument() { Name = name, Value = val };
                }
            }

            return namedArgs;
        }

        public class NamedArgument
        {
            public string Name { get; set; }

            public string Value { get; set; }

            public int Integer
            {
                get
                {
                    return int.Parse(this.Value);
                }
            }

            public long Long
            {
                get
                {
                    return long.Parse(this.Value);
                }
            }

            public bool Boolean
            {
                get
                {
                    return bool.Parse(this.Value);
                }
            }

            public float Float
            {
                get
                {
                    return float.Parse(this.Value);
                }
            }

            public double Double
            {
                get
                {
                    return double.Parse(this.Value);
                }
            }

            public string String
            {
                get
                {
                    return this.Value;
                }
            }
        }
    }
}
