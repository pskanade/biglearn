#!/bin/bash

ldlib=/usr/local/nccl/lib:/usr/local/cudnn-2.0
IFS=: read -ra var <<< "$LD_LIBRARY_PATH"
for v in ${var[@]}; do
	if [[ ! $v =~ .*"/usr/local/cudnn-2.0" ]] && [[ ! $v =~ .*"/usr/local/nccl/lib" ]]; then
		ldlib=$ldlib:$v
	fi
done
echo "Set \$LD_LIBRARY_PATH=$ldlib"
export LD_LIBRARY_PATH=$ldlib
echo $LD_LIBRARY_PATH


MV2_CUDA_SMP_IPC=1 
MV2_CUDA_BLOCK_SIZE=524288 
MV2_SMP_USE_CMA=0 
MV2_USE_CUDA=1 

while [[ $# -gt 1 ]]; do
	key="$1"

	case $key in
	-c|--config)
		Config=$2
		shift
		;;
	-g|--gpus)
		GPUs=$2
		shift
		;;
	-h|--help)
		echo "Usage ./run.sh -c|--config configfile -g|--gpus gpuids"
		echo "ConfigFile the path of the config file"
		echo "GPU ids The ids of gpus to be used, e.g. 0,1,2,3"
		exit 0
		;;
	*)
		echo "Invalid arguments input!"
		exit -1
		;;
	esac
	shift
done

if [[ -z $Config ]] || [[ -z $GPUs ]]; then
	echo "Invalid arguments input!"
	exit -1
fi

IFS=, read -ra GIDs <<< "$GPUs"

index=0
for v in ${GIDs[@]}; do
	cmdArgs[$index]="-n 1 mono BigLearn.DeepNet.exe -f $Config -AppArgs BUILDER:DSSM_GRAPH;GPUID:${GIDs[$index]}"
	index=$((index+1))
done

for c in ${!cmdArgs[*]}; do
	if [[ -n $exec ]]; then
		exec="$exec :"
	fi

	exec="$exec ${cmdArgs[$c]}"
done

mpirun $exec

