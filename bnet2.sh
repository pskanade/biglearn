
## build BigLearn.Base.dll
msbuild BigLearn.Base/BigLearn.csproj

## build BigLearn.Common.dll
msbuild DeepLearningLib/Common/BigLearn.Common.csproj

## build BigLearn.DeepNet.exe file.
msbuild DeepLearningLib/DeepNet/BigLearn.DeepNet.csproj

msbuild DeepLearningLib/DeepNet/BigLearn.Test.csproj
